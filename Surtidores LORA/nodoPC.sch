<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-RF">
<description>&lt;h3&gt;SparkFun RF, WiFi, Cellular, and Bluetooth&lt;/h3&gt;
In this library you'll find things that send or receive RF-- cellular modules, Bluetooth, WiFi, etc.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="RFM69HCW-XXXS2">
<description>&lt;h3&gt;Hope RF RFM69HCW RF Transciever&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://www.orcam.eu/res/Datablad/rfm69hcwv11.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<smd name="4" x="-7.5" y="1" dx="3" dy="1.2" layer="1"/>
<smd name="3" x="-7.5" y="3" dx="3" dy="1.2" layer="1"/>
<smd name="2" x="-7.5" y="5" dx="3" dy="1.2" layer="1"/>
<smd name="1" x="-7.5" y="7" dx="3" dy="1.2" layer="1"/>
<smd name="5" x="-7.5" y="-1" dx="3" dy="1.2" layer="1"/>
<smd name="6" x="-7.5" y="-3" dx="3" dy="1.2" layer="1"/>
<smd name="7" x="-7.5" y="-5" dx="3" dy="1.2" layer="1"/>
<smd name="8" x="-7.5" y="-7" dx="3" dy="1.2" layer="1"/>
<smd name="9" x="7.5" y="-7" dx="3" dy="1.2" layer="1"/>
<smd name="10" x="7.5" y="-5" dx="3" dy="1.2" layer="1"/>
<smd name="11" x="7.5" y="-3" dx="3" dy="1.2" layer="1"/>
<smd name="12" x="7.5" y="-1" dx="3" dy="1.2" layer="1"/>
<smd name="13" x="7.5" y="1" dx="3" dy="1.2" layer="1"/>
<smd name="14" x="7.5" y="3" dx="3" dy="1.2" layer="1"/>
<smd name="15" x="7.5" y="5" dx="3" dy="1.2" layer="1"/>
<smd name="16" x="7.5" y="7" dx="3" dy="1.2" layer="1"/>
<text x="0" y="8.255" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.3" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-8.127" y1="8.127" x2="8.127" y2="8.127" width="0.2032" layer="21"/>
<wire x1="8.127" y1="-8.127" x2="-8.127" y2="-8.127" width="0.2032" layer="21"/>
<wire x1="-4" y1="6" x2="-4" y2="2" width="0.2032" layer="51"/>
<wire x1="-4" y1="2" x2="0" y2="2" width="0.2032" layer="51"/>
<wire x1="0" y1="2" x2="0" y2="6" width="0.2032" layer="51"/>
<wire x1="0" y1="6" x2="-4" y2="6" width="0.2032" layer="51"/>
<text x="-5.485" y="5.35" size="1.27" layer="51" font="vector" ratio="12">1</text>
<wire x1="2" y1="-0.5" x2="2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="4" y1="1.5" x2="2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="4" y1="-0.5" x2="4" y2="1.5" width="0.2032" layer="51"/>
<wire x1="4" y1="-0.5" x2="2" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="-8" y1="-8" x2="8" y2="-8" width="0.127" layer="51"/>
<wire x1="8" y1="-8" x2="8" y2="8" width="0.127" layer="51"/>
<wire x1="8" y1="8" x2="-8" y2="8" width="0.127" layer="51"/>
<wire x1="-8" y1="8" x2="-8" y2="-8" width="0.127" layer="51"/>
<circle x="-9.089" y="8.392" radius="0.508" width="0" layer="21"/>
<wire x1="-8.127" y1="-8.127" x2="-8.127" y2="-7.859" width="0.2032" layer="21"/>
<wire x1="8.127" y1="-7.859" x2="8.127" y2="-8.127" width="0.2032" layer="21"/>
<wire x1="-8.127" y1="5.826" x2="-8.127" y2="6.1562" width="0.2032" layer="21"/>
<wire x1="8.127" y1="8.127" x2="8.127" y2="7.859" width="0.2032" layer="21"/>
<wire x1="-8.127" y1="7.859" x2="-8.127" y2="8.127" width="0.2032" layer="21"/>
<wire x1="-8.127" y1="3.826" x2="-8.127" y2="4.1562" width="0.2032" layer="21"/>
<wire x1="-8.127" y1="1.826" x2="-8.127" y2="2.1562" width="0.2032" layer="21"/>
<wire x1="-8.127" y1="-0.174" x2="-8.127" y2="0.1562" width="0.2032" layer="21"/>
<wire x1="-8.127" y1="-2.174" x2="-8.127" y2="-1.8438" width="0.2032" layer="21"/>
<wire x1="-8.127" y1="-4.174" x2="-8.127" y2="-3.8438" width="0.2032" layer="21"/>
<wire x1="-8.127" y1="-6.174" x2="-8.127" y2="-5.8438" width="0.2032" layer="21"/>
<wire x1="8.127" y1="-5.826" x2="8.127" y2="-6.1562" width="0.2032" layer="21"/>
<wire x1="8.127" y1="-3.826" x2="8.127" y2="-4.1562" width="0.2032" layer="21"/>
<wire x1="8.127" y1="-1.826" x2="8.127" y2="-2.1562" width="0.2032" layer="21"/>
<wire x1="8.127" y1="0.174" x2="8.127" y2="-0.1562" width="0.2032" layer="21"/>
<wire x1="8.127" y1="2.174" x2="8.127" y2="1.8438" width="0.2032" layer="21"/>
<wire x1="8.127" y1="4.174" x2="8.127" y2="3.8438" width="0.2032" layer="21"/>
<wire x1="8.127" y1="6.174" x2="8.127" y2="5.8438" width="0.2032" layer="21"/>
</package>
<package name="RFM69W-XXXS2">
<description>&lt;h3&gt;Hope RF RFM69HW RF Transceiver&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://www.hoperf.com/upload/rf/RFM69HW-V1.3.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-9.85" y1="8" x2="9.85" y2="8" width="0.127" layer="51"/>
<wire x1="9.85" y1="-8" x2="-9.85" y2="-8" width="0.127" layer="51"/>
<smd name="4" x="-9.35" y="1" dx="3" dy="1.2" layer="1"/>
<smd name="3" x="-9.35" y="3" dx="3" dy="1.2" layer="1"/>
<smd name="2" x="-9.35" y="5" dx="3" dy="1.2" layer="1"/>
<smd name="1" x="-9.35" y="7" dx="3" dy="1.2" layer="1"/>
<smd name="5" x="-9.35" y="-1" dx="3" dy="1.2" layer="1"/>
<smd name="6" x="-9.35" y="-3" dx="3" dy="1.2" layer="1"/>
<smd name="7" x="-9.35" y="-5" dx="3" dy="1.2" layer="1"/>
<smd name="8" x="-9.35" y="-7" dx="3" dy="1.2" layer="1"/>
<smd name="9" x="9.35" y="-7" dx="3" dy="1.2" layer="1"/>
<smd name="10" x="9.35" y="-5" dx="3" dy="1.2" layer="1"/>
<smd name="11" x="9.35" y="-3" dx="3" dy="1.2" layer="1"/>
<smd name="12" x="9.35" y="-1" dx="3" dy="1.2" layer="1"/>
<smd name="13" x="9.35" y="1" dx="3" dy="1.2" layer="1"/>
<smd name="14" x="9.35" y="3" dx="3" dy="1.2" layer="1"/>
<smd name="15" x="9.35" y="5" dx="3" dy="1.2" layer="1"/>
<smd name="16" x="9.35" y="7" dx="3" dy="1.2" layer="1"/>
<wire x1="-5" y1="-1" x2="-5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-5" y1="-6" x2="0" y2="-6" width="0.2032" layer="51"/>
<wire x1="0" y1="-6" x2="0" y2="-1" width="0.2032" layer="51"/>
<wire x1="0" y1="-1" x2="-5" y2="-1" width="0.2032" layer="51"/>
<wire x1="-5" y1="7" x2="-5" y2="4" width="0.2032" layer="51"/>
<wire x1="-5" y1="4" x2="5" y2="4" width="0.2032" layer="51"/>
<wire x1="5" y1="4" x2="5" y2="7" width="0.2032" layer="51"/>
<wire x1="5" y1="7" x2="-5" y2="7" width="0.2032" layer="51"/>
<text x="-6.985" y="6.35" size="1.27" layer="51" font="vector" ratio="12">1</text>
<text x="0" y="8.255" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-6.731" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-9.977" y1="8.127" x2="9.977" y2="8.127" width="0.2032" layer="21"/>
<wire x1="-9.85" y1="-8" x2="-9.85" y2="8" width="0.127" layer="51"/>
<wire x1="9.85" y1="8" x2="9.85" y2="-8" width="0.127" layer="51"/>
<wire x1="9.977" y1="-8.127" x2="-9.977" y2="-8.127" width="0.2032" layer="21"/>
<circle x="-10.435" y="8.381" radius="0.508" width="0" layer="21"/>
<wire x1="-9.977" y1="-8.127" x2="-9.977" y2="-7.875" width="0.2032" layer="21"/>
<wire x1="-9.977" y1="7.875" x2="-9.977" y2="8.127" width="0.2032" layer="21"/>
<wire x1="-9.977" y1="5.8166" x2="-9.977" y2="6.1722" width="0.2032" layer="21"/>
<wire x1="-9.977" y1="3.8166" x2="-9.977" y2="4.1722" width="0.2032" layer="21"/>
<wire x1="-9.977" y1="1.8166" x2="-9.977" y2="2.1722" width="0.2032" layer="21"/>
<wire x1="-9.977" y1="-0.1834" x2="-9.977" y2="0.1722" width="0.2032" layer="21"/>
<wire x1="-9.977" y1="-2.1834" x2="-9.977" y2="-1.8278" width="0.2032" layer="21"/>
<wire x1="-9.977" y1="-4.1834" x2="-9.977" y2="-3.8278" width="0.2032" layer="21"/>
<wire x1="-9.977" y1="-6.1834" x2="-9.977" y2="-5.8278" width="0.2032" layer="21"/>
<wire x1="9.977" y1="-5.8166" x2="9.977" y2="-6.1722" width="0.2032" layer="21"/>
<wire x1="9.977" y1="-3.8166" x2="9.977" y2="-4.1722" width="0.2032" layer="21"/>
<wire x1="9.977" y1="-1.8166" x2="9.977" y2="-2.1722" width="0.2032" layer="21"/>
<wire x1="9.977" y1="0.1834" x2="9.977" y2="-0.1722" width="0.2032" layer="21"/>
<wire x1="9.977" y1="2.1834" x2="9.977" y2="1.8278" width="0.2032" layer="21"/>
<wire x1="9.977" y1="4.1834" x2="9.977" y2="3.8278" width="0.2032" layer="21"/>
<wire x1="9.977" y1="6.1834" x2="9.977" y2="5.8278" width="0.2032" layer="21"/>
<wire x1="9.977" y1="-7.875" x2="9.977" y2="-8.127" width="0.2032" layer="21"/>
<wire x1="9.977" y1="8.127" x2="9.977" y2="7.875" width="0.2032" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RFM69H">
<description>&lt;h3&gt;Hope RF RFM69H ISM Transceiver Module&lt;/h3&gt; 
&lt;p&gt;The RFM69H is a transceiver module capable of operation 
over a wide frequency range, including the 
315,433,868 and 
915MHz license-free ISM (Industry Scientific and Medical) 
frequency bands. All major RF communication parameters 
are programmable and most of them  can  be  dynamically 
set.  The  RFM69H  offers  the unique advantage of 
programmable narrow-band and wide- band  communication 
modes. The RFM69H is optimized for low power 
consumption while offering high RF output power and 
channelized operation.
Compliance  ETSI and FCC 
regulations. &lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.hoperf.cn/upload/docs/RF/SX/RFM69H_DataSheet_v1.3.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="RESET" x="-12.7" y="-10.16" length="short" direction="in"/>
<pin name="DIO0" x="12.7" y="12.7" length="short" rot="R180"/>
<pin name="DIO1" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="DIO2" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="DIO3" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="DIO4" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="DIO5" x="12.7" y="0" length="short" rot="R180"/>
<pin name="3.3V" x="-12.7" y="12.7" length="short" direction="pwr"/>
<pin name="GND@1" x="12.7" y="-10.16" length="short" direction="pwr" rot="R180"/>
<pin name="ANT" x="12.7" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="GND@2" x="12.7" y="-5.08" length="short" direction="pwr" rot="R180"/>
<pin name="SCK" x="-12.7" y="0" length="short" direction="in"/>
<pin name="MISO" x="-12.7" y="5.08" length="short"/>
<pin name="MOSI" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="NSS" x="-12.7" y="-2.54" length="short" direction="in"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="15.494" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-10.16" y="-12.954" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RFM69H" prefix="U">
<description>&lt;h3&gt;Hope RF RFM69H ISM Transceiver Module&lt;/h3&gt; 
&lt;p&gt;The RFM69H is a transceiver module capable of operation over a wide frequency range, including the 315,433,868 and 915MHz license-free ISM (Industry Scientific and Medical) frequency bands. All major RF communication parameters are programmable and most of them  can  be  dynamically set.  The  RFM69H  offers  the unique advantage of programmable narrow-band and wide- band  communication modes. The RFM69H is optimized for low power consumption while offering high RF output power and channelized operation.Compliance  ETSI and FCC regulations. &lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.hoperf.cn/upload/docs/RF/SX/RFM69H_DataSheet_v1.3.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;SparkFun Products&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13909"&gt;RFM69HCW Wireless Transceiver - 915MHz&lt;/a&gt; (COM-13909)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13910"&gt;RFM69HCW Wireless Transceiver - 434MHz&lt;/a&gt; (COM-13910)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12775"&gt;SparkFun RFM69 Breakout (915MHz)&lt;/a&gt; (WRL-12775)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12823"&gt;SparkFun RFM69 Breakout (434MHz)&lt;/a&gt; (WRL-12823)&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RFM69H" x="0" y="-2.54"/>
</gates>
<devices>
<device name="CW-915" package="RFM69HCW-XXXS2">
<connects>
<connect gate="G$1" pin="3.3V" pad="13"/>
<connect gate="G$1" pin="ANT" pad="9"/>
<connect gate="G$1" pin="DIO0" pad="14"/>
<connect gate="G$1" pin="DIO1" pad="15"/>
<connect gate="G$1" pin="DIO2" pad="16"/>
<connect gate="G$1" pin="DIO3" pad="11"/>
<connect gate="G$1" pin="DIO4" pad="12"/>
<connect gate="G$1" pin="DIO5" pad="7"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@2" pad="8 10"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="3"/>
<connect gate="G$1" pin="NSS" pad="5"/>
<connect gate="G$1" pin="RESET" pad="6"/>
<connect gate="G$1" pin="SCK" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-11997"/>
<attribute name="VALUE" value="915MHz"/>
</technology>
</technologies>
</device>
<device name="W" package="RFM69W-XXXS2">
<connects>
<connect gate="G$1" pin="3.3V" pad="8"/>
<connect gate="G$1" pin="ANT" pad="10"/>
<connect gate="G$1" pin="DIO0" pad="2"/>
<connect gate="G$1" pin="DIO1" pad="3"/>
<connect gate="G$1" pin="DIO2" pad="4"/>
<connect gate="G$1" pin="DIO3" pad="5"/>
<connect gate="G$1" pin="DIO4" pad="6"/>
<connect gate="G$1" pin="DIO5" pad="7"/>
<connect gate="G$1" pin="GND@1" pad="9"/>
<connect gate="G$1" pin="GND@2" pad="11"/>
<connect gate="G$1" pin="MISO" pad="13"/>
<connect gate="G$1" pin="MOSI" pad="14"/>
<connect gate="G$1" pin="NSS" pad="15"/>
<connect gate="G$1" pin="RESET" pad="1"/>
<connect gate="G$1" pin="SCK" pad="12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CW-433" package="RFM69HCW-XXXS2">
<connects>
<connect gate="G$1" pin="3.3V" pad="13"/>
<connect gate="G$1" pin="ANT" pad="9"/>
<connect gate="G$1" pin="DIO0" pad="14"/>
<connect gate="G$1" pin="DIO1" pad="15"/>
<connect gate="G$1" pin="DIO2" pad="16"/>
<connect gate="G$1" pin="DIO3" pad="11"/>
<connect gate="G$1" pin="DIO4" pad="12"/>
<connect gate="G$1" pin="DIO5" pad="7"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@2" pad="8 10"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="3"/>
<connect gate="G$1" pin="NSS" pad="5"/>
<connect gate="G$1" pin="RESET" pad="6"/>
<connect gate="G$1" pin="SCK" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-12053"/>
<attribute name="VALUE" value="433MHz"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-coax" urn="urn:adsk.eagle:library:133">
<description>&lt;b&gt;Coax Connectors&lt;/b&gt;&lt;p&gt;
Radiall  and M/A COM.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="BU-SMA-G" library_version="1">
<description>FEMALE &lt;b&gt;SMA CONNECTOR&lt;/b&gt;&lt;p&gt;
Radiall&lt;p&gt;
distributor RS 193-9117</description>
<wire x1="-1.1" y1="3.4" x2="1.1" y2="3.4" width="0.2032" layer="21"/>
<wire x1="3.4" y1="1.1" x2="3.4" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-3.4" x2="-1.1" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="-1.1" x2="-3.4" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="3.4" x2="3.4" y2="3.4" width="0.2032" layer="51"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="-3.4" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-3.4" x2="-3.4" y2="-3.4" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-3.4" x2="-3.4" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.4" y1="3.3" x2="6.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="6.3" y1="3.3" x2="6.3" y2="2.9" width="0.2032" layer="21"/>
<wire x1="6.3" y1="2.9" x2="7.1" y2="2.9" width="0.2032" layer="21"/>
<wire x1="7.1" y1="2.9" x2="7.1" y2="3.3" width="0.2032" layer="21"/>
<wire x1="7.1" y1="3.3" x2="8" y2="3.3" width="0.2032" layer="21"/>
<wire x1="15" y1="3.4" x2="15.3" y2="2.9" width="0.2032" layer="21"/>
<wire x1="15.3" y1="2.9" x2="15.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="15.9" y1="-2.9" x2="15.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="8.9" y1="3.4" x2="15" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.4" y1="-3.3" x2="6.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="6.3" y1="-3.3" x2="6.3" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="6.3" y1="-2.9" x2="7.1" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="7.1" y1="-2.9" x2="7.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="7.1" y1="-3.3" x2="8" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="15" y1="-3.4" x2="15.3" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="15.3" y1="-2.9" x2="15.9" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="8.9" y1="-3.4" x2="15" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="15.3" y1="-2.9" x2="15.3" y2="2.9" width="0.2032" layer="21"/>
<wire x1="5.4" y1="-3.9" x2="5.4" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="5.4" y1="-3.3" x2="5.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="5.4" y1="3.3" x2="5.4" y2="3.9" width="0.2032" layer="21"/>
<wire x1="8" y1="4.4" x2="8.9" y2="4.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="-4.4" x2="8" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="8" y1="-2.2" x2="8" y2="2.2" width="0.2032" layer="21"/>
<wire x1="8" y1="-2.2" x2="8.9" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="8" y1="-2.2" x2="8" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8" y1="-3.3" x2="8" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="8" y1="2.2" x2="8.9" y2="2.2" width="0.2032" layer="21"/>
<wire x1="8" y1="2.2" x2="8" y2="3.3" width="0.2032" layer="21"/>
<wire x1="8" y1="3.3" x2="8" y2="4.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="2.2" x2="8.9" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="3.4" x2="8.9" y2="4.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="2.2" x2="8.9" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="8.9" y1="-4.4" x2="8.9" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="-3.4" x2="8.9" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="3.4" y1="3.9" x2="5.4" y2="3.9" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-3.9" x2="5.4" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="3.4" y1="3.9" x2="3.4" y2="3.4" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-3.4" x2="3.4" y2="-3.9" width="0.2032" layer="51"/>
<wire x1="6.3" y1="2.9" x2="6.3" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="7.1" y1="2.9" x2="7.1" y2="-2.9" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.27"/>
<pad name="2" x="-2.5499" y="2.5499" drill="1.778"/>
<pad name="3" x="2.5499" y="2.5499" drill="1.778"/>
<pad name="4" x="2.5499" y="-2.5499" drill="1.778"/>
<pad name="5" x="-2.5499" y="-2.5499" drill="1.778"/>
<text x="-2.54" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="BNC-FGND" library_version="1">
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.508" x2="-0.762" y2="0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.508" x2="-2.54" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BU-SMA-G" prefix="X" library_version="1">
<description>FEMALE &lt;b&gt;SMA CONNECTOR&lt;/b&gt;&lt;p&gt;
Radiall&lt;p&gt;
distributor RS 193-9117</description>
<gates>
<gate name="G1" symbol="BNC-FGND" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BU-SMA-G">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="2 3 4 5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-IC-Power">
<description>&lt;h3&gt;SparkFun Power Driver and Management ICs&lt;/h3&gt;
In this library you'll find anything that has to do with power delivery, or making power supplies.
&lt;p&gt;Contents:
&lt;ul&gt;&lt;li&gt;LDOs&lt;/li&gt;
&lt;li&gt;Boost/Buck controllers&lt;/li&gt;
&lt;li&gt;Charge pump controllers&lt;/li&gt;
&lt;li&gt;Power sequencers&lt;/li&gt;
&lt;li&gt;Power switches&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TO220-ADJ">
<wire x1="5.08" y1="-1.905" x2="-5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="1.905" x2="-5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.54" x2="8.255" y2="2.54" width="0.2032" layer="51"/>
<wire x1="8.255" y1="2.54" x2="8.255" y2="-5.715" width="0.2032" layer="51"/>
<wire x1="8.255" y1="-5.715" x2="9.525" y2="-5.715" width="0.2032" layer="51"/>
<wire x1="9.525" y1="-5.715" x2="9.525" y2="3.81" width="0.2032" layer="51"/>
<wire x1="9.525" y1="3.81" x2="-9.525" y2="3.81" width="0.2032" layer="51"/>
<wire x1="-9.525" y1="3.81" x2="-9.525" y2="-5.715" width="0.2032" layer="51"/>
<wire x1="-9.525" y1="-5.715" x2="-8.255" y2="-5.715" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="-5.715" x2="-8.255" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="2.54" x2="-5.08" y2="2.54" width="0.2032" layer="51"/>
<pad name="ADJ" x="-2.54" y="0" drill="1" diameter="1.8796" shape="square"/>
<pad name="OUT" x="0" y="0" drill="1" diameter="1.8796"/>
<pad name="IN" x="2.54" y="0" drill="1" diameter="1.8796"/>
</package>
<package name="SOT223">
<description>&lt;b&gt;SOT-223&lt;/b&gt;</description>
<wire x1="3.2766" y1="1.651" x2="3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.2766" y1="-1.651" x2="-3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="-1.651" x2="-3.2766" y2="1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="1.651" x2="3.2766" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="2" x="0" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="3" x="2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="4" x="0" y="3.099" dx="3.6" dy="2.2" layer="1"/>
<text x="-0.8255" y="4.5085" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
</package>
<package name="V-REG_DPACK">
<description>&lt;b&gt;DPAK&lt;/b&gt;&lt;p&gt;
PLASTIC PACKAGE CASE 369C-01&lt;br&gt;
Source: http://www.onsemi.co.jp .. LM317M-D.PDF</description>
<wire x1="3.2766" y1="3.8354" x2="3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-2.159" x2="-3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.159" x2="-3.2766" y2="3.8354" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="3.835" x2="3.2774" y2="3.8346" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.937" x2="-2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.6482" x2="-2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.1054" x2="2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.1054" x2="2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.6482" x2="2.5654" y2="3.937" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.937" x2="-2.5654" y2="3.937" width="0.2032" layer="51"/>
<smd name="1" x="-2.28" y="-4.8" dx="1.6" dy="3" layer="1"/>
<smd name="3" x="2.28" y="-4.8" dx="1.6" dy="3" layer="1"/>
<smd name="4" x="0" y="2.38" dx="5.8" dy="6.2" layer="1"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.0226" x2="0.4318" y2="-2.2606" layer="21"/>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="3.937"/>
<vertex x="-2.5654" y="4.6482"/>
<vertex x="-2.1082" y="5.1054"/>
<vertex x="2.1082" y="5.1054"/>
<vertex x="2.5654" y="4.6482"/>
<vertex x="2.5654" y="3.937"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="78ADJ-2">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="2.54" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">ADJ</text>
<text x="-4.445" y="-0.635" size="1.524" layer="95">IN</text>
<text x="0.635" y="-0.635" size="1.524" layer="95">OUT</text>
<pin name="IN" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="ADJ" x="0" y="-7.62" visible="off" length="short" direction="in" rot="R90"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="V_REG_317" prefix="U">
<description>&lt;b&gt;Voltage Regulator&lt;/b&gt;
Standard LM317 adjustable voltage regulator. AOI (Adjust Output Input). Google 'LM317 Calculator' for easy to use app to get the two resistor values needed. 240/720 for 5V output. 240/390 for 3.3V output. Spark Fun Electronics SKU : COM-00527</description>
<gates>
<gate name="G$1" symbol="78ADJ-2" x="0" y="0"/>
</gates>
<devices>
<device name="SINK" package="TO220-ADJ">
<connects>
<connect gate="G$1" pin="ADJ" pad="ADJ"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUT" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="SOT223">
<connects>
<connect gate="G$1" pin="ADJ" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DPACK" package="V-REG_DPACK">
<connects>
<connect gate="G$1" pin="ADJ" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-09888" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-wago-500" urn="urn:adsk.eagle:library:195">
<description>&lt;b&gt;Wago Screw Clamps&lt;/b&gt;&lt;p&gt;
Grid 5.00 mm&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="W237-102" urn="urn:adsk.eagle:footprint:10676/1" library_version="1">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<wire x1="-3.491" y1="-2.286" x2="-1.484" y2="-0.279" width="0.254" layer="51"/>
<wire x1="1.488" y1="-2.261" x2="3.469" y2="-0.254" width="0.254" layer="51"/>
<wire x1="-4.989" y1="-5.461" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-5.461" x2="-4.989" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-3.389" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-3.389" y1="-3.073" x2="-1.611" y2="-3.073" width="0.1524" layer="51"/>
<wire x1="-1.611" y1="-3.073" x2="1.615" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="3.393" y1="-3.073" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-4.989" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="-3.073" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.531" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-3.073" x2="3.393" y2="-3.073" width="0.1524" layer="51"/>
<circle x="-2.5" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-2.5" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<circle x="2.5038" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="2.5038" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<text x="-5.04" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.8462" y="-5.0038" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.532" y="0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.421" y="0.635" size="1.27" layer="21" ratio="10">2</text>
</package>
<package name="W237-103" urn="urn:adsk.eagle:footprint:10678/1" library_version="1">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<wire x1="4.093" y1="-2.255" x2="5.897" y2="-0.299" width="0.254" layer="51"/>
<wire x1="-0.911" y1="-2.331" x2="0.994" y2="-0.299" width="0.254" layer="51"/>
<wire x1="-5.991" y1="-2.306" x2="-3.984" y2="-0.299" width="0.254" layer="51"/>
<wire x1="-7.489" y1="-5.481" x2="7.497" y2="-5.481" width="0.1524" layer="21"/>
<wire x1="7.497" y1="3.714" x2="7.497" y2="3.511" width="0.1524" layer="21"/>
<wire x1="7.497" y1="3.714" x2="-7.489" y2="3.714" width="0.1524" layer="21"/>
<wire x1="-7.489" y1="-5.481" x2="-7.489" y2="-3.093" width="0.1524" layer="21"/>
<wire x1="-7.489" y1="-3.093" x2="-5.889" y2="-3.093" width="0.1524" layer="21"/>
<wire x1="-5.889" y1="-3.093" x2="-4.111" y2="-3.093" width="0.1524" layer="51"/>
<wire x1="-4.111" y1="-3.093" x2="-0.885" y2="-3.093" width="0.1524" layer="21"/>
<wire x1="0.893" y1="-3.093" x2="4.119" y2="-3.093" width="0.1524" layer="21"/>
<wire x1="5.897" y1="-3.093" x2="7.497" y2="-3.093" width="0.1524" layer="21"/>
<wire x1="-7.489" y1="-3.093" x2="-7.489" y2="3.511" width="0.1524" layer="21"/>
<wire x1="7.497" y1="-3.093" x2="7.497" y2="-5.481" width="0.1524" layer="21"/>
<wire x1="7.497" y1="3.511" x2="-7.489" y2="3.511" width="0.1524" layer="21"/>
<wire x1="7.497" y1="3.511" x2="7.497" y2="-3.093" width="0.1524" layer="21"/>
<wire x1="-7.489" y1="3.511" x2="-7.489" y2="3.714" width="0.1524" layer="21"/>
<wire x1="-0.885" y1="-3.093" x2="0.893" y2="-3.093" width="0.1524" layer="51"/>
<wire x1="4.119" y1="-3.093" x2="5.897" y2="-3.093" width="0.1524" layer="51"/>
<circle x="-5" y="-1.29" radius="1.4986" width="0.1524" layer="51"/>
<circle x="5.0076" y="-1.29" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-5" y="2.1898" radius="0.508" width="0.1524" layer="21"/>
<circle x="5.0076" y="2.1898" radius="0.508" width="0.1524" layer="21"/>
<circle x="0.0038" y="-1.29" radius="1.4986" width="0.1524" layer="51"/>
<circle x="0.0038" y="2.1898" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-5" y="-1.29" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="0" y="-1.29" drill="1.1938" shape="long" rot="R90"/>
<pad name="3" x="5" y="-1.29" drill="1.1938" shape="long" rot="R90"/>
<text x="-6.905" y="0.615" size="1.27" layer="51" ratio="10">1</text>
<text x="-1.8504" y="0.5642" size="1.27" layer="51" ratio="10">2</text>
<text x="3.1534" y="0.615" size="1.27" layer="51" ratio="10">3</text>
<text x="-5.3048" y="-4.9476" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.6858" y="-7.4622" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="W237-102" urn="urn:adsk.eagle:package:10688/1" type="box" library_version="1">
<description>WAGO SCREW CLAMP</description>
</package3d>
<package3d name="W237-103" urn="urn:adsk.eagle:package:10691/1" type="box" library_version="1">
<description>WAGO SCREW CLAMP</description>
</package3d>
</packages3d>
<symbols>
<symbol name="KL" urn="urn:adsk.eagle:symbol:10675/1" library_version="1">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="KL+V" urn="urn:adsk.eagle:symbol:10673/1" library_version="1">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="W237-102" urn="urn:adsk.eagle:component:10702/1" prefix="X" uservalue="yes" library_version="1">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="KL+V" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-102">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10688/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="237-102" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="70K9898" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="W237-103" urn="urn:adsk.eagle:component:10707/1" prefix="X" uservalue="yes" library_version="1">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="KL" x="0" y="0" addlevel="always"/>
<gate name="-3" symbol="KL+V" x="0" y="-5.08" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-103">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
<connect gate="-3" pin="KL" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10691/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="237-103" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="18M7116" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="1210">
<description>&lt;p&gt;&lt;b&gt;Generic 3225 (1210) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.5365" y1="1.1865" x2="1.5365" y2="1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="1.1865" x2="1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="-1.1865" x2="-1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="-1.5365" y1="-1.1865" x2="-1.5365" y2="1.1865" width="0.127" layer="51"/>
<smd name="1" x="-1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<smd name="2" x="1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<text x="0" y="1.397" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.59" y1="1.45" x2="2.59" y2="1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="1.45" x2="2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="-1.45" x2="-2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="-2.59" y1="-1.45" x2="-2.59" y2="1.45" width="0.0508" layer="39"/>
</package>
<package name="1206">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="12PF" prefix="C" uservalue="yes">
<description>&lt;h3&gt;12pF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-50V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09137" constant="no"/>
<attribute name="VALUE" value="12pF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22UF" prefix="C">
<description>&lt;h3&gt;22µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0805-6.3V-20%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08402"/>
<attribute name="VALUE" value="22uF"/>
</technology>
</technologies>
</device>
<device name="-1210-16V-20%" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2.2UF" prefix="C">
<description>&lt;h3&gt;2.2µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-10V-20%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-07888" constant="no"/>
<attribute name="VALUE" value="2.2uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-(+80/-20%)" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11624"/>
<attribute name="VALUE" value="2.2uF"/>
</technology>
</technologies>
</device>
<device name="-1206-50V-10%" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-10009"/>
<attribute name="VALUE" value="2.2uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED_5MM">
<description>&lt;B&gt;LED 5mm PTH&lt;/B&gt;&lt;p&gt;
5 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED-IR-THRU&lt;/li&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM">
<description>&lt;h3&gt;LED 3MM PTH&lt;/h3&gt;

3 mm, round.

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="22" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="22" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="22" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="22" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="22"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="22"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED-1206">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
</package>
<package name="LED-0603">
<description>&lt;B&gt;LED 0603 SMT&lt;/B&gt;&lt;p&gt;
0603, surface mount.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.075inch &lt;/li&gt;
&lt;li&gt;Area: 0.06" x 0.03"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED - BLUE&lt;/li&gt;</description>
<smd name="C" x="0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<smd name="A" x="-0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<text x="0" y="0.635" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5875" y1="0.47625" x2="1.5875" y2="-0.47625" width="0.127" layer="21"/>
<wire x1="0.15875" y1="0.47625" x2="0.15875" y2="0" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="0.15875" y2="-0.47625" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="-0.3175" width="0.127" layer="51"/>
</package>
<package name="LED_10MM">
<description>&lt;B&gt;LED 10mm PTH&lt;/B&gt;&lt;p&gt;
10 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.2inch&lt;/li&gt;
&lt;li&gt;Diameter: 10mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="22"/>
<text x="0" y="5.715" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.715" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="51"/>
</package>
<package name="FKIT-LED-1206">
<description>&lt;B&gt;LED 1206 SMT&lt;/B&gt;&lt;p&gt;
1206, surface mount
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125"x 0.06" &lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="0.3175" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="-0.3175" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM-NS">
<description>&lt;h3&gt;LED 3MM PTH- No Silk&lt;/h3&gt;

3 mm, round, no silk outline of package

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.8923" y1="1.2954" x2="1.8923" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.8923" y1="-1.2954" x2="1.8923" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED_5MM-KIT">
<description>&lt;h3&gt;LED 5mm KIT PTH&lt;/h3&gt;
&lt;p&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example Device:
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED-1206-BOTTOM">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-3.429" y="-4.572" size="1.778" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="1.905" y="-4.572" size="1.778" layer="96" font="vector" rot="R90" align="top-left">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LED (Generic)&lt;/b&gt;
&lt;p&gt;Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. Generic LEDs with no color specified.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED_3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08794" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED_10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED_3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED_5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="AXIAL-0.3">
<description>&lt;h3&gt;AXIAL-0.3&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1">
<description>&lt;h3&gt;AXIAL-0.1&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="21" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1-KIT">
<description>&lt;h3&gt;AXIAL-0.1-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.1 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="1.27" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.27OHM" prefix="R">
<description>&lt;h3&gt;0.27Ω resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08787"/>
<attribute name="VALUE" value="0.27"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10KOHM" prefix="R">
<description>&lt;h3&gt;10kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-HORIZ-1/4W-1%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-1%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-1%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/4W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-1%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/6W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/6W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/6W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/6W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-00824"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-DiscreteSemi">
<description>&lt;h3&gt;SparkFun Discrete Semiconductors&lt;/h3&gt;
This library contains diodes, optoisolators, TRIACs, MOSFETs, transistors, etc. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SOD-323">
<description>SOD-323 (Small Outline Diode)</description>
<wire x1="-1.77" y1="0.625" x2="-1.77" y2="-0.625" width="0.2032" layer="21"/>
<smd name="C" x="-1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<smd name="A" x="1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-0.9" y1="0.625" x2="0.9" y2="0.625" width="0.2032" layer="21"/>
<wire x1="-0.9" y1="-0.625" x2="0.9" y2="-0.625" width="0.2032" layer="21"/>
</package>
<package name="SOD-523">
<description>SOD-523 (Small Outline Diode)</description>
<smd name="C" x="0.7" y="0" dx="0.4" dy="0.4" layer="1"/>
<smd name="A" x="-0.7" y="0" dx="0.4" dy="0.4" layer="1"/>
<wire x1="-0.625" y1="-0.425" x2="0.625" y2="-0.425" width="0.127" layer="21"/>
<wire x1="0.625" y1="0.425" x2="-0.625" y2="0.425" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.4" x2="0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.4" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.1" x2="0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.4" x2="0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="-0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.4" x2="-0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.1" x2="0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.1" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="1.1176" y1="0.3048" x2="1.1176" y2="-0.3048" width="0.2032" layer="21"/>
</package>
<package name="SMA-DIODE">
<description>&lt;B&gt;Diode&lt;/B&gt;
&lt;p&gt;Basic SMA packaged diode. Good for reverse polarization protection. Common part #: MBRA140&lt;/p&gt;
&lt;p&gt;SMA is the smallest package in the DO-214 standard (DO-214AC)&lt;/p&gt;</description>
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1" x2="3.175" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.15" y="0" dx="1.27" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.15" y="0" dx="1.27" dy="1.47" layer="1"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT23-3">
<description>SOT23-3</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<text x="-1.651" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.651" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="DPAK">
<wire x1="3.2766" y1="2.4654" x2="3.277" y2="-3.729" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-3.729" x2="-3.277" y2="-3.729" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-3.729" x2="-3.2766" y2="2.4654" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="2.465" x2="3.2774" y2="2.4646" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="2.567" x2="-2.5654" y2="3.2782" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.2782" x2="-2.1082" y2="3.7354" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="3.7354" x2="2.1082" y2="3.7354" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="3.7354" x2="2.5654" y2="3.2782" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.2782" x2="2.5654" y2="2.567" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="2.567" x2="-2.5654" y2="2.567" width="0.2032" layer="51"/>
<rectangle x1="-2.7178" y1="-6.7262" x2="-1.8542" y2="-3.8306" layer="51"/>
<rectangle x1="1.8542" y1="-6.7262" x2="2.7178" y2="-3.8306" layer="51"/>
<rectangle x1="-0.4318" y1="-4.5926" x2="0.4318" y2="-3.8306" layer="21"/>
<smd name="1" x="-2.28" y="-5.31" dx="1.6" dy="3" layer="1"/>
<smd name="3" x="2.28" y="-5.31" dx="1.6" dy="3" layer="1"/>
<smd name="4" x="0" y="1.588" dx="4.826" dy="5.715" layer="1"/>
<text x="-3.81" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="3.81" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="2.567"/>
<vertex x="-2.5654" y="3.2782"/>
<vertex x="-2.1082" y="3.7354"/>
<vertex x="2.1082" y="3.7354"/>
<vertex x="2.5654" y="3.2782"/>
<vertex x="2.5654" y="2.567"/>
</polygon>
</package>
<package name="SOT323">
<wire x1="1.1224" y1="0.6604" x2="1.1224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1224" y1="-0.6604" x2="-1.1224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1224" y1="-0.6604" x2="-1.1224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1224" y1="0.6604" x2="1.1224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.673" y1="0.7" x2="-1.1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.1" y1="0.7" x2="-1.1" y2="-0.354" width="0.2032" layer="21"/>
<wire x1="0.673" y1="0.7" x2="1.1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.354" width="0.2032" layer="21"/>
<smd name="1" x="-0.65" y="-0.925" dx="0.7" dy="0.7" layer="1"/>
<smd name="2" x="0.65" y="-0.925" dx="0.7" dy="0.7" layer="1"/>
<smd name="3" x="0" y="0.925" dx="0.7" dy="0.7" layer="1"/>
<text x="-1.27" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.27" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="TO220V">
<description>&lt;b&gt;TO 220 Vertical&lt;/b&gt; Package works with various parts including N-Channel MOSFET SparkFun SKU: COM-10213</description>
<wire x1="-5.08" y1="2.032" x2="-5.08" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.032" x2="5.08" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.032" x2="-5.08" y2="2.032" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="2.032" x2="-5.08" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="3.048" x2="5.08" y2="3.048" width="0.2032" layer="21"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="2.032" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-0.381" x2="-4.191" y2="-1.27" width="0.2032" layer="21" curve="92.798868"/>
<wire x1="4.191" y1="-1.27" x2="5.08" y2="-0.381" width="0.2032" layer="21" curve="92.798868"/>
<wire x1="-4.191" y1="-1.27" x2="-3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.191" y1="-1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<rectangle x1="-5.08" y1="2.032" x2="5.08" y2="3.048" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.334" y="0.254" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="5.334" y="0.254" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="TO-263/D2PAK">
<wire x1="5" y1="-1" x2="5" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="5" y1="-3.4" x2="-5" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="-5" y1="-3.4" x2="-5" y2="-1" width="0.2032" layer="21"/>
<rectangle x1="-3.27" y1="-7.6" x2="-1.81" y2="-3.4" layer="51"/>
<rectangle x1="-0.73" y1="-4.9" x2="0.73" y2="-3.4" layer="21"/>
<smd name="1" x="-2.54" y="-7.045" dx="2.32" dy="3.81" layer="1"/>
<smd name="3" x="2.54" y="-7.045" dx="2.32" dy="3.81" layer="1"/>
<smd name="2" x="0" y="4.125" dx="11" dy="9.65" layer="1"/>
<text x="-5.715" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="5.715" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
<polygon width="0.1998" layer="51">
<vertex x="-5" y="6.25"/>
<vertex x="-5" y="7"/>
<vertex x="-1" y="7.65"/>
<vertex x="1" y="7.65"/>
<vertex x="5" y="7"/>
<vertex x="5" y="6.25"/>
</polygon>
<wire x1="-5" y1="6.25" x2="-5" y2="-0.65" width="0.127" layer="51"/>
<wire x1="5" y1="-0.65" x2="5" y2="6.25" width="0.127" layer="51"/>
<wire x1="-1.81" y1="-3.4" x2="-1.81" y2="-7.6" width="0.127" layer="51"/>
<wire x1="-1.81" y1="-7.6" x2="-3.27" y2="-7.6" width="0.127" layer="51"/>
<wire x1="-3.27" y1="-7.6" x2="-3.27" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-3.27" y1="-3.4" x2="-1.81" y2="-3.4" width="0.127" layer="51"/>
<wire x1="1.81" y1="-3.4" x2="1.81" y2="-7.6" width="0.127" layer="51"/>
<wire x1="1.81" y1="-7.6" x2="3.27" y2="-7.6" width="0.127" layer="51"/>
<wire x1="3.27" y1="-7.6" x2="3.27" y2="-3.4" width="0.127" layer="51"/>
<wire x1="3.27" y1="-3.4" x2="1.81" y2="-3.4" width="0.127" layer="51"/>
<rectangle x1="1.81" y1="-7.6" x2="3.27" y2="-3.4" layer="51"/>
<rectangle x1="-3.27" y1="-4.9" x2="-1.81" y2="-3.4" layer="21"/>
<rectangle x1="1.81" y1="-4.9" x2="3.27" y2="-3.4" layer="21"/>
</package>
<package name="SO08">
<description>SOIC, 0.15 inch width</description>
<wire x1="2.3368" y1="1.9463" x2="-2.3368" y2="1.9463" width="0.2032" layer="21"/>
<wire x1="2.4368" y1="-1.9463" x2="2.7178" y2="-1.5653" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.7178" y1="1.4653" x2="-2.3368" y2="1.9463" width="0.2032" layer="21" curve="-90.023829"/>
<wire x1="2.3368" y1="1.9463" x2="2.7178" y2="1.5653" width="0.2032" layer="21" curve="-90.030084"/>
<wire x1="-2.7178" y1="-1.6653" x2="-2.3368" y2="-1.9463" width="0.2032" layer="21" curve="90.060185"/>
<wire x1="-2.3368" y1="-1.9463" x2="2.4368" y2="-1.9463" width="0.2032" layer="21"/>
<wire x1="2.7178" y1="-1.5653" x2="2.7178" y2="1.5653" width="0.2032" layer="21"/>
<wire x1="-2.667" y1="0.6096" x2="-2.667" y2="-0.6604" width="0.2032" layer="21" curve="-180"/>
<wire x1="-2.7178" y1="1.4526" x2="-2.7178" y2="0.6096" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="-1.6653" x2="-2.7178" y2="-0.6604" width="0.2032" layer="21"/>
<rectangle x1="-2.159" y1="-3.302" x2="-1.651" y2="-2.2733" layer="51"/>
<rectangle x1="-0.889" y1="-3.302" x2="-0.381" y2="-2.2733" layer="51"/>
<rectangle x1="0.381" y1="-3.302" x2="0.889" y2="-2.2733" layer="51"/>
<rectangle x1="1.651" y1="-3.302" x2="2.159" y2="-2.2733" layer="51"/>
<rectangle x1="-0.889" y1="2.286" x2="-0.381" y2="3.302" layer="51"/>
<rectangle x1="0.381" y1="2.286" x2="0.889" y2="3.302" layer="51"/>
<rectangle x1="1.651" y1="2.286" x2="2.159" y2="3.302" layer="51"/>
<smd name="1" x="-1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="2" x="-0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="3" x="0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="4" x="1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="5" x="1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="6" x="0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="7" x="-0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="8" x="-1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<text x="-3.175" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="3.81" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-2.159" y1="2.286" x2="-1.651" y2="3.302" layer="51"/>
<polygon width="0.002540625" layer="21">
<vertex x="-2.69875" y="-2.38125" curve="90"/>
<vertex x="-3.01625" y="-2.06375" curve="90"/>
<vertex x="-3.33375" y="-2.38125" curve="90"/>
<vertex x="-3.01625" y="-2.69875" curve="90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="DIODE-SCHOTTKY">
<description>&lt;h3&gt; Schottky Diode&lt;/h3&gt;
Diode with low voltage drop</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.778" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="94"/>
<text x="-2.54" y="2.032" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.27" x2="0.762" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="1.016" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-1.27" y="1.27"/>
<vertex x="1.27" y="0"/>
<vertex x="-1.27" y="-1.27"/>
</polygon>
</symbol>
<symbol name="LABELED-NMOS">
<description>&lt;h3&gt; N-channel MOSFET transistor&lt;/h3&gt;
Switches electronic signals</description>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="0.6858" x2="-1.9812" y2="-0.8382" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.2954" x2="-1.9812" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.905" x2="-1.9812" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="2.54" x2="-1.9812" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="1.8034" x2="-1.9812" y2="1.0922" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.2192" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.7112" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="1.8034" x2="2.54" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.8034" x2="2.54" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.5588" x2="3.302" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.5588" x2="1.778" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="1.778" y="-0.7112"/>
<vertex x="2.54" y="0.5588"/>
<vertex x="3.302" y="-0.7112"/>
</polygon>
<wire x1="3.302" y1="0.5588" x2="3.4798" y2="0.7366" width="0.1524" layer="94"/>
<wire x1="1.6002" y1="0.381" x2="1.778" y2="0.5588" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-1.9812" y="0"/>
<vertex x="-1.2192" y="0.254"/>
<vertex x="-1.2192" y="-0.254"/>
</polygon>
<text x="5.08" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIODE-SCHOTTKY" prefix="D">
<description>&lt;h3&gt;Schottky diode&lt;/h3&gt;
&lt;p&gt;A Schottky diode is a semiconductor diode which has a low forward voltage drop and a very fast switching action.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE-SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="-BAT20J" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11623"/>
<attribute name="VALUE" value="1A/23V/620mV"/>
</technology>
</technologies>
</device>
<device name="-RB751S40" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11018"/>
<attribute name="VALUE" value="120mA/40V/370mV"/>
</technology>
</technologies>
</device>
<device name="-SS14" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08053"/>
<attribute name="VALUE" value="1A/40V/500mV"/>
</technology>
</technologies>
</device>
<device name="-PMEG4005EJ" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-10955"/>
<attribute name="VALUE" value="0.5A/40V/420mV"/>
</technology>
</technologies>
</device>
<device name="-B340A" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09886"/>
<attribute name="VALUE" value="3A/40V/500mV"/>
</technology>
</technologies>
</device>
<device name="-ZLLS500" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08411"/>
<attribute name="VALUE" value="700mA/40V/533mV"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-NCH" prefix="Q">
<description>&lt;h3&gt;N-channel MOSFETs&lt;/h3&gt;
&lt;p&gt;Voltage controlled devices that allow control of high current outputs.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13261”&gt;SparkFun OpenScale&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12651”&gt;SparkFun Digital Sandbox&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/10182”&gt;SparkFun Monster Moto Shield&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/11214”&gt;SparkFun MOSFET Power Controller&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="NMOS" symbol="LABELED-NMOS" x="0" y="0"/>
</gates>
<devices>
<device name="-FDD8780" package="DPAK">
<connects>
<connect gate="NMOS" pin="D" pad="4"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-09984"/>
<attribute name="VALUE" value="35A/25V/8.5mΩ"/>
</technology>
</technologies>
</device>
<device name="-2N7002PW" package="SOT323">
<connects>
<connect gate="NMOS" pin="D" pad="3"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-11151"/>
<attribute name="VALUE" value="310mA/60V/1.6Ω"/>
</technology>
</technologies>
</device>
<device name="-FQP30N06L" package="TO220V">
<connects>
<connect gate="NMOS" pin="D" pad="2"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-10060"/>
<attribute name="VALUE" value="60V/32A/35mΩ"/>
</technology>
</technologies>
</device>
<device name="-BSS138" package="SOT23-3">
<connects>
<connect gate="NMOS" pin="D" pad="3"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-00830"/>
<attribute name="VALUE" value="220mA/50V/3.5Ω"/>
</technology>
</technologies>
</device>
<device name="-PSMN7R0" package="TO-263/D2PAK">
<connects>
<connect gate="NMOS" pin="D" pad="2"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-12437"/>
<attribute name="VALUE" value="100A/100V/6.8mΩ"/>
</technology>
</technologies>
</device>
<device name="-AO3404A" package="SOT23-3">
<connects>
<connect gate="NMOS" pin="D" pad="3"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-12988"/>
<attribute name="VALUE" value="5.8A/30V/35mΩ"/>
</technology>
</technologies>
</device>
<device name="-FDS6630A" package="SO08">
<connects>
<connect gate="NMOS" pin="D" pad="5 6 7 8"/>
<connect gate="NMOS" pin="G" pad="4"/>
<connect gate="NMOS" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-08089"/>
<attribute name="VALUE" value="6.5A/30V/38mΩ"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X03">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X3">
<description>&lt;h3&gt;PTH - 3 Pin Vertical Molex Polarized Header&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/01932.pdf"&gt; 4UCONN part # 01932 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<text x="1.143" y="2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0.889" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="2.159" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Locking Footprint&lt;/h3&gt;
Pins are staggered 0.005" off center to lock pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Long Pad w/ Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to lock pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<text x="-1.27" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X3_LOCK">
<description>&lt;h3&gt;PTH - 3 Pin Vertical Molex Polarized Header&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/01932.pdf"&gt; 4UCONN part # 01932 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<text x="1.143" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0.889" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH Locking&lt;/h3&gt;
Holes are offset 0.007" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="2.032" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="1.905" y="-4.699" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-PTH">
<description>&lt;h3&gt;JST 3 Pin Right Angle Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins - 3 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-5MM-3">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -3 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="13.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="4.2" x2="13.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="13.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="13.1" y1="4" x2="13.7" y2="4" width="0.2032" layer="51"/>
<wire x1="13.7" y1="4" x2="13.7" y2="3" width="0.2032" layer="51"/>
<wire x1="13.7" y1="3" x2="13.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.413" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.413"/>
<pad name="3" x="10" y="0" drill="1.3" diameter="2.413"/>
<text x="3.683" y="2.794" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.429" y="1.905" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Locking Footprint w/out Silk Outline&lt;/h3&gt;
Holes are offset from center 0.005" to lock pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-SMD">
<description>&lt;h3&gt;JST 3 Pin Right Angle SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.99" y1="-2.07" x2="-4.99" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.99" y1="-5.57" x2="-4.19" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-5.57" x2="-4.19" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-3.07" x2="-2.99" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="-3.07" x2="4.21" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-3.07" x2="4.21" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-5.57" x2="5.01" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="5.01" y1="-5.57" x2="5.01" y2="-2.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="1.93" x2="-2.99" y2="1.93" width="0.2032" layer="21"/>
<smd name="1" x="-1.99" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="3" x="2.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-4.39" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="4.41" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<text x="-1.397" y="0.635" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03-1MM-RA">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin SMD&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1" y1="-4.6" x2="1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="1.75" y1="-0.35" x2="2.4997" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.4997" y1="-0.35" x2="2.4997" y2="-2" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-0.35" x2="-1.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_FEMALE">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Female Header&lt;/h3&gt;
Silk outline of pin location
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.935" y1="4.25" x2="-3.935" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="4.25" x2="-3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="-4.25" x2="3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-3.935" y1="-4.25" x2="3.935" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-0.32" y1="6.8" x2="0.32" y2="7.65" layer="51"/>
<rectangle x1="2.22" y1="6.8" x2="2.86" y2="7.65" layer="51"/>
<rectangle x1="-2.86" y1="6.8" x2="-2.22" y2="7.65" layer="51"/>
<smd name="3" x="2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="0" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-1.524" y="0.889" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE">
<description>&lt;h3&gt;SMD- 3 Pin Right Angle Male Headers&lt;/h3&gt;
No silk outline, but tDocu layer shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<text x="-1.524" y="0.254" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE_POST">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Male Header w/ Alignment Posts&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt; 4UCONN part # 11026 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.6"/>
<hole x="1.27" y="0" drill="1.6"/>
<text x="-1.397" y="1.524" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-PTH-VERT">
<description>&lt;h3&gt;JST 3 Pin Vertical Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-2.25" x2="-3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="2.25" x2="3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="2.25" x2="3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.75" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.75" size="1.27" layer="51">-</text>
<text x="1.7" y="0.95" size="0.8" layer="51">S</text>
<text x="-1.397" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLER">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Male Header w/ Alignment Posts&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt; 4UCONN part # 11026 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3589"/>
<hole x="1.27" y="0" drill="1.3589"/>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLEST">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3462"/>
<hole x="1.27" y="0" drill="1.3462"/>
</package>
<package name="JST-3-PTH-NS">
<description>&lt;h3&gt;JST 3 Pin Right Angle Plated Through Hole &amp;ndash; NO SILK&lt;/h3&gt;
&lt;p&gt;No silkscreen outline. tDoc layer (51) indicates connector footprint.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH &amp;ndash; NO SILK&lt;/h3&gt;
&lt;p&gt;No silkscreen outline. tDoc layer (51) indicates connector footprint.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="2.159" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X3">
<description>&lt;h3&gt;Plated Through Hole - 2x3&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.605" x2="-0.635" y2="-1.605" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<wire x1="-0.635" y1="-1.605" x2="0.635" y2="-1.605" width="0.2032" layer="22"/>
</package>
<package name="2X3-NS">
<description>&lt;h3&gt;Plated Through Hole - 2x3 No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.2032" layer="51"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.2032" layer="51"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-2.875" x2="-3.175" y2="-2.875" width="0.2032" layer="51"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" diameter="1.8796"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<text x="-3.81" y="2.667" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-3.81" y="-3.937" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X3_OFFSET">
<description>&lt;h3&gt;Plated Through Hole - 2x3 Long Pads w/ Offset Holes&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-4.399" x2="-3.175" y2="-4.399" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R270"/>
<pad name="2" x="-2.54" y="1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<pad name="3" x="0" y="-1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R270"/>
<pad name="4" x="0" y="1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<pad name="5" x="2.54" y="-1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R270"/>
<pad name="6" x="2.54" y="1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<text x="-4.064" y="-1.27" size="0.6096" layer="25" font="vector" ratio="20" rot="R90">&gt;NAME</text>
<text x="4.699" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20" rot="R90">&gt;VALUE</text>
<wire x1="-3.175" y1="-4.399" x2="-1.905" y2="-4.399" width="0.2032" layer="22"/>
</package>
<package name="2X3_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 2x3 Locking Footprint&lt;/h3&gt;
Holes are offset from center 0.005", to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.605" x2="-0.635" y2="-1.605" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.667" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.413" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.667" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<text x="-1.27" y="3.937" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.635" y1="-1.605" x2="0.635" y2="-1.605" width="0.2032" layer="22"/>
</package>
<package name="2X3_TEST_POINTS">
<description>&lt;h3&gt;Plated Through Hole - 2x3 Test Point Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-2.54" y="-1.27" drill="0.508" stop="no"/>
<pad name="2" x="-2.54" y="1.27" drill="0.508" stop="no"/>
<pad name="3" x="0" y="-1.27" drill="0.508" stop="no"/>
<pad name="4" x="0" y="1.27" drill="0.508" stop="no"/>
<pad name="5" x="2.54" y="-1.27" drill="0.508" stop="no"/>
<pad name="6" x="2.54" y="1.27" drill="0.508" stop="no"/>
<wire x1="-2.8956" y1="-2.0574" x2="-2.2098" y2="-2.0574" width="0.2032" layer="21"/>
<circle x="-2.54" y="1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="0" y="1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="2.54" y="1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="-2.54" y="-1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="0" y="-1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="2.54" y="-1.27" radius="0.61065625" width="0" layer="29"/>
<text x="-3.048" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-3.048" y="-3.048" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.1844" y1="-2.0574" x2="-2.8702" y2="-2.0574" width="0.2032" layer="22"/>
</package>
<package name="2X4">
<description>&lt;h3&gt;Plated Through Hole - 2x4&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:8&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.2032" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.2032" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.8796"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<wire x1="-4.445" y1="-2.794" x2="-3.175" y2="-2.794" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.794" x2="-4.445" y2="-2.794" width="0.2032" layer="22"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.2032" layer="21"/>
<text x="-5.08" y="2.794" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.683" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_03">
<description>&lt;h3&gt;3 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="AVR_SPI_PROGRAMMER_6">
<description>&lt;h3&gt;AVR ISP 6 Pin&lt;/h3&gt;
&lt;p&gt;This is the reduced ISP connector for AVR programming. Common on Arduino.</description>
<wire x1="-5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="-5.08" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-5.08" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="8.001" y="0.254" size="0.8128" layer="94" font="vector">MOSI</text>
<text x="-5.08" y="-2.286" size="0.8128" layer="94" font="vector" align="bottom-right">RST</text>
<text x="-5.207" y="0.254" size="0.8128" layer="94" font="vector" align="bottom-right">SCK</text>
<text x="-5.207" y="2.794" size="0.8128" layer="94" font="vector" align="bottom-right">MISO</text>
<text x="8.001" y="2.794" size="0.8128" layer="94" font="vector">VCC</text>
<text x="8.001" y="-2.286" size="0.8128" layer="94" font="vector">GND</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" direction="pas" function="dot"/>
<pin name="2" x="10.16" y="2.54" visible="pad" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="pad" direction="pas" function="dot"/>
<pin name="4" x="10.16" y="0" visible="pad" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" direction="pas" function="dot"/>
<pin name="6" x="10.16" y="-2.54" visible="pad" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="CONN_04X2">
<description>&lt;h3&gt;8 Pin Connection&lt;/h3&gt;
4x2 pin layout</description>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-4.064" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_03" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13875"&gt; Stackable Header - 3 Pin (Female, 0.1")&lt;/a&gt; (PRT-13875)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="J$1" symbol="CONN_03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08625" constant="no"/>
<attribute name="SF_ID" value="PRT-08096" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
<attribute name="SF_ID" value="PRT-08235" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08625" constant="no"/>
<attribute name="SF_ID" value="PRT-08096" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
<attribute name="SF_ID" value="PRT-08235" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="WIRE-10037" constant="no"/>
<attribute name="SF_ID" value="PRT-09915" constant="no"/>
</technology>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X03_PP_HOLES_ONLY">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SCREW-5MM" package="SCREWTERMINAL-5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10134" constant="no"/>
<attribute name="SF_SKU" value="PRT-08433" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X03_LOCK_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-SMD" package="JST-3-SMD">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12591" constant="no"/>
<attribute name="VALUE" value="3-PIN SMD" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X03-1MM-RA">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X03_SMD_RA_FEMALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10926"/>
<attribute name="VALUE" value="1x3 RA Female .1&quot;"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE" package="1X03_SMD_RA_MALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10925"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE_POST" package="1X03_SMD_RA_MALE_POST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-3-PTH-VERT">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13230" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLER" package="1X03_SMD_RA_MALE_POST_SMALLER">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11912" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLEST" package="1X03_SMD_RA_MALE_POST_SMALLEST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-NS" package="JST-3-PTH-NS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW-NS" package="SCREWTERMINAL-3.5MM-3-NS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVR_SPI_PROG_3X2" prefix="J">
<description>&lt;h3&gt;AVR ISP 6 Pin&lt;/h3&gt;
&lt;p&gt;This is the reduced ISP connector for AVR programming. Common on Arduino. This footprint will take up less PCB space and can be used with a 10-pin to 6-pin adapter such as SKU: BOB-08508&lt;/p&gt;

&lt;p&gt;&lt;b&gt;**Special note about "TEST_POINT" package.&lt;/b&gt; The stop mask is on the top side, so if you want your programming test points to be on the bottom of your board, make sure to place this package on the bottom side of the board. This also ensure that the orientation to program from the bottom side will be correct.&lt;/p&gt;

&lt;p&gt;SparkFun Products:
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11591"&gt;SparkFun ISP Pogo Adapter&lt;/a&gt; - NS&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11801"&gt;Tiny AVR Programmer&lt;/a&gt; -PTH&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="AVR_SPI_PROGRAMMER_6" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="2X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NS" package="2X3-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="OFFSET_PADS" package="2X3_OFFSET">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2X3_LOCK" package="2X3_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TESTPOINTS" package="2X3_TEST_POINTS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_04X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_04X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="aestethics">
<packages>
<package name="MUTEX-LOGO">
<rectangle x1="5.9182" y1="0.9906" x2="6.0706" y2="1.0414" layer="37"/>
<rectangle x1="5.8674" y1="0.9906" x2="5.9182" y2="1.0414" layer="37"/>
<rectangle x1="6.0706" y1="0.9906" x2="6.1214" y2="1.0414" layer="37"/>
<rectangle x1="5.8166" y1="1.0414" x2="6.1722" y2="1.0922" layer="37"/>
<rectangle x1="8.509" y1="1.0414" x2="9.3218" y2="1.0922" layer="37"/>
<rectangle x1="3.2766" y1="1.0414" x2="3.3274" y2="1.0922" layer="37"/>
<rectangle x1="4.0894" y1="1.0414" x2="4.1402" y2="1.0922" layer="37"/>
<rectangle x1="4.9022" y1="1.0414" x2="4.953" y2="1.0922" layer="37"/>
<rectangle x1="5.7658" y1="1.0414" x2="5.8166" y2="1.0922" layer="37"/>
<rectangle x1="6.1722" y1="1.0414" x2="6.223" y2="1.0922" layer="37"/>
<rectangle x1="7.493" y1="1.0414" x2="7.5438" y2="1.0922" layer="37"/>
<rectangle x1="9.6774" y1="1.0414" x2="9.7282" y2="1.0922" layer="37"/>
<rectangle x1="10.8458" y1="1.0414" x2="10.8966" y2="1.0922" layer="37"/>
<rectangle x1="10.8966" y1="1.0414" x2="10.9474" y2="1.0922" layer="37"/>
<rectangle x1="5.715" y1="1.0414" x2="5.7658" y2="1.0922" layer="37"/>
<rectangle x1="6.223" y1="1.0414" x2="6.2738" y2="1.0922" layer="37"/>
<rectangle x1="8.4582" y1="1.0414" x2="8.509" y2="1.0922" layer="37"/>
<rectangle x1="4.0386" y1="1.0414" x2="4.0894" y2="1.0922" layer="37"/>
<rectangle x1="4.8514" y1="1.0414" x2="4.9022" y2="1.0922" layer="37"/>
<rectangle x1="9.3218" y1="1.0414" x2="9.3726" y2="1.0922" layer="37"/>
<rectangle x1="4.1402" y1="1.0414" x2="4.191" y2="1.0922" layer="37"/>
<rectangle x1="3.3274" y1="1.0414" x2="3.3782" y2="1.0922" layer="37"/>
<rectangle x1="9.7282" y1="1.0414" x2="9.779" y2="1.0922" layer="37"/>
<rectangle x1="7.5438" y1="1.0414" x2="7.5946" y2="1.0922" layer="37"/>
<rectangle x1="3.2766" y1="1.0922" x2="3.3274" y2="1.143" layer="37"/>
<rectangle x1="4.0386" y1="1.0922" x2="4.1402" y2="1.143" layer="37"/>
<rectangle x1="4.9022" y1="1.0922" x2="4.953" y2="1.143" layer="37"/>
<rectangle x1="5.6642" y1="1.0922" x2="5.7658" y2="1.143" layer="37"/>
<rectangle x1="6.1722" y1="1.0922" x2="6.3246" y2="1.143" layer="37"/>
<rectangle x1="7.493" y1="1.0922" x2="7.5438" y2="1.143" layer="37"/>
<rectangle x1="8.4582" y1="1.0922" x2="8.5598" y2="1.143" layer="37"/>
<rectangle x1="9.7282" y1="1.0922" x2="9.779" y2="1.143" layer="37"/>
<rectangle x1="10.8458" y1="1.0922" x2="10.8966" y2="1.143" layer="37"/>
<rectangle x1="4.1402" y1="1.0922" x2="4.191" y2="1.143" layer="37"/>
<rectangle x1="5.7658" y1="1.0922" x2="5.8166" y2="1.143" layer="37"/>
<rectangle x1="8.5598" y1="1.0922" x2="8.6106" y2="1.143" layer="37"/>
<rectangle x1="9.6774" y1="1.0922" x2="9.7282" y2="1.143" layer="37"/>
<rectangle x1="8.6106" y1="1.0922" x2="9.3218" y2="1.143" layer="37"/>
<rectangle x1="6.1214" y1="1.0922" x2="6.1722" y2="1.143" layer="37"/>
<rectangle x1="5.8166" y1="1.0922" x2="5.8674" y2="1.143" layer="37"/>
<rectangle x1="5.8674" y1="1.0922" x2="5.9182" y2="1.143" layer="37"/>
<rectangle x1="6.0706" y1="1.0922" x2="6.1214" y2="1.143" layer="37"/>
<rectangle x1="4.8514" y1="1.0922" x2="4.9022" y2="1.143" layer="37"/>
<rectangle x1="3.3274" y1="1.0922" x2="3.3782" y2="1.143" layer="37"/>
<rectangle x1="10.8966" y1="1.0922" x2="10.9474" y2="1.143" layer="37"/>
<rectangle x1="10.795" y1="1.0922" x2="10.8458" y2="1.143" layer="37"/>
<rectangle x1="9.3218" y1="1.0922" x2="9.3726" y2="1.143" layer="37"/>
<rectangle x1="5.6134" y1="1.0922" x2="5.6642" y2="1.143" layer="37"/>
<rectangle x1="7.5438" y1="1.0922" x2="7.5946" y2="1.143" layer="37"/>
<rectangle x1="6.3246" y1="1.0922" x2="6.3754" y2="1.143" layer="37"/>
<rectangle x1="3.2766" y1="1.143" x2="3.3274" y2="1.1938" layer="37"/>
<rectangle x1="4.0386" y1="1.143" x2="4.191" y2="1.1938" layer="37"/>
<rectangle x1="4.9022" y1="1.143" x2="4.953" y2="1.1938" layer="37"/>
<rectangle x1="5.6134" y1="1.143" x2="5.6642" y2="1.1938" layer="37"/>
<rectangle x1="6.2738" y1="1.143" x2="6.3754" y2="1.1938" layer="37"/>
<rectangle x1="7.493" y1="1.143" x2="7.5438" y2="1.1938" layer="37"/>
<rectangle x1="8.4582" y1="1.143" x2="8.5598" y2="1.1938" layer="37"/>
<rectangle x1="9.7282" y1="1.143" x2="9.8298" y2="1.1938" layer="37"/>
<rectangle x1="10.795" y1="1.143" x2="10.8458" y2="1.1938" layer="37"/>
<rectangle x1="5.6642" y1="1.143" x2="5.715" y2="1.1938" layer="37"/>
<rectangle x1="10.8458" y1="1.143" x2="10.8966" y2="1.1938" layer="37"/>
<rectangle x1="4.8514" y1="1.143" x2="4.9022" y2="1.1938" layer="37"/>
<rectangle x1="5.5626" y1="1.143" x2="5.6134" y2="1.1938" layer="37"/>
<rectangle x1="6.223" y1="1.143" x2="6.2738" y2="1.1938" layer="37"/>
<rectangle x1="3.3274" y1="1.143" x2="3.3782" y2="1.1938" layer="37"/>
<rectangle x1="3.9878" y1="1.143" x2="4.0386" y2="1.1938" layer="37"/>
<rectangle x1="5.715" y1="1.143" x2="5.7658" y2="1.1938" layer="37"/>
<rectangle x1="6.3754" y1="1.143" x2="6.4262" y2="1.1938" layer="37"/>
<rectangle x1="4.191" y1="1.143" x2="4.2418" y2="1.1938" layer="37"/>
<rectangle x1="7.5438" y1="1.143" x2="7.5946" y2="1.1938" layer="37"/>
<rectangle x1="3.2766" y1="1.1938" x2="3.3274" y2="1.2446" layer="37"/>
<rectangle x1="3.9878" y1="1.1938" x2="4.0386" y2="1.2446" layer="37"/>
<rectangle x1="4.1402" y1="1.1938" x2="4.191" y2="1.2446" layer="37"/>
<rectangle x1="4.9022" y1="1.1938" x2="4.953" y2="1.2446" layer="37"/>
<rectangle x1="5.5626" y1="1.1938" x2="5.6134" y2="1.2446" layer="37"/>
<rectangle x1="6.3754" y1="1.1938" x2="6.4262" y2="1.2446" layer="37"/>
<rectangle x1="7.493" y1="1.1938" x2="7.5438" y2="1.2446" layer="37"/>
<rectangle x1="8.4582" y1="1.1938" x2="8.5598" y2="1.2446" layer="37"/>
<rectangle x1="9.779" y1="1.1938" x2="9.8298" y2="1.2446" layer="37"/>
<rectangle x1="10.7442" y1="1.1938" x2="10.8458" y2="1.2446" layer="37"/>
<rectangle x1="6.3246" y1="1.1938" x2="6.3754" y2="1.2446" layer="37"/>
<rectangle x1="4.0386" y1="1.1938" x2="4.0894" y2="1.2446" layer="37"/>
<rectangle x1="4.191" y1="1.1938" x2="4.2418" y2="1.2446" layer="37"/>
<rectangle x1="5.6134" y1="1.1938" x2="5.6642" y2="1.2446" layer="37"/>
<rectangle x1="4.8514" y1="1.1938" x2="4.9022" y2="1.2446" layer="37"/>
<rectangle x1="3.3274" y1="1.1938" x2="3.3782" y2="1.2446" layer="37"/>
<rectangle x1="9.7282" y1="1.1938" x2="9.779" y2="1.2446" layer="37"/>
<rectangle x1="9.8298" y1="1.1938" x2="9.8806" y2="1.2446" layer="37"/>
<rectangle x1="5.5118" y1="1.1938" x2="5.5626" y2="1.2446" layer="37"/>
<rectangle x1="6.4262" y1="1.1938" x2="6.477" y2="1.2446" layer="37"/>
<rectangle x1="7.5438" y1="1.1938" x2="7.5946" y2="1.2446" layer="37"/>
<rectangle x1="3.2766" y1="1.2446" x2="3.3274" y2="1.2954" layer="37"/>
<rectangle x1="3.9878" y1="1.2446" x2="4.0386" y2="1.2954" layer="37"/>
<rectangle x1="4.191" y1="1.2446" x2="4.2418" y2="1.2954" layer="37"/>
<rectangle x1="4.9022" y1="1.2446" x2="4.953" y2="1.2954" layer="37"/>
<rectangle x1="5.5118" y1="1.2446" x2="5.5626" y2="1.2954" layer="37"/>
<rectangle x1="6.3754" y1="1.2446" x2="6.477" y2="1.2954" layer="37"/>
<rectangle x1="7.493" y1="1.2446" x2="7.5438" y2="1.2954" layer="37"/>
<rectangle x1="8.4582" y1="1.2446" x2="8.5598" y2="1.2954" layer="37"/>
<rectangle x1="9.8298" y1="1.2446" x2="9.8806" y2="1.2954" layer="37"/>
<rectangle x1="10.7442" y1="1.2446" x2="10.795" y2="1.2954" layer="37"/>
<rectangle x1="5.5626" y1="1.2446" x2="5.6134" y2="1.2954" layer="37"/>
<rectangle x1="9.779" y1="1.2446" x2="9.8298" y2="1.2954" layer="37"/>
<rectangle x1="10.6934" y1="1.2446" x2="10.7442" y2="1.2954" layer="37"/>
<rectangle x1="4.8514" y1="1.2446" x2="4.9022" y2="1.2954" layer="37"/>
<rectangle x1="3.3274" y1="1.2446" x2="3.3782" y2="1.2954" layer="37"/>
<rectangle x1="4.1402" y1="1.2446" x2="4.191" y2="1.2954" layer="37"/>
<rectangle x1="3.937" y1="1.2446" x2="3.9878" y2="1.2954" layer="37"/>
<rectangle x1="4.0386" y1="1.2446" x2="4.0894" y2="1.2954" layer="37"/>
<rectangle x1="10.795" y1="1.2446" x2="10.8458" y2="1.2954" layer="37"/>
<rectangle x1="7.5438" y1="1.2446" x2="7.5946" y2="1.2954" layer="37"/>
<rectangle x1="3.2766" y1="1.2954" x2="3.3274" y2="1.3462" layer="37"/>
<rectangle x1="4.191" y1="1.2954" x2="4.2418" y2="1.3462" layer="37"/>
<rectangle x1="4.9022" y1="1.2954" x2="4.953" y2="1.3462" layer="37"/>
<rectangle x1="5.5118" y1="1.2954" x2="5.5626" y2="1.3462" layer="37"/>
<rectangle x1="6.4262" y1="1.2954" x2="6.477" y2="1.3462" layer="37"/>
<rectangle x1="7.493" y1="1.2954" x2="7.5438" y2="1.3462" layer="37"/>
<rectangle x1="8.4582" y1="1.2954" x2="8.5598" y2="1.3462" layer="37"/>
<rectangle x1="9.8298" y1="1.2954" x2="9.9314" y2="1.3462" layer="37"/>
<rectangle x1="10.6934" y1="1.2954" x2="10.7442" y2="1.3462" layer="37"/>
<rectangle x1="3.937" y1="1.2954" x2="4.0386" y2="1.3462" layer="37"/>
<rectangle x1="4.2418" y1="1.2954" x2="4.2926" y2="1.3462" layer="37"/>
<rectangle x1="5.461" y1="1.2954" x2="5.5118" y2="1.3462" layer="37"/>
<rectangle x1="10.7442" y1="1.2954" x2="10.795" y2="1.3462" layer="37"/>
<rectangle x1="4.8514" y1="1.2954" x2="4.9022" y2="1.3462" layer="37"/>
<rectangle x1="6.477" y1="1.2954" x2="6.5278" y2="1.3462" layer="37"/>
<rectangle x1="3.3274" y1="1.2954" x2="3.3782" y2="1.3462" layer="37"/>
<rectangle x1="7.5438" y1="1.2954" x2="7.5946" y2="1.3462" layer="37"/>
<rectangle x1="6.3754" y1="1.2954" x2="6.4262" y2="1.3462" layer="37"/>
<rectangle x1="3.2766" y1="1.3462" x2="3.3274" y2="1.397" layer="37"/>
<rectangle x1="3.937" y1="1.3462" x2="3.9878" y2="1.397" layer="37"/>
<rectangle x1="4.2418" y1="1.3462" x2="4.2926" y2="1.397" layer="37"/>
<rectangle x1="4.9022" y1="1.3462" x2="4.953" y2="1.397" layer="37"/>
<rectangle x1="5.461" y1="1.3462" x2="5.5118" y2="1.397" layer="37"/>
<rectangle x1="6.477" y1="1.3462" x2="6.5278" y2="1.397" layer="37"/>
<rectangle x1="7.493" y1="1.3462" x2="7.5438" y2="1.397" layer="37"/>
<rectangle x1="8.4582" y1="1.3462" x2="8.5598" y2="1.397" layer="37"/>
<rectangle x1="9.8806" y1="1.3462" x2="9.9314" y2="1.397" layer="37"/>
<rectangle x1="10.6426" y1="1.3462" x2="10.6934" y2="1.397" layer="37"/>
<rectangle x1="6.4262" y1="1.3462" x2="6.477" y2="1.397" layer="37"/>
<rectangle x1="10.6934" y1="1.3462" x2="10.7442" y2="1.397" layer="37"/>
<rectangle x1="5.5118" y1="1.3462" x2="5.5626" y2="1.397" layer="37"/>
<rectangle x1="9.9314" y1="1.3462" x2="9.9822" y2="1.397" layer="37"/>
<rectangle x1="4.8514" y1="1.3462" x2="4.9022" y2="1.397" layer="37"/>
<rectangle x1="3.3274" y1="1.3462" x2="3.3782" y2="1.397" layer="37"/>
<rectangle x1="3.9878" y1="1.3462" x2="4.0386" y2="1.397" layer="37"/>
<rectangle x1="4.191" y1="1.3462" x2="4.2418" y2="1.397" layer="37"/>
<rectangle x1="9.8298" y1="1.3462" x2="9.8806" y2="1.397" layer="37"/>
<rectangle x1="7.5438" y1="1.3462" x2="7.5946" y2="1.397" layer="37"/>
<rectangle x1="3.2766" y1="1.397" x2="3.3274" y2="1.4478" layer="37"/>
<rectangle x1="4.2418" y1="1.397" x2="4.2926" y2="1.4478" layer="37"/>
<rectangle x1="4.9022" y1="1.397" x2="4.953" y2="1.4478" layer="37"/>
<rectangle x1="5.461" y1="1.397" x2="5.5118" y2="1.4478" layer="37"/>
<rectangle x1="6.477" y1="1.397" x2="6.5278" y2="1.4478" layer="37"/>
<rectangle x1="7.493" y1="1.397" x2="7.5438" y2="1.4478" layer="37"/>
<rectangle x1="8.4582" y1="1.397" x2="8.5598" y2="1.4478" layer="37"/>
<rectangle x1="9.9314" y1="1.397" x2="9.9822" y2="1.4478" layer="37"/>
<rectangle x1="10.6426" y1="1.397" x2="10.6934" y2="1.4478" layer="37"/>
<rectangle x1="3.937" y1="1.397" x2="3.9878" y2="1.4478" layer="37"/>
<rectangle x1="3.8862" y1="1.397" x2="3.937" y2="1.4478" layer="37"/>
<rectangle x1="10.5918" y1="1.397" x2="10.6426" y2="1.4478" layer="37"/>
<rectangle x1="4.2926" y1="1.397" x2="4.3434" y2="1.4478" layer="37"/>
<rectangle x1="9.8806" y1="1.397" x2="9.9314" y2="1.4478" layer="37"/>
<rectangle x1="4.8514" y1="1.397" x2="4.9022" y2="1.4478" layer="37"/>
<rectangle x1="3.3274" y1="1.397" x2="3.3782" y2="1.4478" layer="37"/>
<rectangle x1="5.4102" y1="1.397" x2="5.461" y2="1.4478" layer="37"/>
<rectangle x1="7.5438" y1="1.397" x2="7.5946" y2="1.4478" layer="37"/>
<rectangle x1="6.4262" y1="1.397" x2="6.477" y2="1.4478" layer="37"/>
<rectangle x1="3.2766" y1="1.4478" x2="3.3274" y2="1.4986" layer="37"/>
<rectangle x1="3.8862" y1="1.4478" x2="3.937" y2="1.4986" layer="37"/>
<rectangle x1="4.2926" y1="1.4478" x2="4.3434" y2="1.4986" layer="37"/>
<rectangle x1="4.9022" y1="1.4478" x2="4.953" y2="1.4986" layer="37"/>
<rectangle x1="5.4102" y1="1.4478" x2="5.5118" y2="1.4986" layer="37"/>
<rectangle x1="6.477" y1="1.4478" x2="6.5278" y2="1.4986" layer="37"/>
<rectangle x1="7.493" y1="1.4478" x2="7.5438" y2="1.4986" layer="37"/>
<rectangle x1="8.4582" y1="1.4478" x2="8.5598" y2="1.4986" layer="37"/>
<rectangle x1="9.9822" y1="1.4478" x2="10.033" y2="1.4986" layer="37"/>
<rectangle x1="10.5918" y1="1.4478" x2="10.6426" y2="1.4986" layer="37"/>
<rectangle x1="9.9314" y1="1.4478" x2="9.9822" y2="1.4986" layer="37"/>
<rectangle x1="3.937" y1="1.4478" x2="3.9878" y2="1.4986" layer="37"/>
<rectangle x1="4.2418" y1="1.4478" x2="4.2926" y2="1.4986" layer="37"/>
<rectangle x1="6.5278" y1="1.4478" x2="6.5786" y2="1.4986" layer="37"/>
<rectangle x1="4.8514" y1="1.4478" x2="4.9022" y2="1.4986" layer="37"/>
<rectangle x1="10.6426" y1="1.4478" x2="10.6934" y2="1.4986" layer="37"/>
<rectangle x1="3.3274" y1="1.4478" x2="3.3782" y2="1.4986" layer="37"/>
<rectangle x1="10.541" y1="1.4478" x2="10.5918" y2="1.4986" layer="37"/>
<rectangle x1="7.5438" y1="1.4478" x2="7.5946" y2="1.4986" layer="37"/>
<rectangle x1="3.2766" y1="1.4986" x2="3.3274" y2="1.5494" layer="37"/>
<rectangle x1="3.8862" y1="1.4986" x2="3.937" y2="1.5494" layer="37"/>
<rectangle x1="4.2926" y1="1.4986" x2="4.3434" y2="1.5494" layer="37"/>
<rectangle x1="4.9022" y1="1.4986" x2="4.953" y2="1.5494" layer="37"/>
<rectangle x1="5.4102" y1="1.4986" x2="5.5118" y2="1.5494" layer="37"/>
<rectangle x1="6.477" y1="1.4986" x2="6.5786" y2="1.5494" layer="37"/>
<rectangle x1="7.493" y1="1.4986" x2="7.5438" y2="1.5494" layer="37"/>
<rectangle x1="8.4582" y1="1.4986" x2="8.5598" y2="1.5494" layer="37"/>
<rectangle x1="9.9822" y1="1.4986" x2="10.033" y2="1.5494" layer="37"/>
<rectangle x1="10.541" y1="1.4986" x2="10.5918" y2="1.5494" layer="37"/>
<rectangle x1="10.033" y1="1.4986" x2="10.0838" y2="1.5494" layer="37"/>
<rectangle x1="10.5918" y1="1.4986" x2="10.6426" y2="1.5494" layer="37"/>
<rectangle x1="3.8354" y1="1.4986" x2="3.8862" y2="1.5494" layer="37"/>
<rectangle x1="4.3434" y1="1.4986" x2="4.3942" y2="1.5494" layer="37"/>
<rectangle x1="4.8514" y1="1.4986" x2="4.9022" y2="1.5494" layer="37"/>
<rectangle x1="3.3274" y1="1.4986" x2="3.3782" y2="1.5494" layer="37"/>
<rectangle x1="7.5438" y1="1.4986" x2="7.5946" y2="1.5494" layer="37"/>
<rectangle x1="3.2766" y1="1.5494" x2="3.3274" y2="1.6002" layer="37"/>
<rectangle x1="3.8354" y1="1.5494" x2="3.8862" y2="1.6002" layer="37"/>
<rectangle x1="4.3434" y1="1.5494" x2="4.3942" y2="1.6002" layer="37"/>
<rectangle x1="4.9022" y1="1.5494" x2="4.953" y2="1.6002" layer="37"/>
<rectangle x1="5.4102" y1="1.5494" x2="5.461" y2="1.6002" layer="37"/>
<rectangle x1="6.477" y1="1.5494" x2="6.5786" y2="1.6002" layer="37"/>
<rectangle x1="7.493" y1="1.5494" x2="7.5438" y2="1.6002" layer="37"/>
<rectangle x1="8.4582" y1="1.5494" x2="8.5598" y2="1.6002" layer="37"/>
<rectangle x1="10.033" y1="1.5494" x2="10.0838" y2="1.6002" layer="37"/>
<rectangle x1="10.4902" y1="1.5494" x2="10.5918" y2="1.6002" layer="37"/>
<rectangle x1="3.8862" y1="1.5494" x2="3.937" y2="1.6002" layer="37"/>
<rectangle x1="4.2926" y1="1.5494" x2="4.3434" y2="1.6002" layer="37"/>
<rectangle x1="5.461" y1="1.5494" x2="5.5118" y2="1.6002" layer="37"/>
<rectangle x1="9.9822" y1="1.5494" x2="10.033" y2="1.6002" layer="37"/>
<rectangle x1="4.8514" y1="1.5494" x2="4.9022" y2="1.6002" layer="37"/>
<rectangle x1="3.3274" y1="1.5494" x2="3.3782" y2="1.6002" layer="37"/>
<rectangle x1="10.0838" y1="1.5494" x2="10.1346" y2="1.6002" layer="37"/>
<rectangle x1="7.5438" y1="1.5494" x2="7.5946" y2="1.6002" layer="37"/>
<rectangle x1="3.2766" y1="1.6002" x2="3.3274" y2="1.651" layer="37"/>
<rectangle x1="3.8354" y1="1.6002" x2="3.8862" y2="1.651" layer="37"/>
<rectangle x1="4.3434" y1="1.6002" x2="4.3942" y2="1.651" layer="37"/>
<rectangle x1="4.9022" y1="1.6002" x2="4.953" y2="1.651" layer="37"/>
<rectangle x1="5.4102" y1="1.6002" x2="5.461" y2="1.651" layer="37"/>
<rectangle x1="6.477" y1="1.6002" x2="6.5786" y2="1.651" layer="37"/>
<rectangle x1="7.493" y1="1.6002" x2="7.5438" y2="1.651" layer="37"/>
<rectangle x1="8.4582" y1="1.6002" x2="8.5598" y2="1.651" layer="37"/>
<rectangle x1="10.0838" y1="1.6002" x2="10.1346" y2="1.651" layer="37"/>
<rectangle x1="10.4902" y1="1.6002" x2="10.541" y2="1.651" layer="37"/>
<rectangle x1="10.033" y1="1.6002" x2="10.0838" y2="1.651" layer="37"/>
<rectangle x1="3.7846" y1="1.6002" x2="3.8354" y2="1.651" layer="37"/>
<rectangle x1="4.8514" y1="1.6002" x2="4.9022" y2="1.651" layer="37"/>
<rectangle x1="10.4394" y1="1.6002" x2="10.4902" y2="1.651" layer="37"/>
<rectangle x1="4.3942" y1="1.6002" x2="4.445" y2="1.651" layer="37"/>
<rectangle x1="3.3274" y1="1.6002" x2="3.3782" y2="1.651" layer="37"/>
<rectangle x1="5.461" y1="1.6002" x2="5.5118" y2="1.651" layer="37"/>
<rectangle x1="10.541" y1="1.6002" x2="10.5918" y2="1.651" layer="37"/>
<rectangle x1="7.5438" y1="1.6002" x2="7.5946" y2="1.651" layer="37"/>
<rectangle x1="3.2766" y1="1.651" x2="3.3274" y2="1.7018" layer="37"/>
<rectangle x1="3.7846" y1="1.651" x2="3.8862" y2="1.7018" layer="37"/>
<rectangle x1="4.3942" y1="1.651" x2="4.445" y2="1.7018" layer="37"/>
<rectangle x1="4.9022" y1="1.651" x2="4.953" y2="1.7018" layer="37"/>
<rectangle x1="5.4102" y1="1.651" x2="5.461" y2="1.7018" layer="37"/>
<rectangle x1="6.477" y1="1.651" x2="6.5786" y2="1.7018" layer="37"/>
<rectangle x1="7.493" y1="1.651" x2="7.5438" y2="1.7018" layer="37"/>
<rectangle x1="8.4582" y1="1.651" x2="8.5598" y2="1.7018" layer="37"/>
<rectangle x1="10.0838" y1="1.651" x2="10.1854" y2="1.7018" layer="37"/>
<rectangle x1="10.4394" y1="1.651" x2="10.4902" y2="1.7018" layer="37"/>
<rectangle x1="4.3434" y1="1.651" x2="4.3942" y2="1.7018" layer="37"/>
<rectangle x1="10.4902" y1="1.651" x2="10.541" y2="1.7018" layer="37"/>
<rectangle x1="4.8514" y1="1.651" x2="4.9022" y2="1.7018" layer="37"/>
<rectangle x1="3.3274" y1="1.651" x2="3.3782" y2="1.7018" layer="37"/>
<rectangle x1="5.461" y1="1.651" x2="5.5118" y2="1.7018" layer="37"/>
<rectangle x1="7.5438" y1="1.651" x2="7.5946" y2="1.7018" layer="37"/>
<rectangle x1="3.2766" y1="1.7018" x2="3.3274" y2="1.7526" layer="37"/>
<rectangle x1="3.7846" y1="1.7018" x2="3.8354" y2="1.7526" layer="37"/>
<rectangle x1="4.3942" y1="1.7018" x2="4.445" y2="1.7526" layer="37"/>
<rectangle x1="4.9022" y1="1.7018" x2="4.953" y2="1.7526" layer="37"/>
<rectangle x1="5.4102" y1="1.7018" x2="5.461" y2="1.7526" layer="37"/>
<rectangle x1="6.477" y1="1.7018" x2="6.5786" y2="1.7526" layer="37"/>
<rectangle x1="7.493" y1="1.7018" x2="7.5438" y2="1.7526" layer="37"/>
<rectangle x1="8.4582" y1="1.7018" x2="8.5598" y2="1.7526" layer="37"/>
<rectangle x1="10.1346" y1="1.7018" x2="10.1854" y2="1.7526" layer="37"/>
<rectangle x1="10.3886" y1="1.7018" x2="10.4902" y2="1.7526" layer="37"/>
<rectangle x1="4.8514" y1="1.7018" x2="4.9022" y2="1.7526" layer="37"/>
<rectangle x1="10.1854" y1="1.7018" x2="10.2362" y2="1.7526" layer="37"/>
<rectangle x1="10.0838" y1="1.7018" x2="10.1346" y2="1.7526" layer="37"/>
<rectangle x1="3.3274" y1="1.7018" x2="3.3782" y2="1.7526" layer="37"/>
<rectangle x1="3.7338" y1="1.7018" x2="3.7846" y2="1.7526" layer="37"/>
<rectangle x1="5.461" y1="1.7018" x2="5.5118" y2="1.7526" layer="37"/>
<rectangle x1="4.445" y1="1.7018" x2="4.4958" y2="1.7526" layer="37"/>
<rectangle x1="7.5438" y1="1.7018" x2="7.5946" y2="1.7526" layer="37"/>
<rectangle x1="3.2766" y1="1.7526" x2="3.3274" y2="1.8034" layer="37"/>
<rectangle x1="3.7846" y1="1.7526" x2="3.8354" y2="1.8034" layer="37"/>
<rectangle x1="4.3942" y1="1.7526" x2="4.4958" y2="1.8034" layer="37"/>
<rectangle x1="4.9022" y1="1.7526" x2="4.953" y2="1.8034" layer="37"/>
<rectangle x1="5.4102" y1="1.7526" x2="5.461" y2="1.8034" layer="37"/>
<rectangle x1="6.477" y1="1.7526" x2="6.5786" y2="1.8034" layer="37"/>
<rectangle x1="7.493" y1="1.7526" x2="7.5438" y2="1.8034" layer="37"/>
<rectangle x1="8.4582" y1="1.7526" x2="8.5598" y2="1.8034" layer="37"/>
<rectangle x1="10.1854" y1="1.7526" x2="10.2362" y2="1.8034" layer="37"/>
<rectangle x1="10.3886" y1="1.7526" x2="10.4394" y2="1.8034" layer="37"/>
<rectangle x1="3.7338" y1="1.7526" x2="3.7846" y2="1.8034" layer="37"/>
<rectangle x1="10.1346" y1="1.7526" x2="10.1854" y2="1.8034" layer="37"/>
<rectangle x1="10.3378" y1="1.7526" x2="10.3886" y2="1.8034" layer="37"/>
<rectangle x1="4.8514" y1="1.7526" x2="4.9022" y2="1.8034" layer="37"/>
<rectangle x1="3.3274" y1="1.7526" x2="3.3782" y2="1.8034" layer="37"/>
<rectangle x1="5.461" y1="1.7526" x2="5.5118" y2="1.8034" layer="37"/>
<rectangle x1="7.5438" y1="1.7526" x2="7.5946" y2="1.8034" layer="37"/>
<rectangle x1="10.4394" y1="1.7526" x2="10.4902" y2="1.8034" layer="37"/>
<rectangle x1="3.2766" y1="1.8034" x2="3.3274" y2="1.8542" layer="37"/>
<rectangle x1="3.7338" y1="1.8034" x2="3.7846" y2="1.8542" layer="37"/>
<rectangle x1="4.445" y1="1.8034" x2="4.4958" y2="1.8542" layer="37"/>
<rectangle x1="4.9022" y1="1.8034" x2="4.953" y2="1.8542" layer="37"/>
<rectangle x1="5.4102" y1="1.8034" x2="5.461" y2="1.8542" layer="37"/>
<rectangle x1="6.477" y1="1.8034" x2="6.5786" y2="1.8542" layer="37"/>
<rectangle x1="7.493" y1="1.8034" x2="7.5438" y2="1.8542" layer="37"/>
<rectangle x1="8.4582" y1="1.8034" x2="8.5598" y2="1.8542" layer="37"/>
<rectangle x1="10.1854" y1="1.8034" x2="10.2362" y2="1.8542" layer="37"/>
<rectangle x1="10.3378" y1="1.8034" x2="10.3886" y2="1.8542" layer="37"/>
<rectangle x1="10.2362" y1="1.8034" x2="10.287" y2="1.8542" layer="37"/>
<rectangle x1="10.287" y1="1.8034" x2="10.3378" y2="1.8542" layer="37"/>
<rectangle x1="10.3886" y1="1.8034" x2="10.4394" y2="1.8542" layer="37"/>
<rectangle x1="4.8514" y1="1.8034" x2="4.9022" y2="1.8542" layer="37"/>
<rectangle x1="3.3274" y1="1.8034" x2="3.3782" y2="1.8542" layer="37"/>
<rectangle x1="5.461" y1="1.8034" x2="5.5118" y2="1.8542" layer="37"/>
<rectangle x1="3.683" y1="1.8034" x2="3.7338" y2="1.8542" layer="37"/>
<rectangle x1="4.4958" y1="1.8034" x2="4.5466" y2="1.8542" layer="37"/>
<rectangle x1="7.5438" y1="1.8034" x2="7.5946" y2="1.8542" layer="37"/>
<rectangle x1="3.2766" y1="1.8542" x2="3.3274" y2="1.905" layer="37"/>
<rectangle x1="3.7338" y1="1.8542" x2="3.7846" y2="1.905" layer="37"/>
<rectangle x1="4.445" y1="1.8542" x2="4.5466" y2="1.905" layer="37"/>
<rectangle x1="4.9022" y1="1.8542" x2="4.953" y2="1.905" layer="37"/>
<rectangle x1="5.4102" y1="1.8542" x2="5.461" y2="1.905" layer="37"/>
<rectangle x1="6.477" y1="1.8542" x2="6.5786" y2="1.905" layer="37"/>
<rectangle x1="7.493" y1="1.8542" x2="7.5438" y2="1.905" layer="37"/>
<rectangle x1="8.4582" y1="1.8542" x2="8.5598" y2="1.905" layer="37"/>
<rectangle x1="10.2362" y1="1.8542" x2="10.3378" y2="1.905" layer="37"/>
<rectangle x1="3.683" y1="1.8542" x2="3.7338" y2="1.905" layer="37"/>
<rectangle x1="10.3378" y1="1.8542" x2="10.3886" y2="1.905" layer="37"/>
<rectangle x1="8.5598" y1="1.8542" x2="8.6106" y2="1.905" layer="37"/>
<rectangle x1="8.6106" y1="1.8542" x2="9.1694" y2="1.905" layer="37"/>
<rectangle x1="4.8514" y1="1.8542" x2="4.9022" y2="1.905" layer="37"/>
<rectangle x1="3.3274" y1="1.8542" x2="3.3782" y2="1.905" layer="37"/>
<rectangle x1="5.461" y1="1.8542" x2="5.5118" y2="1.905" layer="37"/>
<rectangle x1="10.1854" y1="1.8542" x2="10.2362" y2="1.905" layer="37"/>
<rectangle x1="7.5438" y1="1.8542" x2="7.5946" y2="1.905" layer="37"/>
<rectangle x1="3.2766" y1="1.905" x2="3.3274" y2="1.9558" layer="37"/>
<rectangle x1="3.683" y1="1.905" x2="3.7338" y2="1.9558" layer="37"/>
<rectangle x1="4.4958" y1="1.905" x2="4.5466" y2="1.9558" layer="37"/>
<rectangle x1="4.9022" y1="1.905" x2="4.953" y2="1.9558" layer="37"/>
<rectangle x1="5.4102" y1="1.905" x2="5.461" y2="1.9558" layer="37"/>
<rectangle x1="6.477" y1="1.905" x2="6.5786" y2="1.9558" layer="37"/>
<rectangle x1="7.493" y1="1.905" x2="7.5438" y2="1.9558" layer="37"/>
<rectangle x1="8.4582" y1="1.905" x2="9.1694" y2="1.9558" layer="37"/>
<rectangle x1="10.2362" y1="1.905" x2="10.3378" y2="1.9558" layer="37"/>
<rectangle x1="10.3378" y1="1.905" x2="10.3886" y2="1.9558" layer="37"/>
<rectangle x1="4.8514" y1="1.905" x2="4.9022" y2="1.9558" layer="37"/>
<rectangle x1="9.1694" y1="1.905" x2="9.2202" y2="1.9558" layer="37"/>
<rectangle x1="3.3274" y1="1.905" x2="3.3782" y2="1.9558" layer="37"/>
<rectangle x1="5.461" y1="1.905" x2="5.5118" y2="1.9558" layer="37"/>
<rectangle x1="3.6322" y1="1.905" x2="3.683" y2="1.9558" layer="37"/>
<rectangle x1="7.5438" y1="1.905" x2="7.5946" y2="1.9558" layer="37"/>
<rectangle x1="3.7338" y1="1.905" x2="3.7846" y2="1.9558" layer="37"/>
<rectangle x1="4.445" y1="1.905" x2="4.4958" y2="1.9558" layer="37"/>
<rectangle x1="4.5466" y1="1.905" x2="4.5974" y2="1.9558" layer="37"/>
<rectangle x1="3.2766" y1="1.9558" x2="3.3274" y2="2.0066" layer="37"/>
<rectangle x1="3.683" y1="1.9558" x2="3.7338" y2="2.0066" layer="37"/>
<rectangle x1="4.4958" y1="1.9558" x2="4.5466" y2="2.0066" layer="37"/>
<rectangle x1="4.9022" y1="1.9558" x2="4.953" y2="2.0066" layer="37"/>
<rectangle x1="5.4102" y1="1.9558" x2="5.461" y2="2.0066" layer="37"/>
<rectangle x1="6.477" y1="1.9558" x2="6.5786" y2="2.0066" layer="37"/>
<rectangle x1="7.493" y1="1.9558" x2="7.5438" y2="2.0066" layer="37"/>
<rectangle x1="8.4582" y1="1.9558" x2="8.5598" y2="2.0066" layer="37"/>
<rectangle x1="10.2362" y1="1.9558" x2="10.3886" y2="2.0066" layer="37"/>
<rectangle x1="3.6322" y1="1.9558" x2="3.683" y2="2.0066" layer="37"/>
<rectangle x1="4.5466" y1="1.9558" x2="4.5974" y2="2.0066" layer="37"/>
<rectangle x1="10.1854" y1="1.9558" x2="10.2362" y2="2.0066" layer="37"/>
<rectangle x1="4.8514" y1="1.9558" x2="4.9022" y2="2.0066" layer="37"/>
<rectangle x1="3.3274" y1="1.9558" x2="3.3782" y2="2.0066" layer="37"/>
<rectangle x1="5.461" y1="1.9558" x2="5.5118" y2="2.0066" layer="37"/>
<rectangle x1="7.5438" y1="1.9558" x2="7.5946" y2="2.0066" layer="37"/>
<rectangle x1="3.2766" y1="2.0066" x2="3.3274" y2="2.0574" layer="37"/>
<rectangle x1="3.6322" y1="2.0066" x2="3.683" y2="2.0574" layer="37"/>
<rectangle x1="4.5466" y1="2.0066" x2="4.5974" y2="2.0574" layer="37"/>
<rectangle x1="4.9022" y1="2.0066" x2="4.953" y2="2.0574" layer="37"/>
<rectangle x1="5.4102" y1="2.0066" x2="5.461" y2="2.0574" layer="37"/>
<rectangle x1="6.477" y1="2.0066" x2="6.5786" y2="2.0574" layer="37"/>
<rectangle x1="7.493" y1="2.0066" x2="7.5438" y2="2.0574" layer="37"/>
<rectangle x1="8.4582" y1="2.0066" x2="8.5598" y2="2.0574" layer="37"/>
<rectangle x1="10.3378" y1="2.0066" x2="10.3886" y2="2.0574" layer="37"/>
<rectangle x1="10.1854" y1="2.0066" x2="10.287" y2="2.0574" layer="37"/>
<rectangle x1="10.3886" y1="2.0066" x2="10.4394" y2="2.0574" layer="37"/>
<rectangle x1="4.8514" y1="2.0066" x2="4.9022" y2="2.0574" layer="37"/>
<rectangle x1="3.3274" y1="2.0066" x2="3.3782" y2="2.0574" layer="37"/>
<rectangle x1="5.461" y1="2.0066" x2="5.5118" y2="2.0574" layer="37"/>
<rectangle x1="3.683" y1="2.0066" x2="3.7338" y2="2.0574" layer="37"/>
<rectangle x1="4.4958" y1="2.0066" x2="4.5466" y2="2.0574" layer="37"/>
<rectangle x1="10.287" y1="2.0066" x2="10.3378" y2="2.0574" layer="37"/>
<rectangle x1="7.5438" y1="2.0066" x2="7.5946" y2="2.0574" layer="37"/>
<rectangle x1="3.2766" y1="2.0574" x2="3.3274" y2="2.1082" layer="37"/>
<rectangle x1="3.6322" y1="2.0574" x2="3.683" y2="2.1082" layer="37"/>
<rectangle x1="4.5466" y1="2.0574" x2="4.5974" y2="2.1082" layer="37"/>
<rectangle x1="4.9022" y1="2.0574" x2="4.953" y2="2.1082" layer="37"/>
<rectangle x1="5.4102" y1="2.0574" x2="5.461" y2="2.1082" layer="37"/>
<rectangle x1="6.477" y1="2.0574" x2="6.5786" y2="2.1082" layer="37"/>
<rectangle x1="7.493" y1="2.0574" x2="7.5438" y2="2.1082" layer="37"/>
<rectangle x1="8.4582" y1="2.0574" x2="8.5598" y2="2.1082" layer="37"/>
<rectangle x1="10.1854" y1="2.0574" x2="10.2362" y2="2.1082" layer="37"/>
<rectangle x1="10.3886" y1="2.0574" x2="10.4394" y2="2.1082" layer="37"/>
<rectangle x1="3.5814" y1="2.0574" x2="3.6322" y2="2.1082" layer="37"/>
<rectangle x1="4.5974" y1="2.0574" x2="4.6482" y2="2.1082" layer="37"/>
<rectangle x1="10.1346" y1="2.0574" x2="10.1854" y2="2.1082" layer="37"/>
<rectangle x1="10.4394" y1="2.0574" x2="10.4902" y2="2.1082" layer="37"/>
<rectangle x1="4.8514" y1="2.0574" x2="4.9022" y2="2.1082" layer="37"/>
<rectangle x1="3.3274" y1="2.0574" x2="3.3782" y2="2.1082" layer="37"/>
<rectangle x1="5.461" y1="2.0574" x2="5.5118" y2="2.1082" layer="37"/>
<rectangle x1="10.3378" y1="2.0574" x2="10.3886" y2="2.1082" layer="37"/>
<rectangle x1="7.5438" y1="2.0574" x2="7.5946" y2="2.1082" layer="37"/>
<rectangle x1="3.2766" y1="2.1082" x2="3.3274" y2="2.159" layer="37"/>
<rectangle x1="3.5814" y1="2.1082" x2="3.6322" y2="2.159" layer="37"/>
<rectangle x1="4.5974" y1="2.1082" x2="4.6482" y2="2.159" layer="37"/>
<rectangle x1="4.9022" y1="2.1082" x2="4.953" y2="2.159" layer="37"/>
<rectangle x1="5.4102" y1="2.1082" x2="5.461" y2="2.159" layer="37"/>
<rectangle x1="6.477" y1="2.1082" x2="6.5786" y2="2.159" layer="37"/>
<rectangle x1="7.493" y1="2.1082" x2="7.5438" y2="2.159" layer="37"/>
<rectangle x1="8.4582" y1="2.1082" x2="8.5598" y2="2.159" layer="37"/>
<rectangle x1="10.1346" y1="2.1082" x2="10.1854" y2="2.159" layer="37"/>
<rectangle x1="10.4394" y1="2.1082" x2="10.4902" y2="2.159" layer="37"/>
<rectangle x1="10.3886" y1="2.1082" x2="10.4394" y2="2.159" layer="37"/>
<rectangle x1="10.0838" y1="2.1082" x2="10.1346" y2="2.159" layer="37"/>
<rectangle x1="10.1854" y1="2.1082" x2="10.2362" y2="2.159" layer="37"/>
<rectangle x1="4.8514" y1="2.1082" x2="4.9022" y2="2.159" layer="37"/>
<rectangle x1="3.6322" y1="2.1082" x2="3.683" y2="2.159" layer="37"/>
<rectangle x1="3.3274" y1="2.1082" x2="3.3782" y2="2.159" layer="37"/>
<rectangle x1="4.5466" y1="2.1082" x2="4.5974" y2="2.159" layer="37"/>
<rectangle x1="5.461" y1="2.1082" x2="5.5118" y2="2.159" layer="37"/>
<rectangle x1="10.4902" y1="2.1082" x2="10.541" y2="2.159" layer="37"/>
<rectangle x1="7.5438" y1="2.1082" x2="7.5946" y2="2.159" layer="37"/>
<rectangle x1="3.2766" y1="2.159" x2="3.3274" y2="2.2098" layer="37"/>
<rectangle x1="3.5814" y1="2.159" x2="3.6322" y2="2.2098" layer="37"/>
<rectangle x1="4.5974" y1="2.159" x2="4.699" y2="2.2098" layer="37"/>
<rectangle x1="4.9022" y1="2.159" x2="4.953" y2="2.2098" layer="37"/>
<rectangle x1="5.4102" y1="2.159" x2="5.461" y2="2.2098" layer="37"/>
<rectangle x1="6.477" y1="2.159" x2="6.5786" y2="2.2098" layer="37"/>
<rectangle x1="7.493" y1="2.159" x2="7.5438" y2="2.2098" layer="37"/>
<rectangle x1="8.4582" y1="2.159" x2="8.5598" y2="2.2098" layer="37"/>
<rectangle x1="10.0838" y1="2.159" x2="10.1346" y2="2.2098" layer="37"/>
<rectangle x1="10.4902" y1="2.159" x2="10.541" y2="2.2098" layer="37"/>
<rectangle x1="10.1346" y1="2.159" x2="10.1854" y2="2.2098" layer="37"/>
<rectangle x1="10.4394" y1="2.159" x2="10.4902" y2="2.2098" layer="37"/>
<rectangle x1="3.5306" y1="2.159" x2="3.5814" y2="2.2098" layer="37"/>
<rectangle x1="4.8514" y1="2.159" x2="4.9022" y2="2.2098" layer="37"/>
<rectangle x1="3.3274" y1="2.159" x2="3.3782" y2="2.2098" layer="37"/>
<rectangle x1="5.461" y1="2.159" x2="5.5118" y2="2.2098" layer="37"/>
<rectangle x1="7.5438" y1="2.159" x2="7.5946" y2="2.2098" layer="37"/>
<rectangle x1="3.2766" y1="2.2098" x2="3.3274" y2="2.2606" layer="37"/>
<rectangle x1="3.5306" y1="2.2098" x2="3.5814" y2="2.2606" layer="37"/>
<rectangle x1="4.6482" y1="2.2098" x2="4.699" y2="2.2606" layer="37"/>
<rectangle x1="4.9022" y1="2.2098" x2="4.953" y2="2.2606" layer="37"/>
<rectangle x1="5.4102" y1="2.2098" x2="5.461" y2="2.2606" layer="37"/>
<rectangle x1="6.477" y1="2.2098" x2="6.5786" y2="2.2606" layer="37"/>
<rectangle x1="7.493" y1="2.2098" x2="7.5438" y2="2.2606" layer="37"/>
<rectangle x1="8.4582" y1="2.2098" x2="8.5598" y2="2.2606" layer="37"/>
<rectangle x1="10.0838" y1="2.2098" x2="10.1346" y2="2.2606" layer="37"/>
<rectangle x1="10.4902" y1="2.2098" x2="10.541" y2="2.2606" layer="37"/>
<rectangle x1="10.033" y1="2.2098" x2="10.0838" y2="2.2606" layer="37"/>
<rectangle x1="10.541" y1="2.2098" x2="10.5918" y2="2.2606" layer="37"/>
<rectangle x1="3.5814" y1="2.2098" x2="3.6322" y2="2.2606" layer="37"/>
<rectangle x1="4.5974" y1="2.2098" x2="4.6482" y2="2.2606" layer="37"/>
<rectangle x1="4.8514" y1="2.2098" x2="4.9022" y2="2.2606" layer="37"/>
<rectangle x1="3.3274" y1="2.2098" x2="3.3782" y2="2.2606" layer="37"/>
<rectangle x1="5.461" y1="2.2098" x2="5.5118" y2="2.2606" layer="37"/>
<rectangle x1="7.5438" y1="2.2098" x2="7.5946" y2="2.2606" layer="37"/>
<rectangle x1="3.2766" y1="2.2606" x2="3.3274" y2="2.3114" layer="37"/>
<rectangle x1="3.5306" y1="2.2606" x2="3.5814" y2="2.3114" layer="37"/>
<rectangle x1="4.6482" y1="2.2606" x2="4.699" y2="2.3114" layer="37"/>
<rectangle x1="4.9022" y1="2.2606" x2="4.953" y2="2.3114" layer="37"/>
<rectangle x1="5.4102" y1="2.2606" x2="5.461" y2="2.3114" layer="37"/>
<rectangle x1="6.477" y1="2.2606" x2="6.5786" y2="2.3114" layer="37"/>
<rectangle x1="7.493" y1="2.2606" x2="7.5438" y2="2.3114" layer="37"/>
<rectangle x1="8.4582" y1="2.2606" x2="8.5598" y2="2.3114" layer="37"/>
<rectangle x1="10.033" y1="2.2606" x2="10.0838" y2="2.3114" layer="37"/>
<rectangle x1="10.541" y1="2.2606" x2="10.5918" y2="2.3114" layer="37"/>
<rectangle x1="4.699" y1="2.2606" x2="4.7498" y2="2.3114" layer="37"/>
<rectangle x1="3.4798" y1="2.2606" x2="3.5306" y2="2.3114" layer="37"/>
<rectangle x1="9.9822" y1="2.2606" x2="10.033" y2="2.3114" layer="37"/>
<rectangle x1="10.4902" y1="2.2606" x2="10.541" y2="2.3114" layer="37"/>
<rectangle x1="4.8514" y1="2.2606" x2="4.9022" y2="2.3114" layer="37"/>
<rectangle x1="3.3274" y1="2.2606" x2="3.3782" y2="2.3114" layer="37"/>
<rectangle x1="5.461" y1="2.2606" x2="5.5118" y2="2.3114" layer="37"/>
<rectangle x1="10.0838" y1="2.2606" x2="10.1346" y2="2.3114" layer="37"/>
<rectangle x1="10.5918" y1="2.2606" x2="10.6426" y2="2.3114" layer="37"/>
<rectangle x1="7.5438" y1="2.2606" x2="7.5946" y2="2.3114" layer="37"/>
<rectangle x1="3.2766" y1="2.3114" x2="3.3274" y2="2.3622" layer="37"/>
<rectangle x1="3.4798" y1="2.3114" x2="3.5306" y2="2.3622" layer="37"/>
<rectangle x1="4.699" y1="2.3114" x2="4.7498" y2="2.3622" layer="37"/>
<rectangle x1="4.9022" y1="2.3114" x2="4.953" y2="2.3622" layer="37"/>
<rectangle x1="5.4102" y1="2.3114" x2="5.461" y2="2.3622" layer="37"/>
<rectangle x1="6.477" y1="2.3114" x2="6.5786" y2="2.3622" layer="37"/>
<rectangle x1="7.493" y1="2.3114" x2="7.5438" y2="2.3622" layer="37"/>
<rectangle x1="8.4582" y1="2.3114" x2="8.5598" y2="2.3622" layer="37"/>
<rectangle x1="9.9822" y1="2.3114" x2="10.033" y2="2.3622" layer="37"/>
<rectangle x1="10.5918" y1="2.3114" x2="10.6426" y2="2.3622" layer="37"/>
<rectangle x1="10.541" y1="2.3114" x2="10.5918" y2="2.3622" layer="37"/>
<rectangle x1="10.033" y1="2.3114" x2="10.0838" y2="2.3622" layer="37"/>
<rectangle x1="3.5306" y1="2.3114" x2="3.5814" y2="2.3622" layer="37"/>
<rectangle x1="4.6482" y1="2.3114" x2="4.699" y2="2.3622" layer="37"/>
<rectangle x1="4.8514" y1="2.3114" x2="4.9022" y2="2.3622" layer="37"/>
<rectangle x1="3.3274" y1="2.3114" x2="3.3782" y2="2.3622" layer="37"/>
<rectangle x1="5.461" y1="2.3114" x2="5.5118" y2="2.3622" layer="37"/>
<rectangle x1="7.5438" y1="2.3114" x2="7.5946" y2="2.3622" layer="37"/>
<rectangle x1="3.2766" y1="2.3622" x2="3.3274" y2="2.413" layer="37"/>
<rectangle x1="3.4798" y1="2.3622" x2="3.5306" y2="2.413" layer="37"/>
<rectangle x1="4.699" y1="2.3622" x2="4.7498" y2="2.413" layer="37"/>
<rectangle x1="4.9022" y1="2.3622" x2="4.953" y2="2.413" layer="37"/>
<rectangle x1="5.4102" y1="2.3622" x2="5.461" y2="2.413" layer="37"/>
<rectangle x1="6.477" y1="2.3622" x2="6.5786" y2="2.413" layer="37"/>
<rectangle x1="7.493" y1="2.3622" x2="7.5438" y2="2.413" layer="37"/>
<rectangle x1="8.4582" y1="2.3622" x2="8.5598" y2="2.413" layer="37"/>
<rectangle x1="9.9314" y1="2.3622" x2="10.033" y2="2.413" layer="37"/>
<rectangle x1="10.5918" y1="2.3622" x2="10.6426" y2="2.413" layer="37"/>
<rectangle x1="10.6426" y1="2.3622" x2="10.6934" y2="2.413" layer="37"/>
<rectangle x1="4.7498" y1="2.3622" x2="4.8006" y2="2.413" layer="37"/>
<rectangle x1="3.429" y1="2.3622" x2="3.4798" y2="2.413" layer="37"/>
<rectangle x1="4.8514" y1="2.3622" x2="4.9022" y2="2.413" layer="37"/>
<rectangle x1="3.3274" y1="2.3622" x2="3.3782" y2="2.413" layer="37"/>
<rectangle x1="5.461" y1="2.3622" x2="5.5118" y2="2.413" layer="37"/>
<rectangle x1="7.5438" y1="2.3622" x2="7.5946" y2="2.413" layer="37"/>
<rectangle x1="3.2766" y1="2.413" x2="3.3274" y2="2.4638" layer="37"/>
<rectangle x1="3.429" y1="2.413" x2="3.4798" y2="2.4638" layer="37"/>
<rectangle x1="4.7498" y1="2.413" x2="4.8006" y2="2.4638" layer="37"/>
<rectangle x1="4.9022" y1="2.413" x2="4.953" y2="2.4638" layer="37"/>
<rectangle x1="5.4102" y1="2.413" x2="5.461" y2="2.4638" layer="37"/>
<rectangle x1="6.477" y1="2.413" x2="6.5786" y2="2.4638" layer="37"/>
<rectangle x1="7.493" y1="2.413" x2="7.5438" y2="2.4638" layer="37"/>
<rectangle x1="8.4582" y1="2.413" x2="8.5598" y2="2.4638" layer="37"/>
<rectangle x1="9.9314" y1="2.413" x2="9.9822" y2="2.4638" layer="37"/>
<rectangle x1="10.6426" y1="2.413" x2="10.6934" y2="2.4638" layer="37"/>
<rectangle x1="3.4798" y1="2.413" x2="3.5306" y2="2.4638" layer="37"/>
<rectangle x1="4.699" y1="2.413" x2="4.7498" y2="2.4638" layer="37"/>
<rectangle x1="4.8514" y1="2.413" x2="4.9022" y2="2.4638" layer="37"/>
<rectangle x1="3.3274" y1="2.413" x2="3.3782" y2="2.4638" layer="37"/>
<rectangle x1="9.8806" y1="2.413" x2="9.9314" y2="2.4638" layer="37"/>
<rectangle x1="5.461" y1="2.413" x2="5.5118" y2="2.4638" layer="37"/>
<rectangle x1="10.5918" y1="2.413" x2="10.6426" y2="2.4638" layer="37"/>
<rectangle x1="10.6934" y1="2.413" x2="10.7442" y2="2.4638" layer="37"/>
<rectangle x1="3.3782" y1="2.413" x2="3.429" y2="2.4638" layer="37"/>
<rectangle x1="9.9822" y1="2.413" x2="10.033" y2="2.4638" layer="37"/>
<rectangle x1="7.5438" y1="2.413" x2="7.5946" y2="2.4638" layer="37"/>
<rectangle x1="3.2766" y1="2.4638" x2="3.4798" y2="2.5146" layer="37"/>
<rectangle x1="4.7498" y1="2.4638" x2="4.953" y2="2.5146" layer="37"/>
<rectangle x1="5.4102" y1="2.4638" x2="5.461" y2="2.5146" layer="37"/>
<rectangle x1="6.477" y1="2.4638" x2="6.5786" y2="2.5146" layer="37"/>
<rectangle x1="7.493" y1="2.4638" x2="7.5438" y2="2.5146" layer="37"/>
<rectangle x1="8.4582" y1="2.4638" x2="8.5598" y2="2.5146" layer="37"/>
<rectangle x1="9.8806" y1="2.4638" x2="9.9314" y2="2.5146" layer="37"/>
<rectangle x1="10.6934" y1="2.4638" x2="10.7442" y2="2.5146" layer="37"/>
<rectangle x1="10.6426" y1="2.4638" x2="10.6934" y2="2.5146" layer="37"/>
<rectangle x1="9.9314" y1="2.4638" x2="9.9822" y2="2.5146" layer="37"/>
<rectangle x1="5.461" y1="2.4638" x2="5.5118" y2="2.5146" layer="37"/>
<rectangle x1="7.5438" y1="2.4638" x2="7.5946" y2="2.5146" layer="37"/>
<rectangle x1="9.8298" y1="2.4638" x2="9.8806" y2="2.5146" layer="37"/>
<rectangle x1="3.2766" y1="2.5146" x2="3.429" y2="2.5654" layer="37"/>
<rectangle x1="4.8006" y1="2.5146" x2="4.953" y2="2.5654" layer="37"/>
<rectangle x1="5.4102" y1="2.5146" x2="5.461" y2="2.5654" layer="37"/>
<rectangle x1="6.477" y1="2.5146" x2="6.5786" y2="2.5654" layer="37"/>
<rectangle x1="7.493" y1="2.5146" x2="7.5438" y2="2.5654" layer="37"/>
<rectangle x1="8.4582" y1="2.5146" x2="8.5598" y2="2.5654" layer="37"/>
<rectangle x1="9.8298" y1="2.5146" x2="9.9314" y2="2.5654" layer="37"/>
<rectangle x1="10.6934" y1="2.5146" x2="10.7442" y2="2.5654" layer="37"/>
<rectangle x1="3.429" y1="2.5146" x2="3.4798" y2="2.5654" layer="37"/>
<rectangle x1="4.7498" y1="2.5146" x2="4.8006" y2="2.5654" layer="37"/>
<rectangle x1="10.7442" y1="2.5146" x2="10.795" y2="2.5654" layer="37"/>
<rectangle x1="5.461" y1="2.5146" x2="5.5118" y2="2.5654" layer="37"/>
<rectangle x1="7.5438" y1="2.5146" x2="7.5946" y2="2.5654" layer="37"/>
<rectangle x1="3.2766" y1="2.5654" x2="3.429" y2="2.6162" layer="37"/>
<rectangle x1="4.8006" y1="2.5654" x2="4.953" y2="2.6162" layer="37"/>
<rectangle x1="5.4102" y1="2.5654" x2="5.461" y2="2.6162" layer="37"/>
<rectangle x1="6.477" y1="2.5654" x2="6.5786" y2="2.6162" layer="37"/>
<rectangle x1="7.493" y1="2.5654" x2="7.5438" y2="2.6162" layer="37"/>
<rectangle x1="8.4582" y1="2.5654" x2="8.5598" y2="2.6162" layer="37"/>
<rectangle x1="9.8298" y1="2.5654" x2="9.8806" y2="2.6162" layer="37"/>
<rectangle x1="10.7442" y1="2.5654" x2="10.795" y2="2.6162" layer="37"/>
<rectangle x1="9.779" y1="2.5654" x2="9.8298" y2="2.6162" layer="37"/>
<rectangle x1="10.795" y1="2.5654" x2="10.8458" y2="2.6162" layer="37"/>
<rectangle x1="5.461" y1="2.5654" x2="5.5118" y2="2.6162" layer="37"/>
<rectangle x1="10.6934" y1="2.5654" x2="10.7442" y2="2.6162" layer="37"/>
<rectangle x1="7.5438" y1="2.5654" x2="7.5946" y2="2.6162" layer="37"/>
<rectangle x1="4.7498" y1="2.5654" x2="4.8006" y2="2.6162" layer="37"/>
<rectangle x1="3.2766" y1="2.6162" x2="3.3782" y2="2.667" layer="37"/>
<rectangle x1="4.8514" y1="2.6162" x2="4.953" y2="2.667" layer="37"/>
<rectangle x1="5.4102" y1="2.6162" x2="5.461" y2="2.667" layer="37"/>
<rectangle x1="6.477" y1="2.6162" x2="6.5786" y2="2.667" layer="37"/>
<rectangle x1="7.493" y1="2.6162" x2="7.5438" y2="2.667" layer="37"/>
<rectangle x1="8.4582" y1="2.6162" x2="8.5598" y2="2.667" layer="37"/>
<rectangle x1="9.779" y1="2.6162" x2="9.8298" y2="2.667" layer="37"/>
<rectangle x1="10.795" y1="2.6162" x2="10.8458" y2="2.667" layer="37"/>
<rectangle x1="3.3782" y1="2.6162" x2="3.429" y2="2.667" layer="37"/>
<rectangle x1="4.8006" y1="2.6162" x2="4.8514" y2="2.667" layer="37"/>
<rectangle x1="10.7442" y1="2.6162" x2="10.795" y2="2.667" layer="37"/>
<rectangle x1="9.8298" y1="2.6162" x2="9.8806" y2="2.667" layer="37"/>
<rectangle x1="5.461" y1="2.6162" x2="5.5118" y2="2.667" layer="37"/>
<rectangle x1="7.5438" y1="2.6162" x2="7.5946" y2="2.667" layer="37"/>
<rectangle x1="9.7282" y1="2.6162" x2="9.779" y2="2.667" layer="37"/>
<rectangle x1="3.2766" y1="2.667" x2="3.3782" y2="2.7178" layer="37"/>
<rectangle x1="4.8514" y1="2.667" x2="4.953" y2="2.7178" layer="37"/>
<rectangle x1="5.4102" y1="2.667" x2="5.461" y2="2.7178" layer="37"/>
<rectangle x1="6.477" y1="2.667" x2="6.5786" y2="2.7178" layer="37"/>
<rectangle x1="7.493" y1="2.667" x2="7.5438" y2="2.7178" layer="37"/>
<rectangle x1="8.4582" y1="2.667" x2="8.5598" y2="2.7178" layer="37"/>
<rectangle x1="9.7282" y1="2.667" x2="9.8298" y2="2.7178" layer="37"/>
<rectangle x1="10.795" y1="2.667" x2="10.8966" y2="2.7178" layer="37"/>
<rectangle x1="7.5438" y1="2.667" x2="7.5946" y2="2.7178" layer="37"/>
<rectangle x1="7.4422" y1="2.667" x2="7.493" y2="2.7178" layer="37"/>
<rectangle x1="8.5598" y1="2.667" x2="8.6106" y2="2.7178" layer="37"/>
<rectangle x1="7.5946" y1="2.667" x2="7.6454" y2="2.7178" layer="37"/>
<rectangle x1="5.461" y1="2.667" x2="5.5118" y2="2.7178" layer="37"/>
<rectangle x1="6.985" y1="2.667" x2="7.4422" y2="2.7178" layer="37"/>
<rectangle x1="7.6454" y1="2.667" x2="8.1026" y2="2.7178" layer="37"/>
<rectangle x1="8.6106" y1="2.667" x2="9.271" y2="2.7178" layer="37"/>
<rectangle x1="6.9342" y1="2.667" x2="6.985" y2="2.7178" layer="37"/>
<rectangle x1="9.271" y1="2.667" x2="9.3218" y2="2.7178" layer="37"/>
<rectangle x1="4.8006" y1="2.667" x2="4.8514" y2="2.7178" layer="37"/>
<rectangle x1="8.1026" y1="2.667" x2="8.1534" y2="2.7178" layer="37"/>
<rectangle x1="3.3782" y1="2.667" x2="3.429" y2="2.7178" layer="37"/>
<rectangle x1="3.2766" y1="2.7178" x2="3.3274" y2="2.7686" layer="37"/>
<rectangle x1="4.9022" y1="2.7178" x2="4.953" y2="2.7686" layer="37"/>
<rectangle x1="5.4102" y1="2.7178" x2="5.461" y2="2.7686" layer="37"/>
<rectangle x1="6.5278" y1="2.7178" x2="6.5786" y2="2.7686" layer="37"/>
<rectangle x1="6.9342" y1="2.7178" x2="8.1534" y2="2.7686" layer="37"/>
<rectangle x1="8.509" y1="2.7178" x2="9.3218" y2="2.7686" layer="37"/>
<rectangle x1="9.7282" y1="2.7178" x2="9.779" y2="2.7686" layer="37"/>
<rectangle x1="10.8458" y1="2.7178" x2="10.8966" y2="2.7686" layer="37"/>
<rectangle x1="3.3274" y1="2.7178" x2="3.3782" y2="2.7686" layer="37"/>
<rectangle x1="4.8514" y1="2.7178" x2="4.9022" y2="2.7686" layer="37"/>
<rectangle x1="6.477" y1="2.7178" x2="6.5278" y2="2.7686" layer="37"/>
<rectangle x1="8.4582" y1="2.7178" x2="8.509" y2="2.7686" layer="37"/>
<rectangle x1="9.3218" y1="2.7178" x2="9.3726" y2="2.7686" layer="37"/>
<rectangle x1="6.8834" y1="2.7178" x2="6.9342" y2="2.7686" layer="37"/>
<rectangle x1="5.461" y1="2.7178" x2="5.5118" y2="2.7686" layer="37"/>
<rectangle x1="10.795" y1="2.7178" x2="10.8458" y2="2.7686" layer="37"/>
<rectangle x1="3.0734" y1="3.3782" x2="3.2766" y2="3.429" layer="37"/>
<rectangle x1="4.3434" y1="3.3782" x2="4.5466" y2="3.429" layer="37"/>
<rectangle x1="9.779" y1="3.3782" x2="9.9822" y2="3.429" layer="37"/>
<rectangle x1="10.9982" y1="3.3782" x2="11.2014" y2="3.429" layer="37"/>
<rectangle x1="9.9822" y1="3.3782" x2="10.033" y2="3.429" layer="37"/>
<rectangle x1="4.5466" y1="3.3782" x2="4.5974" y2="3.429" layer="37"/>
<rectangle x1="4.2926" y1="3.3782" x2="4.3434" y2="3.429" layer="37"/>
<rectangle x1="3.2766" y1="3.3782" x2="3.3274" y2="3.429" layer="37"/>
<rectangle x1="3.0226" y1="3.3782" x2="3.0734" y2="3.429" layer="37"/>
<rectangle x1="10.9474" y1="3.3782" x2="10.9982" y2="3.429" layer="37"/>
<rectangle x1="2.9718" y1="3.429" x2="3.3274" y2="3.4798" layer="37"/>
<rectangle x1="4.2926" y1="3.429" x2="4.6482" y2="3.4798" layer="37"/>
<rectangle x1="9.7282" y1="3.429" x2="10.0838" y2="3.4798" layer="37"/>
<rectangle x1="10.9474" y1="3.429" x2="11.2522" y2="3.4798" layer="37"/>
<rectangle x1="4.2418" y1="3.429" x2="4.2926" y2="3.4798" layer="37"/>
<rectangle x1="3.3274" y1="3.429" x2="3.3782" y2="3.4798" layer="37"/>
<rectangle x1="9.6774" y1="3.429" x2="9.7282" y2="3.4798" layer="37"/>
<rectangle x1="10.8966" y1="3.429" x2="10.9474" y2="3.4798" layer="37"/>
<rectangle x1="2.9718" y1="3.4798" x2="3.3782" y2="3.5306" layer="37"/>
<rectangle x1="4.2418" y1="3.4798" x2="4.6482" y2="3.5306" layer="37"/>
<rectangle x1="9.6774" y1="3.4798" x2="10.1346" y2="3.5306" layer="37"/>
<rectangle x1="10.8966" y1="3.4798" x2="11.2522" y2="3.5306" layer="37"/>
<rectangle x1="11.2522" y1="3.4798" x2="11.303" y2="3.5306" layer="37"/>
<rectangle x1="4.6482" y1="3.4798" x2="4.699" y2="3.5306" layer="37"/>
<rectangle x1="2.921" y1="3.4798" x2="2.9718" y2="3.5306" layer="37"/>
<rectangle x1="4.191" y1="3.4798" x2="4.2418" y2="3.5306" layer="37"/>
<rectangle x1="3.3782" y1="3.4798" x2="3.429" y2="3.5306" layer="37"/>
<rectangle x1="2.9718" y1="3.5306" x2="3.429" y2="3.5814" layer="37"/>
<rectangle x1="4.191" y1="3.5306" x2="4.699" y2="3.5814" layer="37"/>
<rectangle x1="9.6774" y1="3.5306" x2="10.1346" y2="3.5814" layer="37"/>
<rectangle x1="10.8966" y1="3.5306" x2="11.2522" y2="3.5814" layer="37"/>
<rectangle x1="2.921" y1="3.5306" x2="2.9718" y2="3.5814" layer="37"/>
<rectangle x1="11.2522" y1="3.5306" x2="11.303" y2="3.5814" layer="37"/>
<rectangle x1="9.6266" y1="3.5306" x2="9.6774" y2="3.5814" layer="37"/>
<rectangle x1="2.921" y1="3.5814" x2="3.429" y2="3.6322" layer="37"/>
<rectangle x1="4.191" y1="3.5814" x2="4.699" y2="3.6322" layer="37"/>
<rectangle x1="9.6266" y1="3.5814" x2="10.1346" y2="3.6322" layer="37"/>
<rectangle x1="10.8966" y1="3.5814" x2="11.2522" y2="3.6322" layer="37"/>
<rectangle x1="11.2522" y1="3.5814" x2="11.303" y2="3.6322" layer="37"/>
<rectangle x1="10.1346" y1="3.5814" x2="10.1854" y2="3.6322" layer="37"/>
<rectangle x1="2.921" y1="3.6322" x2="3.429" y2="3.683" layer="37"/>
<rectangle x1="4.191" y1="3.6322" x2="4.699" y2="3.683" layer="37"/>
<rectangle x1="9.6266" y1="3.6322" x2="10.1854" y2="3.683" layer="37"/>
<rectangle x1="10.8966" y1="3.6322" x2="11.303" y2="3.683" layer="37"/>
<rectangle x1="10.8458" y1="3.6322" x2="10.8966" y2="3.683" layer="37"/>
<rectangle x1="2.921" y1="3.683" x2="3.429" y2="3.7338" layer="37"/>
<rectangle x1="4.191" y1="3.683" x2="4.699" y2="3.7338" layer="37"/>
<rectangle x1="9.6266" y1="3.683" x2="10.1854" y2="3.7338" layer="37"/>
<rectangle x1="10.8966" y1="3.683" x2="11.303" y2="3.7338" layer="37"/>
<rectangle x1="10.8458" y1="3.683" x2="10.8966" y2="3.7338" layer="37"/>
<rectangle x1="2.921" y1="3.7338" x2="3.429" y2="3.7846" layer="37"/>
<rectangle x1="4.191" y1="3.7338" x2="4.699" y2="3.7846" layer="37"/>
<rectangle x1="9.6266" y1="3.7338" x2="10.1854" y2="3.7846" layer="37"/>
<rectangle x1="10.8966" y1="3.7338" x2="11.303" y2="3.7846" layer="37"/>
<rectangle x1="10.8458" y1="3.7338" x2="10.8966" y2="3.7846" layer="37"/>
<rectangle x1="2.921" y1="3.7846" x2="3.429" y2="3.8354" layer="37"/>
<rectangle x1="4.191" y1="3.7846" x2="4.699" y2="3.8354" layer="37"/>
<rectangle x1="9.6266" y1="3.7846" x2="10.1854" y2="3.8354" layer="37"/>
<rectangle x1="10.8966" y1="3.7846" x2="11.303" y2="3.8354" layer="37"/>
<rectangle x1="10.8458" y1="3.7846" x2="10.8966" y2="3.8354" layer="37"/>
<rectangle x1="2.921" y1="3.8354" x2="3.429" y2="3.8862" layer="37"/>
<rectangle x1="4.191" y1="3.8354" x2="4.699" y2="3.8862" layer="37"/>
<rectangle x1="9.6266" y1="3.8354" x2="10.1854" y2="3.8862" layer="37"/>
<rectangle x1="10.8966" y1="3.8354" x2="11.303" y2="3.8862" layer="37"/>
<rectangle x1="7.3406" y1="3.8354" x2="7.3914" y2="3.8862" layer="37"/>
<rectangle x1="7.3914" y1="3.8354" x2="7.4422" y2="3.8862" layer="37"/>
<rectangle x1="7.2898" y1="3.8354" x2="7.3406" y2="3.8862" layer="37"/>
<rectangle x1="10.8458" y1="3.8354" x2="10.8966" y2="3.8862" layer="37"/>
<rectangle x1="2.921" y1="3.8862" x2="3.429" y2="3.937" layer="37"/>
<rectangle x1="4.191" y1="3.8862" x2="4.699" y2="3.937" layer="37"/>
<rectangle x1="7.239" y1="3.8862" x2="7.493" y2="3.937" layer="37"/>
<rectangle x1="9.6266" y1="3.8862" x2="10.1854" y2="3.937" layer="37"/>
<rectangle x1="10.8966" y1="3.8862" x2="11.303" y2="3.937" layer="37"/>
<rectangle x1="7.1882" y1="3.8862" x2="7.239" y2="3.937" layer="37"/>
<rectangle x1="7.493" y1="3.8862" x2="7.5438" y2="3.937" layer="37"/>
<rectangle x1="10.8458" y1="3.8862" x2="10.8966" y2="3.937" layer="37"/>
<rectangle x1="2.921" y1="3.937" x2="3.429" y2="3.9878" layer="37"/>
<rectangle x1="4.191" y1="3.937" x2="4.699" y2="3.9878" layer="37"/>
<rectangle x1="7.1882" y1="3.937" x2="7.5438" y2="3.9878" layer="37"/>
<rectangle x1="9.6266" y1="3.937" x2="10.1854" y2="3.9878" layer="37"/>
<rectangle x1="10.8966" y1="3.937" x2="11.303" y2="3.9878" layer="37"/>
<rectangle x1="7.1374" y1="3.937" x2="7.1882" y2="3.9878" layer="37"/>
<rectangle x1="7.5438" y1="3.937" x2="7.5946" y2="3.9878" layer="37"/>
<rectangle x1="10.8458" y1="3.937" x2="10.8966" y2="3.9878" layer="37"/>
<rectangle x1="2.921" y1="3.9878" x2="3.429" y2="4.0386" layer="37"/>
<rectangle x1="4.191" y1="3.9878" x2="4.699" y2="4.0386" layer="37"/>
<rectangle x1="7.1374" y1="3.9878" x2="7.5946" y2="4.0386" layer="37"/>
<rectangle x1="9.6266" y1="3.9878" x2="10.1854" y2="4.0386" layer="37"/>
<rectangle x1="10.8966" y1="3.9878" x2="11.303" y2="4.0386" layer="37"/>
<rectangle x1="7.0866" y1="3.9878" x2="7.1374" y2="4.0386" layer="37"/>
<rectangle x1="7.5946" y1="3.9878" x2="7.6454" y2="4.0386" layer="37"/>
<rectangle x1="10.8458" y1="3.9878" x2="10.8966" y2="4.0386" layer="37"/>
<rectangle x1="2.921" y1="4.0386" x2="3.429" y2="4.0894" layer="37"/>
<rectangle x1="4.191" y1="4.0386" x2="4.699" y2="4.0894" layer="37"/>
<rectangle x1="7.0866" y1="4.0386" x2="7.6454" y2="4.0894" layer="37"/>
<rectangle x1="9.6266" y1="4.0386" x2="10.1854" y2="4.0894" layer="37"/>
<rectangle x1="10.8966" y1="4.0386" x2="11.303" y2="4.0894" layer="37"/>
<rectangle x1="7.0358" y1="4.0386" x2="7.0866" y2="4.0894" layer="37"/>
<rectangle x1="7.6454" y1="4.0386" x2="7.6962" y2="4.0894" layer="37"/>
<rectangle x1="10.8458" y1="4.0386" x2="10.8966" y2="4.0894" layer="37"/>
<rectangle x1="2.921" y1="4.0894" x2="3.429" y2="4.1402" layer="37"/>
<rectangle x1="4.191" y1="4.0894" x2="4.699" y2="4.1402" layer="37"/>
<rectangle x1="7.0358" y1="4.0894" x2="7.6962" y2="4.1402" layer="37"/>
<rectangle x1="9.6266" y1="4.0894" x2="10.1854" y2="4.1402" layer="37"/>
<rectangle x1="10.8966" y1="4.0894" x2="11.303" y2="4.1402" layer="37"/>
<rectangle x1="6.985" y1="4.0894" x2="7.0358" y2="4.1402" layer="37"/>
<rectangle x1="7.6962" y1="4.0894" x2="7.747" y2="4.1402" layer="37"/>
<rectangle x1="10.8458" y1="4.0894" x2="10.8966" y2="4.1402" layer="37"/>
<rectangle x1="2.921" y1="4.1402" x2="3.429" y2="4.191" layer="37"/>
<rectangle x1="4.191" y1="4.1402" x2="4.699" y2="4.191" layer="37"/>
<rectangle x1="6.985" y1="4.1402" x2="7.747" y2="4.191" layer="37"/>
<rectangle x1="9.6266" y1="4.1402" x2="10.1854" y2="4.191" layer="37"/>
<rectangle x1="10.8966" y1="4.1402" x2="11.303" y2="4.191" layer="37"/>
<rectangle x1="6.9342" y1="4.1402" x2="6.985" y2="4.191" layer="37"/>
<rectangle x1="7.747" y1="4.1402" x2="7.7978" y2="4.191" layer="37"/>
<rectangle x1="10.8458" y1="4.1402" x2="10.8966" y2="4.191" layer="37"/>
<rectangle x1="2.921" y1="4.191" x2="3.429" y2="4.2418" layer="37"/>
<rectangle x1="4.191" y1="4.191" x2="4.699" y2="4.2418" layer="37"/>
<rectangle x1="6.985" y1="4.191" x2="7.7978" y2="4.2418" layer="37"/>
<rectangle x1="9.6266" y1="4.191" x2="10.1854" y2="4.2418" layer="37"/>
<rectangle x1="10.8966" y1="4.191" x2="11.303" y2="4.2418" layer="37"/>
<rectangle x1="6.9342" y1="4.191" x2="6.985" y2="4.2418" layer="37"/>
<rectangle x1="6.8834" y1="4.191" x2="6.9342" y2="4.2418" layer="37"/>
<rectangle x1="7.7978" y1="4.191" x2="7.8486" y2="4.2418" layer="37"/>
<rectangle x1="10.8458" y1="4.191" x2="10.8966" y2="4.2418" layer="37"/>
<rectangle x1="2.921" y1="4.2418" x2="3.429" y2="4.2926" layer="37"/>
<rectangle x1="4.191" y1="4.2418" x2="4.699" y2="4.2926" layer="37"/>
<rectangle x1="6.8834" y1="4.2418" x2="7.8486" y2="4.2926" layer="37"/>
<rectangle x1="9.6266" y1="4.2418" x2="10.1854" y2="4.2926" layer="37"/>
<rectangle x1="10.8966" y1="4.2418" x2="11.303" y2="4.2926" layer="37"/>
<rectangle x1="6.8326" y1="4.2418" x2="6.8834" y2="4.2926" layer="37"/>
<rectangle x1="10.8458" y1="4.2418" x2="10.8966" y2="4.2926" layer="37"/>
<rectangle x1="7.8486" y1="4.2418" x2="7.8994" y2="4.2926" layer="37"/>
<rectangle x1="2.921" y1="4.2926" x2="3.429" y2="4.3434" layer="37"/>
<rectangle x1="4.191" y1="4.2926" x2="4.699" y2="4.3434" layer="37"/>
<rectangle x1="6.8326" y1="4.2926" x2="7.8994" y2="4.3434" layer="37"/>
<rectangle x1="9.6266" y1="4.2926" x2="10.1854" y2="4.3434" layer="37"/>
<rectangle x1="10.8966" y1="4.2926" x2="11.303" y2="4.3434" layer="37"/>
<rectangle x1="10.8458" y1="4.2926" x2="10.8966" y2="4.3434" layer="37"/>
<rectangle x1="2.921" y1="4.3434" x2="3.429" y2="4.3942" layer="37"/>
<rectangle x1="4.191" y1="4.3434" x2="4.699" y2="4.3942" layer="37"/>
<rectangle x1="6.7818" y1="4.3434" x2="7.9502" y2="4.3942" layer="37"/>
<rectangle x1="9.6266" y1="4.3434" x2="10.1854" y2="4.3942" layer="37"/>
<rectangle x1="10.8966" y1="4.3434" x2="11.303" y2="4.3942" layer="37"/>
<rectangle x1="10.8458" y1="4.3434" x2="10.8966" y2="4.3942" layer="37"/>
<rectangle x1="2.921" y1="4.3942" x2="3.429" y2="4.445" layer="37"/>
<rectangle x1="4.191" y1="4.3942" x2="4.699" y2="4.445" layer="37"/>
<rectangle x1="6.731" y1="4.3942" x2="8.001" y2="4.445" layer="37"/>
<rectangle x1="9.6266" y1="4.3942" x2="10.1854" y2="4.445" layer="37"/>
<rectangle x1="10.8966" y1="4.3942" x2="11.303" y2="4.445" layer="37"/>
<rectangle x1="10.8458" y1="4.3942" x2="10.8966" y2="4.445" layer="37"/>
<rectangle x1="2.921" y1="4.445" x2="3.429" y2="4.4958" layer="37"/>
<rectangle x1="4.191" y1="4.445" x2="4.699" y2="4.4958" layer="37"/>
<rectangle x1="6.6802" y1="4.445" x2="8.0518" y2="4.4958" layer="37"/>
<rectangle x1="9.6266" y1="4.445" x2="10.1854" y2="4.4958" layer="37"/>
<rectangle x1="10.8966" y1="4.445" x2="11.303" y2="4.4958" layer="37"/>
<rectangle x1="10.8458" y1="4.445" x2="10.8966" y2="4.4958" layer="37"/>
<rectangle x1="2.921" y1="4.4958" x2="3.429" y2="4.5466" layer="37"/>
<rectangle x1="4.191" y1="4.4958" x2="4.699" y2="4.5466" layer="37"/>
<rectangle x1="6.6294" y1="4.4958" x2="7.3406" y2="4.5466" layer="37"/>
<rectangle x1="7.3914" y1="4.4958" x2="8.1026" y2="4.5466" layer="37"/>
<rectangle x1="9.6266" y1="4.4958" x2="10.1854" y2="4.5466" layer="37"/>
<rectangle x1="10.8966" y1="4.4958" x2="11.303" y2="4.5466" layer="37"/>
<rectangle x1="7.3406" y1="4.4958" x2="7.3914" y2="4.5466" layer="37"/>
<rectangle x1="10.8458" y1="4.4958" x2="10.8966" y2="4.5466" layer="37"/>
<rectangle x1="2.921" y1="4.5466" x2="3.429" y2="4.5974" layer="37"/>
<rectangle x1="4.191" y1="4.5466" x2="4.699" y2="4.5974" layer="37"/>
<rectangle x1="6.6294" y1="4.5466" x2="7.2898" y2="4.5974" layer="37"/>
<rectangle x1="7.4422" y1="4.5466" x2="8.1534" y2="4.5974" layer="37"/>
<rectangle x1="9.6266" y1="4.5466" x2="10.1854" y2="4.5974" layer="37"/>
<rectangle x1="10.8966" y1="4.5466" x2="11.303" y2="4.5974" layer="37"/>
<rectangle x1="6.5786" y1="4.5466" x2="6.6294" y2="4.5974" layer="37"/>
<rectangle x1="7.3914" y1="4.5466" x2="7.4422" y2="4.5974" layer="37"/>
<rectangle x1="7.2898" y1="4.5466" x2="7.3406" y2="4.5974" layer="37"/>
<rectangle x1="10.8458" y1="4.5466" x2="10.8966" y2="4.5974" layer="37"/>
<rectangle x1="2.921" y1="4.5974" x2="3.429" y2="4.6482" layer="37"/>
<rectangle x1="4.191" y1="4.5974" x2="4.699" y2="4.6482" layer="37"/>
<rectangle x1="6.5786" y1="4.5974" x2="7.239" y2="4.6482" layer="37"/>
<rectangle x1="7.493" y1="4.5974" x2="8.1534" y2="4.6482" layer="37"/>
<rectangle x1="9.6266" y1="4.5974" x2="10.1854" y2="4.6482" layer="37"/>
<rectangle x1="10.8966" y1="4.5974" x2="11.303" y2="4.6482" layer="37"/>
<rectangle x1="6.5278" y1="4.5974" x2="6.5786" y2="4.6482" layer="37"/>
<rectangle x1="8.1534" y1="4.5974" x2="8.2042" y2="4.6482" layer="37"/>
<rectangle x1="7.4422" y1="4.5974" x2="7.493" y2="4.6482" layer="37"/>
<rectangle x1="7.239" y1="4.5974" x2="7.2898" y2="4.6482" layer="37"/>
<rectangle x1="10.8458" y1="4.5974" x2="10.8966" y2="4.6482" layer="37"/>
<rectangle x1="2.921" y1="4.6482" x2="3.429" y2="4.699" layer="37"/>
<rectangle x1="4.191" y1="4.6482" x2="4.699" y2="4.699" layer="37"/>
<rectangle x1="6.5278" y1="4.6482" x2="7.1882" y2="4.699" layer="37"/>
<rectangle x1="7.5438" y1="4.6482" x2="8.2042" y2="4.699" layer="37"/>
<rectangle x1="9.6266" y1="4.6482" x2="10.1854" y2="4.699" layer="37"/>
<rectangle x1="10.8966" y1="4.6482" x2="11.303" y2="4.699" layer="37"/>
<rectangle x1="6.477" y1="4.6482" x2="6.5278" y2="4.699" layer="37"/>
<rectangle x1="8.2042" y1="4.6482" x2="8.255" y2="4.699" layer="37"/>
<rectangle x1="7.493" y1="4.6482" x2="7.5438" y2="4.699" layer="37"/>
<rectangle x1="7.1882" y1="4.6482" x2="7.239" y2="4.699" layer="37"/>
<rectangle x1="10.8458" y1="4.6482" x2="10.8966" y2="4.699" layer="37"/>
<rectangle x1="2.921" y1="4.699" x2="3.429" y2="4.7498" layer="37"/>
<rectangle x1="4.191" y1="4.699" x2="4.699" y2="4.7498" layer="37"/>
<rectangle x1="6.477" y1="4.699" x2="7.1374" y2="4.7498" layer="37"/>
<rectangle x1="7.5946" y1="4.699" x2="8.255" y2="4.7498" layer="37"/>
<rectangle x1="9.6266" y1="4.699" x2="10.1854" y2="4.7498" layer="37"/>
<rectangle x1="10.8966" y1="4.699" x2="11.303" y2="4.7498" layer="37"/>
<rectangle x1="6.4262" y1="4.699" x2="6.477" y2="4.7498" layer="37"/>
<rectangle x1="7.5438" y1="4.699" x2="7.5946" y2="4.7498" layer="37"/>
<rectangle x1="7.1374" y1="4.699" x2="7.1882" y2="4.7498" layer="37"/>
<rectangle x1="8.255" y1="4.699" x2="8.3058" y2="4.7498" layer="37"/>
<rectangle x1="10.8458" y1="4.699" x2="10.8966" y2="4.7498" layer="37"/>
<rectangle x1="2.921" y1="4.7498" x2="3.429" y2="4.8006" layer="37"/>
<rectangle x1="4.191" y1="4.7498" x2="4.699" y2="4.8006" layer="37"/>
<rectangle x1="6.4262" y1="4.7498" x2="7.0866" y2="4.8006" layer="37"/>
<rectangle x1="7.6454" y1="4.7498" x2="8.3058" y2="4.8006" layer="37"/>
<rectangle x1="9.6266" y1="4.7498" x2="10.1854" y2="4.8006" layer="37"/>
<rectangle x1="10.8966" y1="4.7498" x2="11.303" y2="4.8006" layer="37"/>
<rectangle x1="7.0866" y1="4.7498" x2="7.1374" y2="4.8006" layer="37"/>
<rectangle x1="7.5946" y1="4.7498" x2="7.6454" y2="4.8006" layer="37"/>
<rectangle x1="6.3754" y1="4.7498" x2="6.4262" y2="4.8006" layer="37"/>
<rectangle x1="8.3058" y1="4.7498" x2="8.3566" y2="4.8006" layer="37"/>
<rectangle x1="10.8458" y1="4.7498" x2="10.8966" y2="4.8006" layer="37"/>
<rectangle x1="2.921" y1="4.8006" x2="3.429" y2="4.8514" layer="37"/>
<rectangle x1="4.191" y1="4.8006" x2="4.699" y2="4.8514" layer="37"/>
<rectangle x1="6.3754" y1="4.8006" x2="7.0358" y2="4.8514" layer="37"/>
<rectangle x1="7.6962" y1="4.8006" x2="8.3566" y2="4.8514" layer="37"/>
<rectangle x1="9.6266" y1="4.8006" x2="10.1854" y2="4.8514" layer="37"/>
<rectangle x1="10.8966" y1="4.8006" x2="11.303" y2="4.8514" layer="37"/>
<rectangle x1="7.0358" y1="4.8006" x2="7.0866" y2="4.8514" layer="37"/>
<rectangle x1="7.6454" y1="4.8006" x2="7.6962" y2="4.8514" layer="37"/>
<rectangle x1="6.3246" y1="4.8006" x2="6.3754" y2="4.8514" layer="37"/>
<rectangle x1="10.8458" y1="4.8006" x2="10.8966" y2="4.8514" layer="37"/>
<rectangle x1="8.3566" y1="4.8006" x2="8.4074" y2="4.8514" layer="37"/>
<rectangle x1="2.921" y1="4.8514" x2="3.429" y2="4.9022" layer="37"/>
<rectangle x1="4.191" y1="4.8514" x2="4.699" y2="4.9022" layer="37"/>
<rectangle x1="6.3246" y1="4.8514" x2="7.0358" y2="4.9022" layer="37"/>
<rectangle x1="7.747" y1="4.8514" x2="8.4074" y2="4.9022" layer="37"/>
<rectangle x1="9.6266" y1="4.8514" x2="10.1854" y2="4.9022" layer="37"/>
<rectangle x1="10.8966" y1="4.8514" x2="11.303" y2="4.9022" layer="37"/>
<rectangle x1="7.6962" y1="4.8514" x2="7.747" y2="4.9022" layer="37"/>
<rectangle x1="6.2738" y1="4.8514" x2="6.3246" y2="4.9022" layer="37"/>
<rectangle x1="10.8458" y1="4.8514" x2="10.8966" y2="4.9022" layer="37"/>
<rectangle x1="2.921" y1="4.9022" x2="3.429" y2="4.953" layer="37"/>
<rectangle x1="4.191" y1="4.9022" x2="4.699" y2="4.953" layer="37"/>
<rectangle x1="6.2738" y1="4.9022" x2="6.985" y2="4.953" layer="37"/>
<rectangle x1="7.747" y1="4.9022" x2="8.4074" y2="4.953" layer="37"/>
<rectangle x1="9.6266" y1="4.9022" x2="10.1854" y2="4.953" layer="37"/>
<rectangle x1="10.8966" y1="4.9022" x2="11.303" y2="4.953" layer="37"/>
<rectangle x1="8.4074" y1="4.9022" x2="8.4582" y2="4.953" layer="37"/>
<rectangle x1="6.223" y1="4.9022" x2="6.2738" y2="4.953" layer="37"/>
<rectangle x1="10.8458" y1="4.9022" x2="10.8966" y2="4.953" layer="37"/>
<rectangle x1="2.921" y1="4.953" x2="3.429" y2="5.0038" layer="37"/>
<rectangle x1="4.191" y1="4.953" x2="4.699" y2="5.0038" layer="37"/>
<rectangle x1="6.223" y1="4.953" x2="6.9342" y2="5.0038" layer="37"/>
<rectangle x1="7.7978" y1="4.953" x2="8.4582" y2="5.0038" layer="37"/>
<rectangle x1="9.6266" y1="4.953" x2="10.1854" y2="5.0038" layer="37"/>
<rectangle x1="10.8966" y1="4.953" x2="11.303" y2="5.0038" layer="37"/>
<rectangle x1="8.4582" y1="4.953" x2="8.509" y2="5.0038" layer="37"/>
<rectangle x1="6.1722" y1="4.953" x2="6.223" y2="5.0038" layer="37"/>
<rectangle x1="10.8458" y1="4.953" x2="10.8966" y2="5.0038" layer="37"/>
<rectangle x1="2.921" y1="5.0038" x2="3.429" y2="5.0546" layer="37"/>
<rectangle x1="4.191" y1="5.0038" x2="4.699" y2="5.0546" layer="37"/>
<rectangle x1="6.1722" y1="5.0038" x2="6.8834" y2="5.0546" layer="37"/>
<rectangle x1="7.8486" y1="5.0038" x2="8.509" y2="5.0546" layer="37"/>
<rectangle x1="9.6266" y1="5.0038" x2="10.1854" y2="5.0546" layer="37"/>
<rectangle x1="10.8966" y1="5.0038" x2="11.303" y2="5.0546" layer="37"/>
<rectangle x1="8.509" y1="5.0038" x2="8.5598" y2="5.0546" layer="37"/>
<rectangle x1="6.1214" y1="5.0038" x2="6.1722" y2="5.0546" layer="37"/>
<rectangle x1="10.8458" y1="5.0038" x2="10.8966" y2="5.0546" layer="37"/>
<rectangle x1="2.921" y1="5.0546" x2="3.429" y2="5.1054" layer="37"/>
<rectangle x1="4.191" y1="5.0546" x2="4.699" y2="5.1054" layer="37"/>
<rectangle x1="6.1214" y1="5.0546" x2="6.8326" y2="5.1054" layer="37"/>
<rectangle x1="7.8994" y1="5.0546" x2="8.5598" y2="5.1054" layer="37"/>
<rectangle x1="9.6266" y1="5.0546" x2="10.1854" y2="5.1054" layer="37"/>
<rectangle x1="10.8966" y1="5.0546" x2="11.303" y2="5.1054" layer="37"/>
<rectangle x1="8.5598" y1="5.0546" x2="8.6106" y2="5.1054" layer="37"/>
<rectangle x1="6.0706" y1="5.0546" x2="6.1214" y2="5.1054" layer="37"/>
<rectangle x1="10.8458" y1="5.0546" x2="10.8966" y2="5.1054" layer="37"/>
<rectangle x1="2.921" y1="5.1054" x2="3.429" y2="5.1562" layer="37"/>
<rectangle x1="4.191" y1="5.1054" x2="4.699" y2="5.1562" layer="37"/>
<rectangle x1="6.0706" y1="5.1054" x2="6.7818" y2="5.1562" layer="37"/>
<rectangle x1="7.9502" y1="5.1054" x2="8.6106" y2="5.1562" layer="37"/>
<rectangle x1="9.6266" y1="5.1054" x2="10.1854" y2="5.1562" layer="37"/>
<rectangle x1="10.8966" y1="5.1054" x2="11.303" y2="5.1562" layer="37"/>
<rectangle x1="8.6106" y1="5.1054" x2="8.6614" y2="5.1562" layer="37"/>
<rectangle x1="7.8994" y1="5.1054" x2="7.9502" y2="5.1562" layer="37"/>
<rectangle x1="10.8458" y1="5.1054" x2="10.8966" y2="5.1562" layer="37"/>
<rectangle x1="2.921" y1="5.1562" x2="3.429" y2="5.207" layer="37"/>
<rectangle x1="4.191" y1="5.1562" x2="4.699" y2="5.207" layer="37"/>
<rectangle x1="6.0706" y1="5.1562" x2="6.731" y2="5.207" layer="37"/>
<rectangle x1="8.001" y1="5.1562" x2="8.6614" y2="5.207" layer="37"/>
<rectangle x1="9.6266" y1="5.1562" x2="10.1854" y2="5.207" layer="37"/>
<rectangle x1="10.8966" y1="5.1562" x2="11.303" y2="5.207" layer="37"/>
<rectangle x1="6.0198" y1="5.1562" x2="6.0706" y2="5.207" layer="37"/>
<rectangle x1="8.6614" y1="5.1562" x2="8.7122" y2="5.207" layer="37"/>
<rectangle x1="7.9502" y1="5.1562" x2="8.001" y2="5.207" layer="37"/>
<rectangle x1="10.8458" y1="5.1562" x2="10.8966" y2="5.207" layer="37"/>
<rectangle x1="2.921" y1="5.207" x2="3.429" y2="5.2578" layer="37"/>
<rectangle x1="4.191" y1="5.207" x2="4.699" y2="5.2578" layer="37"/>
<rectangle x1="6.0198" y1="5.207" x2="6.6802" y2="5.2578" layer="37"/>
<rectangle x1="8.0518" y1="5.207" x2="8.7122" y2="5.2578" layer="37"/>
<rectangle x1="9.6266" y1="5.207" x2="10.1854" y2="5.2578" layer="37"/>
<rectangle x1="10.8966" y1="5.207" x2="11.303" y2="5.2578" layer="37"/>
<rectangle x1="5.969" y1="5.207" x2="6.0198" y2="5.2578" layer="37"/>
<rectangle x1="8.7122" y1="5.207" x2="8.763" y2="5.2578" layer="37"/>
<rectangle x1="8.001" y1="5.207" x2="8.0518" y2="5.2578" layer="37"/>
<rectangle x1="10.8458" y1="5.207" x2="10.8966" y2="5.2578" layer="37"/>
<rectangle x1="2.921" y1="5.2578" x2="3.429" y2="5.3086" layer="37"/>
<rectangle x1="4.191" y1="5.2578" x2="4.699" y2="5.3086" layer="37"/>
<rectangle x1="5.969" y1="5.2578" x2="6.6294" y2="5.3086" layer="37"/>
<rectangle x1="8.1026" y1="5.2578" x2="8.763" y2="5.3086" layer="37"/>
<rectangle x1="9.6266" y1="5.2578" x2="10.1854" y2="5.3086" layer="37"/>
<rectangle x1="10.8966" y1="5.2578" x2="11.303" y2="5.3086" layer="37"/>
<rectangle x1="5.9182" y1="5.2578" x2="5.969" y2="5.3086" layer="37"/>
<rectangle x1="8.0518" y1="5.2578" x2="8.1026" y2="5.3086" layer="37"/>
<rectangle x1="8.763" y1="5.2578" x2="8.8138" y2="5.3086" layer="37"/>
<rectangle x1="6.6294" y1="5.2578" x2="6.6802" y2="5.3086" layer="37"/>
<rectangle x1="10.8458" y1="5.2578" x2="10.8966" y2="5.3086" layer="37"/>
<rectangle x1="2.921" y1="5.3086" x2="3.429" y2="5.3594" layer="37"/>
<rectangle x1="4.191" y1="5.3086" x2="4.699" y2="5.3594" layer="37"/>
<rectangle x1="5.9182" y1="5.3086" x2="6.5786" y2="5.3594" layer="37"/>
<rectangle x1="8.1534" y1="5.3086" x2="8.8138" y2="5.3594" layer="37"/>
<rectangle x1="9.6266" y1="5.3086" x2="10.1854" y2="5.3594" layer="37"/>
<rectangle x1="10.8966" y1="5.3086" x2="11.303" y2="5.3594" layer="37"/>
<rectangle x1="5.8674" y1="5.3086" x2="5.9182" y2="5.3594" layer="37"/>
<rectangle x1="8.1026" y1="5.3086" x2="8.1534" y2="5.3594" layer="37"/>
<rectangle x1="8.8138" y1="5.3086" x2="8.8646" y2="5.3594" layer="37"/>
<rectangle x1="6.5786" y1="5.3086" x2="6.6294" y2="5.3594" layer="37"/>
<rectangle x1="10.8458" y1="5.3086" x2="10.8966" y2="5.3594" layer="37"/>
<rectangle x1="2.921" y1="5.3594" x2="3.429" y2="5.4102" layer="37"/>
<rectangle x1="4.191" y1="5.3594" x2="4.699" y2="5.4102" layer="37"/>
<rectangle x1="5.8674" y1="5.3594" x2="6.5278" y2="5.4102" layer="37"/>
<rectangle x1="8.2042" y1="5.3594" x2="8.8646" y2="5.4102" layer="37"/>
<rectangle x1="9.6266" y1="5.3594" x2="10.1854" y2="5.4102" layer="37"/>
<rectangle x1="10.8966" y1="5.3594" x2="11.303" y2="5.4102" layer="37"/>
<rectangle x1="5.8166" y1="5.3594" x2="5.8674" y2="5.4102" layer="37"/>
<rectangle x1="8.1534" y1="5.3594" x2="8.2042" y2="5.4102" layer="37"/>
<rectangle x1="8.8646" y1="5.3594" x2="8.9154" y2="5.4102" layer="37"/>
<rectangle x1="6.5278" y1="5.3594" x2="6.5786" y2="5.4102" layer="37"/>
<rectangle x1="10.8458" y1="5.3594" x2="10.8966" y2="5.4102" layer="37"/>
<rectangle x1="2.921" y1="5.4102" x2="3.429" y2="5.461" layer="37"/>
<rectangle x1="4.191" y1="5.4102" x2="4.699" y2="5.461" layer="37"/>
<rectangle x1="5.8166" y1="5.4102" x2="6.477" y2="5.461" layer="37"/>
<rectangle x1="8.255" y1="5.4102" x2="8.9154" y2="5.461" layer="37"/>
<rectangle x1="9.6266" y1="5.4102" x2="10.1854" y2="5.461" layer="37"/>
<rectangle x1="10.8966" y1="5.4102" x2="11.303" y2="5.461" layer="37"/>
<rectangle x1="8.2042" y1="5.4102" x2="8.255" y2="5.461" layer="37"/>
<rectangle x1="5.7658" y1="5.4102" x2="5.8166" y2="5.461" layer="37"/>
<rectangle x1="6.477" y1="5.4102" x2="6.5278" y2="5.461" layer="37"/>
<rectangle x1="8.9154" y1="5.4102" x2="8.9662" y2="5.461" layer="37"/>
<rectangle x1="10.8458" y1="5.4102" x2="10.8966" y2="5.461" layer="37"/>
<rectangle x1="2.921" y1="5.461" x2="3.429" y2="5.5118" layer="37"/>
<rectangle x1="4.191" y1="5.461" x2="4.699" y2="5.5118" layer="37"/>
<rectangle x1="5.7658" y1="5.461" x2="6.4262" y2="5.5118" layer="37"/>
<rectangle x1="8.255" y1="5.461" x2="8.9662" y2="5.5118" layer="37"/>
<rectangle x1="9.6266" y1="5.461" x2="10.1854" y2="5.5118" layer="37"/>
<rectangle x1="10.8966" y1="5.461" x2="11.303" y2="5.5118" layer="37"/>
<rectangle x1="5.715" y1="5.461" x2="5.7658" y2="5.5118" layer="37"/>
<rectangle x1="6.4262" y1="5.461" x2="6.477" y2="5.5118" layer="37"/>
<rectangle x1="10.8458" y1="5.461" x2="10.8966" y2="5.5118" layer="37"/>
<rectangle x1="2.921" y1="5.5118" x2="3.429" y2="5.5626" layer="37"/>
<rectangle x1="4.191" y1="5.5118" x2="4.699" y2="5.5626" layer="37"/>
<rectangle x1="5.715" y1="5.5118" x2="6.3754" y2="5.5626" layer="37"/>
<rectangle x1="8.3058" y1="5.5118" x2="9.017" y2="5.5626" layer="37"/>
<rectangle x1="9.6266" y1="5.5118" x2="10.1854" y2="5.5626" layer="37"/>
<rectangle x1="10.8966" y1="5.5118" x2="11.303" y2="5.5626" layer="37"/>
<rectangle x1="5.6642" y1="5.5118" x2="5.715" y2="5.5626" layer="37"/>
<rectangle x1="6.3754" y1="5.5118" x2="6.4262" y2="5.5626" layer="37"/>
<rectangle x1="10.8458" y1="5.5118" x2="10.8966" y2="5.5626" layer="37"/>
<rectangle x1="2.921" y1="5.5626" x2="3.429" y2="5.6134" layer="37"/>
<rectangle x1="4.191" y1="5.5626" x2="4.699" y2="5.6134" layer="37"/>
<rectangle x1="5.6642" y1="5.5626" x2="6.3246" y2="5.6134" layer="37"/>
<rectangle x1="8.3566" y1="5.5626" x2="9.0678" y2="5.6134" layer="37"/>
<rectangle x1="9.6266" y1="5.5626" x2="10.1854" y2="5.6134" layer="37"/>
<rectangle x1="10.8966" y1="5.5626" x2="11.303" y2="5.6134" layer="37"/>
<rectangle x1="6.3246" y1="5.5626" x2="6.3754" y2="5.6134" layer="37"/>
<rectangle x1="5.6134" y1="5.5626" x2="5.6642" y2="5.6134" layer="37"/>
<rectangle x1="10.8458" y1="5.5626" x2="10.8966" y2="5.6134" layer="37"/>
<rectangle x1="2.921" y1="5.6134" x2="3.429" y2="5.6642" layer="37"/>
<rectangle x1="4.191" y1="5.6134" x2="4.699" y2="5.6642" layer="37"/>
<rectangle x1="5.6134" y1="5.6134" x2="6.2738" y2="5.6642" layer="37"/>
<rectangle x1="8.4074" y1="5.6134" x2="9.1186" y2="5.6642" layer="37"/>
<rectangle x1="9.6266" y1="5.6134" x2="10.1854" y2="5.6642" layer="37"/>
<rectangle x1="10.8966" y1="5.6134" x2="11.303" y2="5.6642" layer="37"/>
<rectangle x1="6.2738" y1="5.6134" x2="6.3246" y2="5.6642" layer="37"/>
<rectangle x1="5.5626" y1="5.6134" x2="5.6134" y2="5.6642" layer="37"/>
<rectangle x1="10.8458" y1="5.6134" x2="10.8966" y2="5.6642" layer="37"/>
<rectangle x1="2.921" y1="5.6642" x2="3.429" y2="5.715" layer="37"/>
<rectangle x1="4.191" y1="5.6642" x2="4.699" y2="5.715" layer="37"/>
<rectangle x1="5.5626" y1="5.6642" x2="6.223" y2="5.715" layer="37"/>
<rectangle x1="8.4582" y1="5.6642" x2="9.1694" y2="5.715" layer="37"/>
<rectangle x1="9.6266" y1="5.6642" x2="10.1854" y2="5.715" layer="37"/>
<rectangle x1="10.8966" y1="5.6642" x2="11.303" y2="5.715" layer="37"/>
<rectangle x1="6.223" y1="5.6642" x2="6.2738" y2="5.715" layer="37"/>
<rectangle x1="5.5118" y1="5.6642" x2="5.5626" y2="5.715" layer="37"/>
<rectangle x1="10.8458" y1="5.6642" x2="10.8966" y2="5.715" layer="37"/>
<rectangle x1="8.4074" y1="5.6642" x2="8.4582" y2="5.715" layer="37"/>
<rectangle x1="2.921" y1="5.715" x2="3.429" y2="5.7658" layer="37"/>
<rectangle x1="4.191" y1="5.715" x2="4.699" y2="5.7658" layer="37"/>
<rectangle x1="5.5118" y1="5.715" x2="6.1722" y2="5.7658" layer="37"/>
<rectangle x1="8.509" y1="5.715" x2="9.1694" y2="5.7658" layer="37"/>
<rectangle x1="9.6266" y1="5.715" x2="10.1854" y2="5.7658" layer="37"/>
<rectangle x1="10.8966" y1="5.715" x2="11.303" y2="5.7658" layer="37"/>
<rectangle x1="9.1694" y1="5.715" x2="9.2202" y2="5.7658" layer="37"/>
<rectangle x1="6.1722" y1="5.715" x2="6.223" y2="5.7658" layer="37"/>
<rectangle x1="8.4582" y1="5.715" x2="8.509" y2="5.7658" layer="37"/>
<rectangle x1="5.461" y1="5.715" x2="5.5118" y2="5.7658" layer="37"/>
<rectangle x1="10.8458" y1="5.715" x2="10.8966" y2="5.7658" layer="37"/>
<rectangle x1="2.921" y1="5.7658" x2="3.429" y2="5.8166" layer="37"/>
<rectangle x1="4.191" y1="5.7658" x2="4.699" y2="5.8166" layer="37"/>
<rectangle x1="5.461" y1="5.7658" x2="6.1214" y2="5.8166" layer="37"/>
<rectangle x1="7.4422" y1="5.7658" x2="7.6454" y2="5.8166" layer="37"/>
<rectangle x1="8.5598" y1="5.7658" x2="9.2202" y2="5.8166" layer="37"/>
<rectangle x1="9.6266" y1="5.7658" x2="10.1854" y2="5.8166" layer="37"/>
<rectangle x1="10.8966" y1="5.7658" x2="11.303" y2="5.8166" layer="37"/>
<rectangle x1="6.1214" y1="5.7658" x2="6.1722" y2="5.8166" layer="37"/>
<rectangle x1="7.6454" y1="5.7658" x2="7.6962" y2="5.8166" layer="37"/>
<rectangle x1="9.2202" y1="5.7658" x2="9.271" y2="5.8166" layer="37"/>
<rectangle x1="7.3914" y1="5.7658" x2="7.4422" y2="5.8166" layer="37"/>
<rectangle x1="8.509" y1="5.7658" x2="8.5598" y2="5.8166" layer="37"/>
<rectangle x1="10.8458" y1="5.7658" x2="10.8966" y2="5.8166" layer="37"/>
<rectangle x1="5.4102" y1="5.7658" x2="5.461" y2="5.8166" layer="37"/>
<rectangle x1="2.921" y1="5.8166" x2="3.429" y2="5.8674" layer="37"/>
<rectangle x1="4.191" y1="5.8166" x2="4.699" y2="5.8674" layer="37"/>
<rectangle x1="5.4102" y1="5.8166" x2="6.0706" y2="5.8674" layer="37"/>
<rectangle x1="7.3914" y1="5.8166" x2="7.747" y2="5.8674" layer="37"/>
<rectangle x1="8.6106" y1="5.8166" x2="9.271" y2="5.8674" layer="37"/>
<rectangle x1="9.6266" y1="5.8166" x2="10.1854" y2="5.8674" layer="37"/>
<rectangle x1="10.8966" y1="5.8166" x2="11.303" y2="5.8674" layer="37"/>
<rectangle x1="6.0706" y1="5.8166" x2="6.1214" y2="5.8674" layer="37"/>
<rectangle x1="7.3406" y1="5.8166" x2="7.3914" y2="5.8674" layer="37"/>
<rectangle x1="9.271" y1="5.8166" x2="9.3218" y2="5.8674" layer="37"/>
<rectangle x1="8.5598" y1="5.8166" x2="8.6106" y2="5.8674" layer="37"/>
<rectangle x1="10.8458" y1="5.8166" x2="10.8966" y2="5.8674" layer="37"/>
<rectangle x1="2.921" y1="5.8674" x2="3.429" y2="5.9182" layer="37"/>
<rectangle x1="4.191" y1="5.8674" x2="4.699" y2="5.9182" layer="37"/>
<rectangle x1="5.3594" y1="5.8674" x2="6.0198" y2="5.9182" layer="37"/>
<rectangle x1="7.3406" y1="5.8674" x2="7.7978" y2="5.9182" layer="37"/>
<rectangle x1="8.6614" y1="5.8674" x2="9.3218" y2="5.9182" layer="37"/>
<rectangle x1="9.6266" y1="5.8674" x2="10.1854" y2="5.9182" layer="37"/>
<rectangle x1="10.8966" y1="5.8674" x2="11.303" y2="5.9182" layer="37"/>
<rectangle x1="6.0198" y1="5.8674" x2="6.0706" y2="5.9182" layer="37"/>
<rectangle x1="8.6106" y1="5.8674" x2="8.6614" y2="5.9182" layer="37"/>
<rectangle x1="7.2898" y1="5.8674" x2="7.3406" y2="5.9182" layer="37"/>
<rectangle x1="9.3218" y1="5.8674" x2="9.3726" y2="5.9182" layer="37"/>
<rectangle x1="10.8458" y1="5.8674" x2="10.8966" y2="5.9182" layer="37"/>
<rectangle x1="2.921" y1="5.9182" x2="3.429" y2="5.969" layer="37"/>
<rectangle x1="4.191" y1="5.9182" x2="4.699" y2="5.969" layer="37"/>
<rectangle x1="5.3594" y1="5.9182" x2="6.0198" y2="5.969" layer="37"/>
<rectangle x1="7.2898" y1="5.9182" x2="7.7978" y2="5.969" layer="37"/>
<rectangle x1="8.7122" y1="5.9182" x2="9.3218" y2="5.969" layer="37"/>
<rectangle x1="9.6266" y1="5.9182" x2="10.1854" y2="5.969" layer="37"/>
<rectangle x1="10.8966" y1="5.9182" x2="11.303" y2="5.969" layer="37"/>
<rectangle x1="9.3218" y1="5.9182" x2="9.3726" y2="5.969" layer="37"/>
<rectangle x1="8.6614" y1="5.9182" x2="8.7122" y2="5.969" layer="37"/>
<rectangle x1="5.3086" y1="5.9182" x2="5.3594" y2="5.969" layer="37"/>
<rectangle x1="7.239" y1="5.9182" x2="7.2898" y2="5.969" layer="37"/>
<rectangle x1="10.8458" y1="5.9182" x2="10.8966" y2="5.969" layer="37"/>
<rectangle x1="2.921" y1="5.969" x2="3.429" y2="6.0198" layer="37"/>
<rectangle x1="4.191" y1="5.969" x2="4.699" y2="6.0198" layer="37"/>
<rectangle x1="5.3086" y1="5.969" x2="5.969" y2="6.0198" layer="37"/>
<rectangle x1="7.239" y1="5.969" x2="7.7978" y2="6.0198" layer="37"/>
<rectangle x1="8.7122" y1="5.969" x2="9.3726" y2="6.0198" layer="37"/>
<rectangle x1="9.6266" y1="5.969" x2="10.1854" y2="6.0198" layer="37"/>
<rectangle x1="10.8966" y1="5.969" x2="11.303" y2="6.0198" layer="37"/>
<rectangle x1="7.7978" y1="5.969" x2="7.8486" y2="6.0198" layer="37"/>
<rectangle x1="5.969" y1="5.969" x2="6.0198" y2="6.0198" layer="37"/>
<rectangle x1="10.8458" y1="5.969" x2="10.8966" y2="6.0198" layer="37"/>
<rectangle x1="2.921" y1="6.0198" x2="3.429" y2="6.0706" layer="37"/>
<rectangle x1="4.191" y1="6.0198" x2="4.699" y2="6.0706" layer="37"/>
<rectangle x1="5.3086" y1="6.0198" x2="5.969" y2="6.0706" layer="37"/>
<rectangle x1="7.1882" y1="6.0198" x2="7.7978" y2="6.0706" layer="37"/>
<rectangle x1="8.7122" y1="6.0198" x2="9.3726" y2="6.0706" layer="37"/>
<rectangle x1="9.6266" y1="6.0198" x2="10.1854" y2="6.0706" layer="37"/>
<rectangle x1="10.8966" y1="6.0198" x2="11.303" y2="6.0706" layer="37"/>
<rectangle x1="7.7978" y1="6.0198" x2="7.8486" y2="6.0706" layer="37"/>
<rectangle x1="10.8458" y1="6.0198" x2="10.8966" y2="6.0706" layer="37"/>
<rectangle x1="2.921" y1="6.0706" x2="3.429" y2="6.1214" layer="37"/>
<rectangle x1="4.191" y1="6.0706" x2="4.699" y2="6.1214" layer="37"/>
<rectangle x1="5.3594" y1="6.0706" x2="5.969" y2="6.1214" layer="37"/>
<rectangle x1="7.1882" y1="6.0706" x2="7.7978" y2="6.1214" layer="37"/>
<rectangle x1="8.7122" y1="6.0706" x2="9.3726" y2="6.1214" layer="37"/>
<rectangle x1="9.6266" y1="6.0706" x2="10.1854" y2="6.1214" layer="37"/>
<rectangle x1="10.8966" y1="6.0706" x2="11.303" y2="6.1214" layer="37"/>
<rectangle x1="7.1374" y1="6.0706" x2="7.1882" y2="6.1214" layer="37"/>
<rectangle x1="5.3086" y1="6.0706" x2="5.3594" y2="6.1214" layer="37"/>
<rectangle x1="5.969" y1="6.0706" x2="6.0198" y2="6.1214" layer="37"/>
<rectangle x1="10.8458" y1="6.0706" x2="10.8966" y2="6.1214" layer="37"/>
<rectangle x1="2.921" y1="6.1214" x2="3.429" y2="6.1722" layer="37"/>
<rectangle x1="4.191" y1="6.1214" x2="4.699" y2="6.1722" layer="37"/>
<rectangle x1="5.3594" y1="6.1214" x2="6.0198" y2="6.1722" layer="37"/>
<rectangle x1="7.1374" y1="6.1214" x2="7.7978" y2="6.1722" layer="37"/>
<rectangle x1="8.7122" y1="6.1214" x2="9.3218" y2="6.1722" layer="37"/>
<rectangle x1="9.6266" y1="6.1214" x2="10.1854" y2="6.1722" layer="37"/>
<rectangle x1="10.8966" y1="6.1214" x2="11.303" y2="6.1722" layer="37"/>
<rectangle x1="7.0866" y1="6.1214" x2="7.1374" y2="6.1722" layer="37"/>
<rectangle x1="8.6614" y1="6.1214" x2="8.7122" y2="6.1722" layer="37"/>
<rectangle x1="9.3218" y1="6.1214" x2="9.3726" y2="6.1722" layer="37"/>
<rectangle x1="6.0198" y1="6.1214" x2="6.0706" y2="6.1722" layer="37"/>
<rectangle x1="10.8458" y1="6.1214" x2="10.8966" y2="6.1722" layer="37"/>
<rectangle x1="2.921" y1="6.1722" x2="3.429" y2="6.223" layer="37"/>
<rectangle x1="4.191" y1="6.1722" x2="4.699" y2="6.223" layer="37"/>
<rectangle x1="5.4102" y1="6.1722" x2="6.0198" y2="6.223" layer="37"/>
<rectangle x1="7.0866" y1="6.1722" x2="7.747" y2="6.223" layer="37"/>
<rectangle x1="8.6614" y1="6.1722" x2="9.271" y2="6.223" layer="37"/>
<rectangle x1="9.6266" y1="6.1722" x2="10.1854" y2="6.223" layer="37"/>
<rectangle x1="10.8966" y1="6.1722" x2="11.303" y2="6.223" layer="37"/>
<rectangle x1="6.0198" y1="6.1722" x2="6.0706" y2="6.223" layer="37"/>
<rectangle x1="9.271" y1="6.1722" x2="9.3218" y2="6.223" layer="37"/>
<rectangle x1="5.3594" y1="6.1722" x2="5.4102" y2="6.223" layer="37"/>
<rectangle x1="7.0358" y1="6.1722" x2="7.0866" y2="6.223" layer="37"/>
<rectangle x1="8.6106" y1="6.1722" x2="8.6614" y2="6.223" layer="37"/>
<rectangle x1="6.0706" y1="6.1722" x2="6.1214" y2="6.223" layer="37"/>
<rectangle x1="10.8458" y1="6.1722" x2="10.8966" y2="6.223" layer="37"/>
<rectangle x1="2.921" y1="6.223" x2="3.429" y2="6.2738" layer="37"/>
<rectangle x1="4.191" y1="6.223" x2="4.699" y2="6.2738" layer="37"/>
<rectangle x1="5.4102" y1="6.223" x2="6.0706" y2="6.2738" layer="37"/>
<rectangle x1="7.0866" y1="6.223" x2="7.6962" y2="6.2738" layer="37"/>
<rectangle x1="8.6106" y1="6.223" x2="9.271" y2="6.2738" layer="37"/>
<rectangle x1="9.6266" y1="6.223" x2="10.1854" y2="6.2738" layer="37"/>
<rectangle x1="10.8966" y1="6.223" x2="11.303" y2="6.2738" layer="37"/>
<rectangle x1="6.0706" y1="6.223" x2="6.1214" y2="6.2738" layer="37"/>
<rectangle x1="7.0358" y1="6.223" x2="7.0866" y2="6.2738" layer="37"/>
<rectangle x1="7.6962" y1="6.223" x2="7.747" y2="6.2738" layer="37"/>
<rectangle x1="8.5598" y1="6.223" x2="8.6106" y2="6.2738" layer="37"/>
<rectangle x1="6.985" y1="6.223" x2="7.0358" y2="6.2738" layer="37"/>
<rectangle x1="10.8458" y1="6.223" x2="10.8966" y2="6.2738" layer="37"/>
<rectangle x1="2.921" y1="6.2738" x2="3.429" y2="6.3246" layer="37"/>
<rectangle x1="4.191" y1="6.2738" x2="4.699" y2="6.3246" layer="37"/>
<rectangle x1="5.461" y1="6.2738" x2="6.1214" y2="6.3246" layer="37"/>
<rectangle x1="7.0358" y1="6.2738" x2="7.6454" y2="6.3246" layer="37"/>
<rectangle x1="8.5598" y1="6.2738" x2="9.2202" y2="6.3246" layer="37"/>
<rectangle x1="9.6266" y1="6.2738" x2="10.1854" y2="6.3246" layer="37"/>
<rectangle x1="10.8966" y1="6.2738" x2="11.303" y2="6.3246" layer="37"/>
<rectangle x1="6.1214" y1="6.2738" x2="6.1722" y2="6.3246" layer="37"/>
<rectangle x1="6.985" y1="6.2738" x2="7.0358" y2="6.3246" layer="37"/>
<rectangle x1="7.6454" y1="6.2738" x2="7.6962" y2="6.3246" layer="37"/>
<rectangle x1="9.2202" y1="6.2738" x2="9.271" y2="6.3246" layer="37"/>
<rectangle x1="10.8458" y1="6.2738" x2="10.8966" y2="6.3246" layer="37"/>
<rectangle x1="2.921" y1="6.3246" x2="3.429" y2="6.3754" layer="37"/>
<rectangle x1="4.191" y1="6.3246" x2="4.699" y2="6.3754" layer="37"/>
<rectangle x1="5.5626" y1="6.3246" x2="6.1722" y2="6.3754" layer="37"/>
<rectangle x1="6.985" y1="6.3246" x2="7.5946" y2="6.3754" layer="37"/>
<rectangle x1="8.5598" y1="6.3246" x2="9.1694" y2="6.3754" layer="37"/>
<rectangle x1="9.6266" y1="6.3246" x2="10.1854" y2="6.3754" layer="37"/>
<rectangle x1="10.8966" y1="6.3246" x2="11.303" y2="6.3754" layer="37"/>
<rectangle x1="5.5118" y1="6.3246" x2="5.5626" y2="6.3754" layer="37"/>
<rectangle x1="6.1722" y1="6.3246" x2="6.223" y2="6.3754" layer="37"/>
<rectangle x1="6.9342" y1="6.3246" x2="6.985" y2="6.3754" layer="37"/>
<rectangle x1="8.509" y1="6.3246" x2="8.5598" y2="6.3754" layer="37"/>
<rectangle x1="7.5946" y1="6.3246" x2="7.6454" y2="6.3754" layer="37"/>
<rectangle x1="9.1694" y1="6.3246" x2="9.2202" y2="6.3754" layer="37"/>
<rectangle x1="10.8458" y1="6.3246" x2="10.8966" y2="6.3754" layer="37"/>
<rectangle x1="2.921" y1="6.3754" x2="3.429" y2="6.4262" layer="37"/>
<rectangle x1="4.191" y1="6.3754" x2="4.699" y2="6.4262" layer="37"/>
<rectangle x1="5.6134" y1="6.3754" x2="6.223" y2="6.4262" layer="37"/>
<rectangle x1="6.9342" y1="6.3754" x2="7.5438" y2="6.4262" layer="37"/>
<rectangle x1="8.4582" y1="6.3754" x2="9.1186" y2="6.4262" layer="37"/>
<rectangle x1="9.6266" y1="6.3754" x2="10.1854" y2="6.4262" layer="37"/>
<rectangle x1="10.8966" y1="6.3754" x2="11.303" y2="6.4262" layer="37"/>
<rectangle x1="5.5626" y1="6.3754" x2="5.6134" y2="6.4262" layer="37"/>
<rectangle x1="6.223" y1="6.3754" x2="6.2738" y2="6.4262" layer="37"/>
<rectangle x1="7.5438" y1="6.3754" x2="7.5946" y2="6.4262" layer="37"/>
<rectangle x1="9.1186" y1="6.3754" x2="9.1694" y2="6.4262" layer="37"/>
<rectangle x1="6.8834" y1="6.3754" x2="6.9342" y2="6.4262" layer="37"/>
<rectangle x1="10.8458" y1="6.3754" x2="10.8966" y2="6.4262" layer="37"/>
<rectangle x1="2.921" y1="6.4262" x2="3.429" y2="6.477" layer="37"/>
<rectangle x1="4.191" y1="6.4262" x2="4.699" y2="6.477" layer="37"/>
<rectangle x1="5.6134" y1="6.4262" x2="6.2738" y2="6.477" layer="37"/>
<rectangle x1="6.8834" y1="6.4262" x2="7.493" y2="6.477" layer="37"/>
<rectangle x1="8.4074" y1="6.4262" x2="9.0678" y2="6.477" layer="37"/>
<rectangle x1="9.6266" y1="6.4262" x2="10.1854" y2="6.477" layer="37"/>
<rectangle x1="10.8966" y1="6.4262" x2="11.303" y2="6.477" layer="37"/>
<rectangle x1="7.493" y1="6.4262" x2="7.5438" y2="6.477" layer="37"/>
<rectangle x1="9.0678" y1="6.4262" x2="9.1186" y2="6.477" layer="37"/>
<rectangle x1="6.2738" y1="6.4262" x2="6.3246" y2="6.477" layer="37"/>
<rectangle x1="6.8326" y1="6.4262" x2="6.8834" y2="6.477" layer="37"/>
<rectangle x1="10.8458" y1="6.4262" x2="10.8966" y2="6.477" layer="37"/>
<rectangle x1="2.921" y1="6.477" x2="3.429" y2="6.5278" layer="37"/>
<rectangle x1="4.191" y1="6.477" x2="4.699" y2="6.5278" layer="37"/>
<rectangle x1="5.6642" y1="6.477" x2="6.3246" y2="6.5278" layer="37"/>
<rectangle x1="6.8326" y1="6.477" x2="7.493" y2="6.5278" layer="37"/>
<rectangle x1="8.4074" y1="6.477" x2="9.0678" y2="6.5278" layer="37"/>
<rectangle x1="9.6266" y1="6.477" x2="10.1854" y2="6.5278" layer="37"/>
<rectangle x1="10.8966" y1="6.477" x2="11.303" y2="6.5278" layer="37"/>
<rectangle x1="8.3566" y1="6.477" x2="8.4074" y2="6.5278" layer="37"/>
<rectangle x1="6.3246" y1="6.477" x2="6.3754" y2="6.5278" layer="37"/>
<rectangle x1="6.7818" y1="6.477" x2="6.8326" y2="6.5278" layer="37"/>
<rectangle x1="5.6134" y1="6.477" x2="5.6642" y2="6.5278" layer="37"/>
<rectangle x1="10.8458" y1="6.477" x2="10.8966" y2="6.5278" layer="37"/>
<rectangle x1="2.921" y1="6.5278" x2="3.429" y2="6.5786" layer="37"/>
<rectangle x1="4.191" y1="6.5278" x2="4.699" y2="6.5786" layer="37"/>
<rectangle x1="5.715" y1="6.5278" x2="6.3754" y2="6.5786" layer="37"/>
<rectangle x1="6.7818" y1="6.5278" x2="7.4422" y2="6.5786" layer="37"/>
<rectangle x1="8.3566" y1="6.5278" x2="9.017" y2="6.5786" layer="37"/>
<rectangle x1="9.6266" y1="6.5278" x2="10.1854" y2="6.5786" layer="37"/>
<rectangle x1="10.8966" y1="6.5278" x2="11.303" y2="6.5786" layer="37"/>
<rectangle x1="6.3754" y1="6.5278" x2="6.4262" y2="6.5786" layer="37"/>
<rectangle x1="8.3058" y1="6.5278" x2="8.3566" y2="6.5786" layer="37"/>
<rectangle x1="5.6642" y1="6.5278" x2="5.715" y2="6.5786" layer="37"/>
<rectangle x1="7.4422" y1="6.5278" x2="7.493" y2="6.5786" layer="37"/>
<rectangle x1="10.8458" y1="6.5278" x2="10.8966" y2="6.5786" layer="37"/>
<rectangle x1="2.921" y1="6.5786" x2="3.429" y2="6.6294" layer="37"/>
<rectangle x1="4.191" y1="6.5786" x2="4.699" y2="6.6294" layer="37"/>
<rectangle x1="5.7658" y1="6.5786" x2="6.4262" y2="6.6294" layer="37"/>
<rectangle x1="6.731" y1="6.5786" x2="7.3914" y2="6.6294" layer="37"/>
<rectangle x1="8.3058" y1="6.5786" x2="8.9662" y2="6.6294" layer="37"/>
<rectangle x1="9.6266" y1="6.5786" x2="10.1854" y2="6.6294" layer="37"/>
<rectangle x1="10.8966" y1="6.5786" x2="11.303" y2="6.6294" layer="37"/>
<rectangle x1="7.3914" y1="6.5786" x2="7.4422" y2="6.6294" layer="37"/>
<rectangle x1="5.715" y1="6.5786" x2="5.7658" y2="6.6294" layer="37"/>
<rectangle x1="6.4262" y1="6.5786" x2="6.477" y2="6.6294" layer="37"/>
<rectangle x1="8.9662" y1="6.5786" x2="9.017" y2="6.6294" layer="37"/>
<rectangle x1="10.8458" y1="6.5786" x2="10.8966" y2="6.6294" layer="37"/>
<rectangle x1="2.921" y1="6.6294" x2="3.429" y2="6.6802" layer="37"/>
<rectangle x1="4.191" y1="6.6294" x2="4.699" y2="6.6802" layer="37"/>
<rectangle x1="5.8166" y1="6.6294" x2="6.477" y2="6.6802" layer="37"/>
<rectangle x1="6.6802" y1="6.6294" x2="7.3406" y2="6.6802" layer="37"/>
<rectangle x1="8.255" y1="6.6294" x2="8.9154" y2="6.6802" layer="37"/>
<rectangle x1="9.6266" y1="6.6294" x2="10.1854" y2="6.6802" layer="37"/>
<rectangle x1="10.8966" y1="6.6294" x2="11.303" y2="6.6802" layer="37"/>
<rectangle x1="7.3406" y1="6.6294" x2="7.3914" y2="6.6802" layer="37"/>
<rectangle x1="5.7658" y1="6.6294" x2="5.8166" y2="6.6802" layer="37"/>
<rectangle x1="8.9154" y1="6.6294" x2="8.9662" y2="6.6802" layer="37"/>
<rectangle x1="10.8458" y1="6.6294" x2="10.8966" y2="6.6802" layer="37"/>
<rectangle x1="2.921" y1="6.6802" x2="3.429" y2="6.731" layer="37"/>
<rectangle x1="4.191" y1="6.6802" x2="4.699" y2="6.731" layer="37"/>
<rectangle x1="5.8674" y1="6.6802" x2="6.5278" y2="6.731" layer="37"/>
<rectangle x1="6.6802" y1="6.6802" x2="7.2898" y2="6.731" layer="37"/>
<rectangle x1="8.2042" y1="6.6802" x2="8.8646" y2="6.731" layer="37"/>
<rectangle x1="9.6266" y1="6.6802" x2="10.1854" y2="6.731" layer="37"/>
<rectangle x1="10.8966" y1="6.6802" x2="11.303" y2="6.731" layer="37"/>
<rectangle x1="7.2898" y1="6.6802" x2="7.3406" y2="6.731" layer="37"/>
<rectangle x1="5.8166" y1="6.6802" x2="5.8674" y2="6.731" layer="37"/>
<rectangle x1="6.6294" y1="6.6802" x2="6.6802" y2="6.731" layer="37"/>
<rectangle x1="8.8646" y1="6.6802" x2="8.9154" y2="6.731" layer="37"/>
<rectangle x1="10.8458" y1="6.6802" x2="10.8966" y2="6.731" layer="37"/>
<rectangle x1="2.921" y1="6.731" x2="3.429" y2="6.7818" layer="37"/>
<rectangle x1="4.191" y1="6.731" x2="4.699" y2="6.7818" layer="37"/>
<rectangle x1="5.9182" y1="6.731" x2="7.239" y2="6.7818" layer="37"/>
<rectangle x1="8.2042" y1="6.731" x2="8.8138" y2="6.7818" layer="37"/>
<rectangle x1="9.6266" y1="6.731" x2="10.1854" y2="6.7818" layer="37"/>
<rectangle x1="10.8966" y1="6.731" x2="11.303" y2="6.7818" layer="37"/>
<rectangle x1="7.239" y1="6.731" x2="7.2898" y2="6.7818" layer="37"/>
<rectangle x1="8.1534" y1="6.731" x2="8.2042" y2="6.7818" layer="37"/>
<rectangle x1="5.8674" y1="6.731" x2="5.9182" y2="6.7818" layer="37"/>
<rectangle x1="8.8138" y1="6.731" x2="8.8646" y2="6.7818" layer="37"/>
<rectangle x1="10.8458" y1="6.731" x2="10.8966" y2="6.7818" layer="37"/>
<rectangle x1="2.921" y1="6.7818" x2="3.429" y2="6.8326" layer="37"/>
<rectangle x1="4.191" y1="6.7818" x2="4.699" y2="6.8326" layer="37"/>
<rectangle x1="5.969" y1="6.7818" x2="7.239" y2="6.8326" layer="37"/>
<rectangle x1="8.1534" y1="6.7818" x2="8.8138" y2="6.8326" layer="37"/>
<rectangle x1="9.6266" y1="6.7818" x2="10.1854" y2="6.8326" layer="37"/>
<rectangle x1="10.8966" y1="6.7818" x2="11.303" y2="6.8326" layer="37"/>
<rectangle x1="5.9182" y1="6.7818" x2="5.969" y2="6.8326" layer="37"/>
<rectangle x1="8.1026" y1="6.7818" x2="8.1534" y2="6.8326" layer="37"/>
<rectangle x1="10.8458" y1="6.7818" x2="10.8966" y2="6.8326" layer="37"/>
<rectangle x1="2.921" y1="6.8326" x2="3.429" y2="6.8834" layer="37"/>
<rectangle x1="4.191" y1="6.8326" x2="4.699" y2="6.8834" layer="37"/>
<rectangle x1="6.0198" y1="6.8326" x2="7.1882" y2="6.8834" layer="37"/>
<rectangle x1="8.1026" y1="6.8326" x2="8.763" y2="6.8834" layer="37"/>
<rectangle x1="9.6266" y1="6.8326" x2="10.1854" y2="6.8834" layer="37"/>
<rectangle x1="10.8966" y1="6.8326" x2="11.303" y2="6.8834" layer="37"/>
<rectangle x1="5.969" y1="6.8326" x2="6.0198" y2="6.8834" layer="37"/>
<rectangle x1="7.1882" y1="6.8326" x2="7.239" y2="6.8834" layer="37"/>
<rectangle x1="8.0518" y1="6.8326" x2="8.1026" y2="6.8834" layer="37"/>
<rectangle x1="10.8458" y1="6.8326" x2="10.8966" y2="6.8834" layer="37"/>
<rectangle x1="2.921" y1="6.8834" x2="3.429" y2="6.9342" layer="37"/>
<rectangle x1="4.191" y1="6.8834" x2="4.699" y2="6.9342" layer="37"/>
<rectangle x1="6.0198" y1="6.8834" x2="7.1374" y2="6.9342" layer="37"/>
<rectangle x1="8.0518" y1="6.8834" x2="8.7122" y2="6.9342" layer="37"/>
<rectangle x1="9.6266" y1="6.8834" x2="10.1854" y2="6.9342" layer="37"/>
<rectangle x1="10.8966" y1="6.8834" x2="11.303" y2="6.9342" layer="37"/>
<rectangle x1="7.1374" y1="6.8834" x2="7.1882" y2="6.9342" layer="37"/>
<rectangle x1="8.7122" y1="6.8834" x2="8.763" y2="6.9342" layer="37"/>
<rectangle x1="10.8458" y1="6.8834" x2="10.8966" y2="6.9342" layer="37"/>
<rectangle x1="5.969" y1="6.8834" x2="6.0198" y2="6.9342" layer="37"/>
<rectangle x1="2.921" y1="6.9342" x2="3.429" y2="6.985" layer="37"/>
<rectangle x1="4.191" y1="6.9342" x2="4.699" y2="6.985" layer="37"/>
<rectangle x1="6.0706" y1="6.9342" x2="7.0866" y2="6.985" layer="37"/>
<rectangle x1="8.0518" y1="6.9342" x2="8.6614" y2="6.985" layer="37"/>
<rectangle x1="9.6266" y1="6.9342" x2="10.1854" y2="6.985" layer="37"/>
<rectangle x1="10.8966" y1="6.9342" x2="11.303" y2="6.985" layer="37"/>
<rectangle x1="7.0866" y1="6.9342" x2="7.1374" y2="6.985" layer="37"/>
<rectangle x1="8.001" y1="6.9342" x2="8.0518" y2="6.985" layer="37"/>
<rectangle x1="8.6614" y1="6.9342" x2="8.7122" y2="6.985" layer="37"/>
<rectangle x1="6.0198" y1="6.9342" x2="6.0706" y2="6.985" layer="37"/>
<rectangle x1="10.8458" y1="6.9342" x2="10.8966" y2="6.985" layer="37"/>
<rectangle x1="2.921" y1="6.985" x2="3.429" y2="7.0358" layer="37"/>
<rectangle x1="4.191" y1="6.985" x2="4.699" y2="7.0358" layer="37"/>
<rectangle x1="6.1214" y1="6.985" x2="7.0866" y2="7.0358" layer="37"/>
<rectangle x1="8.001" y1="6.985" x2="8.6106" y2="7.0358" layer="37"/>
<rectangle x1="9.6266" y1="6.985" x2="10.1854" y2="7.0358" layer="37"/>
<rectangle x1="10.8966" y1="6.985" x2="11.303" y2="7.0358" layer="37"/>
<rectangle x1="7.9502" y1="6.985" x2="8.001" y2="7.0358" layer="37"/>
<rectangle x1="8.6106" y1="6.985" x2="8.6614" y2="7.0358" layer="37"/>
<rectangle x1="6.0706" y1="6.985" x2="6.1214" y2="7.0358" layer="37"/>
<rectangle x1="10.8458" y1="6.985" x2="10.8966" y2="7.0358" layer="37"/>
<rectangle x1="2.921" y1="7.0358" x2="3.429" y2="7.0866" layer="37"/>
<rectangle x1="4.191" y1="7.0358" x2="4.699" y2="7.0866" layer="37"/>
<rectangle x1="6.1722" y1="7.0358" x2="7.0358" y2="7.0866" layer="37"/>
<rectangle x1="7.9502" y1="7.0358" x2="8.5598" y2="7.0866" layer="37"/>
<rectangle x1="9.6266" y1="7.0358" x2="10.1854" y2="7.0866" layer="37"/>
<rectangle x1="10.8966" y1="7.0358" x2="11.303" y2="7.0866" layer="37"/>
<rectangle x1="8.5598" y1="7.0358" x2="8.6106" y2="7.0866" layer="37"/>
<rectangle x1="6.1214" y1="7.0358" x2="6.1722" y2="7.0866" layer="37"/>
<rectangle x1="7.8994" y1="7.0358" x2="7.9502" y2="7.0866" layer="37"/>
<rectangle x1="10.8458" y1="7.0358" x2="10.8966" y2="7.0866" layer="37"/>
<rectangle x1="2.921" y1="7.0866" x2="3.429" y2="7.1374" layer="37"/>
<rectangle x1="4.191" y1="7.0866" x2="4.699" y2="7.1374" layer="37"/>
<rectangle x1="6.1722" y1="7.0866" x2="6.985" y2="7.1374" layer="37"/>
<rectangle x1="7.8994" y1="7.0866" x2="8.5598" y2="7.1374" layer="37"/>
<rectangle x1="9.6266" y1="7.0866" x2="10.1854" y2="7.1374" layer="37"/>
<rectangle x1="10.8966" y1="7.0866" x2="11.303" y2="7.1374" layer="37"/>
<rectangle x1="7.8486" y1="7.0866" x2="7.8994" y2="7.1374" layer="37"/>
<rectangle x1="6.985" y1="7.0866" x2="7.0358" y2="7.1374" layer="37"/>
<rectangle x1="10.8458" y1="7.0866" x2="10.8966" y2="7.1374" layer="37"/>
<rectangle x1="2.921" y1="7.1374" x2="3.429" y2="7.1882" layer="37"/>
<rectangle x1="4.191" y1="7.1374" x2="4.699" y2="7.1882" layer="37"/>
<rectangle x1="6.223" y1="7.1374" x2="6.9342" y2="7.1882" layer="37"/>
<rectangle x1="7.8486" y1="7.1374" x2="8.509" y2="7.1882" layer="37"/>
<rectangle x1="9.6266" y1="7.1374" x2="10.1854" y2="7.1882" layer="37"/>
<rectangle x1="10.8966" y1="7.1374" x2="11.303" y2="7.1882" layer="37"/>
<rectangle x1="6.9342" y1="7.1374" x2="6.985" y2="7.1882" layer="37"/>
<rectangle x1="7.7978" y1="7.1374" x2="7.8486" y2="7.1882" layer="37"/>
<rectangle x1="10.8458" y1="7.1374" x2="10.8966" y2="7.1882" layer="37"/>
<rectangle x1="2.921" y1="7.1882" x2="3.429" y2="7.239" layer="37"/>
<rectangle x1="4.191" y1="7.1882" x2="4.699" y2="7.239" layer="37"/>
<rectangle x1="6.223" y1="7.1882" x2="6.8834" y2="7.239" layer="37"/>
<rectangle x1="7.8486" y1="7.1882" x2="8.4582" y2="7.239" layer="37"/>
<rectangle x1="9.6266" y1="7.1882" x2="10.1854" y2="7.239" layer="37"/>
<rectangle x1="10.8966" y1="7.1882" x2="11.303" y2="7.239" layer="37"/>
<rectangle x1="7.7978" y1="7.1882" x2="7.8486" y2="7.239" layer="37"/>
<rectangle x1="6.8834" y1="7.1882" x2="6.9342" y2="7.239" layer="37"/>
<rectangle x1="8.4582" y1="7.1882" x2="8.509" y2="7.239" layer="37"/>
<rectangle x1="10.8458" y1="7.1882" x2="10.8966" y2="7.239" layer="37"/>
<rectangle x1="2.921" y1="7.239" x2="3.429" y2="7.2898" layer="37"/>
<rectangle x1="4.191" y1="7.239" x2="4.699" y2="7.2898" layer="37"/>
<rectangle x1="6.223" y1="7.239" x2="6.8834" y2="7.2898" layer="37"/>
<rectangle x1="7.7978" y1="7.239" x2="8.4074" y2="7.2898" layer="37"/>
<rectangle x1="9.6266" y1="7.239" x2="10.1854" y2="7.2898" layer="37"/>
<rectangle x1="10.8966" y1="7.239" x2="11.303" y2="7.2898" layer="37"/>
<rectangle x1="6.1722" y1="7.239" x2="6.223" y2="7.2898" layer="37"/>
<rectangle x1="7.747" y1="7.239" x2="7.7978" y2="7.2898" layer="37"/>
<rectangle x1="8.4074" y1="7.239" x2="8.4582" y2="7.2898" layer="37"/>
<rectangle x1="10.8458" y1="7.239" x2="10.8966" y2="7.2898" layer="37"/>
<rectangle x1="2.921" y1="7.2898" x2="3.429" y2="7.3406" layer="37"/>
<rectangle x1="4.191" y1="7.2898" x2="4.699" y2="7.3406" layer="37"/>
<rectangle x1="6.1722" y1="7.2898" x2="6.8326" y2="7.3406" layer="37"/>
<rectangle x1="7.747" y1="7.2898" x2="8.3566" y2="7.3406" layer="37"/>
<rectangle x1="9.6266" y1="7.2898" x2="10.1854" y2="7.3406" layer="37"/>
<rectangle x1="10.8966" y1="7.2898" x2="11.303" y2="7.3406" layer="37"/>
<rectangle x1="6.1214" y1="7.2898" x2="6.1722" y2="7.3406" layer="37"/>
<rectangle x1="7.6962" y1="7.2898" x2="7.747" y2="7.3406" layer="37"/>
<rectangle x1="8.3566" y1="7.2898" x2="8.4074" y2="7.3406" layer="37"/>
<rectangle x1="10.8458" y1="7.2898" x2="10.8966" y2="7.3406" layer="37"/>
<rectangle x1="2.921" y1="7.3406" x2="3.429" y2="7.3914" layer="37"/>
<rectangle x1="4.191" y1="7.3406" x2="4.699" y2="7.3914" layer="37"/>
<rectangle x1="6.1214" y1="7.3406" x2="6.7818" y2="7.3914" layer="37"/>
<rectangle x1="7.6962" y1="7.3406" x2="8.3058" y2="7.3914" layer="37"/>
<rectangle x1="9.6266" y1="7.3406" x2="10.1854" y2="7.3914" layer="37"/>
<rectangle x1="10.8966" y1="7.3406" x2="11.303" y2="7.3914" layer="37"/>
<rectangle x1="8.3058" y1="7.3406" x2="8.3566" y2="7.3914" layer="37"/>
<rectangle x1="6.0706" y1="7.3406" x2="6.1214" y2="7.3914" layer="37"/>
<rectangle x1="7.6454" y1="7.3406" x2="7.6962" y2="7.3914" layer="37"/>
<rectangle x1="10.8458" y1="7.3406" x2="10.8966" y2="7.3914" layer="37"/>
<rectangle x1="2.921" y1="7.3914" x2="3.429" y2="7.4422" layer="37"/>
<rectangle x1="4.191" y1="7.3914" x2="4.699" y2="7.4422" layer="37"/>
<rectangle x1="6.0706" y1="7.3914" x2="6.731" y2="7.4422" layer="37"/>
<rectangle x1="7.6454" y1="7.3914" x2="8.255" y2="7.4422" layer="37"/>
<rectangle x1="9.6266" y1="7.3914" x2="10.1854" y2="7.4422" layer="37"/>
<rectangle x1="10.8966" y1="7.3914" x2="11.303" y2="7.4422" layer="37"/>
<rectangle x1="8.255" y1="7.3914" x2="8.3058" y2="7.4422" layer="37"/>
<rectangle x1="7.5946" y1="7.3914" x2="7.6454" y2="7.4422" layer="37"/>
<rectangle x1="6.0198" y1="7.3914" x2="6.0706" y2="7.4422" layer="37"/>
<rectangle x1="6.731" y1="7.3914" x2="6.7818" y2="7.4422" layer="37"/>
<rectangle x1="10.8458" y1="7.3914" x2="10.8966" y2="7.4422" layer="37"/>
<rectangle x1="2.921" y1="7.4422" x2="3.429" y2="7.493" layer="37"/>
<rectangle x1="4.191" y1="7.4422" x2="4.699" y2="7.493" layer="37"/>
<rectangle x1="6.0198" y1="7.4422" x2="6.6802" y2="7.493" layer="37"/>
<rectangle x1="7.5946" y1="7.4422" x2="8.2042" y2="7.493" layer="37"/>
<rectangle x1="9.6266" y1="7.4422" x2="10.1854" y2="7.493" layer="37"/>
<rectangle x1="10.8966" y1="7.4422" x2="11.303" y2="7.493" layer="37"/>
<rectangle x1="8.2042" y1="7.4422" x2="8.255" y2="7.493" layer="37"/>
<rectangle x1="6.6802" y1="7.4422" x2="6.731" y2="7.493" layer="37"/>
<rectangle x1="10.8458" y1="7.4422" x2="10.8966" y2="7.493" layer="37"/>
<rectangle x1="7.5438" y1="7.4422" x2="7.5946" y2="7.493" layer="37"/>
<rectangle x1="8.255" y1="7.4422" x2="8.3058" y2="7.493" layer="37"/>
<rectangle x1="2.921" y1="7.493" x2="3.429" y2="7.5438" layer="37"/>
<rectangle x1="4.191" y1="7.493" x2="4.699" y2="7.5438" layer="37"/>
<rectangle x1="6.0198" y1="7.493" x2="6.6294" y2="7.5438" layer="37"/>
<rectangle x1="7.5438" y1="7.493" x2="8.2042" y2="7.5438" layer="37"/>
<rectangle x1="9.6266" y1="7.493" x2="10.1854" y2="7.5438" layer="37"/>
<rectangle x1="10.8966" y1="7.493" x2="11.303" y2="7.5438" layer="37"/>
<rectangle x1="5.969" y1="7.493" x2="6.0198" y2="7.5438" layer="37"/>
<rectangle x1="6.6294" y1="7.493" x2="6.6802" y2="7.5438" layer="37"/>
<rectangle x1="8.2042" y1="7.493" x2="8.255" y2="7.5438" layer="37"/>
<rectangle x1="10.8458" y1="7.493" x2="10.8966" y2="7.5438" layer="37"/>
<rectangle x1="2.921" y1="7.5438" x2="3.429" y2="7.5946" layer="37"/>
<rectangle x1="4.191" y1="7.5438" x2="4.699" y2="7.5946" layer="37"/>
<rectangle x1="5.969" y1="7.5438" x2="6.5786" y2="7.5946" layer="37"/>
<rectangle x1="7.493" y1="7.5438" x2="8.1534" y2="7.5946" layer="37"/>
<rectangle x1="9.6266" y1="7.5438" x2="10.1854" y2="7.5946" layer="37"/>
<rectangle x1="10.8966" y1="7.5438" x2="11.303" y2="7.5946" layer="37"/>
<rectangle x1="5.9182" y1="7.5438" x2="5.969" y2="7.5946" layer="37"/>
<rectangle x1="6.5786" y1="7.5438" x2="6.6294" y2="7.5946" layer="37"/>
<rectangle x1="8.1534" y1="7.5438" x2="8.2042" y2="7.5946" layer="37"/>
<rectangle x1="10.8458" y1="7.5438" x2="10.8966" y2="7.5946" layer="37"/>
<rectangle x1="2.921" y1="7.5946" x2="3.429" y2="7.6454" layer="37"/>
<rectangle x1="4.191" y1="7.5946" x2="4.699" y2="7.6454" layer="37"/>
<rectangle x1="5.9182" y1="7.5946" x2="6.5278" y2="7.6454" layer="37"/>
<rectangle x1="7.4422" y1="7.5946" x2="8.1026" y2="7.6454" layer="37"/>
<rectangle x1="9.6266" y1="7.5946" x2="10.1854" y2="7.6454" layer="37"/>
<rectangle x1="10.8966" y1="7.5946" x2="11.303" y2="7.6454" layer="37"/>
<rectangle x1="6.5278" y1="7.5946" x2="6.5786" y2="7.6454" layer="37"/>
<rectangle x1="8.1026" y1="7.5946" x2="8.1534" y2="7.6454" layer="37"/>
<rectangle x1="5.8674" y1="7.5946" x2="5.9182" y2="7.6454" layer="37"/>
<rectangle x1="10.8458" y1="7.5946" x2="10.8966" y2="7.6454" layer="37"/>
<rectangle x1="2.921" y1="7.6454" x2="3.429" y2="7.6962" layer="37"/>
<rectangle x1="4.191" y1="7.6454" x2="4.699" y2="7.6962" layer="37"/>
<rectangle x1="5.8674" y1="7.6454" x2="6.5278" y2="7.6962" layer="37"/>
<rectangle x1="7.4422" y1="7.6454" x2="8.1026" y2="7.6962" layer="37"/>
<rectangle x1="9.6266" y1="7.6454" x2="10.1854" y2="7.6962" layer="37"/>
<rectangle x1="10.8966" y1="7.6454" x2="11.303" y2="7.6962" layer="37"/>
<rectangle x1="7.3914" y1="7.6454" x2="7.4422" y2="7.6962" layer="37"/>
<rectangle x1="5.8166" y1="7.6454" x2="5.8674" y2="7.6962" layer="37"/>
<rectangle x1="10.8458" y1="7.6454" x2="10.8966" y2="7.6962" layer="37"/>
<rectangle x1="2.921" y1="7.6962" x2="3.429" y2="7.747" layer="37"/>
<rectangle x1="4.191" y1="7.6962" x2="4.699" y2="7.747" layer="37"/>
<rectangle x1="5.8166" y1="7.6962" x2="6.477" y2="7.747" layer="37"/>
<rectangle x1="7.3914" y1="7.6962" x2="8.0518" y2="7.747" layer="37"/>
<rectangle x1="9.6266" y1="7.6962" x2="10.1854" y2="7.747" layer="37"/>
<rectangle x1="10.8966" y1="7.6962" x2="11.303" y2="7.747" layer="37"/>
<rectangle x1="7.3406" y1="7.6962" x2="7.3914" y2="7.747" layer="37"/>
<rectangle x1="6.477" y1="7.6962" x2="6.5278" y2="7.747" layer="37"/>
<rectangle x1="10.8458" y1="7.6962" x2="10.8966" y2="7.747" layer="37"/>
<rectangle x1="5.7658" y1="7.6962" x2="5.8166" y2="7.747" layer="37"/>
<rectangle x1="2.921" y1="7.747" x2="3.429" y2="7.7978" layer="37"/>
<rectangle x1="4.191" y1="7.747" x2="4.699" y2="7.7978" layer="37"/>
<rectangle x1="5.7658" y1="7.747" x2="6.4262" y2="7.7978" layer="37"/>
<rectangle x1="7.3406" y1="7.747" x2="8.001" y2="7.7978" layer="37"/>
<rectangle x1="9.6266" y1="7.747" x2="10.1854" y2="7.7978" layer="37"/>
<rectangle x1="10.8966" y1="7.747" x2="11.303" y2="7.7978" layer="37"/>
<rectangle x1="6.4262" y1="7.747" x2="6.477" y2="7.7978" layer="37"/>
<rectangle x1="10.8458" y1="7.747" x2="10.8966" y2="7.7978" layer="37"/>
<rectangle x1="8.001" y1="7.747" x2="8.0518" y2="7.7978" layer="37"/>
<rectangle x1="2.921" y1="7.7978" x2="3.429" y2="7.8486" layer="37"/>
<rectangle x1="4.191" y1="7.7978" x2="4.699" y2="7.8486" layer="37"/>
<rectangle x1="5.7658" y1="7.7978" x2="6.3754" y2="7.8486" layer="37"/>
<rectangle x1="7.2898" y1="7.7978" x2="7.9502" y2="7.8486" layer="37"/>
<rectangle x1="8.5598" y1="7.7978" x2="8.6106" y2="7.8486" layer="37"/>
<rectangle x1="9.6266" y1="7.7978" x2="10.1854" y2="7.8486" layer="37"/>
<rectangle x1="10.8966" y1="7.7978" x2="11.303" y2="7.8486" layer="37"/>
<rectangle x1="5.715" y1="7.7978" x2="5.7658" y2="7.8486" layer="37"/>
<rectangle x1="6.3754" y1="7.7978" x2="6.4262" y2="7.8486" layer="37"/>
<rectangle x1="8.509" y1="7.7978" x2="8.5598" y2="7.8486" layer="37"/>
<rectangle x1="8.6106" y1="7.7978" x2="8.6614" y2="7.8486" layer="37"/>
<rectangle x1="7.9502" y1="7.7978" x2="8.001" y2="7.8486" layer="37"/>
<rectangle x1="10.8458" y1="7.7978" x2="10.8966" y2="7.8486" layer="37"/>
<rectangle x1="2.921" y1="7.8486" x2="3.429" y2="7.8994" layer="37"/>
<rectangle x1="4.191" y1="7.8486" x2="4.699" y2="7.8994" layer="37"/>
<rectangle x1="5.715" y1="7.8486" x2="6.3246" y2="7.8994" layer="37"/>
<rectangle x1="7.239" y1="7.8486" x2="7.8994" y2="7.8994" layer="37"/>
<rectangle x1="8.4582" y1="7.8486" x2="8.7122" y2="7.8994" layer="37"/>
<rectangle x1="9.6266" y1="7.8486" x2="10.1854" y2="7.8994" layer="37"/>
<rectangle x1="10.8966" y1="7.8486" x2="11.303" y2="7.8994" layer="37"/>
<rectangle x1="6.3246" y1="7.8486" x2="6.3754" y2="7.8994" layer="37"/>
<rectangle x1="8.4074" y1="7.8486" x2="8.4582" y2="7.8994" layer="37"/>
<rectangle x1="5.6642" y1="7.8486" x2="5.715" y2="7.8994" layer="37"/>
<rectangle x1="7.8994" y1="7.8486" x2="7.9502" y2="7.8994" layer="37"/>
<rectangle x1="8.7122" y1="7.8486" x2="8.763" y2="7.8994" layer="37"/>
<rectangle x1="10.8458" y1="7.8486" x2="10.8966" y2="7.8994" layer="37"/>
<rectangle x1="2.921" y1="7.8994" x2="3.429" y2="7.9502" layer="37"/>
<rectangle x1="4.191" y1="7.8994" x2="4.699" y2="7.9502" layer="37"/>
<rectangle x1="5.6642" y1="7.8994" x2="6.2738" y2="7.9502" layer="37"/>
<rectangle x1="7.239" y1="7.8994" x2="7.8486" y2="7.9502" layer="37"/>
<rectangle x1="8.3566" y1="7.8994" x2="8.763" y2="7.9502" layer="37"/>
<rectangle x1="9.6266" y1="7.8994" x2="10.1854" y2="7.9502" layer="37"/>
<rectangle x1="10.8966" y1="7.8994" x2="11.303" y2="7.9502" layer="37"/>
<rectangle x1="6.2738" y1="7.8994" x2="6.3246" y2="7.9502" layer="37"/>
<rectangle x1="7.1882" y1="7.8994" x2="7.239" y2="7.9502" layer="37"/>
<rectangle x1="7.8486" y1="7.8994" x2="7.8994" y2="7.9502" layer="37"/>
<rectangle x1="8.763" y1="7.8994" x2="8.8138" y2="7.9502" layer="37"/>
<rectangle x1="5.6134" y1="7.8994" x2="5.6642" y2="7.9502" layer="37"/>
<rectangle x1="10.8458" y1="7.8994" x2="10.8966" y2="7.9502" layer="37"/>
<rectangle x1="2.921" y1="7.9502" x2="3.429" y2="8.001" layer="37"/>
<rectangle x1="4.191" y1="7.9502" x2="4.699" y2="8.001" layer="37"/>
<rectangle x1="5.6134" y1="7.9502" x2="6.2738" y2="8.001" layer="37"/>
<rectangle x1="7.1882" y1="7.9502" x2="7.8486" y2="8.001" layer="37"/>
<rectangle x1="8.3566" y1="7.9502" x2="8.8138" y2="8.001" layer="37"/>
<rectangle x1="9.6266" y1="7.9502" x2="10.1854" y2="8.001" layer="37"/>
<rectangle x1="10.8966" y1="7.9502" x2="11.303" y2="8.001" layer="37"/>
<rectangle x1="7.1374" y1="7.9502" x2="7.1882" y2="8.001" layer="37"/>
<rectangle x1="8.8138" y1="7.9502" x2="8.8646" y2="8.001" layer="37"/>
<rectangle x1="5.5626" y1="7.9502" x2="5.6134" y2="8.001" layer="37"/>
<rectangle x1="8.3058" y1="7.9502" x2="8.3566" y2="8.001" layer="37"/>
<rectangle x1="10.8458" y1="7.9502" x2="10.8966" y2="8.001" layer="37"/>
<rectangle x1="2.921" y1="8.001" x2="3.429" y2="8.0518" layer="37"/>
<rectangle x1="4.191" y1="8.001" x2="4.699" y2="8.0518" layer="37"/>
<rectangle x1="5.5626" y1="8.001" x2="6.223" y2="8.0518" layer="37"/>
<rectangle x1="7.1374" y1="8.001" x2="7.7978" y2="8.0518" layer="37"/>
<rectangle x1="8.3058" y1="8.001" x2="8.8646" y2="8.0518" layer="37"/>
<rectangle x1="9.6266" y1="8.001" x2="10.1854" y2="8.0518" layer="37"/>
<rectangle x1="10.8966" y1="8.001" x2="11.303" y2="8.0518" layer="37"/>
<rectangle x1="7.0866" y1="8.001" x2="7.1374" y2="8.0518" layer="37"/>
<rectangle x1="6.223" y1="8.001" x2="6.2738" y2="8.0518" layer="37"/>
<rectangle x1="8.8646" y1="8.001" x2="8.9154" y2="8.0518" layer="37"/>
<rectangle x1="10.8458" y1="8.001" x2="10.8966" y2="8.0518" layer="37"/>
<rectangle x1="2.921" y1="8.0518" x2="3.429" y2="8.1026" layer="37"/>
<rectangle x1="4.191" y1="8.0518" x2="4.699" y2="8.1026" layer="37"/>
<rectangle x1="5.5118" y1="8.0518" x2="6.1722" y2="8.1026" layer="37"/>
<rectangle x1="7.1374" y1="8.0518" x2="7.747" y2="8.1026" layer="37"/>
<rectangle x1="8.3058" y1="8.0518" x2="8.9154" y2="8.1026" layer="37"/>
<rectangle x1="9.6266" y1="8.0518" x2="10.1854" y2="8.1026" layer="37"/>
<rectangle x1="10.8966" y1="8.0518" x2="11.303" y2="8.1026" layer="37"/>
<rectangle x1="7.0866" y1="8.0518" x2="7.1374" y2="8.1026" layer="37"/>
<rectangle x1="6.1722" y1="8.0518" x2="6.223" y2="8.1026" layer="37"/>
<rectangle x1="7.747" y1="8.0518" x2="7.7978" y2="8.1026" layer="37"/>
<rectangle x1="10.8458" y1="8.0518" x2="10.8966" y2="8.1026" layer="37"/>
<rectangle x1="2.921" y1="8.1026" x2="3.429" y2="8.1534" layer="37"/>
<rectangle x1="4.191" y1="8.1026" x2="4.699" y2="8.1534" layer="37"/>
<rectangle x1="5.5118" y1="8.1026" x2="6.1214" y2="8.1534" layer="37"/>
<rectangle x1="7.0866" y1="8.1026" x2="7.6962" y2="8.1534" layer="37"/>
<rectangle x1="8.3058" y1="8.1026" x2="8.9154" y2="8.1534" layer="37"/>
<rectangle x1="9.6266" y1="8.1026" x2="10.1854" y2="8.1534" layer="37"/>
<rectangle x1="10.8966" y1="8.1026" x2="11.303" y2="8.1534" layer="37"/>
<rectangle x1="5.461" y1="8.1026" x2="5.5118" y2="8.1534" layer="37"/>
<rectangle x1="7.0358" y1="8.1026" x2="7.0866" y2="8.1534" layer="37"/>
<rectangle x1="8.9154" y1="8.1026" x2="8.9662" y2="8.1534" layer="37"/>
<rectangle x1="6.1214" y1="8.1026" x2="6.1722" y2="8.1534" layer="37"/>
<rectangle x1="7.6962" y1="8.1026" x2="7.747" y2="8.1534" layer="37"/>
<rectangle x1="10.8458" y1="8.1026" x2="10.8966" y2="8.1534" layer="37"/>
<rectangle x1="2.921" y1="8.1534" x2="3.429" y2="8.2042" layer="37"/>
<rectangle x1="4.191" y1="8.1534" x2="4.699" y2="8.2042" layer="37"/>
<rectangle x1="5.461" y1="8.1534" x2="6.0706" y2="8.2042" layer="37"/>
<rectangle x1="7.0358" y1="8.1534" x2="7.6962" y2="8.2042" layer="37"/>
<rectangle x1="8.3566" y1="8.1534" x2="8.9662" y2="8.2042" layer="37"/>
<rectangle x1="9.6266" y1="8.1534" x2="10.1854" y2="8.2042" layer="37"/>
<rectangle x1="10.8966" y1="8.1534" x2="11.303" y2="8.2042" layer="37"/>
<rectangle x1="6.0706" y1="8.1534" x2="6.1214" y2="8.2042" layer="37"/>
<rectangle x1="6.985" y1="8.1534" x2="7.0358" y2="8.2042" layer="37"/>
<rectangle x1="5.4102" y1="8.1534" x2="5.461" y2="8.2042" layer="37"/>
<rectangle x1="8.9662" y1="8.1534" x2="9.017" y2="8.2042" layer="37"/>
<rectangle x1="10.8458" y1="8.1534" x2="10.8966" y2="8.2042" layer="37"/>
<rectangle x1="2.921" y1="8.2042" x2="3.429" y2="8.255" layer="37"/>
<rectangle x1="4.191" y1="8.2042" x2="4.699" y2="8.255" layer="37"/>
<rectangle x1="5.4102" y1="8.2042" x2="6.0706" y2="8.255" layer="37"/>
<rectangle x1="6.985" y1="8.2042" x2="7.6962" y2="8.255" layer="37"/>
<rectangle x1="8.3566" y1="8.2042" x2="9.017" y2="8.255" layer="37"/>
<rectangle x1="9.6266" y1="8.2042" x2="10.1854" y2="8.255" layer="37"/>
<rectangle x1="10.8966" y1="8.2042" x2="11.303" y2="8.255" layer="37"/>
<rectangle x1="7.6962" y1="8.2042" x2="7.747" y2="8.255" layer="37"/>
<rectangle x1="6.9342" y1="8.2042" x2="6.985" y2="8.255" layer="37"/>
<rectangle x1="5.3594" y1="8.2042" x2="5.4102" y2="8.255" layer="37"/>
<rectangle x1="9.017" y1="8.2042" x2="9.0678" y2="8.255" layer="37"/>
<rectangle x1="10.8458" y1="8.2042" x2="10.8966" y2="8.255" layer="37"/>
<rectangle x1="2.921" y1="8.255" x2="3.429" y2="8.3058" layer="37"/>
<rectangle x1="4.191" y1="8.255" x2="4.699" y2="8.3058" layer="37"/>
<rectangle x1="5.3594" y1="8.255" x2="6.0198" y2="8.3058" layer="37"/>
<rectangle x1="6.9342" y1="8.255" x2="7.747" y2="8.3058" layer="37"/>
<rectangle x1="8.4074" y1="8.255" x2="9.0678" y2="8.3058" layer="37"/>
<rectangle x1="9.6266" y1="8.255" x2="10.1854" y2="8.3058" layer="37"/>
<rectangle x1="10.8966" y1="8.255" x2="11.303" y2="8.3058" layer="37"/>
<rectangle x1="6.8834" y1="8.255" x2="6.9342" y2="8.3058" layer="37"/>
<rectangle x1="5.3086" y1="8.255" x2="5.3594" y2="8.3058" layer="37"/>
<rectangle x1="7.747" y1="8.255" x2="7.7978" y2="8.3058" layer="37"/>
<rectangle x1="6.0198" y1="8.255" x2="6.0706" y2="8.3058" layer="37"/>
<rectangle x1="10.8458" y1="8.255" x2="10.8966" y2="8.3058" layer="37"/>
<rectangle x1="2.921" y1="8.3058" x2="3.429" y2="8.3566" layer="37"/>
<rectangle x1="4.191" y1="8.3058" x2="4.699" y2="8.3566" layer="37"/>
<rectangle x1="5.3086" y1="8.3058" x2="5.969" y2="8.3566" layer="37"/>
<rectangle x1="6.8834" y1="8.3058" x2="7.7978" y2="8.3566" layer="37"/>
<rectangle x1="8.4582" y1="8.3058" x2="9.1186" y2="8.3566" layer="37"/>
<rectangle x1="9.6266" y1="8.3058" x2="10.1854" y2="8.3566" layer="37"/>
<rectangle x1="10.8966" y1="8.3058" x2="11.303" y2="8.3566" layer="37"/>
<rectangle x1="5.969" y1="8.3058" x2="6.0198" y2="8.3566" layer="37"/>
<rectangle x1="6.8326" y1="8.3058" x2="6.8834" y2="8.3566" layer="37"/>
<rectangle x1="10.8458" y1="8.3058" x2="10.8966" y2="8.3566" layer="37"/>
<rectangle x1="2.921" y1="8.3566" x2="3.429" y2="8.4074" layer="37"/>
<rectangle x1="4.191" y1="8.3566" x2="4.699" y2="8.4074" layer="37"/>
<rectangle x1="5.2578" y1="8.3566" x2="5.969" y2="8.4074" layer="37"/>
<rectangle x1="6.8834" y1="8.3566" x2="7.7978" y2="8.4074" layer="37"/>
<rectangle x1="8.509" y1="8.3566" x2="9.1186" y2="8.4074" layer="37"/>
<rectangle x1="9.6266" y1="8.3566" x2="10.1854" y2="8.4074" layer="37"/>
<rectangle x1="10.8966" y1="8.3566" x2="11.303" y2="8.4074" layer="37"/>
<rectangle x1="6.8326" y1="8.3566" x2="6.8834" y2="8.4074" layer="37"/>
<rectangle x1="7.7978" y1="8.3566" x2="7.8486" y2="8.4074" layer="37"/>
<rectangle x1="9.1186" y1="8.3566" x2="9.1694" y2="8.4074" layer="37"/>
<rectangle x1="8.4582" y1="8.3566" x2="8.509" y2="8.4074" layer="37"/>
<rectangle x1="10.8458" y1="8.3566" x2="10.8966" y2="8.4074" layer="37"/>
<rectangle x1="2.921" y1="8.4074" x2="3.429" y2="8.4582" layer="37"/>
<rectangle x1="4.191" y1="8.4074" x2="4.699" y2="8.4582" layer="37"/>
<rectangle x1="5.2578" y1="8.4074" x2="5.9182" y2="8.4582" layer="37"/>
<rectangle x1="6.8326" y1="8.4074" x2="7.8486" y2="8.4582" layer="37"/>
<rectangle x1="8.509" y1="8.4074" x2="9.1694" y2="8.4582" layer="37"/>
<rectangle x1="9.6266" y1="8.4074" x2="10.1854" y2="8.4582" layer="37"/>
<rectangle x1="10.8966" y1="8.4074" x2="11.303" y2="8.4582" layer="37"/>
<rectangle x1="5.207" y1="8.4074" x2="5.2578" y2="8.4582" layer="37"/>
<rectangle x1="6.7818" y1="8.4074" x2="6.8326" y2="8.4582" layer="37"/>
<rectangle x1="9.1694" y1="8.4074" x2="9.2202" y2="8.4582" layer="37"/>
<rectangle x1="7.8486" y1="8.4074" x2="7.8994" y2="8.4582" layer="37"/>
<rectangle x1="10.8458" y1="8.4074" x2="10.8966" y2="8.4582" layer="37"/>
<rectangle x1="2.921" y1="8.4582" x2="3.429" y2="8.509" layer="37"/>
<rectangle x1="4.191" y1="8.4582" x2="4.699" y2="8.509" layer="37"/>
<rectangle x1="5.207" y1="8.4582" x2="5.8166" y2="8.509" layer="37"/>
<rectangle x1="6.7818" y1="8.4582" x2="7.8994" y2="8.509" layer="37"/>
<rectangle x1="8.6106" y1="8.4582" x2="9.1694" y2="8.509" layer="37"/>
<rectangle x1="9.6266" y1="8.4582" x2="10.1854" y2="8.509" layer="37"/>
<rectangle x1="10.8966" y1="8.4582" x2="11.303" y2="8.509" layer="37"/>
<rectangle x1="5.1562" y1="8.4582" x2="5.207" y2="8.509" layer="37"/>
<rectangle x1="5.8166" y1="8.4582" x2="5.8674" y2="8.509" layer="37"/>
<rectangle x1="6.731" y1="8.4582" x2="6.7818" y2="8.509" layer="37"/>
<rectangle x1="8.5598" y1="8.4582" x2="8.6106" y2="8.509" layer="37"/>
<rectangle x1="9.1694" y1="8.4582" x2="9.2202" y2="8.509" layer="37"/>
<rectangle x1="9.2202" y1="8.4582" x2="9.271" y2="8.509" layer="37"/>
<rectangle x1="10.8458" y1="8.4582" x2="10.8966" y2="8.509" layer="37"/>
<rectangle x1="2.921" y1="8.509" x2="3.429" y2="8.5598" layer="37"/>
<rectangle x1="4.191" y1="8.509" x2="4.699" y2="8.5598" layer="37"/>
<rectangle x1="5.1562" y1="8.509" x2="5.8166" y2="8.5598" layer="37"/>
<rectangle x1="6.731" y1="8.509" x2="7.9502" y2="8.5598" layer="37"/>
<rectangle x1="8.6106" y1="8.509" x2="9.271" y2="8.5598" layer="37"/>
<rectangle x1="9.6266" y1="8.509" x2="10.1854" y2="8.5598" layer="37"/>
<rectangle x1="10.8966" y1="8.509" x2="11.303" y2="8.5598" layer="37"/>
<rectangle x1="5.1054" y1="8.509" x2="5.1562" y2="8.5598" layer="37"/>
<rectangle x1="6.6802" y1="8.509" x2="6.731" y2="8.5598" layer="37"/>
<rectangle x1="10.8458" y1="8.509" x2="10.8966" y2="8.5598" layer="37"/>
<rectangle x1="2.921" y1="8.5598" x2="3.429" y2="8.6106" layer="37"/>
<rectangle x1="4.191" y1="8.5598" x2="4.699" y2="8.6106" layer="37"/>
<rectangle x1="5.1054" y1="8.5598" x2="5.7658" y2="8.6106" layer="37"/>
<rectangle x1="6.6802" y1="8.5598" x2="7.2898" y2="8.6106" layer="37"/>
<rectangle x1="7.4422" y1="8.5598" x2="8.001" y2="8.6106" layer="37"/>
<rectangle x1="8.6614" y1="8.5598" x2="9.271" y2="8.6106" layer="37"/>
<rectangle x1="9.6266" y1="8.5598" x2="10.1854" y2="8.6106" layer="37"/>
<rectangle x1="10.8966" y1="8.5598" x2="11.303" y2="8.6106" layer="37"/>
<rectangle x1="7.2898" y1="8.5598" x2="7.3406" y2="8.6106" layer="37"/>
<rectangle x1="9.271" y1="8.5598" x2="9.3218" y2="8.6106" layer="37"/>
<rectangle x1="7.3914" y1="8.5598" x2="7.4422" y2="8.6106" layer="37"/>
<rectangle x1="6.6294" y1="8.5598" x2="6.6802" y2="8.6106" layer="37"/>
<rectangle x1="5.0546" y1="8.5598" x2="5.1054" y2="8.6106" layer="37"/>
<rectangle x1="8.6106" y1="8.5598" x2="8.6614" y2="8.6106" layer="37"/>
<rectangle x1="5.7658" y1="8.5598" x2="5.8166" y2="8.6106" layer="37"/>
<rectangle x1="7.3406" y1="8.5598" x2="7.3914" y2="8.6106" layer="37"/>
<rectangle x1="10.8458" y1="8.5598" x2="10.8966" y2="8.6106" layer="37"/>
<rectangle x1="2.921" y1="8.6106" x2="3.429" y2="8.6614" layer="37"/>
<rectangle x1="4.191" y1="8.6106" x2="4.699" y2="8.6614" layer="37"/>
<rectangle x1="5.0546" y1="8.6106" x2="5.715" y2="8.6614" layer="37"/>
<rectangle x1="6.6294" y1="8.6106" x2="7.239" y2="8.6614" layer="37"/>
<rectangle x1="7.4422" y1="8.6106" x2="8.001" y2="8.6614" layer="37"/>
<rectangle x1="8.7122" y1="8.6106" x2="9.3218" y2="8.6614" layer="37"/>
<rectangle x1="9.6266" y1="8.6106" x2="10.1854" y2="8.6614" layer="37"/>
<rectangle x1="10.8966" y1="8.6106" x2="11.303" y2="8.6614" layer="37"/>
<rectangle x1="7.239" y1="8.6106" x2="7.2898" y2="8.6614" layer="37"/>
<rectangle x1="8.6614" y1="8.6106" x2="8.7122" y2="8.6614" layer="37"/>
<rectangle x1="9.3218" y1="8.6106" x2="9.3726" y2="8.6614" layer="37"/>
<rectangle x1="5.715" y1="8.6106" x2="5.7658" y2="8.6614" layer="37"/>
<rectangle x1="8.001" y1="8.6106" x2="8.0518" y2="8.6614" layer="37"/>
<rectangle x1="6.5786" y1="8.6106" x2="6.6294" y2="8.6614" layer="37"/>
<rectangle x1="10.8458" y1="8.6106" x2="10.8966" y2="8.6614" layer="37"/>
<rectangle x1="7.2898" y1="8.6106" x2="7.3406" y2="8.6614" layer="37"/>
<rectangle x1="2.921" y1="8.6614" x2="3.429" y2="8.7122" layer="37"/>
<rectangle x1="4.191" y1="8.6614" x2="4.699" y2="8.7122" layer="37"/>
<rectangle x1="5.0546" y1="8.6614" x2="5.6642" y2="8.7122" layer="37"/>
<rectangle x1="6.5786" y1="8.6614" x2="7.239" y2="8.7122" layer="37"/>
<rectangle x1="7.493" y1="8.6614" x2="8.0518" y2="8.7122" layer="37"/>
<rectangle x1="8.763" y1="8.6614" x2="9.3726" y2="8.7122" layer="37"/>
<rectangle x1="9.6266" y1="8.6614" x2="10.1854" y2="8.7122" layer="37"/>
<rectangle x1="10.8966" y1="8.6614" x2="11.303" y2="8.7122" layer="37"/>
<rectangle x1="5.0038" y1="8.6614" x2="5.0546" y2="8.7122" layer="37"/>
<rectangle x1="8.7122" y1="8.6614" x2="8.763" y2="8.7122" layer="37"/>
<rectangle x1="5.6642" y1="8.6614" x2="5.715" y2="8.7122" layer="37"/>
<rectangle x1="7.239" y1="8.6614" x2="7.2898" y2="8.7122" layer="37"/>
<rectangle x1="9.3726" y1="8.6614" x2="9.4234" y2="8.7122" layer="37"/>
<rectangle x1="10.8458" y1="8.6614" x2="10.8966" y2="8.7122" layer="37"/>
<rectangle x1="2.921" y1="8.7122" x2="3.429" y2="8.763" layer="37"/>
<rectangle x1="4.191" y1="8.7122" x2="4.699" y2="8.763" layer="37"/>
<rectangle x1="5.0038" y1="8.7122" x2="5.6134" y2="8.763" layer="37"/>
<rectangle x1="6.5278" y1="8.7122" x2="7.1882" y2="8.763" layer="37"/>
<rectangle x1="7.5438" y1="8.7122" x2="8.1026" y2="8.763" layer="37"/>
<rectangle x1="8.763" y1="8.7122" x2="9.4234" y2="8.763" layer="37"/>
<rectangle x1="9.6266" y1="8.7122" x2="10.1854" y2="8.763" layer="37"/>
<rectangle x1="10.8966" y1="8.7122" x2="11.303" y2="8.763" layer="37"/>
<rectangle x1="4.953" y1="8.7122" x2="5.0038" y2="8.763" layer="37"/>
<rectangle x1="5.6134" y1="8.7122" x2="5.6642" y2="8.763" layer="37"/>
<rectangle x1="7.1882" y1="8.7122" x2="7.239" y2="8.763" layer="37"/>
<rectangle x1="7.493" y1="8.7122" x2="7.5438" y2="8.763" layer="37"/>
<rectangle x1="10.8458" y1="8.7122" x2="10.8966" y2="8.763" layer="37"/>
<rectangle x1="2.921" y1="8.763" x2="3.429" y2="8.8138" layer="37"/>
<rectangle x1="4.191" y1="8.763" x2="4.699" y2="8.8138" layer="37"/>
<rectangle x1="4.953" y1="8.763" x2="5.6134" y2="8.8138" layer="37"/>
<rectangle x1="6.5278" y1="8.763" x2="7.1374" y2="8.8138" layer="37"/>
<rectangle x1="7.5946" y1="8.763" x2="8.1026" y2="8.8138" layer="37"/>
<rectangle x1="8.8138" y1="8.763" x2="9.4234" y2="8.8138" layer="37"/>
<rectangle x1="9.6266" y1="8.763" x2="10.1854" y2="8.8138" layer="37"/>
<rectangle x1="10.8966" y1="8.763" x2="11.303" y2="8.8138" layer="37"/>
<rectangle x1="6.477" y1="8.763" x2="6.5278" y2="8.8138" layer="37"/>
<rectangle x1="7.1374" y1="8.763" x2="7.1882" y2="8.8138" layer="37"/>
<rectangle x1="7.5438" y1="8.763" x2="7.5946" y2="8.8138" layer="37"/>
<rectangle x1="9.4234" y1="8.763" x2="9.4742" y2="8.8138" layer="37"/>
<rectangle x1="4.9022" y1="8.763" x2="4.953" y2="8.8138" layer="37"/>
<rectangle x1="8.1026" y1="8.763" x2="8.1534" y2="8.8138" layer="37"/>
<rectangle x1="10.8458" y1="8.763" x2="10.8966" y2="8.8138" layer="37"/>
<rectangle x1="8.763" y1="8.763" x2="8.8138" y2="8.8138" layer="37"/>
<rectangle x1="2.921" y1="8.8138" x2="3.429" y2="8.8646" layer="37"/>
<rectangle x1="4.191" y1="8.8138" x2="4.699" y2="8.8646" layer="37"/>
<rectangle x1="4.9022" y1="8.8138" x2="5.5626" y2="8.8646" layer="37"/>
<rectangle x1="6.477" y1="8.8138" x2="7.0866" y2="8.8646" layer="37"/>
<rectangle x1="7.5946" y1="8.8138" x2="8.1534" y2="8.8646" layer="37"/>
<rectangle x1="8.8646" y1="8.8138" x2="9.4742" y2="8.8646" layer="37"/>
<rectangle x1="9.6266" y1="8.8138" x2="10.1854" y2="8.8646" layer="37"/>
<rectangle x1="10.8966" y1="8.8138" x2="11.303" y2="8.8646" layer="37"/>
<rectangle x1="7.0866" y1="8.8138" x2="7.1374" y2="8.8646" layer="37"/>
<rectangle x1="6.4262" y1="8.8138" x2="6.477" y2="8.8646" layer="37"/>
<rectangle x1="9.4742" y1="8.8138" x2="9.525" y2="8.8646" layer="37"/>
<rectangle x1="4.8514" y1="8.8138" x2="4.9022" y2="8.8646" layer="37"/>
<rectangle x1="8.8138" y1="8.8138" x2="8.8646" y2="8.8646" layer="37"/>
<rectangle x1="10.8458" y1="8.8138" x2="10.8966" y2="8.8646" layer="37"/>
<rectangle x1="2.921" y1="8.8646" x2="3.429" y2="8.9154" layer="37"/>
<rectangle x1="4.191" y1="8.8646" x2="4.699" y2="8.9154" layer="37"/>
<rectangle x1="4.8514" y1="8.8646" x2="5.5118" y2="8.9154" layer="37"/>
<rectangle x1="6.4262" y1="8.8646" x2="7.0866" y2="8.9154" layer="37"/>
<rectangle x1="7.6454" y1="8.8646" x2="8.1534" y2="8.9154" layer="37"/>
<rectangle x1="8.9154" y1="8.8646" x2="9.525" y2="8.9154" layer="37"/>
<rectangle x1="9.6266" y1="8.8646" x2="10.1854" y2="8.9154" layer="37"/>
<rectangle x1="10.8966" y1="8.8646" x2="11.303" y2="8.9154" layer="37"/>
<rectangle x1="8.1534" y1="8.8646" x2="8.2042" y2="8.9154" layer="37"/>
<rectangle x1="8.8646" y1="8.8646" x2="8.9154" y2="8.9154" layer="37"/>
<rectangle x1="6.3754" y1="8.8646" x2="6.4262" y2="8.9154" layer="37"/>
<rectangle x1="9.525" y1="8.8646" x2="9.5758" y2="8.9154" layer="37"/>
<rectangle x1="7.5946" y1="8.8646" x2="7.6454" y2="8.9154" layer="37"/>
<rectangle x1="4.8006" y1="8.8646" x2="4.8514" y2="8.9154" layer="37"/>
<rectangle x1="10.8458" y1="8.8646" x2="10.8966" y2="8.9154" layer="37"/>
<rectangle x1="5.5118" y1="8.8646" x2="5.5626" y2="8.9154" layer="37"/>
<rectangle x1="2.921" y1="8.9154" x2="3.429" y2="8.9662" layer="37"/>
<rectangle x1="4.191" y1="8.9154" x2="4.699" y2="8.9662" layer="37"/>
<rectangle x1="4.8006" y1="8.9154" x2="5.461" y2="8.9662" layer="37"/>
<rectangle x1="6.3754" y1="8.9154" x2="7.0358" y2="8.9662" layer="37"/>
<rectangle x1="7.6962" y1="8.9154" x2="8.2042" y2="8.9662" layer="37"/>
<rectangle x1="8.9662" y1="8.9154" x2="10.1854" y2="8.9662" layer="37"/>
<rectangle x1="10.8966" y1="8.9154" x2="11.303" y2="8.9662" layer="37"/>
<rectangle x1="8.9154" y1="8.9154" x2="8.9662" y2="8.9662" layer="37"/>
<rectangle x1="7.6454" y1="8.9154" x2="7.6962" y2="8.9662" layer="37"/>
<rectangle x1="8.2042" y1="8.9154" x2="8.255" y2="8.9662" layer="37"/>
<rectangle x1="5.461" y1="8.9154" x2="5.5118" y2="8.9662" layer="37"/>
<rectangle x1="7.0358" y1="8.9154" x2="7.0866" y2="8.9662" layer="37"/>
<rectangle x1="10.8458" y1="8.9154" x2="10.8966" y2="8.9662" layer="37"/>
<rectangle x1="6.3246" y1="8.9154" x2="6.3754" y2="8.9662" layer="37"/>
<rectangle x1="2.921" y1="8.9662" x2="3.429" y2="9.017" layer="37"/>
<rectangle x1="4.191" y1="8.9662" x2="5.4102" y2="9.017" layer="37"/>
<rectangle x1="6.3246" y1="8.9662" x2="6.985" y2="9.017" layer="37"/>
<rectangle x1="7.6962" y1="8.9662" x2="8.255" y2="9.017" layer="37"/>
<rectangle x1="8.9662" y1="8.9662" x2="10.1854" y2="9.017" layer="37"/>
<rectangle x1="10.8966" y1="8.9662" x2="11.303" y2="9.017" layer="37"/>
<rectangle x1="5.4102" y1="8.9662" x2="5.461" y2="9.017" layer="37"/>
<rectangle x1="6.985" y1="8.9662" x2="7.0358" y2="9.017" layer="37"/>
<rectangle x1="10.8458" y1="8.9662" x2="10.8966" y2="9.017" layer="37"/>
<rectangle x1="8.255" y1="8.9662" x2="8.3058" y2="9.017" layer="37"/>
<rectangle x1="2.921" y1="9.017" x2="3.429" y2="9.0678" layer="37"/>
<rectangle x1="4.191" y1="9.017" x2="5.3594" y2="9.0678" layer="37"/>
<rectangle x1="6.2738" y1="9.017" x2="6.9342" y2="9.0678" layer="37"/>
<rectangle x1="7.747" y1="9.017" x2="8.255" y2="9.0678" layer="37"/>
<rectangle x1="9.017" y1="9.017" x2="10.1854" y2="9.0678" layer="37"/>
<rectangle x1="10.8966" y1="9.017" x2="11.303" y2="9.0678" layer="37"/>
<rectangle x1="8.255" y1="9.017" x2="8.3058" y2="9.0678" layer="37"/>
<rectangle x1="5.3594" y1="9.017" x2="5.4102" y2="9.0678" layer="37"/>
<rectangle x1="6.9342" y1="9.017" x2="6.985" y2="9.0678" layer="37"/>
<rectangle x1="8.9662" y1="9.017" x2="9.017" y2="9.0678" layer="37"/>
<rectangle x1="10.8458" y1="9.017" x2="10.8966" y2="9.0678" layer="37"/>
<rectangle x1="2.921" y1="9.0678" x2="3.429" y2="9.1186" layer="37"/>
<rectangle x1="4.191" y1="9.0678" x2="5.3594" y2="9.1186" layer="37"/>
<rectangle x1="6.2738" y1="9.0678" x2="6.9342" y2="9.1186" layer="37"/>
<rectangle x1="7.7978" y1="9.0678" x2="8.3058" y2="9.1186" layer="37"/>
<rectangle x1="9.0678" y1="9.0678" x2="10.1854" y2="9.1186" layer="37"/>
<rectangle x1="10.8966" y1="9.0678" x2="11.303" y2="9.1186" layer="37"/>
<rectangle x1="6.223" y1="9.0678" x2="6.2738" y2="9.1186" layer="37"/>
<rectangle x1="9.017" y1="9.0678" x2="9.0678" y2="9.1186" layer="37"/>
<rectangle x1="8.3058" y1="9.0678" x2="8.3566" y2="9.1186" layer="37"/>
<rectangle x1="7.747" y1="9.0678" x2="7.7978" y2="9.1186" layer="37"/>
<rectangle x1="10.8458" y1="9.0678" x2="10.8966" y2="9.1186" layer="37"/>
<rectangle x1="2.921" y1="9.1186" x2="3.429" y2="9.1694" layer="37"/>
<rectangle x1="4.191" y1="9.1186" x2="5.3086" y2="9.1694" layer="37"/>
<rectangle x1="6.223" y1="9.1186" x2="6.8834" y2="9.1694" layer="37"/>
<rectangle x1="7.8486" y1="9.1186" x2="8.3566" y2="9.1694" layer="37"/>
<rectangle x1="9.1186" y1="9.1186" x2="10.1854" y2="9.1694" layer="37"/>
<rectangle x1="10.8966" y1="9.1186" x2="11.303" y2="9.1694" layer="37"/>
<rectangle x1="7.7978" y1="9.1186" x2="7.8486" y2="9.1694" layer="37"/>
<rectangle x1="9.0678" y1="9.1186" x2="9.1186" y2="9.1694" layer="37"/>
<rectangle x1="6.1722" y1="9.1186" x2="6.223" y2="9.1694" layer="37"/>
<rectangle x1="8.3566" y1="9.1186" x2="8.4074" y2="9.1694" layer="37"/>
<rectangle x1="10.8458" y1="9.1186" x2="10.8966" y2="9.1694" layer="37"/>
<rectangle x1="2.921" y1="9.1694" x2="3.429" y2="9.2202" layer="37"/>
<rectangle x1="4.191" y1="9.1694" x2="5.2578" y2="9.2202" layer="37"/>
<rectangle x1="6.1722" y1="9.1694" x2="6.8326" y2="9.2202" layer="37"/>
<rectangle x1="7.8486" y1="9.1694" x2="8.3566" y2="9.2202" layer="37"/>
<rectangle x1="9.1186" y1="9.1694" x2="10.1854" y2="9.2202" layer="37"/>
<rectangle x1="10.8966" y1="9.1694" x2="11.303" y2="9.2202" layer="37"/>
<rectangle x1="8.3566" y1="9.1694" x2="8.4074" y2="9.2202" layer="37"/>
<rectangle x1="6.1214" y1="9.1694" x2="6.1722" y2="9.2202" layer="37"/>
<rectangle x1="5.2578" y1="9.1694" x2="5.3086" y2="9.2202" layer="37"/>
<rectangle x1="10.8458" y1="9.1694" x2="10.8966" y2="9.2202" layer="37"/>
<rectangle x1="2.921" y1="9.2202" x2="3.429" y2="9.271" layer="37"/>
<rectangle x1="4.191" y1="9.2202" x2="5.207" y2="9.271" layer="37"/>
<rectangle x1="6.1214" y1="9.2202" x2="6.7818" y2="9.271" layer="37"/>
<rectangle x1="7.8994" y1="9.2202" x2="8.4074" y2="9.271" layer="37"/>
<rectangle x1="9.1694" y1="9.2202" x2="10.1854" y2="9.271" layer="37"/>
<rectangle x1="10.8966" y1="9.2202" x2="11.303" y2="9.271" layer="37"/>
<rectangle x1="8.4074" y1="9.2202" x2="8.4582" y2="9.271" layer="37"/>
<rectangle x1="5.207" y1="9.2202" x2="5.2578" y2="9.271" layer="37"/>
<rectangle x1="7.8486" y1="9.2202" x2="7.8994" y2="9.271" layer="37"/>
<rectangle x1="9.1186" y1="9.2202" x2="9.1694" y2="9.271" layer="37"/>
<rectangle x1="6.0706" y1="9.2202" x2="6.1214" y2="9.271" layer="37"/>
<rectangle x1="6.7818" y1="9.2202" x2="6.8326" y2="9.271" layer="37"/>
<rectangle x1="10.8458" y1="9.2202" x2="10.8966" y2="9.271" layer="37"/>
<rectangle x1="2.921" y1="9.271" x2="3.429" y2="9.3218" layer="37"/>
<rectangle x1="4.191" y1="9.271" x2="5.1562" y2="9.3218" layer="37"/>
<rectangle x1="6.0706" y1="9.271" x2="6.731" y2="9.3218" layer="37"/>
<rectangle x1="7.9502" y1="9.271" x2="8.4582" y2="9.3218" layer="37"/>
<rectangle x1="9.2202" y1="9.271" x2="10.1854" y2="9.3218" layer="37"/>
<rectangle x1="10.8966" y1="9.271" x2="11.303" y2="9.3218" layer="37"/>
<rectangle x1="9.1694" y1="9.271" x2="9.2202" y2="9.3218" layer="37"/>
<rectangle x1="5.1562" y1="9.271" x2="5.207" y2="9.3218" layer="37"/>
<rectangle x1="7.8994" y1="9.271" x2="7.9502" y2="9.3218" layer="37"/>
<rectangle x1="6.731" y1="9.271" x2="6.7818" y2="9.3218" layer="37"/>
<rectangle x1="8.4582" y1="9.271" x2="8.509" y2="9.3218" layer="37"/>
<rectangle x1="10.8458" y1="9.271" x2="10.8966" y2="9.3218" layer="37"/>
<rectangle x1="2.921" y1="9.3218" x2="3.429" y2="9.3726" layer="37"/>
<rectangle x1="4.191" y1="9.3218" x2="5.1054" y2="9.3726" layer="37"/>
<rectangle x1="6.0706" y1="9.3218" x2="6.6802" y2="9.3726" layer="37"/>
<rectangle x1="7.9502" y1="9.3218" x2="8.509" y2="9.3726" layer="37"/>
<rectangle x1="9.2202" y1="9.3218" x2="10.1854" y2="9.3726" layer="37"/>
<rectangle x1="10.8966" y1="9.3218" x2="11.303" y2="9.3726" layer="37"/>
<rectangle x1="5.1054" y1="9.3218" x2="5.1562" y2="9.3726" layer="37"/>
<rectangle x1="6.0198" y1="9.3218" x2="6.0706" y2="9.3726" layer="37"/>
<rectangle x1="6.6802" y1="9.3218" x2="6.731" y2="9.3726" layer="37"/>
<rectangle x1="10.8458" y1="9.3218" x2="10.8966" y2="9.3726" layer="37"/>
<rectangle x1="2.921" y1="9.3726" x2="3.429" y2="9.4234" layer="37"/>
<rectangle x1="4.191" y1="9.3726" x2="5.1054" y2="9.4234" layer="37"/>
<rectangle x1="6.0198" y1="9.3726" x2="6.6802" y2="9.4234" layer="37"/>
<rectangle x1="8.001" y1="9.3726" x2="8.509" y2="9.4234" layer="37"/>
<rectangle x1="9.271" y1="9.3726" x2="10.1854" y2="9.4234" layer="37"/>
<rectangle x1="10.8966" y1="9.3726" x2="11.303" y2="9.4234" layer="37"/>
<rectangle x1="8.509" y1="9.3726" x2="8.5598" y2="9.4234" layer="37"/>
<rectangle x1="5.969" y1="9.3726" x2="6.0198" y2="9.4234" layer="37"/>
<rectangle x1="7.9502" y1="9.3726" x2="8.001" y2="9.4234" layer="37"/>
<rectangle x1="10.8458" y1="9.3726" x2="10.8966" y2="9.4234" layer="37"/>
<rectangle x1="2.921" y1="9.4234" x2="3.429" y2="9.4742" layer="37"/>
<rectangle x1="4.191" y1="9.4234" x2="5.0546" y2="9.4742" layer="37"/>
<rectangle x1="5.969" y1="9.4234" x2="6.6294" y2="9.4742" layer="37"/>
<rectangle x1="8.0518" y1="9.4234" x2="8.5598" y2="9.4742" layer="37"/>
<rectangle x1="9.3218" y1="9.4234" x2="10.1854" y2="9.4742" layer="37"/>
<rectangle x1="10.8966" y1="9.4234" x2="11.303" y2="9.4742" layer="37"/>
<rectangle x1="8.001" y1="9.4234" x2="8.0518" y2="9.4742" layer="37"/>
<rectangle x1="8.5598" y1="9.4234" x2="8.6106" y2="9.4742" layer="37"/>
<rectangle x1="5.9182" y1="9.4234" x2="5.969" y2="9.4742" layer="37"/>
<rectangle x1="9.271" y1="9.4234" x2="9.3218" y2="9.4742" layer="37"/>
<rectangle x1="10.8458" y1="9.4234" x2="10.8966" y2="9.4742" layer="37"/>
<rectangle x1="5.0546" y1="9.4234" x2="5.1054" y2="9.4742" layer="37"/>
<rectangle x1="2.921" y1="9.4742" x2="3.429" y2="9.525" layer="37"/>
<rectangle x1="4.191" y1="9.4742" x2="5.0038" y2="9.525" layer="37"/>
<rectangle x1="5.9182" y1="9.4742" x2="6.5786" y2="9.525" layer="37"/>
<rectangle x1="8.1026" y1="9.4742" x2="8.6106" y2="9.525" layer="37"/>
<rectangle x1="9.3726" y1="9.4742" x2="10.1854" y2="9.525" layer="37"/>
<rectangle x1="10.8966" y1="9.4742" x2="11.303" y2="9.525" layer="37"/>
<rectangle x1="8.0518" y1="9.4742" x2="8.1026" y2="9.525" layer="37"/>
<rectangle x1="9.3218" y1="9.4742" x2="9.3726" y2="9.525" layer="37"/>
<rectangle x1="5.0038" y1="9.4742" x2="5.0546" y2="9.525" layer="37"/>
<rectangle x1="5.8674" y1="9.4742" x2="5.9182" y2="9.525" layer="37"/>
<rectangle x1="8.6106" y1="9.4742" x2="8.6614" y2="9.525" layer="37"/>
<rectangle x1="10.8458" y1="9.4742" x2="10.8966" y2="9.525" layer="37"/>
<rectangle x1="2.921" y1="9.525" x2="3.429" y2="9.5758" layer="37"/>
<rectangle x1="4.191" y1="9.525" x2="4.953" y2="9.5758" layer="37"/>
<rectangle x1="5.8674" y1="9.525" x2="6.5278" y2="9.5758" layer="37"/>
<rectangle x1="8.1534" y1="9.525" x2="8.6614" y2="9.5758" layer="37"/>
<rectangle x1="9.4234" y1="9.525" x2="10.1854" y2="9.5758" layer="37"/>
<rectangle x1="10.8966" y1="9.525" x2="11.303" y2="9.5758" layer="37"/>
<rectangle x1="8.1026" y1="9.525" x2="8.1534" y2="9.5758" layer="37"/>
<rectangle x1="9.3726" y1="9.525" x2="9.4234" y2="9.5758" layer="37"/>
<rectangle x1="4.953" y1="9.525" x2="5.0038" y2="9.5758" layer="37"/>
<rectangle x1="6.5278" y1="9.525" x2="6.5786" y2="9.5758" layer="37"/>
<rectangle x1="10.8458" y1="9.525" x2="10.8966" y2="9.5758" layer="37"/>
<rectangle x1="5.8166" y1="9.525" x2="5.8674" y2="9.5758" layer="37"/>
<rectangle x1="2.921" y1="9.5758" x2="3.429" y2="9.6266" layer="37"/>
<rectangle x1="4.191" y1="9.5758" x2="4.9022" y2="9.6266" layer="37"/>
<rectangle x1="5.8166" y1="9.5758" x2="6.477" y2="9.6266" layer="37"/>
<rectangle x1="8.1534" y1="9.5758" x2="8.7122" y2="9.6266" layer="37"/>
<rectangle x1="9.4234" y1="9.5758" x2="10.1854" y2="9.6266" layer="37"/>
<rectangle x1="10.8966" y1="9.5758" x2="11.303" y2="9.6266" layer="37"/>
<rectangle x1="4.9022" y1="9.5758" x2="4.953" y2="9.6266" layer="37"/>
<rectangle x1="8.1026" y1="9.5758" x2="8.1534" y2="9.6266" layer="37"/>
<rectangle x1="6.477" y1="9.5758" x2="6.5278" y2="9.6266" layer="37"/>
<rectangle x1="10.8458" y1="9.5758" x2="10.8966" y2="9.6266" layer="37"/>
<rectangle x1="2.921" y1="9.6266" x2="3.429" y2="9.6774" layer="37"/>
<rectangle x1="4.191" y1="9.6266" x2="4.9022" y2="9.6774" layer="37"/>
<rectangle x1="5.8166" y1="9.6266" x2="6.4262" y2="9.6774" layer="37"/>
<rectangle x1="8.2042" y1="9.6266" x2="8.7122" y2="9.6774" layer="37"/>
<rectangle x1="9.4742" y1="9.6266" x2="10.1854" y2="9.6774" layer="37"/>
<rectangle x1="10.8966" y1="9.6266" x2="11.303" y2="9.6774" layer="37"/>
<rectangle x1="5.7658" y1="9.6266" x2="5.8166" y2="9.6774" layer="37"/>
<rectangle x1="8.1534" y1="9.6266" x2="8.2042" y2="9.6774" layer="37"/>
<rectangle x1="6.4262" y1="9.6266" x2="6.477" y2="9.6774" layer="37"/>
<rectangle x1="8.7122" y1="9.6266" x2="8.763" y2="9.6774" layer="37"/>
<rectangle x1="10.8458" y1="9.6266" x2="10.8966" y2="9.6774" layer="37"/>
<rectangle x1="2.921" y1="9.6774" x2="3.429" y2="9.7282" layer="37"/>
<rectangle x1="4.191" y1="9.6774" x2="4.8514" y2="9.7282" layer="37"/>
<rectangle x1="5.7658" y1="9.6774" x2="6.3754" y2="9.7282" layer="37"/>
<rectangle x1="8.2042" y1="9.6774" x2="8.763" y2="9.7282" layer="37"/>
<rectangle x1="9.525" y1="9.6774" x2="10.1854" y2="9.7282" layer="37"/>
<rectangle x1="10.8966" y1="9.6774" x2="11.303" y2="9.7282" layer="37"/>
<rectangle x1="6.3754" y1="9.6774" x2="6.4262" y2="9.7282" layer="37"/>
<rectangle x1="5.715" y1="9.6774" x2="5.7658" y2="9.7282" layer="37"/>
<rectangle x1="9.4742" y1="9.6774" x2="9.525" y2="9.7282" layer="37"/>
<rectangle x1="10.8458" y1="9.6774" x2="10.8966" y2="9.7282" layer="37"/>
<rectangle x1="2.921" y1="9.7282" x2="3.429" y2="9.779" layer="37"/>
<rectangle x1="4.191" y1="9.7282" x2="4.8006" y2="9.779" layer="37"/>
<rectangle x1="5.715" y1="9.7282" x2="6.3246" y2="9.779" layer="37"/>
<rectangle x1="8.255" y1="9.7282" x2="8.8138" y2="9.779" layer="37"/>
<rectangle x1="9.5758" y1="9.7282" x2="10.1854" y2="9.779" layer="37"/>
<rectangle x1="10.8966" y1="9.7282" x2="11.303" y2="9.779" layer="37"/>
<rectangle x1="6.3246" y1="9.7282" x2="6.3754" y2="9.779" layer="37"/>
<rectangle x1="9.525" y1="9.7282" x2="9.5758" y2="9.779" layer="37"/>
<rectangle x1="5.6642" y1="9.7282" x2="5.715" y2="9.779" layer="37"/>
<rectangle x1="8.2042" y1="9.7282" x2="8.255" y2="9.779" layer="37"/>
<rectangle x1="10.8458" y1="9.7282" x2="10.8966" y2="9.779" layer="37"/>
<rectangle x1="4.8006" y1="9.7282" x2="4.8514" y2="9.779" layer="37"/>
<rectangle x1="2.921" y1="9.779" x2="3.429" y2="9.8298" layer="37"/>
<rectangle x1="4.191" y1="9.779" x2="4.7498" y2="9.8298" layer="37"/>
<rectangle x1="5.6642" y1="9.779" x2="6.3246" y2="9.8298" layer="37"/>
<rectangle x1="8.3058" y1="9.779" x2="8.8138" y2="9.8298" layer="37"/>
<rectangle x1="9.6266" y1="9.779" x2="10.1346" y2="9.8298" layer="37"/>
<rectangle x1="10.8966" y1="9.779" x2="11.303" y2="9.8298" layer="37"/>
<rectangle x1="9.5758" y1="9.779" x2="9.6266" y2="9.8298" layer="37"/>
<rectangle x1="8.255" y1="9.779" x2="8.3058" y2="9.8298" layer="37"/>
<rectangle x1="10.1346" y1="9.779" x2="10.1854" y2="9.8298" layer="37"/>
<rectangle x1="8.8138" y1="9.779" x2="8.8646" y2="9.8298" layer="37"/>
<rectangle x1="4.7498" y1="9.779" x2="4.8006" y2="9.8298" layer="37"/>
<rectangle x1="5.6134" y1="9.779" x2="5.6642" y2="9.8298" layer="37"/>
<rectangle x1="10.8458" y1="9.779" x2="10.8966" y2="9.8298" layer="37"/>
<rectangle x1="2.921" y1="9.8298" x2="3.429" y2="9.8806" layer="37"/>
<rectangle x1="4.191" y1="9.8298" x2="4.699" y2="9.8806" layer="37"/>
<rectangle x1="5.6134" y1="9.8298" x2="6.2738" y2="9.8806" layer="37"/>
<rectangle x1="8.3058" y1="9.8298" x2="8.8646" y2="9.8806" layer="37"/>
<rectangle x1="9.6266" y1="9.8298" x2="10.1346" y2="9.8806" layer="37"/>
<rectangle x1="10.8966" y1="9.8298" x2="11.303" y2="9.8806" layer="37"/>
<rectangle x1="4.699" y1="9.8298" x2="4.7498" y2="9.8806" layer="37"/>
<rectangle x1="6.2738" y1="9.8298" x2="6.3246" y2="9.8806" layer="37"/>
<rectangle x1="10.8458" y1="9.8298" x2="10.8966" y2="9.8806" layer="37"/>
<rectangle x1="8.8646" y1="9.8298" x2="8.9154" y2="9.8806" layer="37"/>
<rectangle x1="2.921" y1="9.8806" x2="3.429" y2="9.9314" layer="37"/>
<rectangle x1="4.2418" y1="9.8806" x2="4.6482" y2="9.9314" layer="37"/>
<rectangle x1="5.5626" y1="9.8806" x2="6.223" y2="9.9314" layer="37"/>
<rectangle x1="8.3566" y1="9.8806" x2="8.9154" y2="9.9314" layer="37"/>
<rectangle x1="9.6774" y1="9.8806" x2="10.1346" y2="9.9314" layer="37"/>
<rectangle x1="10.8966" y1="9.8806" x2="11.303" y2="9.9314" layer="37"/>
<rectangle x1="4.6482" y1="9.8806" x2="4.699" y2="9.9314" layer="37"/>
<rectangle x1="4.191" y1="9.8806" x2="4.2418" y2="9.9314" layer="37"/>
<rectangle x1="6.223" y1="9.8806" x2="6.2738" y2="9.9314" layer="37"/>
<rectangle x1="9.6266" y1="9.8806" x2="9.6774" y2="9.9314" layer="37"/>
<rectangle x1="10.8458" y1="9.8806" x2="10.8966" y2="9.9314" layer="37"/>
<rectangle x1="2.921" y1="9.9314" x2="3.429" y2="9.9822" layer="37"/>
<rectangle x1="4.2418" y1="9.9314" x2="4.5974" y2="9.9822" layer="37"/>
<rectangle x1="5.5626" y1="9.9314" x2="6.1722" y2="9.9822" layer="37"/>
<rectangle x1="8.4074" y1="9.9314" x2="8.9154" y2="9.9822" layer="37"/>
<rectangle x1="9.7282" y1="9.9314" x2="10.0838" y2="9.9822" layer="37"/>
<rectangle x1="10.8966" y1="9.9314" x2="11.303" y2="9.9822" layer="37"/>
<rectangle x1="4.5974" y1="9.9314" x2="4.6482" y2="9.9822" layer="37"/>
<rectangle x1="5.5118" y1="9.9314" x2="5.5626" y2="9.9822" layer="37"/>
<rectangle x1="8.9154" y1="9.9314" x2="8.9662" y2="9.9822" layer="37"/>
<rectangle x1="6.1722" y1="9.9314" x2="6.223" y2="9.9822" layer="37"/>
<rectangle x1="8.3566" y1="9.9314" x2="8.4074" y2="9.9822" layer="37"/>
<rectangle x1="9.6774" y1="9.9314" x2="9.7282" y2="9.9822" layer="37"/>
<rectangle x1="10.8458" y1="9.9314" x2="10.8966" y2="9.9822" layer="37"/>
<rectangle x1="10.0838" y1="9.9314" x2="10.1346" y2="9.9822" layer="37"/>
<rectangle x1="2.921" y1="9.9822" x2="3.429" y2="10.033" layer="37"/>
<rectangle x1="4.3434" y1="9.9822" x2="4.5466" y2="10.033" layer="37"/>
<rectangle x1="5.5118" y1="9.9822" x2="6.1214" y2="10.033" layer="37"/>
<rectangle x1="8.4074" y1="9.9822" x2="8.9662" y2="10.033" layer="37"/>
<rectangle x1="9.779" y1="9.9822" x2="9.9822" y2="10.033" layer="37"/>
<rectangle x1="10.8966" y1="9.9822" x2="11.303" y2="10.033" layer="37"/>
<rectangle x1="6.1214" y1="9.9822" x2="6.1722" y2="10.033" layer="37"/>
<rectangle x1="9.9822" y1="9.9822" x2="10.033" y2="10.033" layer="37"/>
<rectangle x1="4.5466" y1="9.9822" x2="4.5974" y2="10.033" layer="37"/>
<rectangle x1="4.2926" y1="9.9822" x2="4.3434" y2="10.033" layer="37"/>
<rectangle x1="5.461" y1="9.9822" x2="5.5118" y2="10.033" layer="37"/>
<rectangle x1="8.9662" y1="9.9822" x2="9.017" y2="10.033" layer="37"/>
<rectangle x1="10.8458" y1="9.9822" x2="10.8966" y2="10.033" layer="37"/>
<rectangle x1="9.7282" y1="9.9822" x2="9.779" y2="10.033" layer="37"/>
<rectangle x1="2.921" y1="10.033" x2="3.429" y2="10.0838" layer="37"/>
<rectangle x1="5.461" y1="10.033" x2="6.0706" y2="10.0838" layer="37"/>
<rectangle x1="8.4582" y1="10.033" x2="9.017" y2="10.0838" layer="37"/>
<rectangle x1="10.8966" y1="10.033" x2="11.303" y2="10.0838" layer="37"/>
<rectangle x1="6.0706" y1="10.033" x2="6.1214" y2="10.0838" layer="37"/>
<rectangle x1="5.4102" y1="10.033" x2="5.461" y2="10.0838" layer="37"/>
<rectangle x1="10.8458" y1="10.033" x2="10.8966" y2="10.0838" layer="37"/>
<rectangle x1="2.921" y1="10.0838" x2="3.429" y2="10.1346" layer="37"/>
<rectangle x1="5.4102" y1="10.0838" x2="6.0706" y2="10.1346" layer="37"/>
<rectangle x1="8.509" y1="10.0838" x2="9.0678" y2="10.1346" layer="37"/>
<rectangle x1="10.8966" y1="10.0838" x2="11.303" y2="10.1346" layer="37"/>
<rectangle x1="8.4582" y1="10.0838" x2="8.509" y2="10.1346" layer="37"/>
<rectangle x1="10.8458" y1="10.0838" x2="10.8966" y2="10.1346" layer="37"/>
<rectangle x1="6.0706" y1="10.0838" x2="6.1214" y2="10.1346" layer="37"/>
<rectangle x1="2.921" y1="10.1346" x2="3.429" y2="10.1854" layer="37"/>
<rectangle x1="5.3594" y1="10.1346" x2="6.0198" y2="10.1854" layer="37"/>
<rectangle x1="8.5598" y1="10.1346" x2="9.0678" y2="10.1854" layer="37"/>
<rectangle x1="10.8966" y1="10.1346" x2="11.303" y2="10.1854" layer="37"/>
<rectangle x1="8.509" y1="10.1346" x2="8.5598" y2="10.1854" layer="37"/>
<rectangle x1="9.0678" y1="10.1346" x2="9.1186" y2="10.1854" layer="37"/>
<rectangle x1="6.0198" y1="10.1346" x2="6.0706" y2="10.1854" layer="37"/>
<rectangle x1="10.8458" y1="10.1346" x2="10.8966" y2="10.1854" layer="37"/>
<rectangle x1="2.921" y1="10.1854" x2="3.429" y2="10.2362" layer="37"/>
<rectangle x1="5.3086" y1="10.1854" x2="5.969" y2="10.2362" layer="37"/>
<rectangle x1="8.5598" y1="10.1854" x2="9.1186" y2="10.2362" layer="37"/>
<rectangle x1="10.8966" y1="10.1854" x2="11.303" y2="10.2362" layer="37"/>
<rectangle x1="5.969" y1="10.1854" x2="6.0198" y2="10.2362" layer="37"/>
<rectangle x1="10.8458" y1="10.1854" x2="10.8966" y2="10.2362" layer="37"/>
<rectangle x1="9.1186" y1="10.1854" x2="9.1694" y2="10.2362" layer="37"/>
<rectangle x1="2.921" y1="10.2362" x2="3.429" y2="10.287" layer="37"/>
<rectangle x1="5.3086" y1="10.2362" x2="5.969" y2="10.287" layer="37"/>
<rectangle x1="8.6106" y1="10.2362" x2="9.1694" y2="10.287" layer="37"/>
<rectangle x1="10.8966" y1="10.2362" x2="11.303" y2="10.287" layer="37"/>
<rectangle x1="5.2578" y1="10.2362" x2="5.3086" y2="10.287" layer="37"/>
<rectangle x1="10.8458" y1="10.2362" x2="10.8966" y2="10.287" layer="37"/>
<rectangle x1="2.921" y1="10.287" x2="3.429" y2="10.3378" layer="37"/>
<rectangle x1="5.2578" y1="10.287" x2="5.9182" y2="10.3378" layer="37"/>
<rectangle x1="8.6614" y1="10.287" x2="9.1694" y2="10.3378" layer="37"/>
<rectangle x1="10.8966" y1="10.287" x2="11.303" y2="10.3378" layer="37"/>
<rectangle x1="9.1694" y1="10.287" x2="9.2202" y2="10.3378" layer="37"/>
<rectangle x1="8.6106" y1="10.287" x2="8.6614" y2="10.3378" layer="37"/>
<rectangle x1="5.207" y1="10.287" x2="5.2578" y2="10.3378" layer="37"/>
<rectangle x1="10.8458" y1="10.287" x2="10.8966" y2="10.3378" layer="37"/>
<rectangle x1="2.921" y1="10.3378" x2="3.429" y2="10.3886" layer="37"/>
<rectangle x1="5.207" y1="10.3378" x2="5.8674" y2="10.3886" layer="37"/>
<rectangle x1="8.6614" y1="10.3378" x2="9.2202" y2="10.3886" layer="37"/>
<rectangle x1="10.8966" y1="10.3378" x2="11.303" y2="10.3886" layer="37"/>
<rectangle x1="9.2202" y1="10.3378" x2="9.271" y2="10.3886" layer="37"/>
<rectangle x1="5.1562" y1="10.3378" x2="5.207" y2="10.3886" layer="37"/>
<rectangle x1="10.8458" y1="10.3378" x2="10.8966" y2="10.3886" layer="37"/>
<rectangle x1="2.921" y1="10.3886" x2="3.429" y2="10.4394" layer="37"/>
<rectangle x1="5.1562" y1="10.3886" x2="5.8166" y2="10.4394" layer="37"/>
<rectangle x1="8.7122" y1="10.3886" x2="9.2202" y2="10.4394" layer="37"/>
<rectangle x1="10.8966" y1="10.3886" x2="11.303" y2="10.4394" layer="37"/>
<rectangle x1="9.2202" y1="10.3886" x2="9.271" y2="10.4394" layer="37"/>
<rectangle x1="5.8166" y1="10.3886" x2="5.8674" y2="10.4394" layer="37"/>
<rectangle x1="10.8458" y1="10.3886" x2="10.8966" y2="10.4394" layer="37"/>
<rectangle x1="2.921" y1="10.4394" x2="3.429" y2="10.4902" layer="37"/>
<rectangle x1="5.1054" y1="10.4394" x2="5.7658" y2="10.4902" layer="37"/>
<rectangle x1="8.763" y1="10.4394" x2="9.271" y2="10.4902" layer="37"/>
<rectangle x1="10.8966" y1="10.4394" x2="11.303" y2="10.4902" layer="37"/>
<rectangle x1="9.271" y1="10.4394" x2="9.3218" y2="10.4902" layer="37"/>
<rectangle x1="5.7658" y1="10.4394" x2="5.8166" y2="10.4902" layer="37"/>
<rectangle x1="8.7122" y1="10.4394" x2="8.763" y2="10.4902" layer="37"/>
<rectangle x1="10.8458" y1="10.4394" x2="10.8966" y2="10.4902" layer="37"/>
<rectangle x1="2.921" y1="10.4902" x2="3.429" y2="10.541" layer="37"/>
<rectangle x1="5.1054" y1="10.4902" x2="5.715" y2="10.541" layer="37"/>
<rectangle x1="8.8138" y1="10.4902" x2="9.3218" y2="10.541" layer="37"/>
<rectangle x1="10.8966" y1="10.4902" x2="11.303" y2="10.541" layer="37"/>
<rectangle x1="5.0546" y1="10.4902" x2="5.1054" y2="10.541" layer="37"/>
<rectangle x1="8.763" y1="10.4902" x2="8.8138" y2="10.541" layer="37"/>
<rectangle x1="5.715" y1="10.4902" x2="5.7658" y2="10.541" layer="37"/>
<rectangle x1="9.3218" y1="10.4902" x2="9.3726" y2="10.541" layer="37"/>
<rectangle x1="10.8458" y1="10.4902" x2="10.8966" y2="10.541" layer="37"/>
<rectangle x1="2.921" y1="10.541" x2="3.429" y2="10.5918" layer="37"/>
<rectangle x1="5.0546" y1="10.541" x2="5.6642" y2="10.5918" layer="37"/>
<rectangle x1="8.8646" y1="10.541" x2="9.3218" y2="10.5918" layer="37"/>
<rectangle x1="10.8966" y1="10.541" x2="11.303" y2="10.5918" layer="37"/>
<rectangle x1="5.0038" y1="10.541" x2="5.0546" y2="10.5918" layer="37"/>
<rectangle x1="5.6642" y1="10.541" x2="5.715" y2="10.5918" layer="37"/>
<rectangle x1="8.8138" y1="10.541" x2="8.8646" y2="10.5918" layer="37"/>
<rectangle x1="9.3218" y1="10.541" x2="9.3726" y2="10.5918" layer="37"/>
<rectangle x1="10.8458" y1="10.541" x2="10.8966" y2="10.5918" layer="37"/>
<rectangle x1="2.921" y1="10.5918" x2="3.429" y2="10.6426" layer="37"/>
<rectangle x1="4.953" y1="10.5918" x2="5.6134" y2="10.6426" layer="37"/>
<rectangle x1="8.8646" y1="10.5918" x2="9.4234" y2="10.6426" layer="37"/>
<rectangle x1="10.8966" y1="10.5918" x2="11.303" y2="10.6426" layer="37"/>
<rectangle x1="5.6134" y1="10.5918" x2="5.6642" y2="10.6426" layer="37"/>
<rectangle x1="8.8138" y1="10.5918" x2="8.8646" y2="10.6426" layer="37"/>
<rectangle x1="4.9022" y1="10.5918" x2="4.953" y2="10.6426" layer="37"/>
<rectangle x1="10.8458" y1="10.5918" x2="10.8966" y2="10.6426" layer="37"/>
<rectangle x1="2.921" y1="10.6426" x2="3.429" y2="10.6934" layer="37"/>
<rectangle x1="4.1402" y1="10.6426" x2="4.191" y2="10.6934" layer="37"/>
<rectangle x1="4.8514" y1="10.6426" x2="5.6134" y2="10.6934" layer="37"/>
<rectangle x1="8.9154" y1="10.6426" x2="9.525" y2="10.6934" layer="37"/>
<rectangle x1="10.8966" y1="10.6426" x2="11.303" y2="10.6934" layer="37"/>
<rectangle x1="4.8006" y1="10.6426" x2="4.8514" y2="10.6934" layer="37"/>
<rectangle x1="10.8458" y1="10.6426" x2="10.8966" y2="10.6934" layer="37"/>
<rectangle x1="4.191" y1="10.6426" x2="4.2418" y2="10.6934" layer="37"/>
<rectangle x1="3.429" y1="10.6426" x2="3.4798" y2="10.6934" layer="37"/>
<rectangle x1="8.8646" y1="10.6426" x2="8.9154" y2="10.6934" layer="37"/>
<rectangle x1="4.0894" y1="10.6426" x2="4.1402" y2="10.6934" layer="37"/>
<rectangle x1="9.525" y1="10.6426" x2="9.5758" y2="10.6934" layer="37"/>
<rectangle x1="4.7498" y1="10.6426" x2="4.8006" y2="10.6934" layer="37"/>
<rectangle x1="10.795" y1="10.6426" x2="10.8458" y2="10.6934" layer="37"/>
<rectangle x1="3.4798" y1="10.6426" x2="3.5306" y2="10.6934" layer="37"/>
<rectangle x1="3.5306" y1="10.6426" x2="4.0894" y2="10.6934" layer="37"/>
<rectangle x1="4.2418" y1="10.6426" x2="4.6482" y2="10.6934" layer="37"/>
<rectangle x1="9.5758" y1="10.6426" x2="10.795" y2="10.6934" layer="37"/>
<rectangle x1="2.921" y1="10.6934" x2="5.5626" y2="10.7442" layer="37"/>
<rectangle x1="8.9662" y1="10.6934" x2="11.303" y2="10.7442" layer="37"/>
<rectangle x1="8.9154" y1="10.6934" x2="8.9662" y2="10.7442" layer="37"/>
<rectangle x1="5.5626" y1="10.6934" x2="5.6134" y2="10.7442" layer="37"/>
<rectangle x1="2.921" y1="10.7442" x2="5.5118" y2="10.795" layer="37"/>
<rectangle x1="8.9662" y1="10.7442" x2="11.303" y2="10.795" layer="37"/>
<rectangle x1="5.5118" y1="10.7442" x2="5.5626" y2="10.795" layer="37"/>
<rectangle x1="2.921" y1="10.795" x2="5.461" y2="10.8458" layer="37"/>
<rectangle x1="9.017" y1="10.795" x2="11.303" y2="10.8458" layer="37"/>
<rectangle x1="5.461" y1="10.795" x2="5.5118" y2="10.8458" layer="37"/>
<rectangle x1="8.9662" y1="10.795" x2="9.017" y2="10.8458" layer="37"/>
<rectangle x1="2.921" y1="10.8458" x2="5.4102" y2="10.8966" layer="37"/>
<rectangle x1="9.0678" y1="10.8458" x2="11.303" y2="10.8966" layer="37"/>
<rectangle x1="9.017" y1="10.8458" x2="9.0678" y2="10.8966" layer="37"/>
<rectangle x1="5.4102" y1="10.8458" x2="5.461" y2="10.8966" layer="37"/>
<rectangle x1="2.921" y1="10.8966" x2="5.3594" y2="10.9474" layer="37"/>
<rectangle x1="9.1186" y1="10.8966" x2="11.303" y2="10.9474" layer="37"/>
<rectangle x1="5.3594" y1="10.8966" x2="5.4102" y2="10.9474" layer="37"/>
<rectangle x1="9.0678" y1="10.8966" x2="9.1186" y2="10.9474" layer="37"/>
<rectangle x1="2.921" y1="10.9474" x2="5.3594" y2="10.9982" layer="37"/>
<rectangle x1="9.1186" y1="10.9474" x2="11.303" y2="10.9982" layer="37"/>
<rectangle x1="9.0678" y1="10.9474" x2="9.1186" y2="10.9982" layer="37"/>
<rectangle x1="2.9718" y1="10.9982" x2="5.3086" y2="11.049" layer="37"/>
<rectangle x1="9.1694" y1="10.9982" x2="11.2522" y2="11.049" layer="37"/>
<rectangle x1="2.921" y1="10.9982" x2="2.9718" y2="11.049" layer="37"/>
<rectangle x1="11.2522" y1="10.9982" x2="11.303" y2="11.049" layer="37"/>
<rectangle x1="9.1186" y1="10.9982" x2="9.1694" y2="11.049" layer="37"/>
<rectangle x1="5.3086" y1="10.9982" x2="5.3594" y2="11.049" layer="37"/>
<rectangle x1="2.9718" y1="11.049" x2="5.2578" y2="11.0998" layer="37"/>
<rectangle x1="9.2202" y1="11.049" x2="11.2522" y2="11.0998" layer="37"/>
<rectangle x1="9.1694" y1="11.049" x2="9.2202" y2="11.0998" layer="37"/>
<rectangle x1="11.2522" y1="11.049" x2="11.303" y2="11.0998" layer="37"/>
<rectangle x1="5.2578" y1="11.049" x2="5.3086" y2="11.0998" layer="37"/>
<rectangle x1="2.921" y1="11.049" x2="2.9718" y2="11.0998" layer="37"/>
<rectangle x1="3.0226" y1="11.0998" x2="5.207" y2="11.1506" layer="37"/>
<rectangle x1="9.2202" y1="11.0998" x2="11.2522" y2="11.1506" layer="37"/>
<rectangle x1="2.9718" y1="11.0998" x2="3.0226" y2="11.1506" layer="37"/>
<rectangle x1="5.207" y1="11.0998" x2="5.2578" y2="11.1506" layer="37"/>
<rectangle x1="3.0734" y1="11.1506" x2="5.1054" y2="11.2014" layer="37"/>
<rectangle x1="9.3218" y1="11.1506" x2="11.1506" y2="11.2014" layer="37"/>
<rectangle x1="5.1054" y1="11.1506" x2="5.1562" y2="11.2014" layer="37"/>
<rectangle x1="9.271" y1="11.1506" x2="9.3218" y2="11.2014" layer="37"/>
<rectangle x1="11.1506" y1="11.1506" x2="11.2014" y2="11.2014" layer="37"/>
</package>
</packages>
<symbols>
<symbol name="MUTEX-LOGO">
<text x="0" y="0" size="2.54" layer="94" font="vector">LOGO</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MUTEX-LOGO" prefix="LOGO">
<gates>
<gate name="G$1" symbol="MUTEX-LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MUTEX-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Aesthetics&lt;/h3&gt;
This library contiains non-functional items such as logos, build/ordering notes, frame blocks, etc. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="FR-A4L">
<description>&lt;h3&gt;Schematic Frame - A4L - European Format&lt;/h3&gt;
&lt;p&gt;Standard A4 size frame in Landscape&lt;/p&gt;
&lt;p&gt;Devices using:
&lt;ul&gt;&lt;li&gt;FRAME-A4L&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
<rectangle x1="178.7652" y1="0" x2="179.3748" y2="20.32" layer="94"/>
<rectangle x1="225.7552" y1="-26.67" x2="226.3648" y2="67.31" layer="94" rot="R90"/>
<wire x1="225.29" y1="-0.1" x2="225.29" y2="5.08" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="273.05" y2="5.08" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="179.07" y2="5.08" width="0.1016" layer="94"/>
<wire x1="179.07" y1="10.16" x2="225.29" y2="10.16" width="0.1016" layer="94"/>
<wire x1="225.29" y1="10.16" x2="273.05" y2="10.16" width="0.1016" layer="94"/>
<wire x1="179.07" y1="15.24" x2="273.05" y2="15.24" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="225.29" y2="10.16" width="0.1016" layer="94"/>
<wire x1="179.07" y1="19.05" x2="179.07" y2="20.32" width="0.6096" layer="94"/>
<wire x1="179.07" y1="20.32" x2="180.34" y2="20.32" width="0.6096" layer="94"/>
<text x="181.61" y="11.43" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="181.61" y="6.35" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="195.58" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="181.61" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="181.61" y="16.51" size="2.54" layer="94" font="vector">&gt;CNAME</text>
<text x="226.16" y="1.27" size="2.54" layer="94" font="vector">Rev:</text>
<text x="226.26" y="6.35" size="2.54" layer="94" font="vector">&gt;DESIGNER</text>
<text x="234.92" y="1.17" size="2.54" layer="94" font="vector">&gt;CREVISION</text>
<frame x1="-3.81" y1="-3.81" x2="276.86" y2="182.88" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A4L" prefix="FRAME">
<description>&lt;h3&gt;Schematic Frame - A4L - European Format&lt;/h3&gt;
&lt;p&gt;Standard A4 size frame in Landscape&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="FR-A4L" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ESP8266-ESP12E">
<packages>
<package name="ESP8266-ESP12E">
<description>
&lt;b&gt;ESP8266-12E with additional I/O and GPIO04/05 corrected&lt;/b&gt;&lt;p&gt;
The author cannot warrant that this library is free from error
or will meet your specific requirements.&lt;p&gt;
&lt;author&gt;Created by PuceBaboon.com. Komagane, Nagano, JAPAN&lt;/author&gt;
</description>
<wire x1="16.2" y1="0" x2="16.2" y2="24.1" width="0.127" layer="21"/>
<wire x1="16.2" y1="24.1" x2="0" y2="24.1" width="0.127" layer="21"/>
<wire x1="0" y1="24.1" x2="0" y2="0" width="0.127" layer="21"/>
<smd name="8" x="0" y="2" dx="2" dy="1.2" layer="1"/>
<smd name="7" x="0" y="4" dx="2" dy="1.2" layer="1"/>
<smd name="6" x="0" y="6" dx="2" dy="1.2" layer="1"/>
<smd name="5" x="0" y="8" dx="2" dy="1.2" layer="1"/>
<smd name="4" x="0" y="10" dx="2" dy="1.2" layer="1"/>
<smd name="3" x="0" y="12" dx="2" dy="1.2" layer="1"/>
<smd name="2" x="0" y="14" dx="2" dy="1.2" layer="1"/>
<smd name="1" x="0" y="16" dx="2" dy="1.2" layer="1"/>
<smd name="9" x="16.2" y="2" dx="2" dy="1.2" layer="1"/>
<smd name="10" x="16.2" y="4" dx="2" dy="1.2" layer="1"/>
<smd name="11" x="16.2" y="6" dx="2" dy="1.2" layer="1"/>
<smd name="12" x="16.2" y="8" dx="2" dy="1.2" layer="1"/>
<smd name="13" x="16.2" y="10" dx="2" dy="1.2" layer="1"/>
<smd name="14" x="16.2" y="12" dx="2" dy="1.2" layer="1"/>
<smd name="15" x="16.2" y="14" dx="2" dy="1.2" layer="1"/>
<smd name="16" x="16.2" y="16" dx="2" dy="1.2" layer="1"/>
<text x="5" y="2" size="1.27" layer="21" rot="R90">&gt;NAME</text>
<text x="5" y="11.11" size="1.27" layer="21">ESP-12E</text>
<wire x1="2.54" y1="16.51" x2="2.54" y2="17.78" width="0.127" layer="51"/>
<wire x1="2.54" y1="17.78" x2="2.54" y2="22.86" width="0.127" layer="51"/>
<wire x1="2.54" y1="22.86" x2="3.81" y2="22.86" width="0.127" layer="51"/>
<wire x1="3.81" y1="22.86" x2="3.81" y2="19.05" width="0.127" layer="51"/>
<wire x1="3.81" y1="19.05" x2="5.08" y2="19.05" width="0.127" layer="51"/>
<wire x1="5.08" y1="19.05" x2="5.08" y2="22.86" width="0.127" layer="51"/>
<wire x1="5.08" y1="22.86" x2="6.35" y2="22.86" width="0.127" layer="51"/>
<wire x1="6.35" y1="22.86" x2="6.35" y2="19.05" width="0.127" layer="51"/>
<wire x1="6.35" y1="19.05" x2="7.62" y2="19.05" width="0.127" layer="51"/>
<wire x1="7.62" y1="19.05" x2="7.62" y2="22.86" width="0.127" layer="51"/>
<wire x1="7.62" y1="22.86" x2="8.89" y2="22.86" width="0.127" layer="51"/>
<wire x1="8.89" y1="22.86" x2="8.89" y2="19.05" width="0.127" layer="51"/>
<wire x1="8.89" y1="19.05" x2="10.16" y2="19.05" width="0.127" layer="51"/>
<wire x1="10.16" y1="19.05" x2="10.16" y2="22.86" width="0.127" layer="51"/>
<wire x1="10.16" y1="22.86" x2="13.97" y2="22.86" width="0.127" layer="51"/>
<wire x1="2.54" y1="17.78" x2="10.16" y2="17.78" width="0.127" layer="51"/>
<smd name="17" x="11" y="0" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="18" x="3" y="0" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="19" x="5" y="0" dx="2" dy="1.2" layer="1" rot="R90"/>
<wire x1="0" y1="0" x2="16.2" y2="0" width="0.127" layer="21"/>
<smd name="20" x="13" y="0" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="21" x="7" y="0" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="22" x="9" y="0" dx="2" dy="1.2" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="ESP12E">
<description>
&lt;b&gt;ESP8266-12E with additional I/O and GPIO04/05 corrected&lt;/b&gt;&lt;p&gt;
The author cannot warrant that this library is free from error
or will meet your specific requirements.&lt;p&gt;
&lt;author&gt;Created by PuceBaboon.com. Komagane, Nagano, JAPAN&lt;/author&gt;
</description>
<pin name="GND" x="10.16" y="-10.16" length="middle" rot="R180"/>
<pin name="GPIO15" x="10.16" y="-7.62" length="middle" rot="R180"/>
<pin name="GPIO2" x="10.16" y="-5.08" length="middle" rot="R180"/>
<pin name="GPIO0" x="10.16" y="-2.54" length="middle" rot="R180"/>
<pin name="GPIO4" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="GPIO5" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="RXD" x="10.16" y="5.08" length="middle" rot="R180"/>
<pin name="TXD" x="10.16" y="7.62" length="middle" rot="R180"/>
<pin name="REST" x="-22.86" y="7.62" length="middle"/>
<pin name="ADC" x="-22.86" y="5.08" length="middle"/>
<pin name="CH_PD" x="-22.86" y="2.54" length="middle"/>
<pin name="GPIO16" x="-22.86" y="0" length="middle"/>
<pin name="GPIO14" x="-22.86" y="-2.54" length="middle"/>
<pin name="GPIO12" x="-22.86" y="-5.08" length="middle"/>
<pin name="GPIO13" x="-22.86" y="-7.62" length="middle"/>
<pin name="VCC" x="-22.86" y="-10.16" length="middle"/>
<wire x1="-17.78" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="5.08" y2="15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="15.24" x2="-17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="-17.78" y1="15.24" x2="-17.78" y2="-20.32" width="0.254" layer="94"/>
<text x="-15.24" y="10.16" size="1.27" layer="94">ESP8266_ESP12E</text>
<text x="-10.16" y="12.7" size="1.27" layer="94">&gt;NAME</text>
<pin name="CS0" x="-12.7" y="-25.4" length="middle" rot="R90"/>
<pin name="MISO" x="-10.16" y="-25.4" length="middle" rot="R90"/>
<pin name="GPIO9" x="-7.62" y="-25.4" length="middle" rot="R90"/>
<pin name="GPIO10" x="-5.08" y="-25.4" length="middle" rot="R90"/>
<pin name="MOSI" x="-2.54" y="-25.4" length="middle" rot="R90"/>
<pin name="SCLK" x="0" y="-25.4" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP8266-12E" prefix="MDL" uservalue="yes">
<description>
&lt;b&gt;ESP8266-12E with additional I/O and GPIO04/05 corrected&lt;/b&gt;&lt;p&gt;
The author cannot warrant that this library is free from error
or will meet your specific requirements.&lt;p&gt;
&lt;author&gt;Created by PuceBaboon.com. Komagane, Nagano, JAPAN&lt;/author&gt;
</description>
<gates>
<gate name="G$1" symbol="ESP12E" x="5.08" y="0"/>
</gates>
<devices>
<device name="ESP8266-ESP12E" package="ESP8266-ESP12E">
<connects>
<connect gate="G$1" pin="ADC" pad="2"/>
<connect gate="G$1" pin="CH_PD" pad="3"/>
<connect gate="G$1" pin="CS0" pad="17"/>
<connect gate="G$1" pin="GND" pad="9"/>
<connect gate="G$1" pin="GPIO0" pad="12"/>
<connect gate="G$1" pin="GPIO10" pad="20"/>
<connect gate="G$1" pin="GPIO12" pad="6"/>
<connect gate="G$1" pin="GPIO13" pad="7"/>
<connect gate="G$1" pin="GPIO14" pad="5"/>
<connect gate="G$1" pin="GPIO15" pad="10"/>
<connect gate="G$1" pin="GPIO16" pad="4"/>
<connect gate="G$1" pin="GPIO2" pad="11"/>
<connect gate="G$1" pin="GPIO4" pad="13"/>
<connect gate="G$1" pin="GPIO5" pad="14"/>
<connect gate="G$1" pin="GPIO9" pad="19"/>
<connect gate="G$1" pin="MISO" pad="18"/>
<connect gate="G$1" pin="MOSI" pad="21"/>
<connect gate="G$1" pin="REST" pad="1"/>
<connect gate="G$1" pin="RXD" pad="15"/>
<connect gate="G$1" pin="SCLK" pad="22"/>
<connect gate="G$1" pin="TXD" pad="16"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Switches">
<description>&lt;h3&gt;SparkFun Switches, Buttons, Encoders&lt;/h3&gt;
In this library you'll find switches, buttons, joysticks, and anything that moves to create or disrupt an electrical connection.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TACTILE_SWITCH_PTH_6.0MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 6.0mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-1000)&lt;/p&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="0" y="3.302" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_4.5MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 4.5mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://spec_sheets.e-switch.com/specs/P010338.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.3" y1="2.2" x2="-1.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-2.2" x2="1.3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="0.8" x2="1.8" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="1.8" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="-2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<text x="0" y="2.413" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_12MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 12mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-5050)&lt;/p&gt;</description>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
<text x="0" y="6.223" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-6.223" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_6.0X3.5MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 6.0 x 3.5 mm&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/1101.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-3" y1="1.1" x2="-3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="3" y1="1.1" x2="3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.75" y1="1.75" x2="-3" y2="1.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.75" y1="1.75" x2="2.75" y2="1.75" width="0.2032" layer="21"/>
<wire x1="2.75" y1="1.75" x2="3" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="3" y1="-1.5" x2="2.75" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.75" y1="-1.75" x2="-2.75" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.5" x2="-2.75" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-3" y1="-1.5" x2="-3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="1.1" x2="3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="-1.5" x2="3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2" y1="0" x2="-1" y2="0" width="0.127" layer="51"/>
<wire x1="-1" y1="0" x2="0.1" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.3" y1="0" x2="2" y2="0" width="0.127" layer="51"/>
<smd name="1" x="-3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<smd name="2" x="3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.905" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_6.2MM_TALL">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 6.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.apem.com/files/apem/brochures/ADTS6-ADTSM-KTSC6.pdf"&gt;Datasheet&lt;/a&gt; (ADTSM63NVTR)&lt;/p&gt;</description>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.2032" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.2032" layer="21"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.2032" layer="21"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.2032" layer="21"/>
<smd name="A1" x="-3.975" y="-2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="A2" x="3.975" y="-2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="B1" x="-3.975" y="2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="B2" x="3.975" y="2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<text x="0" y="3.175" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_RIGHT_ANGLE_KIT">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/Switches/SW016.JPG"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="1.5" y1="-3.8" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="3.5" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="3.5" x2="3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.65" y1="3.5" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="2" x2="3" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.777" y1="1" x2="-3.777" y2="-2.127" width="0.2032" layer="21"/>
<wire x1="-3.777" y1="-2.127" x2="3.777" y2="-2.127" width="0.2032" layer="21"/>
<wire x1="3.777" y1="-2.127" x2="3.777" y2="1" width="0.2032" layer="21"/>
<wire x1="2" y1="2.127" x2="-2" y2="2.127" width="0.2032" layer="21"/>
<pad name="ANCHOR1" x="-3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="ANCHOR2" x="3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<circle x="2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<circle x="2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="-2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_12MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 12mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://cdn.sparkfun.com/datasheets/Components/Switches/N301102.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<smd name="4" x="-6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="2" x="-6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="1" x="6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="3" x="6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<wire x1="-6" y1="-5" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="-6" y2="5" width="0.2032" layer="21"/>
<text x="0" y="6.223" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-6.223" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_6.0MM_KIT">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 6.0mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-1000)&lt;/p&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-3.2664" y="3.142"/>
<vertex x="-3.2589" y="3.1445" curve="89.986886"/>
<vertex x="-4.1326" y="2.286"/>
<vertex x="-4.1351" y="2.2657" curve="90.00652"/>
<vertex x="-3.2563" y="1.392"/>
<vertex x="-3.2487" y="1.3869" curve="90.006616"/>
<vertex x="-2.3826" y="2.2403"/>
<vertex x="-2.3775" y="2.2683" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.2462" y="2.7026"/>
<vertex x="-3.2589" y="2.7051" curve="90.026544"/>
<vertex x="-3.6881" y="2.2733"/>
<vertex x="-3.6881" y="2.2632" curve="89.974074"/>
<vertex x="-3.2562" y="1.8213"/>
<vertex x="-3.2259" y="1.8186" curve="90.051271"/>
<vertex x="-2.8093" y="2.2658"/>
<vertex x="-2.8093" y="2.2606" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="3.2411" y="3.1395"/>
<vertex x="3.2486" y="3.142" curve="89.986886"/>
<vertex x="2.3749" y="2.2835"/>
<vertex x="2.3724" y="2.2632" curve="90.00652"/>
<vertex x="3.2512" y="1.3895"/>
<vertex x="3.2588" y="1.3844" curve="90.006616"/>
<vertex x="4.1249" y="2.2378"/>
<vertex x="4.13" y="2.2658" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.2613" y="2.7001"/>
<vertex x="3.2486" y="2.7026" curve="90.026544"/>
<vertex x="2.8194" y="2.2708"/>
<vertex x="2.8194" y="2.2607" curve="89.974074"/>
<vertex x="3.2513" y="1.8188"/>
<vertex x="3.2816" y="1.8161" curve="90.051271"/>
<vertex x="3.6982" y="2.2633"/>
<vertex x="3.6982" y="2.2581" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.2613" y="-1.3868"/>
<vertex x="-3.2538" y="-1.3843" curve="89.986886"/>
<vertex x="-4.1275" y="-2.2428"/>
<vertex x="-4.13" y="-2.2631" curve="90.00652"/>
<vertex x="-3.2512" y="-3.1368"/>
<vertex x="-3.2436" y="-3.1419" curve="90.006616"/>
<vertex x="-2.3775" y="-2.2885"/>
<vertex x="-2.3724" y="-2.2605" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.2411" y="-1.8262"/>
<vertex x="-3.2538" y="-1.8237" curve="90.026544"/>
<vertex x="-3.683" y="-2.2555"/>
<vertex x="-3.683" y="-2.2656" curve="89.974074"/>
<vertex x="-3.2511" y="-2.7075"/>
<vertex x="-3.2208" y="-2.7102" curve="90.051271"/>
<vertex x="-2.8042" y="-2.263"/>
<vertex x="-2.8042" y="-2.2682" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="3.2411" y="-1.3843"/>
<vertex x="3.2486" y="-1.3818" curve="89.986886"/>
<vertex x="2.3749" y="-2.2403"/>
<vertex x="2.3724" y="-2.2606" curve="90.00652"/>
<vertex x="3.2512" y="-3.1343"/>
<vertex x="3.2588" y="-3.1394" curve="90.006616"/>
<vertex x="4.1249" y="-2.286"/>
<vertex x="4.13" y="-2.258" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.2613" y="-1.8237"/>
<vertex x="3.2486" y="-1.8212" curve="90.026544"/>
<vertex x="2.8194" y="-2.253"/>
<vertex x="2.8194" y="-2.2631" curve="89.974074"/>
<vertex x="3.2513" y="-2.705"/>
<vertex x="3.2816" y="-2.7077" curve="90.051271"/>
<vertex x="3.6982" y="-2.2605"/>
<vertex x="3.6982" y="-2.2657" curve="90.012964"/>
</polygon>
<text x="0" y="3.175" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_5.2MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 5.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/Buttons/SMD-Button.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-1.54" y1="-2.54" x2="-2.54" y2="-1.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.24" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.54" x2="-1.54" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.54" y1="2.54" x2="1.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.54" y1="2.54" x2="2.54" y2="1.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.24" x2="2.54" y2="-1.24" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.54" x2="1.54" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.54" y1="-2.54" x2="-1.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.794" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="2.794" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.794" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="4" x="2.794" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_RIGHT_ANGLE">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;</description>
<hole x="0" y="0.9" drill="0.7"/>
<hole x="0" y="-0.9" drill="0.7"/>
<smd name="1" x="-1.95" y="0" dx="2" dy="1.1" layer="1" rot="R90"/>
<smd name="2" x="1.95" y="0" dx="2" dy="1.1" layer="1" rot="R90"/>
<wire x1="-2" y1="1.2" x2="-2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.5" x2="2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="2" y1="1.5" x2="2" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.2" x2="-2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.5" x2="-0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-1.5" x2="0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="0.7" y1="-1.5" x2="2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.5" x2="2" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-2.1" x2="0.7" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="0.7" y1="-2.1" x2="0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-2.1" x2="-0.7" y2="-1.5" width="0.2032" layer="21"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_4.6X2.8MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 4.6 x 2.8mm&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ck-components.com/media/1479/kmr2.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<smd name="3" x="2.05" y="0.8" dx="0.9" dy="1" layer="1"/>
<smd name="2" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1"/>
<smd name="1" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1"/>
<smd name="4" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1"/>
<wire x1="-2.1" y1="1.4" x2="-2.1" y2="-1.4" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.4" x2="2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.4" x2="2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.805" width="0.127" layer="21"/>
<wire x1="1.338" y1="-1.4" x2="-1.338" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-1.338" y1="1.4" x2="1.338" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="0.13" x2="-2.1" y2="-0.13" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-0.13" x2="2.1" y2="0.13" width="0.2032" layer="21"/>
<rectangle x1="-2.3" y1="0.5" x2="-2.1" y2="1.1" layer="51"/>
<rectangle x1="-2.3" y1="-1.1" x2="-2.1" y2="-0.5" layer="51"/>
<rectangle x1="2.1" y1="-1.1" x2="2.3" y2="-0.5" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="0.5" x2="2.3" y2="1.1" layer="51" rot="R180"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="SWITCH-MOMENTARY-2">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;</description>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.508" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOMENTARY-SWITCH-SPST" prefix="S">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;h4&gt;Variants&lt;/h4&gt;
&lt;h5&gt;PTH-12MM - 12mm square, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9190"&gt;Momentary Pushbutton Switch - 12mm Square&lt;/a&gt; (COM-09190)&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;PTH-6.0MM, PTH-6.0MM-KIT - 6.0mm square, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/97"&gt;Mini Pushbutton Switch&lt;/a&gt; (COM-00097)&lt;/li&gt;
&lt;li&gt;KIT package intended for soldering kit's - only one side of pads' copper is exposed.&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;PTH-RIGHT-ANGLE-KIT - Right-angle, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10791"&gt;Right Angle Tactile Button&lt;/a&gt; - Used on &lt;a href="https://www.sparkfun.com/products/11734"&gt;
SparkFun BigTime Watch Kit&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-12MM - 12mm square, surface-mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12993"&gt;Tactile Button - SMD (12mm)&lt;/a&gt; (COM-12993)&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/11888"&gt;SparkFun PicoBoard&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-4.5MM - 4.5mm Square Trackball Switch&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13169"&gt;SparkFun Blackberry Trackballer Breakout&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-4.6MMX2.8MM -  4.60mm x 2.80mm, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13664"&gt;SparkFun SAMD21 Mini Breakout&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-5.2MM, SMD-5.2-REDUNDANT - 5.2mm square, surface-mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8720"&gt;Mini Pushbutton Switch - SMD&lt;/a&gt; (COM-08720)&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/11114"&gt;Arduino Pro Mini&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;REDUNDANT package connects both switch circuits together&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-6.0X3.5MM - 6.0 x 3.5mm, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8229"&gt;Momentary Reset Switch SMD&lt;/a&gt; (COM-08229)&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-6.2MM-TALL - 6.2mm square, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12992"&gt;Tactile Button - SMD (6mm)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/12651"&gt;SparkFun Digital Sandbox&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-RIGHT-ANGLE - Right-angle, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13036"&gt;SparkFun Block for Intel® Edison - Arduino&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="SWITCH-MOMENTARY-2" x="0" y="0"/>
</gates>
<devices>
<device name="-PTH-6.0MM" package="TACTILE_SWITCH_PTH_6.0MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" SWCH-08441"/>
<attribute name="SF_SKU" value="COM-00097"/>
</technology>
</technologies>
</device>
<device name="-SMD-4.5MM" package="TACTILE_SWITCH_SMD_4.5MM">
<connects>
<connect gate="G$1" pin="1" pad="2 3"/>
<connect gate="G$1" pin="2" pad="1 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-09213"/>
</technology>
</technologies>
</device>
<device name="-PTH-12MM" package="TACTILE_SWITCH_PTH_12MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-09185"/>
<attribute name="SF_SKU" value="COM-09190"/>
</technology>
</technologies>
</device>
<device name="-SMD-6.0X3.5MM" package="TACTILE_SWITCH_SMD_6.0X3.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-00815"/>
<attribute name="SF_SKU" value="COM-08229"/>
</technology>
</technologies>
</device>
<device name="-SMD-6.2MM-TALL" package="TACTILE_SWITCH_SMD_6.2MM_TALL">
<connects>
<connect gate="G$1" pin="1" pad="A2"/>
<connect gate="G$1" pin="2" pad="B2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-11966"/>
<attribute name="SF_SKU" value="COM-12992"/>
</technology>
</technologies>
</device>
<device name="-PTH-RIGHT-ANGLE-KIT" package="TACTILE_SWITCH_PTH_RIGHT_ANGLE_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10672"/>
<attribute name="SF_SKU" value="COM-10791"/>
</technology>
</technologies>
</device>
<device name="-SMD-12MM" package="TACTILE_SWITCH_SMD_12MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-11967"/>
<attribute name="SF_SKU" value="COM-12993"/>
</technology>
</technologies>
</device>
<device name="-PTH-6.0MM-KIT" package="TACTILE_SWITCH_PTH_6.0MM_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08441"/>
<attribute name="SF_SKU" value="COM-00097 "/>
</technology>
</technologies>
</device>
<device name="-SMD-5.2MM" package="TACTILE_SWITCH_SMD_5.2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08247"/>
<attribute name="SF_SKU" value="COM-08720"/>
</technology>
</technologies>
</device>
<device name="-SMD-5.2-REDUNDANT" package="TACTILE_SWITCH_SMD_5.2MM">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08247"/>
<attribute name="SF_SKU" value="COM-08720"/>
</technology>
</technologies>
</device>
<device name="-SMD-RIGHT-ANGLE" package="TACTILE_SWITCH_SMD_RIGHT_ANGLE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="COMP-12265" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SMD-4.6X2.8MM" package="TACTILE_SWITCH_SMD_4.6X2.8MM">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-13065"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-IC-Microcontroller">
<description>&lt;h3&gt;SparkFun Microcontrollers&lt;/h3&gt;
This library contains microcontrollers.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TQFP32-08">
<description>&lt;B&gt;Thin Plasic Quad Flat Package&lt;/B&gt; Grid 0.8 mm</description>
<wire x1="3.505" y1="3.505" x2="3.505" y2="-3.505" width="0.2032" layer="21"/>
<wire x1="3.505" y1="-3.505" x2="-3.505" y2="-3.505" width="0.2032" layer="21"/>
<wire x1="-3.505" y1="-3.505" x2="-3.505" y2="3.15" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="3.505" y2="3.505" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="-3.505" y2="3.15" width="0.2032" layer="21"/>
<circle x="-3.6576" y="3.683" radius="0.1524" width="0.2032" layer="21"/>
<smd name="1" x="-4.2926" y="2.8" dx="1.27" dy="0.5588" layer="1"/>
<smd name="2" x="-4.2926" y="2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="3" x="-4.2926" y="1.2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="4" x="-4.2926" y="0.4" dx="1.27" dy="0.5588" layer="1"/>
<smd name="5" x="-4.2926" y="-0.4" dx="1.27" dy="0.5588" layer="1"/>
<smd name="6" x="-4.2926" y="-1.2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="7" x="-4.2926" y="-2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="8" x="-4.2926" y="-2.8" dx="1.27" dy="0.5588" layer="1"/>
<smd name="9" x="-2.8" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="10" x="-2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="11" x="-1.2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="12" x="-0.4" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="13" x="0.4" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="14" x="1.2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="15" x="2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="16" x="2.8" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="17" x="4.2926" y="-2.8" dx="1.27" dy="0.5588" layer="1"/>
<smd name="18" x="4.2926" y="-2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="19" x="4.2926" y="-1.2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="20" x="4.2926" y="-0.4" dx="1.27" dy="0.5588" layer="1"/>
<smd name="21" x="4.2926" y="0.4" dx="1.27" dy="0.5588" layer="1"/>
<smd name="22" x="4.2926" y="1.2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="23" x="4.2926" y="2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="24" x="4.2926" y="2.8" dx="1.27" dy="0.5588" layer="1"/>
<smd name="25" x="2.8" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="26" x="2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="27" x="1.2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="28" x="0.4" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="29" x="-0.4" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="30" x="-1.2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="31" x="-2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="32" x="-2.8" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-4.5466" y1="2.5714" x2="-3.556" y2="3.0286" layer="51"/>
<rectangle x1="-4.5466" y1="1.7714" x2="-3.556" y2="2.2286" layer="51"/>
<rectangle x1="-4.5466" y1="0.9714" x2="-3.556" y2="1.4286" layer="51"/>
<rectangle x1="-4.5466" y1="0.1714" x2="-3.556" y2="0.6286" layer="51"/>
<rectangle x1="-4.5466" y1="-0.6286" x2="-3.556" y2="-0.1714" layer="51"/>
<rectangle x1="-4.5466" y1="-1.4286" x2="-3.556" y2="-0.9714" layer="51"/>
<rectangle x1="-4.5466" y1="-2.2286" x2="-3.556" y2="-1.7714" layer="51"/>
<rectangle x1="-4.5466" y1="-3.0286" x2="-3.556" y2="-2.5714" layer="51"/>
<rectangle x1="-3.0286" y1="-4.5466" x2="-2.5714" y2="-3.556" layer="51"/>
<rectangle x1="-2.2286" y1="-4.5466" x2="-1.7714" y2="-3.556" layer="51"/>
<rectangle x1="-1.4286" y1="-4.5466" x2="-0.9714" y2="-3.556" layer="51"/>
<rectangle x1="-0.6286" y1="-4.5466" x2="-0.1714" y2="-3.556" layer="51"/>
<rectangle x1="0.1714" y1="-4.5466" x2="0.6286" y2="-3.556" layer="51"/>
<rectangle x1="0.9714" y1="-4.5466" x2="1.4286" y2="-3.556" layer="51"/>
<rectangle x1="1.7714" y1="-4.5466" x2="2.2286" y2="-3.556" layer="51"/>
<rectangle x1="2.5714" y1="-4.5466" x2="3.0286" y2="-3.556" layer="51"/>
<rectangle x1="3.556" y1="-3.0286" x2="4.5466" y2="-2.5714" layer="51"/>
<rectangle x1="3.556" y1="-2.2286" x2="4.5466" y2="-1.7714" layer="51"/>
<rectangle x1="3.556" y1="-1.4286" x2="4.5466" y2="-0.9714" layer="51"/>
<rectangle x1="3.556" y1="-0.6286" x2="4.5466" y2="-0.1714" layer="51"/>
<rectangle x1="3.556" y1="0.1714" x2="4.5466" y2="0.6286" layer="51"/>
<rectangle x1="3.556" y1="0.9714" x2="4.5466" y2="1.4286" layer="51"/>
<rectangle x1="3.556" y1="1.7714" x2="4.5466" y2="2.2286" layer="51"/>
<rectangle x1="3.556" y1="2.5714" x2="4.5466" y2="3.0286" layer="51"/>
<rectangle x1="2.5714" y1="3.556" x2="3.0286" y2="4.5466" layer="51"/>
<rectangle x1="1.7714" y1="3.556" x2="2.2286" y2="4.5466" layer="51"/>
<rectangle x1="0.9714" y1="3.556" x2="1.4286" y2="4.5466" layer="51"/>
<rectangle x1="0.1714" y1="3.556" x2="0.6286" y2="4.5466" layer="51"/>
<rectangle x1="-0.6286" y1="3.556" x2="-0.1714" y2="4.5466" layer="51"/>
<rectangle x1="-1.4286" y1="3.556" x2="-0.9714" y2="4.5466" layer="51"/>
<rectangle x1="-2.2286" y1="3.556" x2="-1.7714" y2="4.5466" layer="51"/>
<rectangle x1="-3.0286" y1="3.556" x2="-2.5714" y2="4.5466" layer="51"/>
<text x="0" y="5.715" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-6.35" size="0.6096" layer="27" font="vector" ratio="20" align="bottom-center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ATMEGAXX8-32PIN_NOPAD">
<wire x1="-20.32" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="-30.48" x2="-20.32" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-30.48" x2="-20.32" y2="33.02" width="0.254" layer="94"/>
<text x="-20.32" y="33.782" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PB5(SCK)" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2)" x="-25.4" y="0" length="middle"/>
<pin name="PB6(XTAL1/TOSC1)" x="-25.4" y="5.08" length="middle"/>
<pin name="GND@3" x="-25.4" y="-22.86" length="middle"/>
<pin name="GND@5" x="-25.4" y="-25.4" length="middle"/>
<pin name="VCC@4" x="-25.4" y="22.86" length="middle"/>
<pin name="VCC@6" x="-25.4" y="20.32" length="middle"/>
<pin name="AGND" x="-25.4" y="-20.32" length="middle"/>
<pin name="AREF" x="-25.4" y="15.24" length="middle"/>
<pin name="AVCC" x="-25.4" y="25.4" length="middle"/>
<pin name="PB4(MISO)" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2)" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B)" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PB1(OC1A)" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="PB0(ICP)" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PD6(AIN0)" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PD5(T1)" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PD4(XCK/T0)" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PD3(INT1)" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="PD2(INT0)" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="PD1(TXD)" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PD0(RXD)" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="ADC7" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="ADC6" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL)" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA)" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="PC3(ADC3)" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PC2(ADC2)" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="PC1(ADC1)" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PC0(ADC0)" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="PC6(/RESET)" x="-25.4" y="30.48" length="middle" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA328P_TQFP" prefix="U">
<description>&lt;h3&gt;Popular 328P in QFP&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/products/13448"&gt;Storefront component: ATmega328 - TQFP&lt;/a&gt; (COM-13448)&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.sparkfun.com/datasheets/Components/SMD/ATMega328.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12757"&gt;SparkFun RedBoard - Programmed with Arduino&lt;/a&gt; (DEV-12757)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11113"&gt;Arduino Pro Mini 328 - 5V/16MHz&lt;/a&gt; (DEV-11113)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11114"&gt;Arduino Pro Mini 328 - 3.3V/8MHz&lt;/a&gt; (DEV-11114)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13712"&gt;SparkFun OpenLog&lt;/a&gt; (DEV-13712)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12923"&gt;SparkFun MicroView - OLED Arduino Module&lt;/a&gt; (DEV-12923)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12651"&gt;SparkFun Digital Sandbox&lt;/a&gt; (DEV-12651)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11888"&gt;SparkFun PicoBoard&lt;/a&gt; (WIG-11888)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10116"&gt;Arduino Fio&lt;/a&gt; (DEV-10116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13342"&gt;LilyPad Arduino 328 Main Board&lt;/a&gt; (DEV-13342)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13741"&gt;SparkFun RedStick&lt;/a&gt; (DEV-13741)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11013"&gt;LilyPad MP3&lt;/a&gt; (DEV-11013)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10941"&gt;LilyPad Arduino SimpleSnap&lt;/a&gt; (DEV-10941)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13899"&gt;SparkFun Stepoko&lt;/a&gt; (ROB-13899)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11345"&gt;SparkFun Geiger Counter&lt;/a&gt; (SEN-11345)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12781"&gt;SparkFun EL Sequencer&lt;/a&gt; (COM-12781)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10915"&gt;Arduino Pro 328 - 5V/16MHz&lt;/a&gt; (DEV-10915)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12773"&gt;CryptoCape&lt;/a&gt; (DEV-12773)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10740"&gt;SparkFun IR Thermometer Evaluation Board - MLX90614&lt;/a&gt; (SEN-10740)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12097"&gt;SparkFun RedBot Mainboard&lt;/a&gt; (ROB-12097)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10914"&gt;Arduino Pro 328 - 3.3V/8MHz&lt;/a&gt; (DEV-10914)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13036"&gt;SparkFun Block for Intel® Edison - Arduino&lt;/a&gt; (DEV-13036)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10935"&gt;SparkFun Simon Says - Surface Mount Soldering Kit&lt;/a&gt; (KIT-10935)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11441"&gt;SparkFun 7-Segment Serial Display - Red&lt;/a&gt; (COM-11441)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11629"&gt;SparkFun 7-Segment Serial Display - White&lt;/a&gt; (COM-11629)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11443"&gt;SparkFun 7-Segment Serial Display - Yellow&lt;/a&gt; (COM-11443)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11440"&gt;SparkFun 7-Segment Serial Display - Kelly Green&lt;/a&gt; (COM-11440)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11442"&gt;SparkFun 7-Segment Serial Display - Blue&lt;/a&gt; (COM-11442)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11647"&gt;SparkFun OpenSegment Serial Display - 20mm (Blue)&lt;/a&gt; (COM-11647)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11646"&gt;SparkFun OpenSegment Serial Display - 20mm (Green)&lt;/a&gt; (COM-11646)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11648"&gt;SparkFun OpenSegment Serial Display - 20mm (White)&lt;/a&gt; (COM-11648)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11644"&gt;SparkFun OpenSegment Serial Display - 20mm (Red)&lt;/a&gt; (COM-11644)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11645"&gt;SparkFun OpenSegment Serial Display - 20mm (Yellow)&lt;/a&gt; (COM-11645)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13190"&gt;SparkFun OpenSegment Shield&lt;/a&gt; (DEV-13190)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="U$1" symbol="ATMEGAXX8-32PIN_NOPAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP32-08">
<connects>
<connect gate="U$1" pin="ADC6" pad="19"/>
<connect gate="U$1" pin="ADC7" pad="22"/>
<connect gate="U$1" pin="AGND" pad="21"/>
<connect gate="U$1" pin="AREF" pad="20"/>
<connect gate="U$1" pin="AVCC" pad="18"/>
<connect gate="U$1" pin="GND@3" pad="3"/>
<connect gate="U$1" pin="GND@5" pad="5"/>
<connect gate="U$1" pin="PB0(ICP)" pad="12"/>
<connect gate="U$1" pin="PB1(OC1A)" pad="13"/>
<connect gate="U$1" pin="PB2(SS/OC1B)" pad="14"/>
<connect gate="U$1" pin="PB3(MOSI/OC2)" pad="15"/>
<connect gate="U$1" pin="PB4(MISO)" pad="16"/>
<connect gate="U$1" pin="PB5(SCK)" pad="17"/>
<connect gate="U$1" pin="PB6(XTAL1/TOSC1)" pad="7"/>
<connect gate="U$1" pin="PB7(XTAL2/TOSC2)" pad="8"/>
<connect gate="U$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="U$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="U$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="U$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="U$1" pin="PC4(ADC4/SDA)" pad="27"/>
<connect gate="U$1" pin="PC5(ADC5/SCL)" pad="28"/>
<connect gate="U$1" pin="PC6(/RESET)" pad="29"/>
<connect gate="U$1" pin="PD0(RXD)" pad="30"/>
<connect gate="U$1" pin="PD1(TXD)" pad="31"/>
<connect gate="U$1" pin="PD2(INT0)" pad="32"/>
<connect gate="U$1" pin="PD3(INT1)" pad="1"/>
<connect gate="U$1" pin="PD4(XCK/T0)" pad="2"/>
<connect gate="U$1" pin="PD5(T1)" pad="9"/>
<connect gate="U$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="U$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="U$1" pin="VCC@4" pad="4"/>
<connect gate="U$1" pin="VCC@6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-09069" constant="no"/>
<attribute name="VALUE" value="ATMEGA328P_TQFP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="texas-custom">
<packages>
<package name="TSSOP14">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 14&lt;/b&gt;&lt;p&gt;
MAX3223-MAX3243.pdf</description>
<wire x1="-2.5146" y1="-2.2828" x2="2.5146" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="2.5146" y1="2.2828" x2="2.5146" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="2.5146" y1="2.2828" x2="-2.5146" y2="2.2828" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-2.2828" x2="-2.5146" y2="2.2828" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-2.0542" x2="2.286" y2="-2.0542" width="0.0508" layer="21"/>
<wire x1="2.286" y1="2.0542" x2="2.286" y2="-2.0542" width="0.0508" layer="21"/>
<wire x1="2.286" y1="2.0542" x2="-2.286" y2="2.0542" width="0.0508" layer="21"/>
<wire x1="-2.286" y1="-2.0542" x2="-2.286" y2="2.0542" width="0.0508" layer="21"/>
<circle x="-1.6256" y="-1.2192" radius="0.4572" width="0.1524" layer="21"/>
<smd name="1" x="-1.95" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-1.3" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-0.65" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="0" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="0.65" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="1.3" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="1.95" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="1.95" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="1.3" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="0.65" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="0" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="-0.65" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="-1.3" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="-1.95" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-2.8956" y="-2.0828" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.8862" y="-2.0828" size="1.016" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.0516" y1="-3.121" x2="-1.8484" y2="-2.2828" layer="51"/>
<rectangle x1="-1.4016" y1="-3.121" x2="-1.1984" y2="-2.2828" layer="51"/>
<rectangle x1="-0.7516" y1="-3.121" x2="-0.5484" y2="-2.2828" layer="51"/>
<rectangle x1="-0.1016" y1="-3.121" x2="0.1016" y2="-2.2828" layer="51"/>
<rectangle x1="0.5484" y1="-3.121" x2="0.7516" y2="-2.2828" layer="51"/>
<rectangle x1="1.1984" y1="-3.121" x2="1.4016" y2="-2.2828" layer="51"/>
<rectangle x1="1.8484" y1="-3.121" x2="2.0516" y2="-2.2828" layer="51"/>
<rectangle x1="1.8484" y1="2.2828" x2="2.0516" y2="3.121" layer="51"/>
<rectangle x1="1.1984" y1="2.2828" x2="1.4016" y2="3.121" layer="51"/>
<rectangle x1="0.5484" y1="2.2828" x2="0.7516" y2="3.121" layer="51"/>
<rectangle x1="-0.1016" y1="2.2828" x2="0.1016" y2="3.121" layer="51"/>
<rectangle x1="-0.7516" y1="2.2828" x2="-0.5484" y2="3.121" layer="51"/>
<rectangle x1="-1.4016" y1="2.2828" x2="-1.1984" y2="3.121" layer="51"/>
<rectangle x1="-2.0516" y1="2.2828" x2="-1.8484" y2="3.121" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="TXS0104">
<wire x1="0" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="17.78" y1="25.4" x2="0" y2="25.4" width="0.254" layer="94"/>
<wire x1="0" y1="25.4" x2="0" y2="12.7" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="17.78" y2="12.7" width="0.254" layer="94"/>
<pin name="A1" x="-5.08" y="10.16" length="middle"/>
<pin name="A2" x="-5.08" y="7.62" length="middle"/>
<pin name="A3" x="-5.08" y="5.08" length="middle"/>
<pin name="A4" x="-5.08" y="2.54" length="middle"/>
<pin name="B4" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="B3" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="B2" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="B1" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="VCCA" x="-5.08" y="22.86" length="middle"/>
<pin name="VCCB" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="GND" x="-5.08" y="20.32" length="middle"/>
<text x="0" y="30.48" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="27.94" size="1.778" layer="96">&gt;Value</text>
<pin name="OE" x="-5.08" y="17.78" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TXS0104" prefix="IC">
<description>TXS0104E 4-Bit Bidirectional Voltage-Level Translator for Open-Drain Push-Pull</description>
<gates>
<gate name="G$1" symbol="TXS0104" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP14">
<connects>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="A3" pad="4"/>
<connect gate="G$1" pin="A4" pad="5"/>
<connect gate="G$1" pin="B1" pad="13"/>
<connect gate="G$1" pin="B2" pad="12"/>
<connect gate="G$1" pin="B3" pad="11"/>
<connect gate="G$1" pin="B4" pad="10"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="OE" pad="8"/>
<connect gate="G$1" pin="VCCA" pad="1"/>
<connect gate="G$1" pin="VCCB" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Clocks">
<description>&lt;h3&gt;SparkFun Clocks, Oscillators and Resonators&lt;/h3&gt;
This library contains the real-time clocks, oscillators, resonators, and crystals we use. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CRYSTAL-SMD-5X3.2-4PAD">
<description>&lt;h3&gt;5x3.2mm SMD Crystal&lt;/h3&gt;
&lt;p&gt;Example: &lt;a href="https://www.sparkfun.com/products/94"&gt;16MHz SMD Crystal&lt;/a&gt; (&lt;a href="https://www.sparkfun.com/datasheets/Components/SPK-5032-16MHZ.pdf"&gt;Datasheet&lt;/a&gt;)&lt;/p&gt;</description>
<wire x1="-0.6" y1="1.7" x2="0.6" y2="1.7" width="0.2032" layer="21"/>
<wire x1="2.6" y1="0.3" x2="2.6" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.7" x2="-0.6" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="0.3" x2="-2.6" y2="-0.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.85" y="-1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="3" x="1.85" y="1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="4" x="-1.85" y="1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="2" x="1.85" y="-1.15" dx="1.9" dy="1.1" layer="1"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-1.6" x2="2.5" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.5" y1="-1.6" x2="2.5" y2="1.6" width="0.127" layer="51"/>
<wire x1="2.5" y1="1.6" x2="-2.5" y2="1.6" width="0.127" layer="51"/>
<polygon width="0.127" layer="51">
<vertex x="-2.5" y="1.6"/>
<vertex x="-2.5" y="0.8"/>
<vertex x="-1.3" y="0.8"/>
<vertex x="-1.3" y="1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="2.5" y="-1.6"/>
<vertex x="2.5" y="-0.8"/>
<vertex x="1.3" y="-0.8"/>
<vertex x="1.3" y="-1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="1.3" y="1.6"/>
<vertex x="1.3" y="0.8"/>
<vertex x="2.5" y="0.8"/>
<vertex x="2.5" y="1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="-1.3" y="-1.6"/>
<vertex x="-1.3" y="-0.8"/>
<vertex x="-2.5" y="-0.8"/>
<vertex x="-2.5" y="-1.6"/>
</polygon>
</package>
<package name="CRYSTAL-SMD-3.2X2.5MM">
<description>&lt;h3&gt;3.2 x 2.5mm SMD Crystal Package&lt;/h3&gt;
&lt;p&gt;Example: &lt;a href="http://www.digikey.com/product-search/en?keywords=SER3627TR-ND"&gt;SX-32S&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-0.4" y1="1.377" x2="0.4" y2="1.377" width="0.2032" layer="21"/>
<wire x1="-1.727" y1="-0.15" x2="-1.727" y2="0.15" width="0.2032" layer="21"/>
<wire x1="1.727" y1="0.15" x2="1.727" y2="-0.15" width="0.2032" layer="21"/>
<wire x1="0.4" y1="-1.377" x2="-0.4" y2="-1.377" width="0.2032" layer="21"/>
<rectangle x1="-1.6" y1="0.35" x2="-0.6" y2="1.15" layer="51"/>
<rectangle x1="0.6" y1="-1.15" x2="1.6" y2="-0.35" layer="51" rot="R180"/>
<rectangle x1="-1.6" y1="-1.15" x2="-0.6" y2="-0.35" layer="51"/>
<rectangle x1="0.6" y1="0.35" x2="1.6" y2="1.15" layer="51" rot="R180"/>
<smd name="1" x="-1.175" y="-0.875" dx="1.2" dy="1.1" layer="1" rot="R180"/>
<smd name="2" x="1.175" y="-0.875" dx="1.2" dy="1.1" layer="1"/>
<smd name="3" x="1.175" y="0.875" dx="1.2" dy="1.1" layer="1"/>
<smd name="4" x="-1.175" y="0.875" dx="1.2" dy="1.1" layer="1" rot="R180"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL-GND">
<description>&lt;h3&gt;Crystal with Ground pin&lt;/h3&gt;</description>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<text x="1.524" y="-1.524" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0" y1="-2.8" x2="0" y2="-1.6" width="0.1524" layer="94"/>
<text x="-1.27" y="-1.524" size="1.778" layer="95" font="vector" align="top-right">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTAL-GROUNDED" prefix="Y">
<description>&lt;h3&gt;Crystals w/ Ground Pin (Generic)&lt;/h3&gt;
&lt;p&gt;These are &lt;b&gt;passive&lt;/b&gt; quartz crystals, which can be used as a clock source for a microcontroller.&lt;/p&gt;
&lt;p&gt;Crystal's are usually two-terminal devices. A third terminal may be optionally connected to ground, as recommended by the manufaturer.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CRYSTAL-GND" x="0" y="0"/>
</gates>
<devices>
<device name="SMD-5X3.2" package="CRYSTAL-SMD-5X3.2-4PAD">
<connects>
<connect gate="G$1" pin="1" pad="3"/>
<connect gate="G$1" pin="2" pad="1"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-3.2X2.5" package="CRYSTAL-SMD-3.2X2.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="751xx" urn="urn:adsk.eagle:library:89">
<description>&lt;b&gt;75xxx Series Devices&lt;/b&gt;&lt;p&gt;

&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL08" urn="urn:adsk.eagle:footprint:3290/1" library_version="1">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="D_R-PDSO-G8" urn="urn:adsk.eagle:footprint:3291/1" library_version="1">
<description>&lt;b&gt;D (R-PDSO-G8)&lt;/b&gt;&lt;p&gt;
Source: http://focus.ti.com/lit/ds/slos063b/slos063b.pdf</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="21"/>
<smd name="2" x="-0.635" y="-2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="7" x="-0.635" y="2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="1" x="-1.905" y="-2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="3" x="0.635" y="-2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="4" x="1.905" y="-2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="8" x="-1.905" y="2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="6" x="0.635" y="2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="5" x="1.905" y="2.75" dx="0.6" dy="1.5" layer="1"/>
<text x="-2.667" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.937" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.2" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.2" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.2" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.2" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.2" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.2" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.2" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.2" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="DIL08" urn="urn:adsk.eagle:package:3298/1" type="box" library_version="1">
<description>Dual In Line Package</description>
</package3d>
<package3d name="D_R-PDSO-G8" urn="urn:adsk.eagle:package:3309/1" type="box" library_version="1">
<description>D (R-PDSO-G8)
Source: http://focus.ti.com/lit/ds/slos063b/slos063b.pdf</description>
</package3d>
</packages3d>
<symbols>
<symbol name="75176" urn="urn:adsk.eagle:symbol:3289/1" library_version="1">
<wire x1="-3.81" y1="4.064" x2="-3.81" y2="1.016" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.016" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-3.81" y2="4.064" width="0.254" layer="94"/>
<wire x1="-1.27" y1="4.064" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="1.016" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-5.08" x2="-1.27" y2="-3.556" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.556" x2="-1.27" y2="-6.731" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-6.731" x2="-3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-4.191" y1="5.08" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.429" width="0.1524" layer="94"/>
<wire x1="-5.461" y1="2.54" x2="-3.937" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="1.27" y2="3.81" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="0.254" y2="1.524" width="0.1524" layer="94"/>
<wire x1="0.254" y1="1.524" x2="0.254" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-4.191" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-3.429" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="-4.191" x2="1.27" y2="-4.191" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-4.191" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-2.54" x2="0.254" y2="-6.096" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-6.096" x2="-0.381" y2="-6.096" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-5.08" x2="-5.461" y2="-5.08" width="0.1524" layer="94"/>
<circle x="-0.762" y="1.524" radius="0.381" width="0.1524" layer="94"/>
<circle x="-2.54" y="-3.81" radius="0.381" width="0.1524" layer="94"/>
<circle x="-0.762" y="-6.096" radius="0.381" width="0.1524" layer="94"/>
<circle x="0.254" y="-2.54" radius="0.2839" width="0" layer="94"/>
<circle x="1.27" y="0" radius="0.2839" width="0" layer="94"/>
<text x="-10.16" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="R" x="-12.7" y="-5.08" length="short" direction="in"/>
<pin name="!RE" x="-12.7" y="-2.54" length="short" direction="in"/>
<pin name="DE" x="-12.7" y="5.08" length="short" direction="in"/>
<pin name="D" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="GND" x="12.7" y="-7.62" length="short" direction="pwr" rot="R180"/>
<pin name="VCC" x="12.7" y="5.08" length="short" direction="pwr" rot="R180"/>
<pin name="A" x="12.7" y="0" length="short" direction="out" rot="R180"/>
<pin name="B" x="12.7" y="-2.54" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="75176A" urn="urn:adsk.eagle:component:3310/1" prefix="IC" library_version="1">
<description>&lt;b&gt;DIFFERENTIAL BUS TRANSCEIVER&lt;/b&gt;&lt;p&gt;
Source: http://focus.ti.com/lit/ds/symlink/sn75176a.pdf</description>
<gates>
<gate name="G$1" symbol="75176" x="0" y="0"/>
</gates>
<devices>
<device name="P" package="DIL08">
<connects>
<connect gate="G$1" pin="!RE" pad="2"/>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="R" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3298/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="D_R-PDSO-G8">
<connects>
<connect gate="G$1" pin="!RE" pad="2"/>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="R" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3309/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-wago-508" urn="urn:adsk.eagle:library:196">
<description>&lt;b&gt;Wago Screw Clamps&lt;/b&gt;&lt;p&gt;
Grid 5.08 mm&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="W237-5P" urn="urn:adsk.eagle:footprint:10715/1" library_version="1">
<description>&lt;b&gt;WAGO SREW CLAMP&lt;/b&gt;</description>
<wire x1="-11.1506" y1="-1.651" x2="-9.144" y2="0.3556" width="0.254" layer="51"/>
<wire x1="-5.9944" y1="-1.6764" x2="-4.0894" y2="0.3556" width="0.254" layer="51"/>
<wire x1="-0.9144" y1="-1.6002" x2="0.889" y2="0.3556" width="0.254" layer="51"/>
<wire x1="4.0894" y1="-1.651" x2="6.096" y2="0.3556" width="0.254" layer="51"/>
<wire x1="9.2456" y1="-1.6764" x2="11.1506" y2="0.3556" width="0.254" layer="51"/>
<wire x1="-12.7" y1="-4.826" x2="12.7" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.7" y1="4.445" x2="12.7" y2="4.191" width="0.1524" layer="21"/>
<wire x1="12.7" y1="4.445" x2="2.54" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-4.826" x2="-12.7" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.413" x2="-11.049" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-11.049" y1="-2.413" x2="-9.271" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="-2.413" x2="-5.969" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="-2.413" x2="-0.889" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.413" x2="2.54" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.413" x2="-12.7" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="4.191" x2="12.7" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="4.191" x2="-12.7" y2="4.445" width="0.1524" layer="21"/>
<wire x1="12.7" y1="4.191" x2="12.7" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-2.413" x2="12.7" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="2.54" y1="4.445" x2="2.54" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="2.54" y1="4.445" x2="-12.7" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-2.413" x2="4.191" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.969" y1="-2.413" x2="9.271" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="11.049" y1="-2.413" x2="12.7" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-2.413" x2="-4.191" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-2.413" x2="0.889" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="4.191" y1="-2.413" x2="5.969" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="9.271" y1="-2.413" x2="11.049" y2="-2.413" width="0.1524" layer="51"/>
<circle x="-10.16" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="0" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-10.16" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<circle x="0" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<circle x="-5.08" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-5.08" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<circle x="5.08" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="5.08" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<circle x="10.16" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="10.16" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-10.16" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<pad name="3" x="0" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<pad name="4" x="5.08" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<text x="-12.065" y="1.27" size="1.27" layer="51" ratio="10">1</text>
<text x="-6.9342" y="1.2192" size="1.27" layer="51" ratio="10">2</text>
<text x="-1.8542" y="1.27" size="1.27" layer="51" ratio="10">3</text>
<text x="-11.43" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-11.43" y="-6.858" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="1.27" size="1.27" layer="51" ratio="10">4</text>
<text x="8.3058" y="1.2192" size="1.27" layer="51" ratio="10">5</text>
</package>
</packages>
<packages3d>
<package3d name="W237-5P" urn="urn:adsk.eagle:package:10736/1" type="box" library_version="1">
<description>WAGO SREW CLAMP</description>
</package3d>
</packages3d>
<symbols>
<symbol name="KL" urn="urn:adsk.eagle:symbol:10710/1" library_version="1">
<wire x1="1.778" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<circle x="0.889" y="0" radius="0.898" width="0.254" layer="94"/>
<text x="-1.27" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="KL+V" urn="urn:adsk.eagle:symbol:10711/1" library_version="1">
<wire x1="1.778" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<circle x="0.889" y="0" radius="0.898" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="W237-05P" urn="urn:adsk.eagle:component:10751/1" prefix="X" uservalue="yes" library_version="1">
<description>&lt;b&gt;WAGO SREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="0" addlevel="always"/>
<gate name="-2" symbol="KL" x="0" y="-5.08" addlevel="always"/>
<gate name="-3" symbol="KL" x="0" y="-10.16" addlevel="always"/>
<gate name="-4" symbol="KL" x="0" y="-15.24" addlevel="always"/>
<gate name="-5" symbol="KL+V" x="0" y="-20.32" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-5P">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
<connect gate="-3" pin="KL" pad="3"/>
<connect gate="-4" pin="KL" pad="4"/>
<connect gate="-5" pin="KL" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10736/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="SparkFun-RF" deviceset="RFM69H" device="CW-915" value="915MHz"/>
<part name="X1" library="con-coax" library_urn="urn:adsk.eagle:library:133" deviceset="BU-SMA-G" device=""/>
<part name="U3" library="SparkFun-IC-Power" deviceset="V_REG_317" device="SMD" value="1117"/>
<part name="X2" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-102" device="" package3d_urn="urn:adsk.eagle:package:10688/1"/>
<part name="C4" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="1uF"/>
<part name="D1" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="R3" library="SparkFun-Resistors" deviceset="0.27OHM" device="-0603-1/10W-1%" value="330"/>
<part name="D3" library="SparkFun-DiscreteSemi" deviceset="DIODE-SCHOTTKY" device="-BAT20J" value="BAT20J"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C7" library="SparkFun-Capacitors" deviceset="22UF" device="-1210-16V-20%"/>
<part name="C8" library="SparkFun-Capacitors" deviceset="2.2UF" device="-0805-25V-(+80/-20%)" value="2.2uF"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C5" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="10uF"/>
<part name="+3V7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="LOGO1" library="aestethics" deviceset="MUTEX-LOGO" device=""/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_03" device="POLAR"/>
<part name="FRAME1" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device=""/>
<part name="FRAME2" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device=""/>
<part name="MDL1" library="ESP8266-ESP12E" deviceset="ESP8266-12E" device="ESP8266-ESP12E"/>
<part name="R6" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R7" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R8" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R9" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R10" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="S1" library="SparkFun-Switches" deviceset="MOMENTARY-SWITCH-SPST" device="-SMD-6.0X3.5MM" value=""/>
<part name="J3" library="SparkFun-Connectors" deviceset="CONN_03" device=""/>
<part name="R11" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="U2" library="SparkFun-IC-Microcontroller" deviceset="ATMEGA328P_TQFP" device="" value="ATMEGA328P_TQFP"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC1" library="texas-custom" deviceset="TXS0104" device="" value="TXB0104"/>
<part name="J1" library="SparkFun-Connectors" deviceset="AVR_SPI_PROG_3X2" device="PTH"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R1" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="15k"/>
<part name="R2" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="1M"/>
<part name="Y1" library="SparkFun-Clocks" deviceset="CRYSTAL-GROUNDED" device="SMD-5X3.2" value="8MHz"/>
<part name="C1" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="12pF"/>
<part name="C2" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="12pF"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="Q1" library="SparkFun-DiscreteSemi" deviceset="MOSFET-NCH" device="-BSS138" value="BSS138"/>
<part name="Q2" library="SparkFun-DiscreteSemi" deviceset="MOSFET-NCH" device="-BSS138" value="BSS138"/>
<part name="R4" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R5" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R12" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R13" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="+3V10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="IC2" library="751xx" library_urn="urn:adsk.eagle:library:89" deviceset="75176A" device="D" package3d_urn="urn:adsk.eagle:package:3309/1" value="ST1480"/>
<part name="X3" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-103" device="" package3d_urn="urn:adsk.eagle:package:10691/1"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X4" library="con-wago-508" library_urn="urn:adsk.eagle:library:196" deviceset="W237-05P" device="" package3d_urn="urn:adsk.eagle:package:10736/1"/>
<part name="+3V12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="J4" library="SparkFun-Connectors" deviceset="CONN_03" device="POLAR"/>
<part name="Q3" library="SparkFun-DiscreteSemi" deviceset="MOSFET-NCH" device="-BSS138" value="BSS138"/>
<part name="R14" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R15" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="+3V13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="Q4" library="SparkFun-DiscreteSemi" deviceset="MOSFET-NCH" device="-BSS138" value="BSS138"/>
<part name="R16" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R17" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="+3V14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="Q5" library="SparkFun-DiscreteSemi" deviceset="MOSFET-NCH" device="-BSS138" value="BSS138"/>
<part name="R18" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R19" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="+3V15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="R20" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="220"/>
<part name="D2" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R21" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="27"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R22" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="220"/>
<part name="D4" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C3" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="100nF"/>
<part name="C6" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="100nF"/>
<part name="C9" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="1uF"/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="C10" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="1uF"/>
<part name="+3V17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J5" library="SparkFun-Connectors" deviceset="CONN_04X2" device=""/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="508" y="55.88"/>
<instance part="X1" gate="G1" x="541.02" y="48.26" rot="MR0"/>
<instance part="+3V5" gate="G$1" x="492.76" y="73.66"/>
<instance part="GND2" gate="1" x="525.78" y="38.1"/>
<instance part="GND7" gate="1" x="535.94" y="38.1"/>
<instance part="LOGO1" gate="G$1" x="556.26" y="-45.72"/>
<instance part="J2" gate="J$1" x="843.28" y="106.68" rot="R180"/>
<instance part="FRAME2" gate="G$1" x="287.02" y="-35.56"/>
<instance part="MDL1" gate="G$1" x="391.16" y="81.28"/>
<instance part="R6" gate="G$1" x="360.68" y="83.82" rot="R180"/>
<instance part="R7" gate="G$1" x="419.1" y="76.2"/>
<instance part="R8" gate="G$1" x="365.76" y="15.24" rot="R90"/>
<instance part="R9" gate="G$1" x="312.42" y="-2.54" rot="R90"/>
<instance part="+3V3" gate="G$1" x="347.98" y="88.9"/>
<instance part="+3V4" gate="G$1" x="431.8" y="81.28"/>
<instance part="+3V6" gate="G$1" x="365.76" y="25.4"/>
<instance part="+3V8" gate="G$1" x="312.42" y="7.62"/>
<instance part="R10" gate="G$1" x="419.1" y="68.58" rot="R180"/>
<instance part="GND21" gate="1" x="431.8" y="60.96"/>
<instance part="S1" gate="G$1" x="302.26" y="-10.16"/>
<instance part="J3" gate="J$1" x="378.46" y="5.08" rot="R180"/>
<instance part="R11" gate="G$1" x="365.76" y="-5.08" rot="R90"/>
<instance part="GND22" gate="1" x="365.76" y="-15.24"/>
<instance part="GND23" gate="1" x="294.64" y="-17.78"/>
<instance part="U2" gate="U$1" x="777.24" y="71.12"/>
<instance part="GND1" gate="1" x="746.76" y="38.1"/>
<instance part="IC1" gate="G$1" x="449.58" y="17.78" rot="MR0"/>
<instance part="J1" gate="G$1" x="495.3" y="10.16"/>
<instance part="P+2" gate="1" x="510.54" y="20.32"/>
<instance part="GND3" gate="1" x="510.54" y="-2.54"/>
<instance part="+3V1" gate="G$1" x="746.76" y="109.22"/>
<instance part="+3V9" gate="G$1" x="736.6" y="119.38"/>
<instance part="R1" gate="G$1" x="736.6" y="109.22" rot="R90"/>
<instance part="R2" gate="G$1" x="744.22" y="73.66" rot="R90"/>
<instance part="Y1" gate="G$1" x="736.6" y="73.66" rot="R270"/>
<instance part="C1" gate="G$1" x="736.6" y="58.42"/>
<instance part="C2" gate="G$1" x="723.9" y="58.42"/>
<instance part="GND4" gate="1" x="736.6" y="50.8"/>
<instance part="GND11" gate="1" x="723.9" y="50.8"/>
<instance part="Q1" gate="NMOS" x="868.68" y="73.66" rot="R270"/>
<instance part="Q2" gate="NMOS" x="868.68" y="30.48" rot="R270"/>
<instance part="R4" gate="G$1" x="858.52" y="78.74" rot="R90"/>
<instance part="R5" gate="G$1" x="878.84" y="78.74" rot="R90"/>
<instance part="R12" gate="G$1" x="858.52" y="35.56" rot="R90"/>
<instance part="R13" gate="G$1" x="878.84" y="35.56" rot="R90"/>
<instance part="+3V10" gate="G$1" x="858.52" y="91.44"/>
<instance part="+3V11" gate="G$1" x="858.52" y="48.26"/>
<instance part="P+3" gate="1" x="878.84" y="91.44" rot="MR0"/>
<instance part="P+4" gate="1" x="878.84" y="48.26" rot="MR0"/>
<instance part="IC2" gate="G$1" x="840.74" y="144.78"/>
<instance part="X3" gate="-1" x="886.46" y="134.62" rot="R180"/>
<instance part="X3" gate="-2" x="886.46" y="139.7" rot="R180"/>
<instance part="X3" gate="-3" x="886.46" y="144.78" rot="R180"/>
<instance part="GND15" gate="1" x="858.52" y="129.54"/>
<instance part="X4" gate="-1" x="797.56" y="-17.78" rot="R180"/>
<instance part="X4" gate="-2" x="797.56" y="-12.7" rot="R180"/>
<instance part="X4" gate="-3" x="797.56" y="-7.62" rot="R180"/>
<instance part="X4" gate="-4" x="797.56" y="-2.54" rot="R180"/>
<instance part="X4" gate="-5" x="797.56" y="2.54" rot="R180"/>
<instance part="+3V12" gate="G$1" x="855.98" y="160.02"/>
<instance part="J4" gate="J$1" x="843.28" y="86.36" rot="R180"/>
<instance part="Q3" gate="NMOS" x="868.68" y="-12.7" rot="R270"/>
<instance part="R14" gate="G$1" x="858.52" y="-7.62" rot="R90"/>
<instance part="R15" gate="G$1" x="878.84" y="-7.62" rot="R90"/>
<instance part="+3V13" gate="G$1" x="858.52" y="5.08"/>
<instance part="P+5" gate="1" x="878.84" y="5.08" rot="MR0"/>
<instance part="Q4" gate="NMOS" x="868.68" y="-58.42" rot="R270"/>
<instance part="R16" gate="G$1" x="858.52" y="-53.34" rot="R90"/>
<instance part="R17" gate="G$1" x="878.84" y="-53.34" rot="R90"/>
<instance part="+3V14" gate="G$1" x="858.52" y="-40.64"/>
<instance part="P+6" gate="1" x="878.84" y="-40.64" rot="MR0"/>
<instance part="Q5" gate="NMOS" x="868.68" y="-101.6" rot="R270"/>
<instance part="R18" gate="G$1" x="858.52" y="-96.52" rot="R90"/>
<instance part="R19" gate="G$1" x="878.84" y="-96.52" rot="R90"/>
<instance part="+3V15" gate="G$1" x="858.52" y="-83.82"/>
<instance part="P+7" gate="1" x="878.84" y="-83.82" rot="MR0"/>
<instance part="R20" gate="G$1" x="817.88" y="53.34" rot="R180"/>
<instance part="D2" gate="G$1" x="828.04" y="53.34" rot="R90"/>
<instance part="GND14" gate="1" x="835.66" y="45.72"/>
<instance part="R21" gate="G$1" x="876.3" y="124.46" rot="R90"/>
<instance part="GND16" gate="1" x="876.3" y="114.3"/>
<instance part="GND17" gate="1" x="406.4" y="60.96"/>
<instance part="R22" gate="G$1" x="360.68" y="76.2" rot="R180"/>
<instance part="D4" gate="G$1" x="340.36" y="76.2" rot="R270"/>
<instance part="GND18" gate="1" x="332.74" y="68.58"/>
<instance part="J5" gate="G$1" x="937.26" y="68.58"/>
<instance part="GND26" gate="1" x="947.42" y="58.42"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND@2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="520.7" y1="50.8" x2="525.78" y2="50.8" width="0.1524" layer="91"/>
<wire x1="525.78" y1="50.8" x2="525.78" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND@1"/>
<wire x1="525.78" y1="45.72" x2="525.78" y2="40.64" width="0.1524" layer="91"/>
<wire x1="520.7" y1="45.72" x2="525.78" y2="45.72" width="0.1524" layer="91"/>
<junction x="525.78" y="45.72"/>
</segment>
<segment>
<pinref part="X1" gate="G1" pin="2"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="538.48" y1="45.72" x2="535.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="535.94" y1="45.72" x2="535.94" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="365.76" y1="-10.16" x2="365.76" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="1"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="297.18" y1="-10.16" x2="294.64" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="294.64" y1="-10.16" x2="294.64" y2="-15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="AGND"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="751.84" y1="50.8" x2="746.76" y2="50.8" width="0.1524" layer="91"/>
<wire x1="746.76" y1="50.8" x2="746.76" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U2" gate="U$1" pin="GND@5"/>
<wire x1="746.76" y1="48.26" x2="746.76" y2="45.72" width="0.1524" layer="91"/>
<wire x1="746.76" y1="45.72" x2="746.76" y2="40.64" width="0.1524" layer="91"/>
<wire x1="751.84" y1="45.72" x2="746.76" y2="45.72" width="0.1524" layer="91"/>
<junction x="746.76" y="45.72"/>
<pinref part="U2" gate="U$1" pin="GND@3"/>
<wire x1="751.84" y1="48.26" x2="746.76" y2="48.26" width="0.1524" layer="91"/>
<junction x="746.76" y="48.26"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="454.66" y1="38.1" x2="462.28" y2="38.1" width="0.1524" layer="91"/>
<label x="462.28" y="38.1" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="6"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="505.46" y1="7.62" x2="510.54" y2="7.62" width="0.1524" layer="91"/>
<wire x1="510.54" y1="7.62" x2="510.54" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="736.6" y1="55.88" x2="736.6" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="723.9" y1="55.88" x2="723.9" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="853.44" y1="137.16" x2="858.52" y2="137.16" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="858.52" y1="132.08" x2="858.52" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="J$1" pin="3"/>
<wire x1="835.66" y1="104.14" x2="830.58" y2="104.14" width="0.1524" layer="91"/>
<label x="830.58" y="104.14" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="J$1" pin="3"/>
<wire x1="835.66" y1="83.82" x2="830.58" y2="83.82" width="0.1524" layer="91"/>
<label x="830.58" y="83.82" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="833.12" y1="53.34" x2="835.66" y2="53.34" width="0.1524" layer="91"/>
<wire x1="835.66" y1="53.34" x2="835.66" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="876.3" y1="119.38" x2="876.3" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="424.18" y1="68.58" x2="431.8" y2="68.58" width="0.1524" layer="91"/>
<wire x1="431.8" y1="68.58" x2="431.8" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MDL1" gate="G$1" pin="GND"/>
<wire x1="401.32" y1="71.12" x2="406.4" y2="71.12" width="0.1524" layer="91"/>
<wire x1="406.4" y1="71.12" x2="406.4" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="335.28" y1="76.2" x2="332.74" y2="76.2" width="0.1524" layer="91"/>
<wire x1="332.74" y1="76.2" x2="332.74" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="944.88" y1="73.66" x2="947.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="947.42" y1="73.66" x2="947.42" y2="71.12" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="8"/>
<wire x1="947.42" y1="71.12" x2="947.42" y2="68.58" width="0.1524" layer="91"/>
<wire x1="947.42" y1="68.58" x2="947.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="947.42" y1="66.04" x2="947.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="944.88" y1="66.04" x2="947.42" y2="66.04" width="0.1524" layer="91"/>
<junction x="947.42" y="66.04"/>
<pinref part="J5" gate="G$1" pin="6"/>
<wire x1="944.88" y1="68.58" x2="947.42" y2="68.58" width="0.1524" layer="91"/>
<junction x="947.42" y="68.58"/>
<pinref part="J5" gate="G$1" pin="4"/>
<wire x1="944.88" y1="71.12" x2="947.42" y2="71.12" width="0.1524" layer="91"/>
<junction x="947.42" y="71.12"/>
<pinref part="GND26" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="3.3V"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="495.3" y1="68.58" x2="492.76" y2="68.58" width="0.1524" layer="91"/>
<wire x1="492.76" y1="68.58" x2="492.76" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="365.76" y1="20.32" x2="365.76" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="312.42" y1="2.54" x2="312.42" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MDL1" gate="G$1" pin="VCC"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="368.3" y1="71.12" x2="347.98" y2="71.12" width="0.1524" layer="91"/>
<wire x1="347.98" y1="71.12" x2="347.98" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="347.98" y1="83.82" x2="347.98" y2="86.36" width="0.1524" layer="91"/>
<wire x1="355.6" y1="83.82" x2="347.98" y2="83.82" width="0.1524" layer="91"/>
<junction x="347.98" y="83.82"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VCCA"/>
<wire x1="454.66" y1="40.64" x2="459.74" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="OE"/>
<wire x1="459.74" y1="40.64" x2="462.28" y2="40.64" width="0.1524" layer="91"/>
<wire x1="454.66" y1="35.56" x2="459.74" y2="35.56" width="0.1524" layer="91"/>
<wire x1="459.74" y1="35.56" x2="459.74" y2="40.64" width="0.1524" layer="91"/>
<junction x="459.74" y="40.64"/>
<label x="462.28" y="40.64" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="VCC@6"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="751.84" y1="91.44" x2="746.76" y2="91.44" width="0.1524" layer="91"/>
<wire x1="746.76" y1="91.44" x2="746.76" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U2" gate="U$1" pin="VCC@4"/>
<wire x1="746.76" y1="93.98" x2="746.76" y2="96.52" width="0.1524" layer="91"/>
<wire x1="746.76" y1="96.52" x2="746.76" y2="106.68" width="0.1524" layer="91"/>
<wire x1="751.84" y1="93.98" x2="746.76" y2="93.98" width="0.1524" layer="91"/>
<junction x="746.76" y="93.98"/>
<pinref part="U2" gate="U$1" pin="AVCC"/>
<wire x1="751.84" y1="96.52" x2="746.76" y2="96.52" width="0.1524" layer="91"/>
<junction x="746.76" y="96.52"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="736.6" y1="114.3" x2="736.6" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q1" gate="NMOS" pin="G"/>
<wire x1="866.14" y1="78.74" x2="866.14" y2="86.36" width="0.1524" layer="91"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
<wire x1="866.14" y1="86.36" x2="858.52" y2="86.36" width="0.1524" layer="91"/>
<wire x1="858.52" y1="86.36" x2="858.52" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="858.52" y1="83.82" x2="858.52" y2="86.36" width="0.1524" layer="91"/>
<junction x="858.52" y="86.36"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<wire x1="858.52" y1="40.64" x2="858.52" y2="43.18" width="0.1524" layer="91"/>
<pinref part="Q2" gate="NMOS" pin="G"/>
<wire x1="858.52" y1="43.18" x2="858.52" y2="45.72" width="0.1524" layer="91"/>
<wire x1="866.14" y1="35.56" x2="866.14" y2="43.18" width="0.1524" layer="91"/>
<wire x1="866.14" y1="43.18" x2="858.52" y2="43.18" width="0.1524" layer="91"/>
<junction x="858.52" y="43.18"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
<wire x1="853.44" y1="149.86" x2="855.98" y2="149.86" width="0.1524" layer="91"/>
<wire x1="855.98" y1="149.86" x2="855.98" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q3" gate="NMOS" pin="G"/>
<wire x1="866.14" y1="-7.62" x2="866.14" y2="0" width="0.1524" layer="91"/>
<pinref part="+3V13" gate="G$1" pin="+3V3"/>
<wire x1="866.14" y1="0" x2="858.52" y2="0" width="0.1524" layer="91"/>
<wire x1="858.52" y1="0" x2="858.52" y2="2.54" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="858.52" y1="-2.54" x2="858.52" y2="0" width="0.1524" layer="91"/>
<junction x="858.52" y="0"/>
</segment>
<segment>
<pinref part="Q4" gate="NMOS" pin="G"/>
<wire x1="866.14" y1="-53.34" x2="866.14" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="+3V14" gate="G$1" pin="+3V3"/>
<wire x1="866.14" y1="-45.72" x2="858.52" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="858.52" y1="-45.72" x2="858.52" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="858.52" y1="-48.26" x2="858.52" y2="-45.72" width="0.1524" layer="91"/>
<junction x="858.52" y="-45.72"/>
</segment>
<segment>
<pinref part="Q5" gate="NMOS" pin="G"/>
<wire x1="866.14" y1="-96.52" x2="866.14" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="+3V15" gate="G$1" pin="+3V3"/>
<wire x1="866.14" y1="-88.9" x2="858.52" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="858.52" y1="-88.9" x2="858.52" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="858.52" y1="-91.44" x2="858.52" y2="-88.9" width="0.1524" layer="91"/>
<junction x="858.52" y="-88.9"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="424.18" y1="76.2" x2="431.8" y2="76.2" width="0.1524" layer="91"/>
<wire x1="431.8" y1="76.2" x2="431.8" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="ANT"/>
<pinref part="X1" gate="G1" pin="1"/>
<wire x1="520.7" y1="48.26" x2="538.48" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="2"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="307.34" y1="-10.16" x2="312.42" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="312.42" y1="-10.16" x2="312.42" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="312.42" y1="-10.16" x2="317.5" y2="-10.16" width="0.1524" layer="91"/>
<junction x="312.42" y="-10.16"/>
<label x="317.5" y="-10.16" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="MDL1" gate="G$1" pin="REST"/>
<wire x1="368.3" y1="88.9" x2="363.22" y2="88.9" width="0.1524" layer="91"/>
<label x="363.22" y="88.9" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SS_3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="NSS"/>
<wire x1="495.3" y1="53.34" x2="492.76" y2="53.34" width="0.1524" layer="91"/>
<label x="492.76" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PB2(SS/OC1B)"/>
<wire x1="800.1" y1="50.8" x2="805.18" y2="50.8" width="0.1524" layer="91"/>
<label x="805.18" y="50.8" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="MOSI_3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="MOSI"/>
<wire x1="495.3" y1="58.42" x2="492.76" y2="58.42" width="0.1524" layer="91"/>
<label x="492.76" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="A2"/>
<wire x1="454.66" y1="25.4" x2="457.2" y2="25.4" width="0.1524" layer="91"/>
<label x="457.2" y="25.4" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PB3(MOSI/OC2)"/>
<wire x1="800.1" y1="48.26" x2="805.18" y2="48.26" width="0.1524" layer="91"/>
<label x="805.18" y="48.26" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="MISO_3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="MISO"/>
<wire x1="495.3" y1="60.96" x2="492.76" y2="60.96" width="0.1524" layer="91"/>
<label x="492.76" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="A1"/>
<wire x1="454.66" y1="27.94" x2="457.2" y2="27.94" width="0.1524" layer="91"/>
<label x="457.2" y="27.94" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PB4(MISO)"/>
<wire x1="800.1" y1="45.72" x2="805.18" y2="45.72" width="0.1524" layer="91"/>
<label x="805.18" y="45.72" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="SCK_3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCK"/>
<wire x1="495.3" y1="55.88" x2="492.76" y2="55.88" width="0.1524" layer="91"/>
<label x="492.76" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="A3"/>
<wire x1="454.66" y1="22.86" x2="457.2" y2="22.86" width="0.1524" layer="91"/>
<label x="457.2" y="22.86" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PB5(SCK)"/>
<wire x1="800.1" y1="43.18" x2="805.18" y2="43.18" width="0.1524" layer="91"/>
<label x="805.18" y="43.18" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="INT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DIO0"/>
<wire x1="520.7" y1="68.58" x2="523.24" y2="68.58" width="0.1524" layer="91"/>
<label x="523.24" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PD2(INT0)"/>
<wire x1="800.1" y1="73.66" x2="805.18" y2="73.66" width="0.1524" layer="91"/>
<label x="805.18" y="73.66" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="RX_ESP" class="0">
<segment>
<pinref part="MDL1" gate="G$1" pin="RXD"/>
<wire x1="401.32" y1="86.36" x2="406.4" y2="86.36" width="0.1524" layer="91"/>
<label x="406.4" y="86.36" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="J$1" pin="2"/>
<wire x1="835.66" y1="86.36" x2="830.58" y2="86.36" width="0.1524" layer="91"/>
<label x="830.58" y="86.36" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PD4(XCK/T0)"/>
<wire x1="800.1" y1="68.58" x2="805.18" y2="68.58" width="0.1524" layer="91"/>
<label x="805.18" y="68.58" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="TX_ESP" class="0">
<segment>
<pinref part="MDL1" gate="G$1" pin="TXD"/>
<wire x1="401.32" y1="88.9" x2="406.4" y2="88.9" width="0.1524" layer="91"/>
<label x="406.4" y="88.9" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="J$1" pin="1"/>
<wire x1="835.66" y1="88.9" x2="830.58" y2="88.9" width="0.1524" layer="91"/>
<label x="830.58" y="88.9" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PD3(INT1)"/>
<wire x1="800.1" y1="71.12" x2="805.18" y2="71.12" width="0.1524" layer="91"/>
<label x="805.18" y="71.12" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="CH_PD" class="0">
<segment>
<pinref part="MDL1" gate="G$1" pin="CH_PD"/>
<wire x1="368.3" y1="83.82" x2="365.76" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="J3" gate="J$1" pin="3"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="370.84" y1="2.54" x2="365.76" y2="2.54" width="0.1524" layer="91"/>
<wire x1="365.76" y1="2.54" x2="365.76" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="J3" gate="J$1" pin="1"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="370.84" y1="7.62" x2="365.76" y2="7.62" width="0.1524" layer="91"/>
<wire x1="365.76" y1="7.62" x2="365.76" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GPIO0" class="0">
<segment>
<pinref part="J3" gate="J$1" pin="2"/>
<wire x1="370.84" y1="5.08" x2="363.22" y2="5.08" width="0.1524" layer="91"/>
<label x="363.22" y="5.08" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MDL1" gate="G$1" pin="GPIO0"/>
<wire x1="401.32" y1="78.74" x2="406.4" y2="78.74" width="0.1524" layer="91"/>
<label x="406.4" y="78.74" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VCCB"/>
<wire x1="426.72" y1="40.64" x2="421.64" y2="40.64" width="0.1524" layer="91"/>
<label x="421.64" y="40.64" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="505.46" y1="12.7" x2="510.54" y2="12.7" width="0.1524" layer="91"/>
<wire x1="510.54" y1="12.7" x2="510.54" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="878.84" y1="83.82" x2="878.84" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="878.84" y1="40.64" x2="878.84" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="878.84" y1="-2.54" x2="878.84" y2="2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="P+6" gate="1" pin="+5V"/>
<wire x1="878.84" y1="-48.26" x2="878.84" y2="-43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="P+7" gate="1" pin="+5V"/>
<wire x1="878.84" y1="-91.44" x2="878.84" y2="-86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="505.46" y1="10.16" x2="513.08" y2="10.16" width="0.1524" layer="91"/>
<label x="513.08" y="10.16" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="B2"/>
<wire x1="426.72" y1="25.4" x2="424.18" y2="25.4" width="0.1524" layer="91"/>
<label x="424.18" y="25.4" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="487.68" y1="10.16" x2="482.6" y2="10.16" width="0.1524" layer="91"/>
<label x="482.6" y="10.16" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="B3"/>
<wire x1="426.72" y1="22.86" x2="424.18" y2="22.86" width="0.1524" layer="91"/>
<label x="424.18" y="22.86" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="487.68" y1="12.7" x2="482.6" y2="12.7" width="0.1524" layer="91"/>
<label x="482.6" y="12.7" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="B1"/>
<wire x1="426.72" y1="27.94" x2="424.18" y2="27.94" width="0.1524" layer="91"/>
<label x="424.18" y="27.94" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ARD_RST" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="5"/>
<wire x1="487.68" y1="7.62" x2="482.6" y2="7.62" width="0.1524" layer="91"/>
<label x="482.6" y="7.62" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PC6(/RESET)"/>
<wire x1="751.84" y1="101.6" x2="736.6" y2="101.6" width="0.1524" layer="91"/>
<label x="731.52" y="101.6" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="736.6" y1="101.6" x2="731.52" y2="101.6" width="0.1524" layer="91"/>
<wire x1="736.6" y1="104.14" x2="736.6" y2="101.6" width="0.1524" layer="91"/>
<junction x="736.6" y="101.6"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PB6(XTAL1/TOSC1)"/>
<wire x1="751.84" y1="76.2" x2="749.3" y2="76.2" width="0.1524" layer="91"/>
<wire x1="749.3" y1="76.2" x2="749.3" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="749.3" y1="81.28" x2="744.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="744.22" y1="81.28" x2="736.6" y2="81.28" width="0.1524" layer="91"/>
<wire x1="736.6" y1="81.28" x2="723.9" y2="81.28" width="0.1524" layer="91"/>
<wire x1="723.9" y1="81.28" x2="723.9" y2="63.5" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="736.6" y1="76.2" x2="736.6" y2="81.28" width="0.1524" layer="91"/>
<junction x="736.6" y="81.28"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="744.22" y1="78.74" x2="744.22" y2="81.28" width="0.1524" layer="91"/>
<junction x="744.22" y="81.28"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PB7(XTAL2/TOSC2)"/>
<wire x1="751.84" y1="71.12" x2="749.3" y2="71.12" width="0.1524" layer="91"/>
<wire x1="749.3" y1="71.12" x2="749.3" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="749.3" y1="66.04" x2="744.22" y2="66.04" width="0.1524" layer="91"/>
<wire x1="744.22" y1="66.04" x2="744.22" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="736.6" y1="63.5" x2="736.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="736.6" y1="66.04" x2="736.6" y2="71.12" width="0.1524" layer="91"/>
<wire x1="744.22" y1="66.04" x2="736.6" y2="66.04" width="0.1524" layer="91"/>
<junction x="744.22" y="66.04"/>
<junction x="736.6" y="66.04"/>
</segment>
</net>
<net name="RX_ARD" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PD0(RXD)"/>
<wire x1="800.1" y1="78.74" x2="805.18" y2="78.74" width="0.1524" layer="91"/>
<label x="805.18" y="78.74" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="J$1" pin="2"/>
<wire x1="835.66" y1="106.68" x2="830.58" y2="106.68" width="0.1524" layer="91"/>
<label x="830.58" y="106.68" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="R"/>
<wire x1="828.04" y1="139.7" x2="820.42" y2="139.7" width="0.1524" layer="91"/>
<label x="820.42" y="139.7" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX_ARD" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PD1(TXD)"/>
<wire x1="800.1" y1="76.2" x2="805.18" y2="76.2" width="0.1524" layer="91"/>
<label x="805.18" y="76.2" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="J$1" pin="1"/>
<wire x1="835.66" y1="109.22" x2="830.58" y2="109.22" width="0.1524" layer="91"/>
<label x="830.58" y="109.22" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="D"/>
<wire x1="828.04" y1="147.32" x2="820.42" y2="147.32" width="0.1524" layer="91"/>
<label x="820.42" y="147.32" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DIR" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="!RE"/>
<wire x1="828.04" y1="142.24" x2="825.5" y2="142.24" width="0.1524" layer="91"/>
<wire x1="825.5" y1="142.24" x2="825.5" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="DE"/>
<wire x1="825.5" y1="149.86" x2="825.5" y2="154.94" width="0.1524" layer="91"/>
<wire x1="828.04" y1="149.86" x2="825.5" y2="149.86" width="0.1524" layer="91"/>
<junction x="825.5" y="149.86"/>
<label x="825.5" y="154.94" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PC5(ADC5/SCL)"/>
<wire x1="800.1" y1="88.9" x2="805.18" y2="88.9" width="0.1524" layer="91"/>
<label x="805.18" y="88.9" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="B-" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="B"/>
<wire x1="853.44" y1="142.24" x2="876.3" y2="142.24" width="0.1524" layer="91"/>
<wire x1="876.3" y1="142.24" x2="876.3" y2="139.7" width="0.1524" layer="91"/>
<pinref part="X3" gate="-2" pin="KL"/>
<wire x1="876.3" y1="139.7" x2="881.38" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A+" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="A"/>
<pinref part="X3" gate="-3" pin="KL"/>
<wire x1="853.44" y1="144.78" x2="881.38" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TXS1" class="0">
<segment>
<pinref part="Q2" gate="NMOS" pin="S"/>
<wire x1="863.6" y1="27.94" x2="858.52" y2="27.94" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="858.52" y1="27.94" x2="853.44" y2="27.94" width="0.1524" layer="91"/>
<wire x1="858.52" y1="30.48" x2="858.52" y2="27.94" width="0.1524" layer="91"/>
<junction x="858.52" y="27.94"/>
<label x="853.44" y="27.94" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PC1(ADC1)"/>
<wire x1="800.1" y1="99.06" x2="805.18" y2="99.06" width="0.1524" layer="91"/>
<label x="805.18" y="99.06" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="TXS1_5V" class="0">
<segment>
<pinref part="Q2" gate="NMOS" pin="D"/>
<wire x1="873.76" y1="27.94" x2="878.84" y2="27.94" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="878.84" y1="27.94" x2="883.92" y2="27.94" width="0.1524" layer="91"/>
<wire x1="878.84" y1="30.48" x2="878.84" y2="27.94" width="0.1524" layer="91"/>
<junction x="878.84" y="27.94"/>
<label x="883.92" y="27.94" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="X4" gate="-4" pin="KL"/>
<wire x1="792.48" y1="-2.54" x2="787.4" y2="-2.54" width="0.1524" layer="91"/>
<label x="787.4" y="-2.54" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TXS2" class="0">
<segment>
<pinref part="Q3" gate="NMOS" pin="S"/>
<wire x1="863.6" y1="-15.24" x2="858.52" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="858.52" y1="-15.24" x2="853.44" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="858.52" y1="-12.7" x2="858.52" y2="-15.24" width="0.1524" layer="91"/>
<junction x="858.52" y="-15.24"/>
<label x="853.44" y="-15.24" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PC2(ADC2)"/>
<wire x1="800.1" y1="96.52" x2="805.18" y2="96.52" width="0.1524" layer="91"/>
<label x="805.18" y="96.52" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="TXS2_5V" class="0">
<segment>
<pinref part="Q3" gate="NMOS" pin="D"/>
<wire x1="873.76" y1="-15.24" x2="878.84" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="878.84" y1="-15.24" x2="883.92" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="878.84" y1="-12.7" x2="878.84" y2="-15.24" width="0.1524" layer="91"/>
<junction x="878.84" y="-15.24"/>
<label x="883.92" y="-15.24" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="X4" gate="-3" pin="KL"/>
<wire x1="792.48" y1="-7.62" x2="787.4" y2="-7.62" width="0.1524" layer="91"/>
<label x="787.4" y="-7.62" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TXS3" class="0">
<segment>
<pinref part="Q4" gate="NMOS" pin="S"/>
<wire x1="863.6" y1="-60.96" x2="858.52" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="858.52" y1="-58.42" x2="858.52" y2="-60.96" width="0.1524" layer="91"/>
<junction x="858.52" y="-60.96"/>
<wire x1="858.52" y1="-60.96" x2="853.44" y2="-60.96" width="0.1524" layer="91"/>
<label x="853.44" y="-60.96" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PC3(ADC3)"/>
<wire x1="800.1" y1="93.98" x2="805.18" y2="93.98" width="0.1524" layer="91"/>
<label x="805.18" y="93.98" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="TXS3_5V" class="0">
<segment>
<pinref part="Q4" gate="NMOS" pin="D"/>
<wire x1="873.76" y1="-60.96" x2="878.84" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="878.84" y1="-60.96" x2="883.92" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="878.84" y1="-58.42" x2="878.84" y2="-60.96" width="0.1524" layer="91"/>
<junction x="878.84" y="-60.96"/>
<label x="883.92" y="-60.96" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="X4" gate="-2" pin="KL"/>
<wire x1="792.48" y1="-12.7" x2="787.4" y2="-12.7" width="0.1524" layer="91"/>
<label x="787.4" y="-12.7" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TXS4" class="0">
<segment>
<pinref part="Q5" gate="NMOS" pin="S"/>
<wire x1="863.6" y1="-104.14" x2="858.52" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="858.52" y1="-104.14" x2="853.44" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="858.52" y1="-101.6" x2="858.52" y2="-104.14" width="0.1524" layer="91"/>
<junction x="858.52" y="-104.14"/>
<label x="853.44" y="-104.14" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PC4(ADC4/SDA)"/>
<wire x1="800.1" y1="91.44" x2="805.18" y2="91.44" width="0.1524" layer="91"/>
<label x="805.18" y="91.44" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="TXS4_5V" class="0">
<segment>
<pinref part="Q5" gate="NMOS" pin="D"/>
<wire x1="873.76" y1="-104.14" x2="878.84" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="878.84" y1="-104.14" x2="883.92" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="878.84" y1="-101.6" x2="878.84" y2="-104.14" width="0.1524" layer="91"/>
<junction x="878.84" y="-104.14"/>
<label x="883.92" y="-104.14" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="X4" gate="-1" pin="KL"/>
<wire x1="792.48" y1="-17.78" x2="787.4" y2="-17.78" width="0.1524" layer="91"/>
<label x="787.4" y="-17.78" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RXS_5V" class="0">
<segment>
<pinref part="Q1" gate="NMOS" pin="D"/>
<wire x1="873.76" y1="71.12" x2="878.84" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="878.84" y1="73.66" x2="878.84" y2="71.12" width="0.1524" layer="91"/>
<junction x="878.84" y="71.12"/>
<wire x1="878.84" y1="71.12" x2="883.92" y2="71.12" width="0.1524" layer="91"/>
<label x="883.92" y="71.12" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="X4" gate="-5" pin="KL"/>
<wire x1="792.48" y1="2.54" x2="787.4" y2="2.54" width="0.1524" layer="91"/>
<label x="787.4" y="2.54" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RXS" class="0">
<segment>
<pinref part="Q1" gate="NMOS" pin="S"/>
<wire x1="863.6" y1="71.12" x2="858.52" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="858.52" y1="73.66" x2="858.52" y2="71.12" width="0.1524" layer="91"/>
<junction x="858.52" y="71.12"/>
<wire x1="858.52" y1="71.12" x2="853.44" y2="71.12" width="0.1524" layer="91"/>
<label x="853.44" y="71.12" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="U$1" pin="PC0(ADC0)"/>
<wire x1="800.1" y1="101.6" x2="805.18" y2="101.6" width="0.1524" layer="91"/>
<label x="805.18" y="101.6" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="825.5" y1="53.34" x2="822.96" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="X3" gate="-1" pin="KL"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="881.38" y1="134.62" x2="876.3" y2="134.62" width="0.1524" layer="91"/>
<wire x1="876.3" y1="134.62" x2="876.3" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="MDL1" gate="G$1" pin="GPIO2"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="401.32" y1="76.2" x2="414.02" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="MDL1" gate="G$1" pin="GPIO15"/>
<wire x1="401.32" y1="73.66" x2="408.94" y2="73.66" width="0.1524" layer="91"/>
<wire x1="408.94" y1="73.66" x2="408.94" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="408.94" y1="68.58" x2="414.02" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="342.9" y1="76.2" x2="355.6" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="MDL1" gate="G$1" pin="GPIO12"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="368.3" y1="76.2" x2="365.76" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PB1(OC1A)"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="800.1" y1="53.34" x2="812.8" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EN1" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PD5(T1)"/>
<wire x1="800.1" y1="66.04" x2="805.18" y2="66.04" width="0.1524" layer="91"/>
<label x="805.18" y="66.04" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="7"/>
<wire x1="929.64" y1="66.04" x2="927.1" y2="66.04" width="0.1524" layer="91"/>
<label x="927.1" y="66.04" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="EN2" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PD6(AIN0)"/>
<wire x1="800.1" y1="63.5" x2="805.18" y2="63.5" width="0.1524" layer="91"/>
<label x="805.18" y="63.5" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="5"/>
<wire x1="929.64" y1="68.58" x2="927.1" y2="68.58" width="0.1524" layer="91"/>
<label x="927.1" y="68.58" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="EN3" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PD7(AIN1)"/>
<wire x1="800.1" y1="60.96" x2="805.18" y2="60.96" width="0.1524" layer="91"/>
<label x="805.18" y="60.96" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="3"/>
<wire x1="929.64" y1="71.12" x2="927.1" y2="71.12" width="0.1524" layer="91"/>
<label x="927.1" y="71.12" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="EN4" class="0">
<segment>
<pinref part="U2" gate="U$1" pin="PB0(ICP)"/>
<wire x1="800.1" y1="55.88" x2="805.18" y2="55.88" width="0.1524" layer="91"/>
<label x="805.18" y="55.88" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="929.64" y1="73.66" x2="927.1" y2="73.66" width="0.1524" layer="91"/>
<label x="927.1" y="73.66" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="U3" gate="G$1" x="139.7" y="139.7"/>
<instance part="X2" gate="-1" x="99.06" y="139.7"/>
<instance part="X2" gate="-2" x="99.06" y="134.62"/>
<instance part="C4" gate="G$1" x="205.74" y="60.96"/>
<instance part="D1" gate="G$1" x="152.4" y="121.92"/>
<instance part="R3" gate="G$1" x="152.4" y="132.08" rot="R90"/>
<instance part="D3" gate="G$1" x="109.22" y="139.7"/>
<instance part="GND5" gate="1" x="106.68" y="127"/>
<instance part="GND6" gate="1" x="139.7" y="127"/>
<instance part="GND8" gate="1" x="152.4" y="109.22"/>
<instance part="C7" gate="G$1" x="160.02" y="132.08"/>
<instance part="C8" gate="G$1" x="121.92" y="132.08"/>
<instance part="GND9" gate="1" x="121.92" y="121.92"/>
<instance part="GND10" gate="1" x="160.02" y="121.92"/>
<instance part="+3V2" gate="G$1" x="205.74" y="71.12"/>
<instance part="GND12" gate="1" x="205.74" y="53.34"/>
<instance part="C5" gate="G$1" x="218.44" y="60.96"/>
<instance part="+3V7" gate="G$1" x="218.44" y="71.12"/>
<instance part="GND13" gate="1" x="218.44" y="53.34"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="P+1" gate="1" x="121.92" y="147.32"/>
<instance part="C3" gate="G$1" x="175.26" y="60.96"/>
<instance part="C6" gate="G$1" x="165.1" y="60.96"/>
<instance part="C9" gate="G$1" x="154.94" y="60.96"/>
<instance part="GND19" gate="1" x="175.26" y="53.34"/>
<instance part="GND20" gate="1" x="165.1" y="53.34"/>
<instance part="GND24" gate="1" x="154.94" y="53.34"/>
<instance part="+3V16" gate="G$1" x="160.02" y="78.74"/>
<instance part="P+8" gate="1" x="175.26" y="78.74"/>
<instance part="C10" gate="G$1" x="195.58" y="60.96"/>
<instance part="+3V17" gate="G$1" x="195.58" y="71.12"/>
<instance part="GND25" gate="1" x="195.58" y="53.34"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="X2" gate="-2" pin="KL"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="104.14" y1="134.62" x2="106.68" y2="134.62" width="0.1524" layer="91"/>
<wire x1="106.68" y1="134.62" x2="106.68" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="ADJ"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="139.7" y1="132.08" x2="139.7" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="152.4" y1="116.84" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="121.92" y1="129.54" x2="121.92" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="160.02" y1="129.54" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="205.74" y1="58.42" x2="205.74" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="218.44" y1="58.42" x2="218.44" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="154.94" y1="58.42" x2="154.94" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="165.1" y1="58.42" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="175.26" y1="58.42" x2="175.26" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="195.58" y1="58.42" x2="195.58" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="205.74" y1="66.04" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="218.44" y1="66.04" x2="218.44" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="OUT"/>
<wire x1="147.32" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="152.4" y1="139.7" x2="160.02" y2="139.7" width="0.1524" layer="91"/>
<wire x1="160.02" y1="139.7" x2="167.64" y2="139.7" width="0.1524" layer="91"/>
<wire x1="160.02" y1="137.16" x2="160.02" y2="139.7" width="0.1524" layer="91"/>
<junction x="160.02" y="139.7"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="152.4" y1="137.16" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<junction x="152.4" y="139.7"/>
<label x="167.64" y="139.7" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="+3V16" gate="G$1" pin="+3V3"/>
<wire x1="160.02" y1="71.12" x2="160.02" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="160.02" y1="71.12" x2="154.94" y2="71.12" width="0.1524" layer="91"/>
<wire x1="154.94" y1="71.12" x2="154.94" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="165.1" y1="66.04" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<wire x1="165.1" y1="71.12" x2="160.02" y2="71.12" width="0.1524" layer="91"/>
<junction x="160.02" y="71.12"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<pinref part="+3V17" gate="G$1" pin="+3V3"/>
<wire x1="195.58" y1="66.04" x2="195.58" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="152.4" y1="127" x2="152.4" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="X2" gate="-1" pin="KL"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="104.14" y1="139.7" x2="106.68" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="U3" gate="G$1" pin="IN"/>
<wire x1="111.76" y1="139.7" x2="121.92" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="121.92" y1="139.7" x2="132.08" y2="139.7" width="0.1524" layer="91"/>
<wire x1="121.92" y1="137.16" x2="121.92" y2="139.7" width="0.1524" layer="91"/>
<junction x="121.92" y="139.7"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="121.92" y1="144.78" x2="121.92" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+8" gate="1" pin="+5V"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="175.26" y1="76.2" x2="175.26" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
