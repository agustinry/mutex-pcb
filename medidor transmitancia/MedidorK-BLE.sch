<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.3.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="CC1310Misc">
<packages>
<package name="VQFN48-7X7">
<smd name="EGP" x="0" y="0" dx="5.15" dy="5.15" layer="1"/>
<smd name="43" x="-0.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="44" x="-0.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="45" x="-1.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="46" x="-1.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="47" x="-2.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="48" x="-2.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="42" x="0.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="41" x="0.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="40" x="1.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="39" x="1.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="38" x="2.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="37" x="2.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="17" x="-0.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="16" x="-1.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="15" x="-1.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="14" x="-2.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="13" x="-2.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="18" x="-0.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="19" x="0.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="20" x="0.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="21" x="1.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="22" x="1.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="23" x="2.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="24" x="2.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="7" x="-3.475" y="-0.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="8" x="-3.475" y="-0.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="9" x="-3.475" y="-1.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="10" x="-3.475" y="-1.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="11" x="-3.475" y="-2.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="12" x="-3.475" y="-2.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="6" x="-3.475" y="0.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="5" x="-3.475" y="0.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="4" x="-3.475" y="1.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="3" x="-3.475" y="1.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="2" x="-3.475" y="2.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="1" x="-3.475" y="2.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="30" x="3.475" y="-0.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="29" x="3.475" y="-0.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="28" x="3.475" y="-1.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="27" x="3.475" y="-1.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="26" x="3.475" y="-2.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="25" x="3.475" y="-2.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="31" x="3.475" y="0.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="32" x="3.475" y="0.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="33" x="3.475" y="1.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="34" x="3.475" y="1.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="35" x="3.475" y="2.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="36" x="3.475" y="2.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<wire x1="-2.8575" y1="3.4925" x2="-3.4925" y2="2.8575" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="2.8575" x2="-3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-3.4925" x2="3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-3.4925" x2="3.4925" y2="3.4925" width="0.127" layer="21"/>
<wire x1="3.4925" y1="3.4925" x2="-2.8575" y2="3.4925" width="0.127" layer="21"/>
<text x="-3.81" y="6.35" size="1.27" layer="25">&gt;Name</text>
<text x="-3.81" y="5.08" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="2X05-0.05-SMD">
<smd name="1" x="0" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="3" x="1.27" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="5" x="2.54" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="7" x="3.81" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="9" x="5.08" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="10" x="5.08" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<smd name="8" x="3.81" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<smd name="6" x="2.54" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<smd name="4" x="1.27" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<smd name="2" x="0" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<wire x1="-1.27" y1="0" x2="6.35" y2="0" width="0.127" layer="21"/>
<wire x1="6.35" y1="0" x2="6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.81" x2="-1.27" y2="3.81" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="0" width="0.127" layer="21"/>
</package>
<package name="0850BM14E0016T">
<smd name="1" x="0.325" y="-0.05" dx="0.45" dy="0.25" layer="1" rot="R270"/>
<smd name="2" x="0.825" y="-0.05" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<smd name="3" x="1.325" y="-0.05" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<smd name="4" x="1.325" y="0.85" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<smd name="5" x="0.825" y="0.85" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<smd name="6" x="0.325" y="0.85" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<wire x1="0.05" y1="0" x2="1.65" y2="0" width="0.127" layer="21"/>
<wire x1="1.65" y1="0" x2="1.65" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.65" y1="0.8" x2="0.05" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.05" y1="0.8" x2="0.05" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.4" x2="0.4" y2="0.4" width="0.127" layer="21"/>
</package>
<package name="2520">
<smd name="P$2" x="0" y="1" dx="1.25" dy="2.5" layer="1"/>
<smd name="P$3" x="2.5" y="1" dx="1.25" dy="2.5" layer="1"/>
<wire x1="0" y1="0" x2="0.6" y2="0" width="0.127" layer="21"/>
<wire x1="0.6" y1="0" x2="1.9" y2="0" width="0.127" layer="21"/>
<wire x1="1.9" y1="0" x2="2.5" y2="0" width="0.127" layer="21"/>
<wire x1="2.5" y1="0" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2" x2="1.9" y2="2" width="0.127" layer="21"/>
<wire x1="1.9" y1="2" x2="0.6" y2="2" width="0.127" layer="21"/>
<wire x1="0.6" y1="2" x2="0" y2="2" width="0.127" layer="21"/>
<wire x1="0" y1="2" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0.6" y1="0" x2="0.6" y2="2" width="0.127" layer="21"/>
<wire x1="1.9" y1="0" x2="1.9" y2="2" width="0.127" layer="21"/>
<text x="-1.64" y="3.19" size="1.27" layer="25">&gt;Name</text>
<text x="-1.67" y="-2.21" size="1.27" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="CC2640RGZ">
<wire x1="0" y1="0" x2="55.88" y2="0" width="0.254" layer="94"/>
<wire x1="55.88" y1="0" x2="55.88" y2="83.82" width="0.254" layer="94"/>
<wire x1="55.88" y1="83.82" x2="0" y2="83.82" width="0.254" layer="94"/>
<wire x1="0" y1="83.82" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="RF_P" x="60.96" y="33.02" length="middle" rot="R180"/>
<pin name="RF_N" x="60.96" y="27.94" length="middle" rot="R180"/>
<pin name="DIO0" x="-5.08" y="78.74" length="middle"/>
<pin name="X32K_Q1" x="60.96" y="15.24" length="middle" rot="R180"/>
<pin name="X32K_Q2" x="60.96" y="12.7" length="middle" rot="R180"/>
<pin name="DIO1" x="-5.08" y="76.2" length="middle"/>
<pin name="DIO2" x="-5.08" y="73.66" length="middle"/>
<pin name="DIO3" x="-5.08" y="71.12" length="middle"/>
<pin name="DIO4" x="-5.08" y="68.58" length="middle"/>
<pin name="DIO5" x="-5.08" y="66.04" length="middle"/>
<pin name="DIO6" x="-5.08" y="63.5" length="middle"/>
<pin name="DIO7" x="-5.08" y="60.96" length="middle"/>
<pin name="DIO8" x="-5.08" y="58.42" length="middle"/>
<pin name="DIO9" x="-5.08" y="55.88" length="middle"/>
<pin name="DIO10" x="-5.08" y="53.34" length="middle"/>
<pin name="DIO11" x="-5.08" y="50.8" length="middle"/>
<pin name="DIO12" x="-5.08" y="48.26" length="middle"/>
<pin name="DIO13" x="-5.08" y="45.72" length="middle"/>
<pin name="DIO14" x="-5.08" y="43.18" length="middle"/>
<pin name="DIO15" x="-5.08" y="40.64" length="middle"/>
<pin name="VDDS2" x="17.78" y="88.9" length="middle" rot="R270"/>
<pin name="VDDS3" x="22.86" y="88.9" length="middle" rot="R270"/>
<pin name="DCOUPL" x="-5.08" y="10.16" length="middle"/>
<pin name="JTAG_TMSC" x="-5.08" y="33.02" length="middle"/>
<pin name="DJTAG_TCKC" x="-5.08" y="30.48" length="middle"/>
<pin name="DIO16/JTAG_TDO" x="-5.08" y="38.1" length="middle"/>
<pin name="DIO17/JTAG_TDI" x="-5.08" y="35.56" length="middle"/>
<pin name="DIO18" x="60.96" y="71.12" length="middle" rot="R180"/>
<pin name="DIO19" x="60.96" y="68.58" length="middle" rot="R180"/>
<pin name="DIO20" x="60.96" y="66.04" length="middle" rot="R180"/>
<pin name="DIO21" x="60.96" y="63.5" length="middle" rot="R180"/>
<pin name="DIO22" x="60.96" y="60.96" length="middle" rot="R180"/>
<pin name="DCDC_SW" x="38.1" y="88.9" length="middle" rot="R270"/>
<pin name="VDDS_DCDC" x="33.02" y="88.9" length="middle" rot="R270"/>
<pin name="RESET_N" x="-5.08" y="20.32" length="middle"/>
<pin name="DIO23" x="60.96" y="58.42" length="middle" rot="R180"/>
<pin name="DIO24" x="60.96" y="55.88" length="middle" rot="R180"/>
<pin name="DIO25" x="60.96" y="53.34" length="middle" rot="R180"/>
<pin name="DIO26" x="60.96" y="50.8" length="middle" rot="R180"/>
<pin name="DIO27" x="60.96" y="48.26" length="middle" rot="R180"/>
<pin name="DIO28" x="60.96" y="45.72" length="middle" rot="R180"/>
<pin name="DIO29" x="60.96" y="43.18" length="middle" rot="R180"/>
<pin name="DIO30" x="60.96" y="40.64" length="middle" rot="R180"/>
<pin name="VDDS" x="12.7" y="88.9" length="middle" rot="R270"/>
<pin name="VDDR" x="27.94" y="88.9" length="middle" rot="R270"/>
<pin name="X24M_N" x="60.96" y="7.62" length="middle" rot="R180"/>
<pin name="X24M_P" x="60.96" y="5.08" length="middle" rot="R180"/>
<pin name="VDDR_RF" x="43.18" y="88.9" length="middle" rot="R270"/>
<pin name="GND" x="25.4" y="-5.08" length="middle" rot="R90"/>
<text x="22.86" y="55.88" size="1.778" layer="95">&gt;Name</text>
<text x="22.86" y="50.8" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="2450BM14G0011">
<wire x1="0" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="BP1" x="-5.08" y="12.7" length="middle"/>
<pin name="BP2" x="-5.08" y="7.62" length="middle"/>
<pin name="NC" x="-5.08" y="2.54" length="middle"/>
<pin name="GND@6" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="GND" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="UBP" x="22.86" y="12.7" length="middle" rot="R180"/>
<text x="0" y="17.78" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="20.32" size="1.778" layer="95">&gt;Value</text>
</symbol>
<symbol name="INDUCTOR">
<description>&lt;h3&gt;Inductors&lt;/h3&gt;
&lt;p&gt;Resist changes in electrical current. Basically a coil of wire.&lt;/p&gt;</description>
<text x="1.27" y="2.54" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.27" y="-2.54" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
</symbol>
<symbol name="JTAG">
<wire x1="0" y1="-2.54" x2="17.78" y2="-2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-2.54" x2="17.78" y2="22.86" width="0.254" layer="94"/>
<wire x1="17.78" y1="22.86" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="+V" x="-5.08" y="20.32" length="middle"/>
<pin name="GND" x="-5.08" y="15.24" length="middle"/>
<pin name="GND@5" x="-5.08" y="10.16" length="middle"/>
<pin name="NC" x="-5.08" y="5.08" length="middle"/>
<pin name="GND@9" x="-5.08" y="0" length="middle"/>
<pin name="TMSC" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="TCKC" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="TDO" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="TDI" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="RST" x="22.86" y="0" length="middle" rot="R180"/>
<text x="0" y="25.4" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="-7.62" size="1.778" layer="96">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CC2640RGZ" prefix="MCU">
<description>SimpleLink ultra-low power wireless MCU for Bluetooth low energy</description>
<gates>
<gate name="G$1" symbol="CC2640RGZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VQFN48-7X7">
<connects>
<connect gate="G$1" pin="DCDC_SW" pad="33"/>
<connect gate="G$1" pin="DCOUPL" pad="23"/>
<connect gate="G$1" pin="DIO0" pad="5"/>
<connect gate="G$1" pin="DIO1" pad="6"/>
<connect gate="G$1" pin="DIO10" pad="16"/>
<connect gate="G$1" pin="DIO11" pad="17"/>
<connect gate="G$1" pin="DIO12" pad="18"/>
<connect gate="G$1" pin="DIO13" pad="19"/>
<connect gate="G$1" pin="DIO14" pad="20"/>
<connect gate="G$1" pin="DIO15" pad="21"/>
<connect gate="G$1" pin="DIO16/JTAG_TDO" pad="26"/>
<connect gate="G$1" pin="DIO17/JTAG_TDI" pad="27"/>
<connect gate="G$1" pin="DIO18" pad="28"/>
<connect gate="G$1" pin="DIO19" pad="29"/>
<connect gate="G$1" pin="DIO2" pad="7"/>
<connect gate="G$1" pin="DIO20" pad="30"/>
<connect gate="G$1" pin="DIO21" pad="31"/>
<connect gate="G$1" pin="DIO22" pad="32"/>
<connect gate="G$1" pin="DIO23" pad="36"/>
<connect gate="G$1" pin="DIO24" pad="37"/>
<connect gate="G$1" pin="DIO25" pad="38"/>
<connect gate="G$1" pin="DIO26" pad="39"/>
<connect gate="G$1" pin="DIO27" pad="40"/>
<connect gate="G$1" pin="DIO28" pad="41"/>
<connect gate="G$1" pin="DIO29" pad="42"/>
<connect gate="G$1" pin="DIO3" pad="8"/>
<connect gate="G$1" pin="DIO30" pad="43"/>
<connect gate="G$1" pin="DIO4" pad="9"/>
<connect gate="G$1" pin="DIO5" pad="10"/>
<connect gate="G$1" pin="DIO6" pad="11"/>
<connect gate="G$1" pin="DIO7" pad="12"/>
<connect gate="G$1" pin="DIO8" pad="14"/>
<connect gate="G$1" pin="DIO9" pad="15"/>
<connect gate="G$1" pin="DJTAG_TCKC" pad="25"/>
<connect gate="G$1" pin="GND" pad="EGP"/>
<connect gate="G$1" pin="JTAG_TMSC" pad="24"/>
<connect gate="G$1" pin="RESET_N" pad="35"/>
<connect gate="G$1" pin="RF_N" pad="2"/>
<connect gate="G$1" pin="RF_P" pad="1"/>
<connect gate="G$1" pin="VDDR" pad="45"/>
<connect gate="G$1" pin="VDDR_RF" pad="48"/>
<connect gate="G$1" pin="VDDS" pad="44"/>
<connect gate="G$1" pin="VDDS2" pad="13"/>
<connect gate="G$1" pin="VDDS3" pad="22"/>
<connect gate="G$1" pin="VDDS_DCDC" pad="34"/>
<connect gate="G$1" pin="X24M_N" pad="46"/>
<connect gate="G$1" pin="X24M_P" pad="47"/>
<connect gate="G$1" pin="X32K_Q1" pad="3"/>
<connect gate="G$1" pin="X32K_Q2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2450BM14G0011" prefix="BALUN">
<description>2.4GHz Impedance Matched Balun + embedded FCC/ETSI Band Pass Filter For Texas
Instruments CC2620, CC2630, CC2640, CC2650 chipsets operated on INTERNAL BIAS MODE</description>
<gates>
<gate name="G$1" symbol="2450BM14G0011" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0850BM14E0016T">
<connects>
<connect gate="G$1" pin="BP1" pad="3"/>
<connect gate="G$1" pin="BP2" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="GND@6" pad="6"/>
<connect gate="G$1" pin="NC" pad="2"/>
<connect gate="G$1" pin="UBP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LQM2HPN1R5MG0L" prefix="L">
<description>Chip coil power inductor</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="2520">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
<connect gate="G$1" pin="2" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JTAG-CC13XX/26XX" prefix="J">
<description>JTAG SMD connector for Texas CC13xx/CC26xx series - XDS110 Debug Probe</description>
<gates>
<gate name="G$1" symbol="JTAG" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="2X05-0.05-SMD">
<connects>
<connect gate="G$1" pin="+V" pad="1"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="NC" pad="7"/>
<connect gate="G$1" pin="RST" pad="10"/>
<connect gate="G$1" pin="TCKC" pad="4"/>
<connect gate="G$1" pin="TDI" pad="8"/>
<connect gate="G$1" pin="TDO" pad="6"/>
<connect gate="G$1" pin="TMSC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-RF">
<description>&lt;h3&gt;SparkFun RF, WiFi, Cellular, and Bluetooth&lt;/h3&gt;
In this library you'll find things that send or receive RF-- cellular modules, Bluetooth, WiFi, etc.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TRACE_ANTENNA_2.4GHZ_25.7MM">
<description>&lt;h3&gt;2.4GHz Inverted F PCB Trace Antenna&lt;/h3&gt;
&lt;p&gt;PCB trace antenna with a 25.7 x 7.5 mm footprint.&lt;/p&gt;
&lt;p&gt;Based on layout from &lt;a href="http://www.ti.com/lit/an/swru120b/swru120b.pdf"&gt;TI design note DN0007&lt;/a&gt;.&lt;/p&gt;</description>
<polygon width="0.002540625" layer="1">
<vertex x="-0.23" y="0"/>
<vertex x="-0.23" y="2.28"/>
<vertex x="-0.73" y="2.69"/>
<vertex x="-3.88" y="2.69"/>
<vertex x="-3.88" y="3.9"/>
<vertex x="-2.88" y="3.9"/>
<vertex x="-2.88" y="4.7"/>
<vertex x="-8.68" y="4.7"/>
<vertex x="-8.68" y="2.03"/>
<vertex x="-0.68" y="2.03"/>
<vertex x="-0.68" y="0"/>
<vertex x="-1.68" y="0"/>
<vertex x="-1.68" y="0.7"/>
<vertex x="-2.88" y="0.7"/>
<vertex x="-2.88" y="0"/>
<vertex x="-3.88" y="0"/>
<vertex x="-3.88" y="0.74"/>
<vertex x="-10.86" y="0.74"/>
<vertex x="-10.86" y="6.91"/>
<vertex x="14.72" y="6.91"/>
<vertex x="14.72" y="5.7"/>
<vertex x="-1.68" y="5.7"/>
<vertex x="-1.68" y="3.9"/>
<vertex x="-0.68" y="3.9"/>
<vertex x="-0.68" y="3.29"/>
<vertex x="0.23" y="2.53"/>
<vertex x="0.23" y="0"/>
</polygon>
<smd name="ANT" x="0" y="0.23" dx="0.46" dy="0.46" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="GND" x="-1.18" y="0.23" dx="1" dy="0.46" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="GND2" x="-3.38" y="0.23" dx="1" dy="0.46" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-0.68" y1="0" x2="-11" y2="0" width="0.1" layer="51"/>
<wire x1="-0.68" y1="-2" x2="-0.68" y2="0" width="0.1" layer="51"/>
<wire x1="0.68" y1="0" x2="0.68" y2="-2" width="0.1" layer="51"/>
<wire x1="0.68" y1="0" x2="14.75" y2="0" width="0.1" layer="51"/>
<text x="-11.049" y="-1.397" size="0.889" layer="51" font="vector">Ground Plane</text>
<text x="2.921" y="-1.397" size="0.889" layer="51" font="vector">Ground Plane</text>
<wire x1="-11" y1="7.52" x2="14.75" y2="7.52" width="0.1" layer="51"/>
<text x="-4.449" y="7.703" size="0.889" layer="51" font="vector">Board edge</text>
<text x="3.81" y="4.445" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="3.81" y="3.81" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="TRACE_ANTENNA_2.4GHZ_15.2MM">
<description>&lt;h3&gt;2.4GHz Meander PCB Trace Antenna&lt;/h3&gt;
&lt;p&gt;PCB trace antenna with a 15.2 x 5.7mm footprint.&lt;/p&gt;
&lt;p&gt;Based on layout from &lt;a href="http://www.ti.com/lit/an/swra117d/swra117d.pdf"&gt;TI app note AN043&lt;/a&gt;.&lt;/p&gt;</description>
<polygon width="0.002540625" layer="1">
<vertex x="-0.25" y="-0.5"/>
<vertex x="-0.25" y="4.4"/>
<vertex x="-1.65" y="4.4"/>
<vertex x="-1.65" y="-0.5"/>
<vertex x="-2.55" y="-0.5"/>
<vertex x="-2.55" y="4.9"/>
<vertex x="2.45" y="4.9"/>
<vertex x="2.45" y="2.26"/>
<vertex x="4.45" y="2.26"/>
<vertex x="4.45" y="4.9"/>
<vertex x="7.15" y="4.9"/>
<vertex x="7.15" y="2.26"/>
<vertex x="9.15" y="2.26"/>
<vertex x="9.15" y="4.9"/>
<vertex x="11.85" y="4.9"/>
<vertex x="11.85" y="0.46"/>
<vertex x="11.35" y="0.46"/>
<vertex x="11.35" y="4.4"/>
<vertex x="9.65" y="4.4"/>
<vertex x="9.65" y="1.76"/>
<vertex x="6.65" y="1.76"/>
<vertex x="6.65" y="4.4"/>
<vertex x="4.95" y="4.4"/>
<vertex x="4.95" y="1.76"/>
<vertex x="1.95" y="1.76"/>
<vertex x="1.95" y="4.4"/>
<vertex x="0.25" y="4.4"/>
<vertex x="0.25" y="-0.5"/>
</polygon>
<wire x1="-3" y1="0" x2="12" y2="0" width="0.05" layer="51"/>
<smd name="GND" x="-2.1" y="-0.25" dx="0.9" dy="0.5" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="ANT" x="0" y="-0.25" dx="0.5" dy="0.5" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-3" y1="5.2" x2="12" y2="5.2" width="0.05" layer="51"/>
<text x="1" y="-0.8" size="0.64" layer="51" font="vector">Ground Plane</text>
<text x="1.5" y="5.5" size="0.64" layer="51" font="vector">Board Edge</text>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ANTENNA-GROUNDED">
<description>&lt;h3&gt;Antenna (with ground termination)&lt;/h3&gt;</description>
<wire x1="0" y1="-2.54" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<circle x="0" y="-5.08" radius="1.1359" width="0.254" layer="94"/>
<text x="3.048" y="-5.08" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="3.048" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="GND" x="2.54" y="-10.16" visible="off" length="short" rot="R90"/>
<pin name="SIGNAL" x="0" y="-10.16" visible="off" length="short" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ANTENNA-GROUNDED" prefix="E">
<description>&lt;h3&gt;Antenna w/ Ground Connection&lt;/h3&gt;
&lt;p&gt;2.4GHz antennae with signal and ground terminals.&lt;/p&gt;
&lt;p&gt;&lt;ul&gt;
&lt;li&gt;&lt;b&gt;TRACE_ANTENNA_2.4GHZ_15.2MM&lt;/b&gt; - Meander Trace antenna
&lt;ul&gt;&lt;li&gt;15.2 x 5.7mm footprint&lt;/li&gt;
&lt;li&gt;Based on layout from &lt;a href="http://www.ti.com/lit/an/swra117d/swra117d.pdf"&gt;TI app note AN043&lt;/a&gt;.&lt;/li&gt;&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;b&gt;TRACE_ANTENNA_2.4GHZ_25.7MM&lt;/b&gt; - Inverted F trace antenna
&lt;ul&gt;&lt;li&gt;25.7 x 7.5 mm footprint.&lt;/li&gt;
&lt;li&gt;Based on layout from &lt;a href="http://www.ti.com/lit/an/swru120b/swru120b.pdf"&gt;TI design note DN0007&lt;/a&gt;.&lt;/li&gt;&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="ANTENNA-GROUNDED" x="0" y="5.08"/>
</gates>
<devices>
<device name="TRACE-25.7MM" package="TRACE_ANTENNA_2.4GHZ_25.7MM">
<connects>
<connect gate="G$1" pin="GND" pad="GND GND2"/>
<connect gate="G$1" pin="SIGNAL" pad="ANT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TRACE-15.2MM" package="TRACE_ANTENNA_2.4GHZ_15.2MM">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SIGNAL" pad="ANT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-IC-Conversion">
<description>&lt;h3&gt;SparkFun Conversion ICs&lt;/h3&gt;
This library is for ICs that perform analog to digital conversion, as well as digital to analog.  This includes not only strict ADCs, but audio in and out converters, decoders, thermocouple measurement ics the like.  Anything that is a single part that works between the two domains goes here.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SO08">
<description>SOIC, 0.15 inch width</description>
<wire x1="2.3368" y1="1.9463" x2="-2.3368" y2="1.9463" width="0.2032" layer="21"/>
<wire x1="2.4368" y1="-1.9463" x2="2.7178" y2="-1.5653" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.7178" y1="1.4653" x2="-2.3368" y2="1.9463" width="0.2032" layer="21" curve="-90.023829"/>
<wire x1="2.3368" y1="1.9463" x2="2.7178" y2="1.5653" width="0.2032" layer="21" curve="-90.030084"/>
<wire x1="-2.7178" y1="-1.6653" x2="-2.3368" y2="-1.9463" width="0.2032" layer="21" curve="90.060185"/>
<wire x1="-2.3368" y1="-1.9463" x2="2.4368" y2="-1.9463" width="0.2032" layer="21"/>
<wire x1="2.7178" y1="-1.5653" x2="2.7178" y2="1.5653" width="0.2032" layer="21"/>
<wire x1="-2.667" y1="0.6096" x2="-2.667" y2="-0.6604" width="0.2032" layer="21" curve="-180"/>
<wire x1="-2.7178" y1="1.4526" x2="-2.7178" y2="0.6096" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="-1.6653" x2="-2.7178" y2="-0.6604" width="0.2032" layer="21"/>
<rectangle x1="-2.159" y1="-3.302" x2="-1.651" y2="-2.2733" layer="51"/>
<rectangle x1="-0.889" y1="-3.302" x2="-0.381" y2="-2.2733" layer="51"/>
<rectangle x1="0.381" y1="-3.302" x2="0.889" y2="-2.2733" layer="51"/>
<rectangle x1="1.651" y1="-3.302" x2="2.159" y2="-2.2733" layer="51"/>
<rectangle x1="-0.889" y1="2.286" x2="-0.381" y2="3.302" layer="51"/>
<rectangle x1="0.381" y1="2.286" x2="0.889" y2="3.302" layer="51"/>
<rectangle x1="1.651" y1="2.286" x2="2.159" y2="3.302" layer="51"/>
<smd name="1" x="-1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="2" x="-0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="3" x="0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="4" x="1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="5" x="1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="6" x="0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="7" x="-0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="8" x="-1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<text x="-3.175" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="3.81" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-2.159" y1="2.286" x2="-1.651" y2="3.302" layer="51"/>
<polygon width="0.002540625" layer="21">
<vertex x="-2.69875" y="-2.38125" curve="90"/>
<vertex x="-3.01625" y="-2.06375" curve="90"/>
<vertex x="-3.33375" y="-2.38125" curve="90"/>
<vertex x="-3.01625" y="-2.69875" curve="90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="MAX31855K">
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<text x="-7.62" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.778" layer="96">&gt;Value</text>
<pin name="GND" x="-12.7" y="0" length="middle"/>
<pin name="T-" x="-12.7" y="-5.08" length="middle"/>
<pin name="T+" x="-12.7" y="-7.62" length="middle"/>
<pin name="VCC" x="-12.7" y="7.62" length="middle"/>
<pin name="(MI)SO" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="!CS" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="SCK" x="12.7" y="-2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MAX31855K" prefix="U">
<description>&lt;h3&gt;Cold-Junction-Compensated K-Thermocoupleto-Digital
Converter (-270°C to +1800°C)&lt;/h3&gt;
&lt;p&gt;The MAX31855K performs cold-junction compensation
and digitizes the signal from a type-K thermocouple.&lt;br&gt;
The data is output in a 14-bit resolution, SPI™-compatible,
read-only format.&lt;br&gt;
This converter resolves temperatures to 0.25°C, allows readings as high as +1800°C and as low as -270°C, and exhibits thermocouple accuracy of ±2°C for temperatures ranging from -200°C to +700°C for K-type thermocouples.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://cdn.sparkfun.com/datasheets/Sensors/Temp/MAX31855K.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13266"&gt;SparkFun Thermocouple Breakout - MAX31855K&lt;/a&gt; (SEN-13266)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="MAX31855K" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO08">
<connects>
<connect gate="G$1" pin="!CS" pad="6"/>
<connect gate="G$1" pin="(MI)SO" pad="7"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="SCK" pad="5"/>
<connect gate="G$1" pin="T+" pad="3"/>
<connect gate="G$1" pin="T-" pad="2"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-12578" constant="no"/>
<attribute name="VALUE" value="MAX31855K" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl" urn="urn:adsk.eagle:library:334">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="PRL1632">
<description>&lt;b&gt;PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)&lt;/b&gt;&lt;p&gt;
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<wire x1="0.7275" y1="-1.5228" x2="-0.7277" y2="-1.5228" width="0.1524" layer="51"/>
<wire x1="0.7275" y1="1.5228" x2="-0.7152" y2="1.5228" width="0.1524" layer="51"/>
<smd name="2" x="0.822" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="1" x="-0.822" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-1.4" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.4" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.4" y2="1.6" layer="51"/>
<rectangle x1="0.4" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="HPC0201">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="C01005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PRL1632" package="PRL1632">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="C01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Clocks">
<description>&lt;h3&gt;SparkFun Clocks, Oscillators and Resonators&lt;/h3&gt;
This library contains the real-time clocks, oscillators, resonators, and crystals we use. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CRYSTAL-SMD-3.2X2.5MM">
<description>&lt;h3&gt;3.2 x 2.5mm SMD Crystal Package&lt;/h3&gt;
&lt;p&gt;Example: &lt;a href="http://www.digikey.com/product-search/en?keywords=SER3627TR-ND"&gt;SX-32S&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-0.4" y1="1.377" x2="0.4" y2="1.377" width="0.2032" layer="21"/>
<wire x1="-1.727" y1="-0.15" x2="-1.727" y2="0.15" width="0.2032" layer="21"/>
<wire x1="1.727" y1="0.15" x2="1.727" y2="-0.15" width="0.2032" layer="21"/>
<wire x1="0.4" y1="-1.377" x2="-0.4" y2="-1.377" width="0.2032" layer="21"/>
<rectangle x1="-1.6" y1="0.35" x2="-0.6" y2="1.15" layer="51"/>
<rectangle x1="0.6" y1="-1.15" x2="1.6" y2="-0.35" layer="51" rot="R180"/>
<rectangle x1="-1.6" y1="-1.15" x2="-0.6" y2="-0.35" layer="51"/>
<rectangle x1="0.6" y1="0.35" x2="1.6" y2="1.15" layer="51" rot="R180"/>
<smd name="1" x="-1.175" y="-0.875" dx="1.2" dy="1.1" layer="1" rot="R180"/>
<smd name="2" x="1.175" y="-0.875" dx="1.2" dy="1.1" layer="1"/>
<smd name="3" x="1.175" y="0.875" dx="1.2" dy="1.1" layer="1"/>
<smd name="4" x="-1.175" y="0.875" dx="1.2" dy="1.1" layer="1" rot="R180"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="HC49U">
<description>&lt;h3&gt;HC49/U 11.6x4.6mm PTH Crystal (13.46mm height)&lt;/h3&gt;
&lt;p&gt;&lt;a href="https://www.digikey.com/Web%20Export/Supplier%20Content/Citizen_300/PDF/Citizen_HC49US.pdf?redirected=1"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.1524" layer="21" curve="180"/>
<wire x1="-0.254" y1="0.889" x2="0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.889" x2="0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.889" x2="-0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.889" x2="-0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-2.794" x2="3.81" y2="2.794" layer="43"/>
<rectangle x1="-4.318" y1="-2.54" x2="-3.81" y2="2.54" layer="43"/>
<rectangle x1="-4.826" y1="-2.286" x2="-4.318" y2="2.286" layer="43"/>
<rectangle x1="-5.334" y1="-1.778" x2="-4.826" y2="1.778" layer="43"/>
<rectangle x1="-5.588" y1="-1.27" x2="-5.334" y2="1.016" layer="43"/>
<rectangle x1="3.81" y1="-2.54" x2="4.318" y2="2.54" layer="43"/>
<rectangle x1="4.318" y1="-2.286" x2="4.826" y2="2.286" layer="43"/>
<rectangle x1="4.826" y1="-1.778" x2="5.334" y2="1.778" layer="43"/>
<rectangle x1="5.334" y1="-1.016" x2="5.588" y2="1.016" layer="43"/>
</package>
<package name="CRYSTAL-PTH-3X8-CYL">
<description>&lt;h3&gt;3x8mm Cylindrical Can (Radial) PTH Crystal&lt;/h3&gt;

This is the "KIT" version, which has limited top masking for improved ease of assembly.

&lt;p&gt;Example product: &lt;a href="https://www.sparkfun.com/products/540"&gt;32kHz crystal&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ecsxtal.com/store/pdf/ECS-3x8.pdf"&gt;Example datasheet&lt;/a&gt; (ECS-3X8)&lt;/p&gt;</description>
<wire x1="-1.397" y1="1.651" x2="1.397" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.524" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="9.652" x2="-1.27" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="9.906" x2="1.27" y2="9.906" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.651" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.651" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="1.397" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.5588" y1="0.7112" x2="0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.016" width="0.4064" layer="21"/>
<wire x1="-0.5588" y1="0.7112" x2="-0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="5.588" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.588" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.969" x2="0" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.826" x2="0" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0.762" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0" y2="6.477" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0.762" y2="5.969" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6764"/>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="1.016" x2="1.778" y2="10.414" layer="43"/>
<text x="-1.778" y="5.334" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.778" y="5.334" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="CRYSTAL-PTH-2X6-CYL">
<description>&lt;h3&gt;2x6mm Cylindrical Can (Radial) PTH Crystal&lt;/h3&gt;

&lt;p&gt;Example product: &lt;a href="https://www.sparkfun.com/products/540"&gt;32kHz crystal&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ecsxtal.com/store/pdf/ECS-3x8.pdf"&gt;Example datasheet&lt;/a&gt; (ECS-2X6)&lt;/p&gt;</description>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.747" x2="1.016" y2="7.493" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="7.493" x2="-0.762" y2="7.747" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="7.747" x2="0.762" y2="7.747" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="4.953" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.953" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.334" x2="0" y2="5.334" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.191" x2="0" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0.508" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0.508" y2="5.334" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6764"/>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="0.762" x2="1.778" y2="8.382" layer="43"/>
<text x="-1.27" y="4.572" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.27" y="4.572" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="HC49UP">
<description>&lt;h3&gt;HC-49/UP 11.4x4.8mm SMD Crystal&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://www.standardcrystalcorp.com/pdf%5Cc-3.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993" cap="flat"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485" cap="flat"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828" cap="flat"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828" cap="flat"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157" cap="flat"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524" cap="flat"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213" cap="flat"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828" cap="flat"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828" cap="flat"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213" cap="flat"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
<text x="-0.254" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="HC49US">
<description>&lt;h3&gt;HC49/US 11.6x4.6mm PTH Crystal&lt;/h3&gt;
&lt;p&gt;&lt;a href="https://www.digikey.com/Web%20Export/Supplier%20Content/Citizen_300/PDF/Citizen_HC49US.pdf?redirected=1"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.2032" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.2032" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.429" y2="-2.286" width="0.2032" layer="21" curve="-180"/>
<wire x1="-3.429" y1="2.286" x2="-3.429" y2="-2.286" width="0.2032" layer="21" curve="180"/>
<pad name="1" x="-2.54" y="0" drill="0.7" diameter="1.651" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" rot="R90"/>
<text x="0" y="2.54" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="CRYSTAL-SMD-2X6-CYL">
<description>&lt;h3&gt;6.0x2.0mm Cylindrical Can (Radial) SMD Crystal&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://cfm.citizen.co.jp/english/product/pdf/CMR200T.pdf"&gt;Example&lt;/a&gt;&lt;/p&gt;</description>
<smd name="X1" x="-1.27" y="0" dx="1" dy="2.5" layer="1"/>
<smd name="X2" x="1.27" y="0" dx="1" dy="2.5" layer="1"/>
<smd name="SHEILD" x="0" y="5.08" dx="2.5" dy="6" layer="1"/>
<text x="0" y="8.255" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="-1.27" y1="-1" x2="-1.27" y2="1" width="0.3" layer="51"/>
<wire x1="-1.27" y1="1" x2="-0.6" y2="1" width="0.3" layer="51"/>
<wire x1="-0.6" y1="1" x2="-0.6" y2="2" width="0.3" layer="51"/>
<wire x1="-0.6" y1="2" x2="-0.5" y2="2" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1" x2="1.27" y2="1" width="0.3" layer="51"/>
<wire x1="1.27" y1="1" x2="0.6" y2="1" width="0.3" layer="51"/>
<wire x1="0.6" y1="1" x2="0.6" y2="2" width="0.3" layer="51"/>
<wire x1="0.6" y1="2" x2="0.5" y2="2" width="0.127" layer="51"/>
<polygon width="0.1" layer="51">
<vertex x="-1" y="8"/>
<vertex x="-1" y="2"/>
<vertex x="1" y="2"/>
<vertex x="1" y="8"/>
</polygon>
</package>
<package name="CRYSTAL-SMD-5X3.2-4PAD">
<description>&lt;h3&gt;5x3.2mm SMD Crystal&lt;/h3&gt;
&lt;p&gt;Example: &lt;a href="https://www.sparkfun.com/products/94"&gt;16MHz SMD Crystal&lt;/a&gt; (&lt;a href="https://www.sparkfun.com/datasheets/Components/SPK-5032-16MHZ.pdf"&gt;Datasheet&lt;/a&gt;)&lt;/p&gt;</description>
<wire x1="-0.6" y1="1.7" x2="0.6" y2="1.7" width="0.2032" layer="21"/>
<wire x1="2.6" y1="0.3" x2="2.6" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.7" x2="-0.6" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="0.3" x2="-2.6" y2="-0.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.85" y="-1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="3" x="1.85" y="1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="4" x="-1.85" y="1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="2" x="1.85" y="-1.15" dx="1.9" dy="1.1" layer="1"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-1.6" x2="2.5" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.5" y1="-1.6" x2="2.5" y2="1.6" width="0.127" layer="51"/>
<wire x1="2.5" y1="1.6" x2="-2.5" y2="1.6" width="0.127" layer="51"/>
<polygon width="0.127" layer="51">
<vertex x="-2.5" y="1.6"/>
<vertex x="-2.5" y="0.8"/>
<vertex x="-1.3" y="0.8"/>
<vertex x="-1.3" y="1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="2.5" y="-1.6"/>
<vertex x="2.5" y="-0.8"/>
<vertex x="1.3" y="-0.8"/>
<vertex x="1.3" y="-1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="1.3" y="1.6"/>
<vertex x="1.3" y="0.8"/>
<vertex x="2.5" y="0.8"/>
<vertex x="2.5" y="1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="-1.3" y="-1.6"/>
<vertex x="-1.3" y="-0.8"/>
<vertex x="-2.5" y="-0.8"/>
<vertex x="-2.5" y="-1.6"/>
</polygon>
</package>
<package name="CRYSTAL-SMD-MC-146">
<description>&lt;h3&gt;7x1.5mm MC-146 Flat Lead SMD Crystal&lt;/h3&gt;

&lt;p&gt;&lt;a href="https://support.epson.biz/td/api/doc_check.php?dl=brief_MC-156_en.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="0.35" y1="0" x2="7.05" y2="0" width="0.127" layer="51"/>
<wire x1="7.05" y1="0" x2="7.05" y2="1.5" width="0.127" layer="51"/>
<wire x1="7.05" y1="1.5" x2="0.35" y2="1.5" width="0.127" layer="51"/>
<wire x1="0.35" y1="1.5" x2="0.35" y2="0" width="0.127" layer="51"/>
<smd name="P$1" x="0.6" y="0.3" dx="1.2" dy="0.6" layer="1"/>
<smd name="P$2" x="0.6" y="1.2" dx="1.2" dy="0.6" layer="1"/>
<smd name="NC2" x="6.9" y="0.3" dx="1.2" dy="0.6" layer="1"/>
<smd name="NC1" x="6.9" y="1.2" dx="1.2" dy="0.6" layer="1"/>
<text x="3.81" y="1.851" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="3.81" y="-0.327" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="7.05" y1="-0.2" x2="0.35" y2="-0.2" width="0.2032" layer="21"/>
<wire x1="0.35" y1="1.7" x2="7.05" y2="1.7" width="0.2032" layer="21"/>
</package>
<package name="CRYSTAL-PTH-2X6-CYL-KIT">
<description>&lt;h3&gt;2x6mm Cylindrical Can (Radial) PTH Crystal&lt;/h3&gt;

This is the "KIT" version, which has limited top masking for improved ease of assembly.

&lt;p&gt;Example product: &lt;a href="https://www.sparkfun.com/products/540"&gt;32kHz crystal&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ecsxtal.com/store/pdf/ECS-3x8.pdf"&gt;Example datasheet&lt;/a&gt; (ECS-2X6)&lt;/p&gt;</description>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.747" x2="1.016" y2="7.493" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="7.493" x2="-0.762" y2="7.747" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="7.747" x2="0.762" y2="7.747" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="4.953" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.953" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.334" x2="0" y2="5.334" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.191" x2="0" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0.508" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0.508" y2="5.334" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764" stop="no"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6764" stop="no"/>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="0.762" x2="1.778" y2="8.382" layer="43"/>
<circle x="-1.27" y="0" radius="0.508" width="0" layer="29"/>
<circle x="1.27" y="0" radius="0.508" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="0.924571875" width="0" layer="30"/>
<circle x="1.27" y="0" radius="0.915809375" width="0" layer="30"/>
<text x="-1.27" y="4.572" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.27" y="4.572" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="CRYSTAL-SMD-5X3.2-2PAD">
<description>&lt;h3&gt;5x3.2mm 2-pad SMD Crystal&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://www.txccrystal.com/images/pdf/7a.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<smd name="P$1" x="1.85" y="0" dx="1.7" dy="2.4" layer="1"/>
<smd name="P$2" x="-1.85" y="0" dx="1.7" dy="2.4" layer="1"/>
<wire x1="-2.6" y1="1.7" x2="2.6" y2="1.7" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.7" x2="-2.6" y2="-1.7" width="0.2032" layer="21"/>
<text x="0" y="1.878" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.878" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.5" y1="-1.6" x2="2.5" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.5" y1="-1.6" x2="2.5" y2="1.6" width="0.127" layer="51"/>
<wire x1="2.5" y1="1.6" x2="-2.5" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.6" y1="1.7" x2="-2.6" y2="1.4" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.7" x2="2.6" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.7" x2="-2.6" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.7" x2="2.6" y2="1.4" width="0.2032" layer="21"/>
<polygon width="0.127" layer="51">
<vertex x="-2.5" y="1"/>
<vertex x="-1.2" y="1"/>
<vertex x="-1.2" y="-1"/>
<vertex x="-2.5" y="-1"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="2.5" y="-1"/>
<vertex x="1.2" y="-1"/>
<vertex x="1.2" y="1"/>
<vertex x="2.5" y="1"/>
</polygon>
</package>
<package name="CRYSTAL-SMD-3.2X1.5MM">
<description>&lt;h3&gt;3.2 x 1.5mm SMD Crystal Package&lt;/h3&gt;
&lt;p&gt;Example: &lt;a href="http://www.sii.co.jp/en/quartz/files/2013/03/file_PRODUCT_MASTER_50812_GRAPHIC03.pdf"&gt;SX-32S&lt;/a&gt;&lt;/p&gt;</description>
<smd name="P$1" x="-1.25" y="0" dx="1" dy="1.8" layer="1"/>
<smd name="P$2" x="1.25" y="0" dx="1" dy="1.8" layer="1"/>
<wire x1="-1.6" y1="0.75" x2="1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.75" x2="1.6" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.75" x2="-1.6" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.75" x2="-1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0.85" x2="0.5" y2="0.85" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.85" x2="-0.5" y2="-0.85" width="0.2032" layer="21"/>
<text x="0" y="1.043" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.043" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL-GND">
<description>&lt;h3&gt;Crystal with Ground pin&lt;/h3&gt;</description>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<text x="1.524" y="-1.524" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0" y1="-2.8" x2="0" y2="-1.6" width="0.1524" layer="94"/>
<text x="-1.27" y="-1.524" size="1.778" layer="95" font="vector" align="top-right">&gt;NAME</text>
</symbol>
<symbol name="CRYSTAL">
<description>&lt;h3&gt;Crystal (no ground pin)&lt;/h3&gt;</description>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="0" y="2.032" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.032" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTAL-26MHZ" prefix="Y">
<description>&lt;h3&gt;26MHz Crystal w/ Ground Pin&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://www.digikey.com/product-detail/en/epson/TSX-3225%2026.0000MF09Z-AC3/SER3627CT-ND/1802878"&gt;Digikey Example&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;ul&gt;&lt;li&gt;Frequency: 26MHz&lt;/li&gt;
&lt;li&gt;Frequency Stability: &amp;plusmn;9ppm&lt;/li&gt;
&lt;li&gt;Frequency Tolerance: &amp;plusmn;10ppm&lt;/li&gt;
&lt;li&gt;Load Capacitance: 9pF&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13907”&gt;SparkFun ESP32 Thing&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13711”&gt;SparkFun ESP8266 Thing Dev Board&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CRYSTAL-GND" x="0" y="0"/>
</gates>
<devices>
<device name="SMD-3.2X2.5" package="CRYSTAL-SMD-3.2X2.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XTAL-12454"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL" prefix="Y" uservalue="yes">
<description>&lt;h3&gt;Crystals (Generic)&lt;/h3&gt;
&lt;p&gt;These are &lt;b&gt;passive&lt;/b&gt; quartz crystals, which can be used as a clock source for a microcontroller.&lt;/p&gt;
&lt;p&gt;Crystal's are two-terminal devices. They require external "load" capacitors to generate an oscillating signal.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="PTH-HC49UV" package="HC49U">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-3X8" package="CRYSTAL-PTH-3X8-CYL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2X6" package="CRYSTAL-PTH-2X6-CYL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-HC49UP" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-HC49US" package="HC49US">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-2X6" package="CRYSTAL-SMD-2X6-CYL">
<connects>
<connect gate="G$1" pin="1" pad="X1"/>
<connect gate="G$1" pin="2" pad="X2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-5X3.2-4PAD" package="CRYSTAL-SMD-5X3.2-4PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-MC146" package="CRYSTAL-SMD-MC-146">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
<connect gate="G$1" pin="2" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2X6-KIT" package="CRYSTAL-PTH-2X6-CYL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-5X3.2-2PAD" package="CRYSTAL-SMD-5X3.2-2PAD">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-3.2X1.5" package="CRYSTAL-SMD-3.2X1.5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-3.2X2.5" package="CRYSTAL-SMD-3.2X2.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-IC-Power">
<description>&lt;h3&gt;SparkFun Power Driver and Management ICs&lt;/h3&gt;
In this library you'll find anything that has to do with power delivery, or making power supplies.
&lt;p&gt;Contents:
&lt;ul&gt;&lt;li&gt;LDOs&lt;/li&gt;
&lt;li&gt;Boost/Buck controllers&lt;/li&gt;
&lt;li&gt;Charge pump controllers&lt;/li&gt;
&lt;li&gt;Power sequencers&lt;/li&gt;
&lt;li&gt;Power switches&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="PDSO-N12">
<description>&lt;h3&gt;12-Pin VSON 2.5 x 4.0 mm IC&lt;/h3&gt;
&lt;p&gt;&lt;ul&gt;&lt;li&gt;Dimensions: 2.5 x 4.0 x 1.0 mm&lt;/li&gt;
&lt;li&gt;Pitch: 0.4mm&lt;/li&gt;
&lt;li&gt;Pins: 12 + Bottom Pad&lt;/li&gt;&lt;/p&gt;
&lt;p&gt;Used by the BQ27441 LiPo Fuel Gauge&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ti.com/lit/ds/symlink/bq27441-g1.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;</description>
<wire x1="1.25" y1="2" x2="-1.25" y2="2" width="0.127" layer="51"/>
<wire x1="-1.25" y1="2" x2="-1.25" y2="-2" width="0.127" layer="51"/>
<wire x1="-1.25" y1="-2" x2="1.25" y2="-2" width="0.127" layer="51"/>
<wire x1="1.25" y1="-2" x2="1.25" y2="2" width="0.127" layer="51"/>
<smd name="1" x="-1" y="-1.975" dx="0.2" dy="0.85" layer="1" cream="no"/>
<smd name="2" x="-0.6" y="-1.975" dx="0.2" dy="0.85" layer="1" cream="no"/>
<smd name="3" x="-0.2" y="-1.975" dx="0.2" dy="0.85" layer="1" cream="no"/>
<smd name="4" x="0.2" y="-1.975" dx="0.2" dy="0.85" layer="1" cream="no"/>
<smd name="5" x="0.6" y="-1.975" dx="0.2" dy="0.85" layer="1" cream="no"/>
<smd name="6" x="1" y="-1.975" dx="0.2" dy="0.85" layer="1" cream="no"/>
<smd name="7" x="1" y="1.975" dx="0.2" dy="0.85" layer="1" rot="R180" cream="no"/>
<smd name="8" x="0.6" y="1.975" dx="0.2" dy="0.85" layer="1" rot="R180" cream="no"/>
<smd name="9" x="0.2" y="1.975" dx="0.2" dy="0.85" layer="1" rot="R180" cream="no"/>
<smd name="10" x="-0.2" y="1.975" dx="0.2" dy="0.85" layer="1" rot="R180" cream="no"/>
<smd name="11" x="-0.6" y="1.975" dx="0.2" dy="0.85" layer="1" rot="R180" cream="no"/>
<smd name="12" x="-1" y="1.975" dx="0.2" dy="0.85" layer="1" rot="R180" cream="no"/>
<smd name="EP" x="0" y="0" dx="1.95" dy="1.95" layer="1" cream="no"/>
<polygon width="0.0127" layer="1">
<vertex x="1.65" y="1.225"/>
<vertex x="-1.65" y="1.225"/>
<vertex x="-1.65" y="0.975"/>
<vertex x="-0.975" y="0.975"/>
<vertex x="-0.975" y="-0.975"/>
<vertex x="-1.65" y="-0.975"/>
<vertex x="-1.65" y="-1.225"/>
<vertex x="1.65" y="-1.225"/>
<vertex x="1.65" y="-0.975"/>
<vertex x="0.975" y="-0.975"/>
<vertex x="0.975" y="0.975"/>
<vertex x="1.65" y="0.975"/>
</polygon>
<smd name="EP1" x="-1.15" y="1.1" dx="1" dy="0.25" layer="1" cream="no"/>
<smd name="EP3" x="1.15" y="-1.1" dx="1" dy="0.25" layer="1" rot="R180" cream="no"/>
<smd name="EP2" x="-1.15" y="-1.1" dx="1" dy="0.25" layer="1" cream="no"/>
<smd name="EP4" x="1.15" y="1.1" dx="1" dy="0.25" layer="1" rot="R180" cream="no"/>
<wire x1="-1.35" y1="2" x2="-1.35" y2="1.4" width="0.2032" layer="21"/>
<wire x1="1.35" y1="-2" x2="1.35" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="1.35" y1="2" x2="1.35" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-1.35" y1="0.8" x2="-1.35" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.35" y1="-0.8" x2="1.35" y2="0.8" width="0.2032" layer="21"/>
<circle x="-1.55" y="-2.2" radius="0.22360625" width="0" layer="21"/>
<wire x1="-1.35" y1="-2" x2="-1.35" y2="-1.4" width="0.2032" layer="21"/>
<text x="-1.905" y="0" size="0.6096" layer="25" ratio="20" rot="R90" align="bottom-center">&gt;Name</text>
<text x="1.778" y="0" size="0.6096" layer="27" ratio="20" rot="R90" align="top-center">&gt;Value</text>
<rectangle x1="-0.9" y1="0.1" x2="-0.1" y2="1.1" layer="31"/>
<rectangle x1="0.1" y1="-1.1" x2="0.9" y2="-0.1" layer="31" rot="R180"/>
<rectangle x1="0.1" y1="0.1" x2="0.9" y2="1.1" layer="31" rot="R180"/>
<rectangle x1="-0.9" y1="-1.1" x2="-0.1" y2="-0.1" layer="31"/>
<rectangle x1="-1.625" y1="0.975" x2="-1.1" y2="1.225" layer="31"/>
<rectangle x1="1.1" y1="-1.225" x2="1.625" y2="-0.975" layer="31" rot="R180"/>
<rectangle x1="1.1" y1="0.975" x2="1.625" y2="1.225" layer="31" rot="R180"/>
<rectangle x1="-1.625" y1="-1.225" x2="-1.1" y2="-0.975" layer="31"/>
<rectangle x1="-1.75" y1="0.875" x2="1.75" y2="1.325" layer="29"/>
<rectangle x1="-1.75" y1="-1.325" x2="1.75" y2="-0.875" layer="29" rot="R180"/>
<wire x1="-1.35" y1="2" x2="-1.25" y2="2" width="0.2032" layer="21"/>
<wire x1="1.35" y1="-2" x2="1.25" y2="-2" width="0.2032" layer="21"/>
<wire x1="1.35" y1="2" x2="1.25" y2="2" width="0.127" layer="21"/>
<wire x1="-1.35" y1="-2" x2="-1.25" y2="-2" width="0.127" layer="21"/>
<rectangle x1="0.1" y1="1.575" x2="0.3" y2="2.375" layer="31"/>
<rectangle x1="0.5" y1="1.575" x2="0.7" y2="2.375" layer="31"/>
<rectangle x1="0.5" y1="1.575" x2="0.7" y2="2.375" layer="31"/>
<rectangle x1="0.9" y1="1.575" x2="1.1" y2="2.375" layer="31"/>
<rectangle x1="-0.3" y1="1.575" x2="-0.1" y2="2.375" layer="31"/>
<rectangle x1="-0.7" y1="1.575" x2="-0.5" y2="2.375" layer="31"/>
<rectangle x1="-1.1" y1="1.575" x2="-0.9" y2="2.375" layer="31"/>
<rectangle x1="-1.1" y1="-2.375" x2="-0.9" y2="-1.575" layer="31" rot="R180"/>
<rectangle x1="-0.7" y1="-2.375" x2="-0.5" y2="-1.575" layer="31" rot="R180"/>
<rectangle x1="-0.3" y1="-2.375" x2="-0.1" y2="-1.575" layer="31" rot="R180"/>
<rectangle x1="0.1" y1="-2.375" x2="0.3" y2="-1.575" layer="31" rot="R180"/>
<rectangle x1="0.5" y1="-2.375" x2="0.7" y2="-1.575" layer="31" rot="R180"/>
<rectangle x1="0.9" y1="-2.375" x2="1.1" y2="-1.575" layer="31" rot="R180"/>
</package>
<package name="PVQFN-N16">
<description>&lt;h3&gt;16-Pin VSON (VQFN) 3.0 x 3.0 mm IC&lt;/h3&gt;
&lt;p&gt;&lt;ul&gt;&lt;li&gt;Dimensions: 3.0 x3.0 x 1.0 mm&lt;/li&gt;
&lt;li&gt;Pitch: 0.5mm&lt;/li&gt;
&lt;li&gt;Pins: 16 + Bottom Pad&lt;/li&gt;&lt;/p&gt;
&lt;p&gt;Used by the BQ24075 LiPo Charger&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ti.com/product/BQ24075/datasheet"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;</description>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="1.5" y2="1.5" width="0.127" layer="51"/>
<polygon width="0" layer="51">
<vertex x="0.13" y="1.5"/>
<vertex x="0.13" y="1.21" curve="90"/>
<vertex x="0.24" y="1.1"/>
<vertex x="0.26" y="1.1" curve="90"/>
<vertex x="0.37" y="1.21"/>
<vertex x="0.37" y="1.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-0.37" y="1.5"/>
<vertex x="-0.37" y="1.21" curve="90"/>
<vertex x="-0.26" y="1.1"/>
<vertex x="-0.24" y="1.1" curve="90"/>
<vertex x="-0.13" y="1.21"/>
<vertex x="-0.13" y="1.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-0.87" y="1.5"/>
<vertex x="-0.87" y="1.21" curve="90"/>
<vertex x="-0.76" y="1.1"/>
<vertex x="-0.74" y="1.1" curve="90"/>
<vertex x="-0.63" y="1.21"/>
<vertex x="-0.63" y="1.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.63" y="1.5"/>
<vertex x="0.63" y="1.21" curve="90"/>
<vertex x="0.74" y="1.1"/>
<vertex x="0.76" y="1.1" curve="90"/>
<vertex x="0.87" y="1.21"/>
<vertex x="0.87" y="1.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.5" y="0.13"/>
<vertex x="-1.21" y="0.13" curve="90"/>
<vertex x="-1.1" y="0.24"/>
<vertex x="-1.1" y="0.26" curve="90"/>
<vertex x="-1.21" y="0.37"/>
<vertex x="-1.5" y="0.37"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.5" y="-0.37"/>
<vertex x="-1.21" y="-0.37" curve="90"/>
<vertex x="-1.1" y="-0.26"/>
<vertex x="-1.1" y="-0.24" curve="90"/>
<vertex x="-1.21" y="-0.13"/>
<vertex x="-1.5" y="-0.13"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.5" y="-0.87"/>
<vertex x="-1.21" y="-0.87" curve="90"/>
<vertex x="-1.1" y="-0.76"/>
<vertex x="-1.1" y="-0.74" curve="90"/>
<vertex x="-1.21" y="-0.63"/>
<vertex x="-1.5" y="-0.63"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.5" y="0.63"/>
<vertex x="-1.21" y="0.63" curve="90"/>
<vertex x="-1.1" y="0.74"/>
<vertex x="-1.1" y="0.76" curve="90"/>
<vertex x="-1.21" y="0.87"/>
<vertex x="-1.5" y="0.87"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-0.13" y="-1.5"/>
<vertex x="-0.13" y="-1.21" curve="90"/>
<vertex x="-0.24" y="-1.1"/>
<vertex x="-0.26" y="-1.1" curve="90"/>
<vertex x="-0.37" y="-1.21"/>
<vertex x="-0.37" y="-1.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.37" y="-1.5"/>
<vertex x="0.37" y="-1.21" curve="90"/>
<vertex x="0.26" y="-1.1"/>
<vertex x="0.24" y="-1.1" curve="90"/>
<vertex x="0.13" y="-1.21"/>
<vertex x="0.13" y="-1.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.87" y="-1.5"/>
<vertex x="0.87" y="-1.21" curve="90"/>
<vertex x="0.76" y="-1.1"/>
<vertex x="0.74" y="-1.1" curve="90"/>
<vertex x="0.63" y="-1.21"/>
<vertex x="0.63" y="-1.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-0.63" y="-1.5"/>
<vertex x="-0.63" y="-1.21" curve="90"/>
<vertex x="-0.74" y="-1.1"/>
<vertex x="-0.76" y="-1.1" curve="90"/>
<vertex x="-0.87" y="-1.21"/>
<vertex x="-0.87" y="-1.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="1.5" y="-0.13"/>
<vertex x="1.21" y="-0.13" curve="90"/>
<vertex x="1.1" y="-0.24"/>
<vertex x="1.1" y="-0.26" curve="90"/>
<vertex x="1.21" y="-0.37"/>
<vertex x="1.5" y="-0.37"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="1.5" y="0.37"/>
<vertex x="1.21" y="0.37" curve="90"/>
<vertex x="1.1" y="0.26"/>
<vertex x="1.1" y="0.24" curve="90"/>
<vertex x="1.21" y="0.13"/>
<vertex x="1.5" y="0.13"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="1.5" y="0.87"/>
<vertex x="1.21" y="0.87" curve="90"/>
<vertex x="1.1" y="0.76"/>
<vertex x="1.1" y="0.74" curve="90"/>
<vertex x="1.21" y="0.63"/>
<vertex x="1.5" y="0.63"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="1.5" y="-0.63"/>
<vertex x="1.21" y="-0.63" curve="90"/>
<vertex x="1.1" y="-0.74"/>
<vertex x="1.1" y="-0.76" curve="90"/>
<vertex x="1.21" y="-0.87"/>
<vertex x="1.5" y="-0.87"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.85" y="0.85"/>
<vertex x="-0.85" y="0.85"/>
<vertex x="-0.85" y="-0.63"/>
<vertex x="-0.63" y="-0.85"/>
<vertex x="0.85" y="-0.85"/>
</polygon>
<smd name="1" x="-0.75" y="-1.475" dx="0.28" dy="0.85" layer="1" cream="no"/>
<rectangle x1="-0.865" y1="-1.875" x2="-0.635" y2="-1.075" layer="31"/>
<smd name="2" x="-0.25" y="-1.475" dx="0.28" dy="0.85" layer="1" cream="no"/>
<rectangle x1="-0.365" y1="-1.875" x2="-0.135" y2="-1.075" layer="31"/>
<smd name="3" x="0.25" y="-1.475" dx="0.28" dy="0.85" layer="1" cream="no"/>
<rectangle x1="0.135" y1="-1.875" x2="0.365" y2="-1.075" layer="31"/>
<smd name="4" x="0.75" y="-1.475" dx="0.28" dy="0.85" layer="1" cream="no"/>
<rectangle x1="0.635" y1="-1.875" x2="0.865" y2="-1.075" layer="31"/>
<smd name="5" x="1.475" y="-0.75" dx="0.28" dy="0.85" layer="1" rot="R90" cream="no"/>
<rectangle x1="1.36" y1="-1.15" x2="1.59" y2="-0.35" layer="31" rot="R90"/>
<smd name="6" x="1.475" y="-0.25" dx="0.28" dy="0.85" layer="1" rot="R90" cream="no"/>
<rectangle x1="1.36" y1="-0.65" x2="1.59" y2="0.15" layer="31" rot="R90"/>
<smd name="7" x="1.475" y="0.25" dx="0.28" dy="0.85" layer="1" rot="R90" cream="no"/>
<rectangle x1="1.36" y1="-0.15" x2="1.59" y2="0.65" layer="31" rot="R90"/>
<smd name="8" x="1.475" y="0.75" dx="0.28" dy="0.85" layer="1" rot="R90" cream="no"/>
<rectangle x1="1.36" y1="0.35" x2="1.59" y2="1.15" layer="31" rot="R90"/>
<smd name="9" x="0.75" y="1.475" dx="0.28" dy="0.85" layer="1" rot="R180" cream="no"/>
<rectangle x1="0.635" y1="1.075" x2="0.865" y2="1.875" layer="31" rot="R180"/>
<smd name="10" x="0.25" y="1.475" dx="0.28" dy="0.85" layer="1" rot="R180" cream="no"/>
<rectangle x1="0.135" y1="1.075" x2="0.365" y2="1.875" layer="31" rot="R180"/>
<smd name="11" x="-0.25" y="1.475" dx="0.28" dy="0.85" layer="1" rot="R180" cream="no"/>
<rectangle x1="-0.365" y1="1.075" x2="-0.135" y2="1.875" layer="31" rot="R180"/>
<smd name="12" x="-0.75" y="1.475" dx="0.28" dy="0.85" layer="1" rot="R180" cream="no"/>
<rectangle x1="-0.865" y1="1.075" x2="-0.635" y2="1.875" layer="31" rot="R180"/>
<smd name="13" x="-1.475" y="0.75" dx="0.28" dy="0.85" layer="1" rot="R270" cream="no"/>
<rectangle x1="-1.59" y1="0.35" x2="-1.36" y2="1.15" layer="31" rot="R270"/>
<smd name="14" x="-1.475" y="0.25" dx="0.28" dy="0.85" layer="1" rot="R270" cream="no"/>
<rectangle x1="-1.59" y1="-0.15" x2="-1.36" y2="0.65" layer="31" rot="R270"/>
<smd name="15" x="-1.475" y="-0.25" dx="0.28" dy="0.85" layer="1" rot="R270" cream="no"/>
<rectangle x1="-1.59" y1="-0.65" x2="-1.36" y2="0.15" layer="31" rot="R270"/>
<smd name="16" x="-1.475" y="-0.75" dx="0.28" dy="0.85" layer="1" rot="R270" cream="no"/>
<rectangle x1="-1.59" y1="-1.15" x2="-1.36" y2="-0.35" layer="31" rot="R270"/>
<smd name="EP" x="0" y="0" dx="1.7" dy="1.7" layer="1" cream="no"/>
<rectangle x1="0.1" y1="0.1" x2="0.8" y2="0.8" layer="31"/>
<rectangle x1="-0.8" y1="-0.8" x2="-0.1" y2="-0.1" layer="31" rot="R180"/>
<rectangle x1="-0.8" y1="0.1" x2="-0.1" y2="0.8" layer="31" rot="R90"/>
<rectangle x1="0.1" y1="-0.8" x2="0.8" y2="-0.1" layer="31" rot="R270"/>
<wire x1="-1.7" y1="-1.06" x2="-1.06" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="1.6" y1="-1.06" x2="1.6" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="1.06" y1="-1.6" x2="1.6" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="1.06" y1="1.6" x2="1.6" y2="1.6" width="0.2032" layer="21"/>
<wire x1="1.6" y1="1.06" x2="1.6" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="1.06" x2="-1.6" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-1.06" y1="1.6" x2="-1.6" y2="1.6" width="0.2032" layer="21"/>
<circle x="-1.7" y="-1.7" radius="0.219315625" width="0" layer="21"/>
<text x="0" y="2.032" size="0.6096" layer="25" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.032" size="0.6096" layer="27" ratio="20" rot="R180" align="bottom-center">&gt;Value</text>
<wire x1="1.1" y1="-1.905" x2="1.1" y2="-1.1" width="0.127" layer="41"/>
<wire x1="-1.1" y1="1.905" x2="-1.1" y2="1.1" width="0.127" layer="41"/>
<wire x1="1.1" y1="1.095" x2="1.1" y2="1.9" width="0.127" layer="41"/>
<wire x1="-1.1" y1="-1.095" x2="-1.1" y2="-1.9" width="0.127" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="BQ27441-G1">
<description>&lt;h3&gt;Texas Instruments BQ27441-G1 LiPo Battery Fuel Gauge&lt;/h3&gt;
&lt;p&gt;The Texas Instruments bq27441-G1 fuel gauge is a microcontroller peripheral that provides system-side fuel gauging for single-cell Li-Ion batteries. The device requires minimal user configuration and system microcontroller firmware development.&lt;/p&gt;
&lt;p&gt;The bq27441-G1 battery fuel gauge uses the patented Impedance Track™ algorithm for fuel gauging, and provides information such as remaining battery capacity (mAh), state-of-charge (%), and battery voltage (mV).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ti.com/product/BQ27441-G1/datasheet"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="SDA" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="SCL" x="12.7" y="0" length="short" rot="R180"/>
<pin name="VSS" x="-10.16" y="-5.08" length="short"/>
<pin name="VDD" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="BAT" x="-10.16" y="0" length="short"/>
<pin name="SRN" x="-10.16" y="7.62" length="short"/>
<pin name="SRP" x="-10.16" y="5.08" length="short"/>
<pin name="BIN" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="GPOUT" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="PWPD" x="-10.16" y="-7.62" length="short"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.62" y="-10.414" size="1.778" layer="96" align="top-left">&gt;Value</text>
<text x="-7.62" y="10.414" size="1.778" layer="96">&gt;Name</text>
</symbol>
<symbol name="BQ24075">
<description>&lt;h3&gt;Texas Instruments BQ24075 LiPo Battery Chargerand Power-Path Management IC&lt;/h3&gt;
&lt;p&gt;The bq2407x series of devices are integrated Li-Ion linear chargers and system power path management devices targeted at space-limited portable applications. The devices operate from either a USB port or an AC adapter and support charge currents up to 1.5 A. The input voltage range with input overvoltage protection supports unregulated adapters. The USB input current limit accuracy and start up sequence allow the bq2407x to meet USB-IF inrush current specifications. Additionally, the input dynamic power management (VIN-DPM) prevents the charger from crashing incorrectly configured USB sources.&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ti.com/product/BQ24075/datasheet"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="TS" x="-12.7" y="5.08" length="short"/>
<pin name="BAT" x="-12.7" y="7.62" length="short"/>
<pin name="!CE!" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="EN2" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="EN1" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="!PGOOD!" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="VSS" x="-12.7" y="-10.16" length="short"/>
<pin name="EP" x="-12.7" y="-12.7" length="short"/>
<pin name="!CHG!" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="OUT" x="12.7" y="12.7" length="short" rot="R180"/>
<pin name="ILIM" x="-12.7" y="-5.08" length="short"/>
<pin name="IN" x="-12.7" y="12.7" length="short"/>
<pin name="TMR" x="-12.7" y="0" length="short"/>
<pin name="SYSOFF" x="12.7" y="0" length="short" rot="R180"/>
<pin name="ISET" x="-12.7" y="-2.54" length="short"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="15.494" size="1.778" layer="95">&gt;Name</text>
<text x="-10.16" y="-15.494" size="1.778" layer="96" align="top-left">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ27441-G1" prefix="U" uservalue="yes">
<description>&lt;h3&gt;Texas Instruments BQ27441-G1 LiPo Battery Fuel Gauge&lt;/h3&gt;
&lt;p&gt;The Texas Instruments bq27441-G1 fuel gauge is a microcontroller peripheral that provides system-side fuel gauging for single-cell Li-Ion batteries. The device requires minimal user configuration and system microcontroller firmware development.&lt;/p&gt;
&lt;p&gt;The bq27441-G1 battery fuel gauge uses the patented Impedance Track™ algorithm for fuel gauging, and provides information such as remaining battery capacity (mAh), state-of-charge (%), and battery voltage (mV).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ti.com/product/BQ27441-G1/datasheet"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13777"&gt;SparkFun Battery Babysitter&lt;/a&gt; (PRT-13777)&lt;/li&gt;&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="BQ27441-G1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PDSO-N12">
<connects>
<connect gate="G$1" pin="BAT" pad="6"/>
<connect gate="G$1" pin="BIN" pad="10"/>
<connect gate="G$1" pin="GPOUT" pad="12"/>
<connect gate="G$1" pin="PWPD" pad="EP EP1 EP2 EP3 EP4"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="SRN" pad="7"/>
<connect gate="G$1" pin="SRP" pad="8"/>
<connect gate="G$1" pin="VDD" pad="5"/>
<connect gate="G$1" pin="VSS" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-13220"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BQ24075" prefix="U" uservalue="yes">
<description>&lt;h3&gt;Texas Instruments BQ24075 LiPo Battery Chargerand Power-Path Management IC&lt;/h3&gt;
&lt;p&gt;The bq2407x series of devices are integrated Li-Ion linear chargers and system power path management devices targeted at space-limited portable applications. The devices operate from either a USB port or an AC adapter and support charge currents up to 1.5 A. The input voltage range with input overvoltage protection supports unregulated adapters. The USB input current limit accuracy and start up sequence allow the bq2407x to meet USB-IF inrush current specifications. Additionally, the input dynamic power management (VIN-DPM) prevents the charger from crashing incorrectly configured USB sources.&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ti.com/product/BQ24075/datasheet"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13777"&gt;SparkFun Battery Babysitter&lt;/a&gt; (PRT-13777)&lt;/li&gt;&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="BQ24075" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PVQFN-N16">
<connects>
<connect gate="G$1" pin="!CE!" pad="4"/>
<connect gate="G$1" pin="!CHG!" pad="9"/>
<connect gate="G$1" pin="!PGOOD!" pad="7"/>
<connect gate="G$1" pin="BAT" pad="2 3"/>
<connect gate="G$1" pin="EN1" pad="6"/>
<connect gate="G$1" pin="EN2" pad="5"/>
<connect gate="G$1" pin="EP" pad="EP"/>
<connect gate="G$1" pin="ILIM" pad="12"/>
<connect gate="G$1" pin="IN" pad="13"/>
<connect gate="G$1" pin="ISET" pad="16"/>
<connect gate="G$1" pin="OUT" pad="10 11"/>
<connect gate="G$1" pin="SYSOFF" pad="15"/>
<connect gate="G$1" pin="TMR" pad="14"/>
<connect gate="G$1" pin="TS" pad="1"/>
<connect gate="G$1" pin="VSS" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-13219"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="JST-2-SMD">
<description>&lt;h3&gt;JST-Right Angle Male Header SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;li&gt;JST_2MM_MALE&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.397" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="0.635" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="U.FL">
<description>&lt;h3&gt;U.FL SMD Antenna Connector&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;li&gt;Area: 3.0mm x 2.5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://cdn.sparkfun.com/datasheets/Wireless/Antennas/RF-001001.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;U.FL&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.3" y1="0.7" x2="1.3" y2="2" width="0.127" layer="21"/>
<wire x1="1.3" y1="2" x2="-1.3" y2="2" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-2" x2="1.3" y2="-2" width="0.127" layer="21"/>
<wire x1="1.3" y1="-2" x2="1.3" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.3" y1="0.7" x2="-1.3" y2="2" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-2" x2="-1.3" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="2" width="0.127" layer="21"/>
<wire x1="1.4" y1="-2" x2="1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="2" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="2" x2="1.3" y2="2" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.7" x2="1.3" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="1.3" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2" x2="1.3" y2="-2" width="0.127" layer="21"/>
<smd name="P$1" x="0" y="1.375" dx="2.2" dy="0.85" layer="1"/>
<smd name="P$2" x="0" y="-1.375" dx="2.2" dy="0.85" layer="1"/>
<smd name="P$3" x="1.525" y="0" dx="1.05" dy="1" layer="1"/>
<smd name="P$4" x="-1.525" y="0" dx="1.05" dy="1" layer="1"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="USB_MICROB_VERTICAL">
<description>&lt;h3&gt;USB Micro-B Plug Connector&lt;/h3&gt;
Manufacturer part #: ZX80-B-5SA&lt;br&gt;
Manufacturer: Hirose&lt;br&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5 main, 4 shield&lt;/li&gt;
&lt;p&gt;&lt;a href=”https://www.hirose.com/product/en/download_file/key_name/ZX/category/Catalog/doc_file_id/31704/?file_category_id=4&amp;item_id=13&amp;is_series=1”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;USB_Micro-B&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-9.7" y1="3.77" x2="-8.275" y2="3.77" width="0.127" layer="51"/>
<wire x1="-9.7" y1="0.57" x2="-8.275" y2="0.57" width="0.127" layer="51"/>
<wire x1="-9.7" y1="3.77" x2="-10.275" y2="4.3" width="0.127" layer="51"/>
<wire x1="-10.275" y1="4.3" x2="-10.275" y2="5.38" width="0.127" layer="51"/>
<wire x1="-9.7" y1="0.57" x2="-10.275" y2="0" width="0.127" layer="51"/>
<wire x1="-10.275" y1="0" x2="-10.275" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-8.275" y1="3.77" x2="-8.275" y2="0.57" width="0.127" layer="51" curve="-180"/>
<wire x1="9.7" y1="0.57" x2="8.275" y2="0.57" width="0.127" layer="51"/>
<wire x1="8.275" y1="0.57" x2="8.275" y2="3.77" width="0.127" layer="51" curve="-180"/>
<wire x1="9.7" y1="3.77" x2="8.275" y2="3.77" width="0.127" layer="51"/>
<wire x1="9.7" y1="0.57" x2="10.275" y2="0" width="0.127" layer="51"/>
<wire x1="10.275" y1="0" x2="10.275" y2="-0.9" width="0.127" layer="51"/>
<wire x1="9.7" y1="3.77" x2="10.275" y2="4.4" width="0.127" layer="51"/>
<wire x1="10.275" y1="4.4" x2="10.275" y2="5.38" width="0.127" layer="51"/>
<wire x1="-10.275" y1="5.38" x2="-4.275" y2="5.38" width="0.127" layer="51"/>
<wire x1="10.275" y1="5.38" x2="4.275" y2="5.38" width="0.127" layer="51"/>
<wire x1="-10.275" y1="-0.9" x2="-4.275" y2="-0.9" width="0.127" layer="51"/>
<wire x1="10.275" y1="-0.9" x2="4.275" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-4.275" y1="-0.9" x2="-4.275" y2="0" width="0.127" layer="51"/>
<wire x1="4.275" y1="-0.9" x2="4.275" y2="0" width="0.127" layer="51"/>
<wire x1="-4.275" y1="0" x2="4.275" y2="0" width="0.127" layer="51"/>
<wire x1="-4.275" y1="5.38" x2="-4.275" y2="4.48" width="0.127" layer="51"/>
<wire x1="4.275" y1="5.38" x2="4.275" y2="4.48" width="0.127" layer="51"/>
<wire x1="-4.275" y1="4.48" x2="4.275" y2="4.48" width="0.127" layer="51"/>
<pad name="SHLD1" x="-3.12053125" y="3.18301875" drill="1.016" diameter="2.2" rot="R90"/>
<pad name="SHLD3" x="-3.097921875" y="-0.026921875" drill="1.016" diameter="2.2" rot="R90"/>
<pad name="SHLD2" x="3.1218" y="3.160159375" drill="1.016" diameter="2.2" rot="R90"/>
<pad name="SHLD4" x="3.10783125" y="-0.02565625" drill="1.016" diameter="2.2" rot="R90"/>
<smd name="D+" x="0" y="0" dx="2.2" dy="0.4" layer="1" rot="R90"/>
<smd name="D-" x="-0.65" y="0" dx="2.2" dy="0.4" layer="1" rot="R90"/>
<smd name="VBUS" x="-1.3" y="0" dx="2.2" dy="0.4" layer="1" rot="R90"/>
<smd name="GND" x="1.3" y="0" dx="2.2" dy="0.4" layer="1" rot="R90"/>
<smd name="ID" x="0.65" y="0" dx="2.2" dy="0.4" layer="1" rot="R90"/>
<text x="-10.29" y="5.58" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-10.16" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;Value</text>
<hole x="-8.275" y="2.17" drill="3.3"/>
<hole x="8.275" y="2.17" drill="3.3"/>
</package>
<package name="USB-B-MICRO-SMD">
<description>&lt;h3&gt;USB - microB SMD&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Micro-USB.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;USB_MICRO-B&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<text x="-4.66" y="5.96" size="0.3048" layer="51">PCB Front</text>
<wire x1="-3" y1="5.6" x2="-2.3" y2="5.3" width="0.08" layer="51"/>
<wire x1="-2.3" y1="5.3" x2="-2.4" y2="5.5" width="0.08" layer="51"/>
<wire x1="-2.3" y1="5.3" x2="-2.5" y2="5.2" width="0.08" layer="51"/>
<text x="-1.27" y="4.699" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.699" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
<smd name="D+" x="2.725" y="0" dx="0.4" dy="1.45" layer="1" rot="R90"/>
<smd name="D-" x="2.725" y="0.65" dx="0.4" dy="1.45" layer="1" rot="R90"/>
<smd name="VBUS" x="2.725" y="1.3" dx="0.4" dy="1.45" layer="1" rot="R90"/>
<smd name="ID" x="2.725" y="-0.65" dx="0.4" dy="1.45" layer="1" rot="R90"/>
<smd name="GND" x="2.725" y="-1.3" dx="0.4" dy="1.45" layer="1" rot="R90"/>
<hole x="2.2" y="1.9" drill="0.85"/>
<hole x="2.2" y="-1.9" drill="0.85"/>
<rectangle x1="-0.75" y1="2.784" x2="0.75" y2="4.584" layer="31"/>
<rectangle x1="-0.35" y1="-0.75" x2="0.35" y2="0.75" layer="31"/>
<rectangle x1="-0.75" y1="-4.584" x2="0.75" y2="-2.784" layer="31"/>
<wire x1="-2.15" y1="3.9" x2="-2.15" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="3.9" x2="2.85" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="3.9" x2="-2.15" y2="3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="-3.9" x2="-2.15" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.981959375" y1="3.99288125" x2="2" y2="3.99288125" width="0.3048" layer="21"/>
<wire x1="2" y1="3.99288125" x2="2" y2="4" width="0.3048" layer="21"/>
<wire x1="3" y1="4" x2="3" y2="3" width="0.3048" layer="21"/>
<wire x1="2" y1="-4" x2="3" y2="-4" width="0.3048" layer="21"/>
<wire x1="3" y1="-4" x2="3" y2="-3" width="0.3048" layer="21"/>
<wire x1="-1" y1="4" x2="-2" y2="4" width="0.3048" layer="21"/>
<wire x1="-1" y1="-4" x2="-2" y2="-4" width="0.3048" layer="21"/>
<rectangle x1="-0.85" y1="2.684" x2="0.85" y2="4.684" layer="29"/>
<rectangle x1="-0.85" y1="-4.684" x2="0.85" y2="-2.684" layer="29"/>
<rectangle x1="-0.85" y1="-1.35" x2="0.85" y2="1.35" layer="29"/>
<smd name="SHIELD1" x="0" y="3.685" dx="1.8" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="SHIELD3" x="0" y="0" dx="2.5" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="SHIELD2" x="0" y="-3.685" dx="1.8" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<wire x1="-2" y1="7" x2="-2" y2="0" width="0" layer="51"/>
</package>
<package name="USB-MICROB">
<description>&lt;h3&gt;USB Type microUSB Connector-No Bossed Pins&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5 pins, 4 shield connections&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;USB_Micro-B&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.4" y1="-2.15" x2="-3" y2="-2.15" width="0.127" layer="51"/>
<wire x1="3" y1="-2.15" x2="3.4" y2="-2.15" width="0.127" layer="51"/>
<wire x1="-3.4" y1="-2.15" x2="-3.4" y2="-1.45" width="0.127" layer="51"/>
<wire x1="-3.4" y1="-1.45" x2="-3.4" y2="2.85" width="0.127" layer="51"/>
<wire x1="3.4" y1="2.85" x2="2.2" y2="2.85" width="0.127" layer="51"/>
<wire x1="3.4" y1="2.85" x2="3.4" y2="-1.45" width="0.127" layer="51"/>
<wire x1="3.4" y1="-1.45" x2="3.4" y2="-2.15" width="0.127" layer="51"/>
<wire x1="-3.4" y1="-1.45" x2="3.4" y2="-1.45" width="0.127" layer="51"/>
<wire x1="-1.25" y1="-3.4" x2="-2.85" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="-2.85" y1="-3.4" x2="-2.85" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-2.85" y1="3.4" x2="-2.85" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-1.25" y1="3.4" x2="-2.85" y2="3.4" width="0.2032" layer="21"/>
<wire x1="1.45" y1="-3.4" x2="1.45" y2="3.4" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.45" x2="2.2" y2="1.45" width="0.127" layer="51"/>
<wire x1="2.2" y1="1.45" x2="2.2" y2="2.85" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.45" x2="-2.2" y2="2.85" width="0.127" layer="51"/>
<wire x1="-3.4" y1="2.85" x2="-2.2" y2="2.85" width="0.127" layer="51"/>
<wire x1="-2.85" y1="-2.2" x2="-1.45" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-1.45" y1="-2.2" x2="-1.45" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-1.45" y1="2.2" x2="-2.85" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="-2.15" x2="-4" y2="-2.75" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-2.15" x2="4" y2="-2.75" width="0.2032" layer="51"/>
<wire x1="-3" y1="-2.15" x2="-3" y2="-2.55" width="0.127" layer="51"/>
<wire x1="-2.8" y1="-2.8" x2="2.75" y2="-2.8" width="0.127" layer="51"/>
<wire x1="3" y1="-2.6" x2="3" y2="-2.15" width="0.127" layer="51"/>
<wire x1="-3" y1="-2.55" x2="-2.8" y2="-2.8" width="0.127" layer="51" curve="84.547378"/>
<wire x1="2.75" y1="-2.8" x2="3" y2="-2.6" width="0.127" layer="51" curve="84.547378"/>
<smd name="VBUS" x="-2.65" y="-1.3" dx="1.4" dy="0.35" layer="1" rot="R180"/>
<smd name="GND" x="-2.65" y="1.3" dx="1.4" dy="0.35" layer="1" rot="R180"/>
<smd name="D-" x="-2.65" y="-0.65" dx="1.4" dy="0.35" layer="1" rot="R180"/>
<smd name="D+" x="-2.65" y="0" dx="1.4" dy="0.35" layer="1" rot="R180"/>
<smd name="ID" x="-2.65" y="0.65" dx="1.4" dy="0.35" layer="1" rot="R180"/>
<smd name="MT1" x="0" y="-4" dx="1.8" dy="1.9" layer="1" rot="R90"/>
<smd name="MT2" x="0" y="4" dx="1.8" dy="1.9" layer="1" rot="R90"/>
<text x="-2.54" y="5.08" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
<smd name="P$1" x="0" y="-1.27" dx="1.9" dy="1.9" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="1.27" dx="1.9" dy="1.9" layer="1" rot="R90"/>
</package>
<package name="USB-B-MICRO-SMD_RED_PASTE">
<description>&lt;h3&gt;USB - microB SMD Reduced Paste&lt;/h3&gt;
 70% paste area under D+ D- USBID pins for applications where those pins aren't required, to reduce the likelihood of bridges.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Micro-USB.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;USB_MICRO-B&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<text x="-4.66" y="5.96" size="0.3048" layer="51">PCB Front</text>
<wire x1="-3" y1="5.6" x2="-2.3" y2="5.3" width="0.08" layer="51"/>
<wire x1="-2.3" y1="5.3" x2="-2.4" y2="5.5" width="0.08" layer="51"/>
<wire x1="-2.3" y1="5.3" x2="-2.5" y2="5.2" width="0.08" layer="51"/>
<text x="-1.27" y="4.8006" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.8006" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
<smd name="D+" x="2.725" y="0" dx="0.4" dy="1.45" layer="1" rot="R90" cream="no"/>
<smd name="D-" x="2.725" y="0.65" dx="0.4" dy="1.45" layer="1" rot="R90" cream="no"/>
<smd name="VBUS" x="2.725" y="1.3" dx="0.4" dy="1.45" layer="1" rot="R90"/>
<smd name="ID" x="2.725" y="-0.65" dx="0.4" dy="1.45" layer="1" rot="R90" cream="no"/>
<smd name="GND" x="2.725" y="-1.3" dx="0.4" dy="1.45" layer="1" rot="R90"/>
<hole x="2.2" y="1.9" drill="0.85"/>
<hole x="2.2" y="-1.9" drill="0.85"/>
<rectangle x1="-0.75" y1="2.784" x2="0.75" y2="4.584" layer="31"/>
<rectangle x1="-0.35" y1="-0.75" x2="0.35" y2="0.75" layer="31"/>
<rectangle x1="-0.75" y1="-4.584" x2="0.75" y2="-2.784" layer="31"/>
<wire x1="-2.15" y1="3.9" x2="-2.15" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="3.9" x2="2.85" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="3.9" x2="-2.15" y2="3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="-3.9" x2="-2.15" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.981959375" y1="3.99288125" x2="2" y2="3.99288125" width="0.3048" layer="21"/>
<wire x1="2" y1="3.99288125" x2="2" y2="4" width="0.3048" layer="21"/>
<wire x1="3" y1="4" x2="3" y2="3" width="0.3048" layer="21"/>
<wire x1="2" y1="-4" x2="3" y2="-4" width="0.3048" layer="21"/>
<wire x1="3" y1="-4" x2="3" y2="-3" width="0.3048" layer="21"/>
<wire x1="-1" y1="4" x2="-2" y2="4" width="0.3048" layer="21"/>
<wire x1="-1" y1="-4" x2="-2" y2="-4" width="0.3048" layer="21"/>
<rectangle x1="-0.85" y1="2.684" x2="0.85" y2="4.684" layer="29"/>
<rectangle x1="-0.85" y1="-4.684" x2="0.85" y2="-2.684" layer="29"/>
<rectangle x1="-0.85" y1="-1.35" x2="0.85" y2="1.35" layer="29"/>
<smd name="SHIELD1" x="0" y="3.685" dx="1.8" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="SHIELD3" x="0" y="0" dx="2.5" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="SHIELD2" x="0" y="-3.685" dx="1.8" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<wire x1="-2" y1="7" x2="-2" y2="0" width="0" layer="51"/>
<rectangle x1="2.1" y1="0.5" x2="3.35" y2="0.8" layer="31"/>
<rectangle x1="2.1" y1="-0.15" x2="3.35" y2="0.15" layer="31"/>
<rectangle x1="2.1" y1="-0.8" x2="3.35" y2="-0.5" layer="31"/>
</package>
</packages>
<symbols>
<symbol name="USB-5PIN">
<description>&lt;h3&gt;USB - 5 pin&lt;/h3&gt;
&lt;p&gt;5 pin USB connector: VBUS, GND, D+, D-, ID &lt;/p&gt;
&lt;p&gt;Includes 4 pins for shield connections &lt;/p&gt;</description>
<wire x1="5.08" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="2.794" y="-3.302" size="2.54" layer="94" rot="R90">USB</text>
<pin name="D+" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="D-" x="-5.08" y="2.54" visible="pad" length="short"/>
<pin name="VBUS" x="-5.08" y="5.08" visible="pad" length="short" direction="pwr"/>
<pin name="GND" x="-5.08" y="-5.08" visible="pad" length="short" direction="pwr"/>
<pin name="ID" x="-5.08" y="-2.54" visible="pad" length="short"/>
<pin name="SHIELD" x="7.62" y="0" visible="pad" length="short" rot="R180"/>
<text x="-2.54" y="7.874" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-7.874" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="JST_2MM_MALE">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.778" x2="-2.54" y2="3.302" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.302" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.302" width="0.254" layer="94"/>
<wire x1="5.08" y1="3.302" x2="5.08" y2="1.778" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.778" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="4.064" y2="0" width="0.254" layer="94"/>
<wire x1="4.064" y1="0" x2="-1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.524" y2="-2.54" width="0.254" layer="94"/>
<pin name="-" x="0" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="+" x="2.54" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="PAD2" x="5.08" y="2.54" visible="off" length="point" rot="R90"/>
<pin name="PAD1" x="-2.54" y="2.54" visible="off" length="point" rot="R90"/>
<wire x1="-2.54" y1="1.778" x2="-1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.778" x2="-1.778" y2="3.302" width="0.254" layer="94"/>
<wire x1="-1.778" y1="3.302" x2="-2.54" y2="3.302" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.778" x2="4.318" y2="1.778" width="0.254" layer="94"/>
<wire x1="4.318" y1="1.778" x2="4.318" y2="3.302" width="0.254" layer="94"/>
<wire x1="4.318" y1="3.302" x2="5.08" y2="3.302" width="0.254" layer="94"/>
<wire x1="2.032" y1="1.016" x2="3.048" y2="1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="1.524" width="0.254" layer="94"/>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="U.FL">
<wire x1="0" y1="-2.54" x2="0" y2="-7.62" width="0.254" layer="94"/>
<circle x="0" y="0" radius="1.1359" width="0.254" layer="94"/>
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<pin name="GND@0" x="-2.54" y="-5.08" visible="off" length="short"/>
<pin name="SIGNAL" x="5.08" y="0" visible="off" length="middle" rot="R180"/>
<pin name="GND@1" x="-2.54" y="-7.62" visible="off" length="short"/>
<text x="-3.81" y="4.064" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST_2MM_MALE" prefix="J" uservalue="yes">
<description>&lt;h3&gt;JST 2MM MALE RA CONNECTOR&lt;/h3&gt;
Two pin, compact surface mount connector. Commonly used as a battery input connection point. We really like the solid locking feeling and high current rating on these small connectors. We use these all the time as battery connectors. Mates to single-cell LiPo batteries.

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Here is the connector we sell at SparkFun:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8612"&gt;JST Right Angle Connector - White&lt;/a&gt; (PRT-08612)&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.sparkfun.com/datasheets/Prototyping/Connectors/JST-Horizontal.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;It was used on these SparkFun products:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11214"&gt;SparkFun MOSFET Power Controller&lt;/a&gt; (PRT-11214)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13777"&gt;SparkFun Battery Babysitter - LiPo Battery Manager&lt;/a&gt; (PRT-13777)&lt;/li&gt;
&lt;li&gt;And many, many others that required a lipo battery connection.&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="JST_2MM_MALE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="+" pad="2"/>
<connect gate="G$1" pin="-" pad="1"/>
<connect gate="G$1" pin="PAD1" pad="NC1"/>
<connect gate="G$1" pin="PAD2" pad="NC2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
<attribute name="SF_ID" value="PRT-08612" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="U.FL" prefix="J" uservalue="yes">
<description>&lt;h3&gt;SMD Antenna Connector - U.FL&lt;/h3&gt;
&lt;p&gt;2mm height, 3.0mm x 3.0mm receptacle size. &lt;/p&gt;
&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13287"&gt;SparkFun WiFi Shield - ESP8266&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13231”&gt;SparkFun ESP8266 Thing&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/11420”&gt;SparkFun MiniGen - Pro Mini Signal Generator Shield&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="U.FL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="U.FL">
<connects>
<connect gate="G$1" pin="GND@0" pad="P$1"/>
<connect gate="G$1" pin="GND@1" pad="P$2"/>
<connect gate="G$1" pin="SIGNAL" pad="P$3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09193"/>
<attribute name="SF_ID" value="WRL-09144" constant="no"/>
<attribute name="VALUE" value="U.FL"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB_MICRO-B" prefix="J" uservalue="yes">
<description>&lt;h3&gt;USB Type Micro-B Connector&lt;/h3&gt;
&lt;p&gt;Male/female and PTH/SMT variants&lt;/p&gt;
&lt;p&gt;SparkFun Products:
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12035”&gt;SparkFun microB USB Breakout&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/10031”&gt;SparkFun USB microB Plug Breakout&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13231”&gt;SparkFun ESP8266 Thing&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="USB-5PIN" x="0" y="0"/>
</gates>
<devices>
<device name="_MALE-VERT" package="USB_MICROB_VERTICAL">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ID" pad="ID"/>
<connect gate="G$1" pin="SHIELD" pad="SHLD1 SHLD2 SHLD3 SHLD4"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09900"/>
</technology>
</technologies>
</device>
<device name="_FEMALE-SMT" package="USB-B-MICRO-SMD">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ID" pad="ID"/>
<connect gate="G$1" pin="SHIELD" pad="SHIELD1 SHIELD2 SHIELD3"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11752" constant="no"/>
</technology>
</technologies>
</device>
<device name="_SMT" package="USB-MICROB">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ID" pad="ID"/>
<connect gate="G$1" pin="SHIELD" pad="MT1 MT2 P$1 P$2"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09505" constant="no"/>
<attribute name="SF_ID" value="PRT-08533" constant="no"/>
</technology>
</technologies>
</device>
<device name="REDUCED_PASTE" package="USB-B-MICRO-SMD_RED_PASTE">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ID" pad="ID"/>
<connect gate="G$1" pin="SHIELD" pad="SHIELD1 SHIELD2 SHIELD3"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11752"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="sensor-subghz">
<packages>
<package name="DSK-VSON-10">
<smd name="6" x="0" y="0" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="7" x="0.5" y="0" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="8" x="1" y="0" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="9" x="1.5" y="0" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="10" x="2" y="0" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="1" x="2" y="2.5" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="2.5" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="3" x="1" y="2.5" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="4" x="0.5" y="2.5" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="5" x="0" y="2.5" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="EGP" x="1" y="1.25" dx="1.2" dy="2" layer="1" rot="R90"/>
<wire x1="-0.54" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="-0.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.54" y1="2.54" x2="-0.54" y2="0" width="0.127" layer="21"/>
<circle x="2.16" y="2.14" radius="0.1456" width="0.127" layer="51"/>
<text x="-1.27" y="3.81" size="1.27" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="6PWSON">
<smd name="3" x="0" y="0" dx="0.6" dy="0.4" layer="1"/>
<smd name="2" x="0" y="1" dx="0.6" dy="0.4" layer="1"/>
<smd name="1" x="0" y="2" dx="0.6" dy="0.4" layer="1"/>
<smd name="6" x="2.8" y="2" dx="0.6" dy="0.4" layer="1"/>
<smd name="5" x="2.8" y="1" dx="0.6" dy="0.4" layer="1"/>
<smd name="4" x="2.8" y="0" dx="0.6" dy="0.4" layer="1"/>
<smd name="P$7" x="1.4" y="1" dx="1.5" dy="2.4" layer="1"/>
<wire x1="-0.381" y1="-0.508" x2="3.175" y2="-0.508" width="0.15" layer="21"/>
<wire x1="3.175" y1="-0.508" x2="3.175" y2="2.54" width="0.15" layer="21"/>
<wire x1="3.175" y1="2.54" x2="-0.381" y2="2.54" width="0.15" layer="21"/>
<wire x1="-0.381" y1="2.54" x2="-0.381" y2="-0.508" width="0.15" layer="21"/>
<circle x="-0.889" y="3.048" radius="0.254" width="0.15" layer="21"/>
<text x="-1.397" y="3.81" size="1.27" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="TPS6303X">
<wire x1="0" y1="0" x2="22.86" y2="0" width="0.254" layer="94"/>
<wire x1="22.86" y1="0" x2="22.86" y2="33.02" width="0.254" layer="94"/>
<wire x1="22.86" y1="33.02" x2="0" y2="33.02" width="0.254" layer="94"/>
<wire x1="0" y1="33.02" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="L1" x="-5.08" y="30.48" length="middle"/>
<pin name="L2" x="27.94" y="30.48" length="middle" rot="R180"/>
<pin name="VIN" x="-5.08" y="25.4" length="middle"/>
<pin name="VINA" x="-5.08" y="22.86" length="middle"/>
<pin name="EN" x="-5.08" y="17.78" length="middle"/>
<pin name="PY/SYNC" x="-5.08" y="15.24" length="middle"/>
<pin name="GND" x="-5.08" y="2.54" length="middle"/>
<pin name="PGND" x="27.94" y="2.54" length="middle" rot="R180"/>
<pin name="FB" x="27.94" y="17.78" length="middle" rot="R180"/>
<pin name="VOUT" x="27.94" y="25.4" length="middle" rot="R180"/>
<text x="0" y="-5.08" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="-7.62" size="1.778" layer="96">&gt;Value</text>
<pin name="EPAD" x="27.94" y="10.16" length="middle" rot="R180"/>
</symbol>
<symbol name="HDC1080">
<description>i2c humidity and temperature sensor</description>
<wire x1="0" y1="5.08" x2="17.78" y2="5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="5.08" x2="17.78" y2="20.32" width="0.254" layer="94"/>
<wire x1="17.78" y1="20.32" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="0" y2="5.08" width="0.254" layer="94"/>
<pin name="SDA" x="-5.08" y="15.24" length="middle"/>
<pin name="SCL" x="-5.08" y="10.16" length="middle"/>
<pin name="VCC" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="GND" x="22.86" y="10.16" length="middle" rot="R180"/>
<text x="0" y="22.86" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="0" size="1.778" layer="95">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPS6303X" prefix="REG">
<description>TPS6303x High Efficiency Single Inductor Buck-Boost Converter With 1-A Switches</description>
<gates>
<gate name="G$1" symbol="TPS6303X" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DSK-VSON-10">
<connects>
<connect gate="G$1" pin="EN" pad="6"/>
<connect gate="G$1" pin="EPAD" pad="EGP"/>
<connect gate="G$1" pin="FB" pad="10"/>
<connect gate="G$1" pin="GND" pad="9"/>
<connect gate="G$1" pin="L1" pad="4"/>
<connect gate="G$1" pin="L2" pad="2"/>
<connect gate="G$1" pin="PGND" pad="3"/>
<connect gate="G$1" pin="PY/SYNC" pad="7"/>
<connect gate="G$1" pin="VIN" pad="5"/>
<connect gate="G$1" pin="VINA" pad="8"/>
<connect gate="G$1" pin="VOUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDC1080" prefix="U">
<description>HDC1080 Low Power, High Accuracy Digital Humidity Sensor with Temperature Sensor

http://www.ti.com/lit/ds/symlink/hdc1080.pdf</description>
<gates>
<gate name="G$1" symbol="HDC1080" x="0" y="-5.08"/>
</gates>
<devices>
<device name="" package="6PWSON">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="VCC" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Coils">
<description>&lt;h3&gt;SparkFun Coils&lt;/h3&gt;
In this library you'll find magnetics.

&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;Inductors&lt;/li&gt;
&lt;li&gt;Ferrite Beads&lt;/li&gt;
&lt;li&gt;Transformers&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;

&lt;br&gt;
&lt;p&gt;We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.&lt;/p&gt;</description>
<packages>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CR75">
<description>&lt;h3&gt;4600 series footprint&lt;/h3&gt;
&lt;p&gt;Not messing with it since production uses it. Origin unknown but loosely based on this &lt;a href="http://www.murata-ps.com/data/magnetics/kmp_4600.pdf"&gt;datasheet&lt;/a&gt;.&lt;/p&gt;</description>
<wire x1="-4.025" y1="3.65" x2="3.975" y2="3.65" width="0.127" layer="21"/>
<wire x1="3.975" y1="3.65" x2="3.975" y2="2.55" width="0.127" layer="21"/>
<wire x1="-4.025" y1="3.65" x2="-4.025" y2="2.55" width="0.127" layer="21"/>
<wire x1="-4.025" y1="-3.65" x2="3.975" y2="-3.65" width="0.127" layer="21"/>
<wire x1="3.975" y1="-3.65" x2="3.975" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-4.025" y1="-3.65" x2="-4.025" y2="-2.55" width="0.127" layer="21"/>
<smd name="P$1" x="-3.025" y="0" dx="4.7" dy="1.75" layer="1" rot="R90"/>
<smd name="P$2" x="3.025" y="0" dx="4.7" dy="1.75" layer="1" rot="R90"/>
<text x="0" y="3.81" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.81" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="SRN6045">
<description>&lt;h3&gt;SRN6045 series  footprint&lt;/h3&gt;
&lt;p&gt;Roughly based on the recommendation in this &lt;a href="http://www.mouser.com/ds/2/54/RN6045-778135.pdf"&gt;datasheet&lt;/a&gt;.&lt;/p&gt;</description>
<smd name="1" x="-2.175" y="0" dx="6" dy="2.5" layer="1" rot="R90"/>
<smd name="2" x="2.125" y="0" dx="6" dy="2.5" layer="1" rot="R90"/>
<wire x1="-2.175" y1="-3" x2="-3.175" y2="-2" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-2" x2="-3.175" y2="2" width="0.127" layer="51"/>
<wire x1="-3.175" y1="2" x2="-2.175" y2="3" width="0.127" layer="51"/>
<wire x1="-2.175" y1="3" x2="2.125" y2="3" width="0.127" layer="51"/>
<wire x1="2.125" y1="3" x2="3.125" y2="2" width="0.127" layer="51"/>
<wire x1="3.125" y1="2" x2="3.125" y2="-2" width="0.127" layer="51"/>
<wire x1="3.125" y1="-2" x2="2.125" y2="-3" width="0.127" layer="51"/>
<wire x1="2.125" y1="-3" x2="-2.175" y2="-3" width="0.127" layer="51"/>
<text x="0" y="3.223" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.254" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-0.762" y1="2.969" x2="0.762" y2="2.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-3" x2="0.762" y2="-3" width="0.1524" layer="21"/>
</package>
<package name="INDUCTOR_4.7UH">
<description>&lt;h3&gt;CDRH2D18/HPNP footprint&lt;/h3&gt;
&lt;p&gt;Recommended footprint for CDRH2D18/HPNP series inductors from &lt;a href="http://products.sumida.com/products/pdf/CDRH2D18HP.pdf"&gt;here&lt;/a&gt;.&lt;/p&gt;</description>
<wire x1="-1.2" y1="0.9" x2="-0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="1.5" x2="1.2" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-0.9" x2="-0.6783" y2="-1.3739" width="0.2032" layer="21"/>
<wire x1="-0.6783" y1="-1.3739" x2="0.6783" y2="-1.3739" width="0.2032" layer="21" curve="85.420723"/>
<wire x1="0.6783" y1="-1.3739" x2="1.2" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.6" x2="-0.7071" y2="-1.3929" width="0.03" layer="51"/>
<wire x1="-0.7071" y1="-1.3929" x2="0.7071" y2="-1.3929" width="0.03" layer="51" curve="90"/>
<wire x1="0.7071" y1="-1.3929" x2="1.5" y2="-0.6" width="0.03" layer="51"/>
<wire x1="1.5" y1="-0.6" x2="1.5" y2="0.6" width="0.03" layer="51"/>
<wire x1="1.5" y1="0.6" x2="0.6" y2="1.5" width="0.03" layer="51"/>
<wire x1="0.6" y1="1.5" x2="-0.6" y2="1.5" width="0.03" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="-1.5" y2="0.6" width="0.03" layer="51"/>
<wire x1="-1.5" y1="0.6" x2="-1.5" y2="-0.6" width="0.03" layer="51"/>
<smd name="P$1" x="-1.5" y="0" dx="1.3" dy="1.3" layer="1" rot="R90"/>
<smd name="P$2" x="1.5" y="0" dx="1.3" dy="1.3" layer="1" rot="R90"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="INDUCTOR_SDR1307">
<description>&lt;h3&gt;SDR1307 series footprint&lt;/h3&gt;
&lt;p&gt;Footprint based on recommendation from &lt;a href="https://www.bourns.com/pdfs/SDR1307.pdf"&gt;here&lt;/a&gt;.&lt;/p&gt;</description>
<smd name="P$1" x="0" y="4.6" dx="14" dy="4.75" layer="1"/>
<smd name="P$2" x="0" y="-4.6" dx="14" dy="4.75" layer="1"/>
<wire x1="-6.5" y1="1.5" x2="-6.5" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="6.5" y1="1.5" x2="6.5" y2="-1.5" width="0.3048" layer="21"/>
<text x="0" y="7.246" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.23" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="CR54">
<description>&lt;h3&gt;CR54 footprint&lt;/h3&gt;
&lt;p&gt;These vary by manufacturer, but we used the NPIS54LS footprint from &lt;a href="http://www.niccomp.com/catalog/npis_ls.pdf"&gt;here&lt;/a&gt;.</description>
<wire x1="2.6" y1="2.6" x2="-2.6" y2="2.6" width="0.127" layer="51"/>
<wire x1="-2.6" y1="2.6" x2="-2.6" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.6" y1="-2.6" x2="2.6" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2.6" y1="-2.6" x2="2.6" y2="2.6" width="0.127" layer="51"/>
<wire x1="-2.87" y1="2.6" x2="-2.87" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-2.87" x2="2.6" y2="-2.87" width="0.2032" layer="21"/>
<wire x1="2.87" y1="-2.6" x2="2.87" y2="2.6" width="0.2032" layer="21"/>
<wire x1="2.6" y1="2.87" x2="-2.6" y2="2.87" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="1.85" dx="4.2" dy="1.4" layer="1"/>
<smd name="P$2" x="0" y="-1.85" dx="4.2" dy="1.4" layer="1"/>
<text x="0" y="3.077" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.048" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<rectangle x1="-2.1" y1="1.15" x2="2.1" y2="2.55" layer="51"/>
<rectangle x1="-2.1" y1="-2.55" x2="2.1" y2="-1.15" layer="51"/>
<wire x1="-2.87" y1="-2.6" x2="-2.6" y2="-2.87" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-2.87" x2="2.87" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-2.87" y1="2.6" x2="-2.6" y2="2.87" width="0.2032" layer="21"/>
<wire x1="2.6" y1="2.87" x2="2.87" y2="2.6" width="0.2032" layer="21"/>
</package>
<package name="0805">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="FERRITE_BEAD">
<description>&lt;h3&gt;Ferrite Bead (blocks, cores, rings, chokes, etc.)&lt;/h3&gt;
&lt;p&gt;Inductor with layers of ferrite used to suppress high frequencies. Often used to isolate high frequency noise.&lt;/p&gt;</description>
<text x="1.27" y="2.54" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.27" y="-2.54" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="0.889" y1="2.54" x2="0.889" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.143" y1="2.54" x2="1.143" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="INDUCTOR">
<description>&lt;h3&gt;Inductors&lt;/h3&gt;
&lt;p&gt;Resist changes in electrical current. Basically a coil of wire.&lt;/p&gt;</description>
<text x="1.27" y="2.54" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.27" y="-2.54" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FERRITE_BEAD" prefix="FB">
<description>&lt;h3&gt;Ferrite Bead (blocks, cores, rings, chokes, etc.)&lt;/h3&gt;
&lt;p&gt;Inductor with layers of ferrite used to suppress high frequencies. Often used to isolate high frequency noise.&lt;/p&gt;
&lt;p&gt;SparkFun Products:
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13613"&gt;IOIO-OTG - V2.2&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13664"&gt;SparkFun SAMD21 Mini Breakout&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13339"&gt;SparkFun 6 Degrees of Freedom Breakout - LSM6DS3&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13672"&gt;SparkFun SAMD21 Dev Breakout&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="FERRITE_BEAD" x="0" y="0"/>
</gates>
<devices>
<device name="-0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-07859"/>
<attribute name="VALUE" value="30Ω/1.8A"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L">
<description>&lt;h3&gt;Inductors&lt;/h3&gt;
&lt;p&gt;Resist changes in electrical current. Basically a coil of wire.&lt;/p&gt;
&lt;p&gt;SparkFun Products:
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13879"&gt;SparkFun Load Cell Amplifier - HX711&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13613"&gt;IOIO-OTG - V2.2&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=""&gt;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-CR75-68UH" package="CR75">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-09739"/>
<attribute name="VALUE" value="68µH/±20%/1.05A"/>
</technology>
</technologies>
</device>
<device name="-SRN6045-33UH" package="SRN6045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-11627"/>
<attribute name="VALUE" value="33µH/±20%/1.4A"/>
</technology>
</technologies>
</device>
<device name="-CDRH-4.7UH" package="INDUCTOR_4.7UH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-10146"/>
<attribute name="VALUE" value="4.7µH/±30%/1.2A"/>
</technology>
</technologies>
</device>
<device name="-SDR13-27UH" package="INDUCTOR_SDR1307">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-12529"/>
<attribute name="VALUE" value="27µH/±20%/3.3A"/>
</technology>
</technologies>
</device>
<device name="-CR54-3.3UH" package="CR54">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-11157"/>
<attribute name="VALUE" value="3.3µH/±30%/3.4A"/>
</technology>
</technologies>
</device>
<device name="-CR54-47UH" package="CR54">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-10789"/>
<attribute name="VALUE" value="47µH/±30%/750mA"/>
</technology>
</technologies>
</device>
<device name="-0805-3.3UH" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-13076"/>
<attribute name="VALUE" value="3.3µH/±20%/450mA"/>
</technology>
</technologies>
</device>
<device name="-0603-33NH" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-07874"/>
<attribute name="VALUE" value="33nH/±5%/500mA"/>
</technology>
</technologies>
</device>
<device name="-0402-3.9NH" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-13457" constant="no"/>
<attribute name="VALUE" value="3.9NH" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CAP-PTH-10MM">
<description>2 PTH spaced 10mm apart</description>
<wire x1="-0.5" y1="0.635" x2="-0.5" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-5" y="0" drill="0.9" diameter="1.651"/>
<pad name="2" x="5" y="0" drill="0.9" diameter="1.651"/>
<text x="0" y="1" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="-0.5" y1="0" x2="-0.5" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0.5" y1="0.635" x2="0.5" y2="0" width="0.2032" layer="21"/>
<wire x1="0.5" y1="0" x2="0.5" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0" x2="-3.5" y2="0" width="0.2032" layer="21"/>
<wire x1="0.5" y1="0" x2="3.5" y2="0" width="0.2032" layer="21"/>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="1206">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0805">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="1210">
<description>&lt;p&gt;&lt;b&gt;Generic 3225 (1210) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.5365" y1="1.1865" x2="1.5365" y2="1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="1.1865" x2="1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="-1.1865" x2="-1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="-1.5365" y1="-1.1865" x2="-1.5365" y2="1.1865" width="0.127" layer="51"/>
<smd name="1" x="-1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<smd name="2" x="1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<text x="0" y="1.397" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.59" y1="1.45" x2="2.59" y2="1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="1.45" x2="2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="-1.45" x2="-2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="-2.59" y1="-1.45" x2="-2.59" y2="1.45" width="0.0508" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10NF" prefix="C">
<description>&lt;h3&gt;0.01uF/10nF/10,000pF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;

CAP-09321</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-PTH-10MM-10000V-1-%" package="CAP-PTH-10MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09321"/>
<attribute name="VALUE" value="10nF"/>
</technology>
</technologies>
</device>
<device name="-0603-50V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00867"/>
<attribute name="VALUE" value="10nF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.1UF" prefix="C">
<description>&lt;h3&gt;0.1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12416"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-(+80/-20%)" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08604"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-KIT-EZ-50V-20%" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08370"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-100V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08390"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10UF" prefix="C">
<description>&lt;h3&gt;10.0µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-6.3V-20%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11015"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-1206-6.3V-20%" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-10057"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-0805-10V-10%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11330"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-1210-50V-20%" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09824"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1.0UF" prefix="C">
<description>&lt;h3&gt;1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-16V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00868"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12417"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-(+80/-20%)" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11625"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-1206-50V-10%" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09822"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-10%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08064"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1.5PF" prefix="C">
<description>&lt;h3&gt;1.5pF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-50V-16.667%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08763" constant="no"/>
<attribute name="VALUE" value="1.5pF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="3.3V">
<description>&lt;h3&gt;3.3V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" prefix="SUPPLY">
<description>&lt;h3&gt;3.3V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 3.3V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-IC-Comms">
<description>&lt;h3&gt;SparkFun Communication ICs&lt;/h3&gt;
This is the communications library, which contains things that exist on wired busses.  This includes USB to serial conversion ICs, like the FTDI line, plus high speed line drivers, level shifters, bus drivers, CAN transceivers and ethernet PHYs.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SSOP28DB">
<description>&lt;h3&gt;Small Shrink Outline Package - 28 DB&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 28&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;FT232RL&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.2" y1="2.925" x2="5.2" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="-5.2" y1="-2.925" x2="-5.2" y2="2.925" width="0.1524" layer="21"/>
<wire x1="-5.038" y1="2.763" x2="5.038" y2="2.763" width="0.0508" layer="27"/>
<wire x1="5.038" y1="2.763" x2="5.038" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="5.038" y1="-2.763" x2="-5.038" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="-5.038" y1="-2.763" x2="-5.038" y2="2.763" width="0.0508" layer="27"/>
<smd name="28" x="-4.225" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="27" x="-3.575" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="26" x="-2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="25" x="-2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="24" x="-1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="23" x="-0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="22" x="-0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="20" x="0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="21" x="0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="19" x="1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="18" x="2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="17" x="2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="16" x="3.575" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="15" x="4.225" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="1" x="-4.225" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="2" x="-3.575" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="3" x="-2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="4" x="-2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="5" x="-1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="6" x="-0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="7" x="-0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="8" x="0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="9" x="0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="10" x="1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="11" x="2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="12" x="2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="13" x="3.575" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="14" x="4.225" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<rectangle x1="-4.3875" y1="2.9656" x2="-4.0625" y2="3.9" layer="51"/>
<rectangle x1="-4.3875" y1="-3.9" x2="-4.0625" y2="-2.9656" layer="51"/>
<rectangle x1="-3.7375" y1="-3.9" x2="-3.4125" y2="-2.9656" layer="51"/>
<rectangle x1="-3.0875" y1="-3.9" x2="-2.7625" y2="-2.9656" layer="51"/>
<rectangle x1="-3.7375" y1="2.9656" x2="-3.4125" y2="3.9" layer="51"/>
<rectangle x1="-3.0875" y1="2.9656" x2="-2.7625" y2="3.9" layer="51"/>
<rectangle x1="-2.4375" y1="2.9656" x2="-2.1125" y2="3.9" layer="51"/>
<rectangle x1="-1.7875" y1="2.9656" x2="-1.4625" y2="3.9" layer="51"/>
<rectangle x1="-1.1375" y1="2.9656" x2="-0.8125" y2="3.9" layer="51"/>
<rectangle x1="-0.4875" y1="2.9656" x2="-0.1625" y2="3.9" layer="51"/>
<rectangle x1="0.1625" y1="2.9656" x2="0.4875" y2="3.9" layer="51"/>
<rectangle x1="0.8125" y1="2.9656" x2="1.1375" y2="3.9" layer="51"/>
<rectangle x1="1.4625" y1="2.9656" x2="1.7875" y2="3.9" layer="51"/>
<rectangle x1="2.1125" y1="2.9656" x2="2.4375" y2="3.9" layer="51"/>
<rectangle x1="2.7625" y1="2.9656" x2="3.0875" y2="3.9" layer="51"/>
<rectangle x1="3.4125" y1="2.9656" x2="3.7375" y2="3.9" layer="51"/>
<rectangle x1="4.0625" y1="2.9656" x2="4.3875" y2="3.9" layer="51"/>
<rectangle x1="-2.4375" y1="-3.9" x2="-2.1125" y2="-2.9656" layer="51"/>
<rectangle x1="-1.7875" y1="-3.9" x2="-1.4625" y2="-2.9656" layer="51"/>
<rectangle x1="-1.1375" y1="-3.9" x2="-0.8125" y2="-2.9656" layer="51"/>
<rectangle x1="-0.4875" y1="-3.9" x2="-0.1625" y2="-2.9656" layer="51"/>
<rectangle x1="0.1625" y1="-3.9" x2="0.4875" y2="-2.9656" layer="51"/>
<rectangle x1="0.8125" y1="-3.9" x2="1.1375" y2="-2.9656" layer="51"/>
<rectangle x1="1.4625" y1="-3.9" x2="1.7875" y2="-2.9656" layer="51"/>
<rectangle x1="2.1125" y1="-3.9" x2="2.4375" y2="-2.9656" layer="51"/>
<rectangle x1="2.7625" y1="-3.9" x2="3.0875" y2="-2.9656" layer="51"/>
<rectangle x1="3.4125" y1="-3.9" x2="3.7375" y2="-2.9656" layer="51"/>
<rectangle x1="4.0625" y1="-3.9" x2="4.3875" y2="-2.9656" layer="51"/>
<circle x="-5" y="-3.6" radius="0.3592" width="0" layer="21"/>
<wire x1="-5.2" y1="2.925" x2="-4.6" y2="2.925" width="0.1524" layer="21"/>
<wire x1="5.2" y1="2.925" x2="4.6" y2="2.925" width="0.1524" layer="21"/>
<wire x1="-5.2" y1="-2.925" x2="-4.6" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="5.2" y1="-2.925" x2="4.6" y2="-2.925" width="0.1524" layer="21"/>
<text x="-5.55625" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="6.19125" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="FT232R-BASIC">
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-7.62" y="15.748" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3V3OUT" x="-15.24" y="2.54" length="middle" direction="out"/>
<pin name="USBDM" x="-15.24" y="12.7" length="middle"/>
<pin name="USBDP" x="-15.24" y="10.16" length="middle"/>
<pin name="GND1" x="-15.24" y="-10.16" length="middle" direction="pwr"/>
<pin name="GND2" x="-15.24" y="-12.7" length="middle" direction="pwr"/>
<pin name="GND3" x="-15.24" y="-15.24" length="middle" direction="pwr"/>
<pin name="TXD" x="15.24" y="12.7" length="middle" direction="out" rot="R180"/>
<pin name="RXD" x="15.24" y="10.16" length="middle" direction="in" rot="R180"/>
<pin name="VCCIO" x="-15.24" y="0" length="middle" direction="pwr"/>
<pin name="AGND" x="-15.24" y="-7.62" length="middle" direction="pwr"/>
<pin name="TEST" x="-15.24" y="-5.08" length="middle" direction="in"/>
<pin name="VCC" x="-15.24" y="5.08" length="middle" direction="pwr"/>
<pin name="TXLED" x="15.24" y="-12.7" length="middle" function="dot" rot="R180"/>
<pin name="RXLED" x="15.24" y="-15.24" length="middle" function="dot" rot="R180"/>
<pin name="RTS" x="15.24" y="2.54" length="middle" function="dot" rot="R180"/>
<pin name="CTS" x="15.24" y="5.08" length="middle" function="dot" rot="R180"/>
<pin name="DTR" x="15.24" y="0" length="middle" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FT232RL-BASIC" prefix="U">
<description>&lt;h3&gt;FT232RL USB UART&lt;/h3&gt;
&lt;p&gt;This is the simplified version of the FT232RL unit. Only what you need, nothing you don't.&lt;/p&gt;
&lt;p&gt;FT232RL 4&lt;sup&gt;th&lt;/sup&gt; Generation USB UART (USB &amp;lt;-&amp;gt; Serial) Controller.&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.sparkfun.com/datasheets/IC/FT232R_v104.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/650"&gt;Component&lt;/a&gt; (COM-00650)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12097"&gt;SparkFun RedBot Mainboard&lt;/a&gt; (ROB-12097)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9716"&gt;SparkFun FTDI Basic Breakout - 5V&lt;/a&gt; (DEV-09716)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9873"&gt;SparkFun FTDI Basic Breakout - 3.3V&lt;/a&gt; (DEV-09873)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12942"&gt;Bus Pirate - v3.6a&lt;/a&gt; (TOL-12942)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12731"&gt;SparkFun USB to Serial Breakout - FT232RL&lt;/a&gt; (BOB-12731)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11888"&gt;SparkFun PicoBoard&lt;/a&gt; (WIG-11888)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9963"&gt;SparkFun RFID USB Reader&lt;/a&gt; (SEN-09963)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10275"&gt;LilyPad FTDI Basic Breakout - 5V&lt;/a&gt; (DEV-10275)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13899"&gt;SparkFun Stepoko&lt;/a&gt; (ROB-13899)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="FT232R-BASIC" x="0" y="0"/>
</gates>
<devices>
<device name="SSOP" package="SSOP28DB">
<connects>
<connect gate="G$1" pin="3V3OUT" pad="17"/>
<connect gate="G$1" pin="AGND" pad="25"/>
<connect gate="G$1" pin="CTS" pad="11"/>
<connect gate="G$1" pin="DTR" pad="2"/>
<connect gate="G$1" pin="GND1" pad="7"/>
<connect gate="G$1" pin="GND2" pad="18"/>
<connect gate="G$1" pin="GND3" pad="21"/>
<connect gate="G$1" pin="RTS" pad="3"/>
<connect gate="G$1" pin="RXD" pad="5"/>
<connect gate="G$1" pin="RXLED" pad="22"/>
<connect gate="G$1" pin="TEST" pad="26"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="TXLED" pad="23"/>
<connect gate="G$1" pin="USBDM" pad="16"/>
<connect gate="G$1" pin="USBDP" pad="15"/>
<connect gate="G$1" pin="VCC" pad="20"/>
<connect gate="G$1" pin="VCCIO" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-00870"/>
<attribute name="VALUE" value="FT232RL" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED_5MM">
<description>&lt;B&gt;LED 5mm PTH&lt;/B&gt;&lt;p&gt;
5 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED-IR-THRU&lt;/li&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM">
<description>&lt;h3&gt;LED 3MM PTH&lt;/h3&gt;

3 mm, round.

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="22" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="22" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="22" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="22" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="22"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="22"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED-1206">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
</package>
<package name="LED-0603">
<description>&lt;B&gt;LED 0603 SMT&lt;/B&gt;&lt;p&gt;
0603, surface mount.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.075inch &lt;/li&gt;
&lt;li&gt;Area: 0.06" x 0.03"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED - BLUE&lt;/li&gt;</description>
<smd name="C" x="0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<smd name="A" x="-0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<text x="0" y="0.635" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5875" y1="0.47625" x2="1.5875" y2="-0.47625" width="0.127" layer="21"/>
<wire x1="0.15875" y1="0.47625" x2="0.15875" y2="0" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="0.15875" y2="-0.47625" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="-0.3175" width="0.127" layer="51"/>
</package>
<package name="LED_10MM">
<description>&lt;B&gt;LED 10mm PTH&lt;/B&gt;&lt;p&gt;
10 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.2inch&lt;/li&gt;
&lt;li&gt;Diameter: 10mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="22"/>
<text x="0" y="5.715" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.715" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="51"/>
</package>
<package name="FKIT-LED-1206">
<description>&lt;B&gt;LED 1206 SMT&lt;/B&gt;&lt;p&gt;
1206, surface mount
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125"x 0.06" &lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="0.3175" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="-0.3175" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM-NS">
<description>&lt;h3&gt;LED 3MM PTH- No Silk&lt;/h3&gt;

3 mm, round, no silk outline of package

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.8923" y1="1.2954" x2="1.8923" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.8923" y1="-1.2954" x2="1.8923" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED_5MM-KIT">
<description>&lt;h3&gt;LED 5mm KIT PTH&lt;/h3&gt;
&lt;p&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example Device:
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED-1206-BOTTOM">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-3.429" y="-4.572" size="1.778" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="1.905" y="-4.572" size="1.778" layer="96" font="vector" rot="R90" align="top-left">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LED (Generic)&lt;/b&gt;
&lt;p&gt;Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. Generic LEDs with no color specified.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED_3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08794" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED_10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED_3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED_5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Jumpers">
<description>&lt;h3&gt;SparkFun Jumpers&lt;/h3&gt;
In this library you'll find jumpers, or other semipermanent means of changing current paths. The least permanent form is the solder jumper. These can be changed by adding, removing, or moving solder. In cases that are less likely to be changed we have jumpers that are connected with traces. These can be cut with a razor, or reconnected with solder. Reference designator JP.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SMT-JUMPER_3_NO_NO-SILK">
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<smd name="1" x="-0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
</package>
<package name="SMT-JUMPER_3_NO_SILK">
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.27" y1="-1.016" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.016" x2="1.524" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="0.762" x2="-1.27" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="-0.762" x2="-1.27" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.762" x2="-1.524" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.016" x2="1.27" y2="1.016" width="0.1524" layer="21"/>
<smd name="1" x="-0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="SMT-JUMPER_3_NO">
<wire x1="-0.635" y1="-1.397" x2="0.635" y2="-1.397" width="1.27" layer="94" curve="180" cap="flat"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<text x="2.54" y="0.381" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="2.54" y="-0.381" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.635" x2="1.27" y2="0.635" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JUMPER-SMT_3_NO" prefix="JP">
<description>&lt;h3&gt;Normally open jumper&lt;/h3&gt;
&lt;p&gt;This jumper has three pads in close proximity to each other. Apply solder to close the connection(s).&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SMT-JUMPER_3_NO" x="0" y="0"/>
</gates>
<devices>
<device name="_NO-SILK" package="SMT-JUMPER_3_NO_NO-SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SILK" package="SMT-JUMPER_3_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X08">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-10.2362" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.16" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
</package>
<package name="1X08/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-8.89" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-10.795" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="12.065" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINHD8">
<wire x1="-6.35" y1="-10.16" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X8" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X08">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X08/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Switches">
<description>&lt;h3&gt;SparkFun Switches, Buttons, Encoders&lt;/h3&gt;
In this library you'll find switches, buttons, joysticks, and anything that moves to create or disrupt an electrical connection.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SWITCH_SPDT_PTH_11.6X4.0MM">
<description>&lt;h3&gt;SPDT PTH Slide Switch&lt;/h3&gt;
&lt;p&gt;Single-pole, double-throw (SPDT) switch.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/Buttons/P040040c.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="2.175" y1="5.815" x2="-2.175" y2="5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="5.815" x2="-2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="-5.815" x2="2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="2.175" y1="-5.815" x2="2.175" y2="5.815" width="0.2032" layer="21"/>
<pad name="1" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0" y="-2.54" drill="1.016" diameter="1.8796"/>
<text x="-2.413" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;Name</text>
<text x="2.413" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;Value</text>
</package>
<package name="SWITCH_DPDT_SMD_AYZ0202">
<description>&lt;h3&gt;ITT Industries AYZ0202 DPDT Switch - SMD&lt;/h3&gt;
&lt;p&gt;Double-pole, double-throw switches.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/SW_slide_ayz.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-3.6" y1="1.75" x2="-3.6" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="-1.75" x2="3.6" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="3.6" y1="-1.75" x2="3.6" y2="1.75" width="0.2032" layer="21"/>
<wire x1="3.6" y1="1.75" x2="-3.6" y2="1.75" width="0.2032" layer="21"/>
<smd name="3" x="2.5" y="2.825" dx="1" dy="1.15" layer="1"/>
<smd name="2" x="0" y="2.825" dx="1" dy="1.15" layer="1"/>
<smd name="1" x="-2.5" y="2.825" dx="1" dy="1.15" layer="1"/>
<smd name="6" x="2.5" y="-2.825" dx="1" dy="1.15" layer="1"/>
<smd name="5" x="0" y="-2.825" dx="1" dy="1.15" layer="1"/>
<smd name="4" x="-2.5" y="-2.825" dx="1" dy="1.15" layer="1"/>
<hole x="1.5" y="0" drill="0.85"/>
<hole x="-1.5" y="0" drill="0.85"/>
<text x="-3.81" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;Name</text>
<text x="3.81" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;Value</text>
</package>
<package name="SWITCH_SPDT_PTH_11.6X4.0MM_LOCK">
<description>&lt;h3&gt;SPDT PTH Slide Switch - Locking Footprint&lt;/h3&gt;
&lt;p&gt;Single-pole, double-throw (SPDT) switch.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the LOCK version of this package. This package has offset PTH pins, which help to hold the part in place while soldering.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/Buttons/P040040c.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="2.175" y1="5.815" x2="-2.175" y2="5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="5.815" x2="-2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="-5.815" x2="2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="2.175" y1="-5.815" x2="2.175" y2="5.815" width="0.2032" layer="21"/>
<pad name="1" x="0" y="2.7178" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0" y="-2.7178" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2286" y1="-0.3048" x2="0.2286" y2="0.3048" layer="51"/>
<rectangle x1="-0.2286" y1="2.2352" x2="0.2286" y2="2.8448" layer="51"/>
<rectangle x1="-0.2286" y1="-2.8448" x2="0.2286" y2="-2.2352" layer="51"/>
<text x="-2.413" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;Name</text>
<text x="2.413" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;Value</text>
</package>
<package name="SWITCH_SPDT_PTH_11.6X4.0MM_KIT">
<description>&lt;h3&gt;SPDT PTH Slide Switch - KIT Footprint&lt;/h3&gt;
&lt;p&gt;Single-pole, double-throw (SPDT) switch.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/Buttons/P040040c.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="2.175" y1="5.815" x2="-2.175" y2="5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="5.815" x2="-2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="-5.815" x2="2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="2.175" y1="-5.815" x2="2.175" y2="5.815" width="0.2032" layer="21"/>
<pad name="1" x="0" y="2.7178" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="3" x="0" y="-2.7178" drill="1.016" diameter="1.8796" stop="no"/>
<rectangle x1="-0.2286" y1="-0.3048" x2="0.2286" y2="0.3048" layer="51"/>
<rectangle x1="-0.2286" y1="2.2352" x2="0.2286" y2="2.8448" layer="51"/>
<rectangle x1="-0.2286" y1="-2.8448" x2="0.2286" y2="-2.2352" layer="51"/>
<polygon width="0.127" layer="30">
<vertex x="-0.0178" y="1.8414" curve="-90.039946"/>
<vertex x="-0.8787" y="2.6975" curve="-90"/>
<vertex x="-0.0026" y="3.5916" curve="-90.006409"/>
<vertex x="0.8738" y="2.6975" curve="-90.03214"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-0.0051" y="-3.5967" curve="-90.006558"/>
<vertex x="-0.8788" y="-2.7431" curve="-90.037923"/>
<vertex x="0.0128" y="-1.8363" curve="-90.006318"/>
<vertex x="0.8814" y="-2.7432" curve="-90.038792"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-0.0102" y="-0.8738" curve="-90.019852"/>
<vertex x="-0.8762" y="-0.0203" curve="-90.019119"/>
<vertex x="0.0153" y="0.8789" curve="-90"/>
<vertex x="0.8739" y="-0.0077" curve="-90.038897"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="0" y="2.2758" curve="-90.012891"/>
<vertex x="-0.4445" y="2.7" curve="-90"/>
<vertex x="0" y="3.1673" curve="-90"/>
<vertex x="0.4419" y="2.7102" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="0.0026" y="-3.1648" curve="-90.012891"/>
<vertex x="-0.4419" y="-2.7406" curve="-90"/>
<vertex x="0.0026" y="-2.2733" curve="-90"/>
<vertex x="0.4445" y="-2.7304" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="0.0102" y="-0.4471" curve="-90.012891"/>
<vertex x="-0.4343" y="-0.0229" curve="-90"/>
<vertex x="0.0102" y="0.4444" curve="-90"/>
<vertex x="0.4521" y="-0.0127" curve="-90.012967"/>
</polygon>
<text x="-2.413" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;Name</text>
<text x="2.413" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;Value</text>
</package>
<package name="SWITCH_SPST_SMD_A">
<description>&lt;h3&gt;SPDT Slide Switch - SMD&lt;/h3&gt;
&lt;p&gt;Single-pole, double-throw (SPDT) switch.&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/Switches/SLIDE.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-3.35" y1="1.3" x2="-3.35" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-3.35" y1="-1.3" x2="3.35" y2="-1.3" width="0.127" layer="51"/>
<wire x1="3.35" y1="-1.3" x2="3.35" y2="1.3" width="0.127" layer="51"/>
<wire x1="3.35" y1="1.3" x2="-0.1" y2="1.3" width="0.127" layer="51"/>
<wire x1="-0.1" y1="1.3" x2="-1.4" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.4" y1="1.3" x2="-3.35" y2="1.3" width="0.127" layer="51"/>
<wire x1="-0.1" y1="1.3" x2="-0.1" y2="2.8" width="0.127" layer="51"/>
<wire x1="-0.1" y1="2.8" x2="-1.4" y2="2.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="2.8" x2="-1.4" y2="1.3" width="0.127" layer="51"/>
<wire x1="-3.477" y1="0.427" x2="-3.477" y2="-0.427" width="0.2032" layer="21"/>
<wire x1="3.477" y1="0.427" x2="3.477" y2="-0.427" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.427" x2="-2.7" y2="1.427" width="0.2032" layer="21"/>
<wire x1="1.627" y1="-1.427" x2="-0.127" y2="-1.427" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="-1.75" dx="0.7" dy="1.5" layer="1" rot="R180"/>
<smd name="2" x="-0.75" y="-1.75" dx="0.7" dy="1.5" layer="1" rot="R180"/>
<smd name="3" x="2.25" y="-1.75" dx="0.7" dy="1.5" layer="1" rot="R180"/>
<smd name="GND1" x="-3.65" y="1" dx="1" dy="0.6" layer="1"/>
<smd name="GND2" x="-3.65" y="-1.1" dx="1" dy="0.8" layer="1"/>
<smd name="GND3" x="3.65" y="1" dx="1" dy="0.6" layer="1"/>
<smd name="GND4" x="3.65" y="-1.1" dx="1" dy="0.8" layer="1"/>
<hole x="-1.5" y="0" drill="0.9"/>
<hole x="1.5" y="0" drill="0.9"/>
<text x="0.762" y="-1.397" size="0.6096" layer="25" font="vector" ratio="20" rot="R180" align="bottom-center">&gt;Name</text>
<text x="0" y="1.397" size="0.6096" layer="27" font="vector" ratio="20" rot="R180" align="top-center">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="TOGGLE">
<description>&lt;h3&gt;Single Pole, Double Throw (SPDT) Switch&lt;/h3&gt;
&lt;p&gt;Single-pole, double-throw (SPDT) switch.&lt;/p&gt;</description>
<wire x1="0" y1="0" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.175" y2="-2.54" width="0.127" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0.2032" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0.2032" layer="94"/>
<circle x="0" y="0" radius="0.3592" width="0.2032" layer="94"/>
<text x="1.27" y="3.048" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="1.016" y="-3.302" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="P" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="S" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="O" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SWITCH-SPDT" prefix="S" uservalue="yes">
<description>&lt;h3&gt;Single Pole, Double Throw (SPDT) Switch&lt;/h3&gt;
&lt;p&gt;Single-pole, double-throw (SPDT) switch.&lt;/p&gt;
&lt;h4&gt;Variant Overview&lt;/h4&gt;
&lt;h5&gt;SMD-AYZ0202&lt;/h5&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/597"&gt;Surface Mount DPDT Switch&lt;/a&gt; (COM-00597)&lt;/li&gt;
&lt;li&gt;Used on e.g. &lt;a href="https://www.sparkfun.com/products/12049"&gt;LilyPad Arduino USB - ATmega32U4 Board&lt;/a&gt;
&lt;/ul&gt;
&lt;h5&gt;PTH-11.6X4.0MM&lt;/h5&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/102"&gt;SPDT Mini Power Switch&lt;/a&gt; (COM-00102)&lt;/li&gt;
&lt;li&gt;Used on e.g. &lt;a href="https://www.sparkfun.com/products/10547"&gt;SparkFun Simon Says - Through-Hole Soldering Kit&lt;/a&gt; (KIT-10547)&lt;/li&gt;
&lt;/ul&gt;
&lt;h5&gt;PTH-11.6X4.0MM-KIT&lt;/h5&gt;
&lt;ul&gt;
&lt;li&gt;KIT version of SPDT Mini Power Switch - Simplifies soldering by removing tStop of switch pins - only one side can be soldered.&lt;/li&gt;
&lt;/ul&gt;
&lt;h5&gt;PTH-11.6X4.0MM-LOCK&lt;/h5&gt;
&lt;ul&gt;
&lt;li&gt;LOCK version of SPDT Mini Power Switch - Offset pins hold switch in place for easier soldering.&lt;/li&gt;
&lt;/ul&gt;
&lt;h5&gt;SMD-RIGHT-ANGLE&lt;/h5&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10860"&gt;Surface Mount Right Angle Switch&lt;/a&gt; (COM-10860)&lt;/li&gt;
&lt;li&gt;Used on e.g. &lt;a href="https://www.sparkfun.com/products/13231"&gt;SparkFun ESP8266 Thing&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="1" symbol="TOGGLE" x="-2.54" y="0"/>
</gates>
<devices>
<device name="-PTH-11.6X4.0MM" package="SWITCH_SPDT_PTH_11.6X4.0MM">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08261"/>
<attribute name="SF_SKU" value="COM-00102"/>
</technology>
</technologies>
</device>
<device name="-SMD-AYZ0202" package="SWITCH_DPDT_SMD_AYZ0202">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08179" constant="no"/>
<attribute name="SF_ID" value="COM-00597" constant="no"/>
</technology>
</technologies>
</device>
<device name="-PTH-11.6X4.0MM-LOCK" package="SWITCH_SPDT_PTH_11.6X4.0MM_LOCK">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08261"/>
<attribute name="SF_SKU" value="COM-00102 "/>
</technology>
</technologies>
</device>
<device name="-PTH-11.6X4.0MM-KIT" package="SWITCH_SPDT_PTH_11.6X4.0MM_KIT">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08261"/>
<attribute name="SF_SKU" value="COM-00102"/>
</technology>
</technologies>
</device>
<device name="-SMD-RIGHT-ANGLE" package="SWITCH_SPST_SMD_A">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-10651"/>
<attribute name="SF_SKU" value="COM-10860"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="aestethics">
<packages>
<package name="MUTEX-LOGO">
<rectangle x1="5.9182" y1="0.9906" x2="6.0706" y2="1.0414" layer="37"/>
<rectangle x1="5.8674" y1="0.9906" x2="5.9182" y2="1.0414" layer="37"/>
<rectangle x1="6.0706" y1="0.9906" x2="6.1214" y2="1.0414" layer="37"/>
<rectangle x1="5.8166" y1="1.0414" x2="6.1722" y2="1.0922" layer="37"/>
<rectangle x1="8.509" y1="1.0414" x2="9.3218" y2="1.0922" layer="37"/>
<rectangle x1="3.2766" y1="1.0414" x2="3.3274" y2="1.0922" layer="37"/>
<rectangle x1="4.0894" y1="1.0414" x2="4.1402" y2="1.0922" layer="37"/>
<rectangle x1="4.9022" y1="1.0414" x2="4.953" y2="1.0922" layer="37"/>
<rectangle x1="5.7658" y1="1.0414" x2="5.8166" y2="1.0922" layer="37"/>
<rectangle x1="6.1722" y1="1.0414" x2="6.223" y2="1.0922" layer="37"/>
<rectangle x1="7.493" y1="1.0414" x2="7.5438" y2="1.0922" layer="37"/>
<rectangle x1="9.6774" y1="1.0414" x2="9.7282" y2="1.0922" layer="37"/>
<rectangle x1="10.8458" y1="1.0414" x2="10.8966" y2="1.0922" layer="37"/>
<rectangle x1="10.8966" y1="1.0414" x2="10.9474" y2="1.0922" layer="37"/>
<rectangle x1="5.715" y1="1.0414" x2="5.7658" y2="1.0922" layer="37"/>
<rectangle x1="6.223" y1="1.0414" x2="6.2738" y2="1.0922" layer="37"/>
<rectangle x1="8.4582" y1="1.0414" x2="8.509" y2="1.0922" layer="37"/>
<rectangle x1="4.0386" y1="1.0414" x2="4.0894" y2="1.0922" layer="37"/>
<rectangle x1="4.8514" y1="1.0414" x2="4.9022" y2="1.0922" layer="37"/>
<rectangle x1="9.3218" y1="1.0414" x2="9.3726" y2="1.0922" layer="37"/>
<rectangle x1="4.1402" y1="1.0414" x2="4.191" y2="1.0922" layer="37"/>
<rectangle x1="3.3274" y1="1.0414" x2="3.3782" y2="1.0922" layer="37"/>
<rectangle x1="9.7282" y1="1.0414" x2="9.779" y2="1.0922" layer="37"/>
<rectangle x1="7.5438" y1="1.0414" x2="7.5946" y2="1.0922" layer="37"/>
<rectangle x1="3.2766" y1="1.0922" x2="3.3274" y2="1.143" layer="37"/>
<rectangle x1="4.0386" y1="1.0922" x2="4.1402" y2="1.143" layer="37"/>
<rectangle x1="4.9022" y1="1.0922" x2="4.953" y2="1.143" layer="37"/>
<rectangle x1="5.6642" y1="1.0922" x2="5.7658" y2="1.143" layer="37"/>
<rectangle x1="6.1722" y1="1.0922" x2="6.3246" y2="1.143" layer="37"/>
<rectangle x1="7.493" y1="1.0922" x2="7.5438" y2="1.143" layer="37"/>
<rectangle x1="8.4582" y1="1.0922" x2="8.5598" y2="1.143" layer="37"/>
<rectangle x1="9.7282" y1="1.0922" x2="9.779" y2="1.143" layer="37"/>
<rectangle x1="10.8458" y1="1.0922" x2="10.8966" y2="1.143" layer="37"/>
<rectangle x1="4.1402" y1="1.0922" x2="4.191" y2="1.143" layer="37"/>
<rectangle x1="5.7658" y1="1.0922" x2="5.8166" y2="1.143" layer="37"/>
<rectangle x1="8.5598" y1="1.0922" x2="8.6106" y2="1.143" layer="37"/>
<rectangle x1="9.6774" y1="1.0922" x2="9.7282" y2="1.143" layer="37"/>
<rectangle x1="8.6106" y1="1.0922" x2="9.3218" y2="1.143" layer="37"/>
<rectangle x1="6.1214" y1="1.0922" x2="6.1722" y2="1.143" layer="37"/>
<rectangle x1="5.8166" y1="1.0922" x2="5.8674" y2="1.143" layer="37"/>
<rectangle x1="5.8674" y1="1.0922" x2="5.9182" y2="1.143" layer="37"/>
<rectangle x1="6.0706" y1="1.0922" x2="6.1214" y2="1.143" layer="37"/>
<rectangle x1="4.8514" y1="1.0922" x2="4.9022" y2="1.143" layer="37"/>
<rectangle x1="3.3274" y1="1.0922" x2="3.3782" y2="1.143" layer="37"/>
<rectangle x1="10.8966" y1="1.0922" x2="10.9474" y2="1.143" layer="37"/>
<rectangle x1="10.795" y1="1.0922" x2="10.8458" y2="1.143" layer="37"/>
<rectangle x1="9.3218" y1="1.0922" x2="9.3726" y2="1.143" layer="37"/>
<rectangle x1="5.6134" y1="1.0922" x2="5.6642" y2="1.143" layer="37"/>
<rectangle x1="7.5438" y1="1.0922" x2="7.5946" y2="1.143" layer="37"/>
<rectangle x1="6.3246" y1="1.0922" x2="6.3754" y2="1.143" layer="37"/>
<rectangle x1="3.2766" y1="1.143" x2="3.3274" y2="1.1938" layer="37"/>
<rectangle x1="4.0386" y1="1.143" x2="4.191" y2="1.1938" layer="37"/>
<rectangle x1="4.9022" y1="1.143" x2="4.953" y2="1.1938" layer="37"/>
<rectangle x1="5.6134" y1="1.143" x2="5.6642" y2="1.1938" layer="37"/>
<rectangle x1="6.2738" y1="1.143" x2="6.3754" y2="1.1938" layer="37"/>
<rectangle x1="7.493" y1="1.143" x2="7.5438" y2="1.1938" layer="37"/>
<rectangle x1="8.4582" y1="1.143" x2="8.5598" y2="1.1938" layer="37"/>
<rectangle x1="9.7282" y1="1.143" x2="9.8298" y2="1.1938" layer="37"/>
<rectangle x1="10.795" y1="1.143" x2="10.8458" y2="1.1938" layer="37"/>
<rectangle x1="5.6642" y1="1.143" x2="5.715" y2="1.1938" layer="37"/>
<rectangle x1="10.8458" y1="1.143" x2="10.8966" y2="1.1938" layer="37"/>
<rectangle x1="4.8514" y1="1.143" x2="4.9022" y2="1.1938" layer="37"/>
<rectangle x1="5.5626" y1="1.143" x2="5.6134" y2="1.1938" layer="37"/>
<rectangle x1="6.223" y1="1.143" x2="6.2738" y2="1.1938" layer="37"/>
<rectangle x1="3.3274" y1="1.143" x2="3.3782" y2="1.1938" layer="37"/>
<rectangle x1="3.9878" y1="1.143" x2="4.0386" y2="1.1938" layer="37"/>
<rectangle x1="5.715" y1="1.143" x2="5.7658" y2="1.1938" layer="37"/>
<rectangle x1="6.3754" y1="1.143" x2="6.4262" y2="1.1938" layer="37"/>
<rectangle x1="4.191" y1="1.143" x2="4.2418" y2="1.1938" layer="37"/>
<rectangle x1="7.5438" y1="1.143" x2="7.5946" y2="1.1938" layer="37"/>
<rectangle x1="3.2766" y1="1.1938" x2="3.3274" y2="1.2446" layer="37"/>
<rectangle x1="3.9878" y1="1.1938" x2="4.0386" y2="1.2446" layer="37"/>
<rectangle x1="4.1402" y1="1.1938" x2="4.191" y2="1.2446" layer="37"/>
<rectangle x1="4.9022" y1="1.1938" x2="4.953" y2="1.2446" layer="37"/>
<rectangle x1="5.5626" y1="1.1938" x2="5.6134" y2="1.2446" layer="37"/>
<rectangle x1="6.3754" y1="1.1938" x2="6.4262" y2="1.2446" layer="37"/>
<rectangle x1="7.493" y1="1.1938" x2="7.5438" y2="1.2446" layer="37"/>
<rectangle x1="8.4582" y1="1.1938" x2="8.5598" y2="1.2446" layer="37"/>
<rectangle x1="9.779" y1="1.1938" x2="9.8298" y2="1.2446" layer="37"/>
<rectangle x1="10.7442" y1="1.1938" x2="10.8458" y2="1.2446" layer="37"/>
<rectangle x1="6.3246" y1="1.1938" x2="6.3754" y2="1.2446" layer="37"/>
<rectangle x1="4.0386" y1="1.1938" x2="4.0894" y2="1.2446" layer="37"/>
<rectangle x1="4.191" y1="1.1938" x2="4.2418" y2="1.2446" layer="37"/>
<rectangle x1="5.6134" y1="1.1938" x2="5.6642" y2="1.2446" layer="37"/>
<rectangle x1="4.8514" y1="1.1938" x2="4.9022" y2="1.2446" layer="37"/>
<rectangle x1="3.3274" y1="1.1938" x2="3.3782" y2="1.2446" layer="37"/>
<rectangle x1="9.7282" y1="1.1938" x2="9.779" y2="1.2446" layer="37"/>
<rectangle x1="9.8298" y1="1.1938" x2="9.8806" y2="1.2446" layer="37"/>
<rectangle x1="5.5118" y1="1.1938" x2="5.5626" y2="1.2446" layer="37"/>
<rectangle x1="6.4262" y1="1.1938" x2="6.477" y2="1.2446" layer="37"/>
<rectangle x1="7.5438" y1="1.1938" x2="7.5946" y2="1.2446" layer="37"/>
<rectangle x1="3.2766" y1="1.2446" x2="3.3274" y2="1.2954" layer="37"/>
<rectangle x1="3.9878" y1="1.2446" x2="4.0386" y2="1.2954" layer="37"/>
<rectangle x1="4.191" y1="1.2446" x2="4.2418" y2="1.2954" layer="37"/>
<rectangle x1="4.9022" y1="1.2446" x2="4.953" y2="1.2954" layer="37"/>
<rectangle x1="5.5118" y1="1.2446" x2="5.5626" y2="1.2954" layer="37"/>
<rectangle x1="6.3754" y1="1.2446" x2="6.477" y2="1.2954" layer="37"/>
<rectangle x1="7.493" y1="1.2446" x2="7.5438" y2="1.2954" layer="37"/>
<rectangle x1="8.4582" y1="1.2446" x2="8.5598" y2="1.2954" layer="37"/>
<rectangle x1="9.8298" y1="1.2446" x2="9.8806" y2="1.2954" layer="37"/>
<rectangle x1="10.7442" y1="1.2446" x2="10.795" y2="1.2954" layer="37"/>
<rectangle x1="5.5626" y1="1.2446" x2="5.6134" y2="1.2954" layer="37"/>
<rectangle x1="9.779" y1="1.2446" x2="9.8298" y2="1.2954" layer="37"/>
<rectangle x1="10.6934" y1="1.2446" x2="10.7442" y2="1.2954" layer="37"/>
<rectangle x1="4.8514" y1="1.2446" x2="4.9022" y2="1.2954" layer="37"/>
<rectangle x1="3.3274" y1="1.2446" x2="3.3782" y2="1.2954" layer="37"/>
<rectangle x1="4.1402" y1="1.2446" x2="4.191" y2="1.2954" layer="37"/>
<rectangle x1="3.937" y1="1.2446" x2="3.9878" y2="1.2954" layer="37"/>
<rectangle x1="4.0386" y1="1.2446" x2="4.0894" y2="1.2954" layer="37"/>
<rectangle x1="10.795" y1="1.2446" x2="10.8458" y2="1.2954" layer="37"/>
<rectangle x1="7.5438" y1="1.2446" x2="7.5946" y2="1.2954" layer="37"/>
<rectangle x1="3.2766" y1="1.2954" x2="3.3274" y2="1.3462" layer="37"/>
<rectangle x1="4.191" y1="1.2954" x2="4.2418" y2="1.3462" layer="37"/>
<rectangle x1="4.9022" y1="1.2954" x2="4.953" y2="1.3462" layer="37"/>
<rectangle x1="5.5118" y1="1.2954" x2="5.5626" y2="1.3462" layer="37"/>
<rectangle x1="6.4262" y1="1.2954" x2="6.477" y2="1.3462" layer="37"/>
<rectangle x1="7.493" y1="1.2954" x2="7.5438" y2="1.3462" layer="37"/>
<rectangle x1="8.4582" y1="1.2954" x2="8.5598" y2="1.3462" layer="37"/>
<rectangle x1="9.8298" y1="1.2954" x2="9.9314" y2="1.3462" layer="37"/>
<rectangle x1="10.6934" y1="1.2954" x2="10.7442" y2="1.3462" layer="37"/>
<rectangle x1="3.937" y1="1.2954" x2="4.0386" y2="1.3462" layer="37"/>
<rectangle x1="4.2418" y1="1.2954" x2="4.2926" y2="1.3462" layer="37"/>
<rectangle x1="5.461" y1="1.2954" x2="5.5118" y2="1.3462" layer="37"/>
<rectangle x1="10.7442" y1="1.2954" x2="10.795" y2="1.3462" layer="37"/>
<rectangle x1="4.8514" y1="1.2954" x2="4.9022" y2="1.3462" layer="37"/>
<rectangle x1="6.477" y1="1.2954" x2="6.5278" y2="1.3462" layer="37"/>
<rectangle x1="3.3274" y1="1.2954" x2="3.3782" y2="1.3462" layer="37"/>
<rectangle x1="7.5438" y1="1.2954" x2="7.5946" y2="1.3462" layer="37"/>
<rectangle x1="6.3754" y1="1.2954" x2="6.4262" y2="1.3462" layer="37"/>
<rectangle x1="3.2766" y1="1.3462" x2="3.3274" y2="1.397" layer="37"/>
<rectangle x1="3.937" y1="1.3462" x2="3.9878" y2="1.397" layer="37"/>
<rectangle x1="4.2418" y1="1.3462" x2="4.2926" y2="1.397" layer="37"/>
<rectangle x1="4.9022" y1="1.3462" x2="4.953" y2="1.397" layer="37"/>
<rectangle x1="5.461" y1="1.3462" x2="5.5118" y2="1.397" layer="37"/>
<rectangle x1="6.477" y1="1.3462" x2="6.5278" y2="1.397" layer="37"/>
<rectangle x1="7.493" y1="1.3462" x2="7.5438" y2="1.397" layer="37"/>
<rectangle x1="8.4582" y1="1.3462" x2="8.5598" y2="1.397" layer="37"/>
<rectangle x1="9.8806" y1="1.3462" x2="9.9314" y2="1.397" layer="37"/>
<rectangle x1="10.6426" y1="1.3462" x2="10.6934" y2="1.397" layer="37"/>
<rectangle x1="6.4262" y1="1.3462" x2="6.477" y2="1.397" layer="37"/>
<rectangle x1="10.6934" y1="1.3462" x2="10.7442" y2="1.397" layer="37"/>
<rectangle x1="5.5118" y1="1.3462" x2="5.5626" y2="1.397" layer="37"/>
<rectangle x1="9.9314" y1="1.3462" x2="9.9822" y2="1.397" layer="37"/>
<rectangle x1="4.8514" y1="1.3462" x2="4.9022" y2="1.397" layer="37"/>
<rectangle x1="3.3274" y1="1.3462" x2="3.3782" y2="1.397" layer="37"/>
<rectangle x1="3.9878" y1="1.3462" x2="4.0386" y2="1.397" layer="37"/>
<rectangle x1="4.191" y1="1.3462" x2="4.2418" y2="1.397" layer="37"/>
<rectangle x1="9.8298" y1="1.3462" x2="9.8806" y2="1.397" layer="37"/>
<rectangle x1="7.5438" y1="1.3462" x2="7.5946" y2="1.397" layer="37"/>
<rectangle x1="3.2766" y1="1.397" x2="3.3274" y2="1.4478" layer="37"/>
<rectangle x1="4.2418" y1="1.397" x2="4.2926" y2="1.4478" layer="37"/>
<rectangle x1="4.9022" y1="1.397" x2="4.953" y2="1.4478" layer="37"/>
<rectangle x1="5.461" y1="1.397" x2="5.5118" y2="1.4478" layer="37"/>
<rectangle x1="6.477" y1="1.397" x2="6.5278" y2="1.4478" layer="37"/>
<rectangle x1="7.493" y1="1.397" x2="7.5438" y2="1.4478" layer="37"/>
<rectangle x1="8.4582" y1="1.397" x2="8.5598" y2="1.4478" layer="37"/>
<rectangle x1="9.9314" y1="1.397" x2="9.9822" y2="1.4478" layer="37"/>
<rectangle x1="10.6426" y1="1.397" x2="10.6934" y2="1.4478" layer="37"/>
<rectangle x1="3.937" y1="1.397" x2="3.9878" y2="1.4478" layer="37"/>
<rectangle x1="3.8862" y1="1.397" x2="3.937" y2="1.4478" layer="37"/>
<rectangle x1="10.5918" y1="1.397" x2="10.6426" y2="1.4478" layer="37"/>
<rectangle x1="4.2926" y1="1.397" x2="4.3434" y2="1.4478" layer="37"/>
<rectangle x1="9.8806" y1="1.397" x2="9.9314" y2="1.4478" layer="37"/>
<rectangle x1="4.8514" y1="1.397" x2="4.9022" y2="1.4478" layer="37"/>
<rectangle x1="3.3274" y1="1.397" x2="3.3782" y2="1.4478" layer="37"/>
<rectangle x1="5.4102" y1="1.397" x2="5.461" y2="1.4478" layer="37"/>
<rectangle x1="7.5438" y1="1.397" x2="7.5946" y2="1.4478" layer="37"/>
<rectangle x1="6.4262" y1="1.397" x2="6.477" y2="1.4478" layer="37"/>
<rectangle x1="3.2766" y1="1.4478" x2="3.3274" y2="1.4986" layer="37"/>
<rectangle x1="3.8862" y1="1.4478" x2="3.937" y2="1.4986" layer="37"/>
<rectangle x1="4.2926" y1="1.4478" x2="4.3434" y2="1.4986" layer="37"/>
<rectangle x1="4.9022" y1="1.4478" x2="4.953" y2="1.4986" layer="37"/>
<rectangle x1="5.4102" y1="1.4478" x2="5.5118" y2="1.4986" layer="37"/>
<rectangle x1="6.477" y1="1.4478" x2="6.5278" y2="1.4986" layer="37"/>
<rectangle x1="7.493" y1="1.4478" x2="7.5438" y2="1.4986" layer="37"/>
<rectangle x1="8.4582" y1="1.4478" x2="8.5598" y2="1.4986" layer="37"/>
<rectangle x1="9.9822" y1="1.4478" x2="10.033" y2="1.4986" layer="37"/>
<rectangle x1="10.5918" y1="1.4478" x2="10.6426" y2="1.4986" layer="37"/>
<rectangle x1="9.9314" y1="1.4478" x2="9.9822" y2="1.4986" layer="37"/>
<rectangle x1="3.937" y1="1.4478" x2="3.9878" y2="1.4986" layer="37"/>
<rectangle x1="4.2418" y1="1.4478" x2="4.2926" y2="1.4986" layer="37"/>
<rectangle x1="6.5278" y1="1.4478" x2="6.5786" y2="1.4986" layer="37"/>
<rectangle x1="4.8514" y1="1.4478" x2="4.9022" y2="1.4986" layer="37"/>
<rectangle x1="10.6426" y1="1.4478" x2="10.6934" y2="1.4986" layer="37"/>
<rectangle x1="3.3274" y1="1.4478" x2="3.3782" y2="1.4986" layer="37"/>
<rectangle x1="10.541" y1="1.4478" x2="10.5918" y2="1.4986" layer="37"/>
<rectangle x1="7.5438" y1="1.4478" x2="7.5946" y2="1.4986" layer="37"/>
<rectangle x1="3.2766" y1="1.4986" x2="3.3274" y2="1.5494" layer="37"/>
<rectangle x1="3.8862" y1="1.4986" x2="3.937" y2="1.5494" layer="37"/>
<rectangle x1="4.2926" y1="1.4986" x2="4.3434" y2="1.5494" layer="37"/>
<rectangle x1="4.9022" y1="1.4986" x2="4.953" y2="1.5494" layer="37"/>
<rectangle x1="5.4102" y1="1.4986" x2="5.5118" y2="1.5494" layer="37"/>
<rectangle x1="6.477" y1="1.4986" x2="6.5786" y2="1.5494" layer="37"/>
<rectangle x1="7.493" y1="1.4986" x2="7.5438" y2="1.5494" layer="37"/>
<rectangle x1="8.4582" y1="1.4986" x2="8.5598" y2="1.5494" layer="37"/>
<rectangle x1="9.9822" y1="1.4986" x2="10.033" y2="1.5494" layer="37"/>
<rectangle x1="10.541" y1="1.4986" x2="10.5918" y2="1.5494" layer="37"/>
<rectangle x1="10.033" y1="1.4986" x2="10.0838" y2="1.5494" layer="37"/>
<rectangle x1="10.5918" y1="1.4986" x2="10.6426" y2="1.5494" layer="37"/>
<rectangle x1="3.8354" y1="1.4986" x2="3.8862" y2="1.5494" layer="37"/>
<rectangle x1="4.3434" y1="1.4986" x2="4.3942" y2="1.5494" layer="37"/>
<rectangle x1="4.8514" y1="1.4986" x2="4.9022" y2="1.5494" layer="37"/>
<rectangle x1="3.3274" y1="1.4986" x2="3.3782" y2="1.5494" layer="37"/>
<rectangle x1="7.5438" y1="1.4986" x2="7.5946" y2="1.5494" layer="37"/>
<rectangle x1="3.2766" y1="1.5494" x2="3.3274" y2="1.6002" layer="37"/>
<rectangle x1="3.8354" y1="1.5494" x2="3.8862" y2="1.6002" layer="37"/>
<rectangle x1="4.3434" y1="1.5494" x2="4.3942" y2="1.6002" layer="37"/>
<rectangle x1="4.9022" y1="1.5494" x2="4.953" y2="1.6002" layer="37"/>
<rectangle x1="5.4102" y1="1.5494" x2="5.461" y2="1.6002" layer="37"/>
<rectangle x1="6.477" y1="1.5494" x2="6.5786" y2="1.6002" layer="37"/>
<rectangle x1="7.493" y1="1.5494" x2="7.5438" y2="1.6002" layer="37"/>
<rectangle x1="8.4582" y1="1.5494" x2="8.5598" y2="1.6002" layer="37"/>
<rectangle x1="10.033" y1="1.5494" x2="10.0838" y2="1.6002" layer="37"/>
<rectangle x1="10.4902" y1="1.5494" x2="10.5918" y2="1.6002" layer="37"/>
<rectangle x1="3.8862" y1="1.5494" x2="3.937" y2="1.6002" layer="37"/>
<rectangle x1="4.2926" y1="1.5494" x2="4.3434" y2="1.6002" layer="37"/>
<rectangle x1="5.461" y1="1.5494" x2="5.5118" y2="1.6002" layer="37"/>
<rectangle x1="9.9822" y1="1.5494" x2="10.033" y2="1.6002" layer="37"/>
<rectangle x1="4.8514" y1="1.5494" x2="4.9022" y2="1.6002" layer="37"/>
<rectangle x1="3.3274" y1="1.5494" x2="3.3782" y2="1.6002" layer="37"/>
<rectangle x1="10.0838" y1="1.5494" x2="10.1346" y2="1.6002" layer="37"/>
<rectangle x1="7.5438" y1="1.5494" x2="7.5946" y2="1.6002" layer="37"/>
<rectangle x1="3.2766" y1="1.6002" x2="3.3274" y2="1.651" layer="37"/>
<rectangle x1="3.8354" y1="1.6002" x2="3.8862" y2="1.651" layer="37"/>
<rectangle x1="4.3434" y1="1.6002" x2="4.3942" y2="1.651" layer="37"/>
<rectangle x1="4.9022" y1="1.6002" x2="4.953" y2="1.651" layer="37"/>
<rectangle x1="5.4102" y1="1.6002" x2="5.461" y2="1.651" layer="37"/>
<rectangle x1="6.477" y1="1.6002" x2="6.5786" y2="1.651" layer="37"/>
<rectangle x1="7.493" y1="1.6002" x2="7.5438" y2="1.651" layer="37"/>
<rectangle x1="8.4582" y1="1.6002" x2="8.5598" y2="1.651" layer="37"/>
<rectangle x1="10.0838" y1="1.6002" x2="10.1346" y2="1.651" layer="37"/>
<rectangle x1="10.4902" y1="1.6002" x2="10.541" y2="1.651" layer="37"/>
<rectangle x1="10.033" y1="1.6002" x2="10.0838" y2="1.651" layer="37"/>
<rectangle x1="3.7846" y1="1.6002" x2="3.8354" y2="1.651" layer="37"/>
<rectangle x1="4.8514" y1="1.6002" x2="4.9022" y2="1.651" layer="37"/>
<rectangle x1="10.4394" y1="1.6002" x2="10.4902" y2="1.651" layer="37"/>
<rectangle x1="4.3942" y1="1.6002" x2="4.445" y2="1.651" layer="37"/>
<rectangle x1="3.3274" y1="1.6002" x2="3.3782" y2="1.651" layer="37"/>
<rectangle x1="5.461" y1="1.6002" x2="5.5118" y2="1.651" layer="37"/>
<rectangle x1="10.541" y1="1.6002" x2="10.5918" y2="1.651" layer="37"/>
<rectangle x1="7.5438" y1="1.6002" x2="7.5946" y2="1.651" layer="37"/>
<rectangle x1="3.2766" y1="1.651" x2="3.3274" y2="1.7018" layer="37"/>
<rectangle x1="3.7846" y1="1.651" x2="3.8862" y2="1.7018" layer="37"/>
<rectangle x1="4.3942" y1="1.651" x2="4.445" y2="1.7018" layer="37"/>
<rectangle x1="4.9022" y1="1.651" x2="4.953" y2="1.7018" layer="37"/>
<rectangle x1="5.4102" y1="1.651" x2="5.461" y2="1.7018" layer="37"/>
<rectangle x1="6.477" y1="1.651" x2="6.5786" y2="1.7018" layer="37"/>
<rectangle x1="7.493" y1="1.651" x2="7.5438" y2="1.7018" layer="37"/>
<rectangle x1="8.4582" y1="1.651" x2="8.5598" y2="1.7018" layer="37"/>
<rectangle x1="10.0838" y1="1.651" x2="10.1854" y2="1.7018" layer="37"/>
<rectangle x1="10.4394" y1="1.651" x2="10.4902" y2="1.7018" layer="37"/>
<rectangle x1="4.3434" y1="1.651" x2="4.3942" y2="1.7018" layer="37"/>
<rectangle x1="10.4902" y1="1.651" x2="10.541" y2="1.7018" layer="37"/>
<rectangle x1="4.8514" y1="1.651" x2="4.9022" y2="1.7018" layer="37"/>
<rectangle x1="3.3274" y1="1.651" x2="3.3782" y2="1.7018" layer="37"/>
<rectangle x1="5.461" y1="1.651" x2="5.5118" y2="1.7018" layer="37"/>
<rectangle x1="7.5438" y1="1.651" x2="7.5946" y2="1.7018" layer="37"/>
<rectangle x1="3.2766" y1="1.7018" x2="3.3274" y2="1.7526" layer="37"/>
<rectangle x1="3.7846" y1="1.7018" x2="3.8354" y2="1.7526" layer="37"/>
<rectangle x1="4.3942" y1="1.7018" x2="4.445" y2="1.7526" layer="37"/>
<rectangle x1="4.9022" y1="1.7018" x2="4.953" y2="1.7526" layer="37"/>
<rectangle x1="5.4102" y1="1.7018" x2="5.461" y2="1.7526" layer="37"/>
<rectangle x1="6.477" y1="1.7018" x2="6.5786" y2="1.7526" layer="37"/>
<rectangle x1="7.493" y1="1.7018" x2="7.5438" y2="1.7526" layer="37"/>
<rectangle x1="8.4582" y1="1.7018" x2="8.5598" y2="1.7526" layer="37"/>
<rectangle x1="10.1346" y1="1.7018" x2="10.1854" y2="1.7526" layer="37"/>
<rectangle x1="10.3886" y1="1.7018" x2="10.4902" y2="1.7526" layer="37"/>
<rectangle x1="4.8514" y1="1.7018" x2="4.9022" y2="1.7526" layer="37"/>
<rectangle x1="10.1854" y1="1.7018" x2="10.2362" y2="1.7526" layer="37"/>
<rectangle x1="10.0838" y1="1.7018" x2="10.1346" y2="1.7526" layer="37"/>
<rectangle x1="3.3274" y1="1.7018" x2="3.3782" y2="1.7526" layer="37"/>
<rectangle x1="3.7338" y1="1.7018" x2="3.7846" y2="1.7526" layer="37"/>
<rectangle x1="5.461" y1="1.7018" x2="5.5118" y2="1.7526" layer="37"/>
<rectangle x1="4.445" y1="1.7018" x2="4.4958" y2="1.7526" layer="37"/>
<rectangle x1="7.5438" y1="1.7018" x2="7.5946" y2="1.7526" layer="37"/>
<rectangle x1="3.2766" y1="1.7526" x2="3.3274" y2="1.8034" layer="37"/>
<rectangle x1="3.7846" y1="1.7526" x2="3.8354" y2="1.8034" layer="37"/>
<rectangle x1="4.3942" y1="1.7526" x2="4.4958" y2="1.8034" layer="37"/>
<rectangle x1="4.9022" y1="1.7526" x2="4.953" y2="1.8034" layer="37"/>
<rectangle x1="5.4102" y1="1.7526" x2="5.461" y2="1.8034" layer="37"/>
<rectangle x1="6.477" y1="1.7526" x2="6.5786" y2="1.8034" layer="37"/>
<rectangle x1="7.493" y1="1.7526" x2="7.5438" y2="1.8034" layer="37"/>
<rectangle x1="8.4582" y1="1.7526" x2="8.5598" y2="1.8034" layer="37"/>
<rectangle x1="10.1854" y1="1.7526" x2="10.2362" y2="1.8034" layer="37"/>
<rectangle x1="10.3886" y1="1.7526" x2="10.4394" y2="1.8034" layer="37"/>
<rectangle x1="3.7338" y1="1.7526" x2="3.7846" y2="1.8034" layer="37"/>
<rectangle x1="10.1346" y1="1.7526" x2="10.1854" y2="1.8034" layer="37"/>
<rectangle x1="10.3378" y1="1.7526" x2="10.3886" y2="1.8034" layer="37"/>
<rectangle x1="4.8514" y1="1.7526" x2="4.9022" y2="1.8034" layer="37"/>
<rectangle x1="3.3274" y1="1.7526" x2="3.3782" y2="1.8034" layer="37"/>
<rectangle x1="5.461" y1="1.7526" x2="5.5118" y2="1.8034" layer="37"/>
<rectangle x1="7.5438" y1="1.7526" x2="7.5946" y2="1.8034" layer="37"/>
<rectangle x1="10.4394" y1="1.7526" x2="10.4902" y2="1.8034" layer="37"/>
<rectangle x1="3.2766" y1="1.8034" x2="3.3274" y2="1.8542" layer="37"/>
<rectangle x1="3.7338" y1="1.8034" x2="3.7846" y2="1.8542" layer="37"/>
<rectangle x1="4.445" y1="1.8034" x2="4.4958" y2="1.8542" layer="37"/>
<rectangle x1="4.9022" y1="1.8034" x2="4.953" y2="1.8542" layer="37"/>
<rectangle x1="5.4102" y1="1.8034" x2="5.461" y2="1.8542" layer="37"/>
<rectangle x1="6.477" y1="1.8034" x2="6.5786" y2="1.8542" layer="37"/>
<rectangle x1="7.493" y1="1.8034" x2="7.5438" y2="1.8542" layer="37"/>
<rectangle x1="8.4582" y1="1.8034" x2="8.5598" y2="1.8542" layer="37"/>
<rectangle x1="10.1854" y1="1.8034" x2="10.2362" y2="1.8542" layer="37"/>
<rectangle x1="10.3378" y1="1.8034" x2="10.3886" y2="1.8542" layer="37"/>
<rectangle x1="10.2362" y1="1.8034" x2="10.287" y2="1.8542" layer="37"/>
<rectangle x1="10.287" y1="1.8034" x2="10.3378" y2="1.8542" layer="37"/>
<rectangle x1="10.3886" y1="1.8034" x2="10.4394" y2="1.8542" layer="37"/>
<rectangle x1="4.8514" y1="1.8034" x2="4.9022" y2="1.8542" layer="37"/>
<rectangle x1="3.3274" y1="1.8034" x2="3.3782" y2="1.8542" layer="37"/>
<rectangle x1="5.461" y1="1.8034" x2="5.5118" y2="1.8542" layer="37"/>
<rectangle x1="3.683" y1="1.8034" x2="3.7338" y2="1.8542" layer="37"/>
<rectangle x1="4.4958" y1="1.8034" x2="4.5466" y2="1.8542" layer="37"/>
<rectangle x1="7.5438" y1="1.8034" x2="7.5946" y2="1.8542" layer="37"/>
<rectangle x1="3.2766" y1="1.8542" x2="3.3274" y2="1.905" layer="37"/>
<rectangle x1="3.7338" y1="1.8542" x2="3.7846" y2="1.905" layer="37"/>
<rectangle x1="4.445" y1="1.8542" x2="4.5466" y2="1.905" layer="37"/>
<rectangle x1="4.9022" y1="1.8542" x2="4.953" y2="1.905" layer="37"/>
<rectangle x1="5.4102" y1="1.8542" x2="5.461" y2="1.905" layer="37"/>
<rectangle x1="6.477" y1="1.8542" x2="6.5786" y2="1.905" layer="37"/>
<rectangle x1="7.493" y1="1.8542" x2="7.5438" y2="1.905" layer="37"/>
<rectangle x1="8.4582" y1="1.8542" x2="8.5598" y2="1.905" layer="37"/>
<rectangle x1="10.2362" y1="1.8542" x2="10.3378" y2="1.905" layer="37"/>
<rectangle x1="3.683" y1="1.8542" x2="3.7338" y2="1.905" layer="37"/>
<rectangle x1="10.3378" y1="1.8542" x2="10.3886" y2="1.905" layer="37"/>
<rectangle x1="8.5598" y1="1.8542" x2="8.6106" y2="1.905" layer="37"/>
<rectangle x1="8.6106" y1="1.8542" x2="9.1694" y2="1.905" layer="37"/>
<rectangle x1="4.8514" y1="1.8542" x2="4.9022" y2="1.905" layer="37"/>
<rectangle x1="3.3274" y1="1.8542" x2="3.3782" y2="1.905" layer="37"/>
<rectangle x1="5.461" y1="1.8542" x2="5.5118" y2="1.905" layer="37"/>
<rectangle x1="10.1854" y1="1.8542" x2="10.2362" y2="1.905" layer="37"/>
<rectangle x1="7.5438" y1="1.8542" x2="7.5946" y2="1.905" layer="37"/>
<rectangle x1="3.2766" y1="1.905" x2="3.3274" y2="1.9558" layer="37"/>
<rectangle x1="3.683" y1="1.905" x2="3.7338" y2="1.9558" layer="37"/>
<rectangle x1="4.4958" y1="1.905" x2="4.5466" y2="1.9558" layer="37"/>
<rectangle x1="4.9022" y1="1.905" x2="4.953" y2="1.9558" layer="37"/>
<rectangle x1="5.4102" y1="1.905" x2="5.461" y2="1.9558" layer="37"/>
<rectangle x1="6.477" y1="1.905" x2="6.5786" y2="1.9558" layer="37"/>
<rectangle x1="7.493" y1="1.905" x2="7.5438" y2="1.9558" layer="37"/>
<rectangle x1="8.4582" y1="1.905" x2="9.1694" y2="1.9558" layer="37"/>
<rectangle x1="10.2362" y1="1.905" x2="10.3378" y2="1.9558" layer="37"/>
<rectangle x1="10.3378" y1="1.905" x2="10.3886" y2="1.9558" layer="37"/>
<rectangle x1="4.8514" y1="1.905" x2="4.9022" y2="1.9558" layer="37"/>
<rectangle x1="9.1694" y1="1.905" x2="9.2202" y2="1.9558" layer="37"/>
<rectangle x1="3.3274" y1="1.905" x2="3.3782" y2="1.9558" layer="37"/>
<rectangle x1="5.461" y1="1.905" x2="5.5118" y2="1.9558" layer="37"/>
<rectangle x1="3.6322" y1="1.905" x2="3.683" y2="1.9558" layer="37"/>
<rectangle x1="7.5438" y1="1.905" x2="7.5946" y2="1.9558" layer="37"/>
<rectangle x1="3.7338" y1="1.905" x2="3.7846" y2="1.9558" layer="37"/>
<rectangle x1="4.445" y1="1.905" x2="4.4958" y2="1.9558" layer="37"/>
<rectangle x1="4.5466" y1="1.905" x2="4.5974" y2="1.9558" layer="37"/>
<rectangle x1="3.2766" y1="1.9558" x2="3.3274" y2="2.0066" layer="37"/>
<rectangle x1="3.683" y1="1.9558" x2="3.7338" y2="2.0066" layer="37"/>
<rectangle x1="4.4958" y1="1.9558" x2="4.5466" y2="2.0066" layer="37"/>
<rectangle x1="4.9022" y1="1.9558" x2="4.953" y2="2.0066" layer="37"/>
<rectangle x1="5.4102" y1="1.9558" x2="5.461" y2="2.0066" layer="37"/>
<rectangle x1="6.477" y1="1.9558" x2="6.5786" y2="2.0066" layer="37"/>
<rectangle x1="7.493" y1="1.9558" x2="7.5438" y2="2.0066" layer="37"/>
<rectangle x1="8.4582" y1="1.9558" x2="8.5598" y2="2.0066" layer="37"/>
<rectangle x1="10.2362" y1="1.9558" x2="10.3886" y2="2.0066" layer="37"/>
<rectangle x1="3.6322" y1="1.9558" x2="3.683" y2="2.0066" layer="37"/>
<rectangle x1="4.5466" y1="1.9558" x2="4.5974" y2="2.0066" layer="37"/>
<rectangle x1="10.1854" y1="1.9558" x2="10.2362" y2="2.0066" layer="37"/>
<rectangle x1="4.8514" y1="1.9558" x2="4.9022" y2="2.0066" layer="37"/>
<rectangle x1="3.3274" y1="1.9558" x2="3.3782" y2="2.0066" layer="37"/>
<rectangle x1="5.461" y1="1.9558" x2="5.5118" y2="2.0066" layer="37"/>
<rectangle x1="7.5438" y1="1.9558" x2="7.5946" y2="2.0066" layer="37"/>
<rectangle x1="3.2766" y1="2.0066" x2="3.3274" y2="2.0574" layer="37"/>
<rectangle x1="3.6322" y1="2.0066" x2="3.683" y2="2.0574" layer="37"/>
<rectangle x1="4.5466" y1="2.0066" x2="4.5974" y2="2.0574" layer="37"/>
<rectangle x1="4.9022" y1="2.0066" x2="4.953" y2="2.0574" layer="37"/>
<rectangle x1="5.4102" y1="2.0066" x2="5.461" y2="2.0574" layer="37"/>
<rectangle x1="6.477" y1="2.0066" x2="6.5786" y2="2.0574" layer="37"/>
<rectangle x1="7.493" y1="2.0066" x2="7.5438" y2="2.0574" layer="37"/>
<rectangle x1="8.4582" y1="2.0066" x2="8.5598" y2="2.0574" layer="37"/>
<rectangle x1="10.3378" y1="2.0066" x2="10.3886" y2="2.0574" layer="37"/>
<rectangle x1="10.1854" y1="2.0066" x2="10.287" y2="2.0574" layer="37"/>
<rectangle x1="10.3886" y1="2.0066" x2="10.4394" y2="2.0574" layer="37"/>
<rectangle x1="4.8514" y1="2.0066" x2="4.9022" y2="2.0574" layer="37"/>
<rectangle x1="3.3274" y1="2.0066" x2="3.3782" y2="2.0574" layer="37"/>
<rectangle x1="5.461" y1="2.0066" x2="5.5118" y2="2.0574" layer="37"/>
<rectangle x1="3.683" y1="2.0066" x2="3.7338" y2="2.0574" layer="37"/>
<rectangle x1="4.4958" y1="2.0066" x2="4.5466" y2="2.0574" layer="37"/>
<rectangle x1="10.287" y1="2.0066" x2="10.3378" y2="2.0574" layer="37"/>
<rectangle x1="7.5438" y1="2.0066" x2="7.5946" y2="2.0574" layer="37"/>
<rectangle x1="3.2766" y1="2.0574" x2="3.3274" y2="2.1082" layer="37"/>
<rectangle x1="3.6322" y1="2.0574" x2="3.683" y2="2.1082" layer="37"/>
<rectangle x1="4.5466" y1="2.0574" x2="4.5974" y2="2.1082" layer="37"/>
<rectangle x1="4.9022" y1="2.0574" x2="4.953" y2="2.1082" layer="37"/>
<rectangle x1="5.4102" y1="2.0574" x2="5.461" y2="2.1082" layer="37"/>
<rectangle x1="6.477" y1="2.0574" x2="6.5786" y2="2.1082" layer="37"/>
<rectangle x1="7.493" y1="2.0574" x2="7.5438" y2="2.1082" layer="37"/>
<rectangle x1="8.4582" y1="2.0574" x2="8.5598" y2="2.1082" layer="37"/>
<rectangle x1="10.1854" y1="2.0574" x2="10.2362" y2="2.1082" layer="37"/>
<rectangle x1="10.3886" y1="2.0574" x2="10.4394" y2="2.1082" layer="37"/>
<rectangle x1="3.5814" y1="2.0574" x2="3.6322" y2="2.1082" layer="37"/>
<rectangle x1="4.5974" y1="2.0574" x2="4.6482" y2="2.1082" layer="37"/>
<rectangle x1="10.1346" y1="2.0574" x2="10.1854" y2="2.1082" layer="37"/>
<rectangle x1="10.4394" y1="2.0574" x2="10.4902" y2="2.1082" layer="37"/>
<rectangle x1="4.8514" y1="2.0574" x2="4.9022" y2="2.1082" layer="37"/>
<rectangle x1="3.3274" y1="2.0574" x2="3.3782" y2="2.1082" layer="37"/>
<rectangle x1="5.461" y1="2.0574" x2="5.5118" y2="2.1082" layer="37"/>
<rectangle x1="10.3378" y1="2.0574" x2="10.3886" y2="2.1082" layer="37"/>
<rectangle x1="7.5438" y1="2.0574" x2="7.5946" y2="2.1082" layer="37"/>
<rectangle x1="3.2766" y1="2.1082" x2="3.3274" y2="2.159" layer="37"/>
<rectangle x1="3.5814" y1="2.1082" x2="3.6322" y2="2.159" layer="37"/>
<rectangle x1="4.5974" y1="2.1082" x2="4.6482" y2="2.159" layer="37"/>
<rectangle x1="4.9022" y1="2.1082" x2="4.953" y2="2.159" layer="37"/>
<rectangle x1="5.4102" y1="2.1082" x2="5.461" y2="2.159" layer="37"/>
<rectangle x1="6.477" y1="2.1082" x2="6.5786" y2="2.159" layer="37"/>
<rectangle x1="7.493" y1="2.1082" x2="7.5438" y2="2.159" layer="37"/>
<rectangle x1="8.4582" y1="2.1082" x2="8.5598" y2="2.159" layer="37"/>
<rectangle x1="10.1346" y1="2.1082" x2="10.1854" y2="2.159" layer="37"/>
<rectangle x1="10.4394" y1="2.1082" x2="10.4902" y2="2.159" layer="37"/>
<rectangle x1="10.3886" y1="2.1082" x2="10.4394" y2="2.159" layer="37"/>
<rectangle x1="10.0838" y1="2.1082" x2="10.1346" y2="2.159" layer="37"/>
<rectangle x1="10.1854" y1="2.1082" x2="10.2362" y2="2.159" layer="37"/>
<rectangle x1="4.8514" y1="2.1082" x2="4.9022" y2="2.159" layer="37"/>
<rectangle x1="3.6322" y1="2.1082" x2="3.683" y2="2.159" layer="37"/>
<rectangle x1="3.3274" y1="2.1082" x2="3.3782" y2="2.159" layer="37"/>
<rectangle x1="4.5466" y1="2.1082" x2="4.5974" y2="2.159" layer="37"/>
<rectangle x1="5.461" y1="2.1082" x2="5.5118" y2="2.159" layer="37"/>
<rectangle x1="10.4902" y1="2.1082" x2="10.541" y2="2.159" layer="37"/>
<rectangle x1="7.5438" y1="2.1082" x2="7.5946" y2="2.159" layer="37"/>
<rectangle x1="3.2766" y1="2.159" x2="3.3274" y2="2.2098" layer="37"/>
<rectangle x1="3.5814" y1="2.159" x2="3.6322" y2="2.2098" layer="37"/>
<rectangle x1="4.5974" y1="2.159" x2="4.699" y2="2.2098" layer="37"/>
<rectangle x1="4.9022" y1="2.159" x2="4.953" y2="2.2098" layer="37"/>
<rectangle x1="5.4102" y1="2.159" x2="5.461" y2="2.2098" layer="37"/>
<rectangle x1="6.477" y1="2.159" x2="6.5786" y2="2.2098" layer="37"/>
<rectangle x1="7.493" y1="2.159" x2="7.5438" y2="2.2098" layer="37"/>
<rectangle x1="8.4582" y1="2.159" x2="8.5598" y2="2.2098" layer="37"/>
<rectangle x1="10.0838" y1="2.159" x2="10.1346" y2="2.2098" layer="37"/>
<rectangle x1="10.4902" y1="2.159" x2="10.541" y2="2.2098" layer="37"/>
<rectangle x1="10.1346" y1="2.159" x2="10.1854" y2="2.2098" layer="37"/>
<rectangle x1="10.4394" y1="2.159" x2="10.4902" y2="2.2098" layer="37"/>
<rectangle x1="3.5306" y1="2.159" x2="3.5814" y2="2.2098" layer="37"/>
<rectangle x1="4.8514" y1="2.159" x2="4.9022" y2="2.2098" layer="37"/>
<rectangle x1="3.3274" y1="2.159" x2="3.3782" y2="2.2098" layer="37"/>
<rectangle x1="5.461" y1="2.159" x2="5.5118" y2="2.2098" layer="37"/>
<rectangle x1="7.5438" y1="2.159" x2="7.5946" y2="2.2098" layer="37"/>
<rectangle x1="3.2766" y1="2.2098" x2="3.3274" y2="2.2606" layer="37"/>
<rectangle x1="3.5306" y1="2.2098" x2="3.5814" y2="2.2606" layer="37"/>
<rectangle x1="4.6482" y1="2.2098" x2="4.699" y2="2.2606" layer="37"/>
<rectangle x1="4.9022" y1="2.2098" x2="4.953" y2="2.2606" layer="37"/>
<rectangle x1="5.4102" y1="2.2098" x2="5.461" y2="2.2606" layer="37"/>
<rectangle x1="6.477" y1="2.2098" x2="6.5786" y2="2.2606" layer="37"/>
<rectangle x1="7.493" y1="2.2098" x2="7.5438" y2="2.2606" layer="37"/>
<rectangle x1="8.4582" y1="2.2098" x2="8.5598" y2="2.2606" layer="37"/>
<rectangle x1="10.0838" y1="2.2098" x2="10.1346" y2="2.2606" layer="37"/>
<rectangle x1="10.4902" y1="2.2098" x2="10.541" y2="2.2606" layer="37"/>
<rectangle x1="10.033" y1="2.2098" x2="10.0838" y2="2.2606" layer="37"/>
<rectangle x1="10.541" y1="2.2098" x2="10.5918" y2="2.2606" layer="37"/>
<rectangle x1="3.5814" y1="2.2098" x2="3.6322" y2="2.2606" layer="37"/>
<rectangle x1="4.5974" y1="2.2098" x2="4.6482" y2="2.2606" layer="37"/>
<rectangle x1="4.8514" y1="2.2098" x2="4.9022" y2="2.2606" layer="37"/>
<rectangle x1="3.3274" y1="2.2098" x2="3.3782" y2="2.2606" layer="37"/>
<rectangle x1="5.461" y1="2.2098" x2="5.5118" y2="2.2606" layer="37"/>
<rectangle x1="7.5438" y1="2.2098" x2="7.5946" y2="2.2606" layer="37"/>
<rectangle x1="3.2766" y1="2.2606" x2="3.3274" y2="2.3114" layer="37"/>
<rectangle x1="3.5306" y1="2.2606" x2="3.5814" y2="2.3114" layer="37"/>
<rectangle x1="4.6482" y1="2.2606" x2="4.699" y2="2.3114" layer="37"/>
<rectangle x1="4.9022" y1="2.2606" x2="4.953" y2="2.3114" layer="37"/>
<rectangle x1="5.4102" y1="2.2606" x2="5.461" y2="2.3114" layer="37"/>
<rectangle x1="6.477" y1="2.2606" x2="6.5786" y2="2.3114" layer="37"/>
<rectangle x1="7.493" y1="2.2606" x2="7.5438" y2="2.3114" layer="37"/>
<rectangle x1="8.4582" y1="2.2606" x2="8.5598" y2="2.3114" layer="37"/>
<rectangle x1="10.033" y1="2.2606" x2="10.0838" y2="2.3114" layer="37"/>
<rectangle x1="10.541" y1="2.2606" x2="10.5918" y2="2.3114" layer="37"/>
<rectangle x1="4.699" y1="2.2606" x2="4.7498" y2="2.3114" layer="37"/>
<rectangle x1="3.4798" y1="2.2606" x2="3.5306" y2="2.3114" layer="37"/>
<rectangle x1="9.9822" y1="2.2606" x2="10.033" y2="2.3114" layer="37"/>
<rectangle x1="10.4902" y1="2.2606" x2="10.541" y2="2.3114" layer="37"/>
<rectangle x1="4.8514" y1="2.2606" x2="4.9022" y2="2.3114" layer="37"/>
<rectangle x1="3.3274" y1="2.2606" x2="3.3782" y2="2.3114" layer="37"/>
<rectangle x1="5.461" y1="2.2606" x2="5.5118" y2="2.3114" layer="37"/>
<rectangle x1="10.0838" y1="2.2606" x2="10.1346" y2="2.3114" layer="37"/>
<rectangle x1="10.5918" y1="2.2606" x2="10.6426" y2="2.3114" layer="37"/>
<rectangle x1="7.5438" y1="2.2606" x2="7.5946" y2="2.3114" layer="37"/>
<rectangle x1="3.2766" y1="2.3114" x2="3.3274" y2="2.3622" layer="37"/>
<rectangle x1="3.4798" y1="2.3114" x2="3.5306" y2="2.3622" layer="37"/>
<rectangle x1="4.699" y1="2.3114" x2="4.7498" y2="2.3622" layer="37"/>
<rectangle x1="4.9022" y1="2.3114" x2="4.953" y2="2.3622" layer="37"/>
<rectangle x1="5.4102" y1="2.3114" x2="5.461" y2="2.3622" layer="37"/>
<rectangle x1="6.477" y1="2.3114" x2="6.5786" y2="2.3622" layer="37"/>
<rectangle x1="7.493" y1="2.3114" x2="7.5438" y2="2.3622" layer="37"/>
<rectangle x1="8.4582" y1="2.3114" x2="8.5598" y2="2.3622" layer="37"/>
<rectangle x1="9.9822" y1="2.3114" x2="10.033" y2="2.3622" layer="37"/>
<rectangle x1="10.5918" y1="2.3114" x2="10.6426" y2="2.3622" layer="37"/>
<rectangle x1="10.541" y1="2.3114" x2="10.5918" y2="2.3622" layer="37"/>
<rectangle x1="10.033" y1="2.3114" x2="10.0838" y2="2.3622" layer="37"/>
<rectangle x1="3.5306" y1="2.3114" x2="3.5814" y2="2.3622" layer="37"/>
<rectangle x1="4.6482" y1="2.3114" x2="4.699" y2="2.3622" layer="37"/>
<rectangle x1="4.8514" y1="2.3114" x2="4.9022" y2="2.3622" layer="37"/>
<rectangle x1="3.3274" y1="2.3114" x2="3.3782" y2="2.3622" layer="37"/>
<rectangle x1="5.461" y1="2.3114" x2="5.5118" y2="2.3622" layer="37"/>
<rectangle x1="7.5438" y1="2.3114" x2="7.5946" y2="2.3622" layer="37"/>
<rectangle x1="3.2766" y1="2.3622" x2="3.3274" y2="2.413" layer="37"/>
<rectangle x1="3.4798" y1="2.3622" x2="3.5306" y2="2.413" layer="37"/>
<rectangle x1="4.699" y1="2.3622" x2="4.7498" y2="2.413" layer="37"/>
<rectangle x1="4.9022" y1="2.3622" x2="4.953" y2="2.413" layer="37"/>
<rectangle x1="5.4102" y1="2.3622" x2="5.461" y2="2.413" layer="37"/>
<rectangle x1="6.477" y1="2.3622" x2="6.5786" y2="2.413" layer="37"/>
<rectangle x1="7.493" y1="2.3622" x2="7.5438" y2="2.413" layer="37"/>
<rectangle x1="8.4582" y1="2.3622" x2="8.5598" y2="2.413" layer="37"/>
<rectangle x1="9.9314" y1="2.3622" x2="10.033" y2="2.413" layer="37"/>
<rectangle x1="10.5918" y1="2.3622" x2="10.6426" y2="2.413" layer="37"/>
<rectangle x1="10.6426" y1="2.3622" x2="10.6934" y2="2.413" layer="37"/>
<rectangle x1="4.7498" y1="2.3622" x2="4.8006" y2="2.413" layer="37"/>
<rectangle x1="3.429" y1="2.3622" x2="3.4798" y2="2.413" layer="37"/>
<rectangle x1="4.8514" y1="2.3622" x2="4.9022" y2="2.413" layer="37"/>
<rectangle x1="3.3274" y1="2.3622" x2="3.3782" y2="2.413" layer="37"/>
<rectangle x1="5.461" y1="2.3622" x2="5.5118" y2="2.413" layer="37"/>
<rectangle x1="7.5438" y1="2.3622" x2="7.5946" y2="2.413" layer="37"/>
<rectangle x1="3.2766" y1="2.413" x2="3.3274" y2="2.4638" layer="37"/>
<rectangle x1="3.429" y1="2.413" x2="3.4798" y2="2.4638" layer="37"/>
<rectangle x1="4.7498" y1="2.413" x2="4.8006" y2="2.4638" layer="37"/>
<rectangle x1="4.9022" y1="2.413" x2="4.953" y2="2.4638" layer="37"/>
<rectangle x1="5.4102" y1="2.413" x2="5.461" y2="2.4638" layer="37"/>
<rectangle x1="6.477" y1="2.413" x2="6.5786" y2="2.4638" layer="37"/>
<rectangle x1="7.493" y1="2.413" x2="7.5438" y2="2.4638" layer="37"/>
<rectangle x1="8.4582" y1="2.413" x2="8.5598" y2="2.4638" layer="37"/>
<rectangle x1="9.9314" y1="2.413" x2="9.9822" y2="2.4638" layer="37"/>
<rectangle x1="10.6426" y1="2.413" x2="10.6934" y2="2.4638" layer="37"/>
<rectangle x1="3.4798" y1="2.413" x2="3.5306" y2="2.4638" layer="37"/>
<rectangle x1="4.699" y1="2.413" x2="4.7498" y2="2.4638" layer="37"/>
<rectangle x1="4.8514" y1="2.413" x2="4.9022" y2="2.4638" layer="37"/>
<rectangle x1="3.3274" y1="2.413" x2="3.3782" y2="2.4638" layer="37"/>
<rectangle x1="9.8806" y1="2.413" x2="9.9314" y2="2.4638" layer="37"/>
<rectangle x1="5.461" y1="2.413" x2="5.5118" y2="2.4638" layer="37"/>
<rectangle x1="10.5918" y1="2.413" x2="10.6426" y2="2.4638" layer="37"/>
<rectangle x1="10.6934" y1="2.413" x2="10.7442" y2="2.4638" layer="37"/>
<rectangle x1="3.3782" y1="2.413" x2="3.429" y2="2.4638" layer="37"/>
<rectangle x1="9.9822" y1="2.413" x2="10.033" y2="2.4638" layer="37"/>
<rectangle x1="7.5438" y1="2.413" x2="7.5946" y2="2.4638" layer="37"/>
<rectangle x1="3.2766" y1="2.4638" x2="3.4798" y2="2.5146" layer="37"/>
<rectangle x1="4.7498" y1="2.4638" x2="4.953" y2="2.5146" layer="37"/>
<rectangle x1="5.4102" y1="2.4638" x2="5.461" y2="2.5146" layer="37"/>
<rectangle x1="6.477" y1="2.4638" x2="6.5786" y2="2.5146" layer="37"/>
<rectangle x1="7.493" y1="2.4638" x2="7.5438" y2="2.5146" layer="37"/>
<rectangle x1="8.4582" y1="2.4638" x2="8.5598" y2="2.5146" layer="37"/>
<rectangle x1="9.8806" y1="2.4638" x2="9.9314" y2="2.5146" layer="37"/>
<rectangle x1="10.6934" y1="2.4638" x2="10.7442" y2="2.5146" layer="37"/>
<rectangle x1="10.6426" y1="2.4638" x2="10.6934" y2="2.5146" layer="37"/>
<rectangle x1="9.9314" y1="2.4638" x2="9.9822" y2="2.5146" layer="37"/>
<rectangle x1="5.461" y1="2.4638" x2="5.5118" y2="2.5146" layer="37"/>
<rectangle x1="7.5438" y1="2.4638" x2="7.5946" y2="2.5146" layer="37"/>
<rectangle x1="9.8298" y1="2.4638" x2="9.8806" y2="2.5146" layer="37"/>
<rectangle x1="3.2766" y1="2.5146" x2="3.429" y2="2.5654" layer="37"/>
<rectangle x1="4.8006" y1="2.5146" x2="4.953" y2="2.5654" layer="37"/>
<rectangle x1="5.4102" y1="2.5146" x2="5.461" y2="2.5654" layer="37"/>
<rectangle x1="6.477" y1="2.5146" x2="6.5786" y2="2.5654" layer="37"/>
<rectangle x1="7.493" y1="2.5146" x2="7.5438" y2="2.5654" layer="37"/>
<rectangle x1="8.4582" y1="2.5146" x2="8.5598" y2="2.5654" layer="37"/>
<rectangle x1="9.8298" y1="2.5146" x2="9.9314" y2="2.5654" layer="37"/>
<rectangle x1="10.6934" y1="2.5146" x2="10.7442" y2="2.5654" layer="37"/>
<rectangle x1="3.429" y1="2.5146" x2="3.4798" y2="2.5654" layer="37"/>
<rectangle x1="4.7498" y1="2.5146" x2="4.8006" y2="2.5654" layer="37"/>
<rectangle x1="10.7442" y1="2.5146" x2="10.795" y2="2.5654" layer="37"/>
<rectangle x1="5.461" y1="2.5146" x2="5.5118" y2="2.5654" layer="37"/>
<rectangle x1="7.5438" y1="2.5146" x2="7.5946" y2="2.5654" layer="37"/>
<rectangle x1="3.2766" y1="2.5654" x2="3.429" y2="2.6162" layer="37"/>
<rectangle x1="4.8006" y1="2.5654" x2="4.953" y2="2.6162" layer="37"/>
<rectangle x1="5.4102" y1="2.5654" x2="5.461" y2="2.6162" layer="37"/>
<rectangle x1="6.477" y1="2.5654" x2="6.5786" y2="2.6162" layer="37"/>
<rectangle x1="7.493" y1="2.5654" x2="7.5438" y2="2.6162" layer="37"/>
<rectangle x1="8.4582" y1="2.5654" x2="8.5598" y2="2.6162" layer="37"/>
<rectangle x1="9.8298" y1="2.5654" x2="9.8806" y2="2.6162" layer="37"/>
<rectangle x1="10.7442" y1="2.5654" x2="10.795" y2="2.6162" layer="37"/>
<rectangle x1="9.779" y1="2.5654" x2="9.8298" y2="2.6162" layer="37"/>
<rectangle x1="10.795" y1="2.5654" x2="10.8458" y2="2.6162" layer="37"/>
<rectangle x1="5.461" y1="2.5654" x2="5.5118" y2="2.6162" layer="37"/>
<rectangle x1="10.6934" y1="2.5654" x2="10.7442" y2="2.6162" layer="37"/>
<rectangle x1="7.5438" y1="2.5654" x2="7.5946" y2="2.6162" layer="37"/>
<rectangle x1="4.7498" y1="2.5654" x2="4.8006" y2="2.6162" layer="37"/>
<rectangle x1="3.2766" y1="2.6162" x2="3.3782" y2="2.667" layer="37"/>
<rectangle x1="4.8514" y1="2.6162" x2="4.953" y2="2.667" layer="37"/>
<rectangle x1="5.4102" y1="2.6162" x2="5.461" y2="2.667" layer="37"/>
<rectangle x1="6.477" y1="2.6162" x2="6.5786" y2="2.667" layer="37"/>
<rectangle x1="7.493" y1="2.6162" x2="7.5438" y2="2.667" layer="37"/>
<rectangle x1="8.4582" y1="2.6162" x2="8.5598" y2="2.667" layer="37"/>
<rectangle x1="9.779" y1="2.6162" x2="9.8298" y2="2.667" layer="37"/>
<rectangle x1="10.795" y1="2.6162" x2="10.8458" y2="2.667" layer="37"/>
<rectangle x1="3.3782" y1="2.6162" x2="3.429" y2="2.667" layer="37"/>
<rectangle x1="4.8006" y1="2.6162" x2="4.8514" y2="2.667" layer="37"/>
<rectangle x1="10.7442" y1="2.6162" x2="10.795" y2="2.667" layer="37"/>
<rectangle x1="9.8298" y1="2.6162" x2="9.8806" y2="2.667" layer="37"/>
<rectangle x1="5.461" y1="2.6162" x2="5.5118" y2="2.667" layer="37"/>
<rectangle x1="7.5438" y1="2.6162" x2="7.5946" y2="2.667" layer="37"/>
<rectangle x1="9.7282" y1="2.6162" x2="9.779" y2="2.667" layer="37"/>
<rectangle x1="3.2766" y1="2.667" x2="3.3782" y2="2.7178" layer="37"/>
<rectangle x1="4.8514" y1="2.667" x2="4.953" y2="2.7178" layer="37"/>
<rectangle x1="5.4102" y1="2.667" x2="5.461" y2="2.7178" layer="37"/>
<rectangle x1="6.477" y1="2.667" x2="6.5786" y2="2.7178" layer="37"/>
<rectangle x1="7.493" y1="2.667" x2="7.5438" y2="2.7178" layer="37"/>
<rectangle x1="8.4582" y1="2.667" x2="8.5598" y2="2.7178" layer="37"/>
<rectangle x1="9.7282" y1="2.667" x2="9.8298" y2="2.7178" layer="37"/>
<rectangle x1="10.795" y1="2.667" x2="10.8966" y2="2.7178" layer="37"/>
<rectangle x1="7.5438" y1="2.667" x2="7.5946" y2="2.7178" layer="37"/>
<rectangle x1="7.4422" y1="2.667" x2="7.493" y2="2.7178" layer="37"/>
<rectangle x1="8.5598" y1="2.667" x2="8.6106" y2="2.7178" layer="37"/>
<rectangle x1="7.5946" y1="2.667" x2="7.6454" y2="2.7178" layer="37"/>
<rectangle x1="5.461" y1="2.667" x2="5.5118" y2="2.7178" layer="37"/>
<rectangle x1="6.985" y1="2.667" x2="7.4422" y2="2.7178" layer="37"/>
<rectangle x1="7.6454" y1="2.667" x2="8.1026" y2="2.7178" layer="37"/>
<rectangle x1="8.6106" y1="2.667" x2="9.271" y2="2.7178" layer="37"/>
<rectangle x1="6.9342" y1="2.667" x2="6.985" y2="2.7178" layer="37"/>
<rectangle x1="9.271" y1="2.667" x2="9.3218" y2="2.7178" layer="37"/>
<rectangle x1="4.8006" y1="2.667" x2="4.8514" y2="2.7178" layer="37"/>
<rectangle x1="8.1026" y1="2.667" x2="8.1534" y2="2.7178" layer="37"/>
<rectangle x1="3.3782" y1="2.667" x2="3.429" y2="2.7178" layer="37"/>
<rectangle x1="3.2766" y1="2.7178" x2="3.3274" y2="2.7686" layer="37"/>
<rectangle x1="4.9022" y1="2.7178" x2="4.953" y2="2.7686" layer="37"/>
<rectangle x1="5.4102" y1="2.7178" x2="5.461" y2="2.7686" layer="37"/>
<rectangle x1="6.5278" y1="2.7178" x2="6.5786" y2="2.7686" layer="37"/>
<rectangle x1="6.9342" y1="2.7178" x2="8.1534" y2="2.7686" layer="37"/>
<rectangle x1="8.509" y1="2.7178" x2="9.3218" y2="2.7686" layer="37"/>
<rectangle x1="9.7282" y1="2.7178" x2="9.779" y2="2.7686" layer="37"/>
<rectangle x1="10.8458" y1="2.7178" x2="10.8966" y2="2.7686" layer="37"/>
<rectangle x1="3.3274" y1="2.7178" x2="3.3782" y2="2.7686" layer="37"/>
<rectangle x1="4.8514" y1="2.7178" x2="4.9022" y2="2.7686" layer="37"/>
<rectangle x1="6.477" y1="2.7178" x2="6.5278" y2="2.7686" layer="37"/>
<rectangle x1="8.4582" y1="2.7178" x2="8.509" y2="2.7686" layer="37"/>
<rectangle x1="9.3218" y1="2.7178" x2="9.3726" y2="2.7686" layer="37"/>
<rectangle x1="6.8834" y1="2.7178" x2="6.9342" y2="2.7686" layer="37"/>
<rectangle x1="5.461" y1="2.7178" x2="5.5118" y2="2.7686" layer="37"/>
<rectangle x1="10.795" y1="2.7178" x2="10.8458" y2="2.7686" layer="37"/>
<rectangle x1="3.0734" y1="3.3782" x2="3.2766" y2="3.429" layer="37"/>
<rectangle x1="4.3434" y1="3.3782" x2="4.5466" y2="3.429" layer="37"/>
<rectangle x1="9.779" y1="3.3782" x2="9.9822" y2="3.429" layer="37"/>
<rectangle x1="10.9982" y1="3.3782" x2="11.2014" y2="3.429" layer="37"/>
<rectangle x1="9.9822" y1="3.3782" x2="10.033" y2="3.429" layer="37"/>
<rectangle x1="4.5466" y1="3.3782" x2="4.5974" y2="3.429" layer="37"/>
<rectangle x1="4.2926" y1="3.3782" x2="4.3434" y2="3.429" layer="37"/>
<rectangle x1="3.2766" y1="3.3782" x2="3.3274" y2="3.429" layer="37"/>
<rectangle x1="3.0226" y1="3.3782" x2="3.0734" y2="3.429" layer="37"/>
<rectangle x1="10.9474" y1="3.3782" x2="10.9982" y2="3.429" layer="37"/>
<rectangle x1="2.9718" y1="3.429" x2="3.3274" y2="3.4798" layer="37"/>
<rectangle x1="4.2926" y1="3.429" x2="4.6482" y2="3.4798" layer="37"/>
<rectangle x1="9.7282" y1="3.429" x2="10.0838" y2="3.4798" layer="37"/>
<rectangle x1="10.9474" y1="3.429" x2="11.2522" y2="3.4798" layer="37"/>
<rectangle x1="4.2418" y1="3.429" x2="4.2926" y2="3.4798" layer="37"/>
<rectangle x1="3.3274" y1="3.429" x2="3.3782" y2="3.4798" layer="37"/>
<rectangle x1="9.6774" y1="3.429" x2="9.7282" y2="3.4798" layer="37"/>
<rectangle x1="10.8966" y1="3.429" x2="10.9474" y2="3.4798" layer="37"/>
<rectangle x1="2.9718" y1="3.4798" x2="3.3782" y2="3.5306" layer="37"/>
<rectangle x1="4.2418" y1="3.4798" x2="4.6482" y2="3.5306" layer="37"/>
<rectangle x1="9.6774" y1="3.4798" x2="10.1346" y2="3.5306" layer="37"/>
<rectangle x1="10.8966" y1="3.4798" x2="11.2522" y2="3.5306" layer="37"/>
<rectangle x1="11.2522" y1="3.4798" x2="11.303" y2="3.5306" layer="37"/>
<rectangle x1="4.6482" y1="3.4798" x2="4.699" y2="3.5306" layer="37"/>
<rectangle x1="2.921" y1="3.4798" x2="2.9718" y2="3.5306" layer="37"/>
<rectangle x1="4.191" y1="3.4798" x2="4.2418" y2="3.5306" layer="37"/>
<rectangle x1="3.3782" y1="3.4798" x2="3.429" y2="3.5306" layer="37"/>
<rectangle x1="2.9718" y1="3.5306" x2="3.429" y2="3.5814" layer="37"/>
<rectangle x1="4.191" y1="3.5306" x2="4.699" y2="3.5814" layer="37"/>
<rectangle x1="9.6774" y1="3.5306" x2="10.1346" y2="3.5814" layer="37"/>
<rectangle x1="10.8966" y1="3.5306" x2="11.2522" y2="3.5814" layer="37"/>
<rectangle x1="2.921" y1="3.5306" x2="2.9718" y2="3.5814" layer="37"/>
<rectangle x1="11.2522" y1="3.5306" x2="11.303" y2="3.5814" layer="37"/>
<rectangle x1="9.6266" y1="3.5306" x2="9.6774" y2="3.5814" layer="37"/>
<rectangle x1="2.921" y1="3.5814" x2="3.429" y2="3.6322" layer="37"/>
<rectangle x1="4.191" y1="3.5814" x2="4.699" y2="3.6322" layer="37"/>
<rectangle x1="9.6266" y1="3.5814" x2="10.1346" y2="3.6322" layer="37"/>
<rectangle x1="10.8966" y1="3.5814" x2="11.2522" y2="3.6322" layer="37"/>
<rectangle x1="11.2522" y1="3.5814" x2="11.303" y2="3.6322" layer="37"/>
<rectangle x1="10.1346" y1="3.5814" x2="10.1854" y2="3.6322" layer="37"/>
<rectangle x1="2.921" y1="3.6322" x2="3.429" y2="3.683" layer="37"/>
<rectangle x1="4.191" y1="3.6322" x2="4.699" y2="3.683" layer="37"/>
<rectangle x1="9.6266" y1="3.6322" x2="10.1854" y2="3.683" layer="37"/>
<rectangle x1="10.8966" y1="3.6322" x2="11.303" y2="3.683" layer="37"/>
<rectangle x1="10.8458" y1="3.6322" x2="10.8966" y2="3.683" layer="37"/>
<rectangle x1="2.921" y1="3.683" x2="3.429" y2="3.7338" layer="37"/>
<rectangle x1="4.191" y1="3.683" x2="4.699" y2="3.7338" layer="37"/>
<rectangle x1="9.6266" y1="3.683" x2="10.1854" y2="3.7338" layer="37"/>
<rectangle x1="10.8966" y1="3.683" x2="11.303" y2="3.7338" layer="37"/>
<rectangle x1="10.8458" y1="3.683" x2="10.8966" y2="3.7338" layer="37"/>
<rectangle x1="2.921" y1="3.7338" x2="3.429" y2="3.7846" layer="37"/>
<rectangle x1="4.191" y1="3.7338" x2="4.699" y2="3.7846" layer="37"/>
<rectangle x1="9.6266" y1="3.7338" x2="10.1854" y2="3.7846" layer="37"/>
<rectangle x1="10.8966" y1="3.7338" x2="11.303" y2="3.7846" layer="37"/>
<rectangle x1="10.8458" y1="3.7338" x2="10.8966" y2="3.7846" layer="37"/>
<rectangle x1="2.921" y1="3.7846" x2="3.429" y2="3.8354" layer="37"/>
<rectangle x1="4.191" y1="3.7846" x2="4.699" y2="3.8354" layer="37"/>
<rectangle x1="9.6266" y1="3.7846" x2="10.1854" y2="3.8354" layer="37"/>
<rectangle x1="10.8966" y1="3.7846" x2="11.303" y2="3.8354" layer="37"/>
<rectangle x1="10.8458" y1="3.7846" x2="10.8966" y2="3.8354" layer="37"/>
<rectangle x1="2.921" y1="3.8354" x2="3.429" y2="3.8862" layer="37"/>
<rectangle x1="4.191" y1="3.8354" x2="4.699" y2="3.8862" layer="37"/>
<rectangle x1="9.6266" y1="3.8354" x2="10.1854" y2="3.8862" layer="37"/>
<rectangle x1="10.8966" y1="3.8354" x2="11.303" y2="3.8862" layer="37"/>
<rectangle x1="7.3406" y1="3.8354" x2="7.3914" y2="3.8862" layer="37"/>
<rectangle x1="7.3914" y1="3.8354" x2="7.4422" y2="3.8862" layer="37"/>
<rectangle x1="7.2898" y1="3.8354" x2="7.3406" y2="3.8862" layer="37"/>
<rectangle x1="10.8458" y1="3.8354" x2="10.8966" y2="3.8862" layer="37"/>
<rectangle x1="2.921" y1="3.8862" x2="3.429" y2="3.937" layer="37"/>
<rectangle x1="4.191" y1="3.8862" x2="4.699" y2="3.937" layer="37"/>
<rectangle x1="7.239" y1="3.8862" x2="7.493" y2="3.937" layer="37"/>
<rectangle x1="9.6266" y1="3.8862" x2="10.1854" y2="3.937" layer="37"/>
<rectangle x1="10.8966" y1="3.8862" x2="11.303" y2="3.937" layer="37"/>
<rectangle x1="7.1882" y1="3.8862" x2="7.239" y2="3.937" layer="37"/>
<rectangle x1="7.493" y1="3.8862" x2="7.5438" y2="3.937" layer="37"/>
<rectangle x1="10.8458" y1="3.8862" x2="10.8966" y2="3.937" layer="37"/>
<rectangle x1="2.921" y1="3.937" x2="3.429" y2="3.9878" layer="37"/>
<rectangle x1="4.191" y1="3.937" x2="4.699" y2="3.9878" layer="37"/>
<rectangle x1="7.1882" y1="3.937" x2="7.5438" y2="3.9878" layer="37"/>
<rectangle x1="9.6266" y1="3.937" x2="10.1854" y2="3.9878" layer="37"/>
<rectangle x1="10.8966" y1="3.937" x2="11.303" y2="3.9878" layer="37"/>
<rectangle x1="7.1374" y1="3.937" x2="7.1882" y2="3.9878" layer="37"/>
<rectangle x1="7.5438" y1="3.937" x2="7.5946" y2="3.9878" layer="37"/>
<rectangle x1="10.8458" y1="3.937" x2="10.8966" y2="3.9878" layer="37"/>
<rectangle x1="2.921" y1="3.9878" x2="3.429" y2="4.0386" layer="37"/>
<rectangle x1="4.191" y1="3.9878" x2="4.699" y2="4.0386" layer="37"/>
<rectangle x1="7.1374" y1="3.9878" x2="7.5946" y2="4.0386" layer="37"/>
<rectangle x1="9.6266" y1="3.9878" x2="10.1854" y2="4.0386" layer="37"/>
<rectangle x1="10.8966" y1="3.9878" x2="11.303" y2="4.0386" layer="37"/>
<rectangle x1="7.0866" y1="3.9878" x2="7.1374" y2="4.0386" layer="37"/>
<rectangle x1="7.5946" y1="3.9878" x2="7.6454" y2="4.0386" layer="37"/>
<rectangle x1="10.8458" y1="3.9878" x2="10.8966" y2="4.0386" layer="37"/>
<rectangle x1="2.921" y1="4.0386" x2="3.429" y2="4.0894" layer="37"/>
<rectangle x1="4.191" y1="4.0386" x2="4.699" y2="4.0894" layer="37"/>
<rectangle x1="7.0866" y1="4.0386" x2="7.6454" y2="4.0894" layer="37"/>
<rectangle x1="9.6266" y1="4.0386" x2="10.1854" y2="4.0894" layer="37"/>
<rectangle x1="10.8966" y1="4.0386" x2="11.303" y2="4.0894" layer="37"/>
<rectangle x1="7.0358" y1="4.0386" x2="7.0866" y2="4.0894" layer="37"/>
<rectangle x1="7.6454" y1="4.0386" x2="7.6962" y2="4.0894" layer="37"/>
<rectangle x1="10.8458" y1="4.0386" x2="10.8966" y2="4.0894" layer="37"/>
<rectangle x1="2.921" y1="4.0894" x2="3.429" y2="4.1402" layer="37"/>
<rectangle x1="4.191" y1="4.0894" x2="4.699" y2="4.1402" layer="37"/>
<rectangle x1="7.0358" y1="4.0894" x2="7.6962" y2="4.1402" layer="37"/>
<rectangle x1="9.6266" y1="4.0894" x2="10.1854" y2="4.1402" layer="37"/>
<rectangle x1="10.8966" y1="4.0894" x2="11.303" y2="4.1402" layer="37"/>
<rectangle x1="6.985" y1="4.0894" x2="7.0358" y2="4.1402" layer="37"/>
<rectangle x1="7.6962" y1="4.0894" x2="7.747" y2="4.1402" layer="37"/>
<rectangle x1="10.8458" y1="4.0894" x2="10.8966" y2="4.1402" layer="37"/>
<rectangle x1="2.921" y1="4.1402" x2="3.429" y2="4.191" layer="37"/>
<rectangle x1="4.191" y1="4.1402" x2="4.699" y2="4.191" layer="37"/>
<rectangle x1="6.985" y1="4.1402" x2="7.747" y2="4.191" layer="37"/>
<rectangle x1="9.6266" y1="4.1402" x2="10.1854" y2="4.191" layer="37"/>
<rectangle x1="10.8966" y1="4.1402" x2="11.303" y2="4.191" layer="37"/>
<rectangle x1="6.9342" y1="4.1402" x2="6.985" y2="4.191" layer="37"/>
<rectangle x1="7.747" y1="4.1402" x2="7.7978" y2="4.191" layer="37"/>
<rectangle x1="10.8458" y1="4.1402" x2="10.8966" y2="4.191" layer="37"/>
<rectangle x1="2.921" y1="4.191" x2="3.429" y2="4.2418" layer="37"/>
<rectangle x1="4.191" y1="4.191" x2="4.699" y2="4.2418" layer="37"/>
<rectangle x1="6.985" y1="4.191" x2="7.7978" y2="4.2418" layer="37"/>
<rectangle x1="9.6266" y1="4.191" x2="10.1854" y2="4.2418" layer="37"/>
<rectangle x1="10.8966" y1="4.191" x2="11.303" y2="4.2418" layer="37"/>
<rectangle x1="6.9342" y1="4.191" x2="6.985" y2="4.2418" layer="37"/>
<rectangle x1="6.8834" y1="4.191" x2="6.9342" y2="4.2418" layer="37"/>
<rectangle x1="7.7978" y1="4.191" x2="7.8486" y2="4.2418" layer="37"/>
<rectangle x1="10.8458" y1="4.191" x2="10.8966" y2="4.2418" layer="37"/>
<rectangle x1="2.921" y1="4.2418" x2="3.429" y2="4.2926" layer="37"/>
<rectangle x1="4.191" y1="4.2418" x2="4.699" y2="4.2926" layer="37"/>
<rectangle x1="6.8834" y1="4.2418" x2="7.8486" y2="4.2926" layer="37"/>
<rectangle x1="9.6266" y1="4.2418" x2="10.1854" y2="4.2926" layer="37"/>
<rectangle x1="10.8966" y1="4.2418" x2="11.303" y2="4.2926" layer="37"/>
<rectangle x1="6.8326" y1="4.2418" x2="6.8834" y2="4.2926" layer="37"/>
<rectangle x1="10.8458" y1="4.2418" x2="10.8966" y2="4.2926" layer="37"/>
<rectangle x1="7.8486" y1="4.2418" x2="7.8994" y2="4.2926" layer="37"/>
<rectangle x1="2.921" y1="4.2926" x2="3.429" y2="4.3434" layer="37"/>
<rectangle x1="4.191" y1="4.2926" x2="4.699" y2="4.3434" layer="37"/>
<rectangle x1="6.8326" y1="4.2926" x2="7.8994" y2="4.3434" layer="37"/>
<rectangle x1="9.6266" y1="4.2926" x2="10.1854" y2="4.3434" layer="37"/>
<rectangle x1="10.8966" y1="4.2926" x2="11.303" y2="4.3434" layer="37"/>
<rectangle x1="10.8458" y1="4.2926" x2="10.8966" y2="4.3434" layer="37"/>
<rectangle x1="2.921" y1="4.3434" x2="3.429" y2="4.3942" layer="37"/>
<rectangle x1="4.191" y1="4.3434" x2="4.699" y2="4.3942" layer="37"/>
<rectangle x1="6.7818" y1="4.3434" x2="7.9502" y2="4.3942" layer="37"/>
<rectangle x1="9.6266" y1="4.3434" x2="10.1854" y2="4.3942" layer="37"/>
<rectangle x1="10.8966" y1="4.3434" x2="11.303" y2="4.3942" layer="37"/>
<rectangle x1="10.8458" y1="4.3434" x2="10.8966" y2="4.3942" layer="37"/>
<rectangle x1="2.921" y1="4.3942" x2="3.429" y2="4.445" layer="37"/>
<rectangle x1="4.191" y1="4.3942" x2="4.699" y2="4.445" layer="37"/>
<rectangle x1="6.731" y1="4.3942" x2="8.001" y2="4.445" layer="37"/>
<rectangle x1="9.6266" y1="4.3942" x2="10.1854" y2="4.445" layer="37"/>
<rectangle x1="10.8966" y1="4.3942" x2="11.303" y2="4.445" layer="37"/>
<rectangle x1="10.8458" y1="4.3942" x2="10.8966" y2="4.445" layer="37"/>
<rectangle x1="2.921" y1="4.445" x2="3.429" y2="4.4958" layer="37"/>
<rectangle x1="4.191" y1="4.445" x2="4.699" y2="4.4958" layer="37"/>
<rectangle x1="6.6802" y1="4.445" x2="8.0518" y2="4.4958" layer="37"/>
<rectangle x1="9.6266" y1="4.445" x2="10.1854" y2="4.4958" layer="37"/>
<rectangle x1="10.8966" y1="4.445" x2="11.303" y2="4.4958" layer="37"/>
<rectangle x1="10.8458" y1="4.445" x2="10.8966" y2="4.4958" layer="37"/>
<rectangle x1="2.921" y1="4.4958" x2="3.429" y2="4.5466" layer="37"/>
<rectangle x1="4.191" y1="4.4958" x2="4.699" y2="4.5466" layer="37"/>
<rectangle x1="6.6294" y1="4.4958" x2="7.3406" y2="4.5466" layer="37"/>
<rectangle x1="7.3914" y1="4.4958" x2="8.1026" y2="4.5466" layer="37"/>
<rectangle x1="9.6266" y1="4.4958" x2="10.1854" y2="4.5466" layer="37"/>
<rectangle x1="10.8966" y1="4.4958" x2="11.303" y2="4.5466" layer="37"/>
<rectangle x1="7.3406" y1="4.4958" x2="7.3914" y2="4.5466" layer="37"/>
<rectangle x1="10.8458" y1="4.4958" x2="10.8966" y2="4.5466" layer="37"/>
<rectangle x1="2.921" y1="4.5466" x2="3.429" y2="4.5974" layer="37"/>
<rectangle x1="4.191" y1="4.5466" x2="4.699" y2="4.5974" layer="37"/>
<rectangle x1="6.6294" y1="4.5466" x2="7.2898" y2="4.5974" layer="37"/>
<rectangle x1="7.4422" y1="4.5466" x2="8.1534" y2="4.5974" layer="37"/>
<rectangle x1="9.6266" y1="4.5466" x2="10.1854" y2="4.5974" layer="37"/>
<rectangle x1="10.8966" y1="4.5466" x2="11.303" y2="4.5974" layer="37"/>
<rectangle x1="6.5786" y1="4.5466" x2="6.6294" y2="4.5974" layer="37"/>
<rectangle x1="7.3914" y1="4.5466" x2="7.4422" y2="4.5974" layer="37"/>
<rectangle x1="7.2898" y1="4.5466" x2="7.3406" y2="4.5974" layer="37"/>
<rectangle x1="10.8458" y1="4.5466" x2="10.8966" y2="4.5974" layer="37"/>
<rectangle x1="2.921" y1="4.5974" x2="3.429" y2="4.6482" layer="37"/>
<rectangle x1="4.191" y1="4.5974" x2="4.699" y2="4.6482" layer="37"/>
<rectangle x1="6.5786" y1="4.5974" x2="7.239" y2="4.6482" layer="37"/>
<rectangle x1="7.493" y1="4.5974" x2="8.1534" y2="4.6482" layer="37"/>
<rectangle x1="9.6266" y1="4.5974" x2="10.1854" y2="4.6482" layer="37"/>
<rectangle x1="10.8966" y1="4.5974" x2="11.303" y2="4.6482" layer="37"/>
<rectangle x1="6.5278" y1="4.5974" x2="6.5786" y2="4.6482" layer="37"/>
<rectangle x1="8.1534" y1="4.5974" x2="8.2042" y2="4.6482" layer="37"/>
<rectangle x1="7.4422" y1="4.5974" x2="7.493" y2="4.6482" layer="37"/>
<rectangle x1="7.239" y1="4.5974" x2="7.2898" y2="4.6482" layer="37"/>
<rectangle x1="10.8458" y1="4.5974" x2="10.8966" y2="4.6482" layer="37"/>
<rectangle x1="2.921" y1="4.6482" x2="3.429" y2="4.699" layer="37"/>
<rectangle x1="4.191" y1="4.6482" x2="4.699" y2="4.699" layer="37"/>
<rectangle x1="6.5278" y1="4.6482" x2="7.1882" y2="4.699" layer="37"/>
<rectangle x1="7.5438" y1="4.6482" x2="8.2042" y2="4.699" layer="37"/>
<rectangle x1="9.6266" y1="4.6482" x2="10.1854" y2="4.699" layer="37"/>
<rectangle x1="10.8966" y1="4.6482" x2="11.303" y2="4.699" layer="37"/>
<rectangle x1="6.477" y1="4.6482" x2="6.5278" y2="4.699" layer="37"/>
<rectangle x1="8.2042" y1="4.6482" x2="8.255" y2="4.699" layer="37"/>
<rectangle x1="7.493" y1="4.6482" x2="7.5438" y2="4.699" layer="37"/>
<rectangle x1="7.1882" y1="4.6482" x2="7.239" y2="4.699" layer="37"/>
<rectangle x1="10.8458" y1="4.6482" x2="10.8966" y2="4.699" layer="37"/>
<rectangle x1="2.921" y1="4.699" x2="3.429" y2="4.7498" layer="37"/>
<rectangle x1="4.191" y1="4.699" x2="4.699" y2="4.7498" layer="37"/>
<rectangle x1="6.477" y1="4.699" x2="7.1374" y2="4.7498" layer="37"/>
<rectangle x1="7.5946" y1="4.699" x2="8.255" y2="4.7498" layer="37"/>
<rectangle x1="9.6266" y1="4.699" x2="10.1854" y2="4.7498" layer="37"/>
<rectangle x1="10.8966" y1="4.699" x2="11.303" y2="4.7498" layer="37"/>
<rectangle x1="6.4262" y1="4.699" x2="6.477" y2="4.7498" layer="37"/>
<rectangle x1="7.5438" y1="4.699" x2="7.5946" y2="4.7498" layer="37"/>
<rectangle x1="7.1374" y1="4.699" x2="7.1882" y2="4.7498" layer="37"/>
<rectangle x1="8.255" y1="4.699" x2="8.3058" y2="4.7498" layer="37"/>
<rectangle x1="10.8458" y1="4.699" x2="10.8966" y2="4.7498" layer="37"/>
<rectangle x1="2.921" y1="4.7498" x2="3.429" y2="4.8006" layer="37"/>
<rectangle x1="4.191" y1="4.7498" x2="4.699" y2="4.8006" layer="37"/>
<rectangle x1="6.4262" y1="4.7498" x2="7.0866" y2="4.8006" layer="37"/>
<rectangle x1="7.6454" y1="4.7498" x2="8.3058" y2="4.8006" layer="37"/>
<rectangle x1="9.6266" y1="4.7498" x2="10.1854" y2="4.8006" layer="37"/>
<rectangle x1="10.8966" y1="4.7498" x2="11.303" y2="4.8006" layer="37"/>
<rectangle x1="7.0866" y1="4.7498" x2="7.1374" y2="4.8006" layer="37"/>
<rectangle x1="7.5946" y1="4.7498" x2="7.6454" y2="4.8006" layer="37"/>
<rectangle x1="6.3754" y1="4.7498" x2="6.4262" y2="4.8006" layer="37"/>
<rectangle x1="8.3058" y1="4.7498" x2="8.3566" y2="4.8006" layer="37"/>
<rectangle x1="10.8458" y1="4.7498" x2="10.8966" y2="4.8006" layer="37"/>
<rectangle x1="2.921" y1="4.8006" x2="3.429" y2="4.8514" layer="37"/>
<rectangle x1="4.191" y1="4.8006" x2="4.699" y2="4.8514" layer="37"/>
<rectangle x1="6.3754" y1="4.8006" x2="7.0358" y2="4.8514" layer="37"/>
<rectangle x1="7.6962" y1="4.8006" x2="8.3566" y2="4.8514" layer="37"/>
<rectangle x1="9.6266" y1="4.8006" x2="10.1854" y2="4.8514" layer="37"/>
<rectangle x1="10.8966" y1="4.8006" x2="11.303" y2="4.8514" layer="37"/>
<rectangle x1="7.0358" y1="4.8006" x2="7.0866" y2="4.8514" layer="37"/>
<rectangle x1="7.6454" y1="4.8006" x2="7.6962" y2="4.8514" layer="37"/>
<rectangle x1="6.3246" y1="4.8006" x2="6.3754" y2="4.8514" layer="37"/>
<rectangle x1="10.8458" y1="4.8006" x2="10.8966" y2="4.8514" layer="37"/>
<rectangle x1="8.3566" y1="4.8006" x2="8.4074" y2="4.8514" layer="37"/>
<rectangle x1="2.921" y1="4.8514" x2="3.429" y2="4.9022" layer="37"/>
<rectangle x1="4.191" y1="4.8514" x2="4.699" y2="4.9022" layer="37"/>
<rectangle x1="6.3246" y1="4.8514" x2="7.0358" y2="4.9022" layer="37"/>
<rectangle x1="7.747" y1="4.8514" x2="8.4074" y2="4.9022" layer="37"/>
<rectangle x1="9.6266" y1="4.8514" x2="10.1854" y2="4.9022" layer="37"/>
<rectangle x1="10.8966" y1="4.8514" x2="11.303" y2="4.9022" layer="37"/>
<rectangle x1="7.6962" y1="4.8514" x2="7.747" y2="4.9022" layer="37"/>
<rectangle x1="6.2738" y1="4.8514" x2="6.3246" y2="4.9022" layer="37"/>
<rectangle x1="10.8458" y1="4.8514" x2="10.8966" y2="4.9022" layer="37"/>
<rectangle x1="2.921" y1="4.9022" x2="3.429" y2="4.953" layer="37"/>
<rectangle x1="4.191" y1="4.9022" x2="4.699" y2="4.953" layer="37"/>
<rectangle x1="6.2738" y1="4.9022" x2="6.985" y2="4.953" layer="37"/>
<rectangle x1="7.747" y1="4.9022" x2="8.4074" y2="4.953" layer="37"/>
<rectangle x1="9.6266" y1="4.9022" x2="10.1854" y2="4.953" layer="37"/>
<rectangle x1="10.8966" y1="4.9022" x2="11.303" y2="4.953" layer="37"/>
<rectangle x1="8.4074" y1="4.9022" x2="8.4582" y2="4.953" layer="37"/>
<rectangle x1="6.223" y1="4.9022" x2="6.2738" y2="4.953" layer="37"/>
<rectangle x1="10.8458" y1="4.9022" x2="10.8966" y2="4.953" layer="37"/>
<rectangle x1="2.921" y1="4.953" x2="3.429" y2="5.0038" layer="37"/>
<rectangle x1="4.191" y1="4.953" x2="4.699" y2="5.0038" layer="37"/>
<rectangle x1="6.223" y1="4.953" x2="6.9342" y2="5.0038" layer="37"/>
<rectangle x1="7.7978" y1="4.953" x2="8.4582" y2="5.0038" layer="37"/>
<rectangle x1="9.6266" y1="4.953" x2="10.1854" y2="5.0038" layer="37"/>
<rectangle x1="10.8966" y1="4.953" x2="11.303" y2="5.0038" layer="37"/>
<rectangle x1="8.4582" y1="4.953" x2="8.509" y2="5.0038" layer="37"/>
<rectangle x1="6.1722" y1="4.953" x2="6.223" y2="5.0038" layer="37"/>
<rectangle x1="10.8458" y1="4.953" x2="10.8966" y2="5.0038" layer="37"/>
<rectangle x1="2.921" y1="5.0038" x2="3.429" y2="5.0546" layer="37"/>
<rectangle x1="4.191" y1="5.0038" x2="4.699" y2="5.0546" layer="37"/>
<rectangle x1="6.1722" y1="5.0038" x2="6.8834" y2="5.0546" layer="37"/>
<rectangle x1="7.8486" y1="5.0038" x2="8.509" y2="5.0546" layer="37"/>
<rectangle x1="9.6266" y1="5.0038" x2="10.1854" y2="5.0546" layer="37"/>
<rectangle x1="10.8966" y1="5.0038" x2="11.303" y2="5.0546" layer="37"/>
<rectangle x1="8.509" y1="5.0038" x2="8.5598" y2="5.0546" layer="37"/>
<rectangle x1="6.1214" y1="5.0038" x2="6.1722" y2="5.0546" layer="37"/>
<rectangle x1="10.8458" y1="5.0038" x2="10.8966" y2="5.0546" layer="37"/>
<rectangle x1="2.921" y1="5.0546" x2="3.429" y2="5.1054" layer="37"/>
<rectangle x1="4.191" y1="5.0546" x2="4.699" y2="5.1054" layer="37"/>
<rectangle x1="6.1214" y1="5.0546" x2="6.8326" y2="5.1054" layer="37"/>
<rectangle x1="7.8994" y1="5.0546" x2="8.5598" y2="5.1054" layer="37"/>
<rectangle x1="9.6266" y1="5.0546" x2="10.1854" y2="5.1054" layer="37"/>
<rectangle x1="10.8966" y1="5.0546" x2="11.303" y2="5.1054" layer="37"/>
<rectangle x1="8.5598" y1="5.0546" x2="8.6106" y2="5.1054" layer="37"/>
<rectangle x1="6.0706" y1="5.0546" x2="6.1214" y2="5.1054" layer="37"/>
<rectangle x1="10.8458" y1="5.0546" x2="10.8966" y2="5.1054" layer="37"/>
<rectangle x1="2.921" y1="5.1054" x2="3.429" y2="5.1562" layer="37"/>
<rectangle x1="4.191" y1="5.1054" x2="4.699" y2="5.1562" layer="37"/>
<rectangle x1="6.0706" y1="5.1054" x2="6.7818" y2="5.1562" layer="37"/>
<rectangle x1="7.9502" y1="5.1054" x2="8.6106" y2="5.1562" layer="37"/>
<rectangle x1="9.6266" y1="5.1054" x2="10.1854" y2="5.1562" layer="37"/>
<rectangle x1="10.8966" y1="5.1054" x2="11.303" y2="5.1562" layer="37"/>
<rectangle x1="8.6106" y1="5.1054" x2="8.6614" y2="5.1562" layer="37"/>
<rectangle x1="7.8994" y1="5.1054" x2="7.9502" y2="5.1562" layer="37"/>
<rectangle x1="10.8458" y1="5.1054" x2="10.8966" y2="5.1562" layer="37"/>
<rectangle x1="2.921" y1="5.1562" x2="3.429" y2="5.207" layer="37"/>
<rectangle x1="4.191" y1="5.1562" x2="4.699" y2="5.207" layer="37"/>
<rectangle x1="6.0706" y1="5.1562" x2="6.731" y2="5.207" layer="37"/>
<rectangle x1="8.001" y1="5.1562" x2="8.6614" y2="5.207" layer="37"/>
<rectangle x1="9.6266" y1="5.1562" x2="10.1854" y2="5.207" layer="37"/>
<rectangle x1="10.8966" y1="5.1562" x2="11.303" y2="5.207" layer="37"/>
<rectangle x1="6.0198" y1="5.1562" x2="6.0706" y2="5.207" layer="37"/>
<rectangle x1="8.6614" y1="5.1562" x2="8.7122" y2="5.207" layer="37"/>
<rectangle x1="7.9502" y1="5.1562" x2="8.001" y2="5.207" layer="37"/>
<rectangle x1="10.8458" y1="5.1562" x2="10.8966" y2="5.207" layer="37"/>
<rectangle x1="2.921" y1="5.207" x2="3.429" y2="5.2578" layer="37"/>
<rectangle x1="4.191" y1="5.207" x2="4.699" y2="5.2578" layer="37"/>
<rectangle x1="6.0198" y1="5.207" x2="6.6802" y2="5.2578" layer="37"/>
<rectangle x1="8.0518" y1="5.207" x2="8.7122" y2="5.2578" layer="37"/>
<rectangle x1="9.6266" y1="5.207" x2="10.1854" y2="5.2578" layer="37"/>
<rectangle x1="10.8966" y1="5.207" x2="11.303" y2="5.2578" layer="37"/>
<rectangle x1="5.969" y1="5.207" x2="6.0198" y2="5.2578" layer="37"/>
<rectangle x1="8.7122" y1="5.207" x2="8.763" y2="5.2578" layer="37"/>
<rectangle x1="8.001" y1="5.207" x2="8.0518" y2="5.2578" layer="37"/>
<rectangle x1="10.8458" y1="5.207" x2="10.8966" y2="5.2578" layer="37"/>
<rectangle x1="2.921" y1="5.2578" x2="3.429" y2="5.3086" layer="37"/>
<rectangle x1="4.191" y1="5.2578" x2="4.699" y2="5.3086" layer="37"/>
<rectangle x1="5.969" y1="5.2578" x2="6.6294" y2="5.3086" layer="37"/>
<rectangle x1="8.1026" y1="5.2578" x2="8.763" y2="5.3086" layer="37"/>
<rectangle x1="9.6266" y1="5.2578" x2="10.1854" y2="5.3086" layer="37"/>
<rectangle x1="10.8966" y1="5.2578" x2="11.303" y2="5.3086" layer="37"/>
<rectangle x1="5.9182" y1="5.2578" x2="5.969" y2="5.3086" layer="37"/>
<rectangle x1="8.0518" y1="5.2578" x2="8.1026" y2="5.3086" layer="37"/>
<rectangle x1="8.763" y1="5.2578" x2="8.8138" y2="5.3086" layer="37"/>
<rectangle x1="6.6294" y1="5.2578" x2="6.6802" y2="5.3086" layer="37"/>
<rectangle x1="10.8458" y1="5.2578" x2="10.8966" y2="5.3086" layer="37"/>
<rectangle x1="2.921" y1="5.3086" x2="3.429" y2="5.3594" layer="37"/>
<rectangle x1="4.191" y1="5.3086" x2="4.699" y2="5.3594" layer="37"/>
<rectangle x1="5.9182" y1="5.3086" x2="6.5786" y2="5.3594" layer="37"/>
<rectangle x1="8.1534" y1="5.3086" x2="8.8138" y2="5.3594" layer="37"/>
<rectangle x1="9.6266" y1="5.3086" x2="10.1854" y2="5.3594" layer="37"/>
<rectangle x1="10.8966" y1="5.3086" x2="11.303" y2="5.3594" layer="37"/>
<rectangle x1="5.8674" y1="5.3086" x2="5.9182" y2="5.3594" layer="37"/>
<rectangle x1="8.1026" y1="5.3086" x2="8.1534" y2="5.3594" layer="37"/>
<rectangle x1="8.8138" y1="5.3086" x2="8.8646" y2="5.3594" layer="37"/>
<rectangle x1="6.5786" y1="5.3086" x2="6.6294" y2="5.3594" layer="37"/>
<rectangle x1="10.8458" y1="5.3086" x2="10.8966" y2="5.3594" layer="37"/>
<rectangle x1="2.921" y1="5.3594" x2="3.429" y2="5.4102" layer="37"/>
<rectangle x1="4.191" y1="5.3594" x2="4.699" y2="5.4102" layer="37"/>
<rectangle x1="5.8674" y1="5.3594" x2="6.5278" y2="5.4102" layer="37"/>
<rectangle x1="8.2042" y1="5.3594" x2="8.8646" y2="5.4102" layer="37"/>
<rectangle x1="9.6266" y1="5.3594" x2="10.1854" y2="5.4102" layer="37"/>
<rectangle x1="10.8966" y1="5.3594" x2="11.303" y2="5.4102" layer="37"/>
<rectangle x1="5.8166" y1="5.3594" x2="5.8674" y2="5.4102" layer="37"/>
<rectangle x1="8.1534" y1="5.3594" x2="8.2042" y2="5.4102" layer="37"/>
<rectangle x1="8.8646" y1="5.3594" x2="8.9154" y2="5.4102" layer="37"/>
<rectangle x1="6.5278" y1="5.3594" x2="6.5786" y2="5.4102" layer="37"/>
<rectangle x1="10.8458" y1="5.3594" x2="10.8966" y2="5.4102" layer="37"/>
<rectangle x1="2.921" y1="5.4102" x2="3.429" y2="5.461" layer="37"/>
<rectangle x1="4.191" y1="5.4102" x2="4.699" y2="5.461" layer="37"/>
<rectangle x1="5.8166" y1="5.4102" x2="6.477" y2="5.461" layer="37"/>
<rectangle x1="8.255" y1="5.4102" x2="8.9154" y2="5.461" layer="37"/>
<rectangle x1="9.6266" y1="5.4102" x2="10.1854" y2="5.461" layer="37"/>
<rectangle x1="10.8966" y1="5.4102" x2="11.303" y2="5.461" layer="37"/>
<rectangle x1="8.2042" y1="5.4102" x2="8.255" y2="5.461" layer="37"/>
<rectangle x1="5.7658" y1="5.4102" x2="5.8166" y2="5.461" layer="37"/>
<rectangle x1="6.477" y1="5.4102" x2="6.5278" y2="5.461" layer="37"/>
<rectangle x1="8.9154" y1="5.4102" x2="8.9662" y2="5.461" layer="37"/>
<rectangle x1="10.8458" y1="5.4102" x2="10.8966" y2="5.461" layer="37"/>
<rectangle x1="2.921" y1="5.461" x2="3.429" y2="5.5118" layer="37"/>
<rectangle x1="4.191" y1="5.461" x2="4.699" y2="5.5118" layer="37"/>
<rectangle x1="5.7658" y1="5.461" x2="6.4262" y2="5.5118" layer="37"/>
<rectangle x1="8.255" y1="5.461" x2="8.9662" y2="5.5118" layer="37"/>
<rectangle x1="9.6266" y1="5.461" x2="10.1854" y2="5.5118" layer="37"/>
<rectangle x1="10.8966" y1="5.461" x2="11.303" y2="5.5118" layer="37"/>
<rectangle x1="5.715" y1="5.461" x2="5.7658" y2="5.5118" layer="37"/>
<rectangle x1="6.4262" y1="5.461" x2="6.477" y2="5.5118" layer="37"/>
<rectangle x1="10.8458" y1="5.461" x2="10.8966" y2="5.5118" layer="37"/>
<rectangle x1="2.921" y1="5.5118" x2="3.429" y2="5.5626" layer="37"/>
<rectangle x1="4.191" y1="5.5118" x2="4.699" y2="5.5626" layer="37"/>
<rectangle x1="5.715" y1="5.5118" x2="6.3754" y2="5.5626" layer="37"/>
<rectangle x1="8.3058" y1="5.5118" x2="9.017" y2="5.5626" layer="37"/>
<rectangle x1="9.6266" y1="5.5118" x2="10.1854" y2="5.5626" layer="37"/>
<rectangle x1="10.8966" y1="5.5118" x2="11.303" y2="5.5626" layer="37"/>
<rectangle x1="5.6642" y1="5.5118" x2="5.715" y2="5.5626" layer="37"/>
<rectangle x1="6.3754" y1="5.5118" x2="6.4262" y2="5.5626" layer="37"/>
<rectangle x1="10.8458" y1="5.5118" x2="10.8966" y2="5.5626" layer="37"/>
<rectangle x1="2.921" y1="5.5626" x2="3.429" y2="5.6134" layer="37"/>
<rectangle x1="4.191" y1="5.5626" x2="4.699" y2="5.6134" layer="37"/>
<rectangle x1="5.6642" y1="5.5626" x2="6.3246" y2="5.6134" layer="37"/>
<rectangle x1="8.3566" y1="5.5626" x2="9.0678" y2="5.6134" layer="37"/>
<rectangle x1="9.6266" y1="5.5626" x2="10.1854" y2="5.6134" layer="37"/>
<rectangle x1="10.8966" y1="5.5626" x2="11.303" y2="5.6134" layer="37"/>
<rectangle x1="6.3246" y1="5.5626" x2="6.3754" y2="5.6134" layer="37"/>
<rectangle x1="5.6134" y1="5.5626" x2="5.6642" y2="5.6134" layer="37"/>
<rectangle x1="10.8458" y1="5.5626" x2="10.8966" y2="5.6134" layer="37"/>
<rectangle x1="2.921" y1="5.6134" x2="3.429" y2="5.6642" layer="37"/>
<rectangle x1="4.191" y1="5.6134" x2="4.699" y2="5.6642" layer="37"/>
<rectangle x1="5.6134" y1="5.6134" x2="6.2738" y2="5.6642" layer="37"/>
<rectangle x1="8.4074" y1="5.6134" x2="9.1186" y2="5.6642" layer="37"/>
<rectangle x1="9.6266" y1="5.6134" x2="10.1854" y2="5.6642" layer="37"/>
<rectangle x1="10.8966" y1="5.6134" x2="11.303" y2="5.6642" layer="37"/>
<rectangle x1="6.2738" y1="5.6134" x2="6.3246" y2="5.6642" layer="37"/>
<rectangle x1="5.5626" y1="5.6134" x2="5.6134" y2="5.6642" layer="37"/>
<rectangle x1="10.8458" y1="5.6134" x2="10.8966" y2="5.6642" layer="37"/>
<rectangle x1="2.921" y1="5.6642" x2="3.429" y2="5.715" layer="37"/>
<rectangle x1="4.191" y1="5.6642" x2="4.699" y2="5.715" layer="37"/>
<rectangle x1="5.5626" y1="5.6642" x2="6.223" y2="5.715" layer="37"/>
<rectangle x1="8.4582" y1="5.6642" x2="9.1694" y2="5.715" layer="37"/>
<rectangle x1="9.6266" y1="5.6642" x2="10.1854" y2="5.715" layer="37"/>
<rectangle x1="10.8966" y1="5.6642" x2="11.303" y2="5.715" layer="37"/>
<rectangle x1="6.223" y1="5.6642" x2="6.2738" y2="5.715" layer="37"/>
<rectangle x1="5.5118" y1="5.6642" x2="5.5626" y2="5.715" layer="37"/>
<rectangle x1="10.8458" y1="5.6642" x2="10.8966" y2="5.715" layer="37"/>
<rectangle x1="8.4074" y1="5.6642" x2="8.4582" y2="5.715" layer="37"/>
<rectangle x1="2.921" y1="5.715" x2="3.429" y2="5.7658" layer="37"/>
<rectangle x1="4.191" y1="5.715" x2="4.699" y2="5.7658" layer="37"/>
<rectangle x1="5.5118" y1="5.715" x2="6.1722" y2="5.7658" layer="37"/>
<rectangle x1="8.509" y1="5.715" x2="9.1694" y2="5.7658" layer="37"/>
<rectangle x1="9.6266" y1="5.715" x2="10.1854" y2="5.7658" layer="37"/>
<rectangle x1="10.8966" y1="5.715" x2="11.303" y2="5.7658" layer="37"/>
<rectangle x1="9.1694" y1="5.715" x2="9.2202" y2="5.7658" layer="37"/>
<rectangle x1="6.1722" y1="5.715" x2="6.223" y2="5.7658" layer="37"/>
<rectangle x1="8.4582" y1="5.715" x2="8.509" y2="5.7658" layer="37"/>
<rectangle x1="5.461" y1="5.715" x2="5.5118" y2="5.7658" layer="37"/>
<rectangle x1="10.8458" y1="5.715" x2="10.8966" y2="5.7658" layer="37"/>
<rectangle x1="2.921" y1="5.7658" x2="3.429" y2="5.8166" layer="37"/>
<rectangle x1="4.191" y1="5.7658" x2="4.699" y2="5.8166" layer="37"/>
<rectangle x1="5.461" y1="5.7658" x2="6.1214" y2="5.8166" layer="37"/>
<rectangle x1="7.4422" y1="5.7658" x2="7.6454" y2="5.8166" layer="37"/>
<rectangle x1="8.5598" y1="5.7658" x2="9.2202" y2="5.8166" layer="37"/>
<rectangle x1="9.6266" y1="5.7658" x2="10.1854" y2="5.8166" layer="37"/>
<rectangle x1="10.8966" y1="5.7658" x2="11.303" y2="5.8166" layer="37"/>
<rectangle x1="6.1214" y1="5.7658" x2="6.1722" y2="5.8166" layer="37"/>
<rectangle x1="7.6454" y1="5.7658" x2="7.6962" y2="5.8166" layer="37"/>
<rectangle x1="9.2202" y1="5.7658" x2="9.271" y2="5.8166" layer="37"/>
<rectangle x1="7.3914" y1="5.7658" x2="7.4422" y2="5.8166" layer="37"/>
<rectangle x1="8.509" y1="5.7658" x2="8.5598" y2="5.8166" layer="37"/>
<rectangle x1="10.8458" y1="5.7658" x2="10.8966" y2="5.8166" layer="37"/>
<rectangle x1="5.4102" y1="5.7658" x2="5.461" y2="5.8166" layer="37"/>
<rectangle x1="2.921" y1="5.8166" x2="3.429" y2="5.8674" layer="37"/>
<rectangle x1="4.191" y1="5.8166" x2="4.699" y2="5.8674" layer="37"/>
<rectangle x1="5.4102" y1="5.8166" x2="6.0706" y2="5.8674" layer="37"/>
<rectangle x1="7.3914" y1="5.8166" x2="7.747" y2="5.8674" layer="37"/>
<rectangle x1="8.6106" y1="5.8166" x2="9.271" y2="5.8674" layer="37"/>
<rectangle x1="9.6266" y1="5.8166" x2="10.1854" y2="5.8674" layer="37"/>
<rectangle x1="10.8966" y1="5.8166" x2="11.303" y2="5.8674" layer="37"/>
<rectangle x1="6.0706" y1="5.8166" x2="6.1214" y2="5.8674" layer="37"/>
<rectangle x1="7.3406" y1="5.8166" x2="7.3914" y2="5.8674" layer="37"/>
<rectangle x1="9.271" y1="5.8166" x2="9.3218" y2="5.8674" layer="37"/>
<rectangle x1="8.5598" y1="5.8166" x2="8.6106" y2="5.8674" layer="37"/>
<rectangle x1="10.8458" y1="5.8166" x2="10.8966" y2="5.8674" layer="37"/>
<rectangle x1="2.921" y1="5.8674" x2="3.429" y2="5.9182" layer="37"/>
<rectangle x1="4.191" y1="5.8674" x2="4.699" y2="5.9182" layer="37"/>
<rectangle x1="5.3594" y1="5.8674" x2="6.0198" y2="5.9182" layer="37"/>
<rectangle x1="7.3406" y1="5.8674" x2="7.7978" y2="5.9182" layer="37"/>
<rectangle x1="8.6614" y1="5.8674" x2="9.3218" y2="5.9182" layer="37"/>
<rectangle x1="9.6266" y1="5.8674" x2="10.1854" y2="5.9182" layer="37"/>
<rectangle x1="10.8966" y1="5.8674" x2="11.303" y2="5.9182" layer="37"/>
<rectangle x1="6.0198" y1="5.8674" x2="6.0706" y2="5.9182" layer="37"/>
<rectangle x1="8.6106" y1="5.8674" x2="8.6614" y2="5.9182" layer="37"/>
<rectangle x1="7.2898" y1="5.8674" x2="7.3406" y2="5.9182" layer="37"/>
<rectangle x1="9.3218" y1="5.8674" x2="9.3726" y2="5.9182" layer="37"/>
<rectangle x1="10.8458" y1="5.8674" x2="10.8966" y2="5.9182" layer="37"/>
<rectangle x1="2.921" y1="5.9182" x2="3.429" y2="5.969" layer="37"/>
<rectangle x1="4.191" y1="5.9182" x2="4.699" y2="5.969" layer="37"/>
<rectangle x1="5.3594" y1="5.9182" x2="6.0198" y2="5.969" layer="37"/>
<rectangle x1="7.2898" y1="5.9182" x2="7.7978" y2="5.969" layer="37"/>
<rectangle x1="8.7122" y1="5.9182" x2="9.3218" y2="5.969" layer="37"/>
<rectangle x1="9.6266" y1="5.9182" x2="10.1854" y2="5.969" layer="37"/>
<rectangle x1="10.8966" y1="5.9182" x2="11.303" y2="5.969" layer="37"/>
<rectangle x1="9.3218" y1="5.9182" x2="9.3726" y2="5.969" layer="37"/>
<rectangle x1="8.6614" y1="5.9182" x2="8.7122" y2="5.969" layer="37"/>
<rectangle x1="5.3086" y1="5.9182" x2="5.3594" y2="5.969" layer="37"/>
<rectangle x1="7.239" y1="5.9182" x2="7.2898" y2="5.969" layer="37"/>
<rectangle x1="10.8458" y1="5.9182" x2="10.8966" y2="5.969" layer="37"/>
<rectangle x1="2.921" y1="5.969" x2="3.429" y2="6.0198" layer="37"/>
<rectangle x1="4.191" y1="5.969" x2="4.699" y2="6.0198" layer="37"/>
<rectangle x1="5.3086" y1="5.969" x2="5.969" y2="6.0198" layer="37"/>
<rectangle x1="7.239" y1="5.969" x2="7.7978" y2="6.0198" layer="37"/>
<rectangle x1="8.7122" y1="5.969" x2="9.3726" y2="6.0198" layer="37"/>
<rectangle x1="9.6266" y1="5.969" x2="10.1854" y2="6.0198" layer="37"/>
<rectangle x1="10.8966" y1="5.969" x2="11.303" y2="6.0198" layer="37"/>
<rectangle x1="7.7978" y1="5.969" x2="7.8486" y2="6.0198" layer="37"/>
<rectangle x1="5.969" y1="5.969" x2="6.0198" y2="6.0198" layer="37"/>
<rectangle x1="10.8458" y1="5.969" x2="10.8966" y2="6.0198" layer="37"/>
<rectangle x1="2.921" y1="6.0198" x2="3.429" y2="6.0706" layer="37"/>
<rectangle x1="4.191" y1="6.0198" x2="4.699" y2="6.0706" layer="37"/>
<rectangle x1="5.3086" y1="6.0198" x2="5.969" y2="6.0706" layer="37"/>
<rectangle x1="7.1882" y1="6.0198" x2="7.7978" y2="6.0706" layer="37"/>
<rectangle x1="8.7122" y1="6.0198" x2="9.3726" y2="6.0706" layer="37"/>
<rectangle x1="9.6266" y1="6.0198" x2="10.1854" y2="6.0706" layer="37"/>
<rectangle x1="10.8966" y1="6.0198" x2="11.303" y2="6.0706" layer="37"/>
<rectangle x1="7.7978" y1="6.0198" x2="7.8486" y2="6.0706" layer="37"/>
<rectangle x1="10.8458" y1="6.0198" x2="10.8966" y2="6.0706" layer="37"/>
<rectangle x1="2.921" y1="6.0706" x2="3.429" y2="6.1214" layer="37"/>
<rectangle x1="4.191" y1="6.0706" x2="4.699" y2="6.1214" layer="37"/>
<rectangle x1="5.3594" y1="6.0706" x2="5.969" y2="6.1214" layer="37"/>
<rectangle x1="7.1882" y1="6.0706" x2="7.7978" y2="6.1214" layer="37"/>
<rectangle x1="8.7122" y1="6.0706" x2="9.3726" y2="6.1214" layer="37"/>
<rectangle x1="9.6266" y1="6.0706" x2="10.1854" y2="6.1214" layer="37"/>
<rectangle x1="10.8966" y1="6.0706" x2="11.303" y2="6.1214" layer="37"/>
<rectangle x1="7.1374" y1="6.0706" x2="7.1882" y2="6.1214" layer="37"/>
<rectangle x1="5.3086" y1="6.0706" x2="5.3594" y2="6.1214" layer="37"/>
<rectangle x1="5.969" y1="6.0706" x2="6.0198" y2="6.1214" layer="37"/>
<rectangle x1="10.8458" y1="6.0706" x2="10.8966" y2="6.1214" layer="37"/>
<rectangle x1="2.921" y1="6.1214" x2="3.429" y2="6.1722" layer="37"/>
<rectangle x1="4.191" y1="6.1214" x2="4.699" y2="6.1722" layer="37"/>
<rectangle x1="5.3594" y1="6.1214" x2="6.0198" y2="6.1722" layer="37"/>
<rectangle x1="7.1374" y1="6.1214" x2="7.7978" y2="6.1722" layer="37"/>
<rectangle x1="8.7122" y1="6.1214" x2="9.3218" y2="6.1722" layer="37"/>
<rectangle x1="9.6266" y1="6.1214" x2="10.1854" y2="6.1722" layer="37"/>
<rectangle x1="10.8966" y1="6.1214" x2="11.303" y2="6.1722" layer="37"/>
<rectangle x1="7.0866" y1="6.1214" x2="7.1374" y2="6.1722" layer="37"/>
<rectangle x1="8.6614" y1="6.1214" x2="8.7122" y2="6.1722" layer="37"/>
<rectangle x1="9.3218" y1="6.1214" x2="9.3726" y2="6.1722" layer="37"/>
<rectangle x1="6.0198" y1="6.1214" x2="6.0706" y2="6.1722" layer="37"/>
<rectangle x1="10.8458" y1="6.1214" x2="10.8966" y2="6.1722" layer="37"/>
<rectangle x1="2.921" y1="6.1722" x2="3.429" y2="6.223" layer="37"/>
<rectangle x1="4.191" y1="6.1722" x2="4.699" y2="6.223" layer="37"/>
<rectangle x1="5.4102" y1="6.1722" x2="6.0198" y2="6.223" layer="37"/>
<rectangle x1="7.0866" y1="6.1722" x2="7.747" y2="6.223" layer="37"/>
<rectangle x1="8.6614" y1="6.1722" x2="9.271" y2="6.223" layer="37"/>
<rectangle x1="9.6266" y1="6.1722" x2="10.1854" y2="6.223" layer="37"/>
<rectangle x1="10.8966" y1="6.1722" x2="11.303" y2="6.223" layer="37"/>
<rectangle x1="6.0198" y1="6.1722" x2="6.0706" y2="6.223" layer="37"/>
<rectangle x1="9.271" y1="6.1722" x2="9.3218" y2="6.223" layer="37"/>
<rectangle x1="5.3594" y1="6.1722" x2="5.4102" y2="6.223" layer="37"/>
<rectangle x1="7.0358" y1="6.1722" x2="7.0866" y2="6.223" layer="37"/>
<rectangle x1="8.6106" y1="6.1722" x2="8.6614" y2="6.223" layer="37"/>
<rectangle x1="6.0706" y1="6.1722" x2="6.1214" y2="6.223" layer="37"/>
<rectangle x1="10.8458" y1="6.1722" x2="10.8966" y2="6.223" layer="37"/>
<rectangle x1="2.921" y1="6.223" x2="3.429" y2="6.2738" layer="37"/>
<rectangle x1="4.191" y1="6.223" x2="4.699" y2="6.2738" layer="37"/>
<rectangle x1="5.4102" y1="6.223" x2="6.0706" y2="6.2738" layer="37"/>
<rectangle x1="7.0866" y1="6.223" x2="7.6962" y2="6.2738" layer="37"/>
<rectangle x1="8.6106" y1="6.223" x2="9.271" y2="6.2738" layer="37"/>
<rectangle x1="9.6266" y1="6.223" x2="10.1854" y2="6.2738" layer="37"/>
<rectangle x1="10.8966" y1="6.223" x2="11.303" y2="6.2738" layer="37"/>
<rectangle x1="6.0706" y1="6.223" x2="6.1214" y2="6.2738" layer="37"/>
<rectangle x1="7.0358" y1="6.223" x2="7.0866" y2="6.2738" layer="37"/>
<rectangle x1="7.6962" y1="6.223" x2="7.747" y2="6.2738" layer="37"/>
<rectangle x1="8.5598" y1="6.223" x2="8.6106" y2="6.2738" layer="37"/>
<rectangle x1="6.985" y1="6.223" x2="7.0358" y2="6.2738" layer="37"/>
<rectangle x1="10.8458" y1="6.223" x2="10.8966" y2="6.2738" layer="37"/>
<rectangle x1="2.921" y1="6.2738" x2="3.429" y2="6.3246" layer="37"/>
<rectangle x1="4.191" y1="6.2738" x2="4.699" y2="6.3246" layer="37"/>
<rectangle x1="5.461" y1="6.2738" x2="6.1214" y2="6.3246" layer="37"/>
<rectangle x1="7.0358" y1="6.2738" x2="7.6454" y2="6.3246" layer="37"/>
<rectangle x1="8.5598" y1="6.2738" x2="9.2202" y2="6.3246" layer="37"/>
<rectangle x1="9.6266" y1="6.2738" x2="10.1854" y2="6.3246" layer="37"/>
<rectangle x1="10.8966" y1="6.2738" x2="11.303" y2="6.3246" layer="37"/>
<rectangle x1="6.1214" y1="6.2738" x2="6.1722" y2="6.3246" layer="37"/>
<rectangle x1="6.985" y1="6.2738" x2="7.0358" y2="6.3246" layer="37"/>
<rectangle x1="7.6454" y1="6.2738" x2="7.6962" y2="6.3246" layer="37"/>
<rectangle x1="9.2202" y1="6.2738" x2="9.271" y2="6.3246" layer="37"/>
<rectangle x1="10.8458" y1="6.2738" x2="10.8966" y2="6.3246" layer="37"/>
<rectangle x1="2.921" y1="6.3246" x2="3.429" y2="6.3754" layer="37"/>
<rectangle x1="4.191" y1="6.3246" x2="4.699" y2="6.3754" layer="37"/>
<rectangle x1="5.5626" y1="6.3246" x2="6.1722" y2="6.3754" layer="37"/>
<rectangle x1="6.985" y1="6.3246" x2="7.5946" y2="6.3754" layer="37"/>
<rectangle x1="8.5598" y1="6.3246" x2="9.1694" y2="6.3754" layer="37"/>
<rectangle x1="9.6266" y1="6.3246" x2="10.1854" y2="6.3754" layer="37"/>
<rectangle x1="10.8966" y1="6.3246" x2="11.303" y2="6.3754" layer="37"/>
<rectangle x1="5.5118" y1="6.3246" x2="5.5626" y2="6.3754" layer="37"/>
<rectangle x1="6.1722" y1="6.3246" x2="6.223" y2="6.3754" layer="37"/>
<rectangle x1="6.9342" y1="6.3246" x2="6.985" y2="6.3754" layer="37"/>
<rectangle x1="8.509" y1="6.3246" x2="8.5598" y2="6.3754" layer="37"/>
<rectangle x1="7.5946" y1="6.3246" x2="7.6454" y2="6.3754" layer="37"/>
<rectangle x1="9.1694" y1="6.3246" x2="9.2202" y2="6.3754" layer="37"/>
<rectangle x1="10.8458" y1="6.3246" x2="10.8966" y2="6.3754" layer="37"/>
<rectangle x1="2.921" y1="6.3754" x2="3.429" y2="6.4262" layer="37"/>
<rectangle x1="4.191" y1="6.3754" x2="4.699" y2="6.4262" layer="37"/>
<rectangle x1="5.6134" y1="6.3754" x2="6.223" y2="6.4262" layer="37"/>
<rectangle x1="6.9342" y1="6.3754" x2="7.5438" y2="6.4262" layer="37"/>
<rectangle x1="8.4582" y1="6.3754" x2="9.1186" y2="6.4262" layer="37"/>
<rectangle x1="9.6266" y1="6.3754" x2="10.1854" y2="6.4262" layer="37"/>
<rectangle x1="10.8966" y1="6.3754" x2="11.303" y2="6.4262" layer="37"/>
<rectangle x1="5.5626" y1="6.3754" x2="5.6134" y2="6.4262" layer="37"/>
<rectangle x1="6.223" y1="6.3754" x2="6.2738" y2="6.4262" layer="37"/>
<rectangle x1="7.5438" y1="6.3754" x2="7.5946" y2="6.4262" layer="37"/>
<rectangle x1="9.1186" y1="6.3754" x2="9.1694" y2="6.4262" layer="37"/>
<rectangle x1="6.8834" y1="6.3754" x2="6.9342" y2="6.4262" layer="37"/>
<rectangle x1="10.8458" y1="6.3754" x2="10.8966" y2="6.4262" layer="37"/>
<rectangle x1="2.921" y1="6.4262" x2="3.429" y2="6.477" layer="37"/>
<rectangle x1="4.191" y1="6.4262" x2="4.699" y2="6.477" layer="37"/>
<rectangle x1="5.6134" y1="6.4262" x2="6.2738" y2="6.477" layer="37"/>
<rectangle x1="6.8834" y1="6.4262" x2="7.493" y2="6.477" layer="37"/>
<rectangle x1="8.4074" y1="6.4262" x2="9.0678" y2="6.477" layer="37"/>
<rectangle x1="9.6266" y1="6.4262" x2="10.1854" y2="6.477" layer="37"/>
<rectangle x1="10.8966" y1="6.4262" x2="11.303" y2="6.477" layer="37"/>
<rectangle x1="7.493" y1="6.4262" x2="7.5438" y2="6.477" layer="37"/>
<rectangle x1="9.0678" y1="6.4262" x2="9.1186" y2="6.477" layer="37"/>
<rectangle x1="6.2738" y1="6.4262" x2="6.3246" y2="6.477" layer="37"/>
<rectangle x1="6.8326" y1="6.4262" x2="6.8834" y2="6.477" layer="37"/>
<rectangle x1="10.8458" y1="6.4262" x2="10.8966" y2="6.477" layer="37"/>
<rectangle x1="2.921" y1="6.477" x2="3.429" y2="6.5278" layer="37"/>
<rectangle x1="4.191" y1="6.477" x2="4.699" y2="6.5278" layer="37"/>
<rectangle x1="5.6642" y1="6.477" x2="6.3246" y2="6.5278" layer="37"/>
<rectangle x1="6.8326" y1="6.477" x2="7.493" y2="6.5278" layer="37"/>
<rectangle x1="8.4074" y1="6.477" x2="9.0678" y2="6.5278" layer="37"/>
<rectangle x1="9.6266" y1="6.477" x2="10.1854" y2="6.5278" layer="37"/>
<rectangle x1="10.8966" y1="6.477" x2="11.303" y2="6.5278" layer="37"/>
<rectangle x1="8.3566" y1="6.477" x2="8.4074" y2="6.5278" layer="37"/>
<rectangle x1="6.3246" y1="6.477" x2="6.3754" y2="6.5278" layer="37"/>
<rectangle x1="6.7818" y1="6.477" x2="6.8326" y2="6.5278" layer="37"/>
<rectangle x1="5.6134" y1="6.477" x2="5.6642" y2="6.5278" layer="37"/>
<rectangle x1="10.8458" y1="6.477" x2="10.8966" y2="6.5278" layer="37"/>
<rectangle x1="2.921" y1="6.5278" x2="3.429" y2="6.5786" layer="37"/>
<rectangle x1="4.191" y1="6.5278" x2="4.699" y2="6.5786" layer="37"/>
<rectangle x1="5.715" y1="6.5278" x2="6.3754" y2="6.5786" layer="37"/>
<rectangle x1="6.7818" y1="6.5278" x2="7.4422" y2="6.5786" layer="37"/>
<rectangle x1="8.3566" y1="6.5278" x2="9.017" y2="6.5786" layer="37"/>
<rectangle x1="9.6266" y1="6.5278" x2="10.1854" y2="6.5786" layer="37"/>
<rectangle x1="10.8966" y1="6.5278" x2="11.303" y2="6.5786" layer="37"/>
<rectangle x1="6.3754" y1="6.5278" x2="6.4262" y2="6.5786" layer="37"/>
<rectangle x1="8.3058" y1="6.5278" x2="8.3566" y2="6.5786" layer="37"/>
<rectangle x1="5.6642" y1="6.5278" x2="5.715" y2="6.5786" layer="37"/>
<rectangle x1="7.4422" y1="6.5278" x2="7.493" y2="6.5786" layer="37"/>
<rectangle x1="10.8458" y1="6.5278" x2="10.8966" y2="6.5786" layer="37"/>
<rectangle x1="2.921" y1="6.5786" x2="3.429" y2="6.6294" layer="37"/>
<rectangle x1="4.191" y1="6.5786" x2="4.699" y2="6.6294" layer="37"/>
<rectangle x1="5.7658" y1="6.5786" x2="6.4262" y2="6.6294" layer="37"/>
<rectangle x1="6.731" y1="6.5786" x2="7.3914" y2="6.6294" layer="37"/>
<rectangle x1="8.3058" y1="6.5786" x2="8.9662" y2="6.6294" layer="37"/>
<rectangle x1="9.6266" y1="6.5786" x2="10.1854" y2="6.6294" layer="37"/>
<rectangle x1="10.8966" y1="6.5786" x2="11.303" y2="6.6294" layer="37"/>
<rectangle x1="7.3914" y1="6.5786" x2="7.4422" y2="6.6294" layer="37"/>
<rectangle x1="5.715" y1="6.5786" x2="5.7658" y2="6.6294" layer="37"/>
<rectangle x1="6.4262" y1="6.5786" x2="6.477" y2="6.6294" layer="37"/>
<rectangle x1="8.9662" y1="6.5786" x2="9.017" y2="6.6294" layer="37"/>
<rectangle x1="10.8458" y1="6.5786" x2="10.8966" y2="6.6294" layer="37"/>
<rectangle x1="2.921" y1="6.6294" x2="3.429" y2="6.6802" layer="37"/>
<rectangle x1="4.191" y1="6.6294" x2="4.699" y2="6.6802" layer="37"/>
<rectangle x1="5.8166" y1="6.6294" x2="6.477" y2="6.6802" layer="37"/>
<rectangle x1="6.6802" y1="6.6294" x2="7.3406" y2="6.6802" layer="37"/>
<rectangle x1="8.255" y1="6.6294" x2="8.9154" y2="6.6802" layer="37"/>
<rectangle x1="9.6266" y1="6.6294" x2="10.1854" y2="6.6802" layer="37"/>
<rectangle x1="10.8966" y1="6.6294" x2="11.303" y2="6.6802" layer="37"/>
<rectangle x1="7.3406" y1="6.6294" x2="7.3914" y2="6.6802" layer="37"/>
<rectangle x1="5.7658" y1="6.6294" x2="5.8166" y2="6.6802" layer="37"/>
<rectangle x1="8.9154" y1="6.6294" x2="8.9662" y2="6.6802" layer="37"/>
<rectangle x1="10.8458" y1="6.6294" x2="10.8966" y2="6.6802" layer="37"/>
<rectangle x1="2.921" y1="6.6802" x2="3.429" y2="6.731" layer="37"/>
<rectangle x1="4.191" y1="6.6802" x2="4.699" y2="6.731" layer="37"/>
<rectangle x1="5.8674" y1="6.6802" x2="6.5278" y2="6.731" layer="37"/>
<rectangle x1="6.6802" y1="6.6802" x2="7.2898" y2="6.731" layer="37"/>
<rectangle x1="8.2042" y1="6.6802" x2="8.8646" y2="6.731" layer="37"/>
<rectangle x1="9.6266" y1="6.6802" x2="10.1854" y2="6.731" layer="37"/>
<rectangle x1="10.8966" y1="6.6802" x2="11.303" y2="6.731" layer="37"/>
<rectangle x1="7.2898" y1="6.6802" x2="7.3406" y2="6.731" layer="37"/>
<rectangle x1="5.8166" y1="6.6802" x2="5.8674" y2="6.731" layer="37"/>
<rectangle x1="6.6294" y1="6.6802" x2="6.6802" y2="6.731" layer="37"/>
<rectangle x1="8.8646" y1="6.6802" x2="8.9154" y2="6.731" layer="37"/>
<rectangle x1="10.8458" y1="6.6802" x2="10.8966" y2="6.731" layer="37"/>
<rectangle x1="2.921" y1="6.731" x2="3.429" y2="6.7818" layer="37"/>
<rectangle x1="4.191" y1="6.731" x2="4.699" y2="6.7818" layer="37"/>
<rectangle x1="5.9182" y1="6.731" x2="7.239" y2="6.7818" layer="37"/>
<rectangle x1="8.2042" y1="6.731" x2="8.8138" y2="6.7818" layer="37"/>
<rectangle x1="9.6266" y1="6.731" x2="10.1854" y2="6.7818" layer="37"/>
<rectangle x1="10.8966" y1="6.731" x2="11.303" y2="6.7818" layer="37"/>
<rectangle x1="7.239" y1="6.731" x2="7.2898" y2="6.7818" layer="37"/>
<rectangle x1="8.1534" y1="6.731" x2="8.2042" y2="6.7818" layer="37"/>
<rectangle x1="5.8674" y1="6.731" x2="5.9182" y2="6.7818" layer="37"/>
<rectangle x1="8.8138" y1="6.731" x2="8.8646" y2="6.7818" layer="37"/>
<rectangle x1="10.8458" y1="6.731" x2="10.8966" y2="6.7818" layer="37"/>
<rectangle x1="2.921" y1="6.7818" x2="3.429" y2="6.8326" layer="37"/>
<rectangle x1="4.191" y1="6.7818" x2="4.699" y2="6.8326" layer="37"/>
<rectangle x1="5.969" y1="6.7818" x2="7.239" y2="6.8326" layer="37"/>
<rectangle x1="8.1534" y1="6.7818" x2="8.8138" y2="6.8326" layer="37"/>
<rectangle x1="9.6266" y1="6.7818" x2="10.1854" y2="6.8326" layer="37"/>
<rectangle x1="10.8966" y1="6.7818" x2="11.303" y2="6.8326" layer="37"/>
<rectangle x1="5.9182" y1="6.7818" x2="5.969" y2="6.8326" layer="37"/>
<rectangle x1="8.1026" y1="6.7818" x2="8.1534" y2="6.8326" layer="37"/>
<rectangle x1="10.8458" y1="6.7818" x2="10.8966" y2="6.8326" layer="37"/>
<rectangle x1="2.921" y1="6.8326" x2="3.429" y2="6.8834" layer="37"/>
<rectangle x1="4.191" y1="6.8326" x2="4.699" y2="6.8834" layer="37"/>
<rectangle x1="6.0198" y1="6.8326" x2="7.1882" y2="6.8834" layer="37"/>
<rectangle x1="8.1026" y1="6.8326" x2="8.763" y2="6.8834" layer="37"/>
<rectangle x1="9.6266" y1="6.8326" x2="10.1854" y2="6.8834" layer="37"/>
<rectangle x1="10.8966" y1="6.8326" x2="11.303" y2="6.8834" layer="37"/>
<rectangle x1="5.969" y1="6.8326" x2="6.0198" y2="6.8834" layer="37"/>
<rectangle x1="7.1882" y1="6.8326" x2="7.239" y2="6.8834" layer="37"/>
<rectangle x1="8.0518" y1="6.8326" x2="8.1026" y2="6.8834" layer="37"/>
<rectangle x1="10.8458" y1="6.8326" x2="10.8966" y2="6.8834" layer="37"/>
<rectangle x1="2.921" y1="6.8834" x2="3.429" y2="6.9342" layer="37"/>
<rectangle x1="4.191" y1="6.8834" x2="4.699" y2="6.9342" layer="37"/>
<rectangle x1="6.0198" y1="6.8834" x2="7.1374" y2="6.9342" layer="37"/>
<rectangle x1="8.0518" y1="6.8834" x2="8.7122" y2="6.9342" layer="37"/>
<rectangle x1="9.6266" y1="6.8834" x2="10.1854" y2="6.9342" layer="37"/>
<rectangle x1="10.8966" y1="6.8834" x2="11.303" y2="6.9342" layer="37"/>
<rectangle x1="7.1374" y1="6.8834" x2="7.1882" y2="6.9342" layer="37"/>
<rectangle x1="8.7122" y1="6.8834" x2="8.763" y2="6.9342" layer="37"/>
<rectangle x1="10.8458" y1="6.8834" x2="10.8966" y2="6.9342" layer="37"/>
<rectangle x1="5.969" y1="6.8834" x2="6.0198" y2="6.9342" layer="37"/>
<rectangle x1="2.921" y1="6.9342" x2="3.429" y2="6.985" layer="37"/>
<rectangle x1="4.191" y1="6.9342" x2="4.699" y2="6.985" layer="37"/>
<rectangle x1="6.0706" y1="6.9342" x2="7.0866" y2="6.985" layer="37"/>
<rectangle x1="8.0518" y1="6.9342" x2="8.6614" y2="6.985" layer="37"/>
<rectangle x1="9.6266" y1="6.9342" x2="10.1854" y2="6.985" layer="37"/>
<rectangle x1="10.8966" y1="6.9342" x2="11.303" y2="6.985" layer="37"/>
<rectangle x1="7.0866" y1="6.9342" x2="7.1374" y2="6.985" layer="37"/>
<rectangle x1="8.001" y1="6.9342" x2="8.0518" y2="6.985" layer="37"/>
<rectangle x1="8.6614" y1="6.9342" x2="8.7122" y2="6.985" layer="37"/>
<rectangle x1="6.0198" y1="6.9342" x2="6.0706" y2="6.985" layer="37"/>
<rectangle x1="10.8458" y1="6.9342" x2="10.8966" y2="6.985" layer="37"/>
<rectangle x1="2.921" y1="6.985" x2="3.429" y2="7.0358" layer="37"/>
<rectangle x1="4.191" y1="6.985" x2="4.699" y2="7.0358" layer="37"/>
<rectangle x1="6.1214" y1="6.985" x2="7.0866" y2="7.0358" layer="37"/>
<rectangle x1="8.001" y1="6.985" x2="8.6106" y2="7.0358" layer="37"/>
<rectangle x1="9.6266" y1="6.985" x2="10.1854" y2="7.0358" layer="37"/>
<rectangle x1="10.8966" y1="6.985" x2="11.303" y2="7.0358" layer="37"/>
<rectangle x1="7.9502" y1="6.985" x2="8.001" y2="7.0358" layer="37"/>
<rectangle x1="8.6106" y1="6.985" x2="8.6614" y2="7.0358" layer="37"/>
<rectangle x1="6.0706" y1="6.985" x2="6.1214" y2="7.0358" layer="37"/>
<rectangle x1="10.8458" y1="6.985" x2="10.8966" y2="7.0358" layer="37"/>
<rectangle x1="2.921" y1="7.0358" x2="3.429" y2="7.0866" layer="37"/>
<rectangle x1="4.191" y1="7.0358" x2="4.699" y2="7.0866" layer="37"/>
<rectangle x1="6.1722" y1="7.0358" x2="7.0358" y2="7.0866" layer="37"/>
<rectangle x1="7.9502" y1="7.0358" x2="8.5598" y2="7.0866" layer="37"/>
<rectangle x1="9.6266" y1="7.0358" x2="10.1854" y2="7.0866" layer="37"/>
<rectangle x1="10.8966" y1="7.0358" x2="11.303" y2="7.0866" layer="37"/>
<rectangle x1="8.5598" y1="7.0358" x2="8.6106" y2="7.0866" layer="37"/>
<rectangle x1="6.1214" y1="7.0358" x2="6.1722" y2="7.0866" layer="37"/>
<rectangle x1="7.8994" y1="7.0358" x2="7.9502" y2="7.0866" layer="37"/>
<rectangle x1="10.8458" y1="7.0358" x2="10.8966" y2="7.0866" layer="37"/>
<rectangle x1="2.921" y1="7.0866" x2="3.429" y2="7.1374" layer="37"/>
<rectangle x1="4.191" y1="7.0866" x2="4.699" y2="7.1374" layer="37"/>
<rectangle x1="6.1722" y1="7.0866" x2="6.985" y2="7.1374" layer="37"/>
<rectangle x1="7.8994" y1="7.0866" x2="8.5598" y2="7.1374" layer="37"/>
<rectangle x1="9.6266" y1="7.0866" x2="10.1854" y2="7.1374" layer="37"/>
<rectangle x1="10.8966" y1="7.0866" x2="11.303" y2="7.1374" layer="37"/>
<rectangle x1="7.8486" y1="7.0866" x2="7.8994" y2="7.1374" layer="37"/>
<rectangle x1="6.985" y1="7.0866" x2="7.0358" y2="7.1374" layer="37"/>
<rectangle x1="10.8458" y1="7.0866" x2="10.8966" y2="7.1374" layer="37"/>
<rectangle x1="2.921" y1="7.1374" x2="3.429" y2="7.1882" layer="37"/>
<rectangle x1="4.191" y1="7.1374" x2="4.699" y2="7.1882" layer="37"/>
<rectangle x1="6.223" y1="7.1374" x2="6.9342" y2="7.1882" layer="37"/>
<rectangle x1="7.8486" y1="7.1374" x2="8.509" y2="7.1882" layer="37"/>
<rectangle x1="9.6266" y1="7.1374" x2="10.1854" y2="7.1882" layer="37"/>
<rectangle x1="10.8966" y1="7.1374" x2="11.303" y2="7.1882" layer="37"/>
<rectangle x1="6.9342" y1="7.1374" x2="6.985" y2="7.1882" layer="37"/>
<rectangle x1="7.7978" y1="7.1374" x2="7.8486" y2="7.1882" layer="37"/>
<rectangle x1="10.8458" y1="7.1374" x2="10.8966" y2="7.1882" layer="37"/>
<rectangle x1="2.921" y1="7.1882" x2="3.429" y2="7.239" layer="37"/>
<rectangle x1="4.191" y1="7.1882" x2="4.699" y2="7.239" layer="37"/>
<rectangle x1="6.223" y1="7.1882" x2="6.8834" y2="7.239" layer="37"/>
<rectangle x1="7.8486" y1="7.1882" x2="8.4582" y2="7.239" layer="37"/>
<rectangle x1="9.6266" y1="7.1882" x2="10.1854" y2="7.239" layer="37"/>
<rectangle x1="10.8966" y1="7.1882" x2="11.303" y2="7.239" layer="37"/>
<rectangle x1="7.7978" y1="7.1882" x2="7.8486" y2="7.239" layer="37"/>
<rectangle x1="6.8834" y1="7.1882" x2="6.9342" y2="7.239" layer="37"/>
<rectangle x1="8.4582" y1="7.1882" x2="8.509" y2="7.239" layer="37"/>
<rectangle x1="10.8458" y1="7.1882" x2="10.8966" y2="7.239" layer="37"/>
<rectangle x1="2.921" y1="7.239" x2="3.429" y2="7.2898" layer="37"/>
<rectangle x1="4.191" y1="7.239" x2="4.699" y2="7.2898" layer="37"/>
<rectangle x1="6.223" y1="7.239" x2="6.8834" y2="7.2898" layer="37"/>
<rectangle x1="7.7978" y1="7.239" x2="8.4074" y2="7.2898" layer="37"/>
<rectangle x1="9.6266" y1="7.239" x2="10.1854" y2="7.2898" layer="37"/>
<rectangle x1="10.8966" y1="7.239" x2="11.303" y2="7.2898" layer="37"/>
<rectangle x1="6.1722" y1="7.239" x2="6.223" y2="7.2898" layer="37"/>
<rectangle x1="7.747" y1="7.239" x2="7.7978" y2="7.2898" layer="37"/>
<rectangle x1="8.4074" y1="7.239" x2="8.4582" y2="7.2898" layer="37"/>
<rectangle x1="10.8458" y1="7.239" x2="10.8966" y2="7.2898" layer="37"/>
<rectangle x1="2.921" y1="7.2898" x2="3.429" y2="7.3406" layer="37"/>
<rectangle x1="4.191" y1="7.2898" x2="4.699" y2="7.3406" layer="37"/>
<rectangle x1="6.1722" y1="7.2898" x2="6.8326" y2="7.3406" layer="37"/>
<rectangle x1="7.747" y1="7.2898" x2="8.3566" y2="7.3406" layer="37"/>
<rectangle x1="9.6266" y1="7.2898" x2="10.1854" y2="7.3406" layer="37"/>
<rectangle x1="10.8966" y1="7.2898" x2="11.303" y2="7.3406" layer="37"/>
<rectangle x1="6.1214" y1="7.2898" x2="6.1722" y2="7.3406" layer="37"/>
<rectangle x1="7.6962" y1="7.2898" x2="7.747" y2="7.3406" layer="37"/>
<rectangle x1="8.3566" y1="7.2898" x2="8.4074" y2="7.3406" layer="37"/>
<rectangle x1="10.8458" y1="7.2898" x2="10.8966" y2="7.3406" layer="37"/>
<rectangle x1="2.921" y1="7.3406" x2="3.429" y2="7.3914" layer="37"/>
<rectangle x1="4.191" y1="7.3406" x2="4.699" y2="7.3914" layer="37"/>
<rectangle x1="6.1214" y1="7.3406" x2="6.7818" y2="7.3914" layer="37"/>
<rectangle x1="7.6962" y1="7.3406" x2="8.3058" y2="7.3914" layer="37"/>
<rectangle x1="9.6266" y1="7.3406" x2="10.1854" y2="7.3914" layer="37"/>
<rectangle x1="10.8966" y1="7.3406" x2="11.303" y2="7.3914" layer="37"/>
<rectangle x1="8.3058" y1="7.3406" x2="8.3566" y2="7.3914" layer="37"/>
<rectangle x1="6.0706" y1="7.3406" x2="6.1214" y2="7.3914" layer="37"/>
<rectangle x1="7.6454" y1="7.3406" x2="7.6962" y2="7.3914" layer="37"/>
<rectangle x1="10.8458" y1="7.3406" x2="10.8966" y2="7.3914" layer="37"/>
<rectangle x1="2.921" y1="7.3914" x2="3.429" y2="7.4422" layer="37"/>
<rectangle x1="4.191" y1="7.3914" x2="4.699" y2="7.4422" layer="37"/>
<rectangle x1="6.0706" y1="7.3914" x2="6.731" y2="7.4422" layer="37"/>
<rectangle x1="7.6454" y1="7.3914" x2="8.255" y2="7.4422" layer="37"/>
<rectangle x1="9.6266" y1="7.3914" x2="10.1854" y2="7.4422" layer="37"/>
<rectangle x1="10.8966" y1="7.3914" x2="11.303" y2="7.4422" layer="37"/>
<rectangle x1="8.255" y1="7.3914" x2="8.3058" y2="7.4422" layer="37"/>
<rectangle x1="7.5946" y1="7.3914" x2="7.6454" y2="7.4422" layer="37"/>
<rectangle x1="6.0198" y1="7.3914" x2="6.0706" y2="7.4422" layer="37"/>
<rectangle x1="6.731" y1="7.3914" x2="6.7818" y2="7.4422" layer="37"/>
<rectangle x1="10.8458" y1="7.3914" x2="10.8966" y2="7.4422" layer="37"/>
<rectangle x1="2.921" y1="7.4422" x2="3.429" y2="7.493" layer="37"/>
<rectangle x1="4.191" y1="7.4422" x2="4.699" y2="7.493" layer="37"/>
<rectangle x1="6.0198" y1="7.4422" x2="6.6802" y2="7.493" layer="37"/>
<rectangle x1="7.5946" y1="7.4422" x2="8.2042" y2="7.493" layer="37"/>
<rectangle x1="9.6266" y1="7.4422" x2="10.1854" y2="7.493" layer="37"/>
<rectangle x1="10.8966" y1="7.4422" x2="11.303" y2="7.493" layer="37"/>
<rectangle x1="8.2042" y1="7.4422" x2="8.255" y2="7.493" layer="37"/>
<rectangle x1="6.6802" y1="7.4422" x2="6.731" y2="7.493" layer="37"/>
<rectangle x1="10.8458" y1="7.4422" x2="10.8966" y2="7.493" layer="37"/>
<rectangle x1="7.5438" y1="7.4422" x2="7.5946" y2="7.493" layer="37"/>
<rectangle x1="8.255" y1="7.4422" x2="8.3058" y2="7.493" layer="37"/>
<rectangle x1="2.921" y1="7.493" x2="3.429" y2="7.5438" layer="37"/>
<rectangle x1="4.191" y1="7.493" x2="4.699" y2="7.5438" layer="37"/>
<rectangle x1="6.0198" y1="7.493" x2="6.6294" y2="7.5438" layer="37"/>
<rectangle x1="7.5438" y1="7.493" x2="8.2042" y2="7.5438" layer="37"/>
<rectangle x1="9.6266" y1="7.493" x2="10.1854" y2="7.5438" layer="37"/>
<rectangle x1="10.8966" y1="7.493" x2="11.303" y2="7.5438" layer="37"/>
<rectangle x1="5.969" y1="7.493" x2="6.0198" y2="7.5438" layer="37"/>
<rectangle x1="6.6294" y1="7.493" x2="6.6802" y2="7.5438" layer="37"/>
<rectangle x1="8.2042" y1="7.493" x2="8.255" y2="7.5438" layer="37"/>
<rectangle x1="10.8458" y1="7.493" x2="10.8966" y2="7.5438" layer="37"/>
<rectangle x1="2.921" y1="7.5438" x2="3.429" y2="7.5946" layer="37"/>
<rectangle x1="4.191" y1="7.5438" x2="4.699" y2="7.5946" layer="37"/>
<rectangle x1="5.969" y1="7.5438" x2="6.5786" y2="7.5946" layer="37"/>
<rectangle x1="7.493" y1="7.5438" x2="8.1534" y2="7.5946" layer="37"/>
<rectangle x1="9.6266" y1="7.5438" x2="10.1854" y2="7.5946" layer="37"/>
<rectangle x1="10.8966" y1="7.5438" x2="11.303" y2="7.5946" layer="37"/>
<rectangle x1="5.9182" y1="7.5438" x2="5.969" y2="7.5946" layer="37"/>
<rectangle x1="6.5786" y1="7.5438" x2="6.6294" y2="7.5946" layer="37"/>
<rectangle x1="8.1534" y1="7.5438" x2="8.2042" y2="7.5946" layer="37"/>
<rectangle x1="10.8458" y1="7.5438" x2="10.8966" y2="7.5946" layer="37"/>
<rectangle x1="2.921" y1="7.5946" x2="3.429" y2="7.6454" layer="37"/>
<rectangle x1="4.191" y1="7.5946" x2="4.699" y2="7.6454" layer="37"/>
<rectangle x1="5.9182" y1="7.5946" x2="6.5278" y2="7.6454" layer="37"/>
<rectangle x1="7.4422" y1="7.5946" x2="8.1026" y2="7.6454" layer="37"/>
<rectangle x1="9.6266" y1="7.5946" x2="10.1854" y2="7.6454" layer="37"/>
<rectangle x1="10.8966" y1="7.5946" x2="11.303" y2="7.6454" layer="37"/>
<rectangle x1="6.5278" y1="7.5946" x2="6.5786" y2="7.6454" layer="37"/>
<rectangle x1="8.1026" y1="7.5946" x2="8.1534" y2="7.6454" layer="37"/>
<rectangle x1="5.8674" y1="7.5946" x2="5.9182" y2="7.6454" layer="37"/>
<rectangle x1="10.8458" y1="7.5946" x2="10.8966" y2="7.6454" layer="37"/>
<rectangle x1="2.921" y1="7.6454" x2="3.429" y2="7.6962" layer="37"/>
<rectangle x1="4.191" y1="7.6454" x2="4.699" y2="7.6962" layer="37"/>
<rectangle x1="5.8674" y1="7.6454" x2="6.5278" y2="7.6962" layer="37"/>
<rectangle x1="7.4422" y1="7.6454" x2="8.1026" y2="7.6962" layer="37"/>
<rectangle x1="9.6266" y1="7.6454" x2="10.1854" y2="7.6962" layer="37"/>
<rectangle x1="10.8966" y1="7.6454" x2="11.303" y2="7.6962" layer="37"/>
<rectangle x1="7.3914" y1="7.6454" x2="7.4422" y2="7.6962" layer="37"/>
<rectangle x1="5.8166" y1="7.6454" x2="5.8674" y2="7.6962" layer="37"/>
<rectangle x1="10.8458" y1="7.6454" x2="10.8966" y2="7.6962" layer="37"/>
<rectangle x1="2.921" y1="7.6962" x2="3.429" y2="7.747" layer="37"/>
<rectangle x1="4.191" y1="7.6962" x2="4.699" y2="7.747" layer="37"/>
<rectangle x1="5.8166" y1="7.6962" x2="6.477" y2="7.747" layer="37"/>
<rectangle x1="7.3914" y1="7.6962" x2="8.0518" y2="7.747" layer="37"/>
<rectangle x1="9.6266" y1="7.6962" x2="10.1854" y2="7.747" layer="37"/>
<rectangle x1="10.8966" y1="7.6962" x2="11.303" y2="7.747" layer="37"/>
<rectangle x1="7.3406" y1="7.6962" x2="7.3914" y2="7.747" layer="37"/>
<rectangle x1="6.477" y1="7.6962" x2="6.5278" y2="7.747" layer="37"/>
<rectangle x1="10.8458" y1="7.6962" x2="10.8966" y2="7.747" layer="37"/>
<rectangle x1="5.7658" y1="7.6962" x2="5.8166" y2="7.747" layer="37"/>
<rectangle x1="2.921" y1="7.747" x2="3.429" y2="7.7978" layer="37"/>
<rectangle x1="4.191" y1="7.747" x2="4.699" y2="7.7978" layer="37"/>
<rectangle x1="5.7658" y1="7.747" x2="6.4262" y2="7.7978" layer="37"/>
<rectangle x1="7.3406" y1="7.747" x2="8.001" y2="7.7978" layer="37"/>
<rectangle x1="9.6266" y1="7.747" x2="10.1854" y2="7.7978" layer="37"/>
<rectangle x1="10.8966" y1="7.747" x2="11.303" y2="7.7978" layer="37"/>
<rectangle x1="6.4262" y1="7.747" x2="6.477" y2="7.7978" layer="37"/>
<rectangle x1="10.8458" y1="7.747" x2="10.8966" y2="7.7978" layer="37"/>
<rectangle x1="8.001" y1="7.747" x2="8.0518" y2="7.7978" layer="37"/>
<rectangle x1="2.921" y1="7.7978" x2="3.429" y2="7.8486" layer="37"/>
<rectangle x1="4.191" y1="7.7978" x2="4.699" y2="7.8486" layer="37"/>
<rectangle x1="5.7658" y1="7.7978" x2="6.3754" y2="7.8486" layer="37"/>
<rectangle x1="7.2898" y1="7.7978" x2="7.9502" y2="7.8486" layer="37"/>
<rectangle x1="8.5598" y1="7.7978" x2="8.6106" y2="7.8486" layer="37"/>
<rectangle x1="9.6266" y1="7.7978" x2="10.1854" y2="7.8486" layer="37"/>
<rectangle x1="10.8966" y1="7.7978" x2="11.303" y2="7.8486" layer="37"/>
<rectangle x1="5.715" y1="7.7978" x2="5.7658" y2="7.8486" layer="37"/>
<rectangle x1="6.3754" y1="7.7978" x2="6.4262" y2="7.8486" layer="37"/>
<rectangle x1="8.509" y1="7.7978" x2="8.5598" y2="7.8486" layer="37"/>
<rectangle x1="8.6106" y1="7.7978" x2="8.6614" y2="7.8486" layer="37"/>
<rectangle x1="7.9502" y1="7.7978" x2="8.001" y2="7.8486" layer="37"/>
<rectangle x1="10.8458" y1="7.7978" x2="10.8966" y2="7.8486" layer="37"/>
<rectangle x1="2.921" y1="7.8486" x2="3.429" y2="7.8994" layer="37"/>
<rectangle x1="4.191" y1="7.8486" x2="4.699" y2="7.8994" layer="37"/>
<rectangle x1="5.715" y1="7.8486" x2="6.3246" y2="7.8994" layer="37"/>
<rectangle x1="7.239" y1="7.8486" x2="7.8994" y2="7.8994" layer="37"/>
<rectangle x1="8.4582" y1="7.8486" x2="8.7122" y2="7.8994" layer="37"/>
<rectangle x1="9.6266" y1="7.8486" x2="10.1854" y2="7.8994" layer="37"/>
<rectangle x1="10.8966" y1="7.8486" x2="11.303" y2="7.8994" layer="37"/>
<rectangle x1="6.3246" y1="7.8486" x2="6.3754" y2="7.8994" layer="37"/>
<rectangle x1="8.4074" y1="7.8486" x2="8.4582" y2="7.8994" layer="37"/>
<rectangle x1="5.6642" y1="7.8486" x2="5.715" y2="7.8994" layer="37"/>
<rectangle x1="7.8994" y1="7.8486" x2="7.9502" y2="7.8994" layer="37"/>
<rectangle x1="8.7122" y1="7.8486" x2="8.763" y2="7.8994" layer="37"/>
<rectangle x1="10.8458" y1="7.8486" x2="10.8966" y2="7.8994" layer="37"/>
<rectangle x1="2.921" y1="7.8994" x2="3.429" y2="7.9502" layer="37"/>
<rectangle x1="4.191" y1="7.8994" x2="4.699" y2="7.9502" layer="37"/>
<rectangle x1="5.6642" y1="7.8994" x2="6.2738" y2="7.9502" layer="37"/>
<rectangle x1="7.239" y1="7.8994" x2="7.8486" y2="7.9502" layer="37"/>
<rectangle x1="8.3566" y1="7.8994" x2="8.763" y2="7.9502" layer="37"/>
<rectangle x1="9.6266" y1="7.8994" x2="10.1854" y2="7.9502" layer="37"/>
<rectangle x1="10.8966" y1="7.8994" x2="11.303" y2="7.9502" layer="37"/>
<rectangle x1="6.2738" y1="7.8994" x2="6.3246" y2="7.9502" layer="37"/>
<rectangle x1="7.1882" y1="7.8994" x2="7.239" y2="7.9502" layer="37"/>
<rectangle x1="7.8486" y1="7.8994" x2="7.8994" y2="7.9502" layer="37"/>
<rectangle x1="8.763" y1="7.8994" x2="8.8138" y2="7.9502" layer="37"/>
<rectangle x1="5.6134" y1="7.8994" x2="5.6642" y2="7.9502" layer="37"/>
<rectangle x1="10.8458" y1="7.8994" x2="10.8966" y2="7.9502" layer="37"/>
<rectangle x1="2.921" y1="7.9502" x2="3.429" y2="8.001" layer="37"/>
<rectangle x1="4.191" y1="7.9502" x2="4.699" y2="8.001" layer="37"/>
<rectangle x1="5.6134" y1="7.9502" x2="6.2738" y2="8.001" layer="37"/>
<rectangle x1="7.1882" y1="7.9502" x2="7.8486" y2="8.001" layer="37"/>
<rectangle x1="8.3566" y1="7.9502" x2="8.8138" y2="8.001" layer="37"/>
<rectangle x1="9.6266" y1="7.9502" x2="10.1854" y2="8.001" layer="37"/>
<rectangle x1="10.8966" y1="7.9502" x2="11.303" y2="8.001" layer="37"/>
<rectangle x1="7.1374" y1="7.9502" x2="7.1882" y2="8.001" layer="37"/>
<rectangle x1="8.8138" y1="7.9502" x2="8.8646" y2="8.001" layer="37"/>
<rectangle x1="5.5626" y1="7.9502" x2="5.6134" y2="8.001" layer="37"/>
<rectangle x1="8.3058" y1="7.9502" x2="8.3566" y2="8.001" layer="37"/>
<rectangle x1="10.8458" y1="7.9502" x2="10.8966" y2="8.001" layer="37"/>
<rectangle x1="2.921" y1="8.001" x2="3.429" y2="8.0518" layer="37"/>
<rectangle x1="4.191" y1="8.001" x2="4.699" y2="8.0518" layer="37"/>
<rectangle x1="5.5626" y1="8.001" x2="6.223" y2="8.0518" layer="37"/>
<rectangle x1="7.1374" y1="8.001" x2="7.7978" y2="8.0518" layer="37"/>
<rectangle x1="8.3058" y1="8.001" x2="8.8646" y2="8.0518" layer="37"/>
<rectangle x1="9.6266" y1="8.001" x2="10.1854" y2="8.0518" layer="37"/>
<rectangle x1="10.8966" y1="8.001" x2="11.303" y2="8.0518" layer="37"/>
<rectangle x1="7.0866" y1="8.001" x2="7.1374" y2="8.0518" layer="37"/>
<rectangle x1="6.223" y1="8.001" x2="6.2738" y2="8.0518" layer="37"/>
<rectangle x1="8.8646" y1="8.001" x2="8.9154" y2="8.0518" layer="37"/>
<rectangle x1="10.8458" y1="8.001" x2="10.8966" y2="8.0518" layer="37"/>
<rectangle x1="2.921" y1="8.0518" x2="3.429" y2="8.1026" layer="37"/>
<rectangle x1="4.191" y1="8.0518" x2="4.699" y2="8.1026" layer="37"/>
<rectangle x1="5.5118" y1="8.0518" x2="6.1722" y2="8.1026" layer="37"/>
<rectangle x1="7.1374" y1="8.0518" x2="7.747" y2="8.1026" layer="37"/>
<rectangle x1="8.3058" y1="8.0518" x2="8.9154" y2="8.1026" layer="37"/>
<rectangle x1="9.6266" y1="8.0518" x2="10.1854" y2="8.1026" layer="37"/>
<rectangle x1="10.8966" y1="8.0518" x2="11.303" y2="8.1026" layer="37"/>
<rectangle x1="7.0866" y1="8.0518" x2="7.1374" y2="8.1026" layer="37"/>
<rectangle x1="6.1722" y1="8.0518" x2="6.223" y2="8.1026" layer="37"/>
<rectangle x1="7.747" y1="8.0518" x2="7.7978" y2="8.1026" layer="37"/>
<rectangle x1="10.8458" y1="8.0518" x2="10.8966" y2="8.1026" layer="37"/>
<rectangle x1="2.921" y1="8.1026" x2="3.429" y2="8.1534" layer="37"/>
<rectangle x1="4.191" y1="8.1026" x2="4.699" y2="8.1534" layer="37"/>
<rectangle x1="5.5118" y1="8.1026" x2="6.1214" y2="8.1534" layer="37"/>
<rectangle x1="7.0866" y1="8.1026" x2="7.6962" y2="8.1534" layer="37"/>
<rectangle x1="8.3058" y1="8.1026" x2="8.9154" y2="8.1534" layer="37"/>
<rectangle x1="9.6266" y1="8.1026" x2="10.1854" y2="8.1534" layer="37"/>
<rectangle x1="10.8966" y1="8.1026" x2="11.303" y2="8.1534" layer="37"/>
<rectangle x1="5.461" y1="8.1026" x2="5.5118" y2="8.1534" layer="37"/>
<rectangle x1="7.0358" y1="8.1026" x2="7.0866" y2="8.1534" layer="37"/>
<rectangle x1="8.9154" y1="8.1026" x2="8.9662" y2="8.1534" layer="37"/>
<rectangle x1="6.1214" y1="8.1026" x2="6.1722" y2="8.1534" layer="37"/>
<rectangle x1="7.6962" y1="8.1026" x2="7.747" y2="8.1534" layer="37"/>
<rectangle x1="10.8458" y1="8.1026" x2="10.8966" y2="8.1534" layer="37"/>
<rectangle x1="2.921" y1="8.1534" x2="3.429" y2="8.2042" layer="37"/>
<rectangle x1="4.191" y1="8.1534" x2="4.699" y2="8.2042" layer="37"/>
<rectangle x1="5.461" y1="8.1534" x2="6.0706" y2="8.2042" layer="37"/>
<rectangle x1="7.0358" y1="8.1534" x2="7.6962" y2="8.2042" layer="37"/>
<rectangle x1="8.3566" y1="8.1534" x2="8.9662" y2="8.2042" layer="37"/>
<rectangle x1="9.6266" y1="8.1534" x2="10.1854" y2="8.2042" layer="37"/>
<rectangle x1="10.8966" y1="8.1534" x2="11.303" y2="8.2042" layer="37"/>
<rectangle x1="6.0706" y1="8.1534" x2="6.1214" y2="8.2042" layer="37"/>
<rectangle x1="6.985" y1="8.1534" x2="7.0358" y2="8.2042" layer="37"/>
<rectangle x1="5.4102" y1="8.1534" x2="5.461" y2="8.2042" layer="37"/>
<rectangle x1="8.9662" y1="8.1534" x2="9.017" y2="8.2042" layer="37"/>
<rectangle x1="10.8458" y1="8.1534" x2="10.8966" y2="8.2042" layer="37"/>
<rectangle x1="2.921" y1="8.2042" x2="3.429" y2="8.255" layer="37"/>
<rectangle x1="4.191" y1="8.2042" x2="4.699" y2="8.255" layer="37"/>
<rectangle x1="5.4102" y1="8.2042" x2="6.0706" y2="8.255" layer="37"/>
<rectangle x1="6.985" y1="8.2042" x2="7.6962" y2="8.255" layer="37"/>
<rectangle x1="8.3566" y1="8.2042" x2="9.017" y2="8.255" layer="37"/>
<rectangle x1="9.6266" y1="8.2042" x2="10.1854" y2="8.255" layer="37"/>
<rectangle x1="10.8966" y1="8.2042" x2="11.303" y2="8.255" layer="37"/>
<rectangle x1="7.6962" y1="8.2042" x2="7.747" y2="8.255" layer="37"/>
<rectangle x1="6.9342" y1="8.2042" x2="6.985" y2="8.255" layer="37"/>
<rectangle x1="5.3594" y1="8.2042" x2="5.4102" y2="8.255" layer="37"/>
<rectangle x1="9.017" y1="8.2042" x2="9.0678" y2="8.255" layer="37"/>
<rectangle x1="10.8458" y1="8.2042" x2="10.8966" y2="8.255" layer="37"/>
<rectangle x1="2.921" y1="8.255" x2="3.429" y2="8.3058" layer="37"/>
<rectangle x1="4.191" y1="8.255" x2="4.699" y2="8.3058" layer="37"/>
<rectangle x1="5.3594" y1="8.255" x2="6.0198" y2="8.3058" layer="37"/>
<rectangle x1="6.9342" y1="8.255" x2="7.747" y2="8.3058" layer="37"/>
<rectangle x1="8.4074" y1="8.255" x2="9.0678" y2="8.3058" layer="37"/>
<rectangle x1="9.6266" y1="8.255" x2="10.1854" y2="8.3058" layer="37"/>
<rectangle x1="10.8966" y1="8.255" x2="11.303" y2="8.3058" layer="37"/>
<rectangle x1="6.8834" y1="8.255" x2="6.9342" y2="8.3058" layer="37"/>
<rectangle x1="5.3086" y1="8.255" x2="5.3594" y2="8.3058" layer="37"/>
<rectangle x1="7.747" y1="8.255" x2="7.7978" y2="8.3058" layer="37"/>
<rectangle x1="6.0198" y1="8.255" x2="6.0706" y2="8.3058" layer="37"/>
<rectangle x1="10.8458" y1="8.255" x2="10.8966" y2="8.3058" layer="37"/>
<rectangle x1="2.921" y1="8.3058" x2="3.429" y2="8.3566" layer="37"/>
<rectangle x1="4.191" y1="8.3058" x2="4.699" y2="8.3566" layer="37"/>
<rectangle x1="5.3086" y1="8.3058" x2="5.969" y2="8.3566" layer="37"/>
<rectangle x1="6.8834" y1="8.3058" x2="7.7978" y2="8.3566" layer="37"/>
<rectangle x1="8.4582" y1="8.3058" x2="9.1186" y2="8.3566" layer="37"/>
<rectangle x1="9.6266" y1="8.3058" x2="10.1854" y2="8.3566" layer="37"/>
<rectangle x1="10.8966" y1="8.3058" x2="11.303" y2="8.3566" layer="37"/>
<rectangle x1="5.969" y1="8.3058" x2="6.0198" y2="8.3566" layer="37"/>
<rectangle x1="6.8326" y1="8.3058" x2="6.8834" y2="8.3566" layer="37"/>
<rectangle x1="10.8458" y1="8.3058" x2="10.8966" y2="8.3566" layer="37"/>
<rectangle x1="2.921" y1="8.3566" x2="3.429" y2="8.4074" layer="37"/>
<rectangle x1="4.191" y1="8.3566" x2="4.699" y2="8.4074" layer="37"/>
<rectangle x1="5.2578" y1="8.3566" x2="5.969" y2="8.4074" layer="37"/>
<rectangle x1="6.8834" y1="8.3566" x2="7.7978" y2="8.4074" layer="37"/>
<rectangle x1="8.509" y1="8.3566" x2="9.1186" y2="8.4074" layer="37"/>
<rectangle x1="9.6266" y1="8.3566" x2="10.1854" y2="8.4074" layer="37"/>
<rectangle x1="10.8966" y1="8.3566" x2="11.303" y2="8.4074" layer="37"/>
<rectangle x1="6.8326" y1="8.3566" x2="6.8834" y2="8.4074" layer="37"/>
<rectangle x1="7.7978" y1="8.3566" x2="7.8486" y2="8.4074" layer="37"/>
<rectangle x1="9.1186" y1="8.3566" x2="9.1694" y2="8.4074" layer="37"/>
<rectangle x1="8.4582" y1="8.3566" x2="8.509" y2="8.4074" layer="37"/>
<rectangle x1="10.8458" y1="8.3566" x2="10.8966" y2="8.4074" layer="37"/>
<rectangle x1="2.921" y1="8.4074" x2="3.429" y2="8.4582" layer="37"/>
<rectangle x1="4.191" y1="8.4074" x2="4.699" y2="8.4582" layer="37"/>
<rectangle x1="5.2578" y1="8.4074" x2="5.9182" y2="8.4582" layer="37"/>
<rectangle x1="6.8326" y1="8.4074" x2="7.8486" y2="8.4582" layer="37"/>
<rectangle x1="8.509" y1="8.4074" x2="9.1694" y2="8.4582" layer="37"/>
<rectangle x1="9.6266" y1="8.4074" x2="10.1854" y2="8.4582" layer="37"/>
<rectangle x1="10.8966" y1="8.4074" x2="11.303" y2="8.4582" layer="37"/>
<rectangle x1="5.207" y1="8.4074" x2="5.2578" y2="8.4582" layer="37"/>
<rectangle x1="6.7818" y1="8.4074" x2="6.8326" y2="8.4582" layer="37"/>
<rectangle x1="9.1694" y1="8.4074" x2="9.2202" y2="8.4582" layer="37"/>
<rectangle x1="7.8486" y1="8.4074" x2="7.8994" y2="8.4582" layer="37"/>
<rectangle x1="10.8458" y1="8.4074" x2="10.8966" y2="8.4582" layer="37"/>
<rectangle x1="2.921" y1="8.4582" x2="3.429" y2="8.509" layer="37"/>
<rectangle x1="4.191" y1="8.4582" x2="4.699" y2="8.509" layer="37"/>
<rectangle x1="5.207" y1="8.4582" x2="5.8166" y2="8.509" layer="37"/>
<rectangle x1="6.7818" y1="8.4582" x2="7.8994" y2="8.509" layer="37"/>
<rectangle x1="8.6106" y1="8.4582" x2="9.1694" y2="8.509" layer="37"/>
<rectangle x1="9.6266" y1="8.4582" x2="10.1854" y2="8.509" layer="37"/>
<rectangle x1="10.8966" y1="8.4582" x2="11.303" y2="8.509" layer="37"/>
<rectangle x1="5.1562" y1="8.4582" x2="5.207" y2="8.509" layer="37"/>
<rectangle x1="5.8166" y1="8.4582" x2="5.8674" y2="8.509" layer="37"/>
<rectangle x1="6.731" y1="8.4582" x2="6.7818" y2="8.509" layer="37"/>
<rectangle x1="8.5598" y1="8.4582" x2="8.6106" y2="8.509" layer="37"/>
<rectangle x1="9.1694" y1="8.4582" x2="9.2202" y2="8.509" layer="37"/>
<rectangle x1="9.2202" y1="8.4582" x2="9.271" y2="8.509" layer="37"/>
<rectangle x1="10.8458" y1="8.4582" x2="10.8966" y2="8.509" layer="37"/>
<rectangle x1="2.921" y1="8.509" x2="3.429" y2="8.5598" layer="37"/>
<rectangle x1="4.191" y1="8.509" x2="4.699" y2="8.5598" layer="37"/>
<rectangle x1="5.1562" y1="8.509" x2="5.8166" y2="8.5598" layer="37"/>
<rectangle x1="6.731" y1="8.509" x2="7.9502" y2="8.5598" layer="37"/>
<rectangle x1="8.6106" y1="8.509" x2="9.271" y2="8.5598" layer="37"/>
<rectangle x1="9.6266" y1="8.509" x2="10.1854" y2="8.5598" layer="37"/>
<rectangle x1="10.8966" y1="8.509" x2="11.303" y2="8.5598" layer="37"/>
<rectangle x1="5.1054" y1="8.509" x2="5.1562" y2="8.5598" layer="37"/>
<rectangle x1="6.6802" y1="8.509" x2="6.731" y2="8.5598" layer="37"/>
<rectangle x1="10.8458" y1="8.509" x2="10.8966" y2="8.5598" layer="37"/>
<rectangle x1="2.921" y1="8.5598" x2="3.429" y2="8.6106" layer="37"/>
<rectangle x1="4.191" y1="8.5598" x2="4.699" y2="8.6106" layer="37"/>
<rectangle x1="5.1054" y1="8.5598" x2="5.7658" y2="8.6106" layer="37"/>
<rectangle x1="6.6802" y1="8.5598" x2="7.2898" y2="8.6106" layer="37"/>
<rectangle x1="7.4422" y1="8.5598" x2="8.001" y2="8.6106" layer="37"/>
<rectangle x1="8.6614" y1="8.5598" x2="9.271" y2="8.6106" layer="37"/>
<rectangle x1="9.6266" y1="8.5598" x2="10.1854" y2="8.6106" layer="37"/>
<rectangle x1="10.8966" y1="8.5598" x2="11.303" y2="8.6106" layer="37"/>
<rectangle x1="7.2898" y1="8.5598" x2="7.3406" y2="8.6106" layer="37"/>
<rectangle x1="9.271" y1="8.5598" x2="9.3218" y2="8.6106" layer="37"/>
<rectangle x1="7.3914" y1="8.5598" x2="7.4422" y2="8.6106" layer="37"/>
<rectangle x1="6.6294" y1="8.5598" x2="6.6802" y2="8.6106" layer="37"/>
<rectangle x1="5.0546" y1="8.5598" x2="5.1054" y2="8.6106" layer="37"/>
<rectangle x1="8.6106" y1="8.5598" x2="8.6614" y2="8.6106" layer="37"/>
<rectangle x1="5.7658" y1="8.5598" x2="5.8166" y2="8.6106" layer="37"/>
<rectangle x1="7.3406" y1="8.5598" x2="7.3914" y2="8.6106" layer="37"/>
<rectangle x1="10.8458" y1="8.5598" x2="10.8966" y2="8.6106" layer="37"/>
<rectangle x1="2.921" y1="8.6106" x2="3.429" y2="8.6614" layer="37"/>
<rectangle x1="4.191" y1="8.6106" x2="4.699" y2="8.6614" layer="37"/>
<rectangle x1="5.0546" y1="8.6106" x2="5.715" y2="8.6614" layer="37"/>
<rectangle x1="6.6294" y1="8.6106" x2="7.239" y2="8.6614" layer="37"/>
<rectangle x1="7.4422" y1="8.6106" x2="8.001" y2="8.6614" layer="37"/>
<rectangle x1="8.7122" y1="8.6106" x2="9.3218" y2="8.6614" layer="37"/>
<rectangle x1="9.6266" y1="8.6106" x2="10.1854" y2="8.6614" layer="37"/>
<rectangle x1="10.8966" y1="8.6106" x2="11.303" y2="8.6614" layer="37"/>
<rectangle x1="7.239" y1="8.6106" x2="7.2898" y2="8.6614" layer="37"/>
<rectangle x1="8.6614" y1="8.6106" x2="8.7122" y2="8.6614" layer="37"/>
<rectangle x1="9.3218" y1="8.6106" x2="9.3726" y2="8.6614" layer="37"/>
<rectangle x1="5.715" y1="8.6106" x2="5.7658" y2="8.6614" layer="37"/>
<rectangle x1="8.001" y1="8.6106" x2="8.0518" y2="8.6614" layer="37"/>
<rectangle x1="6.5786" y1="8.6106" x2="6.6294" y2="8.6614" layer="37"/>
<rectangle x1="10.8458" y1="8.6106" x2="10.8966" y2="8.6614" layer="37"/>
<rectangle x1="7.2898" y1="8.6106" x2="7.3406" y2="8.6614" layer="37"/>
<rectangle x1="2.921" y1="8.6614" x2="3.429" y2="8.7122" layer="37"/>
<rectangle x1="4.191" y1="8.6614" x2="4.699" y2="8.7122" layer="37"/>
<rectangle x1="5.0546" y1="8.6614" x2="5.6642" y2="8.7122" layer="37"/>
<rectangle x1="6.5786" y1="8.6614" x2="7.239" y2="8.7122" layer="37"/>
<rectangle x1="7.493" y1="8.6614" x2="8.0518" y2="8.7122" layer="37"/>
<rectangle x1="8.763" y1="8.6614" x2="9.3726" y2="8.7122" layer="37"/>
<rectangle x1="9.6266" y1="8.6614" x2="10.1854" y2="8.7122" layer="37"/>
<rectangle x1="10.8966" y1="8.6614" x2="11.303" y2="8.7122" layer="37"/>
<rectangle x1="5.0038" y1="8.6614" x2="5.0546" y2="8.7122" layer="37"/>
<rectangle x1="8.7122" y1="8.6614" x2="8.763" y2="8.7122" layer="37"/>
<rectangle x1="5.6642" y1="8.6614" x2="5.715" y2="8.7122" layer="37"/>
<rectangle x1="7.239" y1="8.6614" x2="7.2898" y2="8.7122" layer="37"/>
<rectangle x1="9.3726" y1="8.6614" x2="9.4234" y2="8.7122" layer="37"/>
<rectangle x1="10.8458" y1="8.6614" x2="10.8966" y2="8.7122" layer="37"/>
<rectangle x1="2.921" y1="8.7122" x2="3.429" y2="8.763" layer="37"/>
<rectangle x1="4.191" y1="8.7122" x2="4.699" y2="8.763" layer="37"/>
<rectangle x1="5.0038" y1="8.7122" x2="5.6134" y2="8.763" layer="37"/>
<rectangle x1="6.5278" y1="8.7122" x2="7.1882" y2="8.763" layer="37"/>
<rectangle x1="7.5438" y1="8.7122" x2="8.1026" y2="8.763" layer="37"/>
<rectangle x1="8.763" y1="8.7122" x2="9.4234" y2="8.763" layer="37"/>
<rectangle x1="9.6266" y1="8.7122" x2="10.1854" y2="8.763" layer="37"/>
<rectangle x1="10.8966" y1="8.7122" x2="11.303" y2="8.763" layer="37"/>
<rectangle x1="4.953" y1="8.7122" x2="5.0038" y2="8.763" layer="37"/>
<rectangle x1="5.6134" y1="8.7122" x2="5.6642" y2="8.763" layer="37"/>
<rectangle x1="7.1882" y1="8.7122" x2="7.239" y2="8.763" layer="37"/>
<rectangle x1="7.493" y1="8.7122" x2="7.5438" y2="8.763" layer="37"/>
<rectangle x1="10.8458" y1="8.7122" x2="10.8966" y2="8.763" layer="37"/>
<rectangle x1="2.921" y1="8.763" x2="3.429" y2="8.8138" layer="37"/>
<rectangle x1="4.191" y1="8.763" x2="4.699" y2="8.8138" layer="37"/>
<rectangle x1="4.953" y1="8.763" x2="5.6134" y2="8.8138" layer="37"/>
<rectangle x1="6.5278" y1="8.763" x2="7.1374" y2="8.8138" layer="37"/>
<rectangle x1="7.5946" y1="8.763" x2="8.1026" y2="8.8138" layer="37"/>
<rectangle x1="8.8138" y1="8.763" x2="9.4234" y2="8.8138" layer="37"/>
<rectangle x1="9.6266" y1="8.763" x2="10.1854" y2="8.8138" layer="37"/>
<rectangle x1="10.8966" y1="8.763" x2="11.303" y2="8.8138" layer="37"/>
<rectangle x1="6.477" y1="8.763" x2="6.5278" y2="8.8138" layer="37"/>
<rectangle x1="7.1374" y1="8.763" x2="7.1882" y2="8.8138" layer="37"/>
<rectangle x1="7.5438" y1="8.763" x2="7.5946" y2="8.8138" layer="37"/>
<rectangle x1="9.4234" y1="8.763" x2="9.4742" y2="8.8138" layer="37"/>
<rectangle x1="4.9022" y1="8.763" x2="4.953" y2="8.8138" layer="37"/>
<rectangle x1="8.1026" y1="8.763" x2="8.1534" y2="8.8138" layer="37"/>
<rectangle x1="10.8458" y1="8.763" x2="10.8966" y2="8.8138" layer="37"/>
<rectangle x1="8.763" y1="8.763" x2="8.8138" y2="8.8138" layer="37"/>
<rectangle x1="2.921" y1="8.8138" x2="3.429" y2="8.8646" layer="37"/>
<rectangle x1="4.191" y1="8.8138" x2="4.699" y2="8.8646" layer="37"/>
<rectangle x1="4.9022" y1="8.8138" x2="5.5626" y2="8.8646" layer="37"/>
<rectangle x1="6.477" y1="8.8138" x2="7.0866" y2="8.8646" layer="37"/>
<rectangle x1="7.5946" y1="8.8138" x2="8.1534" y2="8.8646" layer="37"/>
<rectangle x1="8.8646" y1="8.8138" x2="9.4742" y2="8.8646" layer="37"/>
<rectangle x1="9.6266" y1="8.8138" x2="10.1854" y2="8.8646" layer="37"/>
<rectangle x1="10.8966" y1="8.8138" x2="11.303" y2="8.8646" layer="37"/>
<rectangle x1="7.0866" y1="8.8138" x2="7.1374" y2="8.8646" layer="37"/>
<rectangle x1="6.4262" y1="8.8138" x2="6.477" y2="8.8646" layer="37"/>
<rectangle x1="9.4742" y1="8.8138" x2="9.525" y2="8.8646" layer="37"/>
<rectangle x1="4.8514" y1="8.8138" x2="4.9022" y2="8.8646" layer="37"/>
<rectangle x1="8.8138" y1="8.8138" x2="8.8646" y2="8.8646" layer="37"/>
<rectangle x1="10.8458" y1="8.8138" x2="10.8966" y2="8.8646" layer="37"/>
<rectangle x1="2.921" y1="8.8646" x2="3.429" y2="8.9154" layer="37"/>
<rectangle x1="4.191" y1="8.8646" x2="4.699" y2="8.9154" layer="37"/>
<rectangle x1="4.8514" y1="8.8646" x2="5.5118" y2="8.9154" layer="37"/>
<rectangle x1="6.4262" y1="8.8646" x2="7.0866" y2="8.9154" layer="37"/>
<rectangle x1="7.6454" y1="8.8646" x2="8.1534" y2="8.9154" layer="37"/>
<rectangle x1="8.9154" y1="8.8646" x2="9.525" y2="8.9154" layer="37"/>
<rectangle x1="9.6266" y1="8.8646" x2="10.1854" y2="8.9154" layer="37"/>
<rectangle x1="10.8966" y1="8.8646" x2="11.303" y2="8.9154" layer="37"/>
<rectangle x1="8.1534" y1="8.8646" x2="8.2042" y2="8.9154" layer="37"/>
<rectangle x1="8.8646" y1="8.8646" x2="8.9154" y2="8.9154" layer="37"/>
<rectangle x1="6.3754" y1="8.8646" x2="6.4262" y2="8.9154" layer="37"/>
<rectangle x1="9.525" y1="8.8646" x2="9.5758" y2="8.9154" layer="37"/>
<rectangle x1="7.5946" y1="8.8646" x2="7.6454" y2="8.9154" layer="37"/>
<rectangle x1="4.8006" y1="8.8646" x2="4.8514" y2="8.9154" layer="37"/>
<rectangle x1="10.8458" y1="8.8646" x2="10.8966" y2="8.9154" layer="37"/>
<rectangle x1="5.5118" y1="8.8646" x2="5.5626" y2="8.9154" layer="37"/>
<rectangle x1="2.921" y1="8.9154" x2="3.429" y2="8.9662" layer="37"/>
<rectangle x1="4.191" y1="8.9154" x2="4.699" y2="8.9662" layer="37"/>
<rectangle x1="4.8006" y1="8.9154" x2="5.461" y2="8.9662" layer="37"/>
<rectangle x1="6.3754" y1="8.9154" x2="7.0358" y2="8.9662" layer="37"/>
<rectangle x1="7.6962" y1="8.9154" x2="8.2042" y2="8.9662" layer="37"/>
<rectangle x1="8.9662" y1="8.9154" x2="10.1854" y2="8.9662" layer="37"/>
<rectangle x1="10.8966" y1="8.9154" x2="11.303" y2="8.9662" layer="37"/>
<rectangle x1="8.9154" y1="8.9154" x2="8.9662" y2="8.9662" layer="37"/>
<rectangle x1="7.6454" y1="8.9154" x2="7.6962" y2="8.9662" layer="37"/>
<rectangle x1="8.2042" y1="8.9154" x2="8.255" y2="8.9662" layer="37"/>
<rectangle x1="5.461" y1="8.9154" x2="5.5118" y2="8.9662" layer="37"/>
<rectangle x1="7.0358" y1="8.9154" x2="7.0866" y2="8.9662" layer="37"/>
<rectangle x1="10.8458" y1="8.9154" x2="10.8966" y2="8.9662" layer="37"/>
<rectangle x1="6.3246" y1="8.9154" x2="6.3754" y2="8.9662" layer="37"/>
<rectangle x1="2.921" y1="8.9662" x2="3.429" y2="9.017" layer="37"/>
<rectangle x1="4.191" y1="8.9662" x2="5.4102" y2="9.017" layer="37"/>
<rectangle x1="6.3246" y1="8.9662" x2="6.985" y2="9.017" layer="37"/>
<rectangle x1="7.6962" y1="8.9662" x2="8.255" y2="9.017" layer="37"/>
<rectangle x1="8.9662" y1="8.9662" x2="10.1854" y2="9.017" layer="37"/>
<rectangle x1="10.8966" y1="8.9662" x2="11.303" y2="9.017" layer="37"/>
<rectangle x1="5.4102" y1="8.9662" x2="5.461" y2="9.017" layer="37"/>
<rectangle x1="6.985" y1="8.9662" x2="7.0358" y2="9.017" layer="37"/>
<rectangle x1="10.8458" y1="8.9662" x2="10.8966" y2="9.017" layer="37"/>
<rectangle x1="8.255" y1="8.9662" x2="8.3058" y2="9.017" layer="37"/>
<rectangle x1="2.921" y1="9.017" x2="3.429" y2="9.0678" layer="37"/>
<rectangle x1="4.191" y1="9.017" x2="5.3594" y2="9.0678" layer="37"/>
<rectangle x1="6.2738" y1="9.017" x2="6.9342" y2="9.0678" layer="37"/>
<rectangle x1="7.747" y1="9.017" x2="8.255" y2="9.0678" layer="37"/>
<rectangle x1="9.017" y1="9.017" x2="10.1854" y2="9.0678" layer="37"/>
<rectangle x1="10.8966" y1="9.017" x2="11.303" y2="9.0678" layer="37"/>
<rectangle x1="8.255" y1="9.017" x2="8.3058" y2="9.0678" layer="37"/>
<rectangle x1="5.3594" y1="9.017" x2="5.4102" y2="9.0678" layer="37"/>
<rectangle x1="6.9342" y1="9.017" x2="6.985" y2="9.0678" layer="37"/>
<rectangle x1="8.9662" y1="9.017" x2="9.017" y2="9.0678" layer="37"/>
<rectangle x1="10.8458" y1="9.017" x2="10.8966" y2="9.0678" layer="37"/>
<rectangle x1="2.921" y1="9.0678" x2="3.429" y2="9.1186" layer="37"/>
<rectangle x1="4.191" y1="9.0678" x2="5.3594" y2="9.1186" layer="37"/>
<rectangle x1="6.2738" y1="9.0678" x2="6.9342" y2="9.1186" layer="37"/>
<rectangle x1="7.7978" y1="9.0678" x2="8.3058" y2="9.1186" layer="37"/>
<rectangle x1="9.0678" y1="9.0678" x2="10.1854" y2="9.1186" layer="37"/>
<rectangle x1="10.8966" y1="9.0678" x2="11.303" y2="9.1186" layer="37"/>
<rectangle x1="6.223" y1="9.0678" x2="6.2738" y2="9.1186" layer="37"/>
<rectangle x1="9.017" y1="9.0678" x2="9.0678" y2="9.1186" layer="37"/>
<rectangle x1="8.3058" y1="9.0678" x2="8.3566" y2="9.1186" layer="37"/>
<rectangle x1="7.747" y1="9.0678" x2="7.7978" y2="9.1186" layer="37"/>
<rectangle x1="10.8458" y1="9.0678" x2="10.8966" y2="9.1186" layer="37"/>
<rectangle x1="2.921" y1="9.1186" x2="3.429" y2="9.1694" layer="37"/>
<rectangle x1="4.191" y1="9.1186" x2="5.3086" y2="9.1694" layer="37"/>
<rectangle x1="6.223" y1="9.1186" x2="6.8834" y2="9.1694" layer="37"/>
<rectangle x1="7.8486" y1="9.1186" x2="8.3566" y2="9.1694" layer="37"/>
<rectangle x1="9.1186" y1="9.1186" x2="10.1854" y2="9.1694" layer="37"/>
<rectangle x1="10.8966" y1="9.1186" x2="11.303" y2="9.1694" layer="37"/>
<rectangle x1="7.7978" y1="9.1186" x2="7.8486" y2="9.1694" layer="37"/>
<rectangle x1="9.0678" y1="9.1186" x2="9.1186" y2="9.1694" layer="37"/>
<rectangle x1="6.1722" y1="9.1186" x2="6.223" y2="9.1694" layer="37"/>
<rectangle x1="8.3566" y1="9.1186" x2="8.4074" y2="9.1694" layer="37"/>
<rectangle x1="10.8458" y1="9.1186" x2="10.8966" y2="9.1694" layer="37"/>
<rectangle x1="2.921" y1="9.1694" x2="3.429" y2="9.2202" layer="37"/>
<rectangle x1="4.191" y1="9.1694" x2="5.2578" y2="9.2202" layer="37"/>
<rectangle x1="6.1722" y1="9.1694" x2="6.8326" y2="9.2202" layer="37"/>
<rectangle x1="7.8486" y1="9.1694" x2="8.3566" y2="9.2202" layer="37"/>
<rectangle x1="9.1186" y1="9.1694" x2="10.1854" y2="9.2202" layer="37"/>
<rectangle x1="10.8966" y1="9.1694" x2="11.303" y2="9.2202" layer="37"/>
<rectangle x1="8.3566" y1="9.1694" x2="8.4074" y2="9.2202" layer="37"/>
<rectangle x1="6.1214" y1="9.1694" x2="6.1722" y2="9.2202" layer="37"/>
<rectangle x1="5.2578" y1="9.1694" x2="5.3086" y2="9.2202" layer="37"/>
<rectangle x1="10.8458" y1="9.1694" x2="10.8966" y2="9.2202" layer="37"/>
<rectangle x1="2.921" y1="9.2202" x2="3.429" y2="9.271" layer="37"/>
<rectangle x1="4.191" y1="9.2202" x2="5.207" y2="9.271" layer="37"/>
<rectangle x1="6.1214" y1="9.2202" x2="6.7818" y2="9.271" layer="37"/>
<rectangle x1="7.8994" y1="9.2202" x2="8.4074" y2="9.271" layer="37"/>
<rectangle x1="9.1694" y1="9.2202" x2="10.1854" y2="9.271" layer="37"/>
<rectangle x1="10.8966" y1="9.2202" x2="11.303" y2="9.271" layer="37"/>
<rectangle x1="8.4074" y1="9.2202" x2="8.4582" y2="9.271" layer="37"/>
<rectangle x1="5.207" y1="9.2202" x2="5.2578" y2="9.271" layer="37"/>
<rectangle x1="7.8486" y1="9.2202" x2="7.8994" y2="9.271" layer="37"/>
<rectangle x1="9.1186" y1="9.2202" x2="9.1694" y2="9.271" layer="37"/>
<rectangle x1="6.0706" y1="9.2202" x2="6.1214" y2="9.271" layer="37"/>
<rectangle x1="6.7818" y1="9.2202" x2="6.8326" y2="9.271" layer="37"/>
<rectangle x1="10.8458" y1="9.2202" x2="10.8966" y2="9.271" layer="37"/>
<rectangle x1="2.921" y1="9.271" x2="3.429" y2="9.3218" layer="37"/>
<rectangle x1="4.191" y1="9.271" x2="5.1562" y2="9.3218" layer="37"/>
<rectangle x1="6.0706" y1="9.271" x2="6.731" y2="9.3218" layer="37"/>
<rectangle x1="7.9502" y1="9.271" x2="8.4582" y2="9.3218" layer="37"/>
<rectangle x1="9.2202" y1="9.271" x2="10.1854" y2="9.3218" layer="37"/>
<rectangle x1="10.8966" y1="9.271" x2="11.303" y2="9.3218" layer="37"/>
<rectangle x1="9.1694" y1="9.271" x2="9.2202" y2="9.3218" layer="37"/>
<rectangle x1="5.1562" y1="9.271" x2="5.207" y2="9.3218" layer="37"/>
<rectangle x1="7.8994" y1="9.271" x2="7.9502" y2="9.3218" layer="37"/>
<rectangle x1="6.731" y1="9.271" x2="6.7818" y2="9.3218" layer="37"/>
<rectangle x1="8.4582" y1="9.271" x2="8.509" y2="9.3218" layer="37"/>
<rectangle x1="10.8458" y1="9.271" x2="10.8966" y2="9.3218" layer="37"/>
<rectangle x1="2.921" y1="9.3218" x2="3.429" y2="9.3726" layer="37"/>
<rectangle x1="4.191" y1="9.3218" x2="5.1054" y2="9.3726" layer="37"/>
<rectangle x1="6.0706" y1="9.3218" x2="6.6802" y2="9.3726" layer="37"/>
<rectangle x1="7.9502" y1="9.3218" x2="8.509" y2="9.3726" layer="37"/>
<rectangle x1="9.2202" y1="9.3218" x2="10.1854" y2="9.3726" layer="37"/>
<rectangle x1="10.8966" y1="9.3218" x2="11.303" y2="9.3726" layer="37"/>
<rectangle x1="5.1054" y1="9.3218" x2="5.1562" y2="9.3726" layer="37"/>
<rectangle x1="6.0198" y1="9.3218" x2="6.0706" y2="9.3726" layer="37"/>
<rectangle x1="6.6802" y1="9.3218" x2="6.731" y2="9.3726" layer="37"/>
<rectangle x1="10.8458" y1="9.3218" x2="10.8966" y2="9.3726" layer="37"/>
<rectangle x1="2.921" y1="9.3726" x2="3.429" y2="9.4234" layer="37"/>
<rectangle x1="4.191" y1="9.3726" x2="5.1054" y2="9.4234" layer="37"/>
<rectangle x1="6.0198" y1="9.3726" x2="6.6802" y2="9.4234" layer="37"/>
<rectangle x1="8.001" y1="9.3726" x2="8.509" y2="9.4234" layer="37"/>
<rectangle x1="9.271" y1="9.3726" x2="10.1854" y2="9.4234" layer="37"/>
<rectangle x1="10.8966" y1="9.3726" x2="11.303" y2="9.4234" layer="37"/>
<rectangle x1="8.509" y1="9.3726" x2="8.5598" y2="9.4234" layer="37"/>
<rectangle x1="5.969" y1="9.3726" x2="6.0198" y2="9.4234" layer="37"/>
<rectangle x1="7.9502" y1="9.3726" x2="8.001" y2="9.4234" layer="37"/>
<rectangle x1="10.8458" y1="9.3726" x2="10.8966" y2="9.4234" layer="37"/>
<rectangle x1="2.921" y1="9.4234" x2="3.429" y2="9.4742" layer="37"/>
<rectangle x1="4.191" y1="9.4234" x2="5.0546" y2="9.4742" layer="37"/>
<rectangle x1="5.969" y1="9.4234" x2="6.6294" y2="9.4742" layer="37"/>
<rectangle x1="8.0518" y1="9.4234" x2="8.5598" y2="9.4742" layer="37"/>
<rectangle x1="9.3218" y1="9.4234" x2="10.1854" y2="9.4742" layer="37"/>
<rectangle x1="10.8966" y1="9.4234" x2="11.303" y2="9.4742" layer="37"/>
<rectangle x1="8.001" y1="9.4234" x2="8.0518" y2="9.4742" layer="37"/>
<rectangle x1="8.5598" y1="9.4234" x2="8.6106" y2="9.4742" layer="37"/>
<rectangle x1="5.9182" y1="9.4234" x2="5.969" y2="9.4742" layer="37"/>
<rectangle x1="9.271" y1="9.4234" x2="9.3218" y2="9.4742" layer="37"/>
<rectangle x1="10.8458" y1="9.4234" x2="10.8966" y2="9.4742" layer="37"/>
<rectangle x1="5.0546" y1="9.4234" x2="5.1054" y2="9.4742" layer="37"/>
<rectangle x1="2.921" y1="9.4742" x2="3.429" y2="9.525" layer="37"/>
<rectangle x1="4.191" y1="9.4742" x2="5.0038" y2="9.525" layer="37"/>
<rectangle x1="5.9182" y1="9.4742" x2="6.5786" y2="9.525" layer="37"/>
<rectangle x1="8.1026" y1="9.4742" x2="8.6106" y2="9.525" layer="37"/>
<rectangle x1="9.3726" y1="9.4742" x2="10.1854" y2="9.525" layer="37"/>
<rectangle x1="10.8966" y1="9.4742" x2="11.303" y2="9.525" layer="37"/>
<rectangle x1="8.0518" y1="9.4742" x2="8.1026" y2="9.525" layer="37"/>
<rectangle x1="9.3218" y1="9.4742" x2="9.3726" y2="9.525" layer="37"/>
<rectangle x1="5.0038" y1="9.4742" x2="5.0546" y2="9.525" layer="37"/>
<rectangle x1="5.8674" y1="9.4742" x2="5.9182" y2="9.525" layer="37"/>
<rectangle x1="8.6106" y1="9.4742" x2="8.6614" y2="9.525" layer="37"/>
<rectangle x1="10.8458" y1="9.4742" x2="10.8966" y2="9.525" layer="37"/>
<rectangle x1="2.921" y1="9.525" x2="3.429" y2="9.5758" layer="37"/>
<rectangle x1="4.191" y1="9.525" x2="4.953" y2="9.5758" layer="37"/>
<rectangle x1="5.8674" y1="9.525" x2="6.5278" y2="9.5758" layer="37"/>
<rectangle x1="8.1534" y1="9.525" x2="8.6614" y2="9.5758" layer="37"/>
<rectangle x1="9.4234" y1="9.525" x2="10.1854" y2="9.5758" layer="37"/>
<rectangle x1="10.8966" y1="9.525" x2="11.303" y2="9.5758" layer="37"/>
<rectangle x1="8.1026" y1="9.525" x2="8.1534" y2="9.5758" layer="37"/>
<rectangle x1="9.3726" y1="9.525" x2="9.4234" y2="9.5758" layer="37"/>
<rectangle x1="4.953" y1="9.525" x2="5.0038" y2="9.5758" layer="37"/>
<rectangle x1="6.5278" y1="9.525" x2="6.5786" y2="9.5758" layer="37"/>
<rectangle x1="10.8458" y1="9.525" x2="10.8966" y2="9.5758" layer="37"/>
<rectangle x1="5.8166" y1="9.525" x2="5.8674" y2="9.5758" layer="37"/>
<rectangle x1="2.921" y1="9.5758" x2="3.429" y2="9.6266" layer="37"/>
<rectangle x1="4.191" y1="9.5758" x2="4.9022" y2="9.6266" layer="37"/>
<rectangle x1="5.8166" y1="9.5758" x2="6.477" y2="9.6266" layer="37"/>
<rectangle x1="8.1534" y1="9.5758" x2="8.7122" y2="9.6266" layer="37"/>
<rectangle x1="9.4234" y1="9.5758" x2="10.1854" y2="9.6266" layer="37"/>
<rectangle x1="10.8966" y1="9.5758" x2="11.303" y2="9.6266" layer="37"/>
<rectangle x1="4.9022" y1="9.5758" x2="4.953" y2="9.6266" layer="37"/>
<rectangle x1="8.1026" y1="9.5758" x2="8.1534" y2="9.6266" layer="37"/>
<rectangle x1="6.477" y1="9.5758" x2="6.5278" y2="9.6266" layer="37"/>
<rectangle x1="10.8458" y1="9.5758" x2="10.8966" y2="9.6266" layer="37"/>
<rectangle x1="2.921" y1="9.6266" x2="3.429" y2="9.6774" layer="37"/>
<rectangle x1="4.191" y1="9.6266" x2="4.9022" y2="9.6774" layer="37"/>
<rectangle x1="5.8166" y1="9.6266" x2="6.4262" y2="9.6774" layer="37"/>
<rectangle x1="8.2042" y1="9.6266" x2="8.7122" y2="9.6774" layer="37"/>
<rectangle x1="9.4742" y1="9.6266" x2="10.1854" y2="9.6774" layer="37"/>
<rectangle x1="10.8966" y1="9.6266" x2="11.303" y2="9.6774" layer="37"/>
<rectangle x1="5.7658" y1="9.6266" x2="5.8166" y2="9.6774" layer="37"/>
<rectangle x1="8.1534" y1="9.6266" x2="8.2042" y2="9.6774" layer="37"/>
<rectangle x1="6.4262" y1="9.6266" x2="6.477" y2="9.6774" layer="37"/>
<rectangle x1="8.7122" y1="9.6266" x2="8.763" y2="9.6774" layer="37"/>
<rectangle x1="10.8458" y1="9.6266" x2="10.8966" y2="9.6774" layer="37"/>
<rectangle x1="2.921" y1="9.6774" x2="3.429" y2="9.7282" layer="37"/>
<rectangle x1="4.191" y1="9.6774" x2="4.8514" y2="9.7282" layer="37"/>
<rectangle x1="5.7658" y1="9.6774" x2="6.3754" y2="9.7282" layer="37"/>
<rectangle x1="8.2042" y1="9.6774" x2="8.763" y2="9.7282" layer="37"/>
<rectangle x1="9.525" y1="9.6774" x2="10.1854" y2="9.7282" layer="37"/>
<rectangle x1="10.8966" y1="9.6774" x2="11.303" y2="9.7282" layer="37"/>
<rectangle x1="6.3754" y1="9.6774" x2="6.4262" y2="9.7282" layer="37"/>
<rectangle x1="5.715" y1="9.6774" x2="5.7658" y2="9.7282" layer="37"/>
<rectangle x1="9.4742" y1="9.6774" x2="9.525" y2="9.7282" layer="37"/>
<rectangle x1="10.8458" y1="9.6774" x2="10.8966" y2="9.7282" layer="37"/>
<rectangle x1="2.921" y1="9.7282" x2="3.429" y2="9.779" layer="37"/>
<rectangle x1="4.191" y1="9.7282" x2="4.8006" y2="9.779" layer="37"/>
<rectangle x1="5.715" y1="9.7282" x2="6.3246" y2="9.779" layer="37"/>
<rectangle x1="8.255" y1="9.7282" x2="8.8138" y2="9.779" layer="37"/>
<rectangle x1="9.5758" y1="9.7282" x2="10.1854" y2="9.779" layer="37"/>
<rectangle x1="10.8966" y1="9.7282" x2="11.303" y2="9.779" layer="37"/>
<rectangle x1="6.3246" y1="9.7282" x2="6.3754" y2="9.779" layer="37"/>
<rectangle x1="9.525" y1="9.7282" x2="9.5758" y2="9.779" layer="37"/>
<rectangle x1="5.6642" y1="9.7282" x2="5.715" y2="9.779" layer="37"/>
<rectangle x1="8.2042" y1="9.7282" x2="8.255" y2="9.779" layer="37"/>
<rectangle x1="10.8458" y1="9.7282" x2="10.8966" y2="9.779" layer="37"/>
<rectangle x1="4.8006" y1="9.7282" x2="4.8514" y2="9.779" layer="37"/>
<rectangle x1="2.921" y1="9.779" x2="3.429" y2="9.8298" layer="37"/>
<rectangle x1="4.191" y1="9.779" x2="4.7498" y2="9.8298" layer="37"/>
<rectangle x1="5.6642" y1="9.779" x2="6.3246" y2="9.8298" layer="37"/>
<rectangle x1="8.3058" y1="9.779" x2="8.8138" y2="9.8298" layer="37"/>
<rectangle x1="9.6266" y1="9.779" x2="10.1346" y2="9.8298" layer="37"/>
<rectangle x1="10.8966" y1="9.779" x2="11.303" y2="9.8298" layer="37"/>
<rectangle x1="9.5758" y1="9.779" x2="9.6266" y2="9.8298" layer="37"/>
<rectangle x1="8.255" y1="9.779" x2="8.3058" y2="9.8298" layer="37"/>
<rectangle x1="10.1346" y1="9.779" x2="10.1854" y2="9.8298" layer="37"/>
<rectangle x1="8.8138" y1="9.779" x2="8.8646" y2="9.8298" layer="37"/>
<rectangle x1="4.7498" y1="9.779" x2="4.8006" y2="9.8298" layer="37"/>
<rectangle x1="5.6134" y1="9.779" x2="5.6642" y2="9.8298" layer="37"/>
<rectangle x1="10.8458" y1="9.779" x2="10.8966" y2="9.8298" layer="37"/>
<rectangle x1="2.921" y1="9.8298" x2="3.429" y2="9.8806" layer="37"/>
<rectangle x1="4.191" y1="9.8298" x2="4.699" y2="9.8806" layer="37"/>
<rectangle x1="5.6134" y1="9.8298" x2="6.2738" y2="9.8806" layer="37"/>
<rectangle x1="8.3058" y1="9.8298" x2="8.8646" y2="9.8806" layer="37"/>
<rectangle x1="9.6266" y1="9.8298" x2="10.1346" y2="9.8806" layer="37"/>
<rectangle x1="10.8966" y1="9.8298" x2="11.303" y2="9.8806" layer="37"/>
<rectangle x1="4.699" y1="9.8298" x2="4.7498" y2="9.8806" layer="37"/>
<rectangle x1="6.2738" y1="9.8298" x2="6.3246" y2="9.8806" layer="37"/>
<rectangle x1="10.8458" y1="9.8298" x2="10.8966" y2="9.8806" layer="37"/>
<rectangle x1="8.8646" y1="9.8298" x2="8.9154" y2="9.8806" layer="37"/>
<rectangle x1="2.921" y1="9.8806" x2="3.429" y2="9.9314" layer="37"/>
<rectangle x1="4.2418" y1="9.8806" x2="4.6482" y2="9.9314" layer="37"/>
<rectangle x1="5.5626" y1="9.8806" x2="6.223" y2="9.9314" layer="37"/>
<rectangle x1="8.3566" y1="9.8806" x2="8.9154" y2="9.9314" layer="37"/>
<rectangle x1="9.6774" y1="9.8806" x2="10.1346" y2="9.9314" layer="37"/>
<rectangle x1="10.8966" y1="9.8806" x2="11.303" y2="9.9314" layer="37"/>
<rectangle x1="4.6482" y1="9.8806" x2="4.699" y2="9.9314" layer="37"/>
<rectangle x1="4.191" y1="9.8806" x2="4.2418" y2="9.9314" layer="37"/>
<rectangle x1="6.223" y1="9.8806" x2="6.2738" y2="9.9314" layer="37"/>
<rectangle x1="9.6266" y1="9.8806" x2="9.6774" y2="9.9314" layer="37"/>
<rectangle x1="10.8458" y1="9.8806" x2="10.8966" y2="9.9314" layer="37"/>
<rectangle x1="2.921" y1="9.9314" x2="3.429" y2="9.9822" layer="37"/>
<rectangle x1="4.2418" y1="9.9314" x2="4.5974" y2="9.9822" layer="37"/>
<rectangle x1="5.5626" y1="9.9314" x2="6.1722" y2="9.9822" layer="37"/>
<rectangle x1="8.4074" y1="9.9314" x2="8.9154" y2="9.9822" layer="37"/>
<rectangle x1="9.7282" y1="9.9314" x2="10.0838" y2="9.9822" layer="37"/>
<rectangle x1="10.8966" y1="9.9314" x2="11.303" y2="9.9822" layer="37"/>
<rectangle x1="4.5974" y1="9.9314" x2="4.6482" y2="9.9822" layer="37"/>
<rectangle x1="5.5118" y1="9.9314" x2="5.5626" y2="9.9822" layer="37"/>
<rectangle x1="8.9154" y1="9.9314" x2="8.9662" y2="9.9822" layer="37"/>
<rectangle x1="6.1722" y1="9.9314" x2="6.223" y2="9.9822" layer="37"/>
<rectangle x1="8.3566" y1="9.9314" x2="8.4074" y2="9.9822" layer="37"/>
<rectangle x1="9.6774" y1="9.9314" x2="9.7282" y2="9.9822" layer="37"/>
<rectangle x1="10.8458" y1="9.9314" x2="10.8966" y2="9.9822" layer="37"/>
<rectangle x1="10.0838" y1="9.9314" x2="10.1346" y2="9.9822" layer="37"/>
<rectangle x1="2.921" y1="9.9822" x2="3.429" y2="10.033" layer="37"/>
<rectangle x1="4.3434" y1="9.9822" x2="4.5466" y2="10.033" layer="37"/>
<rectangle x1="5.5118" y1="9.9822" x2="6.1214" y2="10.033" layer="37"/>
<rectangle x1="8.4074" y1="9.9822" x2="8.9662" y2="10.033" layer="37"/>
<rectangle x1="9.779" y1="9.9822" x2="9.9822" y2="10.033" layer="37"/>
<rectangle x1="10.8966" y1="9.9822" x2="11.303" y2="10.033" layer="37"/>
<rectangle x1="6.1214" y1="9.9822" x2="6.1722" y2="10.033" layer="37"/>
<rectangle x1="9.9822" y1="9.9822" x2="10.033" y2="10.033" layer="37"/>
<rectangle x1="4.5466" y1="9.9822" x2="4.5974" y2="10.033" layer="37"/>
<rectangle x1="4.2926" y1="9.9822" x2="4.3434" y2="10.033" layer="37"/>
<rectangle x1="5.461" y1="9.9822" x2="5.5118" y2="10.033" layer="37"/>
<rectangle x1="8.9662" y1="9.9822" x2="9.017" y2="10.033" layer="37"/>
<rectangle x1="10.8458" y1="9.9822" x2="10.8966" y2="10.033" layer="37"/>
<rectangle x1="9.7282" y1="9.9822" x2="9.779" y2="10.033" layer="37"/>
<rectangle x1="2.921" y1="10.033" x2="3.429" y2="10.0838" layer="37"/>
<rectangle x1="5.461" y1="10.033" x2="6.0706" y2="10.0838" layer="37"/>
<rectangle x1="8.4582" y1="10.033" x2="9.017" y2="10.0838" layer="37"/>
<rectangle x1="10.8966" y1="10.033" x2="11.303" y2="10.0838" layer="37"/>
<rectangle x1="6.0706" y1="10.033" x2="6.1214" y2="10.0838" layer="37"/>
<rectangle x1="5.4102" y1="10.033" x2="5.461" y2="10.0838" layer="37"/>
<rectangle x1="10.8458" y1="10.033" x2="10.8966" y2="10.0838" layer="37"/>
<rectangle x1="2.921" y1="10.0838" x2="3.429" y2="10.1346" layer="37"/>
<rectangle x1="5.4102" y1="10.0838" x2="6.0706" y2="10.1346" layer="37"/>
<rectangle x1="8.509" y1="10.0838" x2="9.0678" y2="10.1346" layer="37"/>
<rectangle x1="10.8966" y1="10.0838" x2="11.303" y2="10.1346" layer="37"/>
<rectangle x1="8.4582" y1="10.0838" x2="8.509" y2="10.1346" layer="37"/>
<rectangle x1="10.8458" y1="10.0838" x2="10.8966" y2="10.1346" layer="37"/>
<rectangle x1="6.0706" y1="10.0838" x2="6.1214" y2="10.1346" layer="37"/>
<rectangle x1="2.921" y1="10.1346" x2="3.429" y2="10.1854" layer="37"/>
<rectangle x1="5.3594" y1="10.1346" x2="6.0198" y2="10.1854" layer="37"/>
<rectangle x1="8.5598" y1="10.1346" x2="9.0678" y2="10.1854" layer="37"/>
<rectangle x1="10.8966" y1="10.1346" x2="11.303" y2="10.1854" layer="37"/>
<rectangle x1="8.509" y1="10.1346" x2="8.5598" y2="10.1854" layer="37"/>
<rectangle x1="9.0678" y1="10.1346" x2="9.1186" y2="10.1854" layer="37"/>
<rectangle x1="6.0198" y1="10.1346" x2="6.0706" y2="10.1854" layer="37"/>
<rectangle x1="10.8458" y1="10.1346" x2="10.8966" y2="10.1854" layer="37"/>
<rectangle x1="2.921" y1="10.1854" x2="3.429" y2="10.2362" layer="37"/>
<rectangle x1="5.3086" y1="10.1854" x2="5.969" y2="10.2362" layer="37"/>
<rectangle x1="8.5598" y1="10.1854" x2="9.1186" y2="10.2362" layer="37"/>
<rectangle x1="10.8966" y1="10.1854" x2="11.303" y2="10.2362" layer="37"/>
<rectangle x1="5.969" y1="10.1854" x2="6.0198" y2="10.2362" layer="37"/>
<rectangle x1="10.8458" y1="10.1854" x2="10.8966" y2="10.2362" layer="37"/>
<rectangle x1="9.1186" y1="10.1854" x2="9.1694" y2="10.2362" layer="37"/>
<rectangle x1="2.921" y1="10.2362" x2="3.429" y2="10.287" layer="37"/>
<rectangle x1="5.3086" y1="10.2362" x2="5.969" y2="10.287" layer="37"/>
<rectangle x1="8.6106" y1="10.2362" x2="9.1694" y2="10.287" layer="37"/>
<rectangle x1="10.8966" y1="10.2362" x2="11.303" y2="10.287" layer="37"/>
<rectangle x1="5.2578" y1="10.2362" x2="5.3086" y2="10.287" layer="37"/>
<rectangle x1="10.8458" y1="10.2362" x2="10.8966" y2="10.287" layer="37"/>
<rectangle x1="2.921" y1="10.287" x2="3.429" y2="10.3378" layer="37"/>
<rectangle x1="5.2578" y1="10.287" x2="5.9182" y2="10.3378" layer="37"/>
<rectangle x1="8.6614" y1="10.287" x2="9.1694" y2="10.3378" layer="37"/>
<rectangle x1="10.8966" y1="10.287" x2="11.303" y2="10.3378" layer="37"/>
<rectangle x1="9.1694" y1="10.287" x2="9.2202" y2="10.3378" layer="37"/>
<rectangle x1="8.6106" y1="10.287" x2="8.6614" y2="10.3378" layer="37"/>
<rectangle x1="5.207" y1="10.287" x2="5.2578" y2="10.3378" layer="37"/>
<rectangle x1="10.8458" y1="10.287" x2="10.8966" y2="10.3378" layer="37"/>
<rectangle x1="2.921" y1="10.3378" x2="3.429" y2="10.3886" layer="37"/>
<rectangle x1="5.207" y1="10.3378" x2="5.8674" y2="10.3886" layer="37"/>
<rectangle x1="8.6614" y1="10.3378" x2="9.2202" y2="10.3886" layer="37"/>
<rectangle x1="10.8966" y1="10.3378" x2="11.303" y2="10.3886" layer="37"/>
<rectangle x1="9.2202" y1="10.3378" x2="9.271" y2="10.3886" layer="37"/>
<rectangle x1="5.1562" y1="10.3378" x2="5.207" y2="10.3886" layer="37"/>
<rectangle x1="10.8458" y1="10.3378" x2="10.8966" y2="10.3886" layer="37"/>
<rectangle x1="2.921" y1="10.3886" x2="3.429" y2="10.4394" layer="37"/>
<rectangle x1="5.1562" y1="10.3886" x2="5.8166" y2="10.4394" layer="37"/>
<rectangle x1="8.7122" y1="10.3886" x2="9.2202" y2="10.4394" layer="37"/>
<rectangle x1="10.8966" y1="10.3886" x2="11.303" y2="10.4394" layer="37"/>
<rectangle x1="9.2202" y1="10.3886" x2="9.271" y2="10.4394" layer="37"/>
<rectangle x1="5.8166" y1="10.3886" x2="5.8674" y2="10.4394" layer="37"/>
<rectangle x1="10.8458" y1="10.3886" x2="10.8966" y2="10.4394" layer="37"/>
<rectangle x1="2.921" y1="10.4394" x2="3.429" y2="10.4902" layer="37"/>
<rectangle x1="5.1054" y1="10.4394" x2="5.7658" y2="10.4902" layer="37"/>
<rectangle x1="8.763" y1="10.4394" x2="9.271" y2="10.4902" layer="37"/>
<rectangle x1="10.8966" y1="10.4394" x2="11.303" y2="10.4902" layer="37"/>
<rectangle x1="9.271" y1="10.4394" x2="9.3218" y2="10.4902" layer="37"/>
<rectangle x1="5.7658" y1="10.4394" x2="5.8166" y2="10.4902" layer="37"/>
<rectangle x1="8.7122" y1="10.4394" x2="8.763" y2="10.4902" layer="37"/>
<rectangle x1="10.8458" y1="10.4394" x2="10.8966" y2="10.4902" layer="37"/>
<rectangle x1="2.921" y1="10.4902" x2="3.429" y2="10.541" layer="37"/>
<rectangle x1="5.1054" y1="10.4902" x2="5.715" y2="10.541" layer="37"/>
<rectangle x1="8.8138" y1="10.4902" x2="9.3218" y2="10.541" layer="37"/>
<rectangle x1="10.8966" y1="10.4902" x2="11.303" y2="10.541" layer="37"/>
<rectangle x1="5.0546" y1="10.4902" x2="5.1054" y2="10.541" layer="37"/>
<rectangle x1="8.763" y1="10.4902" x2="8.8138" y2="10.541" layer="37"/>
<rectangle x1="5.715" y1="10.4902" x2="5.7658" y2="10.541" layer="37"/>
<rectangle x1="9.3218" y1="10.4902" x2="9.3726" y2="10.541" layer="37"/>
<rectangle x1="10.8458" y1="10.4902" x2="10.8966" y2="10.541" layer="37"/>
<rectangle x1="2.921" y1="10.541" x2="3.429" y2="10.5918" layer="37"/>
<rectangle x1="5.0546" y1="10.541" x2="5.6642" y2="10.5918" layer="37"/>
<rectangle x1="8.8646" y1="10.541" x2="9.3218" y2="10.5918" layer="37"/>
<rectangle x1="10.8966" y1="10.541" x2="11.303" y2="10.5918" layer="37"/>
<rectangle x1="5.0038" y1="10.541" x2="5.0546" y2="10.5918" layer="37"/>
<rectangle x1="5.6642" y1="10.541" x2="5.715" y2="10.5918" layer="37"/>
<rectangle x1="8.8138" y1="10.541" x2="8.8646" y2="10.5918" layer="37"/>
<rectangle x1="9.3218" y1="10.541" x2="9.3726" y2="10.5918" layer="37"/>
<rectangle x1="10.8458" y1="10.541" x2="10.8966" y2="10.5918" layer="37"/>
<rectangle x1="2.921" y1="10.5918" x2="3.429" y2="10.6426" layer="37"/>
<rectangle x1="4.953" y1="10.5918" x2="5.6134" y2="10.6426" layer="37"/>
<rectangle x1="8.8646" y1="10.5918" x2="9.4234" y2="10.6426" layer="37"/>
<rectangle x1="10.8966" y1="10.5918" x2="11.303" y2="10.6426" layer="37"/>
<rectangle x1="5.6134" y1="10.5918" x2="5.6642" y2="10.6426" layer="37"/>
<rectangle x1="8.8138" y1="10.5918" x2="8.8646" y2="10.6426" layer="37"/>
<rectangle x1="4.9022" y1="10.5918" x2="4.953" y2="10.6426" layer="37"/>
<rectangle x1="10.8458" y1="10.5918" x2="10.8966" y2="10.6426" layer="37"/>
<rectangle x1="2.921" y1="10.6426" x2="3.429" y2="10.6934" layer="37"/>
<rectangle x1="4.1402" y1="10.6426" x2="4.191" y2="10.6934" layer="37"/>
<rectangle x1="4.8514" y1="10.6426" x2="5.6134" y2="10.6934" layer="37"/>
<rectangle x1="8.9154" y1="10.6426" x2="9.525" y2="10.6934" layer="37"/>
<rectangle x1="10.8966" y1="10.6426" x2="11.303" y2="10.6934" layer="37"/>
<rectangle x1="4.8006" y1="10.6426" x2="4.8514" y2="10.6934" layer="37"/>
<rectangle x1="10.8458" y1="10.6426" x2="10.8966" y2="10.6934" layer="37"/>
<rectangle x1="4.191" y1="10.6426" x2="4.2418" y2="10.6934" layer="37"/>
<rectangle x1="3.429" y1="10.6426" x2="3.4798" y2="10.6934" layer="37"/>
<rectangle x1="8.8646" y1="10.6426" x2="8.9154" y2="10.6934" layer="37"/>
<rectangle x1="4.0894" y1="10.6426" x2="4.1402" y2="10.6934" layer="37"/>
<rectangle x1="9.525" y1="10.6426" x2="9.5758" y2="10.6934" layer="37"/>
<rectangle x1="4.7498" y1="10.6426" x2="4.8006" y2="10.6934" layer="37"/>
<rectangle x1="10.795" y1="10.6426" x2="10.8458" y2="10.6934" layer="37"/>
<rectangle x1="3.4798" y1="10.6426" x2="3.5306" y2="10.6934" layer="37"/>
<rectangle x1="3.5306" y1="10.6426" x2="4.0894" y2="10.6934" layer="37"/>
<rectangle x1="4.2418" y1="10.6426" x2="4.6482" y2="10.6934" layer="37"/>
<rectangle x1="9.5758" y1="10.6426" x2="10.795" y2="10.6934" layer="37"/>
<rectangle x1="2.921" y1="10.6934" x2="5.5626" y2="10.7442" layer="37"/>
<rectangle x1="8.9662" y1="10.6934" x2="11.303" y2="10.7442" layer="37"/>
<rectangle x1="8.9154" y1="10.6934" x2="8.9662" y2="10.7442" layer="37"/>
<rectangle x1="5.5626" y1="10.6934" x2="5.6134" y2="10.7442" layer="37"/>
<rectangle x1="2.921" y1="10.7442" x2="5.5118" y2="10.795" layer="37"/>
<rectangle x1="8.9662" y1="10.7442" x2="11.303" y2="10.795" layer="37"/>
<rectangle x1="5.5118" y1="10.7442" x2="5.5626" y2="10.795" layer="37"/>
<rectangle x1="2.921" y1="10.795" x2="5.461" y2="10.8458" layer="37"/>
<rectangle x1="9.017" y1="10.795" x2="11.303" y2="10.8458" layer="37"/>
<rectangle x1="5.461" y1="10.795" x2="5.5118" y2="10.8458" layer="37"/>
<rectangle x1="8.9662" y1="10.795" x2="9.017" y2="10.8458" layer="37"/>
<rectangle x1="2.921" y1="10.8458" x2="5.4102" y2="10.8966" layer="37"/>
<rectangle x1="9.0678" y1="10.8458" x2="11.303" y2="10.8966" layer="37"/>
<rectangle x1="9.017" y1="10.8458" x2="9.0678" y2="10.8966" layer="37"/>
<rectangle x1="5.4102" y1="10.8458" x2="5.461" y2="10.8966" layer="37"/>
<rectangle x1="2.921" y1="10.8966" x2="5.3594" y2="10.9474" layer="37"/>
<rectangle x1="9.1186" y1="10.8966" x2="11.303" y2="10.9474" layer="37"/>
<rectangle x1="5.3594" y1="10.8966" x2="5.4102" y2="10.9474" layer="37"/>
<rectangle x1="9.0678" y1="10.8966" x2="9.1186" y2="10.9474" layer="37"/>
<rectangle x1="2.921" y1="10.9474" x2="5.3594" y2="10.9982" layer="37"/>
<rectangle x1="9.1186" y1="10.9474" x2="11.303" y2="10.9982" layer="37"/>
<rectangle x1="9.0678" y1="10.9474" x2="9.1186" y2="10.9982" layer="37"/>
<rectangle x1="2.9718" y1="10.9982" x2="5.3086" y2="11.049" layer="37"/>
<rectangle x1="9.1694" y1="10.9982" x2="11.2522" y2="11.049" layer="37"/>
<rectangle x1="2.921" y1="10.9982" x2="2.9718" y2="11.049" layer="37"/>
<rectangle x1="11.2522" y1="10.9982" x2="11.303" y2="11.049" layer="37"/>
<rectangle x1="9.1186" y1="10.9982" x2="9.1694" y2="11.049" layer="37"/>
<rectangle x1="5.3086" y1="10.9982" x2="5.3594" y2="11.049" layer="37"/>
<rectangle x1="2.9718" y1="11.049" x2="5.2578" y2="11.0998" layer="37"/>
<rectangle x1="9.2202" y1="11.049" x2="11.2522" y2="11.0998" layer="37"/>
<rectangle x1="9.1694" y1="11.049" x2="9.2202" y2="11.0998" layer="37"/>
<rectangle x1="11.2522" y1="11.049" x2="11.303" y2="11.0998" layer="37"/>
<rectangle x1="5.2578" y1="11.049" x2="5.3086" y2="11.0998" layer="37"/>
<rectangle x1="2.921" y1="11.049" x2="2.9718" y2="11.0998" layer="37"/>
<rectangle x1="3.0226" y1="11.0998" x2="5.207" y2="11.1506" layer="37"/>
<rectangle x1="9.2202" y1="11.0998" x2="11.2522" y2="11.1506" layer="37"/>
<rectangle x1="2.9718" y1="11.0998" x2="3.0226" y2="11.1506" layer="37"/>
<rectangle x1="5.207" y1="11.0998" x2="5.2578" y2="11.1506" layer="37"/>
<rectangle x1="3.0734" y1="11.1506" x2="5.1054" y2="11.2014" layer="37"/>
<rectangle x1="9.3218" y1="11.1506" x2="11.1506" y2="11.2014" layer="37"/>
<rectangle x1="5.1054" y1="11.1506" x2="5.1562" y2="11.2014" layer="37"/>
<rectangle x1="9.271" y1="11.1506" x2="9.3218" y2="11.2014" layer="37"/>
<rectangle x1="11.1506" y1="11.1506" x2="11.2014" y2="11.2014" layer="37"/>
</package>
</packages>
<symbols>
<symbol name="MUTEX-LOGO">
<text x="0" y="0" size="2.54" layer="94" font="vector">LOGO</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MUTEX-LOGO" prefix="LOGO">
<gates>
<gate name="G$1" symbol="MUTEX-LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MUTEX-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="CNAME" value="aasd"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="MCU1" library="CC1310Misc" deviceset="CC2640RGZ" device=""/>
<part name="E1" library="SparkFun-RF" deviceset="ANTENNA-GROUNDED" device="TRACE-25.7MM" value="PCB-ANTENNA"/>
<part name="U1" library="SparkFun-IC-Conversion" deviceset="MAX31855K" device="" value="MAX31855K"/>
<part name="U2" library="SparkFun-IC-Conversion" deviceset="MAX31855K" device="" value="MAX31855K"/>
<part name="U3" library="SparkFun-IC-Conversion" deviceset="MAX31855K" device="" value="MAX31855K"/>
<part name="U4" library="SparkFun-IC-Conversion" deviceset="MAX31855K" device="" value="MAX31855K"/>
<part name="R1" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="Y1" library="SparkFun-Clocks" deviceset="CRYSTAL-26MHZ" device="SMD-3.2X2.5" value="TSX-3225-24MHz"/>
<part name="Y2" library="SparkFun-Clocks" deviceset="CRYSTAL" device="SMD-3.2X1.5" value="FC-135-32.768KHz"/>
<part name="C3" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" value="12pF"/>
<part name="C4" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" value="12pF"/>
<part name="U5" library="SparkFun-IC-Power" deviceset="BQ27441-G1" device="" value="BQ27441-G1"/>
<part name="U6" library="SparkFun-IC-Power" deviceset="BQ24075" device="" value="BQ24075"/>
<part name="J2" library="SparkFun-Connectors" deviceset="JST_2MM_MALE" device=""/>
<part name="FB1" library="SparkFun-Coils" deviceset="FERRITE_BEAD" device="-0603" value="BLM18HE152SN1"/>
<part name="L1" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="470ohm"/>
<part name="L2" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="470ohm"/>
<part name="L3" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="470ohm"/>
<part name="L4" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="470ohm"/>
<part name="L5" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="470ohm"/>
<part name="L6" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="470ohm"/>
<part name="L7" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="470ohm"/>
<part name="L8" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="470ohm"/>
<part name="C5" library="SparkFun-Capacitors" deviceset="10NF" device="-0603-50V-10%" value="10nF"/>
<part name="C6" library="SparkFun-Capacitors" deviceset="10NF" device="-0603-50V-10%" value="10nF"/>
<part name="C7" library="SparkFun-Capacitors" deviceset="10NF" device="-0603-50V-10%" value="10nF"/>
<part name="C8" library="SparkFun-Capacitors" deviceset="10NF" device="-0603-50V-10%" value="10nF"/>
<part name="C9" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C10" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C11" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C12" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C13" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" value="10uF"/>
<part name="C14" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C15" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C16" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C17" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="GND1" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="L9" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0805-3.3UH" value="10uHy"/>
<part name="C18" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" value="10uF"/>
<part name="C19" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C20" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="GND6" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND9" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C21" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0402-16V-10%" value="1.0uF"/>
<part name="GND10" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C1" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="GND11" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C2" library="SparkFun-Capacitors" deviceset="1.5PF" device="-0603-50V-16.667%" value="1.5pF"/>
<part name="GND13" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="J3" library="SparkFun-Connectors" deviceset="U.FL" device="" value="U.FL"/>
<part name="C22" library="SparkFun-Capacitors" deviceset="1.5PF" device="-0603-50V-16.667%" value="1.5pF"/>
<part name="GND14" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND15" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND16" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="BALUN1" library="CC1310Misc" deviceset="2450BM14G0011" device=""/>
<part name="REG1" library="sensor-subghz" deviceset="TPS6303X" device=""/>
<part name="C23" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" value="10uF"/>
<part name="C24" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" value="10uF"/>
<part name="C25" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" value="10uF"/>
<part name="C26" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="L10" library="CC1310Misc" deviceset="LQM2HPN1R5MG0L" device=""/>
<part name="GND17" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND18" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND19" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND20" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND21" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND22" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="U8" library="SparkFun-IC-Comms" deviceset="FT232RL-BASIC" device="SSOP" value="FT232RL"/>
<part name="C27" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" value="10uF"/>
<part name="C28" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C29" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="FB2" library="SparkFun-Coils" deviceset="FERRITE_BEAD" device="-0603" value="BLM18HE152SN1"/>
<part name="GND23" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND24" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND25" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND26" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND27" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R2" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="330"/>
<part name="R3" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="330"/>
<part name="D1" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="D2" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="R4" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="1k1"/>
<part name="R5" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="590"/>
<part name="R6" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="GND28" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND29" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND30" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND31" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND32" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND33" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R7" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R1206" value="0.01"/>
<part name="C30" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="GND34" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND35" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C31" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="GND36" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R8" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="100k"/>
<part name="GND37" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C32" library="SparkFun-Capacitors" deviceset="10UF" device="-0805-10V-10%" value="10uF"/>
<part name="GND38" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND39" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="JP3" library="SparkFun-Jumpers" deviceset="JUMPER-SMT_3_NO" device="_SILK" value="EN1"/>
<part name="JP4" library="SparkFun-Jumpers" deviceset="JUMPER-SMT_3_NO" device="_SILK" value="EN2"/>
<part name="GND41" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND42" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R9" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="R10" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="D3" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="D4" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="R11" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="330"/>
<part name="R12" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="330"/>
<part name="JP2" library="SparkFun-Jumpers" deviceset="JUMPER-SMT_3_NO" device="_SILK" value="PGOOD"/>
<part name="GND43" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C34" library="SparkFun-Capacitors" deviceset="10UF" device="-0805-10V-10%" value="1uF"/>
<part name="GND44" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R13" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="GND45" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R14" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="R15" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="R16" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="GND46" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND47" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND48" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND49" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND50" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND51" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND52" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND53" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY4" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY5" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY6" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY7" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY8" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY9" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY10" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="C35" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="GND54" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY11" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND55" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY12" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY14" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="J4" library="CC1310Misc" deviceset="JTAG-CC13XX/26XX" device=""/>
<part name="SUPPLY15" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND57" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME3" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME4" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME5" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME6" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="JP1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X8" device=""/>
<part name="C36" library="SparkFun-Capacitors" deviceset="10UF" device="-0805-10V-10%" value="10uF"/>
<part name="S1" library="SparkFun-Switches" deviceset="SWITCH-SPDT" device="-SMD-RIGHT-ANGLE"/>
<part name="R18" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" value="100k"/>
<part name="GND58" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="U7" library="sensor-subghz" deviceset="HDC1080" device=""/>
<part name="J5" library="SparkFun-Connectors" deviceset="USB_MICRO-B" device="REDUCED_PASTE"/>
<part name="SUPPLY13" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="LOGO1" library="aestethics" deviceset="MUTEX-LOGO" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="10.16" y="436.88" size="1.27" layer="97" font="vector">Bypass cerca del FT232</text>
<text x="106.68" y="370.84" size="2.54" layer="94">USB - SERIE</text>
<text x="146.05" y="378.968" size="1.778" layer="94">Ing. Agustin I. Reyes</text>
</plain>
<instances>
<instance part="U8" gate="G$1" x="88.9" y="462.28"/>
<instance part="C27" gate="G$1" x="35.56" y="454.66"/>
<instance part="C28" gate="G$1" x="12.7" y="454.66"/>
<instance part="C29" gate="G$1" x="58.42" y="454.66"/>
<instance part="FB2" gate="G$1" x="23.368" y="472.44" rot="R90"/>
<instance part="GND23" gate="1" x="71.12" y="439.42"/>
<instance part="GND24" gate="1" x="58.42" y="444.5"/>
<instance part="GND25" gate="1" x="35.56" y="444.5"/>
<instance part="GND26" gate="1" x="12.7" y="444.5"/>
<instance part="GND27" gate="1" x="5.08" y="452.12"/>
<instance part="R2" gate="G$1" x="119.38" y="452.12" rot="R180"/>
<instance part="R3" gate="G$1" x="119.38" y="444.5" rot="R180"/>
<instance part="D1" gate="G$1" x="132.08" y="452.12" rot="R270"/>
<instance part="D2" gate="G$1" x="132.08" y="444.5" rot="R270"/>
<instance part="FRAME1" gate="G$1" x="-71.12" y="358.14"/>
<instance part="J5" gate="G$1" x="-7.62" y="467.36" rot="MR0"/>
<instance part="LOGO1" gate="G$1" x="190.5" y="358.14"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="5.08" y1="462.28" x2="5.08" y2="454.66" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="GND"/>
<wire x1="-2.54" y1="462.28" x2="5.08" y2="462.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="12.7" y1="452.12" x2="12.7" y2="447.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="35.56" y1="452.12" x2="35.56" y2="447.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="58.42" y1="452.12" x2="58.42" y2="447.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="TEST"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="73.66" y1="457.2" x2="71.12" y2="457.2" width="0.1524" layer="91"/>
<wire x1="71.12" y1="457.2" x2="71.12" y2="454.66" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="AGND"/>
<wire x1="71.12" y1="454.66" x2="71.12" y2="452.12" width="0.1524" layer="91"/>
<wire x1="71.12" y1="452.12" x2="71.12" y2="449.58" width="0.1524" layer="91"/>
<wire x1="71.12" y1="449.58" x2="71.12" y2="447.04" width="0.1524" layer="91"/>
<wire x1="71.12" y1="447.04" x2="71.12" y2="441.96" width="0.1524" layer="91"/>
<wire x1="73.66" y1="454.66" x2="71.12" y2="454.66" width="0.1524" layer="91"/>
<junction x="71.12" y="454.66"/>
<pinref part="U8" gate="G$1" pin="GND1"/>
<wire x1="73.66" y1="452.12" x2="71.12" y2="452.12" width="0.1524" layer="91"/>
<junction x="71.12" y="452.12"/>
<pinref part="U8" gate="G$1" pin="GND2"/>
<wire x1="73.66" y1="449.58" x2="71.12" y2="449.58" width="0.1524" layer="91"/>
<junction x="71.12" y="449.58"/>
<pinref part="U8" gate="G$1" pin="GND3"/>
<wire x1="73.66" y1="447.04" x2="71.12" y2="447.04" width="0.1524" layer="91"/>
<junction x="71.12" y="447.04"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="3V3OUT"/>
<wire x1="73.66" y1="464.82" x2="66.04" y2="464.82" width="0.1524" layer="91"/>
<wire x1="66.04" y1="464.82" x2="66.04" y2="462.28" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="VCCIO"/>
<wire x1="66.04" y1="462.28" x2="73.66" y2="462.28" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="58.42" y1="459.74" x2="58.42" y2="462.28" width="0.1524" layer="91"/>
<wire x1="58.42" y1="462.28" x2="66.04" y2="462.28" width="0.1524" layer="91"/>
<junction x="66.04" y="462.28"/>
</segment>
</net>
<net name="VUSB" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="134.62" y1="452.12" x2="139.7" y2="452.12" width="0.1524" layer="91"/>
<wire x1="139.7" y1="452.12" x2="139.7" y2="459.74" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="134.62" y1="444.5" x2="139.7" y2="444.5" width="0.1524" layer="91"/>
<wire x1="139.7" y1="444.5" x2="139.7" y2="452.12" width="0.1524" layer="91"/>
<junction x="139.7" y="452.12"/>
<label x="139.7" y="459.74" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="FB2" gate="G$1" pin="1"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="12.7" y1="472.44" x2="18.288" y2="472.44" width="0.1524" layer="91"/>
<wire x1="12.7" y1="459.74" x2="12.7" y2="472.44" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="VBUS"/>
<wire x1="-2.54" y1="472.44" x2="12.7" y2="472.44" width="0.1524" layer="91"/>
<junction x="12.7" y="472.44"/>
<wire x1="12.7" y1="472.44" x2="12.7" y2="480.06" width="0.1524" layer="91"/>
<label x="12.7" y="480.06" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<wire x1="58.42" y1="469.9" x2="58.42" y2="474.98" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="USBDM"/>
<wire x1="58.42" y1="474.98" x2="73.66" y2="474.98" width="0.1524" layer="91"/>
<label x="43.18" y="469.9" size="1.27" layer="95" font="vector"/>
<pinref part="J5" gate="G$1" pin="D-"/>
<wire x1="-2.54" y1="469.9" x2="58.42" y2="469.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<wire x1="60.96" y1="467.36" x2="60.96" y2="472.44" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="USBDP"/>
<wire x1="60.96" y1="472.44" x2="73.66" y2="472.44" width="0.1524" layer="91"/>
<label x="43.18" y="467.36" size="1.27" layer="95" font="vector"/>
<pinref part="J5" gate="G$1" pin="D+"/>
<wire x1="-2.54" y1="467.36" x2="60.96" y2="467.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="TXD"/>
<wire x1="104.14" y1="474.98" x2="109.22" y2="474.98" width="0.1524" layer="91"/>
<label x="109.22" y="474.98" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="RXD"/>
<wire x1="104.14" y1="472.44" x2="109.22" y2="472.44" width="0.1524" layer="91"/>
<label x="109.22" y="472.44" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="127" y1="452.12" x2="124.46" y2="452.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="127" y1="444.5" x2="124.46" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="114.3" y1="452.12" x2="109.22" y2="452.12" width="0.1524" layer="91"/>
<wire x1="109.22" y1="452.12" x2="109.22" y2="449.58" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="TXLED"/>
<wire x1="109.22" y1="449.58" x2="104.14" y2="449.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="RXLED"/>
<wire x1="104.14" y1="447.04" x2="109.22" y2="447.04" width="0.1524" layer="91"/>
<wire x1="109.22" y1="447.04" x2="109.22" y2="444.5" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="109.22" y1="444.5" x2="114.3" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="FB2" gate="G$1" pin="2"/>
<wire x1="28.448" y1="472.44" x2="35.56" y2="472.44" width="0.1524" layer="91"/>
<wire x1="35.56" y1="472.44" x2="53.34" y2="472.44" width="0.1524" layer="91"/>
<wire x1="53.34" y1="472.44" x2="53.34" y2="480.06" width="0.1524" layer="91"/>
<wire x1="53.34" y1="480.06" x2="66.04" y2="480.06" width="0.1524" layer="91"/>
<wire x1="66.04" y1="480.06" x2="66.04" y2="467.36" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="VCC"/>
<wire x1="66.04" y1="467.36" x2="73.66" y2="467.36" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="35.56" y1="459.74" x2="35.56" y2="472.44" width="0.1524" layer="91"/>
<junction x="35.56" y="472.44"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="33.02" y="165.1" size="1.27" layer="97" font="vector">Bypass cerca del BQ24075</text>
<text x="195.58" y="137.16" size="1.27" layer="97" font="vector">Current Limit</text>
<text x="195.58" y="127" size="1.27" layer="97" font="vector"> EN2     EN1        Current
  0       0         100mA
  0       1         500mA
  1       0         ISET
  1       1         Standby</text>
<text x="60.96" y="91.44" size="1.27" layer="97" font="vector" rot="R180">590ohm - 1.5A Fast Charge</text>
<text x="175.26" y="10.16" size="2.54" layer="94">MONITOREO
DE BATERIA</text>
<text x="71.12" y="101.6" size="1.6764" layer="97" font="vector">Path Management / Charger</text>
<text x="172.72" y="78.74" size="1.6764" layer="97" font="vector">Fuel Gauge</text>
<text x="240.03" y="20.828" size="1.778" layer="94" rot="MR0">Ing. Agustin I. Reyes</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="U5" gate="G$1" x="180.34" y="63.5"/>
<instance part="U6" gate="G$1" x="81.28" y="124.46"/>
<instance part="J2" gate="G$1" x="104.14" y="58.42" rot="R90"/>
<instance part="R4" gate="G$1" x="55.88" y="109.22" rot="R270"/>
<instance part="R5" gate="G$1" x="43.18" y="109.22" rot="R270"/>
<instance part="R6" gate="G$1" x="25.4" y="109.22" rot="R270"/>
<instance part="GND28" gate="1" x="55.88" y="96.52"/>
<instance part="GND29" gate="1" x="43.18" y="96.52"/>
<instance part="GND30" gate="1" x="25.4" y="96.52"/>
<instance part="GND31" gate="1" x="33.02" y="96.52"/>
<instance part="GND32" gate="1" x="66.04" y="104.14"/>
<instance part="GND33" gate="1" x="116.84" y="50.8"/>
<instance part="R7" gate="G$1" x="154.94" y="71.12"/>
<instance part="C30" gate="G$1" x="160.02" y="55.88"/>
<instance part="GND34" gate="1" x="160.02" y="48.26"/>
<instance part="GND35" gate="1" x="167.64" y="48.26"/>
<instance part="C31" gate="G$1" x="208.28" y="63.5"/>
<instance part="GND36" gate="1" x="208.28" y="53.34"/>
<instance part="R8" gate="G$1" x="114.3" y="109.22" rot="R270"/>
<instance part="GND37" gate="1" x="114.3" y="96.52"/>
<instance part="C32" gate="G$1" x="124.46" y="129.54"/>
<instance part="GND38" gate="1" x="124.46" y="121.92"/>
<instance part="GND39" gate="1" x="40.64" y="137.16"/>
<instance part="JP3" gate="G$1" x="177.8" y="114.3" rot="R180"/>
<instance part="JP4" gate="G$1" x="162.56" y="114.3" rot="R180"/>
<instance part="GND41" gate="1" x="162.56" y="104.14"/>
<instance part="GND42" gate="1" x="177.8" y="104.14"/>
<instance part="R9" gate="G$1" x="162.56" y="127" rot="R270"/>
<instance part="R10" gate="G$1" x="177.8" y="127" rot="R270"/>
<instance part="D3" gate="G$1" x="63.5" y="63.5" rot="MR90"/>
<instance part="D4" gate="G$1" x="63.5" y="53.34" rot="MR90"/>
<instance part="R11" gate="G$1" x="45.72" y="63.5" rot="R180"/>
<instance part="R12" gate="G$1" x="45.72" y="53.34" rot="R180"/>
<instance part="JP2" gate="G$1" x="33.02" y="43.18" rot="R270"/>
<instance part="GND43" gate="1" x="43.18" y="35.56"/>
<instance part="C34" gate="G$1" x="25.4" y="144.78"/>
<instance part="GND44" gate="1" x="25.4" y="137.16"/>
<instance part="R13" gate="G$1" x="198.12" y="48.26" rot="R270"/>
<instance part="GND45" gate="1" x="198.12" y="38.1"/>
<instance part="C36" gate="G$1" x="40.64" y="144.78"/>
<instance part="S1" gate="1" x="213.36" y="93.98"/>
<instance part="R18" gate="G$1" x="223.52" y="106.68" rot="R270"/>
<instance part="GND58" gate="1" x="223.52" y="81.28"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="55.88" y1="104.14" x2="55.88" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="43.18" y1="104.14" x2="43.18" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="25.4" y1="104.14" x2="25.4" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="TMR"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="68.58" y1="124.46" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="33.02" y1="124.46" x2="33.02" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VSS"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="68.58" y1="114.3" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<wire x1="66.04" y1="114.3" x2="66.04" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="EP"/>
<wire x1="66.04" y1="111.76" x2="66.04" y2="106.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="111.76" x2="66.04" y2="111.76" width="0.1524" layer="91"/>
<junction x="66.04" y="111.76"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="-"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="109.22" y1="58.42" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
<wire x1="116.84" y1="58.42" x2="116.84" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VSS"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="170.18" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="167.64" y1="58.42" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="PWPD"/>
<wire x1="167.64" y1="55.88" x2="167.64" y2="50.8" width="0.1524" layer="91"/>
<wire x1="170.18" y1="55.88" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
<junction x="167.64" y="55.88"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="160.02" y1="53.34" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="208.28" y1="60.96" x2="208.28" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="114.3" y1="104.14" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="124.46" y1="127" x2="124.46" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="40.64" y1="142.24" x2="40.64" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="1"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="162.56" y1="109.22" x2="162.56" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="1"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="177.8" y1="109.22" x2="177.8" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="38.1" y1="43.18" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<wire x1="43.18" y1="43.18" x2="43.18" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND44" gate="1" pin="GND"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="25.4" y1="139.7" x2="25.4" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="198.12" y1="43.18" x2="198.12" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S1" gate="1" pin="S"/>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="218.44" y1="91.44" x2="223.52" y2="91.44" width="0.1524" layer="91"/>
<wire x1="223.52" y1="91.44" x2="223.52" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VUSB" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="IN"/>
<wire x1="68.58" y1="137.16" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<wire x1="63.5" y1="137.16" x2="63.5" y2="144.78" width="0.1524" layer="91"/>
<label x="63.5" y="144.78" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="25.4" y1="149.86" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<label x="25.4" y="154.94" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="ILIM"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="68.58" y1="119.38" x2="55.88" y2="119.38" width="0.1524" layer="91"/>
<wire x1="55.88" y1="119.38" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="ISET"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="68.58" y1="121.92" x2="43.18" y2="121.92" width="0.1524" layer="91"/>
<wire x1="43.18" y1="121.92" x2="43.18" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="TS"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="68.58" y1="129.54" x2="25.4" y2="129.54" width="0.1524" layer="91"/>
<wire x1="25.4" y1="129.54" x2="25.4" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBATT" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="BAT"/>
<wire x1="68.58" y1="132.08" x2="58.42" y2="132.08" width="0.1524" layer="91"/>
<wire x1="58.42" y1="132.08" x2="58.42" y2="144.78" width="0.1524" layer="91"/>
<label x="58.42" y="144.78" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SRN"/>
<wire x1="170.18" y1="71.12" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<wire x1="165.1" y1="71.12" x2="165.1" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="160.02" y1="71.12" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<junction x="165.1" y="71.12"/>
<label x="165.1" y="81.28" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="40.64" y1="149.86" x2="40.64" y2="154.94" width="0.1524" layer="91"/>
<label x="40.64" y="154.94" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="C36" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="223.52" y1="111.76" x2="223.52" y2="116.84" width="0.1524" layer="91"/>
<label x="223.52" y="116.84" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT"/>
<wire x1="93.98" y1="137.16" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="137.16" x2="109.22" y2="144.78" width="0.1524" layer="91"/>
<label x="109.22" y="144.78" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="124.46" y1="134.62" x2="124.46" y2="137.16" width="0.1524" layer="91"/>
<wire x1="124.46" y1="137.16" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
<junction x="109.22" y="137.16"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="177.8" y1="132.08" x2="177.8" y2="137.16" width="0.1524" layer="91"/>
<wire x1="162.56" y1="137.16" x2="170.18" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="170.18" y1="137.16" x2="177.8" y2="137.16" width="0.1524" layer="91"/>
<wire x1="162.56" y1="132.08" x2="162.56" y2="137.16" width="0.1524" layer="91"/>
<wire x1="170.18" y1="137.16" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
<junction x="170.18" y="137.16"/>
<label x="170.18" y="142.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="66.04" y1="53.34" x2="76.2" y2="53.34" width="0.1524" layer="91"/>
<wire x1="76.2" y1="53.34" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="76.2" y1="63.5" x2="76.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="66.04" y1="63.5" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<junction x="76.2" y="63.5"/>
<label x="76.2" y="71.12" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="BATTERY_IN" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="+"/>
<wire x1="109.22" y1="60.96" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<wire x1="116.84" y1="60.96" x2="116.84" y2="66.04" width="0.1524" layer="91"/>
<label x="116.84" y="66.04" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SRP"/>
<wire x1="170.18" y1="68.58" x2="165.1" y2="68.58" width="0.1524" layer="91"/>
<wire x1="165.1" y1="68.58" x2="144.78" y2="68.58" width="0.1524" layer="91"/>
<wire x1="144.78" y1="68.58" x2="144.78" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="144.78" y1="71.12" x2="149.86" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="BAT"/>
<wire x1="170.18" y1="63.5" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<wire x1="165.1" y1="63.5" x2="165.1" y2="68.58" width="0.1524" layer="91"/>
<junction x="165.1" y="68.58"/>
<wire x1="154.94" y1="63.5" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
<junction x="165.1" y="63.5"/>
<label x="154.94" y="63.5" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="160.02" y1="63.5" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<wire x1="160.02" y1="60.96" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
<junction x="160.02" y="63.5"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="VDD"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="193.04" y1="71.12" x2="208.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="208.28" y1="71.12" x2="208.28" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="!CE!"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="93.98" y1="121.92" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
<wire x1="114.3" y1="121.92" x2="114.3" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHG" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="!CHG!"/>
<wire x1="93.98" y1="114.3" x2="99.06" y2="114.3" width="0.1524" layer="91"/>
<label x="99.06" y="114.3" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="40.64" y1="63.5" x2="33.02" y2="63.5" width="0.1524" layer="91"/>
<label x="33.02" y="63.5" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PGOOD" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="!PGOOD!"/>
<wire x1="93.98" y1="111.76" x2="99.06" y2="111.76" width="0.1524" layer="91"/>
<label x="99.06" y="111.76" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="3"/>
<wire x1="27.94" y1="43.18" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<label x="20.32" y="43.18" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="EN2" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="EN2"/>
<wire x1="93.98" y1="129.54" x2="101.6" y2="129.54" width="0.1524" layer="91"/>
<label x="101.6" y="129.54" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="2"/>
<wire x1="167.64" y1="114.3" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="170.18" y1="114.3" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<label x="170.18" y="119.38" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="EN1" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="EN1"/>
<wire x1="93.98" y1="127" x2="101.6" y2="127" width="0.1524" layer="91"/>
<label x="101.6" y="127" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="182.88" y1="114.3" x2="187.96" y2="114.3" width="0.1524" layer="91"/>
<wire x1="187.96" y1="114.3" x2="187.96" y2="119.38" width="0.1524" layer="91"/>
<label x="187.96" y="119.38" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="JP4" gate="G$1" pin="3"/>
<wire x1="162.56" y1="121.92" x2="162.56" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="JP3" gate="G$1" pin="3"/>
<wire x1="177.8" y1="121.92" x2="177.8" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="40.64" y1="53.34" x2="33.02" y2="53.34" width="0.1524" layer="91"/>
<wire x1="33.02" y1="53.34" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="50.8" y1="53.34" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="58.42" y1="63.5" x2="50.8" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SDA"/>
<wire x1="193.04" y1="66.04" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<label x="198.12" y="66.04" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SCL"/>
<wire x1="193.04" y1="63.5" x2="198.12" y2="63.5" width="0.1524" layer="91"/>
<label x="198.12" y="63.5" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="BIN"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="193.04" y1="55.88" x2="198.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="198.12" y1="55.88" x2="198.12" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GPOUT" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="GPOUT"/>
<wire x1="193.04" y1="58.42" x2="198.12" y2="58.42" width="0.1524" layer="91"/>
<label x="198.12" y="58.42" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="SYSOFF" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="SYSOFF"/>
<wire x1="93.98" y1="124.46" x2="101.6" y2="124.46" width="0.1524" layer="91"/>
<label x="101.6" y="124.46" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="S1" gate="1" pin="P"/>
<wire x1="210.82" y1="93.98" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<label x="200.66" y="93.98" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="S1" gate="1" pin="O"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="218.44" y1="96.52" x2="223.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="223.52" y1="96.52" x2="223.52" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="177.8" y="12.7" size="2.54" layer="94">SENSORES</text>
<text x="38.1" y="154.94" size="1.6764" layer="97" font="vector">Termocuplas</text>
<text x="180.34" y="78.74" size="1.6764" layer="97" font="vector">Temp. local</text>
<text x="240.03" y="20.828" size="1.778" layer="94" rot="MR0">Ing. Agustin I. Reyes</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="U1" gate="G$1" x="45.72" y="137.16" rot="MR0"/>
<instance part="U2" gate="G$1" x="45.72" y="104.14" rot="MR0"/>
<instance part="U3" gate="G$1" x="45.72" y="68.58" rot="MR0"/>
<instance part="U4" gate="G$1" x="45.72" y="33.02" rot="MR0"/>
<instance part="L1" gate="G$1" x="86.36" y="144.78"/>
<instance part="L2" gate="G$1" x="106.68" y="144.78"/>
<instance part="L3" gate="G$1" x="86.36" y="104.14"/>
<instance part="L4" gate="G$1" x="106.68" y="104.14"/>
<instance part="L5" gate="G$1" x="86.36" y="66.04"/>
<instance part="L6" gate="G$1" x="106.68" y="66.04"/>
<instance part="L7" gate="G$1" x="86.36" y="30.48"/>
<instance part="L8" gate="G$1" x="106.68" y="30.48"/>
<instance part="C5" gate="G$1" x="99.06" y="40.64" rot="R90"/>
<instance part="C6" gate="G$1" x="99.06" y="78.74" rot="R90"/>
<instance part="C7" gate="G$1" x="99.06" y="116.84" rot="R90"/>
<instance part="C8" gate="G$1" x="99.06" y="152.4" rot="R90"/>
<instance part="C9" gate="G$1" x="119.38" y="152.4"/>
<instance part="C10" gate="G$1" x="119.38" y="111.76"/>
<instance part="C11" gate="G$1" x="119.38" y="73.66"/>
<instance part="C12" gate="G$1" x="119.38" y="38.1"/>
<instance part="GND46" gate="1" x="68.58" y="124.46"/>
<instance part="GND47" gate="1" x="68.58" y="88.9"/>
<instance part="GND48" gate="1" x="68.58" y="55.88"/>
<instance part="GND49" gate="1" x="68.58" y="17.78"/>
<instance part="GND50" gate="1" x="119.38" y="144.78"/>
<instance part="GND51" gate="1" x="119.38" y="104.14"/>
<instance part="GND52" gate="1" x="119.38" y="66.04"/>
<instance part="GND53" gate="1" x="119.38" y="30.48"/>
<instance part="SUPPLY1" gate="G$1" x="60.96" y="152.4"/>
<instance part="SUPPLY4" gate="G$1" x="60.96" y="119.38"/>
<instance part="SUPPLY5" gate="G$1" x="60.96" y="81.28"/>
<instance part="SUPPLY6" gate="G$1" x="60.96" y="48.26"/>
<instance part="SUPPLY7" gate="G$1" x="119.38" y="45.72"/>
<instance part="SUPPLY8" gate="G$1" x="119.38" y="83.82"/>
<instance part="SUPPLY9" gate="G$1" x="119.38" y="121.92"/>
<instance part="SUPPLY10" gate="G$1" x="119.38" y="162.56"/>
<instance part="C35" gate="G$1" x="220.98" y="60.96"/>
<instance part="GND54" gate="1" x="220.98" y="50.8"/>
<instance part="SUPPLY11" gate="G$1" x="220.98" y="73.66"/>
<instance part="GND55" gate="1" x="208.28" y="53.34"/>
<instance part="SUPPLY12" gate="G$1" x="208.28" y="71.12"/>
<instance part="JP1" gate="A" x="193.04" y="132.08"/>
<instance part="U7" gate="G$1" x="180.34" y="50.8"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<pinref part="GND49" gate="1" pin="GND"/>
<wire x1="58.42" y1="33.02" x2="68.58" y2="33.02" width="0.1524" layer="91"/>
<wire x1="68.58" y1="33.02" x2="68.58" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="58.42" y1="68.58" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
<wire x1="68.58" y1="68.58" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="58.42" y1="104.14" x2="68.58" y2="104.14" width="0.1524" layer="91"/>
<wire x1="68.58" y1="104.14" x2="68.58" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="58.42" y1="137.16" x2="68.58" y2="137.16" width="0.1524" layer="91"/>
<wire x1="68.58" y1="137.16" x2="68.58" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND53" gate="1" pin="GND"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="119.38" y1="33.02" x2="119.38" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND52" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="119.38" y1="68.58" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND51" gate="1" pin="GND"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="119.38" y1="106.68" x2="119.38" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND50" gate="1" pin="GND"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="119.38" y1="147.32" x2="119.38" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND55" gate="1" pin="GND"/>
<wire x1="208.28" y1="60.96" x2="208.28" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="203.2" y1="60.96" x2="208.28" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="220.98" y1="58.42" x2="220.98" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<pinref part="SUPPLY1" gate="G$1" pin="3.3V"/>
<wire x1="58.42" y1="144.78" x2="60.96" y2="144.78" width="0.1524" layer="91"/>
<wire x1="60.96" y1="144.78" x2="60.96" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCC"/>
<pinref part="SUPPLY4" gate="G$1" pin="3.3V"/>
<wire x1="58.42" y1="111.76" x2="60.96" y2="111.76" width="0.1524" layer="91"/>
<wire x1="60.96" y1="111.76" x2="60.96" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VCC"/>
<pinref part="SUPPLY5" gate="G$1" pin="3.3V"/>
<wire x1="58.42" y1="76.2" x2="60.96" y2="76.2" width="0.1524" layer="91"/>
<wire x1="60.96" y1="76.2" x2="60.96" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="VCC"/>
<pinref part="SUPPLY6" gate="G$1" pin="3.3V"/>
<wire x1="58.42" y1="40.64" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="60.96" y1="40.64" x2="60.96" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY7" gate="G$1" pin="3.3V"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="119.38" y1="45.72" x2="119.38" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY8" gate="G$1" pin="3.3V"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="119.38" y1="83.82" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY9" gate="G$1" pin="3.3V"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="119.38" y1="121.92" x2="119.38" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY10" gate="G$1" pin="3.3V"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="119.38" y1="162.56" x2="119.38" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY12" gate="G$1" pin="3.3V"/>
<wire x1="208.28" y1="66.04" x2="208.28" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VCC"/>
<wire x1="203.2" y1="66.04" x2="208.28" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY11" gate="G$1" pin="3.3V"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="220.98" y1="73.66" x2="220.98" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SCK"/>
<wire x1="33.02" y1="30.48" x2="27.94" y2="30.48" width="0.1524" layer="91"/>
<wire x1="27.94" y1="30.48" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SCK"/>
<wire x1="27.94" y1="66.04" x2="27.94" y2="101.6" width="0.1524" layer="91"/>
<wire x1="27.94" y1="101.6" x2="27.94" y2="134.62" width="0.1524" layer="91"/>
<wire x1="27.94" y1="134.62" x2="27.94" y2="142.24" width="0.1524" layer="91"/>
<wire x1="33.02" y1="134.62" x2="27.94" y2="134.62" width="0.1524" layer="91"/>
<junction x="27.94" y="134.62"/>
<pinref part="U2" gate="G$1" pin="SCK"/>
<wire x1="33.02" y1="101.6" x2="27.94" y2="101.6" width="0.1524" layer="91"/>
<junction x="27.94" y="101.6"/>
<pinref part="U3" gate="G$1" pin="SCK"/>
<wire x1="33.02" y1="66.04" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
<junction x="27.94" y="66.04"/>
<label x="27.94" y="142.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="(MI)SO"/>
<wire x1="33.02" y1="27.94" x2="25.4" y2="27.94" width="0.1524" layer="91"/>
<wire x1="25.4" y1="27.94" x2="25.4" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="(MI)SO"/>
<wire x1="25.4" y1="63.5" x2="25.4" y2="99.06" width="0.1524" layer="91"/>
<wire x1="25.4" y1="99.06" x2="25.4" y2="132.08" width="0.1524" layer="91"/>
<wire x1="25.4" y1="132.08" x2="25.4" y2="142.24" width="0.1524" layer="91"/>
<wire x1="33.02" y1="132.08" x2="25.4" y2="132.08" width="0.1524" layer="91"/>
<junction x="25.4" y="132.08"/>
<pinref part="U2" gate="G$1" pin="(MI)SO"/>
<wire x1="33.02" y1="99.06" x2="25.4" y2="99.06" width="0.1524" layer="91"/>
<junction x="25.4" y="99.06"/>
<pinref part="U3" gate="G$1" pin="(MI)SO"/>
<wire x1="33.02" y1="63.5" x2="25.4" y2="63.5" width="0.1524" layer="91"/>
<junction x="25.4" y="63.5"/>
<label x="25.4" y="142.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CS2" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="!CS"/>
<wire x1="33.02" y1="25.4" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
<wire x1="17.78" y1="25.4" x2="17.78" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="!CS"/>
<wire x1="17.78" y1="60.96" x2="17.78" y2="68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="60.96" x2="17.78" y2="60.96" width="0.1524" layer="91"/>
<junction x="17.78" y="60.96"/>
<label x="17.78" y="68.58" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CS1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="!CS"/>
<wire x1="33.02" y1="96.52" x2="17.78" y2="96.52" width="0.1524" layer="91"/>
<wire x1="17.78" y1="96.52" x2="17.78" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="!CS"/>
<wire x1="17.78" y1="129.54" x2="17.78" y2="137.16" width="0.1524" layer="91"/>
<wire x1="33.02" y1="129.54" x2="17.78" y2="129.54" width="0.1524" layer="91"/>
<junction x="17.78" y="129.54"/>
<label x="17.78" y="137.16" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="93.98" y1="152.4" x2="86.36" y2="152.4" width="0.1524" layer="91"/>
<wire x1="86.36" y1="152.4" x2="86.36" y2="149.86" width="0.1524" layer="91"/>
<wire x1="81.28" y1="129.54" x2="81.28" y2="152.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="152.4" x2="86.36" y2="152.4" width="0.1524" layer="91"/>
<junction x="86.36" y="152.4"/>
<pinref part="U1" gate="G$1" pin="T+"/>
<wire x1="81.28" y1="129.54" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="101.6" y1="152.4" x2="106.68" y2="152.4" width="0.1524" layer="91"/>
<wire x1="106.68" y1="152.4" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="T-"/>
<wire x1="58.42" y1="132.08" x2="76.2" y2="132.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="132.08" x2="76.2" y2="160.02" width="0.1524" layer="91"/>
<wire x1="76.2" y1="160.02" x2="106.68" y2="160.02" width="0.1524" layer="91"/>
<wire x1="106.68" y1="160.02" x2="106.68" y2="152.4" width="0.1524" layer="91"/>
<junction x="106.68" y="152.4"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="93.98" y1="116.84" x2="86.36" y2="116.84" width="0.1524" layer="91"/>
<wire x1="86.36" y1="116.84" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="T+"/>
<wire x1="58.42" y1="96.52" x2="81.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="81.28" y1="96.52" x2="81.28" y2="116.84" width="0.1524" layer="91"/>
<wire x1="81.28" y1="116.84" x2="86.36" y2="116.84" width="0.1524" layer="91"/>
<junction x="86.36" y="116.84"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="101.6" y1="116.84" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<wire x1="106.68" y1="116.84" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="T-"/>
<wire x1="58.42" y1="99.06" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="76.2" y1="99.06" x2="76.2" y2="124.46" width="0.1524" layer="91"/>
<wire x1="76.2" y1="124.46" x2="106.68" y2="124.46" width="0.1524" layer="91"/>
<wire x1="106.68" y1="124.46" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<junction x="106.68" y="116.84"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="93.98" y1="78.74" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<wire x1="86.36" y1="78.74" x2="86.36" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="T+"/>
<wire x1="58.42" y1="60.96" x2="81.28" y2="60.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="60.96" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="81.28" y1="78.74" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<junction x="86.36" y="78.74"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="101.6" y1="78.74" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<wire x1="106.68" y1="78.74" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="T-"/>
<wire x1="58.42" y1="63.5" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<wire x1="76.2" y1="63.5" x2="76.2" y2="86.36" width="0.1524" layer="91"/>
<wire x1="76.2" y1="86.36" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<wire x1="106.68" y1="86.36" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<junction x="106.68" y="78.74"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="L7" gate="G$1" pin="1"/>
<wire x1="93.98" y1="40.64" x2="86.36" y2="40.64" width="0.1524" layer="91"/>
<wire x1="86.36" y1="40.64" x2="86.36" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="T+"/>
<wire x1="58.42" y1="25.4" x2="81.28" y2="25.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="25.4" x2="81.28" y2="40.64" width="0.1524" layer="91"/>
<wire x1="81.28" y1="40.64" x2="86.36" y2="40.64" width="0.1524" layer="91"/>
<junction x="86.36" y="40.64"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="101.6" y1="40.64" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<wire x1="106.68" y1="40.64" x2="106.68" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="T-"/>
<wire x1="58.42" y1="27.94" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
<wire x1="76.2" y1="27.94" x2="76.2" y2="48.26" width="0.1524" layer="91"/>
<wire x1="76.2" y1="48.26" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<wire x1="106.68" y1="48.26" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<junction x="106.68" y="40.64"/>
</segment>
</net>
<net name="T1+" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="86.36" y1="139.7" x2="86.36" y2="134.62" width="0.1524" layer="91"/>
<label x="86.36" y="134.62" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="190.5" y1="142.24" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
<label x="182.88" y="142.24" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="T1-" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="106.68" y1="139.7" x2="106.68" y2="134.62" width="0.1524" layer="91"/>
<label x="106.68" y="134.62" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="190.5" y1="139.7" x2="182.88" y2="139.7" width="0.1524" layer="91"/>
<label x="182.88" y="139.7" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="T2+" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="86.36" y1="99.06" x2="86.36" y2="93.98" width="0.1524" layer="91"/>
<label x="86.36" y="93.98" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="190.5" y1="137.16" x2="182.88" y2="137.16" width="0.1524" layer="91"/>
<label x="182.88" y="137.16" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="T2-" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="106.68" y1="99.06" x2="106.68" y2="93.98" width="0.1524" layer="91"/>
<label x="106.68" y="93.98" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="190.5" y1="134.62" x2="182.88" y2="134.62" width="0.1524" layer="91"/>
<label x="182.88" y="134.62" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="T3+" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="86.36" y1="60.96" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
<label x="86.36" y="55.88" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="5"/>
<wire x1="190.5" y1="132.08" x2="182.88" y2="132.08" width="0.1524" layer="91"/>
<label x="182.88" y="132.08" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="T3-" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="2"/>
<wire x1="106.68" y1="60.96" x2="106.68" y2="55.88" width="0.1524" layer="91"/>
<label x="106.68" y="55.88" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="6"/>
<wire x1="190.5" y1="129.54" x2="182.88" y2="129.54" width="0.1524" layer="91"/>
<label x="182.88" y="129.54" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="T4+" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="2"/>
<wire x1="86.36" y1="25.4" x2="86.36" y2="20.32" width="0.1524" layer="91"/>
<label x="86.36" y="20.32" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="7"/>
<wire x1="190.5" y1="127" x2="182.88" y2="127" width="0.1524" layer="91"/>
<label x="182.88" y="127" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="T4-" class="0">
<segment>
<pinref part="L8" gate="G$1" pin="2"/>
<wire x1="106.68" y1="25.4" x2="106.68" y2="20.32" width="0.1524" layer="91"/>
<label x="106.68" y="20.32" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="8"/>
<wire x1="190.5" y1="124.46" x2="182.88" y2="124.46" width="0.1524" layer="91"/>
<label x="182.88" y="124.46" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="SDA"/>
<wire x1="175.26" y1="66.04" x2="170.18" y2="66.04" width="0.1524" layer="91"/>
<label x="170.18" y="66.04" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="SCL"/>
<wire x1="175.26" y1="60.96" x2="170.18" y2="60.96" width="0.1524" layer="91"/>
<label x="170.18" y="60.96" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="172.72" y="12.7" size="2.54" layer="94">DCDC BUCK BOOST</text>
<text x="239.522" y="20.828" size="1.778" layer="94" rot="MR0">Ing. Agustin I. Reyes</text>
</plain>
<instances>
<instance part="FRAME4" gate="G$1" x="0" y="0"/>
<instance part="REG1" gate="G$1" x="109.22" y="76.2"/>
<instance part="C23" gate="G$1" x="71.12" y="96.52"/>
<instance part="C24" gate="G$1" x="149.86" y="91.44"/>
<instance part="C25" gate="G$1" x="162.56" y="91.44"/>
<instance part="C26" gate="G$1" x="93.98" y="88.9"/>
<instance part="L10" gate="G$1" x="119.38" y="119.38" rot="R90"/>
<instance part="GND17" gate="1" x="93.98" y="81.28"/>
<instance part="GND18" gate="1" x="101.6" y="71.12"/>
<instance part="GND19" gate="1" x="71.12" y="86.36"/>
<instance part="GND20" gate="1" x="139.7" y="71.12"/>
<instance part="GND21" gate="1" x="149.86" y="83.82"/>
<instance part="GND22" gate="1" x="162.56" y="83.82"/>
<instance part="SUPPLY3" gate="G$1" x="172.72" y="106.68"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="REG1" gate="G$1" pin="GND"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="104.14" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="78.74" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="93.98" y1="86.36" x2="93.98" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="71.12" y1="88.9" x2="71.12" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="REG1" gate="G$1" pin="PGND"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="137.16" y1="78.74" x2="139.7" y2="78.74" width="0.1524" layer="91"/>
<wire x1="139.7" y1="78.74" x2="139.7" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="149.86" y1="88.9" x2="149.86" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="162.56" y1="88.9" x2="162.56" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="REG1" gate="G$1" pin="VIN"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="104.14" y1="101.6" x2="71.12" y2="101.6" width="0.1524" layer="91"/>
<wire x1="71.12" y1="101.6" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<wire x1="63.5" y1="101.6" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<junction x="71.12" y="101.6"/>
<label x="63.5" y="109.22" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="SUPPLY3" gate="G$1" pin="3.3V"/>
<wire x1="172.72" y1="106.68" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
<pinref part="REG1" gate="G$1" pin="VOUT"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="137.16" y1="101.6" x2="142.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="142.24" y1="101.6" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<wire x1="149.86" y1="101.6" x2="162.56" y2="101.6" width="0.1524" layer="91"/>
<wire x1="162.56" y1="101.6" x2="162.56" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="149.86" y1="96.52" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<junction x="149.86" y="101.6"/>
<pinref part="REG1" gate="G$1" pin="FB"/>
<wire x1="137.16" y1="93.98" x2="142.24" y2="93.98" width="0.1524" layer="91"/>
<wire x1="142.24" y1="93.98" x2="142.24" y2="101.6" width="0.1524" layer="91"/>
<junction x="142.24" y="101.6"/>
<wire x1="172.72" y1="101.6" x2="162.56" y2="101.6" width="0.1524" layer="91"/>
<junction x="162.56" y="101.6"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="REG1" gate="G$1" pin="L2"/>
<wire x1="137.16" y1="106.68" x2="142.24" y2="106.68" width="0.1524" layer="91"/>
<wire x1="142.24" y1="106.68" x2="142.24" y2="119.38" width="0.1524" layer="91"/>
<pinref part="L10" gate="G$1" pin="2"/>
<wire x1="142.24" y1="119.38" x2="124.46" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="L10" gate="G$1" pin="1"/>
<wire x1="114.3" y1="119.38" x2="99.06" y2="119.38" width="0.1524" layer="91"/>
<wire x1="99.06" y1="119.38" x2="99.06" y2="106.68" width="0.1524" layer="91"/>
<pinref part="REG1" gate="G$1" pin="L1"/>
<wire x1="99.06" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="REG1" gate="G$1" pin="VINA"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="104.14" y1="99.06" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<wire x1="101.6" y1="99.06" x2="93.98" y2="99.06" width="0.1524" layer="91"/>
<wire x1="93.98" y1="99.06" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<pinref part="REG1" gate="G$1" pin="EN"/>
<wire x1="104.14" y1="93.98" x2="101.6" y2="93.98" width="0.1524" layer="91"/>
<wire x1="101.6" y1="93.98" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<junction x="101.6" y="99.06"/>
<pinref part="REG1" gate="G$1" pin="PY/SYNC"/>
<wire x1="104.14" y1="91.44" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<wire x1="101.6" y1="91.44" x2="101.6" y2="93.98" width="0.1524" layer="91"/>
<junction x="101.6" y="93.98"/>
</segment>
</net>
<net name="EP" class="0">
<segment>
<pinref part="REG1" gate="G$1" pin="EPAD"/>
<wire x1="137.16" y1="86.36" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<label x="142.24" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="187.96" y="147.32" size="1.27" layer="97" font="vector">I2C Pull Ups</text>
<text x="213.36" y="147.32" size="1.27" layer="97" font="vector">GPOUT - Battery Low</text>
<text x="177.8" y="12.7" size="2.54" layer="94">MCU CC2640</text>
<text x="239.522" y="20.828" size="1.778" layer="94" rot="MR0">Ing. Agustin I. Reyes</text>
</plain>
<instances>
<instance part="FRAME5" gate="G$1" x="0" y="0"/>
<instance part="MCU1" gate="G$1" x="45.72" y="50.8"/>
<instance part="E1" gate="G$1" x="165.1" y="99.06"/>
<instance part="R1" gate="G$1" x="17.78" y="78.74" rot="R90"/>
<instance part="Y1" gate="G$1" x="114.3" y="50.8"/>
<instance part="Y2" gate="G$1" x="132.08" y="58.42"/>
<instance part="C3" gate="G$1" x="139.7" y="45.72"/>
<instance part="C4" gate="G$1" x="124.46" y="45.72"/>
<instance part="GND9" gate="1" x="71.12" y="38.1"/>
<instance part="C21" gate="G$1" x="35.56" y="50.8"/>
<instance part="GND10" gate="1" x="35.56" y="40.64"/>
<instance part="C1" gate="G$1" x="17.78" y="63.5"/>
<instance part="GND11" gate="1" x="17.78" y="55.88"/>
<instance part="GND12" gate="1" x="152.4" y="66.04"/>
<instance part="C2" gate="G$1" x="157.48" y="83.82" rot="R90"/>
<instance part="GND13" gate="1" x="167.64" y="76.2"/>
<instance part="J3" gate="G$1" x="165.1" y="114.3" rot="MR0"/>
<instance part="C22" gate="G$1" x="149.86" y="93.98" rot="R180"/>
<instance part="GND14" gate="1" x="172.72" y="101.6"/>
<instance part="GND15" gate="1" x="114.3" y="40.64"/>
<instance part="GND16" gate="1" x="132.08" y="33.02"/>
<instance part="BALUN1" gate="G$1" x="121.92" y="71.12"/>
<instance part="J4" gate="G$1" x="203.2" y="45.72"/>
<instance part="SUPPLY15" gate="G$1" x="193.04" y="73.66"/>
<instance part="GND57" gate="1" x="190.5" y="35.56"/>
<instance part="R14" gate="G$1" x="195.58" y="134.62" rot="R270"/>
<instance part="R15" gate="G$1" x="205.74" y="134.62" rot="R270"/>
<instance part="R16" gate="G$1" x="215.9" y="134.62" rot="R270"/>
<instance part="SUPPLY14" gate="G$1" x="205.74" y="149.86"/>
<instance part="SUPPLY13" gate="G$1" x="17.78" y="91.44"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="GND"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="71.12" y1="45.72" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="35.56" y1="48.26" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="17.78" y1="60.96" x2="17.78" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="144.78" y1="78.74" x2="152.4" y2="78.74" width="0.1524" layer="91"/>
<wire x1="152.4" y1="78.74" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="152.4" y2="68.58" width="0.1524" layer="91"/>
<wire x1="144.78" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<junction x="152.4" y="73.66"/>
<pinref part="BALUN1" gate="G$1" pin="GND@6"/>
<pinref part="BALUN1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="E1" gate="G$1" pin="GND"/>
<wire x1="167.64" y1="78.74" x2="167.64" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="GND@0"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="167.64" y1="109.22" x2="172.72" y2="109.22" width="0.1524" layer="91"/>
<wire x1="172.72" y1="109.22" x2="172.72" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="GND@1"/>
<wire x1="172.72" y1="106.68" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<wire x1="167.64" y1="106.68" x2="172.72" y2="106.68" width="0.1524" layer="91"/>
<junction x="172.72" y="106.68"/>
</segment>
<segment>
<pinref part="Y1" gate="G$1" pin="3"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="114.3" y1="45.72" x2="114.3" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="124.46" y1="40.64" x2="124.46" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="124.46" y1="38.1" x2="132.08" y2="38.1" width="0.1524" layer="91"/>
<wire x1="132.08" y1="38.1" x2="139.7" y2="38.1" width="0.1524" layer="91"/>
<wire x1="139.7" y1="38.1" x2="139.7" y2="40.64" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="132.08" y1="35.56" x2="132.08" y2="38.1" width="0.1524" layer="91"/>
<junction x="132.08" y="38.1"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="GND"/>
<pinref part="GND57" gate="1" pin="GND"/>
<wire x1="198.12" y1="60.96" x2="190.5" y2="60.96" width="0.1524" layer="91"/>
<wire x1="190.5" y1="60.96" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="GND@5"/>
<wire x1="190.5" y1="55.88" x2="190.5" y2="45.72" width="0.1524" layer="91"/>
<wire x1="190.5" y1="45.72" x2="190.5" y2="38.1" width="0.1524" layer="91"/>
<wire x1="198.12" y1="55.88" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<junction x="190.5" y="55.88"/>
<pinref part="J4" gate="G$1" pin="GND@9"/>
<wire x1="198.12" y1="45.72" x2="190.5" y2="45.72" width="0.1524" layer="91"/>
<junction x="190.5" y="45.72"/>
</segment>
</net>
<net name="VDDS" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="VDDS_DCDC"/>
<wire x1="78.74" y1="139.7" x2="78.74" y2="144.78" width="0.1524" layer="91"/>
<wire x1="78.74" y1="144.78" x2="68.58" y2="144.78" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="VDDS"/>
<wire x1="68.58" y1="144.78" x2="63.5" y2="144.78" width="0.1524" layer="91"/>
<wire x1="63.5" y1="144.78" x2="58.42" y2="144.78" width="0.1524" layer="91"/>
<wire x1="58.42" y1="144.78" x2="58.42" y2="139.7" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="VDDS2"/>
<wire x1="63.5" y1="139.7" x2="63.5" y2="144.78" width="0.1524" layer="91"/>
<junction x="63.5" y="144.78"/>
<pinref part="MCU1" gate="G$1" pin="VDDS3"/>
<wire x1="68.58" y1="139.7" x2="68.58" y2="144.78" width="0.1524" layer="91"/>
<junction x="68.58" y="144.78"/>
<wire x1="58.42" y1="144.78" x2="58.42" y2="152.4" width="0.1524" layer="91"/>
<junction x="58.42" y="144.78"/>
<label x="58.42" y="152.4" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="SUPPLY15" gate="G$1" pin="3.3V"/>
<wire x1="193.04" y1="66.04" x2="193.04" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="+V"/>
<wire x1="193.04" y1="66.04" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="195.58" y1="139.7" x2="195.58" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="195.58" y1="144.78" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
<wire x1="205.74" y1="144.78" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<wire x1="215.9" y1="144.78" x2="215.9" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="205.74" y1="139.7" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
<junction x="205.74" y="144.78"/>
<pinref part="SUPPLY14" gate="G$1" pin="3.3V"/>
<wire x1="205.74" y1="149.86" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="G$1" pin="3.3V"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="17.78" y1="91.44" x2="17.78" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DCDC_SW" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DCDC_SW"/>
<wire x1="83.82" y1="139.7" x2="83.82" y2="152.4" width="0.1524" layer="91"/>
<label x="83.82" y="152.4" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VDDR" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="VDDR"/>
<wire x1="73.66" y1="139.7" x2="73.66" y2="147.32" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="VDDR_RF"/>
<wire x1="73.66" y1="147.32" x2="73.66" y2="152.4" width="0.1524" layer="91"/>
<wire x1="88.9" y1="139.7" x2="88.9" y2="147.32" width="0.1524" layer="91"/>
<wire x1="88.9" y1="147.32" x2="73.66" y2="147.32" width="0.1524" layer="91"/>
<junction x="73.66" y="147.32"/>
<label x="73.66" y="152.4" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DCOUPL"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="40.64" y1="60.96" x2="35.56" y2="60.96" width="0.1524" layer="91"/>
<wire x1="35.56" y1="60.96" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="RESET_N"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="40.64" y1="71.12" x2="17.78" y2="71.12" width="0.1524" layer="91"/>
<wire x1="17.78" y1="71.12" x2="17.78" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="17.78" y1="68.58" x2="17.78" y2="71.12" width="0.1524" layer="91"/>
<junction x="17.78" y="71.12"/>
<wire x1="17.78" y1="71.12" x2="15.24" y2="71.12" width="0.1524" layer="91"/>
<label x="15.24" y="71.12" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="RST"/>
<wire x1="226.06" y1="45.72" x2="231.14" y2="45.72" width="0.1524" layer="91"/>
<label x="231.14" y="45.72" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="JTAG_TMSC" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="JTAG_TMSC"/>
<wire x1="40.64" y1="83.82" x2="33.02" y2="83.82" width="0.1524" layer="91"/>
<label x="33.02" y="83.82" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="TMSC"/>
<wire x1="226.06" y1="66.04" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
<label x="231.14" y="66.04" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="JTAG_TCKC" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DJTAG_TCKC"/>
<wire x1="40.64" y1="81.28" x2="33.02" y2="81.28" width="0.1524" layer="91"/>
<label x="33.02" y="81.28" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="TCKC"/>
<wire x1="226.06" y1="60.96" x2="231.14" y2="60.96" width="0.1524" layer="91"/>
<label x="231.14" y="60.96" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="JTAG_TDI" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO17/JTAG_TDI"/>
<wire x1="40.64" y1="86.36" x2="33.02" y2="86.36" width="0.1524" layer="91"/>
<label x="33.02" y="86.36" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="TDI"/>
<wire x1="226.06" y1="50.8" x2="231.14" y2="50.8" width="0.1524" layer="91"/>
<label x="231.14" y="50.8" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="JTAG_TDO" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO16/JTAG_TDO"/>
<wire x1="40.64" y1="88.9" x2="33.02" y2="88.9" width="0.1524" layer="91"/>
<label x="33.02" y="88.9" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="TDO"/>
<wire x1="226.06" y1="55.88" x2="231.14" y2="55.88" width="0.1524" layer="91"/>
<label x="231.14" y="55.88" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="144.78" y1="83.82" x2="149.86" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="149.86" y1="83.82" x2="152.4" y2="83.82" width="0.1524" layer="91"/>
<wire x1="149.86" y1="88.9" x2="149.86" y2="83.82" width="0.1524" layer="91"/>
<junction x="149.86" y="83.82"/>
<pinref part="BALUN1" gate="G$1" pin="UBP"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="E1" gate="G$1" pin="SIGNAL"/>
<wire x1="160.02" y1="83.82" x2="165.1" y2="83.82" width="0.1524" layer="91"/>
<wire x1="165.1" y1="83.82" x2="165.1" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SIGNAL"/>
<wire x1="160.02" y1="114.3" x2="149.86" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="149.86" y1="114.3" x2="149.86" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="X24M_N"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="106.68" y1="58.42" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
<wire x1="116.84" y1="58.42" x2="116.84" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="X24M_P"/>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="106.68" y1="55.88" x2="111.76" y2="55.88" width="0.1524" layer="91"/>
<wire x1="111.76" y1="55.88" x2="111.76" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="X32K_Q1"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="106.68" y1="66.04" x2="139.7" y2="66.04" width="0.1524" layer="91"/>
<wire x1="139.7" y1="66.04" x2="139.7" y2="58.42" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="2"/>
<wire x1="139.7" y1="58.42" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<wire x1="134.62" y1="58.42" x2="139.7" y2="58.42" width="0.1524" layer="91"/>
<junction x="139.7" y="58.42"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="X32K_Q2"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="106.68" y1="63.5" x2="124.46" y2="63.5" width="0.1524" layer="91"/>
<wire x1="124.46" y1="63.5" x2="124.46" y2="58.42" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="1"/>
<wire x1="124.46" y1="58.42" x2="124.46" y2="48.26" width="0.1524" layer="91"/>
<wire x1="129.54" y1="58.42" x2="124.46" y2="58.42" width="0.1524" layer="91"/>
<junction x="124.46" y="58.42"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO3"/>
<wire x1="40.64" y1="121.92" x2="35.56" y2="121.92" width="0.1524" layer="91"/>
<label x="35.56" y="121.92" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="195.58" y1="129.54" x2="195.58" y2="127" width="0.1524" layer="91"/>
<label x="195.58" y="127" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO4"/>
<wire x1="40.64" y1="119.38" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<label x="35.56" y="119.38" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="205.74" y1="129.54" x2="205.74" y2="127" width="0.1524" layer="91"/>
<label x="205.74" y="127" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
</net>
<net name="GPOUT" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO5"/>
<wire x1="40.64" y1="116.84" x2="35.56" y2="116.84" width="0.1524" layer="91"/>
<label x="35.56" y="116.84" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="215.9" y1="129.54" x2="215.9" y2="127" width="0.1524" layer="91"/>
<label x="215.9" y="127" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
</net>
<net name="CS2" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO24"/>
<wire x1="106.68" y1="106.68" x2="111.76" y2="106.68" width="0.1524" layer="91"/>
<label x="111.76" y="106.68" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO25"/>
<wire x1="106.68" y1="104.14" x2="111.76" y2="104.14" width="0.1524" layer="91"/>
<label x="111.76" y="104.14" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="CS1" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO26"/>
<wire x1="106.68" y1="101.6" x2="111.76" y2="101.6" width="0.1524" layer="91"/>
<label x="111.76" y="101.6" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO27"/>
<wire x1="106.68" y1="99.06" x2="111.76" y2="99.06" width="0.1524" layer="91"/>
<label x="111.76" y="99.06" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO23"/>
<wire x1="106.68" y1="109.22" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
<label x="111.76" y="109.22" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="DIO28"/>
<wire x1="106.68" y1="96.52" x2="111.76" y2="96.52" width="0.1524" layer="91"/>
<label x="111.76" y="96.52" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="BALUN1" gate="G$1" pin="BP2"/>
<wire x1="116.84" y1="78.74" x2="111.76" y2="78.74" width="0.1524" layer="91"/>
<wire x1="111.76" y1="78.74" x2="111.76" y2="83.82" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="RF_P"/>
<wire x1="111.76" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="RF_N"/>
<wire x1="106.68" y1="78.74" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<wire x1="106.68" y1="76.2" x2="114.3" y2="76.2" width="0.1524" layer="91"/>
<wire x1="114.3" y1="76.2" x2="114.3" y2="83.82" width="0.1524" layer="91"/>
<pinref part="BALUN1" gate="G$1" pin="BP1"/>
<wire x1="114.3" y1="83.82" x2="116.84" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="50.8" y="96.52" size="1.27" layer="97" font="vector">Pin 13</text>
<text x="66.04" y="101.6" size="1.27" layer="97" font="vector">Pin 22</text>
<text x="76.2" y="101.6" size="1.27" layer="97" font="vector">Pin 44</text>
<text x="91.44" y="101.6" size="1.27" layer="97" font="vector">Pin 34</text>
<text x="177.8" y="101.6" size="1.27" layer="97" font="vector">Pin 45</text>
<text x="190.5" y="101.6" size="1.27" layer="97" font="vector">Pin 48</text>
<text x="134.62" y="93.98" size="1.27" layer="97" font="vector">L1 y C18 cerca del pin 33</text>
<text x="172.72" y="12.7" size="2.54" layer="94">BYPASS / DECOUPL</text>
<text x="239.522" y="20.828" size="1.778" layer="94" rot="MR0">Ing. Agustin I. Reyes</text>
</plain>
<instances>
<instance part="FRAME6" gate="G$1" x="0" y="0"/>
<instance part="FB1" gate="G$1" x="48.26" y="99.06" rot="R90"/>
<instance part="C13" gate="G$1" x="88.9" y="91.44"/>
<instance part="C14" gate="G$1" x="58.42" y="91.44"/>
<instance part="C15" gate="G$1" x="68.58" y="91.44"/>
<instance part="C16" gate="G$1" x="78.74" y="91.44"/>
<instance part="C17" gate="G$1" x="99.06" y="91.44"/>
<instance part="GND1" gate="1" x="58.42" y="81.28"/>
<instance part="GND2" gate="1" x="68.58" y="81.28"/>
<instance part="GND3" gate="1" x="78.74" y="81.28"/>
<instance part="GND4" gate="1" x="88.9" y="81.28"/>
<instance part="GND5" gate="1" x="99.06" y="81.28"/>
<instance part="SUPPLY2" gate="G$1" x="38.1" y="104.14"/>
<instance part="L9" gate="G$1" x="154.94" y="99.06" rot="R90"/>
<instance part="C18" gate="G$1" x="167.64" y="91.44"/>
<instance part="C19" gate="G$1" x="180.34" y="91.44"/>
<instance part="C20" gate="G$1" x="193.04" y="91.44"/>
<instance part="GND6" gate="1" x="167.64" y="81.28"/>
<instance part="GND7" gate="1" x="180.34" y="81.28"/>
<instance part="GND8" gate="1" x="193.04" y="81.28"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="58.42" y1="88.9" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="68.58" y1="88.9" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="78.74" y1="88.9" x2="78.74" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="88.9" y1="88.9" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="167.64" y1="88.9" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="180.34" y1="88.9" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="193.04" y1="88.9" x2="193.04" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDDS" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="FB1" gate="G$1" pin="2"/>
<wire x1="99.06" y1="96.52" x2="99.06" y2="99.06" width="0.1524" layer="91"/>
<wire x1="99.06" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="88.9" y1="99.06" x2="78.74" y2="99.06" width="0.1524" layer="91"/>
<wire x1="78.74" y1="99.06" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<wire x1="68.58" y1="99.06" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="53.34" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="96.52" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<junction x="58.42" y="99.06"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="68.58" y1="96.52" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<junction x="68.58" y="99.06"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="78.74" y1="96.52" x2="78.74" y2="99.06" width="0.1524" layer="91"/>
<junction x="78.74" y="99.06"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="88.9" y1="96.52" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<junction x="88.9" y="99.06"/>
<wire x1="58.42" y1="99.06" x2="58.42" y2="104.14" width="0.1524" layer="91"/>
<label x="58.42" y="104.14" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="SUPPLY2" gate="G$1" pin="3.3V"/>
<wire x1="38.1" y1="104.14" x2="38.1" y2="99.06" width="0.1524" layer="91"/>
<pinref part="FB1" gate="G$1" pin="1"/>
<wire x1="38.1" y1="99.06" x2="43.18" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DCDC_SW" class="0">
<segment>
<pinref part="L9" gate="G$1" pin="1"/>
<wire x1="149.86" y1="99.06" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<label x="142.24" y="99.06" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VDDR" class="0">
<segment>
<pinref part="L9" gate="G$1" pin="2"/>
<wire x1="160.02" y1="99.06" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="167.64" y1="99.06" x2="180.34" y2="99.06" width="0.1524" layer="91"/>
<wire x1="180.34" y1="99.06" x2="193.04" y2="99.06" width="0.1524" layer="91"/>
<wire x1="193.04" y1="99.06" x2="193.04" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="180.34" y1="96.52" x2="180.34" y2="99.06" width="0.1524" layer="91"/>
<junction x="180.34" y="99.06"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="167.64" y1="96.52" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<junction x="167.64" y="99.06"/>
<wire x1="167.64" y1="99.06" x2="167.64" y2="106.68" width="0.1524" layer="91"/>
<label x="167.64" y="106.68" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
</compatibility>
</eagle>
