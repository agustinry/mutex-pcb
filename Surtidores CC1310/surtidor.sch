<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.4.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-IC-Power">
<description>&lt;h3&gt;SparkFun Power Driver and Management ICs&lt;/h3&gt;
In this library you'll find anything that has to do with power delivery, or making power supplies.
&lt;p&gt;Contents:
&lt;ul&gt;&lt;li&gt;LDOs&lt;/li&gt;
&lt;li&gt;Boost/Buck controllers&lt;/li&gt;
&lt;li&gt;Charge pump controllers&lt;/li&gt;
&lt;li&gt;Power sequencers&lt;/li&gt;
&lt;li&gt;Power switches&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TO220-ADJ">
<wire x1="5.08" y1="-1.905" x2="-5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="1.905" x2="-5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.54" x2="8.255" y2="2.54" width="0.2032" layer="51"/>
<wire x1="8.255" y1="2.54" x2="8.255" y2="-5.715" width="0.2032" layer="51"/>
<wire x1="8.255" y1="-5.715" x2="9.525" y2="-5.715" width="0.2032" layer="51"/>
<wire x1="9.525" y1="-5.715" x2="9.525" y2="3.81" width="0.2032" layer="51"/>
<wire x1="9.525" y1="3.81" x2="-9.525" y2="3.81" width="0.2032" layer="51"/>
<wire x1="-9.525" y1="3.81" x2="-9.525" y2="-5.715" width="0.2032" layer="51"/>
<wire x1="-9.525" y1="-5.715" x2="-8.255" y2="-5.715" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="-5.715" x2="-8.255" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="2.54" x2="-5.08" y2="2.54" width="0.2032" layer="51"/>
<pad name="ADJ" x="-2.54" y="0" drill="1" diameter="1.8796" shape="square"/>
<pad name="OUT" x="0" y="0" drill="1" diameter="1.8796"/>
<pad name="IN" x="2.54" y="0" drill="1" diameter="1.8796"/>
</package>
<package name="SOT223">
<description>&lt;b&gt;SOT-223&lt;/b&gt;</description>
<wire x1="3.2766" y1="1.651" x2="3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.2766" y1="-1.651" x2="-3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="-1.651" x2="-3.2766" y2="1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="1.651" x2="3.2766" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="2" x="0" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="3" x="2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="4" x="0" y="3.099" dx="3.6" dy="2.2" layer="1"/>
<text x="-0.8255" y="4.5085" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
</package>
<package name="V-REG_DPACK">
<description>&lt;b&gt;DPAK&lt;/b&gt;&lt;p&gt;
PLASTIC PACKAGE CASE 369C-01&lt;br&gt;
Source: http://www.onsemi.co.jp .. LM317M-D.PDF</description>
<wire x1="3.2766" y1="3.8354" x2="3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-2.159" x2="-3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.159" x2="-3.2766" y2="3.8354" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="3.835" x2="3.2774" y2="3.8346" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.937" x2="-2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.6482" x2="-2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.1054" x2="2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.1054" x2="2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.6482" x2="2.5654" y2="3.937" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.937" x2="-2.5654" y2="3.937" width="0.2032" layer="51"/>
<smd name="1" x="-2.28" y="-4.8" dx="1.6" dy="3" layer="1"/>
<smd name="3" x="2.28" y="-4.8" dx="1.6" dy="3" layer="1"/>
<smd name="4" x="0" y="2.38" dx="5.8" dy="6.2" layer="1"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.0226" x2="0.4318" y2="-2.2606" layer="21"/>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="3.937"/>
<vertex x="-2.5654" y="4.6482"/>
<vertex x="-2.1082" y="5.1054"/>
<vertex x="2.1082" y="5.1054"/>
<vertex x="2.5654" y="4.6482"/>
<vertex x="2.5654" y="3.937"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="78ADJ-2">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="2.54" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">ADJ</text>
<text x="-4.445" y="-0.635" size="1.524" layer="95">IN</text>
<text x="0.635" y="-0.635" size="1.524" layer="95">OUT</text>
<pin name="IN" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="ADJ" x="0" y="-7.62" visible="off" length="short" direction="in" rot="R90"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="V_REG_317" prefix="U">
<description>&lt;b&gt;Voltage Regulator&lt;/b&gt;
Standard LM317 adjustable voltage regulator. AOI (Adjust Output Input). Google 'LM317 Calculator' for easy to use app to get the two resistor values needed. 240/720 for 5V output. 240/390 for 3.3V output. Spark Fun Electronics SKU : COM-00527</description>
<gates>
<gate name="G$1" symbol="78ADJ-2" x="0" y="0"/>
</gates>
<devices>
<device name="SINK" package="TO220-ADJ">
<connects>
<connect gate="G$1" pin="ADJ" pad="ADJ"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUT" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="SOT223">
<connects>
<connect gate="G$1" pin="ADJ" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DPACK" package="V-REG_DPACK">
<connects>
<connect gate="G$1" pin="ADJ" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-09888" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-wago-500" urn="urn:adsk.eagle:library:195">
<description>&lt;b&gt;Wago Screw Clamps&lt;/b&gt;&lt;p&gt;
Grid 5.00 mm&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="W237-102" library_version="1">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<wire x1="-3.491" y1="-2.286" x2="-1.484" y2="-0.279" width="0.254" layer="51"/>
<wire x1="1.488" y1="-2.261" x2="3.469" y2="-0.254" width="0.254" layer="51"/>
<wire x1="-4.989" y1="-5.461" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-5.461" x2="-4.989" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-3.389" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-3.389" y1="-3.073" x2="-1.611" y2="-3.073" width="0.1524" layer="51"/>
<wire x1="-1.611" y1="-3.073" x2="1.615" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="3.393" y1="-3.073" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-4.989" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="-3.073" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.531" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-3.073" x2="3.393" y2="-3.073" width="0.1524" layer="51"/>
<circle x="-2.5" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-2.5" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<circle x="2.5038" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="2.5038" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<text x="-5.04" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.8462" y="-5.0038" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.532" y="0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.421" y="0.635" size="1.27" layer="21" ratio="10">2</text>
</package>
</packages>
<symbols>
<symbol name="KL" library_version="1">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="KL+V" library_version="1">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="W237-102" prefix="X" uservalue="yes" library_version="1">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="KL+V" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-102">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="237-102" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="70K9898" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="1210">
<description>&lt;p&gt;&lt;b&gt;Generic 3225 (1210) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.5365" y1="1.1865" x2="1.5365" y2="1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="1.1865" x2="1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="-1.1865" x2="-1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="-1.5365" y1="-1.1865" x2="-1.5365" y2="1.1865" width="0.127" layer="51"/>
<smd name="1" x="-1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<smd name="2" x="1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<text x="0" y="1.397" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.59" y1="1.45" x2="2.59" y2="1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="1.45" x2="2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="-1.45" x2="-2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="-2.59" y1="-1.45" x2="-2.59" y2="1.45" width="0.0508" layer="39"/>
</package>
<package name="1206">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="12PF" prefix="C" uservalue="yes">
<description>&lt;h3&gt;12pF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-50V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09137" constant="no"/>
<attribute name="VALUE" value="12pF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22UF" prefix="C">
<description>&lt;h3&gt;22µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0805-6.3V-20%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08402"/>
<attribute name="VALUE" value="22uF"/>
</technology>
</technologies>
</device>
<device name="-1210-16V-20%" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2.2UF" prefix="C">
<description>&lt;h3&gt;2.2µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-10V-20%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-07888" constant="no"/>
<attribute name="VALUE" value="2.2uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-(+80/-20%)" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11624"/>
<attribute name="VALUE" value="2.2uF"/>
</technology>
</technologies>
</device>
<device name="-1206-50V-10%" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-10009"/>
<attribute name="VALUE" value="2.2uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.1UF" prefix="C">
<description>&lt;h3&gt;0.1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12416"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-(+80/-20%)" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08604"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-KIT-EZ-50V-20%" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08370"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-100V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08390"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.8PF" prefix="C">
<description>&lt;h3&gt;0.8pF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-50V-0.1PF" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-13456"/>
<attribute name="VALUE" value="0.8pF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.18UF" prefix="C">
<description>&lt;h3&gt;0.18µF ceramic capacitor&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-10V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12380"/>
<attribute name="VALUE" value="0.18uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED_5MM">
<description>&lt;B&gt;LED 5mm PTH&lt;/B&gt;&lt;p&gt;
5 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED-IR-THRU&lt;/li&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM">
<description>&lt;h3&gt;LED 3MM PTH&lt;/h3&gt;

3 mm, round.

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="22" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="22" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="22" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="22" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="22"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="22"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED-1206">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
</package>
<package name="LED-0603">
<description>&lt;B&gt;LED 0603 SMT&lt;/B&gt;&lt;p&gt;
0603, surface mount.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.075inch &lt;/li&gt;
&lt;li&gt;Area: 0.06" x 0.03"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED - BLUE&lt;/li&gt;</description>
<smd name="C" x="0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<smd name="A" x="-0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<text x="0" y="0.635" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5875" y1="0.47625" x2="1.5875" y2="-0.47625" width="0.127" layer="21"/>
<wire x1="0.15875" y1="0.47625" x2="0.15875" y2="0" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="0.15875" y2="-0.47625" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="-0.3175" width="0.127" layer="51"/>
</package>
<package name="LED_10MM">
<description>&lt;B&gt;LED 10mm PTH&lt;/B&gt;&lt;p&gt;
10 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.2inch&lt;/li&gt;
&lt;li&gt;Diameter: 10mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="22"/>
<text x="0" y="5.715" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.715" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="51"/>
</package>
<package name="FKIT-LED-1206">
<description>&lt;B&gt;LED 1206 SMT&lt;/B&gt;&lt;p&gt;
1206, surface mount
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125"x 0.06" &lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="0.3175" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="-0.3175" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM-NS">
<description>&lt;h3&gt;LED 3MM PTH- No Silk&lt;/h3&gt;

3 mm, round, no silk outline of package

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.8923" y1="1.2954" x2="1.8923" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.8923" y1="-1.2954" x2="1.8923" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED_5MM-KIT">
<description>&lt;h3&gt;LED 5mm KIT PTH&lt;/h3&gt;
&lt;p&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example Device:
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED-1206-BOTTOM">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-3.429" y="-4.572" size="1.778" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="1.905" y="-4.572" size="1.778" layer="96" font="vector" rot="R90" align="top-left">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LED (Generic)&lt;/b&gt;
&lt;p&gt;Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. Generic LEDs with no color specified.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED_3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08794" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED_10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED_3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED_5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.27OHM" prefix="R">
<description>&lt;h3&gt;0.27Ω resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08787"/>
<attribute name="VALUE" value="0.27"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.05OHM" prefix="R">
<description>&lt;h3&gt;0.05Ω resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/5W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12535"/>
<attribute name="VALUE" value="0.05"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+12V" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+12V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+12V" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-DiscreteSemi">
<description>&lt;h3&gt;SparkFun Discrete Semiconductors&lt;/h3&gt;
This library contains diodes, optoisolators, TRIACs, MOSFETs, transistors, etc. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SOD-323">
<description>SOD-323 (Small Outline Diode)</description>
<wire x1="-1.77" y1="0.625" x2="-1.77" y2="-0.625" width="0.2032" layer="21"/>
<smd name="C" x="-1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<smd name="A" x="1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-0.9" y1="0.625" x2="0.9" y2="0.625" width="0.2032" layer="21"/>
<wire x1="-0.9" y1="-0.625" x2="0.9" y2="-0.625" width="0.2032" layer="21"/>
</package>
<package name="SOD-523">
<description>SOD-523 (Small Outline Diode)</description>
<smd name="C" x="0.7" y="0" dx="0.4" dy="0.4" layer="1"/>
<smd name="A" x="-0.7" y="0" dx="0.4" dy="0.4" layer="1"/>
<wire x1="-0.625" y1="-0.425" x2="0.625" y2="-0.425" width="0.127" layer="21"/>
<wire x1="0.625" y1="0.425" x2="-0.625" y2="0.425" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.4" x2="0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.4" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.1" x2="0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.4" x2="0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="-0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.4" x2="-0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.1" x2="0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.1" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="1.1176" y1="0.3048" x2="1.1176" y2="-0.3048" width="0.2032" layer="21"/>
</package>
<package name="SMA-DIODE">
<description>&lt;B&gt;Diode&lt;/B&gt;
&lt;p&gt;Basic SMA packaged diode. Good for reverse polarization protection. Common part #: MBRA140&lt;/p&gt;
&lt;p&gt;SMA is the smallest package in the DO-214 standard (DO-214AC)&lt;/p&gt;</description>
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1" x2="3.175" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.15" y="0" dx="1.27" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.15" y="0" dx="1.27" dy="1.47" layer="1"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT23-3">
<description>SOT23-3</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<text x="-1.651" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.651" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="DPAK">
<wire x1="3.2766" y1="2.4654" x2="3.277" y2="-3.729" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-3.729" x2="-3.277" y2="-3.729" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-3.729" x2="-3.2766" y2="2.4654" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="2.465" x2="3.2774" y2="2.4646" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="2.567" x2="-2.5654" y2="3.2782" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.2782" x2="-2.1082" y2="3.7354" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="3.7354" x2="2.1082" y2="3.7354" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="3.7354" x2="2.5654" y2="3.2782" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.2782" x2="2.5654" y2="2.567" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="2.567" x2="-2.5654" y2="2.567" width="0.2032" layer="51"/>
<rectangle x1="-2.7178" y1="-6.7262" x2="-1.8542" y2="-3.8306" layer="51"/>
<rectangle x1="1.8542" y1="-6.7262" x2="2.7178" y2="-3.8306" layer="51"/>
<rectangle x1="-0.4318" y1="-4.5926" x2="0.4318" y2="-3.8306" layer="21"/>
<smd name="1" x="-2.28" y="-5.31" dx="1.6" dy="3" layer="1"/>
<smd name="3" x="2.28" y="-5.31" dx="1.6" dy="3" layer="1"/>
<smd name="4" x="0" y="1.588" dx="4.826" dy="5.715" layer="1"/>
<text x="-3.81" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="3.81" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="2.567"/>
<vertex x="-2.5654" y="3.2782"/>
<vertex x="-2.1082" y="3.7354"/>
<vertex x="2.1082" y="3.7354"/>
<vertex x="2.5654" y="3.2782"/>
<vertex x="2.5654" y="2.567"/>
</polygon>
</package>
<package name="SOT323">
<wire x1="1.1224" y1="0.6604" x2="1.1224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1224" y1="-0.6604" x2="-1.1224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1224" y1="-0.6604" x2="-1.1224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1224" y1="0.6604" x2="1.1224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.673" y1="0.7" x2="-1.1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.1" y1="0.7" x2="-1.1" y2="-0.354" width="0.2032" layer="21"/>
<wire x1="0.673" y1="0.7" x2="1.1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.354" width="0.2032" layer="21"/>
<smd name="1" x="-0.65" y="-0.925" dx="0.7" dy="0.7" layer="1"/>
<smd name="2" x="0.65" y="-0.925" dx="0.7" dy="0.7" layer="1"/>
<smd name="3" x="0" y="0.925" dx="0.7" dy="0.7" layer="1"/>
<text x="-1.27" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.27" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="TO220V">
<description>&lt;b&gt;TO 220 Vertical&lt;/b&gt; Package works with various parts including N-Channel MOSFET SparkFun SKU: COM-10213</description>
<wire x1="-5.08" y1="2.032" x2="-5.08" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.032" x2="5.08" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.032" x2="-5.08" y2="2.032" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="2.032" x2="-5.08" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="3.048" x2="5.08" y2="3.048" width="0.2032" layer="21"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="2.032" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-0.381" x2="-4.191" y2="-1.27" width="0.2032" layer="21" curve="92.798868"/>
<wire x1="4.191" y1="-1.27" x2="5.08" y2="-0.381" width="0.2032" layer="21" curve="92.798868"/>
<wire x1="-4.191" y1="-1.27" x2="-3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.191" y1="-1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<rectangle x1="-5.08" y1="2.032" x2="5.08" y2="3.048" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.334" y="0.254" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="5.334" y="0.254" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="TO-263/D2PAK">
<wire x1="5" y1="-1" x2="5" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="5" y1="-3.4" x2="-5" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="-5" y1="-3.4" x2="-5" y2="-1" width="0.2032" layer="21"/>
<rectangle x1="-3.27" y1="-7.6" x2="-1.81" y2="-3.4" layer="51"/>
<rectangle x1="-0.73" y1="-4.9" x2="0.73" y2="-3.4" layer="21"/>
<smd name="1" x="-2.54" y="-7.045" dx="2.32" dy="3.81" layer="1"/>
<smd name="3" x="2.54" y="-7.045" dx="2.32" dy="3.81" layer="1"/>
<smd name="2" x="0" y="4.125" dx="11" dy="9.65" layer="1"/>
<text x="-5.715" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="5.715" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
<polygon width="0.1998" layer="51">
<vertex x="-5" y="6.25"/>
<vertex x="-5" y="7"/>
<vertex x="-1" y="7.65"/>
<vertex x="1" y="7.65"/>
<vertex x="5" y="7"/>
<vertex x="5" y="6.25"/>
</polygon>
<wire x1="-5" y1="6.25" x2="-5" y2="-0.65" width="0.127" layer="51"/>
<wire x1="5" y1="-0.65" x2="5" y2="6.25" width="0.127" layer="51"/>
<wire x1="-1.81" y1="-3.4" x2="-1.81" y2="-7.6" width="0.127" layer="51"/>
<wire x1="-1.81" y1="-7.6" x2="-3.27" y2="-7.6" width="0.127" layer="51"/>
<wire x1="-3.27" y1="-7.6" x2="-3.27" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-3.27" y1="-3.4" x2="-1.81" y2="-3.4" width="0.127" layer="51"/>
<wire x1="1.81" y1="-3.4" x2="1.81" y2="-7.6" width="0.127" layer="51"/>
<wire x1="1.81" y1="-7.6" x2="3.27" y2="-7.6" width="0.127" layer="51"/>
<wire x1="3.27" y1="-7.6" x2="3.27" y2="-3.4" width="0.127" layer="51"/>
<wire x1="3.27" y1="-3.4" x2="1.81" y2="-3.4" width="0.127" layer="51"/>
<rectangle x1="1.81" y1="-7.6" x2="3.27" y2="-3.4" layer="51"/>
<rectangle x1="-3.27" y1="-4.9" x2="-1.81" y2="-3.4" layer="21"/>
<rectangle x1="1.81" y1="-4.9" x2="3.27" y2="-3.4" layer="21"/>
</package>
<package name="SO08">
<description>SOIC, 0.15 inch width</description>
<wire x1="2.3368" y1="1.9463" x2="-2.3368" y2="1.9463" width="0.2032" layer="21"/>
<wire x1="2.4368" y1="-1.9463" x2="2.7178" y2="-1.5653" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.7178" y1="1.4653" x2="-2.3368" y2="1.9463" width="0.2032" layer="21" curve="-90.023829"/>
<wire x1="2.3368" y1="1.9463" x2="2.7178" y2="1.5653" width="0.2032" layer="21" curve="-90.030084"/>
<wire x1="-2.7178" y1="-1.6653" x2="-2.3368" y2="-1.9463" width="0.2032" layer="21" curve="90.060185"/>
<wire x1="-2.3368" y1="-1.9463" x2="2.4368" y2="-1.9463" width="0.2032" layer="21"/>
<wire x1="2.7178" y1="-1.5653" x2="2.7178" y2="1.5653" width="0.2032" layer="21"/>
<wire x1="-2.667" y1="0.6096" x2="-2.667" y2="-0.6604" width="0.2032" layer="21" curve="-180"/>
<wire x1="-2.7178" y1="1.4526" x2="-2.7178" y2="0.6096" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="-1.6653" x2="-2.7178" y2="-0.6604" width="0.2032" layer="21"/>
<rectangle x1="-2.159" y1="-3.302" x2="-1.651" y2="-2.2733" layer="51"/>
<rectangle x1="-0.889" y1="-3.302" x2="-0.381" y2="-2.2733" layer="51"/>
<rectangle x1="0.381" y1="-3.302" x2="0.889" y2="-2.2733" layer="51"/>
<rectangle x1="1.651" y1="-3.302" x2="2.159" y2="-2.2733" layer="51"/>
<rectangle x1="-0.889" y1="2.286" x2="-0.381" y2="3.302" layer="51"/>
<rectangle x1="0.381" y1="2.286" x2="0.889" y2="3.302" layer="51"/>
<rectangle x1="1.651" y1="2.286" x2="2.159" y2="3.302" layer="51"/>
<smd name="1" x="-1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="2" x="-0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="3" x="0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="4" x="1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="5" x="1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="6" x="0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="7" x="-0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="8" x="-1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<text x="-3.175" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="3.81" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-2.159" y1="2.286" x2="-1.651" y2="3.302" layer="51"/>
<polygon width="0.002540625" layer="21">
<vertex x="-2.69875" y="-2.38125" curve="90"/>
<vertex x="-3.01625" y="-2.06375" curve="90"/>
<vertex x="-3.33375" y="-2.38125" curve="90"/>
<vertex x="-3.01625" y="-2.69875" curve="90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="DIODE-SCHOTTKY">
<description>&lt;h3&gt; Schottky Diode&lt;/h3&gt;
Diode with low voltage drop</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.778" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="94"/>
<text x="-2.54" y="2.032" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.27" x2="0.762" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="1.016" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-1.27" y="1.27"/>
<vertex x="1.27" y="0"/>
<vertex x="-1.27" y="-1.27"/>
</polygon>
</symbol>
<symbol name="LABELED-NMOS">
<description>&lt;h3&gt; N-channel MOSFET transistor&lt;/h3&gt;
Switches electronic signals</description>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="0.6858" x2="-1.9812" y2="-0.8382" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.2954" x2="-1.9812" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.905" x2="-1.9812" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="2.54" x2="-1.9812" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="1.8034" x2="-1.9812" y2="1.0922" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.2192" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.7112" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="1.8034" x2="2.54" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.8034" x2="2.54" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.5588" x2="3.302" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.5588" x2="1.778" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="1.778" y="-0.7112"/>
<vertex x="2.54" y="0.5588"/>
<vertex x="3.302" y="-0.7112"/>
</polygon>
<wire x1="3.302" y1="0.5588" x2="3.4798" y2="0.7366" width="0.1524" layer="94"/>
<wire x1="1.6002" y1="0.381" x2="1.778" y2="0.5588" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-1.9812" y="0"/>
<vertex x="-1.2192" y="0.254"/>
<vertex x="-1.2192" y="-0.254"/>
</polygon>
<text x="5.08" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIODE-SCHOTTKY" prefix="D">
<description>&lt;h3&gt;Schottky diode&lt;/h3&gt;
&lt;p&gt;A Schottky diode is a semiconductor diode which has a low forward voltage drop and a very fast switching action.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE-SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="-BAT20J" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11623"/>
<attribute name="VALUE" value="1A/23V/620mV"/>
</technology>
</technologies>
</device>
<device name="-RB751S40" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11018"/>
<attribute name="VALUE" value="120mA/40V/370mV"/>
</technology>
</technologies>
</device>
<device name="-SS14" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08053"/>
<attribute name="VALUE" value="1A/40V/500mV"/>
</technology>
</technologies>
</device>
<device name="-PMEG4005EJ" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-10955"/>
<attribute name="VALUE" value="0.5A/40V/420mV"/>
</technology>
</technologies>
</device>
<device name="-B340A" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09886"/>
<attribute name="VALUE" value="3A/40V/500mV"/>
</technology>
</technologies>
</device>
<device name="-ZLLS500" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08411"/>
<attribute name="VALUE" value="700mA/40V/533mV"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-NCH" prefix="Q">
<description>&lt;h3&gt;N-channel MOSFETs&lt;/h3&gt;
&lt;p&gt;Voltage controlled devices that allow control of high current outputs.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13261”&gt;SparkFun OpenScale&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12651”&gt;SparkFun Digital Sandbox&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/10182”&gt;SparkFun Monster Moto Shield&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/11214”&gt;SparkFun MOSFET Power Controller&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="NMOS" symbol="LABELED-NMOS" x="0" y="0"/>
</gates>
<devices>
<device name="-FDD8780" package="DPAK">
<connects>
<connect gate="NMOS" pin="D" pad="4"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-09984"/>
<attribute name="VALUE" value="35A/25V/8.5mΩ"/>
</technology>
</technologies>
</device>
<device name="-2N7002PW" package="SOT323">
<connects>
<connect gate="NMOS" pin="D" pad="3"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-11151"/>
<attribute name="VALUE" value="310mA/60V/1.6Ω"/>
</technology>
</technologies>
</device>
<device name="-FQP30N06L" package="TO220V">
<connects>
<connect gate="NMOS" pin="D" pad="2"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-10060"/>
<attribute name="VALUE" value="60V/32A/35mΩ"/>
</technology>
</technologies>
</device>
<device name="-BSS138" package="SOT23-3">
<connects>
<connect gate="NMOS" pin="D" pad="3"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-00830"/>
<attribute name="VALUE" value="220mA/50V/3.5Ω"/>
</technology>
</technologies>
</device>
<device name="-PSMN7R0" package="TO-263/D2PAK">
<connects>
<connect gate="NMOS" pin="D" pad="2"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-12437"/>
<attribute name="VALUE" value="100A/100V/6.8mΩ"/>
</technology>
</technologies>
</device>
<device name="-AO3404A" package="SOT23-3">
<connects>
<connect gate="NMOS" pin="D" pad="3"/>
<connect gate="NMOS" pin="G" pad="1"/>
<connect gate="NMOS" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-12988"/>
<attribute name="VALUE" value="5.8A/30V/35mΩ"/>
</technology>
</technologies>
</device>
<device name="-FDS6630A" package="SO08">
<connects>
<connect gate="NMOS" pin="D" pad="5 6 7 8"/>
<connect gate="NMOS" pin="G" pad="4"/>
<connect gate="NMOS" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-08089"/>
<attribute name="VALUE" value="6.5A/30V/38mΩ"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X03">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X3">
<description>&lt;h3&gt;PTH - 3 Pin Vertical Molex Polarized Header&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/01932.pdf"&gt; 4UCONN part # 01932 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<text x="1.143" y="2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0.889" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="2.159" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Locking Footprint&lt;/h3&gt;
Pins are staggered 0.005" off center to lock pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Long Pad w/ Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to lock pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<text x="-1.27" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X3_LOCK">
<description>&lt;h3&gt;PTH - 3 Pin Vertical Molex Polarized Header&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/01932.pdf"&gt; 4UCONN part # 01932 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<text x="1.143" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0.889" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH Locking&lt;/h3&gt;
Holes are offset 0.007" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="2.032" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="1.905" y="-4.699" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-PTH">
<description>&lt;h3&gt;JST 3 Pin Right Angle Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins - 3 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-5MM-3">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -3 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="13.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="4.2" x2="13.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="13.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="13.1" y1="4" x2="13.7" y2="4" width="0.2032" layer="51"/>
<wire x1="13.7" y1="4" x2="13.7" y2="3" width="0.2032" layer="51"/>
<wire x1="13.7" y1="3" x2="13.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.413" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.413"/>
<pad name="3" x="10" y="0" drill="1.3" diameter="2.413"/>
<text x="3.683" y="2.794" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.429" y="1.905" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Locking Footprint w/out Silk Outline&lt;/h3&gt;
Holes are offset from center 0.005" to lock pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-SMD">
<description>&lt;h3&gt;JST 3 Pin Right Angle SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.99" y1="-2.07" x2="-4.99" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.99" y1="-5.57" x2="-4.19" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-5.57" x2="-4.19" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-3.07" x2="-2.99" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="-3.07" x2="4.21" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-3.07" x2="4.21" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-5.57" x2="5.01" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="5.01" y1="-5.57" x2="5.01" y2="-2.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="1.93" x2="-2.99" y2="1.93" width="0.2032" layer="21"/>
<smd name="1" x="-1.99" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="3" x="2.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-4.39" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="4.41" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<text x="-1.397" y="0.635" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03-1MM-RA">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin SMD&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1" y1="-4.6" x2="1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="1.75" y1="-0.35" x2="2.4997" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.4997" y1="-0.35" x2="2.4997" y2="-2" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-0.35" x2="-1.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_FEMALE">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Female Header&lt;/h3&gt;
Silk outline of pin location
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.935" y1="4.25" x2="-3.935" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="4.25" x2="-3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="-4.25" x2="3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-3.935" y1="-4.25" x2="3.935" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-0.32" y1="6.8" x2="0.32" y2="7.65" layer="51"/>
<rectangle x1="2.22" y1="6.8" x2="2.86" y2="7.65" layer="51"/>
<rectangle x1="-2.86" y1="6.8" x2="-2.22" y2="7.65" layer="51"/>
<smd name="3" x="2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="0" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-1.524" y="0.889" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE">
<description>&lt;h3&gt;SMD- 3 Pin Right Angle Male Headers&lt;/h3&gt;
No silk outline, but tDocu layer shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<text x="-1.524" y="0.254" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE_POST">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Male Header w/ Alignment Posts&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt; 4UCONN part # 11026 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.6"/>
<hole x="1.27" y="0" drill="1.6"/>
<text x="-1.397" y="1.524" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-PTH-VERT">
<description>&lt;h3&gt;JST 3 Pin Vertical Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-2.25" x2="-3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="2.25" x2="3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="2.25" x2="3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.75" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.75" size="1.27" layer="51">-</text>
<text x="1.7" y="0.95" size="0.8" layer="51">S</text>
<text x="-1.397" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLER">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Male Header w/ Alignment Posts&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt; 4UCONN part # 11026 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3589"/>
<hole x="1.27" y="0" drill="1.3589"/>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLEST">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3462"/>
<hole x="1.27" y="0" drill="1.3462"/>
</package>
<package name="JST-3-PTH-NS">
<description>&lt;h3&gt;JST 3 Pin Right Angle Plated Through Hole &amp;ndash; NO SILK&lt;/h3&gt;
&lt;p&gt;No silkscreen outline. tDoc layer (51) indicates connector footprint.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH &amp;ndash; NO SILK&lt;/h3&gt;
&lt;p&gt;No silkscreen outline. tDoc layer (51) indicates connector footprint.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="2.159" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_03">
<description>&lt;h3&gt;3 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_03" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13875"&gt; Stackable Header - 3 Pin (Female, 0.1")&lt;/a&gt; (PRT-13875)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="J$1" symbol="CONN_03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08625" constant="no"/>
<attribute name="SF_ID" value="PRT-08096" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
<attribute name="SF_ID" value="PRT-08235" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08625" constant="no"/>
<attribute name="SF_ID" value="PRT-08096" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
<attribute name="SF_ID" value="PRT-08235" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="WIRE-10037" constant="no"/>
<attribute name="SF_ID" value="PRT-09915" constant="no"/>
</technology>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X03_PP_HOLES_ONLY">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SCREW-5MM" package="SCREWTERMINAL-5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10134" constant="no"/>
<attribute name="SF_SKU" value="PRT-08433" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X03_LOCK_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-SMD" package="JST-3-SMD">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12591" constant="no"/>
<attribute name="VALUE" value="3-PIN SMD" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X03-1MM-RA">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X03_SMD_RA_FEMALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10926"/>
<attribute name="VALUE" value="1x3 RA Female .1&quot;"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE" package="1X03_SMD_RA_MALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10925"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE_POST" package="1X03_SMD_RA_MALE_POST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-3-PTH-VERT">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13230" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLER" package="1X03_SMD_RA_MALE_POST_SMALLER">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11912" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLEST" package="1X03_SMD_RA_MALE_POST_SMALLEST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-NS" package="JST-3-PTH-NS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW-NS" package="SCREWTERMINAL-3.5MM-3-NS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="aestethics">
<packages>
<package name="MUTEX-LOGO">
<rectangle x1="5.9182" y1="0.9906" x2="6.0706" y2="1.0414" layer="37"/>
<rectangle x1="5.8674" y1="0.9906" x2="5.9182" y2="1.0414" layer="37"/>
<rectangle x1="6.0706" y1="0.9906" x2="6.1214" y2="1.0414" layer="37"/>
<rectangle x1="5.8166" y1="1.0414" x2="6.1722" y2="1.0922" layer="37"/>
<rectangle x1="8.509" y1="1.0414" x2="9.3218" y2="1.0922" layer="37"/>
<rectangle x1="3.2766" y1="1.0414" x2="3.3274" y2="1.0922" layer="37"/>
<rectangle x1="4.0894" y1="1.0414" x2="4.1402" y2="1.0922" layer="37"/>
<rectangle x1="4.9022" y1="1.0414" x2="4.953" y2="1.0922" layer="37"/>
<rectangle x1="5.7658" y1="1.0414" x2="5.8166" y2="1.0922" layer="37"/>
<rectangle x1="6.1722" y1="1.0414" x2="6.223" y2="1.0922" layer="37"/>
<rectangle x1="7.493" y1="1.0414" x2="7.5438" y2="1.0922" layer="37"/>
<rectangle x1="9.6774" y1="1.0414" x2="9.7282" y2="1.0922" layer="37"/>
<rectangle x1="10.8458" y1="1.0414" x2="10.8966" y2="1.0922" layer="37"/>
<rectangle x1="10.8966" y1="1.0414" x2="10.9474" y2="1.0922" layer="37"/>
<rectangle x1="5.715" y1="1.0414" x2="5.7658" y2="1.0922" layer="37"/>
<rectangle x1="6.223" y1="1.0414" x2="6.2738" y2="1.0922" layer="37"/>
<rectangle x1="8.4582" y1="1.0414" x2="8.509" y2="1.0922" layer="37"/>
<rectangle x1="4.0386" y1="1.0414" x2="4.0894" y2="1.0922" layer="37"/>
<rectangle x1="4.8514" y1="1.0414" x2="4.9022" y2="1.0922" layer="37"/>
<rectangle x1="9.3218" y1="1.0414" x2="9.3726" y2="1.0922" layer="37"/>
<rectangle x1="4.1402" y1="1.0414" x2="4.191" y2="1.0922" layer="37"/>
<rectangle x1="3.3274" y1="1.0414" x2="3.3782" y2="1.0922" layer="37"/>
<rectangle x1="9.7282" y1="1.0414" x2="9.779" y2="1.0922" layer="37"/>
<rectangle x1="7.5438" y1="1.0414" x2="7.5946" y2="1.0922" layer="37"/>
<rectangle x1="3.2766" y1="1.0922" x2="3.3274" y2="1.143" layer="37"/>
<rectangle x1="4.0386" y1="1.0922" x2="4.1402" y2="1.143" layer="37"/>
<rectangle x1="4.9022" y1="1.0922" x2="4.953" y2="1.143" layer="37"/>
<rectangle x1="5.6642" y1="1.0922" x2="5.7658" y2="1.143" layer="37"/>
<rectangle x1="6.1722" y1="1.0922" x2="6.3246" y2="1.143" layer="37"/>
<rectangle x1="7.493" y1="1.0922" x2="7.5438" y2="1.143" layer="37"/>
<rectangle x1="8.4582" y1="1.0922" x2="8.5598" y2="1.143" layer="37"/>
<rectangle x1="9.7282" y1="1.0922" x2="9.779" y2="1.143" layer="37"/>
<rectangle x1="10.8458" y1="1.0922" x2="10.8966" y2="1.143" layer="37"/>
<rectangle x1="4.1402" y1="1.0922" x2="4.191" y2="1.143" layer="37"/>
<rectangle x1="5.7658" y1="1.0922" x2="5.8166" y2="1.143" layer="37"/>
<rectangle x1="8.5598" y1="1.0922" x2="8.6106" y2="1.143" layer="37"/>
<rectangle x1="9.6774" y1="1.0922" x2="9.7282" y2="1.143" layer="37"/>
<rectangle x1="8.6106" y1="1.0922" x2="9.3218" y2="1.143" layer="37"/>
<rectangle x1="6.1214" y1="1.0922" x2="6.1722" y2="1.143" layer="37"/>
<rectangle x1="5.8166" y1="1.0922" x2="5.8674" y2="1.143" layer="37"/>
<rectangle x1="5.8674" y1="1.0922" x2="5.9182" y2="1.143" layer="37"/>
<rectangle x1="6.0706" y1="1.0922" x2="6.1214" y2="1.143" layer="37"/>
<rectangle x1="4.8514" y1="1.0922" x2="4.9022" y2="1.143" layer="37"/>
<rectangle x1="3.3274" y1="1.0922" x2="3.3782" y2="1.143" layer="37"/>
<rectangle x1="10.8966" y1="1.0922" x2="10.9474" y2="1.143" layer="37"/>
<rectangle x1="10.795" y1="1.0922" x2="10.8458" y2="1.143" layer="37"/>
<rectangle x1="9.3218" y1="1.0922" x2="9.3726" y2="1.143" layer="37"/>
<rectangle x1="5.6134" y1="1.0922" x2="5.6642" y2="1.143" layer="37"/>
<rectangle x1="7.5438" y1="1.0922" x2="7.5946" y2="1.143" layer="37"/>
<rectangle x1="6.3246" y1="1.0922" x2="6.3754" y2="1.143" layer="37"/>
<rectangle x1="3.2766" y1="1.143" x2="3.3274" y2="1.1938" layer="37"/>
<rectangle x1="4.0386" y1="1.143" x2="4.191" y2="1.1938" layer="37"/>
<rectangle x1="4.9022" y1="1.143" x2="4.953" y2="1.1938" layer="37"/>
<rectangle x1="5.6134" y1="1.143" x2="5.6642" y2="1.1938" layer="37"/>
<rectangle x1="6.2738" y1="1.143" x2="6.3754" y2="1.1938" layer="37"/>
<rectangle x1="7.493" y1="1.143" x2="7.5438" y2="1.1938" layer="37"/>
<rectangle x1="8.4582" y1="1.143" x2="8.5598" y2="1.1938" layer="37"/>
<rectangle x1="9.7282" y1="1.143" x2="9.8298" y2="1.1938" layer="37"/>
<rectangle x1="10.795" y1="1.143" x2="10.8458" y2="1.1938" layer="37"/>
<rectangle x1="5.6642" y1="1.143" x2="5.715" y2="1.1938" layer="37"/>
<rectangle x1="10.8458" y1="1.143" x2="10.8966" y2="1.1938" layer="37"/>
<rectangle x1="4.8514" y1="1.143" x2="4.9022" y2="1.1938" layer="37"/>
<rectangle x1="5.5626" y1="1.143" x2="5.6134" y2="1.1938" layer="37"/>
<rectangle x1="6.223" y1="1.143" x2="6.2738" y2="1.1938" layer="37"/>
<rectangle x1="3.3274" y1="1.143" x2="3.3782" y2="1.1938" layer="37"/>
<rectangle x1="3.9878" y1="1.143" x2="4.0386" y2="1.1938" layer="37"/>
<rectangle x1="5.715" y1="1.143" x2="5.7658" y2="1.1938" layer="37"/>
<rectangle x1="6.3754" y1="1.143" x2="6.4262" y2="1.1938" layer="37"/>
<rectangle x1="4.191" y1="1.143" x2="4.2418" y2="1.1938" layer="37"/>
<rectangle x1="7.5438" y1="1.143" x2="7.5946" y2="1.1938" layer="37"/>
<rectangle x1="3.2766" y1="1.1938" x2="3.3274" y2="1.2446" layer="37"/>
<rectangle x1="3.9878" y1="1.1938" x2="4.0386" y2="1.2446" layer="37"/>
<rectangle x1="4.1402" y1="1.1938" x2="4.191" y2="1.2446" layer="37"/>
<rectangle x1="4.9022" y1="1.1938" x2="4.953" y2="1.2446" layer="37"/>
<rectangle x1="5.5626" y1="1.1938" x2="5.6134" y2="1.2446" layer="37"/>
<rectangle x1="6.3754" y1="1.1938" x2="6.4262" y2="1.2446" layer="37"/>
<rectangle x1="7.493" y1="1.1938" x2="7.5438" y2="1.2446" layer="37"/>
<rectangle x1="8.4582" y1="1.1938" x2="8.5598" y2="1.2446" layer="37"/>
<rectangle x1="9.779" y1="1.1938" x2="9.8298" y2="1.2446" layer="37"/>
<rectangle x1="10.7442" y1="1.1938" x2="10.8458" y2="1.2446" layer="37"/>
<rectangle x1="6.3246" y1="1.1938" x2="6.3754" y2="1.2446" layer="37"/>
<rectangle x1="4.0386" y1="1.1938" x2="4.0894" y2="1.2446" layer="37"/>
<rectangle x1="4.191" y1="1.1938" x2="4.2418" y2="1.2446" layer="37"/>
<rectangle x1="5.6134" y1="1.1938" x2="5.6642" y2="1.2446" layer="37"/>
<rectangle x1="4.8514" y1="1.1938" x2="4.9022" y2="1.2446" layer="37"/>
<rectangle x1="3.3274" y1="1.1938" x2="3.3782" y2="1.2446" layer="37"/>
<rectangle x1="9.7282" y1="1.1938" x2="9.779" y2="1.2446" layer="37"/>
<rectangle x1="9.8298" y1="1.1938" x2="9.8806" y2="1.2446" layer="37"/>
<rectangle x1="5.5118" y1="1.1938" x2="5.5626" y2="1.2446" layer="37"/>
<rectangle x1="6.4262" y1="1.1938" x2="6.477" y2="1.2446" layer="37"/>
<rectangle x1="7.5438" y1="1.1938" x2="7.5946" y2="1.2446" layer="37"/>
<rectangle x1="3.2766" y1="1.2446" x2="3.3274" y2="1.2954" layer="37"/>
<rectangle x1="3.9878" y1="1.2446" x2="4.0386" y2="1.2954" layer="37"/>
<rectangle x1="4.191" y1="1.2446" x2="4.2418" y2="1.2954" layer="37"/>
<rectangle x1="4.9022" y1="1.2446" x2="4.953" y2="1.2954" layer="37"/>
<rectangle x1="5.5118" y1="1.2446" x2="5.5626" y2="1.2954" layer="37"/>
<rectangle x1="6.3754" y1="1.2446" x2="6.477" y2="1.2954" layer="37"/>
<rectangle x1="7.493" y1="1.2446" x2="7.5438" y2="1.2954" layer="37"/>
<rectangle x1="8.4582" y1="1.2446" x2="8.5598" y2="1.2954" layer="37"/>
<rectangle x1="9.8298" y1="1.2446" x2="9.8806" y2="1.2954" layer="37"/>
<rectangle x1="10.7442" y1="1.2446" x2="10.795" y2="1.2954" layer="37"/>
<rectangle x1="5.5626" y1="1.2446" x2="5.6134" y2="1.2954" layer="37"/>
<rectangle x1="9.779" y1="1.2446" x2="9.8298" y2="1.2954" layer="37"/>
<rectangle x1="10.6934" y1="1.2446" x2="10.7442" y2="1.2954" layer="37"/>
<rectangle x1="4.8514" y1="1.2446" x2="4.9022" y2="1.2954" layer="37"/>
<rectangle x1="3.3274" y1="1.2446" x2="3.3782" y2="1.2954" layer="37"/>
<rectangle x1="4.1402" y1="1.2446" x2="4.191" y2="1.2954" layer="37"/>
<rectangle x1="3.937" y1="1.2446" x2="3.9878" y2="1.2954" layer="37"/>
<rectangle x1="4.0386" y1="1.2446" x2="4.0894" y2="1.2954" layer="37"/>
<rectangle x1="10.795" y1="1.2446" x2="10.8458" y2="1.2954" layer="37"/>
<rectangle x1="7.5438" y1="1.2446" x2="7.5946" y2="1.2954" layer="37"/>
<rectangle x1="3.2766" y1="1.2954" x2="3.3274" y2="1.3462" layer="37"/>
<rectangle x1="4.191" y1="1.2954" x2="4.2418" y2="1.3462" layer="37"/>
<rectangle x1="4.9022" y1="1.2954" x2="4.953" y2="1.3462" layer="37"/>
<rectangle x1="5.5118" y1="1.2954" x2="5.5626" y2="1.3462" layer="37"/>
<rectangle x1="6.4262" y1="1.2954" x2="6.477" y2="1.3462" layer="37"/>
<rectangle x1="7.493" y1="1.2954" x2="7.5438" y2="1.3462" layer="37"/>
<rectangle x1="8.4582" y1="1.2954" x2="8.5598" y2="1.3462" layer="37"/>
<rectangle x1="9.8298" y1="1.2954" x2="9.9314" y2="1.3462" layer="37"/>
<rectangle x1="10.6934" y1="1.2954" x2="10.7442" y2="1.3462" layer="37"/>
<rectangle x1="3.937" y1="1.2954" x2="4.0386" y2="1.3462" layer="37"/>
<rectangle x1="4.2418" y1="1.2954" x2="4.2926" y2="1.3462" layer="37"/>
<rectangle x1="5.461" y1="1.2954" x2="5.5118" y2="1.3462" layer="37"/>
<rectangle x1="10.7442" y1="1.2954" x2="10.795" y2="1.3462" layer="37"/>
<rectangle x1="4.8514" y1="1.2954" x2="4.9022" y2="1.3462" layer="37"/>
<rectangle x1="6.477" y1="1.2954" x2="6.5278" y2="1.3462" layer="37"/>
<rectangle x1="3.3274" y1="1.2954" x2="3.3782" y2="1.3462" layer="37"/>
<rectangle x1="7.5438" y1="1.2954" x2="7.5946" y2="1.3462" layer="37"/>
<rectangle x1="6.3754" y1="1.2954" x2="6.4262" y2="1.3462" layer="37"/>
<rectangle x1="3.2766" y1="1.3462" x2="3.3274" y2="1.397" layer="37"/>
<rectangle x1="3.937" y1="1.3462" x2="3.9878" y2="1.397" layer="37"/>
<rectangle x1="4.2418" y1="1.3462" x2="4.2926" y2="1.397" layer="37"/>
<rectangle x1="4.9022" y1="1.3462" x2="4.953" y2="1.397" layer="37"/>
<rectangle x1="5.461" y1="1.3462" x2="5.5118" y2="1.397" layer="37"/>
<rectangle x1="6.477" y1="1.3462" x2="6.5278" y2="1.397" layer="37"/>
<rectangle x1="7.493" y1="1.3462" x2="7.5438" y2="1.397" layer="37"/>
<rectangle x1="8.4582" y1="1.3462" x2="8.5598" y2="1.397" layer="37"/>
<rectangle x1="9.8806" y1="1.3462" x2="9.9314" y2="1.397" layer="37"/>
<rectangle x1="10.6426" y1="1.3462" x2="10.6934" y2="1.397" layer="37"/>
<rectangle x1="6.4262" y1="1.3462" x2="6.477" y2="1.397" layer="37"/>
<rectangle x1="10.6934" y1="1.3462" x2="10.7442" y2="1.397" layer="37"/>
<rectangle x1="5.5118" y1="1.3462" x2="5.5626" y2="1.397" layer="37"/>
<rectangle x1="9.9314" y1="1.3462" x2="9.9822" y2="1.397" layer="37"/>
<rectangle x1="4.8514" y1="1.3462" x2="4.9022" y2="1.397" layer="37"/>
<rectangle x1="3.3274" y1="1.3462" x2="3.3782" y2="1.397" layer="37"/>
<rectangle x1="3.9878" y1="1.3462" x2="4.0386" y2="1.397" layer="37"/>
<rectangle x1="4.191" y1="1.3462" x2="4.2418" y2="1.397" layer="37"/>
<rectangle x1="9.8298" y1="1.3462" x2="9.8806" y2="1.397" layer="37"/>
<rectangle x1="7.5438" y1="1.3462" x2="7.5946" y2="1.397" layer="37"/>
<rectangle x1="3.2766" y1="1.397" x2="3.3274" y2="1.4478" layer="37"/>
<rectangle x1="4.2418" y1="1.397" x2="4.2926" y2="1.4478" layer="37"/>
<rectangle x1="4.9022" y1="1.397" x2="4.953" y2="1.4478" layer="37"/>
<rectangle x1="5.461" y1="1.397" x2="5.5118" y2="1.4478" layer="37"/>
<rectangle x1="6.477" y1="1.397" x2="6.5278" y2="1.4478" layer="37"/>
<rectangle x1="7.493" y1="1.397" x2="7.5438" y2="1.4478" layer="37"/>
<rectangle x1="8.4582" y1="1.397" x2="8.5598" y2="1.4478" layer="37"/>
<rectangle x1="9.9314" y1="1.397" x2="9.9822" y2="1.4478" layer="37"/>
<rectangle x1="10.6426" y1="1.397" x2="10.6934" y2="1.4478" layer="37"/>
<rectangle x1="3.937" y1="1.397" x2="3.9878" y2="1.4478" layer="37"/>
<rectangle x1="3.8862" y1="1.397" x2="3.937" y2="1.4478" layer="37"/>
<rectangle x1="10.5918" y1="1.397" x2="10.6426" y2="1.4478" layer="37"/>
<rectangle x1="4.2926" y1="1.397" x2="4.3434" y2="1.4478" layer="37"/>
<rectangle x1="9.8806" y1="1.397" x2="9.9314" y2="1.4478" layer="37"/>
<rectangle x1="4.8514" y1="1.397" x2="4.9022" y2="1.4478" layer="37"/>
<rectangle x1="3.3274" y1="1.397" x2="3.3782" y2="1.4478" layer="37"/>
<rectangle x1="5.4102" y1="1.397" x2="5.461" y2="1.4478" layer="37"/>
<rectangle x1="7.5438" y1="1.397" x2="7.5946" y2="1.4478" layer="37"/>
<rectangle x1="6.4262" y1="1.397" x2="6.477" y2="1.4478" layer="37"/>
<rectangle x1="3.2766" y1="1.4478" x2="3.3274" y2="1.4986" layer="37"/>
<rectangle x1="3.8862" y1="1.4478" x2="3.937" y2="1.4986" layer="37"/>
<rectangle x1="4.2926" y1="1.4478" x2="4.3434" y2="1.4986" layer="37"/>
<rectangle x1="4.9022" y1="1.4478" x2="4.953" y2="1.4986" layer="37"/>
<rectangle x1="5.4102" y1="1.4478" x2="5.5118" y2="1.4986" layer="37"/>
<rectangle x1="6.477" y1="1.4478" x2="6.5278" y2="1.4986" layer="37"/>
<rectangle x1="7.493" y1="1.4478" x2="7.5438" y2="1.4986" layer="37"/>
<rectangle x1="8.4582" y1="1.4478" x2="8.5598" y2="1.4986" layer="37"/>
<rectangle x1="9.9822" y1="1.4478" x2="10.033" y2="1.4986" layer="37"/>
<rectangle x1="10.5918" y1="1.4478" x2="10.6426" y2="1.4986" layer="37"/>
<rectangle x1="9.9314" y1="1.4478" x2="9.9822" y2="1.4986" layer="37"/>
<rectangle x1="3.937" y1="1.4478" x2="3.9878" y2="1.4986" layer="37"/>
<rectangle x1="4.2418" y1="1.4478" x2="4.2926" y2="1.4986" layer="37"/>
<rectangle x1="6.5278" y1="1.4478" x2="6.5786" y2="1.4986" layer="37"/>
<rectangle x1="4.8514" y1="1.4478" x2="4.9022" y2="1.4986" layer="37"/>
<rectangle x1="10.6426" y1="1.4478" x2="10.6934" y2="1.4986" layer="37"/>
<rectangle x1="3.3274" y1="1.4478" x2="3.3782" y2="1.4986" layer="37"/>
<rectangle x1="10.541" y1="1.4478" x2="10.5918" y2="1.4986" layer="37"/>
<rectangle x1="7.5438" y1="1.4478" x2="7.5946" y2="1.4986" layer="37"/>
<rectangle x1="3.2766" y1="1.4986" x2="3.3274" y2="1.5494" layer="37"/>
<rectangle x1="3.8862" y1="1.4986" x2="3.937" y2="1.5494" layer="37"/>
<rectangle x1="4.2926" y1="1.4986" x2="4.3434" y2="1.5494" layer="37"/>
<rectangle x1="4.9022" y1="1.4986" x2="4.953" y2="1.5494" layer="37"/>
<rectangle x1="5.4102" y1="1.4986" x2="5.5118" y2="1.5494" layer="37"/>
<rectangle x1="6.477" y1="1.4986" x2="6.5786" y2="1.5494" layer="37"/>
<rectangle x1="7.493" y1="1.4986" x2="7.5438" y2="1.5494" layer="37"/>
<rectangle x1="8.4582" y1="1.4986" x2="8.5598" y2="1.5494" layer="37"/>
<rectangle x1="9.9822" y1="1.4986" x2="10.033" y2="1.5494" layer="37"/>
<rectangle x1="10.541" y1="1.4986" x2="10.5918" y2="1.5494" layer="37"/>
<rectangle x1="10.033" y1="1.4986" x2="10.0838" y2="1.5494" layer="37"/>
<rectangle x1="10.5918" y1="1.4986" x2="10.6426" y2="1.5494" layer="37"/>
<rectangle x1="3.8354" y1="1.4986" x2="3.8862" y2="1.5494" layer="37"/>
<rectangle x1="4.3434" y1="1.4986" x2="4.3942" y2="1.5494" layer="37"/>
<rectangle x1="4.8514" y1="1.4986" x2="4.9022" y2="1.5494" layer="37"/>
<rectangle x1="3.3274" y1="1.4986" x2="3.3782" y2="1.5494" layer="37"/>
<rectangle x1="7.5438" y1="1.4986" x2="7.5946" y2="1.5494" layer="37"/>
<rectangle x1="3.2766" y1="1.5494" x2="3.3274" y2="1.6002" layer="37"/>
<rectangle x1="3.8354" y1="1.5494" x2="3.8862" y2="1.6002" layer="37"/>
<rectangle x1="4.3434" y1="1.5494" x2="4.3942" y2="1.6002" layer="37"/>
<rectangle x1="4.9022" y1="1.5494" x2="4.953" y2="1.6002" layer="37"/>
<rectangle x1="5.4102" y1="1.5494" x2="5.461" y2="1.6002" layer="37"/>
<rectangle x1="6.477" y1="1.5494" x2="6.5786" y2="1.6002" layer="37"/>
<rectangle x1="7.493" y1="1.5494" x2="7.5438" y2="1.6002" layer="37"/>
<rectangle x1="8.4582" y1="1.5494" x2="8.5598" y2="1.6002" layer="37"/>
<rectangle x1="10.033" y1="1.5494" x2="10.0838" y2="1.6002" layer="37"/>
<rectangle x1="10.4902" y1="1.5494" x2="10.5918" y2="1.6002" layer="37"/>
<rectangle x1="3.8862" y1="1.5494" x2="3.937" y2="1.6002" layer="37"/>
<rectangle x1="4.2926" y1="1.5494" x2="4.3434" y2="1.6002" layer="37"/>
<rectangle x1="5.461" y1="1.5494" x2="5.5118" y2="1.6002" layer="37"/>
<rectangle x1="9.9822" y1="1.5494" x2="10.033" y2="1.6002" layer="37"/>
<rectangle x1="4.8514" y1="1.5494" x2="4.9022" y2="1.6002" layer="37"/>
<rectangle x1="3.3274" y1="1.5494" x2="3.3782" y2="1.6002" layer="37"/>
<rectangle x1="10.0838" y1="1.5494" x2="10.1346" y2="1.6002" layer="37"/>
<rectangle x1="7.5438" y1="1.5494" x2="7.5946" y2="1.6002" layer="37"/>
<rectangle x1="3.2766" y1="1.6002" x2="3.3274" y2="1.651" layer="37"/>
<rectangle x1="3.8354" y1="1.6002" x2="3.8862" y2="1.651" layer="37"/>
<rectangle x1="4.3434" y1="1.6002" x2="4.3942" y2="1.651" layer="37"/>
<rectangle x1="4.9022" y1="1.6002" x2="4.953" y2="1.651" layer="37"/>
<rectangle x1="5.4102" y1="1.6002" x2="5.461" y2="1.651" layer="37"/>
<rectangle x1="6.477" y1="1.6002" x2="6.5786" y2="1.651" layer="37"/>
<rectangle x1="7.493" y1="1.6002" x2="7.5438" y2="1.651" layer="37"/>
<rectangle x1="8.4582" y1="1.6002" x2="8.5598" y2="1.651" layer="37"/>
<rectangle x1="10.0838" y1="1.6002" x2="10.1346" y2="1.651" layer="37"/>
<rectangle x1="10.4902" y1="1.6002" x2="10.541" y2="1.651" layer="37"/>
<rectangle x1="10.033" y1="1.6002" x2="10.0838" y2="1.651" layer="37"/>
<rectangle x1="3.7846" y1="1.6002" x2="3.8354" y2="1.651" layer="37"/>
<rectangle x1="4.8514" y1="1.6002" x2="4.9022" y2="1.651" layer="37"/>
<rectangle x1="10.4394" y1="1.6002" x2="10.4902" y2="1.651" layer="37"/>
<rectangle x1="4.3942" y1="1.6002" x2="4.445" y2="1.651" layer="37"/>
<rectangle x1="3.3274" y1="1.6002" x2="3.3782" y2="1.651" layer="37"/>
<rectangle x1="5.461" y1="1.6002" x2="5.5118" y2="1.651" layer="37"/>
<rectangle x1="10.541" y1="1.6002" x2="10.5918" y2="1.651" layer="37"/>
<rectangle x1="7.5438" y1="1.6002" x2="7.5946" y2="1.651" layer="37"/>
<rectangle x1="3.2766" y1="1.651" x2="3.3274" y2="1.7018" layer="37"/>
<rectangle x1="3.7846" y1="1.651" x2="3.8862" y2="1.7018" layer="37"/>
<rectangle x1="4.3942" y1="1.651" x2="4.445" y2="1.7018" layer="37"/>
<rectangle x1="4.9022" y1="1.651" x2="4.953" y2="1.7018" layer="37"/>
<rectangle x1="5.4102" y1="1.651" x2="5.461" y2="1.7018" layer="37"/>
<rectangle x1="6.477" y1="1.651" x2="6.5786" y2="1.7018" layer="37"/>
<rectangle x1="7.493" y1="1.651" x2="7.5438" y2="1.7018" layer="37"/>
<rectangle x1="8.4582" y1="1.651" x2="8.5598" y2="1.7018" layer="37"/>
<rectangle x1="10.0838" y1="1.651" x2="10.1854" y2="1.7018" layer="37"/>
<rectangle x1="10.4394" y1="1.651" x2="10.4902" y2="1.7018" layer="37"/>
<rectangle x1="4.3434" y1="1.651" x2="4.3942" y2="1.7018" layer="37"/>
<rectangle x1="10.4902" y1="1.651" x2="10.541" y2="1.7018" layer="37"/>
<rectangle x1="4.8514" y1="1.651" x2="4.9022" y2="1.7018" layer="37"/>
<rectangle x1="3.3274" y1="1.651" x2="3.3782" y2="1.7018" layer="37"/>
<rectangle x1="5.461" y1="1.651" x2="5.5118" y2="1.7018" layer="37"/>
<rectangle x1="7.5438" y1="1.651" x2="7.5946" y2="1.7018" layer="37"/>
<rectangle x1="3.2766" y1="1.7018" x2="3.3274" y2="1.7526" layer="37"/>
<rectangle x1="3.7846" y1="1.7018" x2="3.8354" y2="1.7526" layer="37"/>
<rectangle x1="4.3942" y1="1.7018" x2="4.445" y2="1.7526" layer="37"/>
<rectangle x1="4.9022" y1="1.7018" x2="4.953" y2="1.7526" layer="37"/>
<rectangle x1="5.4102" y1="1.7018" x2="5.461" y2="1.7526" layer="37"/>
<rectangle x1="6.477" y1="1.7018" x2="6.5786" y2="1.7526" layer="37"/>
<rectangle x1="7.493" y1="1.7018" x2="7.5438" y2="1.7526" layer="37"/>
<rectangle x1="8.4582" y1="1.7018" x2="8.5598" y2="1.7526" layer="37"/>
<rectangle x1="10.1346" y1="1.7018" x2="10.1854" y2="1.7526" layer="37"/>
<rectangle x1="10.3886" y1="1.7018" x2="10.4902" y2="1.7526" layer="37"/>
<rectangle x1="4.8514" y1="1.7018" x2="4.9022" y2="1.7526" layer="37"/>
<rectangle x1="10.1854" y1="1.7018" x2="10.2362" y2="1.7526" layer="37"/>
<rectangle x1="10.0838" y1="1.7018" x2="10.1346" y2="1.7526" layer="37"/>
<rectangle x1="3.3274" y1="1.7018" x2="3.3782" y2="1.7526" layer="37"/>
<rectangle x1="3.7338" y1="1.7018" x2="3.7846" y2="1.7526" layer="37"/>
<rectangle x1="5.461" y1="1.7018" x2="5.5118" y2="1.7526" layer="37"/>
<rectangle x1="4.445" y1="1.7018" x2="4.4958" y2="1.7526" layer="37"/>
<rectangle x1="7.5438" y1="1.7018" x2="7.5946" y2="1.7526" layer="37"/>
<rectangle x1="3.2766" y1="1.7526" x2="3.3274" y2="1.8034" layer="37"/>
<rectangle x1="3.7846" y1="1.7526" x2="3.8354" y2="1.8034" layer="37"/>
<rectangle x1="4.3942" y1="1.7526" x2="4.4958" y2="1.8034" layer="37"/>
<rectangle x1="4.9022" y1="1.7526" x2="4.953" y2="1.8034" layer="37"/>
<rectangle x1="5.4102" y1="1.7526" x2="5.461" y2="1.8034" layer="37"/>
<rectangle x1="6.477" y1="1.7526" x2="6.5786" y2="1.8034" layer="37"/>
<rectangle x1="7.493" y1="1.7526" x2="7.5438" y2="1.8034" layer="37"/>
<rectangle x1="8.4582" y1="1.7526" x2="8.5598" y2="1.8034" layer="37"/>
<rectangle x1="10.1854" y1="1.7526" x2="10.2362" y2="1.8034" layer="37"/>
<rectangle x1="10.3886" y1="1.7526" x2="10.4394" y2="1.8034" layer="37"/>
<rectangle x1="3.7338" y1="1.7526" x2="3.7846" y2="1.8034" layer="37"/>
<rectangle x1="10.1346" y1="1.7526" x2="10.1854" y2="1.8034" layer="37"/>
<rectangle x1="10.3378" y1="1.7526" x2="10.3886" y2="1.8034" layer="37"/>
<rectangle x1="4.8514" y1="1.7526" x2="4.9022" y2="1.8034" layer="37"/>
<rectangle x1="3.3274" y1="1.7526" x2="3.3782" y2="1.8034" layer="37"/>
<rectangle x1="5.461" y1="1.7526" x2="5.5118" y2="1.8034" layer="37"/>
<rectangle x1="7.5438" y1="1.7526" x2="7.5946" y2="1.8034" layer="37"/>
<rectangle x1="10.4394" y1="1.7526" x2="10.4902" y2="1.8034" layer="37"/>
<rectangle x1="3.2766" y1="1.8034" x2="3.3274" y2="1.8542" layer="37"/>
<rectangle x1="3.7338" y1="1.8034" x2="3.7846" y2="1.8542" layer="37"/>
<rectangle x1="4.445" y1="1.8034" x2="4.4958" y2="1.8542" layer="37"/>
<rectangle x1="4.9022" y1="1.8034" x2="4.953" y2="1.8542" layer="37"/>
<rectangle x1="5.4102" y1="1.8034" x2="5.461" y2="1.8542" layer="37"/>
<rectangle x1="6.477" y1="1.8034" x2="6.5786" y2="1.8542" layer="37"/>
<rectangle x1="7.493" y1="1.8034" x2="7.5438" y2="1.8542" layer="37"/>
<rectangle x1="8.4582" y1="1.8034" x2="8.5598" y2="1.8542" layer="37"/>
<rectangle x1="10.1854" y1="1.8034" x2="10.2362" y2="1.8542" layer="37"/>
<rectangle x1="10.3378" y1="1.8034" x2="10.3886" y2="1.8542" layer="37"/>
<rectangle x1="10.2362" y1="1.8034" x2="10.287" y2="1.8542" layer="37"/>
<rectangle x1="10.287" y1="1.8034" x2="10.3378" y2="1.8542" layer="37"/>
<rectangle x1="10.3886" y1="1.8034" x2="10.4394" y2="1.8542" layer="37"/>
<rectangle x1="4.8514" y1="1.8034" x2="4.9022" y2="1.8542" layer="37"/>
<rectangle x1="3.3274" y1="1.8034" x2="3.3782" y2="1.8542" layer="37"/>
<rectangle x1="5.461" y1="1.8034" x2="5.5118" y2="1.8542" layer="37"/>
<rectangle x1="3.683" y1="1.8034" x2="3.7338" y2="1.8542" layer="37"/>
<rectangle x1="4.4958" y1="1.8034" x2="4.5466" y2="1.8542" layer="37"/>
<rectangle x1="7.5438" y1="1.8034" x2="7.5946" y2="1.8542" layer="37"/>
<rectangle x1="3.2766" y1="1.8542" x2="3.3274" y2="1.905" layer="37"/>
<rectangle x1="3.7338" y1="1.8542" x2="3.7846" y2="1.905" layer="37"/>
<rectangle x1="4.445" y1="1.8542" x2="4.5466" y2="1.905" layer="37"/>
<rectangle x1="4.9022" y1="1.8542" x2="4.953" y2="1.905" layer="37"/>
<rectangle x1="5.4102" y1="1.8542" x2="5.461" y2="1.905" layer="37"/>
<rectangle x1="6.477" y1="1.8542" x2="6.5786" y2="1.905" layer="37"/>
<rectangle x1="7.493" y1="1.8542" x2="7.5438" y2="1.905" layer="37"/>
<rectangle x1="8.4582" y1="1.8542" x2="8.5598" y2="1.905" layer="37"/>
<rectangle x1="10.2362" y1="1.8542" x2="10.3378" y2="1.905" layer="37"/>
<rectangle x1="3.683" y1="1.8542" x2="3.7338" y2="1.905" layer="37"/>
<rectangle x1="10.3378" y1="1.8542" x2="10.3886" y2="1.905" layer="37"/>
<rectangle x1="8.5598" y1="1.8542" x2="8.6106" y2="1.905" layer="37"/>
<rectangle x1="8.6106" y1="1.8542" x2="9.1694" y2="1.905" layer="37"/>
<rectangle x1="4.8514" y1="1.8542" x2="4.9022" y2="1.905" layer="37"/>
<rectangle x1="3.3274" y1="1.8542" x2="3.3782" y2="1.905" layer="37"/>
<rectangle x1="5.461" y1="1.8542" x2="5.5118" y2="1.905" layer="37"/>
<rectangle x1="10.1854" y1="1.8542" x2="10.2362" y2="1.905" layer="37"/>
<rectangle x1="7.5438" y1="1.8542" x2="7.5946" y2="1.905" layer="37"/>
<rectangle x1="3.2766" y1="1.905" x2="3.3274" y2="1.9558" layer="37"/>
<rectangle x1="3.683" y1="1.905" x2="3.7338" y2="1.9558" layer="37"/>
<rectangle x1="4.4958" y1="1.905" x2="4.5466" y2="1.9558" layer="37"/>
<rectangle x1="4.9022" y1="1.905" x2="4.953" y2="1.9558" layer="37"/>
<rectangle x1="5.4102" y1="1.905" x2="5.461" y2="1.9558" layer="37"/>
<rectangle x1="6.477" y1="1.905" x2="6.5786" y2="1.9558" layer="37"/>
<rectangle x1="7.493" y1="1.905" x2="7.5438" y2="1.9558" layer="37"/>
<rectangle x1="8.4582" y1="1.905" x2="9.1694" y2="1.9558" layer="37"/>
<rectangle x1="10.2362" y1="1.905" x2="10.3378" y2="1.9558" layer="37"/>
<rectangle x1="10.3378" y1="1.905" x2="10.3886" y2="1.9558" layer="37"/>
<rectangle x1="4.8514" y1="1.905" x2="4.9022" y2="1.9558" layer="37"/>
<rectangle x1="9.1694" y1="1.905" x2="9.2202" y2="1.9558" layer="37"/>
<rectangle x1="3.3274" y1="1.905" x2="3.3782" y2="1.9558" layer="37"/>
<rectangle x1="5.461" y1="1.905" x2="5.5118" y2="1.9558" layer="37"/>
<rectangle x1="3.6322" y1="1.905" x2="3.683" y2="1.9558" layer="37"/>
<rectangle x1="7.5438" y1="1.905" x2="7.5946" y2="1.9558" layer="37"/>
<rectangle x1="3.7338" y1="1.905" x2="3.7846" y2="1.9558" layer="37"/>
<rectangle x1="4.445" y1="1.905" x2="4.4958" y2="1.9558" layer="37"/>
<rectangle x1="4.5466" y1="1.905" x2="4.5974" y2="1.9558" layer="37"/>
<rectangle x1="3.2766" y1="1.9558" x2="3.3274" y2="2.0066" layer="37"/>
<rectangle x1="3.683" y1="1.9558" x2="3.7338" y2="2.0066" layer="37"/>
<rectangle x1="4.4958" y1="1.9558" x2="4.5466" y2="2.0066" layer="37"/>
<rectangle x1="4.9022" y1="1.9558" x2="4.953" y2="2.0066" layer="37"/>
<rectangle x1="5.4102" y1="1.9558" x2="5.461" y2="2.0066" layer="37"/>
<rectangle x1="6.477" y1="1.9558" x2="6.5786" y2="2.0066" layer="37"/>
<rectangle x1="7.493" y1="1.9558" x2="7.5438" y2="2.0066" layer="37"/>
<rectangle x1="8.4582" y1="1.9558" x2="8.5598" y2="2.0066" layer="37"/>
<rectangle x1="10.2362" y1="1.9558" x2="10.3886" y2="2.0066" layer="37"/>
<rectangle x1="3.6322" y1="1.9558" x2="3.683" y2="2.0066" layer="37"/>
<rectangle x1="4.5466" y1="1.9558" x2="4.5974" y2="2.0066" layer="37"/>
<rectangle x1="10.1854" y1="1.9558" x2="10.2362" y2="2.0066" layer="37"/>
<rectangle x1="4.8514" y1="1.9558" x2="4.9022" y2="2.0066" layer="37"/>
<rectangle x1="3.3274" y1="1.9558" x2="3.3782" y2="2.0066" layer="37"/>
<rectangle x1="5.461" y1="1.9558" x2="5.5118" y2="2.0066" layer="37"/>
<rectangle x1="7.5438" y1="1.9558" x2="7.5946" y2="2.0066" layer="37"/>
<rectangle x1="3.2766" y1="2.0066" x2="3.3274" y2="2.0574" layer="37"/>
<rectangle x1="3.6322" y1="2.0066" x2="3.683" y2="2.0574" layer="37"/>
<rectangle x1="4.5466" y1="2.0066" x2="4.5974" y2="2.0574" layer="37"/>
<rectangle x1="4.9022" y1="2.0066" x2="4.953" y2="2.0574" layer="37"/>
<rectangle x1="5.4102" y1="2.0066" x2="5.461" y2="2.0574" layer="37"/>
<rectangle x1="6.477" y1="2.0066" x2="6.5786" y2="2.0574" layer="37"/>
<rectangle x1="7.493" y1="2.0066" x2="7.5438" y2="2.0574" layer="37"/>
<rectangle x1="8.4582" y1="2.0066" x2="8.5598" y2="2.0574" layer="37"/>
<rectangle x1="10.3378" y1="2.0066" x2="10.3886" y2="2.0574" layer="37"/>
<rectangle x1="10.1854" y1="2.0066" x2="10.287" y2="2.0574" layer="37"/>
<rectangle x1="10.3886" y1="2.0066" x2="10.4394" y2="2.0574" layer="37"/>
<rectangle x1="4.8514" y1="2.0066" x2="4.9022" y2="2.0574" layer="37"/>
<rectangle x1="3.3274" y1="2.0066" x2="3.3782" y2="2.0574" layer="37"/>
<rectangle x1="5.461" y1="2.0066" x2="5.5118" y2="2.0574" layer="37"/>
<rectangle x1="3.683" y1="2.0066" x2="3.7338" y2="2.0574" layer="37"/>
<rectangle x1="4.4958" y1="2.0066" x2="4.5466" y2="2.0574" layer="37"/>
<rectangle x1="10.287" y1="2.0066" x2="10.3378" y2="2.0574" layer="37"/>
<rectangle x1="7.5438" y1="2.0066" x2="7.5946" y2="2.0574" layer="37"/>
<rectangle x1="3.2766" y1="2.0574" x2="3.3274" y2="2.1082" layer="37"/>
<rectangle x1="3.6322" y1="2.0574" x2="3.683" y2="2.1082" layer="37"/>
<rectangle x1="4.5466" y1="2.0574" x2="4.5974" y2="2.1082" layer="37"/>
<rectangle x1="4.9022" y1="2.0574" x2="4.953" y2="2.1082" layer="37"/>
<rectangle x1="5.4102" y1="2.0574" x2="5.461" y2="2.1082" layer="37"/>
<rectangle x1="6.477" y1="2.0574" x2="6.5786" y2="2.1082" layer="37"/>
<rectangle x1="7.493" y1="2.0574" x2="7.5438" y2="2.1082" layer="37"/>
<rectangle x1="8.4582" y1="2.0574" x2="8.5598" y2="2.1082" layer="37"/>
<rectangle x1="10.1854" y1="2.0574" x2="10.2362" y2="2.1082" layer="37"/>
<rectangle x1="10.3886" y1="2.0574" x2="10.4394" y2="2.1082" layer="37"/>
<rectangle x1="3.5814" y1="2.0574" x2="3.6322" y2="2.1082" layer="37"/>
<rectangle x1="4.5974" y1="2.0574" x2="4.6482" y2="2.1082" layer="37"/>
<rectangle x1="10.1346" y1="2.0574" x2="10.1854" y2="2.1082" layer="37"/>
<rectangle x1="10.4394" y1="2.0574" x2="10.4902" y2="2.1082" layer="37"/>
<rectangle x1="4.8514" y1="2.0574" x2="4.9022" y2="2.1082" layer="37"/>
<rectangle x1="3.3274" y1="2.0574" x2="3.3782" y2="2.1082" layer="37"/>
<rectangle x1="5.461" y1="2.0574" x2="5.5118" y2="2.1082" layer="37"/>
<rectangle x1="10.3378" y1="2.0574" x2="10.3886" y2="2.1082" layer="37"/>
<rectangle x1="7.5438" y1="2.0574" x2="7.5946" y2="2.1082" layer="37"/>
<rectangle x1="3.2766" y1="2.1082" x2="3.3274" y2="2.159" layer="37"/>
<rectangle x1="3.5814" y1="2.1082" x2="3.6322" y2="2.159" layer="37"/>
<rectangle x1="4.5974" y1="2.1082" x2="4.6482" y2="2.159" layer="37"/>
<rectangle x1="4.9022" y1="2.1082" x2="4.953" y2="2.159" layer="37"/>
<rectangle x1="5.4102" y1="2.1082" x2="5.461" y2="2.159" layer="37"/>
<rectangle x1="6.477" y1="2.1082" x2="6.5786" y2="2.159" layer="37"/>
<rectangle x1="7.493" y1="2.1082" x2="7.5438" y2="2.159" layer="37"/>
<rectangle x1="8.4582" y1="2.1082" x2="8.5598" y2="2.159" layer="37"/>
<rectangle x1="10.1346" y1="2.1082" x2="10.1854" y2="2.159" layer="37"/>
<rectangle x1="10.4394" y1="2.1082" x2="10.4902" y2="2.159" layer="37"/>
<rectangle x1="10.3886" y1="2.1082" x2="10.4394" y2="2.159" layer="37"/>
<rectangle x1="10.0838" y1="2.1082" x2="10.1346" y2="2.159" layer="37"/>
<rectangle x1="10.1854" y1="2.1082" x2="10.2362" y2="2.159" layer="37"/>
<rectangle x1="4.8514" y1="2.1082" x2="4.9022" y2="2.159" layer="37"/>
<rectangle x1="3.6322" y1="2.1082" x2="3.683" y2="2.159" layer="37"/>
<rectangle x1="3.3274" y1="2.1082" x2="3.3782" y2="2.159" layer="37"/>
<rectangle x1="4.5466" y1="2.1082" x2="4.5974" y2="2.159" layer="37"/>
<rectangle x1="5.461" y1="2.1082" x2="5.5118" y2="2.159" layer="37"/>
<rectangle x1="10.4902" y1="2.1082" x2="10.541" y2="2.159" layer="37"/>
<rectangle x1="7.5438" y1="2.1082" x2="7.5946" y2="2.159" layer="37"/>
<rectangle x1="3.2766" y1="2.159" x2="3.3274" y2="2.2098" layer="37"/>
<rectangle x1="3.5814" y1="2.159" x2="3.6322" y2="2.2098" layer="37"/>
<rectangle x1="4.5974" y1="2.159" x2="4.699" y2="2.2098" layer="37"/>
<rectangle x1="4.9022" y1="2.159" x2="4.953" y2="2.2098" layer="37"/>
<rectangle x1="5.4102" y1="2.159" x2="5.461" y2="2.2098" layer="37"/>
<rectangle x1="6.477" y1="2.159" x2="6.5786" y2="2.2098" layer="37"/>
<rectangle x1="7.493" y1="2.159" x2="7.5438" y2="2.2098" layer="37"/>
<rectangle x1="8.4582" y1="2.159" x2="8.5598" y2="2.2098" layer="37"/>
<rectangle x1="10.0838" y1="2.159" x2="10.1346" y2="2.2098" layer="37"/>
<rectangle x1="10.4902" y1="2.159" x2="10.541" y2="2.2098" layer="37"/>
<rectangle x1="10.1346" y1="2.159" x2="10.1854" y2="2.2098" layer="37"/>
<rectangle x1="10.4394" y1="2.159" x2="10.4902" y2="2.2098" layer="37"/>
<rectangle x1="3.5306" y1="2.159" x2="3.5814" y2="2.2098" layer="37"/>
<rectangle x1="4.8514" y1="2.159" x2="4.9022" y2="2.2098" layer="37"/>
<rectangle x1="3.3274" y1="2.159" x2="3.3782" y2="2.2098" layer="37"/>
<rectangle x1="5.461" y1="2.159" x2="5.5118" y2="2.2098" layer="37"/>
<rectangle x1="7.5438" y1="2.159" x2="7.5946" y2="2.2098" layer="37"/>
<rectangle x1="3.2766" y1="2.2098" x2="3.3274" y2="2.2606" layer="37"/>
<rectangle x1="3.5306" y1="2.2098" x2="3.5814" y2="2.2606" layer="37"/>
<rectangle x1="4.6482" y1="2.2098" x2="4.699" y2="2.2606" layer="37"/>
<rectangle x1="4.9022" y1="2.2098" x2="4.953" y2="2.2606" layer="37"/>
<rectangle x1="5.4102" y1="2.2098" x2="5.461" y2="2.2606" layer="37"/>
<rectangle x1="6.477" y1="2.2098" x2="6.5786" y2="2.2606" layer="37"/>
<rectangle x1="7.493" y1="2.2098" x2="7.5438" y2="2.2606" layer="37"/>
<rectangle x1="8.4582" y1="2.2098" x2="8.5598" y2="2.2606" layer="37"/>
<rectangle x1="10.0838" y1="2.2098" x2="10.1346" y2="2.2606" layer="37"/>
<rectangle x1="10.4902" y1="2.2098" x2="10.541" y2="2.2606" layer="37"/>
<rectangle x1="10.033" y1="2.2098" x2="10.0838" y2="2.2606" layer="37"/>
<rectangle x1="10.541" y1="2.2098" x2="10.5918" y2="2.2606" layer="37"/>
<rectangle x1="3.5814" y1="2.2098" x2="3.6322" y2="2.2606" layer="37"/>
<rectangle x1="4.5974" y1="2.2098" x2="4.6482" y2="2.2606" layer="37"/>
<rectangle x1="4.8514" y1="2.2098" x2="4.9022" y2="2.2606" layer="37"/>
<rectangle x1="3.3274" y1="2.2098" x2="3.3782" y2="2.2606" layer="37"/>
<rectangle x1="5.461" y1="2.2098" x2="5.5118" y2="2.2606" layer="37"/>
<rectangle x1="7.5438" y1="2.2098" x2="7.5946" y2="2.2606" layer="37"/>
<rectangle x1="3.2766" y1="2.2606" x2="3.3274" y2="2.3114" layer="37"/>
<rectangle x1="3.5306" y1="2.2606" x2="3.5814" y2="2.3114" layer="37"/>
<rectangle x1="4.6482" y1="2.2606" x2="4.699" y2="2.3114" layer="37"/>
<rectangle x1="4.9022" y1="2.2606" x2="4.953" y2="2.3114" layer="37"/>
<rectangle x1="5.4102" y1="2.2606" x2="5.461" y2="2.3114" layer="37"/>
<rectangle x1="6.477" y1="2.2606" x2="6.5786" y2="2.3114" layer="37"/>
<rectangle x1="7.493" y1="2.2606" x2="7.5438" y2="2.3114" layer="37"/>
<rectangle x1="8.4582" y1="2.2606" x2="8.5598" y2="2.3114" layer="37"/>
<rectangle x1="10.033" y1="2.2606" x2="10.0838" y2="2.3114" layer="37"/>
<rectangle x1="10.541" y1="2.2606" x2="10.5918" y2="2.3114" layer="37"/>
<rectangle x1="4.699" y1="2.2606" x2="4.7498" y2="2.3114" layer="37"/>
<rectangle x1="3.4798" y1="2.2606" x2="3.5306" y2="2.3114" layer="37"/>
<rectangle x1="9.9822" y1="2.2606" x2="10.033" y2="2.3114" layer="37"/>
<rectangle x1="10.4902" y1="2.2606" x2="10.541" y2="2.3114" layer="37"/>
<rectangle x1="4.8514" y1="2.2606" x2="4.9022" y2="2.3114" layer="37"/>
<rectangle x1="3.3274" y1="2.2606" x2="3.3782" y2="2.3114" layer="37"/>
<rectangle x1="5.461" y1="2.2606" x2="5.5118" y2="2.3114" layer="37"/>
<rectangle x1="10.0838" y1="2.2606" x2="10.1346" y2="2.3114" layer="37"/>
<rectangle x1="10.5918" y1="2.2606" x2="10.6426" y2="2.3114" layer="37"/>
<rectangle x1="7.5438" y1="2.2606" x2="7.5946" y2="2.3114" layer="37"/>
<rectangle x1="3.2766" y1="2.3114" x2="3.3274" y2="2.3622" layer="37"/>
<rectangle x1="3.4798" y1="2.3114" x2="3.5306" y2="2.3622" layer="37"/>
<rectangle x1="4.699" y1="2.3114" x2="4.7498" y2="2.3622" layer="37"/>
<rectangle x1="4.9022" y1="2.3114" x2="4.953" y2="2.3622" layer="37"/>
<rectangle x1="5.4102" y1="2.3114" x2="5.461" y2="2.3622" layer="37"/>
<rectangle x1="6.477" y1="2.3114" x2="6.5786" y2="2.3622" layer="37"/>
<rectangle x1="7.493" y1="2.3114" x2="7.5438" y2="2.3622" layer="37"/>
<rectangle x1="8.4582" y1="2.3114" x2="8.5598" y2="2.3622" layer="37"/>
<rectangle x1="9.9822" y1="2.3114" x2="10.033" y2="2.3622" layer="37"/>
<rectangle x1="10.5918" y1="2.3114" x2="10.6426" y2="2.3622" layer="37"/>
<rectangle x1="10.541" y1="2.3114" x2="10.5918" y2="2.3622" layer="37"/>
<rectangle x1="10.033" y1="2.3114" x2="10.0838" y2="2.3622" layer="37"/>
<rectangle x1="3.5306" y1="2.3114" x2="3.5814" y2="2.3622" layer="37"/>
<rectangle x1="4.6482" y1="2.3114" x2="4.699" y2="2.3622" layer="37"/>
<rectangle x1="4.8514" y1="2.3114" x2="4.9022" y2="2.3622" layer="37"/>
<rectangle x1="3.3274" y1="2.3114" x2="3.3782" y2="2.3622" layer="37"/>
<rectangle x1="5.461" y1="2.3114" x2="5.5118" y2="2.3622" layer="37"/>
<rectangle x1="7.5438" y1="2.3114" x2="7.5946" y2="2.3622" layer="37"/>
<rectangle x1="3.2766" y1="2.3622" x2="3.3274" y2="2.413" layer="37"/>
<rectangle x1="3.4798" y1="2.3622" x2="3.5306" y2="2.413" layer="37"/>
<rectangle x1="4.699" y1="2.3622" x2="4.7498" y2="2.413" layer="37"/>
<rectangle x1="4.9022" y1="2.3622" x2="4.953" y2="2.413" layer="37"/>
<rectangle x1="5.4102" y1="2.3622" x2="5.461" y2="2.413" layer="37"/>
<rectangle x1="6.477" y1="2.3622" x2="6.5786" y2="2.413" layer="37"/>
<rectangle x1="7.493" y1="2.3622" x2="7.5438" y2="2.413" layer="37"/>
<rectangle x1="8.4582" y1="2.3622" x2="8.5598" y2="2.413" layer="37"/>
<rectangle x1="9.9314" y1="2.3622" x2="10.033" y2="2.413" layer="37"/>
<rectangle x1="10.5918" y1="2.3622" x2="10.6426" y2="2.413" layer="37"/>
<rectangle x1="10.6426" y1="2.3622" x2="10.6934" y2="2.413" layer="37"/>
<rectangle x1="4.7498" y1="2.3622" x2="4.8006" y2="2.413" layer="37"/>
<rectangle x1="3.429" y1="2.3622" x2="3.4798" y2="2.413" layer="37"/>
<rectangle x1="4.8514" y1="2.3622" x2="4.9022" y2="2.413" layer="37"/>
<rectangle x1="3.3274" y1="2.3622" x2="3.3782" y2="2.413" layer="37"/>
<rectangle x1="5.461" y1="2.3622" x2="5.5118" y2="2.413" layer="37"/>
<rectangle x1="7.5438" y1="2.3622" x2="7.5946" y2="2.413" layer="37"/>
<rectangle x1="3.2766" y1="2.413" x2="3.3274" y2="2.4638" layer="37"/>
<rectangle x1="3.429" y1="2.413" x2="3.4798" y2="2.4638" layer="37"/>
<rectangle x1="4.7498" y1="2.413" x2="4.8006" y2="2.4638" layer="37"/>
<rectangle x1="4.9022" y1="2.413" x2="4.953" y2="2.4638" layer="37"/>
<rectangle x1="5.4102" y1="2.413" x2="5.461" y2="2.4638" layer="37"/>
<rectangle x1="6.477" y1="2.413" x2="6.5786" y2="2.4638" layer="37"/>
<rectangle x1="7.493" y1="2.413" x2="7.5438" y2="2.4638" layer="37"/>
<rectangle x1="8.4582" y1="2.413" x2="8.5598" y2="2.4638" layer="37"/>
<rectangle x1="9.9314" y1="2.413" x2="9.9822" y2="2.4638" layer="37"/>
<rectangle x1="10.6426" y1="2.413" x2="10.6934" y2="2.4638" layer="37"/>
<rectangle x1="3.4798" y1="2.413" x2="3.5306" y2="2.4638" layer="37"/>
<rectangle x1="4.699" y1="2.413" x2="4.7498" y2="2.4638" layer="37"/>
<rectangle x1="4.8514" y1="2.413" x2="4.9022" y2="2.4638" layer="37"/>
<rectangle x1="3.3274" y1="2.413" x2="3.3782" y2="2.4638" layer="37"/>
<rectangle x1="9.8806" y1="2.413" x2="9.9314" y2="2.4638" layer="37"/>
<rectangle x1="5.461" y1="2.413" x2="5.5118" y2="2.4638" layer="37"/>
<rectangle x1="10.5918" y1="2.413" x2="10.6426" y2="2.4638" layer="37"/>
<rectangle x1="10.6934" y1="2.413" x2="10.7442" y2="2.4638" layer="37"/>
<rectangle x1="3.3782" y1="2.413" x2="3.429" y2="2.4638" layer="37"/>
<rectangle x1="9.9822" y1="2.413" x2="10.033" y2="2.4638" layer="37"/>
<rectangle x1="7.5438" y1="2.413" x2="7.5946" y2="2.4638" layer="37"/>
<rectangle x1="3.2766" y1="2.4638" x2="3.4798" y2="2.5146" layer="37"/>
<rectangle x1="4.7498" y1="2.4638" x2="4.953" y2="2.5146" layer="37"/>
<rectangle x1="5.4102" y1="2.4638" x2="5.461" y2="2.5146" layer="37"/>
<rectangle x1="6.477" y1="2.4638" x2="6.5786" y2="2.5146" layer="37"/>
<rectangle x1="7.493" y1="2.4638" x2="7.5438" y2="2.5146" layer="37"/>
<rectangle x1="8.4582" y1="2.4638" x2="8.5598" y2="2.5146" layer="37"/>
<rectangle x1="9.8806" y1="2.4638" x2="9.9314" y2="2.5146" layer="37"/>
<rectangle x1="10.6934" y1="2.4638" x2="10.7442" y2="2.5146" layer="37"/>
<rectangle x1="10.6426" y1="2.4638" x2="10.6934" y2="2.5146" layer="37"/>
<rectangle x1="9.9314" y1="2.4638" x2="9.9822" y2="2.5146" layer="37"/>
<rectangle x1="5.461" y1="2.4638" x2="5.5118" y2="2.5146" layer="37"/>
<rectangle x1="7.5438" y1="2.4638" x2="7.5946" y2="2.5146" layer="37"/>
<rectangle x1="9.8298" y1="2.4638" x2="9.8806" y2="2.5146" layer="37"/>
<rectangle x1="3.2766" y1="2.5146" x2="3.429" y2="2.5654" layer="37"/>
<rectangle x1="4.8006" y1="2.5146" x2="4.953" y2="2.5654" layer="37"/>
<rectangle x1="5.4102" y1="2.5146" x2="5.461" y2="2.5654" layer="37"/>
<rectangle x1="6.477" y1="2.5146" x2="6.5786" y2="2.5654" layer="37"/>
<rectangle x1="7.493" y1="2.5146" x2="7.5438" y2="2.5654" layer="37"/>
<rectangle x1="8.4582" y1="2.5146" x2="8.5598" y2="2.5654" layer="37"/>
<rectangle x1="9.8298" y1="2.5146" x2="9.9314" y2="2.5654" layer="37"/>
<rectangle x1="10.6934" y1="2.5146" x2="10.7442" y2="2.5654" layer="37"/>
<rectangle x1="3.429" y1="2.5146" x2="3.4798" y2="2.5654" layer="37"/>
<rectangle x1="4.7498" y1="2.5146" x2="4.8006" y2="2.5654" layer="37"/>
<rectangle x1="10.7442" y1="2.5146" x2="10.795" y2="2.5654" layer="37"/>
<rectangle x1="5.461" y1="2.5146" x2="5.5118" y2="2.5654" layer="37"/>
<rectangle x1="7.5438" y1="2.5146" x2="7.5946" y2="2.5654" layer="37"/>
<rectangle x1="3.2766" y1="2.5654" x2="3.429" y2="2.6162" layer="37"/>
<rectangle x1="4.8006" y1="2.5654" x2="4.953" y2="2.6162" layer="37"/>
<rectangle x1="5.4102" y1="2.5654" x2="5.461" y2="2.6162" layer="37"/>
<rectangle x1="6.477" y1="2.5654" x2="6.5786" y2="2.6162" layer="37"/>
<rectangle x1="7.493" y1="2.5654" x2="7.5438" y2="2.6162" layer="37"/>
<rectangle x1="8.4582" y1="2.5654" x2="8.5598" y2="2.6162" layer="37"/>
<rectangle x1="9.8298" y1="2.5654" x2="9.8806" y2="2.6162" layer="37"/>
<rectangle x1="10.7442" y1="2.5654" x2="10.795" y2="2.6162" layer="37"/>
<rectangle x1="9.779" y1="2.5654" x2="9.8298" y2="2.6162" layer="37"/>
<rectangle x1="10.795" y1="2.5654" x2="10.8458" y2="2.6162" layer="37"/>
<rectangle x1="5.461" y1="2.5654" x2="5.5118" y2="2.6162" layer="37"/>
<rectangle x1="10.6934" y1="2.5654" x2="10.7442" y2="2.6162" layer="37"/>
<rectangle x1="7.5438" y1="2.5654" x2="7.5946" y2="2.6162" layer="37"/>
<rectangle x1="4.7498" y1="2.5654" x2="4.8006" y2="2.6162" layer="37"/>
<rectangle x1="3.2766" y1="2.6162" x2="3.3782" y2="2.667" layer="37"/>
<rectangle x1="4.8514" y1="2.6162" x2="4.953" y2="2.667" layer="37"/>
<rectangle x1="5.4102" y1="2.6162" x2="5.461" y2="2.667" layer="37"/>
<rectangle x1="6.477" y1="2.6162" x2="6.5786" y2="2.667" layer="37"/>
<rectangle x1="7.493" y1="2.6162" x2="7.5438" y2="2.667" layer="37"/>
<rectangle x1="8.4582" y1="2.6162" x2="8.5598" y2="2.667" layer="37"/>
<rectangle x1="9.779" y1="2.6162" x2="9.8298" y2="2.667" layer="37"/>
<rectangle x1="10.795" y1="2.6162" x2="10.8458" y2="2.667" layer="37"/>
<rectangle x1="3.3782" y1="2.6162" x2="3.429" y2="2.667" layer="37"/>
<rectangle x1="4.8006" y1="2.6162" x2="4.8514" y2="2.667" layer="37"/>
<rectangle x1="10.7442" y1="2.6162" x2="10.795" y2="2.667" layer="37"/>
<rectangle x1="9.8298" y1="2.6162" x2="9.8806" y2="2.667" layer="37"/>
<rectangle x1="5.461" y1="2.6162" x2="5.5118" y2="2.667" layer="37"/>
<rectangle x1="7.5438" y1="2.6162" x2="7.5946" y2="2.667" layer="37"/>
<rectangle x1="9.7282" y1="2.6162" x2="9.779" y2="2.667" layer="37"/>
<rectangle x1="3.2766" y1="2.667" x2="3.3782" y2="2.7178" layer="37"/>
<rectangle x1="4.8514" y1="2.667" x2="4.953" y2="2.7178" layer="37"/>
<rectangle x1="5.4102" y1="2.667" x2="5.461" y2="2.7178" layer="37"/>
<rectangle x1="6.477" y1="2.667" x2="6.5786" y2="2.7178" layer="37"/>
<rectangle x1="7.493" y1="2.667" x2="7.5438" y2="2.7178" layer="37"/>
<rectangle x1="8.4582" y1="2.667" x2="8.5598" y2="2.7178" layer="37"/>
<rectangle x1="9.7282" y1="2.667" x2="9.8298" y2="2.7178" layer="37"/>
<rectangle x1="10.795" y1="2.667" x2="10.8966" y2="2.7178" layer="37"/>
<rectangle x1="7.5438" y1="2.667" x2="7.5946" y2="2.7178" layer="37"/>
<rectangle x1="7.4422" y1="2.667" x2="7.493" y2="2.7178" layer="37"/>
<rectangle x1="8.5598" y1="2.667" x2="8.6106" y2="2.7178" layer="37"/>
<rectangle x1="7.5946" y1="2.667" x2="7.6454" y2="2.7178" layer="37"/>
<rectangle x1="5.461" y1="2.667" x2="5.5118" y2="2.7178" layer="37"/>
<rectangle x1="6.985" y1="2.667" x2="7.4422" y2="2.7178" layer="37"/>
<rectangle x1="7.6454" y1="2.667" x2="8.1026" y2="2.7178" layer="37"/>
<rectangle x1="8.6106" y1="2.667" x2="9.271" y2="2.7178" layer="37"/>
<rectangle x1="6.9342" y1="2.667" x2="6.985" y2="2.7178" layer="37"/>
<rectangle x1="9.271" y1="2.667" x2="9.3218" y2="2.7178" layer="37"/>
<rectangle x1="4.8006" y1="2.667" x2="4.8514" y2="2.7178" layer="37"/>
<rectangle x1="8.1026" y1="2.667" x2="8.1534" y2="2.7178" layer="37"/>
<rectangle x1="3.3782" y1="2.667" x2="3.429" y2="2.7178" layer="37"/>
<rectangle x1="3.2766" y1="2.7178" x2="3.3274" y2="2.7686" layer="37"/>
<rectangle x1="4.9022" y1="2.7178" x2="4.953" y2="2.7686" layer="37"/>
<rectangle x1="5.4102" y1="2.7178" x2="5.461" y2="2.7686" layer="37"/>
<rectangle x1="6.5278" y1="2.7178" x2="6.5786" y2="2.7686" layer="37"/>
<rectangle x1="6.9342" y1="2.7178" x2="8.1534" y2="2.7686" layer="37"/>
<rectangle x1="8.509" y1="2.7178" x2="9.3218" y2="2.7686" layer="37"/>
<rectangle x1="9.7282" y1="2.7178" x2="9.779" y2="2.7686" layer="37"/>
<rectangle x1="10.8458" y1="2.7178" x2="10.8966" y2="2.7686" layer="37"/>
<rectangle x1="3.3274" y1="2.7178" x2="3.3782" y2="2.7686" layer="37"/>
<rectangle x1="4.8514" y1="2.7178" x2="4.9022" y2="2.7686" layer="37"/>
<rectangle x1="6.477" y1="2.7178" x2="6.5278" y2="2.7686" layer="37"/>
<rectangle x1="8.4582" y1="2.7178" x2="8.509" y2="2.7686" layer="37"/>
<rectangle x1="9.3218" y1="2.7178" x2="9.3726" y2="2.7686" layer="37"/>
<rectangle x1="6.8834" y1="2.7178" x2="6.9342" y2="2.7686" layer="37"/>
<rectangle x1="5.461" y1="2.7178" x2="5.5118" y2="2.7686" layer="37"/>
<rectangle x1="10.795" y1="2.7178" x2="10.8458" y2="2.7686" layer="37"/>
<rectangle x1="3.0734" y1="3.3782" x2="3.2766" y2="3.429" layer="37"/>
<rectangle x1="4.3434" y1="3.3782" x2="4.5466" y2="3.429" layer="37"/>
<rectangle x1="9.779" y1="3.3782" x2="9.9822" y2="3.429" layer="37"/>
<rectangle x1="10.9982" y1="3.3782" x2="11.2014" y2="3.429" layer="37"/>
<rectangle x1="9.9822" y1="3.3782" x2="10.033" y2="3.429" layer="37"/>
<rectangle x1="4.5466" y1="3.3782" x2="4.5974" y2="3.429" layer="37"/>
<rectangle x1="4.2926" y1="3.3782" x2="4.3434" y2="3.429" layer="37"/>
<rectangle x1="3.2766" y1="3.3782" x2="3.3274" y2="3.429" layer="37"/>
<rectangle x1="3.0226" y1="3.3782" x2="3.0734" y2="3.429" layer="37"/>
<rectangle x1="10.9474" y1="3.3782" x2="10.9982" y2="3.429" layer="37"/>
<rectangle x1="2.9718" y1="3.429" x2="3.3274" y2="3.4798" layer="37"/>
<rectangle x1="4.2926" y1="3.429" x2="4.6482" y2="3.4798" layer="37"/>
<rectangle x1="9.7282" y1="3.429" x2="10.0838" y2="3.4798" layer="37"/>
<rectangle x1="10.9474" y1="3.429" x2="11.2522" y2="3.4798" layer="37"/>
<rectangle x1="4.2418" y1="3.429" x2="4.2926" y2="3.4798" layer="37"/>
<rectangle x1="3.3274" y1="3.429" x2="3.3782" y2="3.4798" layer="37"/>
<rectangle x1="9.6774" y1="3.429" x2="9.7282" y2="3.4798" layer="37"/>
<rectangle x1="10.8966" y1="3.429" x2="10.9474" y2="3.4798" layer="37"/>
<rectangle x1="2.9718" y1="3.4798" x2="3.3782" y2="3.5306" layer="37"/>
<rectangle x1="4.2418" y1="3.4798" x2="4.6482" y2="3.5306" layer="37"/>
<rectangle x1="9.6774" y1="3.4798" x2="10.1346" y2="3.5306" layer="37"/>
<rectangle x1="10.8966" y1="3.4798" x2="11.2522" y2="3.5306" layer="37"/>
<rectangle x1="11.2522" y1="3.4798" x2="11.303" y2="3.5306" layer="37"/>
<rectangle x1="4.6482" y1="3.4798" x2="4.699" y2="3.5306" layer="37"/>
<rectangle x1="2.921" y1="3.4798" x2="2.9718" y2="3.5306" layer="37"/>
<rectangle x1="4.191" y1="3.4798" x2="4.2418" y2="3.5306" layer="37"/>
<rectangle x1="3.3782" y1="3.4798" x2="3.429" y2="3.5306" layer="37"/>
<rectangle x1="2.9718" y1="3.5306" x2="3.429" y2="3.5814" layer="37"/>
<rectangle x1="4.191" y1="3.5306" x2="4.699" y2="3.5814" layer="37"/>
<rectangle x1="9.6774" y1="3.5306" x2="10.1346" y2="3.5814" layer="37"/>
<rectangle x1="10.8966" y1="3.5306" x2="11.2522" y2="3.5814" layer="37"/>
<rectangle x1="2.921" y1="3.5306" x2="2.9718" y2="3.5814" layer="37"/>
<rectangle x1="11.2522" y1="3.5306" x2="11.303" y2="3.5814" layer="37"/>
<rectangle x1="9.6266" y1="3.5306" x2="9.6774" y2="3.5814" layer="37"/>
<rectangle x1="2.921" y1="3.5814" x2="3.429" y2="3.6322" layer="37"/>
<rectangle x1="4.191" y1="3.5814" x2="4.699" y2="3.6322" layer="37"/>
<rectangle x1="9.6266" y1="3.5814" x2="10.1346" y2="3.6322" layer="37"/>
<rectangle x1="10.8966" y1="3.5814" x2="11.2522" y2="3.6322" layer="37"/>
<rectangle x1="11.2522" y1="3.5814" x2="11.303" y2="3.6322" layer="37"/>
<rectangle x1="10.1346" y1="3.5814" x2="10.1854" y2="3.6322" layer="37"/>
<rectangle x1="2.921" y1="3.6322" x2="3.429" y2="3.683" layer="37"/>
<rectangle x1="4.191" y1="3.6322" x2="4.699" y2="3.683" layer="37"/>
<rectangle x1="9.6266" y1="3.6322" x2="10.1854" y2="3.683" layer="37"/>
<rectangle x1="10.8966" y1="3.6322" x2="11.303" y2="3.683" layer="37"/>
<rectangle x1="10.8458" y1="3.6322" x2="10.8966" y2="3.683" layer="37"/>
<rectangle x1="2.921" y1="3.683" x2="3.429" y2="3.7338" layer="37"/>
<rectangle x1="4.191" y1="3.683" x2="4.699" y2="3.7338" layer="37"/>
<rectangle x1="9.6266" y1="3.683" x2="10.1854" y2="3.7338" layer="37"/>
<rectangle x1="10.8966" y1="3.683" x2="11.303" y2="3.7338" layer="37"/>
<rectangle x1="10.8458" y1="3.683" x2="10.8966" y2="3.7338" layer="37"/>
<rectangle x1="2.921" y1="3.7338" x2="3.429" y2="3.7846" layer="37"/>
<rectangle x1="4.191" y1="3.7338" x2="4.699" y2="3.7846" layer="37"/>
<rectangle x1="9.6266" y1="3.7338" x2="10.1854" y2="3.7846" layer="37"/>
<rectangle x1="10.8966" y1="3.7338" x2="11.303" y2="3.7846" layer="37"/>
<rectangle x1="10.8458" y1="3.7338" x2="10.8966" y2="3.7846" layer="37"/>
<rectangle x1="2.921" y1="3.7846" x2="3.429" y2="3.8354" layer="37"/>
<rectangle x1="4.191" y1="3.7846" x2="4.699" y2="3.8354" layer="37"/>
<rectangle x1="9.6266" y1="3.7846" x2="10.1854" y2="3.8354" layer="37"/>
<rectangle x1="10.8966" y1="3.7846" x2="11.303" y2="3.8354" layer="37"/>
<rectangle x1="10.8458" y1="3.7846" x2="10.8966" y2="3.8354" layer="37"/>
<rectangle x1="2.921" y1="3.8354" x2="3.429" y2="3.8862" layer="37"/>
<rectangle x1="4.191" y1="3.8354" x2="4.699" y2="3.8862" layer="37"/>
<rectangle x1="9.6266" y1="3.8354" x2="10.1854" y2="3.8862" layer="37"/>
<rectangle x1="10.8966" y1="3.8354" x2="11.303" y2="3.8862" layer="37"/>
<rectangle x1="7.3406" y1="3.8354" x2="7.3914" y2="3.8862" layer="37"/>
<rectangle x1="7.3914" y1="3.8354" x2="7.4422" y2="3.8862" layer="37"/>
<rectangle x1="7.2898" y1="3.8354" x2="7.3406" y2="3.8862" layer="37"/>
<rectangle x1="10.8458" y1="3.8354" x2="10.8966" y2="3.8862" layer="37"/>
<rectangle x1="2.921" y1="3.8862" x2="3.429" y2="3.937" layer="37"/>
<rectangle x1="4.191" y1="3.8862" x2="4.699" y2="3.937" layer="37"/>
<rectangle x1="7.239" y1="3.8862" x2="7.493" y2="3.937" layer="37"/>
<rectangle x1="9.6266" y1="3.8862" x2="10.1854" y2="3.937" layer="37"/>
<rectangle x1="10.8966" y1="3.8862" x2="11.303" y2="3.937" layer="37"/>
<rectangle x1="7.1882" y1="3.8862" x2="7.239" y2="3.937" layer="37"/>
<rectangle x1="7.493" y1="3.8862" x2="7.5438" y2="3.937" layer="37"/>
<rectangle x1="10.8458" y1="3.8862" x2="10.8966" y2="3.937" layer="37"/>
<rectangle x1="2.921" y1="3.937" x2="3.429" y2="3.9878" layer="37"/>
<rectangle x1="4.191" y1="3.937" x2="4.699" y2="3.9878" layer="37"/>
<rectangle x1="7.1882" y1="3.937" x2="7.5438" y2="3.9878" layer="37"/>
<rectangle x1="9.6266" y1="3.937" x2="10.1854" y2="3.9878" layer="37"/>
<rectangle x1="10.8966" y1="3.937" x2="11.303" y2="3.9878" layer="37"/>
<rectangle x1="7.1374" y1="3.937" x2="7.1882" y2="3.9878" layer="37"/>
<rectangle x1="7.5438" y1="3.937" x2="7.5946" y2="3.9878" layer="37"/>
<rectangle x1="10.8458" y1="3.937" x2="10.8966" y2="3.9878" layer="37"/>
<rectangle x1="2.921" y1="3.9878" x2="3.429" y2="4.0386" layer="37"/>
<rectangle x1="4.191" y1="3.9878" x2="4.699" y2="4.0386" layer="37"/>
<rectangle x1="7.1374" y1="3.9878" x2="7.5946" y2="4.0386" layer="37"/>
<rectangle x1="9.6266" y1="3.9878" x2="10.1854" y2="4.0386" layer="37"/>
<rectangle x1="10.8966" y1="3.9878" x2="11.303" y2="4.0386" layer="37"/>
<rectangle x1="7.0866" y1="3.9878" x2="7.1374" y2="4.0386" layer="37"/>
<rectangle x1="7.5946" y1="3.9878" x2="7.6454" y2="4.0386" layer="37"/>
<rectangle x1="10.8458" y1="3.9878" x2="10.8966" y2="4.0386" layer="37"/>
<rectangle x1="2.921" y1="4.0386" x2="3.429" y2="4.0894" layer="37"/>
<rectangle x1="4.191" y1="4.0386" x2="4.699" y2="4.0894" layer="37"/>
<rectangle x1="7.0866" y1="4.0386" x2="7.6454" y2="4.0894" layer="37"/>
<rectangle x1="9.6266" y1="4.0386" x2="10.1854" y2="4.0894" layer="37"/>
<rectangle x1="10.8966" y1="4.0386" x2="11.303" y2="4.0894" layer="37"/>
<rectangle x1="7.0358" y1="4.0386" x2="7.0866" y2="4.0894" layer="37"/>
<rectangle x1="7.6454" y1="4.0386" x2="7.6962" y2="4.0894" layer="37"/>
<rectangle x1="10.8458" y1="4.0386" x2="10.8966" y2="4.0894" layer="37"/>
<rectangle x1="2.921" y1="4.0894" x2="3.429" y2="4.1402" layer="37"/>
<rectangle x1="4.191" y1="4.0894" x2="4.699" y2="4.1402" layer="37"/>
<rectangle x1="7.0358" y1="4.0894" x2="7.6962" y2="4.1402" layer="37"/>
<rectangle x1="9.6266" y1="4.0894" x2="10.1854" y2="4.1402" layer="37"/>
<rectangle x1="10.8966" y1="4.0894" x2="11.303" y2="4.1402" layer="37"/>
<rectangle x1="6.985" y1="4.0894" x2="7.0358" y2="4.1402" layer="37"/>
<rectangle x1="7.6962" y1="4.0894" x2="7.747" y2="4.1402" layer="37"/>
<rectangle x1="10.8458" y1="4.0894" x2="10.8966" y2="4.1402" layer="37"/>
<rectangle x1="2.921" y1="4.1402" x2="3.429" y2="4.191" layer="37"/>
<rectangle x1="4.191" y1="4.1402" x2="4.699" y2="4.191" layer="37"/>
<rectangle x1="6.985" y1="4.1402" x2="7.747" y2="4.191" layer="37"/>
<rectangle x1="9.6266" y1="4.1402" x2="10.1854" y2="4.191" layer="37"/>
<rectangle x1="10.8966" y1="4.1402" x2="11.303" y2="4.191" layer="37"/>
<rectangle x1="6.9342" y1="4.1402" x2="6.985" y2="4.191" layer="37"/>
<rectangle x1="7.747" y1="4.1402" x2="7.7978" y2="4.191" layer="37"/>
<rectangle x1="10.8458" y1="4.1402" x2="10.8966" y2="4.191" layer="37"/>
<rectangle x1="2.921" y1="4.191" x2="3.429" y2="4.2418" layer="37"/>
<rectangle x1="4.191" y1="4.191" x2="4.699" y2="4.2418" layer="37"/>
<rectangle x1="6.985" y1="4.191" x2="7.7978" y2="4.2418" layer="37"/>
<rectangle x1="9.6266" y1="4.191" x2="10.1854" y2="4.2418" layer="37"/>
<rectangle x1="10.8966" y1="4.191" x2="11.303" y2="4.2418" layer="37"/>
<rectangle x1="6.9342" y1="4.191" x2="6.985" y2="4.2418" layer="37"/>
<rectangle x1="6.8834" y1="4.191" x2="6.9342" y2="4.2418" layer="37"/>
<rectangle x1="7.7978" y1="4.191" x2="7.8486" y2="4.2418" layer="37"/>
<rectangle x1="10.8458" y1="4.191" x2="10.8966" y2="4.2418" layer="37"/>
<rectangle x1="2.921" y1="4.2418" x2="3.429" y2="4.2926" layer="37"/>
<rectangle x1="4.191" y1="4.2418" x2="4.699" y2="4.2926" layer="37"/>
<rectangle x1="6.8834" y1="4.2418" x2="7.8486" y2="4.2926" layer="37"/>
<rectangle x1="9.6266" y1="4.2418" x2="10.1854" y2="4.2926" layer="37"/>
<rectangle x1="10.8966" y1="4.2418" x2="11.303" y2="4.2926" layer="37"/>
<rectangle x1="6.8326" y1="4.2418" x2="6.8834" y2="4.2926" layer="37"/>
<rectangle x1="10.8458" y1="4.2418" x2="10.8966" y2="4.2926" layer="37"/>
<rectangle x1="7.8486" y1="4.2418" x2="7.8994" y2="4.2926" layer="37"/>
<rectangle x1="2.921" y1="4.2926" x2="3.429" y2="4.3434" layer="37"/>
<rectangle x1="4.191" y1="4.2926" x2="4.699" y2="4.3434" layer="37"/>
<rectangle x1="6.8326" y1="4.2926" x2="7.8994" y2="4.3434" layer="37"/>
<rectangle x1="9.6266" y1="4.2926" x2="10.1854" y2="4.3434" layer="37"/>
<rectangle x1="10.8966" y1="4.2926" x2="11.303" y2="4.3434" layer="37"/>
<rectangle x1="10.8458" y1="4.2926" x2="10.8966" y2="4.3434" layer="37"/>
<rectangle x1="2.921" y1="4.3434" x2="3.429" y2="4.3942" layer="37"/>
<rectangle x1="4.191" y1="4.3434" x2="4.699" y2="4.3942" layer="37"/>
<rectangle x1="6.7818" y1="4.3434" x2="7.9502" y2="4.3942" layer="37"/>
<rectangle x1="9.6266" y1="4.3434" x2="10.1854" y2="4.3942" layer="37"/>
<rectangle x1="10.8966" y1="4.3434" x2="11.303" y2="4.3942" layer="37"/>
<rectangle x1="10.8458" y1="4.3434" x2="10.8966" y2="4.3942" layer="37"/>
<rectangle x1="2.921" y1="4.3942" x2="3.429" y2="4.445" layer="37"/>
<rectangle x1="4.191" y1="4.3942" x2="4.699" y2="4.445" layer="37"/>
<rectangle x1="6.731" y1="4.3942" x2="8.001" y2="4.445" layer="37"/>
<rectangle x1="9.6266" y1="4.3942" x2="10.1854" y2="4.445" layer="37"/>
<rectangle x1="10.8966" y1="4.3942" x2="11.303" y2="4.445" layer="37"/>
<rectangle x1="10.8458" y1="4.3942" x2="10.8966" y2="4.445" layer="37"/>
<rectangle x1="2.921" y1="4.445" x2="3.429" y2="4.4958" layer="37"/>
<rectangle x1="4.191" y1="4.445" x2="4.699" y2="4.4958" layer="37"/>
<rectangle x1="6.6802" y1="4.445" x2="8.0518" y2="4.4958" layer="37"/>
<rectangle x1="9.6266" y1="4.445" x2="10.1854" y2="4.4958" layer="37"/>
<rectangle x1="10.8966" y1="4.445" x2="11.303" y2="4.4958" layer="37"/>
<rectangle x1="10.8458" y1="4.445" x2="10.8966" y2="4.4958" layer="37"/>
<rectangle x1="2.921" y1="4.4958" x2="3.429" y2="4.5466" layer="37"/>
<rectangle x1="4.191" y1="4.4958" x2="4.699" y2="4.5466" layer="37"/>
<rectangle x1="6.6294" y1="4.4958" x2="7.3406" y2="4.5466" layer="37"/>
<rectangle x1="7.3914" y1="4.4958" x2="8.1026" y2="4.5466" layer="37"/>
<rectangle x1="9.6266" y1="4.4958" x2="10.1854" y2="4.5466" layer="37"/>
<rectangle x1="10.8966" y1="4.4958" x2="11.303" y2="4.5466" layer="37"/>
<rectangle x1="7.3406" y1="4.4958" x2="7.3914" y2="4.5466" layer="37"/>
<rectangle x1="10.8458" y1="4.4958" x2="10.8966" y2="4.5466" layer="37"/>
<rectangle x1="2.921" y1="4.5466" x2="3.429" y2="4.5974" layer="37"/>
<rectangle x1="4.191" y1="4.5466" x2="4.699" y2="4.5974" layer="37"/>
<rectangle x1="6.6294" y1="4.5466" x2="7.2898" y2="4.5974" layer="37"/>
<rectangle x1="7.4422" y1="4.5466" x2="8.1534" y2="4.5974" layer="37"/>
<rectangle x1="9.6266" y1="4.5466" x2="10.1854" y2="4.5974" layer="37"/>
<rectangle x1="10.8966" y1="4.5466" x2="11.303" y2="4.5974" layer="37"/>
<rectangle x1="6.5786" y1="4.5466" x2="6.6294" y2="4.5974" layer="37"/>
<rectangle x1="7.3914" y1="4.5466" x2="7.4422" y2="4.5974" layer="37"/>
<rectangle x1="7.2898" y1="4.5466" x2="7.3406" y2="4.5974" layer="37"/>
<rectangle x1="10.8458" y1="4.5466" x2="10.8966" y2="4.5974" layer="37"/>
<rectangle x1="2.921" y1="4.5974" x2="3.429" y2="4.6482" layer="37"/>
<rectangle x1="4.191" y1="4.5974" x2="4.699" y2="4.6482" layer="37"/>
<rectangle x1="6.5786" y1="4.5974" x2="7.239" y2="4.6482" layer="37"/>
<rectangle x1="7.493" y1="4.5974" x2="8.1534" y2="4.6482" layer="37"/>
<rectangle x1="9.6266" y1="4.5974" x2="10.1854" y2="4.6482" layer="37"/>
<rectangle x1="10.8966" y1="4.5974" x2="11.303" y2="4.6482" layer="37"/>
<rectangle x1="6.5278" y1="4.5974" x2="6.5786" y2="4.6482" layer="37"/>
<rectangle x1="8.1534" y1="4.5974" x2="8.2042" y2="4.6482" layer="37"/>
<rectangle x1="7.4422" y1="4.5974" x2="7.493" y2="4.6482" layer="37"/>
<rectangle x1="7.239" y1="4.5974" x2="7.2898" y2="4.6482" layer="37"/>
<rectangle x1="10.8458" y1="4.5974" x2="10.8966" y2="4.6482" layer="37"/>
<rectangle x1="2.921" y1="4.6482" x2="3.429" y2="4.699" layer="37"/>
<rectangle x1="4.191" y1="4.6482" x2="4.699" y2="4.699" layer="37"/>
<rectangle x1="6.5278" y1="4.6482" x2="7.1882" y2="4.699" layer="37"/>
<rectangle x1="7.5438" y1="4.6482" x2="8.2042" y2="4.699" layer="37"/>
<rectangle x1="9.6266" y1="4.6482" x2="10.1854" y2="4.699" layer="37"/>
<rectangle x1="10.8966" y1="4.6482" x2="11.303" y2="4.699" layer="37"/>
<rectangle x1="6.477" y1="4.6482" x2="6.5278" y2="4.699" layer="37"/>
<rectangle x1="8.2042" y1="4.6482" x2="8.255" y2="4.699" layer="37"/>
<rectangle x1="7.493" y1="4.6482" x2="7.5438" y2="4.699" layer="37"/>
<rectangle x1="7.1882" y1="4.6482" x2="7.239" y2="4.699" layer="37"/>
<rectangle x1="10.8458" y1="4.6482" x2="10.8966" y2="4.699" layer="37"/>
<rectangle x1="2.921" y1="4.699" x2="3.429" y2="4.7498" layer="37"/>
<rectangle x1="4.191" y1="4.699" x2="4.699" y2="4.7498" layer="37"/>
<rectangle x1="6.477" y1="4.699" x2="7.1374" y2="4.7498" layer="37"/>
<rectangle x1="7.5946" y1="4.699" x2="8.255" y2="4.7498" layer="37"/>
<rectangle x1="9.6266" y1="4.699" x2="10.1854" y2="4.7498" layer="37"/>
<rectangle x1="10.8966" y1="4.699" x2="11.303" y2="4.7498" layer="37"/>
<rectangle x1="6.4262" y1="4.699" x2="6.477" y2="4.7498" layer="37"/>
<rectangle x1="7.5438" y1="4.699" x2="7.5946" y2="4.7498" layer="37"/>
<rectangle x1="7.1374" y1="4.699" x2="7.1882" y2="4.7498" layer="37"/>
<rectangle x1="8.255" y1="4.699" x2="8.3058" y2="4.7498" layer="37"/>
<rectangle x1="10.8458" y1="4.699" x2="10.8966" y2="4.7498" layer="37"/>
<rectangle x1="2.921" y1="4.7498" x2="3.429" y2="4.8006" layer="37"/>
<rectangle x1="4.191" y1="4.7498" x2="4.699" y2="4.8006" layer="37"/>
<rectangle x1="6.4262" y1="4.7498" x2="7.0866" y2="4.8006" layer="37"/>
<rectangle x1="7.6454" y1="4.7498" x2="8.3058" y2="4.8006" layer="37"/>
<rectangle x1="9.6266" y1="4.7498" x2="10.1854" y2="4.8006" layer="37"/>
<rectangle x1="10.8966" y1="4.7498" x2="11.303" y2="4.8006" layer="37"/>
<rectangle x1="7.0866" y1="4.7498" x2="7.1374" y2="4.8006" layer="37"/>
<rectangle x1="7.5946" y1="4.7498" x2="7.6454" y2="4.8006" layer="37"/>
<rectangle x1="6.3754" y1="4.7498" x2="6.4262" y2="4.8006" layer="37"/>
<rectangle x1="8.3058" y1="4.7498" x2="8.3566" y2="4.8006" layer="37"/>
<rectangle x1="10.8458" y1="4.7498" x2="10.8966" y2="4.8006" layer="37"/>
<rectangle x1="2.921" y1="4.8006" x2="3.429" y2="4.8514" layer="37"/>
<rectangle x1="4.191" y1="4.8006" x2="4.699" y2="4.8514" layer="37"/>
<rectangle x1="6.3754" y1="4.8006" x2="7.0358" y2="4.8514" layer="37"/>
<rectangle x1="7.6962" y1="4.8006" x2="8.3566" y2="4.8514" layer="37"/>
<rectangle x1="9.6266" y1="4.8006" x2="10.1854" y2="4.8514" layer="37"/>
<rectangle x1="10.8966" y1="4.8006" x2="11.303" y2="4.8514" layer="37"/>
<rectangle x1="7.0358" y1="4.8006" x2="7.0866" y2="4.8514" layer="37"/>
<rectangle x1="7.6454" y1="4.8006" x2="7.6962" y2="4.8514" layer="37"/>
<rectangle x1="6.3246" y1="4.8006" x2="6.3754" y2="4.8514" layer="37"/>
<rectangle x1="10.8458" y1="4.8006" x2="10.8966" y2="4.8514" layer="37"/>
<rectangle x1="8.3566" y1="4.8006" x2="8.4074" y2="4.8514" layer="37"/>
<rectangle x1="2.921" y1="4.8514" x2="3.429" y2="4.9022" layer="37"/>
<rectangle x1="4.191" y1="4.8514" x2="4.699" y2="4.9022" layer="37"/>
<rectangle x1="6.3246" y1="4.8514" x2="7.0358" y2="4.9022" layer="37"/>
<rectangle x1="7.747" y1="4.8514" x2="8.4074" y2="4.9022" layer="37"/>
<rectangle x1="9.6266" y1="4.8514" x2="10.1854" y2="4.9022" layer="37"/>
<rectangle x1="10.8966" y1="4.8514" x2="11.303" y2="4.9022" layer="37"/>
<rectangle x1="7.6962" y1="4.8514" x2="7.747" y2="4.9022" layer="37"/>
<rectangle x1="6.2738" y1="4.8514" x2="6.3246" y2="4.9022" layer="37"/>
<rectangle x1="10.8458" y1="4.8514" x2="10.8966" y2="4.9022" layer="37"/>
<rectangle x1="2.921" y1="4.9022" x2="3.429" y2="4.953" layer="37"/>
<rectangle x1="4.191" y1="4.9022" x2="4.699" y2="4.953" layer="37"/>
<rectangle x1="6.2738" y1="4.9022" x2="6.985" y2="4.953" layer="37"/>
<rectangle x1="7.747" y1="4.9022" x2="8.4074" y2="4.953" layer="37"/>
<rectangle x1="9.6266" y1="4.9022" x2="10.1854" y2="4.953" layer="37"/>
<rectangle x1="10.8966" y1="4.9022" x2="11.303" y2="4.953" layer="37"/>
<rectangle x1="8.4074" y1="4.9022" x2="8.4582" y2="4.953" layer="37"/>
<rectangle x1="6.223" y1="4.9022" x2="6.2738" y2="4.953" layer="37"/>
<rectangle x1="10.8458" y1="4.9022" x2="10.8966" y2="4.953" layer="37"/>
<rectangle x1="2.921" y1="4.953" x2="3.429" y2="5.0038" layer="37"/>
<rectangle x1="4.191" y1="4.953" x2="4.699" y2="5.0038" layer="37"/>
<rectangle x1="6.223" y1="4.953" x2="6.9342" y2="5.0038" layer="37"/>
<rectangle x1="7.7978" y1="4.953" x2="8.4582" y2="5.0038" layer="37"/>
<rectangle x1="9.6266" y1="4.953" x2="10.1854" y2="5.0038" layer="37"/>
<rectangle x1="10.8966" y1="4.953" x2="11.303" y2="5.0038" layer="37"/>
<rectangle x1="8.4582" y1="4.953" x2="8.509" y2="5.0038" layer="37"/>
<rectangle x1="6.1722" y1="4.953" x2="6.223" y2="5.0038" layer="37"/>
<rectangle x1="10.8458" y1="4.953" x2="10.8966" y2="5.0038" layer="37"/>
<rectangle x1="2.921" y1="5.0038" x2="3.429" y2="5.0546" layer="37"/>
<rectangle x1="4.191" y1="5.0038" x2="4.699" y2="5.0546" layer="37"/>
<rectangle x1="6.1722" y1="5.0038" x2="6.8834" y2="5.0546" layer="37"/>
<rectangle x1="7.8486" y1="5.0038" x2="8.509" y2="5.0546" layer="37"/>
<rectangle x1="9.6266" y1="5.0038" x2="10.1854" y2="5.0546" layer="37"/>
<rectangle x1="10.8966" y1="5.0038" x2="11.303" y2="5.0546" layer="37"/>
<rectangle x1="8.509" y1="5.0038" x2="8.5598" y2="5.0546" layer="37"/>
<rectangle x1="6.1214" y1="5.0038" x2="6.1722" y2="5.0546" layer="37"/>
<rectangle x1="10.8458" y1="5.0038" x2="10.8966" y2="5.0546" layer="37"/>
<rectangle x1="2.921" y1="5.0546" x2="3.429" y2="5.1054" layer="37"/>
<rectangle x1="4.191" y1="5.0546" x2="4.699" y2="5.1054" layer="37"/>
<rectangle x1="6.1214" y1="5.0546" x2="6.8326" y2="5.1054" layer="37"/>
<rectangle x1="7.8994" y1="5.0546" x2="8.5598" y2="5.1054" layer="37"/>
<rectangle x1="9.6266" y1="5.0546" x2="10.1854" y2="5.1054" layer="37"/>
<rectangle x1="10.8966" y1="5.0546" x2="11.303" y2="5.1054" layer="37"/>
<rectangle x1="8.5598" y1="5.0546" x2="8.6106" y2="5.1054" layer="37"/>
<rectangle x1="6.0706" y1="5.0546" x2="6.1214" y2="5.1054" layer="37"/>
<rectangle x1="10.8458" y1="5.0546" x2="10.8966" y2="5.1054" layer="37"/>
<rectangle x1="2.921" y1="5.1054" x2="3.429" y2="5.1562" layer="37"/>
<rectangle x1="4.191" y1="5.1054" x2="4.699" y2="5.1562" layer="37"/>
<rectangle x1="6.0706" y1="5.1054" x2="6.7818" y2="5.1562" layer="37"/>
<rectangle x1="7.9502" y1="5.1054" x2="8.6106" y2="5.1562" layer="37"/>
<rectangle x1="9.6266" y1="5.1054" x2="10.1854" y2="5.1562" layer="37"/>
<rectangle x1="10.8966" y1="5.1054" x2="11.303" y2="5.1562" layer="37"/>
<rectangle x1="8.6106" y1="5.1054" x2="8.6614" y2="5.1562" layer="37"/>
<rectangle x1="7.8994" y1="5.1054" x2="7.9502" y2="5.1562" layer="37"/>
<rectangle x1="10.8458" y1="5.1054" x2="10.8966" y2="5.1562" layer="37"/>
<rectangle x1="2.921" y1="5.1562" x2="3.429" y2="5.207" layer="37"/>
<rectangle x1="4.191" y1="5.1562" x2="4.699" y2="5.207" layer="37"/>
<rectangle x1="6.0706" y1="5.1562" x2="6.731" y2="5.207" layer="37"/>
<rectangle x1="8.001" y1="5.1562" x2="8.6614" y2="5.207" layer="37"/>
<rectangle x1="9.6266" y1="5.1562" x2="10.1854" y2="5.207" layer="37"/>
<rectangle x1="10.8966" y1="5.1562" x2="11.303" y2="5.207" layer="37"/>
<rectangle x1="6.0198" y1="5.1562" x2="6.0706" y2="5.207" layer="37"/>
<rectangle x1="8.6614" y1="5.1562" x2="8.7122" y2="5.207" layer="37"/>
<rectangle x1="7.9502" y1="5.1562" x2="8.001" y2="5.207" layer="37"/>
<rectangle x1="10.8458" y1="5.1562" x2="10.8966" y2="5.207" layer="37"/>
<rectangle x1="2.921" y1="5.207" x2="3.429" y2="5.2578" layer="37"/>
<rectangle x1="4.191" y1="5.207" x2="4.699" y2="5.2578" layer="37"/>
<rectangle x1="6.0198" y1="5.207" x2="6.6802" y2="5.2578" layer="37"/>
<rectangle x1="8.0518" y1="5.207" x2="8.7122" y2="5.2578" layer="37"/>
<rectangle x1="9.6266" y1="5.207" x2="10.1854" y2="5.2578" layer="37"/>
<rectangle x1="10.8966" y1="5.207" x2="11.303" y2="5.2578" layer="37"/>
<rectangle x1="5.969" y1="5.207" x2="6.0198" y2="5.2578" layer="37"/>
<rectangle x1="8.7122" y1="5.207" x2="8.763" y2="5.2578" layer="37"/>
<rectangle x1="8.001" y1="5.207" x2="8.0518" y2="5.2578" layer="37"/>
<rectangle x1="10.8458" y1="5.207" x2="10.8966" y2="5.2578" layer="37"/>
<rectangle x1="2.921" y1="5.2578" x2="3.429" y2="5.3086" layer="37"/>
<rectangle x1="4.191" y1="5.2578" x2="4.699" y2="5.3086" layer="37"/>
<rectangle x1="5.969" y1="5.2578" x2="6.6294" y2="5.3086" layer="37"/>
<rectangle x1="8.1026" y1="5.2578" x2="8.763" y2="5.3086" layer="37"/>
<rectangle x1="9.6266" y1="5.2578" x2="10.1854" y2="5.3086" layer="37"/>
<rectangle x1="10.8966" y1="5.2578" x2="11.303" y2="5.3086" layer="37"/>
<rectangle x1="5.9182" y1="5.2578" x2="5.969" y2="5.3086" layer="37"/>
<rectangle x1="8.0518" y1="5.2578" x2="8.1026" y2="5.3086" layer="37"/>
<rectangle x1="8.763" y1="5.2578" x2="8.8138" y2="5.3086" layer="37"/>
<rectangle x1="6.6294" y1="5.2578" x2="6.6802" y2="5.3086" layer="37"/>
<rectangle x1="10.8458" y1="5.2578" x2="10.8966" y2="5.3086" layer="37"/>
<rectangle x1="2.921" y1="5.3086" x2="3.429" y2="5.3594" layer="37"/>
<rectangle x1="4.191" y1="5.3086" x2="4.699" y2="5.3594" layer="37"/>
<rectangle x1="5.9182" y1="5.3086" x2="6.5786" y2="5.3594" layer="37"/>
<rectangle x1="8.1534" y1="5.3086" x2="8.8138" y2="5.3594" layer="37"/>
<rectangle x1="9.6266" y1="5.3086" x2="10.1854" y2="5.3594" layer="37"/>
<rectangle x1="10.8966" y1="5.3086" x2="11.303" y2="5.3594" layer="37"/>
<rectangle x1="5.8674" y1="5.3086" x2="5.9182" y2="5.3594" layer="37"/>
<rectangle x1="8.1026" y1="5.3086" x2="8.1534" y2="5.3594" layer="37"/>
<rectangle x1="8.8138" y1="5.3086" x2="8.8646" y2="5.3594" layer="37"/>
<rectangle x1="6.5786" y1="5.3086" x2="6.6294" y2="5.3594" layer="37"/>
<rectangle x1="10.8458" y1="5.3086" x2="10.8966" y2="5.3594" layer="37"/>
<rectangle x1="2.921" y1="5.3594" x2="3.429" y2="5.4102" layer="37"/>
<rectangle x1="4.191" y1="5.3594" x2="4.699" y2="5.4102" layer="37"/>
<rectangle x1="5.8674" y1="5.3594" x2="6.5278" y2="5.4102" layer="37"/>
<rectangle x1="8.2042" y1="5.3594" x2="8.8646" y2="5.4102" layer="37"/>
<rectangle x1="9.6266" y1="5.3594" x2="10.1854" y2="5.4102" layer="37"/>
<rectangle x1="10.8966" y1="5.3594" x2="11.303" y2="5.4102" layer="37"/>
<rectangle x1="5.8166" y1="5.3594" x2="5.8674" y2="5.4102" layer="37"/>
<rectangle x1="8.1534" y1="5.3594" x2="8.2042" y2="5.4102" layer="37"/>
<rectangle x1="8.8646" y1="5.3594" x2="8.9154" y2="5.4102" layer="37"/>
<rectangle x1="6.5278" y1="5.3594" x2="6.5786" y2="5.4102" layer="37"/>
<rectangle x1="10.8458" y1="5.3594" x2="10.8966" y2="5.4102" layer="37"/>
<rectangle x1="2.921" y1="5.4102" x2="3.429" y2="5.461" layer="37"/>
<rectangle x1="4.191" y1="5.4102" x2="4.699" y2="5.461" layer="37"/>
<rectangle x1="5.8166" y1="5.4102" x2="6.477" y2="5.461" layer="37"/>
<rectangle x1="8.255" y1="5.4102" x2="8.9154" y2="5.461" layer="37"/>
<rectangle x1="9.6266" y1="5.4102" x2="10.1854" y2="5.461" layer="37"/>
<rectangle x1="10.8966" y1="5.4102" x2="11.303" y2="5.461" layer="37"/>
<rectangle x1="8.2042" y1="5.4102" x2="8.255" y2="5.461" layer="37"/>
<rectangle x1="5.7658" y1="5.4102" x2="5.8166" y2="5.461" layer="37"/>
<rectangle x1="6.477" y1="5.4102" x2="6.5278" y2="5.461" layer="37"/>
<rectangle x1="8.9154" y1="5.4102" x2="8.9662" y2="5.461" layer="37"/>
<rectangle x1="10.8458" y1="5.4102" x2="10.8966" y2="5.461" layer="37"/>
<rectangle x1="2.921" y1="5.461" x2="3.429" y2="5.5118" layer="37"/>
<rectangle x1="4.191" y1="5.461" x2="4.699" y2="5.5118" layer="37"/>
<rectangle x1="5.7658" y1="5.461" x2="6.4262" y2="5.5118" layer="37"/>
<rectangle x1="8.255" y1="5.461" x2="8.9662" y2="5.5118" layer="37"/>
<rectangle x1="9.6266" y1="5.461" x2="10.1854" y2="5.5118" layer="37"/>
<rectangle x1="10.8966" y1="5.461" x2="11.303" y2="5.5118" layer="37"/>
<rectangle x1="5.715" y1="5.461" x2="5.7658" y2="5.5118" layer="37"/>
<rectangle x1="6.4262" y1="5.461" x2="6.477" y2="5.5118" layer="37"/>
<rectangle x1="10.8458" y1="5.461" x2="10.8966" y2="5.5118" layer="37"/>
<rectangle x1="2.921" y1="5.5118" x2="3.429" y2="5.5626" layer="37"/>
<rectangle x1="4.191" y1="5.5118" x2="4.699" y2="5.5626" layer="37"/>
<rectangle x1="5.715" y1="5.5118" x2="6.3754" y2="5.5626" layer="37"/>
<rectangle x1="8.3058" y1="5.5118" x2="9.017" y2="5.5626" layer="37"/>
<rectangle x1="9.6266" y1="5.5118" x2="10.1854" y2="5.5626" layer="37"/>
<rectangle x1="10.8966" y1="5.5118" x2="11.303" y2="5.5626" layer="37"/>
<rectangle x1="5.6642" y1="5.5118" x2="5.715" y2="5.5626" layer="37"/>
<rectangle x1="6.3754" y1="5.5118" x2="6.4262" y2="5.5626" layer="37"/>
<rectangle x1="10.8458" y1="5.5118" x2="10.8966" y2="5.5626" layer="37"/>
<rectangle x1="2.921" y1="5.5626" x2="3.429" y2="5.6134" layer="37"/>
<rectangle x1="4.191" y1="5.5626" x2="4.699" y2="5.6134" layer="37"/>
<rectangle x1="5.6642" y1="5.5626" x2="6.3246" y2="5.6134" layer="37"/>
<rectangle x1="8.3566" y1="5.5626" x2="9.0678" y2="5.6134" layer="37"/>
<rectangle x1="9.6266" y1="5.5626" x2="10.1854" y2="5.6134" layer="37"/>
<rectangle x1="10.8966" y1="5.5626" x2="11.303" y2="5.6134" layer="37"/>
<rectangle x1="6.3246" y1="5.5626" x2="6.3754" y2="5.6134" layer="37"/>
<rectangle x1="5.6134" y1="5.5626" x2="5.6642" y2="5.6134" layer="37"/>
<rectangle x1="10.8458" y1="5.5626" x2="10.8966" y2="5.6134" layer="37"/>
<rectangle x1="2.921" y1="5.6134" x2="3.429" y2="5.6642" layer="37"/>
<rectangle x1="4.191" y1="5.6134" x2="4.699" y2="5.6642" layer="37"/>
<rectangle x1="5.6134" y1="5.6134" x2="6.2738" y2="5.6642" layer="37"/>
<rectangle x1="8.4074" y1="5.6134" x2="9.1186" y2="5.6642" layer="37"/>
<rectangle x1="9.6266" y1="5.6134" x2="10.1854" y2="5.6642" layer="37"/>
<rectangle x1="10.8966" y1="5.6134" x2="11.303" y2="5.6642" layer="37"/>
<rectangle x1="6.2738" y1="5.6134" x2="6.3246" y2="5.6642" layer="37"/>
<rectangle x1="5.5626" y1="5.6134" x2="5.6134" y2="5.6642" layer="37"/>
<rectangle x1="10.8458" y1="5.6134" x2="10.8966" y2="5.6642" layer="37"/>
<rectangle x1="2.921" y1="5.6642" x2="3.429" y2="5.715" layer="37"/>
<rectangle x1="4.191" y1="5.6642" x2="4.699" y2="5.715" layer="37"/>
<rectangle x1="5.5626" y1="5.6642" x2="6.223" y2="5.715" layer="37"/>
<rectangle x1="8.4582" y1="5.6642" x2="9.1694" y2="5.715" layer="37"/>
<rectangle x1="9.6266" y1="5.6642" x2="10.1854" y2="5.715" layer="37"/>
<rectangle x1="10.8966" y1="5.6642" x2="11.303" y2="5.715" layer="37"/>
<rectangle x1="6.223" y1="5.6642" x2="6.2738" y2="5.715" layer="37"/>
<rectangle x1="5.5118" y1="5.6642" x2="5.5626" y2="5.715" layer="37"/>
<rectangle x1="10.8458" y1="5.6642" x2="10.8966" y2="5.715" layer="37"/>
<rectangle x1="8.4074" y1="5.6642" x2="8.4582" y2="5.715" layer="37"/>
<rectangle x1="2.921" y1="5.715" x2="3.429" y2="5.7658" layer="37"/>
<rectangle x1="4.191" y1="5.715" x2="4.699" y2="5.7658" layer="37"/>
<rectangle x1="5.5118" y1="5.715" x2="6.1722" y2="5.7658" layer="37"/>
<rectangle x1="8.509" y1="5.715" x2="9.1694" y2="5.7658" layer="37"/>
<rectangle x1="9.6266" y1="5.715" x2="10.1854" y2="5.7658" layer="37"/>
<rectangle x1="10.8966" y1="5.715" x2="11.303" y2="5.7658" layer="37"/>
<rectangle x1="9.1694" y1="5.715" x2="9.2202" y2="5.7658" layer="37"/>
<rectangle x1="6.1722" y1="5.715" x2="6.223" y2="5.7658" layer="37"/>
<rectangle x1="8.4582" y1="5.715" x2="8.509" y2="5.7658" layer="37"/>
<rectangle x1="5.461" y1="5.715" x2="5.5118" y2="5.7658" layer="37"/>
<rectangle x1="10.8458" y1="5.715" x2="10.8966" y2="5.7658" layer="37"/>
<rectangle x1="2.921" y1="5.7658" x2="3.429" y2="5.8166" layer="37"/>
<rectangle x1="4.191" y1="5.7658" x2="4.699" y2="5.8166" layer="37"/>
<rectangle x1="5.461" y1="5.7658" x2="6.1214" y2="5.8166" layer="37"/>
<rectangle x1="7.4422" y1="5.7658" x2="7.6454" y2="5.8166" layer="37"/>
<rectangle x1="8.5598" y1="5.7658" x2="9.2202" y2="5.8166" layer="37"/>
<rectangle x1="9.6266" y1="5.7658" x2="10.1854" y2="5.8166" layer="37"/>
<rectangle x1="10.8966" y1="5.7658" x2="11.303" y2="5.8166" layer="37"/>
<rectangle x1="6.1214" y1="5.7658" x2="6.1722" y2="5.8166" layer="37"/>
<rectangle x1="7.6454" y1="5.7658" x2="7.6962" y2="5.8166" layer="37"/>
<rectangle x1="9.2202" y1="5.7658" x2="9.271" y2="5.8166" layer="37"/>
<rectangle x1="7.3914" y1="5.7658" x2="7.4422" y2="5.8166" layer="37"/>
<rectangle x1="8.509" y1="5.7658" x2="8.5598" y2="5.8166" layer="37"/>
<rectangle x1="10.8458" y1="5.7658" x2="10.8966" y2="5.8166" layer="37"/>
<rectangle x1="5.4102" y1="5.7658" x2="5.461" y2="5.8166" layer="37"/>
<rectangle x1="2.921" y1="5.8166" x2="3.429" y2="5.8674" layer="37"/>
<rectangle x1="4.191" y1="5.8166" x2="4.699" y2="5.8674" layer="37"/>
<rectangle x1="5.4102" y1="5.8166" x2="6.0706" y2="5.8674" layer="37"/>
<rectangle x1="7.3914" y1="5.8166" x2="7.747" y2="5.8674" layer="37"/>
<rectangle x1="8.6106" y1="5.8166" x2="9.271" y2="5.8674" layer="37"/>
<rectangle x1="9.6266" y1="5.8166" x2="10.1854" y2="5.8674" layer="37"/>
<rectangle x1="10.8966" y1="5.8166" x2="11.303" y2="5.8674" layer="37"/>
<rectangle x1="6.0706" y1="5.8166" x2="6.1214" y2="5.8674" layer="37"/>
<rectangle x1="7.3406" y1="5.8166" x2="7.3914" y2="5.8674" layer="37"/>
<rectangle x1="9.271" y1="5.8166" x2="9.3218" y2="5.8674" layer="37"/>
<rectangle x1="8.5598" y1="5.8166" x2="8.6106" y2="5.8674" layer="37"/>
<rectangle x1="10.8458" y1="5.8166" x2="10.8966" y2="5.8674" layer="37"/>
<rectangle x1="2.921" y1="5.8674" x2="3.429" y2="5.9182" layer="37"/>
<rectangle x1="4.191" y1="5.8674" x2="4.699" y2="5.9182" layer="37"/>
<rectangle x1="5.3594" y1="5.8674" x2="6.0198" y2="5.9182" layer="37"/>
<rectangle x1="7.3406" y1="5.8674" x2="7.7978" y2="5.9182" layer="37"/>
<rectangle x1="8.6614" y1="5.8674" x2="9.3218" y2="5.9182" layer="37"/>
<rectangle x1="9.6266" y1="5.8674" x2="10.1854" y2="5.9182" layer="37"/>
<rectangle x1="10.8966" y1="5.8674" x2="11.303" y2="5.9182" layer="37"/>
<rectangle x1="6.0198" y1="5.8674" x2="6.0706" y2="5.9182" layer="37"/>
<rectangle x1="8.6106" y1="5.8674" x2="8.6614" y2="5.9182" layer="37"/>
<rectangle x1="7.2898" y1="5.8674" x2="7.3406" y2="5.9182" layer="37"/>
<rectangle x1="9.3218" y1="5.8674" x2="9.3726" y2="5.9182" layer="37"/>
<rectangle x1="10.8458" y1="5.8674" x2="10.8966" y2="5.9182" layer="37"/>
<rectangle x1="2.921" y1="5.9182" x2="3.429" y2="5.969" layer="37"/>
<rectangle x1="4.191" y1="5.9182" x2="4.699" y2="5.969" layer="37"/>
<rectangle x1="5.3594" y1="5.9182" x2="6.0198" y2="5.969" layer="37"/>
<rectangle x1="7.2898" y1="5.9182" x2="7.7978" y2="5.969" layer="37"/>
<rectangle x1="8.7122" y1="5.9182" x2="9.3218" y2="5.969" layer="37"/>
<rectangle x1="9.6266" y1="5.9182" x2="10.1854" y2="5.969" layer="37"/>
<rectangle x1="10.8966" y1="5.9182" x2="11.303" y2="5.969" layer="37"/>
<rectangle x1="9.3218" y1="5.9182" x2="9.3726" y2="5.969" layer="37"/>
<rectangle x1="8.6614" y1="5.9182" x2="8.7122" y2="5.969" layer="37"/>
<rectangle x1="5.3086" y1="5.9182" x2="5.3594" y2="5.969" layer="37"/>
<rectangle x1="7.239" y1="5.9182" x2="7.2898" y2="5.969" layer="37"/>
<rectangle x1="10.8458" y1="5.9182" x2="10.8966" y2="5.969" layer="37"/>
<rectangle x1="2.921" y1="5.969" x2="3.429" y2="6.0198" layer="37"/>
<rectangle x1="4.191" y1="5.969" x2="4.699" y2="6.0198" layer="37"/>
<rectangle x1="5.3086" y1="5.969" x2="5.969" y2="6.0198" layer="37"/>
<rectangle x1="7.239" y1="5.969" x2="7.7978" y2="6.0198" layer="37"/>
<rectangle x1="8.7122" y1="5.969" x2="9.3726" y2="6.0198" layer="37"/>
<rectangle x1="9.6266" y1="5.969" x2="10.1854" y2="6.0198" layer="37"/>
<rectangle x1="10.8966" y1="5.969" x2="11.303" y2="6.0198" layer="37"/>
<rectangle x1="7.7978" y1="5.969" x2="7.8486" y2="6.0198" layer="37"/>
<rectangle x1="5.969" y1="5.969" x2="6.0198" y2="6.0198" layer="37"/>
<rectangle x1="10.8458" y1="5.969" x2="10.8966" y2="6.0198" layer="37"/>
<rectangle x1="2.921" y1="6.0198" x2="3.429" y2="6.0706" layer="37"/>
<rectangle x1="4.191" y1="6.0198" x2="4.699" y2="6.0706" layer="37"/>
<rectangle x1="5.3086" y1="6.0198" x2="5.969" y2="6.0706" layer="37"/>
<rectangle x1="7.1882" y1="6.0198" x2="7.7978" y2="6.0706" layer="37"/>
<rectangle x1="8.7122" y1="6.0198" x2="9.3726" y2="6.0706" layer="37"/>
<rectangle x1="9.6266" y1="6.0198" x2="10.1854" y2="6.0706" layer="37"/>
<rectangle x1="10.8966" y1="6.0198" x2="11.303" y2="6.0706" layer="37"/>
<rectangle x1="7.7978" y1="6.0198" x2="7.8486" y2="6.0706" layer="37"/>
<rectangle x1="10.8458" y1="6.0198" x2="10.8966" y2="6.0706" layer="37"/>
<rectangle x1="2.921" y1="6.0706" x2="3.429" y2="6.1214" layer="37"/>
<rectangle x1="4.191" y1="6.0706" x2="4.699" y2="6.1214" layer="37"/>
<rectangle x1="5.3594" y1="6.0706" x2="5.969" y2="6.1214" layer="37"/>
<rectangle x1="7.1882" y1="6.0706" x2="7.7978" y2="6.1214" layer="37"/>
<rectangle x1="8.7122" y1="6.0706" x2="9.3726" y2="6.1214" layer="37"/>
<rectangle x1="9.6266" y1="6.0706" x2="10.1854" y2="6.1214" layer="37"/>
<rectangle x1="10.8966" y1="6.0706" x2="11.303" y2="6.1214" layer="37"/>
<rectangle x1="7.1374" y1="6.0706" x2="7.1882" y2="6.1214" layer="37"/>
<rectangle x1="5.3086" y1="6.0706" x2="5.3594" y2="6.1214" layer="37"/>
<rectangle x1="5.969" y1="6.0706" x2="6.0198" y2="6.1214" layer="37"/>
<rectangle x1="10.8458" y1="6.0706" x2="10.8966" y2="6.1214" layer="37"/>
<rectangle x1="2.921" y1="6.1214" x2="3.429" y2="6.1722" layer="37"/>
<rectangle x1="4.191" y1="6.1214" x2="4.699" y2="6.1722" layer="37"/>
<rectangle x1="5.3594" y1="6.1214" x2="6.0198" y2="6.1722" layer="37"/>
<rectangle x1="7.1374" y1="6.1214" x2="7.7978" y2="6.1722" layer="37"/>
<rectangle x1="8.7122" y1="6.1214" x2="9.3218" y2="6.1722" layer="37"/>
<rectangle x1="9.6266" y1="6.1214" x2="10.1854" y2="6.1722" layer="37"/>
<rectangle x1="10.8966" y1="6.1214" x2="11.303" y2="6.1722" layer="37"/>
<rectangle x1="7.0866" y1="6.1214" x2="7.1374" y2="6.1722" layer="37"/>
<rectangle x1="8.6614" y1="6.1214" x2="8.7122" y2="6.1722" layer="37"/>
<rectangle x1="9.3218" y1="6.1214" x2="9.3726" y2="6.1722" layer="37"/>
<rectangle x1="6.0198" y1="6.1214" x2="6.0706" y2="6.1722" layer="37"/>
<rectangle x1="10.8458" y1="6.1214" x2="10.8966" y2="6.1722" layer="37"/>
<rectangle x1="2.921" y1="6.1722" x2="3.429" y2="6.223" layer="37"/>
<rectangle x1="4.191" y1="6.1722" x2="4.699" y2="6.223" layer="37"/>
<rectangle x1="5.4102" y1="6.1722" x2="6.0198" y2="6.223" layer="37"/>
<rectangle x1="7.0866" y1="6.1722" x2="7.747" y2="6.223" layer="37"/>
<rectangle x1="8.6614" y1="6.1722" x2="9.271" y2="6.223" layer="37"/>
<rectangle x1="9.6266" y1="6.1722" x2="10.1854" y2="6.223" layer="37"/>
<rectangle x1="10.8966" y1="6.1722" x2="11.303" y2="6.223" layer="37"/>
<rectangle x1="6.0198" y1="6.1722" x2="6.0706" y2="6.223" layer="37"/>
<rectangle x1="9.271" y1="6.1722" x2="9.3218" y2="6.223" layer="37"/>
<rectangle x1="5.3594" y1="6.1722" x2="5.4102" y2="6.223" layer="37"/>
<rectangle x1="7.0358" y1="6.1722" x2="7.0866" y2="6.223" layer="37"/>
<rectangle x1="8.6106" y1="6.1722" x2="8.6614" y2="6.223" layer="37"/>
<rectangle x1="6.0706" y1="6.1722" x2="6.1214" y2="6.223" layer="37"/>
<rectangle x1="10.8458" y1="6.1722" x2="10.8966" y2="6.223" layer="37"/>
<rectangle x1="2.921" y1="6.223" x2="3.429" y2="6.2738" layer="37"/>
<rectangle x1="4.191" y1="6.223" x2="4.699" y2="6.2738" layer="37"/>
<rectangle x1="5.4102" y1="6.223" x2="6.0706" y2="6.2738" layer="37"/>
<rectangle x1="7.0866" y1="6.223" x2="7.6962" y2="6.2738" layer="37"/>
<rectangle x1="8.6106" y1="6.223" x2="9.271" y2="6.2738" layer="37"/>
<rectangle x1="9.6266" y1="6.223" x2="10.1854" y2="6.2738" layer="37"/>
<rectangle x1="10.8966" y1="6.223" x2="11.303" y2="6.2738" layer="37"/>
<rectangle x1="6.0706" y1="6.223" x2="6.1214" y2="6.2738" layer="37"/>
<rectangle x1="7.0358" y1="6.223" x2="7.0866" y2="6.2738" layer="37"/>
<rectangle x1="7.6962" y1="6.223" x2="7.747" y2="6.2738" layer="37"/>
<rectangle x1="8.5598" y1="6.223" x2="8.6106" y2="6.2738" layer="37"/>
<rectangle x1="6.985" y1="6.223" x2="7.0358" y2="6.2738" layer="37"/>
<rectangle x1="10.8458" y1="6.223" x2="10.8966" y2="6.2738" layer="37"/>
<rectangle x1="2.921" y1="6.2738" x2="3.429" y2="6.3246" layer="37"/>
<rectangle x1="4.191" y1="6.2738" x2="4.699" y2="6.3246" layer="37"/>
<rectangle x1="5.461" y1="6.2738" x2="6.1214" y2="6.3246" layer="37"/>
<rectangle x1="7.0358" y1="6.2738" x2="7.6454" y2="6.3246" layer="37"/>
<rectangle x1="8.5598" y1="6.2738" x2="9.2202" y2="6.3246" layer="37"/>
<rectangle x1="9.6266" y1="6.2738" x2="10.1854" y2="6.3246" layer="37"/>
<rectangle x1="10.8966" y1="6.2738" x2="11.303" y2="6.3246" layer="37"/>
<rectangle x1="6.1214" y1="6.2738" x2="6.1722" y2="6.3246" layer="37"/>
<rectangle x1="6.985" y1="6.2738" x2="7.0358" y2="6.3246" layer="37"/>
<rectangle x1="7.6454" y1="6.2738" x2="7.6962" y2="6.3246" layer="37"/>
<rectangle x1="9.2202" y1="6.2738" x2="9.271" y2="6.3246" layer="37"/>
<rectangle x1="10.8458" y1="6.2738" x2="10.8966" y2="6.3246" layer="37"/>
<rectangle x1="2.921" y1="6.3246" x2="3.429" y2="6.3754" layer="37"/>
<rectangle x1="4.191" y1="6.3246" x2="4.699" y2="6.3754" layer="37"/>
<rectangle x1="5.5626" y1="6.3246" x2="6.1722" y2="6.3754" layer="37"/>
<rectangle x1="6.985" y1="6.3246" x2="7.5946" y2="6.3754" layer="37"/>
<rectangle x1="8.5598" y1="6.3246" x2="9.1694" y2="6.3754" layer="37"/>
<rectangle x1="9.6266" y1="6.3246" x2="10.1854" y2="6.3754" layer="37"/>
<rectangle x1="10.8966" y1="6.3246" x2="11.303" y2="6.3754" layer="37"/>
<rectangle x1="5.5118" y1="6.3246" x2="5.5626" y2="6.3754" layer="37"/>
<rectangle x1="6.1722" y1="6.3246" x2="6.223" y2="6.3754" layer="37"/>
<rectangle x1="6.9342" y1="6.3246" x2="6.985" y2="6.3754" layer="37"/>
<rectangle x1="8.509" y1="6.3246" x2="8.5598" y2="6.3754" layer="37"/>
<rectangle x1="7.5946" y1="6.3246" x2="7.6454" y2="6.3754" layer="37"/>
<rectangle x1="9.1694" y1="6.3246" x2="9.2202" y2="6.3754" layer="37"/>
<rectangle x1="10.8458" y1="6.3246" x2="10.8966" y2="6.3754" layer="37"/>
<rectangle x1="2.921" y1="6.3754" x2="3.429" y2="6.4262" layer="37"/>
<rectangle x1="4.191" y1="6.3754" x2="4.699" y2="6.4262" layer="37"/>
<rectangle x1="5.6134" y1="6.3754" x2="6.223" y2="6.4262" layer="37"/>
<rectangle x1="6.9342" y1="6.3754" x2="7.5438" y2="6.4262" layer="37"/>
<rectangle x1="8.4582" y1="6.3754" x2="9.1186" y2="6.4262" layer="37"/>
<rectangle x1="9.6266" y1="6.3754" x2="10.1854" y2="6.4262" layer="37"/>
<rectangle x1="10.8966" y1="6.3754" x2="11.303" y2="6.4262" layer="37"/>
<rectangle x1="5.5626" y1="6.3754" x2="5.6134" y2="6.4262" layer="37"/>
<rectangle x1="6.223" y1="6.3754" x2="6.2738" y2="6.4262" layer="37"/>
<rectangle x1="7.5438" y1="6.3754" x2="7.5946" y2="6.4262" layer="37"/>
<rectangle x1="9.1186" y1="6.3754" x2="9.1694" y2="6.4262" layer="37"/>
<rectangle x1="6.8834" y1="6.3754" x2="6.9342" y2="6.4262" layer="37"/>
<rectangle x1="10.8458" y1="6.3754" x2="10.8966" y2="6.4262" layer="37"/>
<rectangle x1="2.921" y1="6.4262" x2="3.429" y2="6.477" layer="37"/>
<rectangle x1="4.191" y1="6.4262" x2="4.699" y2="6.477" layer="37"/>
<rectangle x1="5.6134" y1="6.4262" x2="6.2738" y2="6.477" layer="37"/>
<rectangle x1="6.8834" y1="6.4262" x2="7.493" y2="6.477" layer="37"/>
<rectangle x1="8.4074" y1="6.4262" x2="9.0678" y2="6.477" layer="37"/>
<rectangle x1="9.6266" y1="6.4262" x2="10.1854" y2="6.477" layer="37"/>
<rectangle x1="10.8966" y1="6.4262" x2="11.303" y2="6.477" layer="37"/>
<rectangle x1="7.493" y1="6.4262" x2="7.5438" y2="6.477" layer="37"/>
<rectangle x1="9.0678" y1="6.4262" x2="9.1186" y2="6.477" layer="37"/>
<rectangle x1="6.2738" y1="6.4262" x2="6.3246" y2="6.477" layer="37"/>
<rectangle x1="6.8326" y1="6.4262" x2="6.8834" y2="6.477" layer="37"/>
<rectangle x1="10.8458" y1="6.4262" x2="10.8966" y2="6.477" layer="37"/>
<rectangle x1="2.921" y1="6.477" x2="3.429" y2="6.5278" layer="37"/>
<rectangle x1="4.191" y1="6.477" x2="4.699" y2="6.5278" layer="37"/>
<rectangle x1="5.6642" y1="6.477" x2="6.3246" y2="6.5278" layer="37"/>
<rectangle x1="6.8326" y1="6.477" x2="7.493" y2="6.5278" layer="37"/>
<rectangle x1="8.4074" y1="6.477" x2="9.0678" y2="6.5278" layer="37"/>
<rectangle x1="9.6266" y1="6.477" x2="10.1854" y2="6.5278" layer="37"/>
<rectangle x1="10.8966" y1="6.477" x2="11.303" y2="6.5278" layer="37"/>
<rectangle x1="8.3566" y1="6.477" x2="8.4074" y2="6.5278" layer="37"/>
<rectangle x1="6.3246" y1="6.477" x2="6.3754" y2="6.5278" layer="37"/>
<rectangle x1="6.7818" y1="6.477" x2="6.8326" y2="6.5278" layer="37"/>
<rectangle x1="5.6134" y1="6.477" x2="5.6642" y2="6.5278" layer="37"/>
<rectangle x1="10.8458" y1="6.477" x2="10.8966" y2="6.5278" layer="37"/>
<rectangle x1="2.921" y1="6.5278" x2="3.429" y2="6.5786" layer="37"/>
<rectangle x1="4.191" y1="6.5278" x2="4.699" y2="6.5786" layer="37"/>
<rectangle x1="5.715" y1="6.5278" x2="6.3754" y2="6.5786" layer="37"/>
<rectangle x1="6.7818" y1="6.5278" x2="7.4422" y2="6.5786" layer="37"/>
<rectangle x1="8.3566" y1="6.5278" x2="9.017" y2="6.5786" layer="37"/>
<rectangle x1="9.6266" y1="6.5278" x2="10.1854" y2="6.5786" layer="37"/>
<rectangle x1="10.8966" y1="6.5278" x2="11.303" y2="6.5786" layer="37"/>
<rectangle x1="6.3754" y1="6.5278" x2="6.4262" y2="6.5786" layer="37"/>
<rectangle x1="8.3058" y1="6.5278" x2="8.3566" y2="6.5786" layer="37"/>
<rectangle x1="5.6642" y1="6.5278" x2="5.715" y2="6.5786" layer="37"/>
<rectangle x1="7.4422" y1="6.5278" x2="7.493" y2="6.5786" layer="37"/>
<rectangle x1="10.8458" y1="6.5278" x2="10.8966" y2="6.5786" layer="37"/>
<rectangle x1="2.921" y1="6.5786" x2="3.429" y2="6.6294" layer="37"/>
<rectangle x1="4.191" y1="6.5786" x2="4.699" y2="6.6294" layer="37"/>
<rectangle x1="5.7658" y1="6.5786" x2="6.4262" y2="6.6294" layer="37"/>
<rectangle x1="6.731" y1="6.5786" x2="7.3914" y2="6.6294" layer="37"/>
<rectangle x1="8.3058" y1="6.5786" x2="8.9662" y2="6.6294" layer="37"/>
<rectangle x1="9.6266" y1="6.5786" x2="10.1854" y2="6.6294" layer="37"/>
<rectangle x1="10.8966" y1="6.5786" x2="11.303" y2="6.6294" layer="37"/>
<rectangle x1="7.3914" y1="6.5786" x2="7.4422" y2="6.6294" layer="37"/>
<rectangle x1="5.715" y1="6.5786" x2="5.7658" y2="6.6294" layer="37"/>
<rectangle x1="6.4262" y1="6.5786" x2="6.477" y2="6.6294" layer="37"/>
<rectangle x1="8.9662" y1="6.5786" x2="9.017" y2="6.6294" layer="37"/>
<rectangle x1="10.8458" y1="6.5786" x2="10.8966" y2="6.6294" layer="37"/>
<rectangle x1="2.921" y1="6.6294" x2="3.429" y2="6.6802" layer="37"/>
<rectangle x1="4.191" y1="6.6294" x2="4.699" y2="6.6802" layer="37"/>
<rectangle x1="5.8166" y1="6.6294" x2="6.477" y2="6.6802" layer="37"/>
<rectangle x1="6.6802" y1="6.6294" x2="7.3406" y2="6.6802" layer="37"/>
<rectangle x1="8.255" y1="6.6294" x2="8.9154" y2="6.6802" layer="37"/>
<rectangle x1="9.6266" y1="6.6294" x2="10.1854" y2="6.6802" layer="37"/>
<rectangle x1="10.8966" y1="6.6294" x2="11.303" y2="6.6802" layer="37"/>
<rectangle x1="7.3406" y1="6.6294" x2="7.3914" y2="6.6802" layer="37"/>
<rectangle x1="5.7658" y1="6.6294" x2="5.8166" y2="6.6802" layer="37"/>
<rectangle x1="8.9154" y1="6.6294" x2="8.9662" y2="6.6802" layer="37"/>
<rectangle x1="10.8458" y1="6.6294" x2="10.8966" y2="6.6802" layer="37"/>
<rectangle x1="2.921" y1="6.6802" x2="3.429" y2="6.731" layer="37"/>
<rectangle x1="4.191" y1="6.6802" x2="4.699" y2="6.731" layer="37"/>
<rectangle x1="5.8674" y1="6.6802" x2="6.5278" y2="6.731" layer="37"/>
<rectangle x1="6.6802" y1="6.6802" x2="7.2898" y2="6.731" layer="37"/>
<rectangle x1="8.2042" y1="6.6802" x2="8.8646" y2="6.731" layer="37"/>
<rectangle x1="9.6266" y1="6.6802" x2="10.1854" y2="6.731" layer="37"/>
<rectangle x1="10.8966" y1="6.6802" x2="11.303" y2="6.731" layer="37"/>
<rectangle x1="7.2898" y1="6.6802" x2="7.3406" y2="6.731" layer="37"/>
<rectangle x1="5.8166" y1="6.6802" x2="5.8674" y2="6.731" layer="37"/>
<rectangle x1="6.6294" y1="6.6802" x2="6.6802" y2="6.731" layer="37"/>
<rectangle x1="8.8646" y1="6.6802" x2="8.9154" y2="6.731" layer="37"/>
<rectangle x1="10.8458" y1="6.6802" x2="10.8966" y2="6.731" layer="37"/>
<rectangle x1="2.921" y1="6.731" x2="3.429" y2="6.7818" layer="37"/>
<rectangle x1="4.191" y1="6.731" x2="4.699" y2="6.7818" layer="37"/>
<rectangle x1="5.9182" y1="6.731" x2="7.239" y2="6.7818" layer="37"/>
<rectangle x1="8.2042" y1="6.731" x2="8.8138" y2="6.7818" layer="37"/>
<rectangle x1="9.6266" y1="6.731" x2="10.1854" y2="6.7818" layer="37"/>
<rectangle x1="10.8966" y1="6.731" x2="11.303" y2="6.7818" layer="37"/>
<rectangle x1="7.239" y1="6.731" x2="7.2898" y2="6.7818" layer="37"/>
<rectangle x1="8.1534" y1="6.731" x2="8.2042" y2="6.7818" layer="37"/>
<rectangle x1="5.8674" y1="6.731" x2="5.9182" y2="6.7818" layer="37"/>
<rectangle x1="8.8138" y1="6.731" x2="8.8646" y2="6.7818" layer="37"/>
<rectangle x1="10.8458" y1="6.731" x2="10.8966" y2="6.7818" layer="37"/>
<rectangle x1="2.921" y1="6.7818" x2="3.429" y2="6.8326" layer="37"/>
<rectangle x1="4.191" y1="6.7818" x2="4.699" y2="6.8326" layer="37"/>
<rectangle x1="5.969" y1="6.7818" x2="7.239" y2="6.8326" layer="37"/>
<rectangle x1="8.1534" y1="6.7818" x2="8.8138" y2="6.8326" layer="37"/>
<rectangle x1="9.6266" y1="6.7818" x2="10.1854" y2="6.8326" layer="37"/>
<rectangle x1="10.8966" y1="6.7818" x2="11.303" y2="6.8326" layer="37"/>
<rectangle x1="5.9182" y1="6.7818" x2="5.969" y2="6.8326" layer="37"/>
<rectangle x1="8.1026" y1="6.7818" x2="8.1534" y2="6.8326" layer="37"/>
<rectangle x1="10.8458" y1="6.7818" x2="10.8966" y2="6.8326" layer="37"/>
<rectangle x1="2.921" y1="6.8326" x2="3.429" y2="6.8834" layer="37"/>
<rectangle x1="4.191" y1="6.8326" x2="4.699" y2="6.8834" layer="37"/>
<rectangle x1="6.0198" y1="6.8326" x2="7.1882" y2="6.8834" layer="37"/>
<rectangle x1="8.1026" y1="6.8326" x2="8.763" y2="6.8834" layer="37"/>
<rectangle x1="9.6266" y1="6.8326" x2="10.1854" y2="6.8834" layer="37"/>
<rectangle x1="10.8966" y1="6.8326" x2="11.303" y2="6.8834" layer="37"/>
<rectangle x1="5.969" y1="6.8326" x2="6.0198" y2="6.8834" layer="37"/>
<rectangle x1="7.1882" y1="6.8326" x2="7.239" y2="6.8834" layer="37"/>
<rectangle x1="8.0518" y1="6.8326" x2="8.1026" y2="6.8834" layer="37"/>
<rectangle x1="10.8458" y1="6.8326" x2="10.8966" y2="6.8834" layer="37"/>
<rectangle x1="2.921" y1="6.8834" x2="3.429" y2="6.9342" layer="37"/>
<rectangle x1="4.191" y1="6.8834" x2="4.699" y2="6.9342" layer="37"/>
<rectangle x1="6.0198" y1="6.8834" x2="7.1374" y2="6.9342" layer="37"/>
<rectangle x1="8.0518" y1="6.8834" x2="8.7122" y2="6.9342" layer="37"/>
<rectangle x1="9.6266" y1="6.8834" x2="10.1854" y2="6.9342" layer="37"/>
<rectangle x1="10.8966" y1="6.8834" x2="11.303" y2="6.9342" layer="37"/>
<rectangle x1="7.1374" y1="6.8834" x2="7.1882" y2="6.9342" layer="37"/>
<rectangle x1="8.7122" y1="6.8834" x2="8.763" y2="6.9342" layer="37"/>
<rectangle x1="10.8458" y1="6.8834" x2="10.8966" y2="6.9342" layer="37"/>
<rectangle x1="5.969" y1="6.8834" x2="6.0198" y2="6.9342" layer="37"/>
<rectangle x1="2.921" y1="6.9342" x2="3.429" y2="6.985" layer="37"/>
<rectangle x1="4.191" y1="6.9342" x2="4.699" y2="6.985" layer="37"/>
<rectangle x1="6.0706" y1="6.9342" x2="7.0866" y2="6.985" layer="37"/>
<rectangle x1="8.0518" y1="6.9342" x2="8.6614" y2="6.985" layer="37"/>
<rectangle x1="9.6266" y1="6.9342" x2="10.1854" y2="6.985" layer="37"/>
<rectangle x1="10.8966" y1="6.9342" x2="11.303" y2="6.985" layer="37"/>
<rectangle x1="7.0866" y1="6.9342" x2="7.1374" y2="6.985" layer="37"/>
<rectangle x1="8.001" y1="6.9342" x2="8.0518" y2="6.985" layer="37"/>
<rectangle x1="8.6614" y1="6.9342" x2="8.7122" y2="6.985" layer="37"/>
<rectangle x1="6.0198" y1="6.9342" x2="6.0706" y2="6.985" layer="37"/>
<rectangle x1="10.8458" y1="6.9342" x2="10.8966" y2="6.985" layer="37"/>
<rectangle x1="2.921" y1="6.985" x2="3.429" y2="7.0358" layer="37"/>
<rectangle x1="4.191" y1="6.985" x2="4.699" y2="7.0358" layer="37"/>
<rectangle x1="6.1214" y1="6.985" x2="7.0866" y2="7.0358" layer="37"/>
<rectangle x1="8.001" y1="6.985" x2="8.6106" y2="7.0358" layer="37"/>
<rectangle x1="9.6266" y1="6.985" x2="10.1854" y2="7.0358" layer="37"/>
<rectangle x1="10.8966" y1="6.985" x2="11.303" y2="7.0358" layer="37"/>
<rectangle x1="7.9502" y1="6.985" x2="8.001" y2="7.0358" layer="37"/>
<rectangle x1="8.6106" y1="6.985" x2="8.6614" y2="7.0358" layer="37"/>
<rectangle x1="6.0706" y1="6.985" x2="6.1214" y2="7.0358" layer="37"/>
<rectangle x1="10.8458" y1="6.985" x2="10.8966" y2="7.0358" layer="37"/>
<rectangle x1="2.921" y1="7.0358" x2="3.429" y2="7.0866" layer="37"/>
<rectangle x1="4.191" y1="7.0358" x2="4.699" y2="7.0866" layer="37"/>
<rectangle x1="6.1722" y1="7.0358" x2="7.0358" y2="7.0866" layer="37"/>
<rectangle x1="7.9502" y1="7.0358" x2="8.5598" y2="7.0866" layer="37"/>
<rectangle x1="9.6266" y1="7.0358" x2="10.1854" y2="7.0866" layer="37"/>
<rectangle x1="10.8966" y1="7.0358" x2="11.303" y2="7.0866" layer="37"/>
<rectangle x1="8.5598" y1="7.0358" x2="8.6106" y2="7.0866" layer="37"/>
<rectangle x1="6.1214" y1="7.0358" x2="6.1722" y2="7.0866" layer="37"/>
<rectangle x1="7.8994" y1="7.0358" x2="7.9502" y2="7.0866" layer="37"/>
<rectangle x1="10.8458" y1="7.0358" x2="10.8966" y2="7.0866" layer="37"/>
<rectangle x1="2.921" y1="7.0866" x2="3.429" y2="7.1374" layer="37"/>
<rectangle x1="4.191" y1="7.0866" x2="4.699" y2="7.1374" layer="37"/>
<rectangle x1="6.1722" y1="7.0866" x2="6.985" y2="7.1374" layer="37"/>
<rectangle x1="7.8994" y1="7.0866" x2="8.5598" y2="7.1374" layer="37"/>
<rectangle x1="9.6266" y1="7.0866" x2="10.1854" y2="7.1374" layer="37"/>
<rectangle x1="10.8966" y1="7.0866" x2="11.303" y2="7.1374" layer="37"/>
<rectangle x1="7.8486" y1="7.0866" x2="7.8994" y2="7.1374" layer="37"/>
<rectangle x1="6.985" y1="7.0866" x2="7.0358" y2="7.1374" layer="37"/>
<rectangle x1="10.8458" y1="7.0866" x2="10.8966" y2="7.1374" layer="37"/>
<rectangle x1="2.921" y1="7.1374" x2="3.429" y2="7.1882" layer="37"/>
<rectangle x1="4.191" y1="7.1374" x2="4.699" y2="7.1882" layer="37"/>
<rectangle x1="6.223" y1="7.1374" x2="6.9342" y2="7.1882" layer="37"/>
<rectangle x1="7.8486" y1="7.1374" x2="8.509" y2="7.1882" layer="37"/>
<rectangle x1="9.6266" y1="7.1374" x2="10.1854" y2="7.1882" layer="37"/>
<rectangle x1="10.8966" y1="7.1374" x2="11.303" y2="7.1882" layer="37"/>
<rectangle x1="6.9342" y1="7.1374" x2="6.985" y2="7.1882" layer="37"/>
<rectangle x1="7.7978" y1="7.1374" x2="7.8486" y2="7.1882" layer="37"/>
<rectangle x1="10.8458" y1="7.1374" x2="10.8966" y2="7.1882" layer="37"/>
<rectangle x1="2.921" y1="7.1882" x2="3.429" y2="7.239" layer="37"/>
<rectangle x1="4.191" y1="7.1882" x2="4.699" y2="7.239" layer="37"/>
<rectangle x1="6.223" y1="7.1882" x2="6.8834" y2="7.239" layer="37"/>
<rectangle x1="7.8486" y1="7.1882" x2="8.4582" y2="7.239" layer="37"/>
<rectangle x1="9.6266" y1="7.1882" x2="10.1854" y2="7.239" layer="37"/>
<rectangle x1="10.8966" y1="7.1882" x2="11.303" y2="7.239" layer="37"/>
<rectangle x1="7.7978" y1="7.1882" x2="7.8486" y2="7.239" layer="37"/>
<rectangle x1="6.8834" y1="7.1882" x2="6.9342" y2="7.239" layer="37"/>
<rectangle x1="8.4582" y1="7.1882" x2="8.509" y2="7.239" layer="37"/>
<rectangle x1="10.8458" y1="7.1882" x2="10.8966" y2="7.239" layer="37"/>
<rectangle x1="2.921" y1="7.239" x2="3.429" y2="7.2898" layer="37"/>
<rectangle x1="4.191" y1="7.239" x2="4.699" y2="7.2898" layer="37"/>
<rectangle x1="6.223" y1="7.239" x2="6.8834" y2="7.2898" layer="37"/>
<rectangle x1="7.7978" y1="7.239" x2="8.4074" y2="7.2898" layer="37"/>
<rectangle x1="9.6266" y1="7.239" x2="10.1854" y2="7.2898" layer="37"/>
<rectangle x1="10.8966" y1="7.239" x2="11.303" y2="7.2898" layer="37"/>
<rectangle x1="6.1722" y1="7.239" x2="6.223" y2="7.2898" layer="37"/>
<rectangle x1="7.747" y1="7.239" x2="7.7978" y2="7.2898" layer="37"/>
<rectangle x1="8.4074" y1="7.239" x2="8.4582" y2="7.2898" layer="37"/>
<rectangle x1="10.8458" y1="7.239" x2="10.8966" y2="7.2898" layer="37"/>
<rectangle x1="2.921" y1="7.2898" x2="3.429" y2="7.3406" layer="37"/>
<rectangle x1="4.191" y1="7.2898" x2="4.699" y2="7.3406" layer="37"/>
<rectangle x1="6.1722" y1="7.2898" x2="6.8326" y2="7.3406" layer="37"/>
<rectangle x1="7.747" y1="7.2898" x2="8.3566" y2="7.3406" layer="37"/>
<rectangle x1="9.6266" y1="7.2898" x2="10.1854" y2="7.3406" layer="37"/>
<rectangle x1="10.8966" y1="7.2898" x2="11.303" y2="7.3406" layer="37"/>
<rectangle x1="6.1214" y1="7.2898" x2="6.1722" y2="7.3406" layer="37"/>
<rectangle x1="7.6962" y1="7.2898" x2="7.747" y2="7.3406" layer="37"/>
<rectangle x1="8.3566" y1="7.2898" x2="8.4074" y2="7.3406" layer="37"/>
<rectangle x1="10.8458" y1="7.2898" x2="10.8966" y2="7.3406" layer="37"/>
<rectangle x1="2.921" y1="7.3406" x2="3.429" y2="7.3914" layer="37"/>
<rectangle x1="4.191" y1="7.3406" x2="4.699" y2="7.3914" layer="37"/>
<rectangle x1="6.1214" y1="7.3406" x2="6.7818" y2="7.3914" layer="37"/>
<rectangle x1="7.6962" y1="7.3406" x2="8.3058" y2="7.3914" layer="37"/>
<rectangle x1="9.6266" y1="7.3406" x2="10.1854" y2="7.3914" layer="37"/>
<rectangle x1="10.8966" y1="7.3406" x2="11.303" y2="7.3914" layer="37"/>
<rectangle x1="8.3058" y1="7.3406" x2="8.3566" y2="7.3914" layer="37"/>
<rectangle x1="6.0706" y1="7.3406" x2="6.1214" y2="7.3914" layer="37"/>
<rectangle x1="7.6454" y1="7.3406" x2="7.6962" y2="7.3914" layer="37"/>
<rectangle x1="10.8458" y1="7.3406" x2="10.8966" y2="7.3914" layer="37"/>
<rectangle x1="2.921" y1="7.3914" x2="3.429" y2="7.4422" layer="37"/>
<rectangle x1="4.191" y1="7.3914" x2="4.699" y2="7.4422" layer="37"/>
<rectangle x1="6.0706" y1="7.3914" x2="6.731" y2="7.4422" layer="37"/>
<rectangle x1="7.6454" y1="7.3914" x2="8.255" y2="7.4422" layer="37"/>
<rectangle x1="9.6266" y1="7.3914" x2="10.1854" y2="7.4422" layer="37"/>
<rectangle x1="10.8966" y1="7.3914" x2="11.303" y2="7.4422" layer="37"/>
<rectangle x1="8.255" y1="7.3914" x2="8.3058" y2="7.4422" layer="37"/>
<rectangle x1="7.5946" y1="7.3914" x2="7.6454" y2="7.4422" layer="37"/>
<rectangle x1="6.0198" y1="7.3914" x2="6.0706" y2="7.4422" layer="37"/>
<rectangle x1="6.731" y1="7.3914" x2="6.7818" y2="7.4422" layer="37"/>
<rectangle x1="10.8458" y1="7.3914" x2="10.8966" y2="7.4422" layer="37"/>
<rectangle x1="2.921" y1="7.4422" x2="3.429" y2="7.493" layer="37"/>
<rectangle x1="4.191" y1="7.4422" x2="4.699" y2="7.493" layer="37"/>
<rectangle x1="6.0198" y1="7.4422" x2="6.6802" y2="7.493" layer="37"/>
<rectangle x1="7.5946" y1="7.4422" x2="8.2042" y2="7.493" layer="37"/>
<rectangle x1="9.6266" y1="7.4422" x2="10.1854" y2="7.493" layer="37"/>
<rectangle x1="10.8966" y1="7.4422" x2="11.303" y2="7.493" layer="37"/>
<rectangle x1="8.2042" y1="7.4422" x2="8.255" y2="7.493" layer="37"/>
<rectangle x1="6.6802" y1="7.4422" x2="6.731" y2="7.493" layer="37"/>
<rectangle x1="10.8458" y1="7.4422" x2="10.8966" y2="7.493" layer="37"/>
<rectangle x1="7.5438" y1="7.4422" x2="7.5946" y2="7.493" layer="37"/>
<rectangle x1="8.255" y1="7.4422" x2="8.3058" y2="7.493" layer="37"/>
<rectangle x1="2.921" y1="7.493" x2="3.429" y2="7.5438" layer="37"/>
<rectangle x1="4.191" y1="7.493" x2="4.699" y2="7.5438" layer="37"/>
<rectangle x1="6.0198" y1="7.493" x2="6.6294" y2="7.5438" layer="37"/>
<rectangle x1="7.5438" y1="7.493" x2="8.2042" y2="7.5438" layer="37"/>
<rectangle x1="9.6266" y1="7.493" x2="10.1854" y2="7.5438" layer="37"/>
<rectangle x1="10.8966" y1="7.493" x2="11.303" y2="7.5438" layer="37"/>
<rectangle x1="5.969" y1="7.493" x2="6.0198" y2="7.5438" layer="37"/>
<rectangle x1="6.6294" y1="7.493" x2="6.6802" y2="7.5438" layer="37"/>
<rectangle x1="8.2042" y1="7.493" x2="8.255" y2="7.5438" layer="37"/>
<rectangle x1="10.8458" y1="7.493" x2="10.8966" y2="7.5438" layer="37"/>
<rectangle x1="2.921" y1="7.5438" x2="3.429" y2="7.5946" layer="37"/>
<rectangle x1="4.191" y1="7.5438" x2="4.699" y2="7.5946" layer="37"/>
<rectangle x1="5.969" y1="7.5438" x2="6.5786" y2="7.5946" layer="37"/>
<rectangle x1="7.493" y1="7.5438" x2="8.1534" y2="7.5946" layer="37"/>
<rectangle x1="9.6266" y1="7.5438" x2="10.1854" y2="7.5946" layer="37"/>
<rectangle x1="10.8966" y1="7.5438" x2="11.303" y2="7.5946" layer="37"/>
<rectangle x1="5.9182" y1="7.5438" x2="5.969" y2="7.5946" layer="37"/>
<rectangle x1="6.5786" y1="7.5438" x2="6.6294" y2="7.5946" layer="37"/>
<rectangle x1="8.1534" y1="7.5438" x2="8.2042" y2="7.5946" layer="37"/>
<rectangle x1="10.8458" y1="7.5438" x2="10.8966" y2="7.5946" layer="37"/>
<rectangle x1="2.921" y1="7.5946" x2="3.429" y2="7.6454" layer="37"/>
<rectangle x1="4.191" y1="7.5946" x2="4.699" y2="7.6454" layer="37"/>
<rectangle x1="5.9182" y1="7.5946" x2="6.5278" y2="7.6454" layer="37"/>
<rectangle x1="7.4422" y1="7.5946" x2="8.1026" y2="7.6454" layer="37"/>
<rectangle x1="9.6266" y1="7.5946" x2="10.1854" y2="7.6454" layer="37"/>
<rectangle x1="10.8966" y1="7.5946" x2="11.303" y2="7.6454" layer="37"/>
<rectangle x1="6.5278" y1="7.5946" x2="6.5786" y2="7.6454" layer="37"/>
<rectangle x1="8.1026" y1="7.5946" x2="8.1534" y2="7.6454" layer="37"/>
<rectangle x1="5.8674" y1="7.5946" x2="5.9182" y2="7.6454" layer="37"/>
<rectangle x1="10.8458" y1="7.5946" x2="10.8966" y2="7.6454" layer="37"/>
<rectangle x1="2.921" y1="7.6454" x2="3.429" y2="7.6962" layer="37"/>
<rectangle x1="4.191" y1="7.6454" x2="4.699" y2="7.6962" layer="37"/>
<rectangle x1="5.8674" y1="7.6454" x2="6.5278" y2="7.6962" layer="37"/>
<rectangle x1="7.4422" y1="7.6454" x2="8.1026" y2="7.6962" layer="37"/>
<rectangle x1="9.6266" y1="7.6454" x2="10.1854" y2="7.6962" layer="37"/>
<rectangle x1="10.8966" y1="7.6454" x2="11.303" y2="7.6962" layer="37"/>
<rectangle x1="7.3914" y1="7.6454" x2="7.4422" y2="7.6962" layer="37"/>
<rectangle x1="5.8166" y1="7.6454" x2="5.8674" y2="7.6962" layer="37"/>
<rectangle x1="10.8458" y1="7.6454" x2="10.8966" y2="7.6962" layer="37"/>
<rectangle x1="2.921" y1="7.6962" x2="3.429" y2="7.747" layer="37"/>
<rectangle x1="4.191" y1="7.6962" x2="4.699" y2="7.747" layer="37"/>
<rectangle x1="5.8166" y1="7.6962" x2="6.477" y2="7.747" layer="37"/>
<rectangle x1="7.3914" y1="7.6962" x2="8.0518" y2="7.747" layer="37"/>
<rectangle x1="9.6266" y1="7.6962" x2="10.1854" y2="7.747" layer="37"/>
<rectangle x1="10.8966" y1="7.6962" x2="11.303" y2="7.747" layer="37"/>
<rectangle x1="7.3406" y1="7.6962" x2="7.3914" y2="7.747" layer="37"/>
<rectangle x1="6.477" y1="7.6962" x2="6.5278" y2="7.747" layer="37"/>
<rectangle x1="10.8458" y1="7.6962" x2="10.8966" y2="7.747" layer="37"/>
<rectangle x1="5.7658" y1="7.6962" x2="5.8166" y2="7.747" layer="37"/>
<rectangle x1="2.921" y1="7.747" x2="3.429" y2="7.7978" layer="37"/>
<rectangle x1="4.191" y1="7.747" x2="4.699" y2="7.7978" layer="37"/>
<rectangle x1="5.7658" y1="7.747" x2="6.4262" y2="7.7978" layer="37"/>
<rectangle x1="7.3406" y1="7.747" x2="8.001" y2="7.7978" layer="37"/>
<rectangle x1="9.6266" y1="7.747" x2="10.1854" y2="7.7978" layer="37"/>
<rectangle x1="10.8966" y1="7.747" x2="11.303" y2="7.7978" layer="37"/>
<rectangle x1="6.4262" y1="7.747" x2="6.477" y2="7.7978" layer="37"/>
<rectangle x1="10.8458" y1="7.747" x2="10.8966" y2="7.7978" layer="37"/>
<rectangle x1="8.001" y1="7.747" x2="8.0518" y2="7.7978" layer="37"/>
<rectangle x1="2.921" y1="7.7978" x2="3.429" y2="7.8486" layer="37"/>
<rectangle x1="4.191" y1="7.7978" x2="4.699" y2="7.8486" layer="37"/>
<rectangle x1="5.7658" y1="7.7978" x2="6.3754" y2="7.8486" layer="37"/>
<rectangle x1="7.2898" y1="7.7978" x2="7.9502" y2="7.8486" layer="37"/>
<rectangle x1="8.5598" y1="7.7978" x2="8.6106" y2="7.8486" layer="37"/>
<rectangle x1="9.6266" y1="7.7978" x2="10.1854" y2="7.8486" layer="37"/>
<rectangle x1="10.8966" y1="7.7978" x2="11.303" y2="7.8486" layer="37"/>
<rectangle x1="5.715" y1="7.7978" x2="5.7658" y2="7.8486" layer="37"/>
<rectangle x1="6.3754" y1="7.7978" x2="6.4262" y2="7.8486" layer="37"/>
<rectangle x1="8.509" y1="7.7978" x2="8.5598" y2="7.8486" layer="37"/>
<rectangle x1="8.6106" y1="7.7978" x2="8.6614" y2="7.8486" layer="37"/>
<rectangle x1="7.9502" y1="7.7978" x2="8.001" y2="7.8486" layer="37"/>
<rectangle x1="10.8458" y1="7.7978" x2="10.8966" y2="7.8486" layer="37"/>
<rectangle x1="2.921" y1="7.8486" x2="3.429" y2="7.8994" layer="37"/>
<rectangle x1="4.191" y1="7.8486" x2="4.699" y2="7.8994" layer="37"/>
<rectangle x1="5.715" y1="7.8486" x2="6.3246" y2="7.8994" layer="37"/>
<rectangle x1="7.239" y1="7.8486" x2="7.8994" y2="7.8994" layer="37"/>
<rectangle x1="8.4582" y1="7.8486" x2="8.7122" y2="7.8994" layer="37"/>
<rectangle x1="9.6266" y1="7.8486" x2="10.1854" y2="7.8994" layer="37"/>
<rectangle x1="10.8966" y1="7.8486" x2="11.303" y2="7.8994" layer="37"/>
<rectangle x1="6.3246" y1="7.8486" x2="6.3754" y2="7.8994" layer="37"/>
<rectangle x1="8.4074" y1="7.8486" x2="8.4582" y2="7.8994" layer="37"/>
<rectangle x1="5.6642" y1="7.8486" x2="5.715" y2="7.8994" layer="37"/>
<rectangle x1="7.8994" y1="7.8486" x2="7.9502" y2="7.8994" layer="37"/>
<rectangle x1="8.7122" y1="7.8486" x2="8.763" y2="7.8994" layer="37"/>
<rectangle x1="10.8458" y1="7.8486" x2="10.8966" y2="7.8994" layer="37"/>
<rectangle x1="2.921" y1="7.8994" x2="3.429" y2="7.9502" layer="37"/>
<rectangle x1="4.191" y1="7.8994" x2="4.699" y2="7.9502" layer="37"/>
<rectangle x1="5.6642" y1="7.8994" x2="6.2738" y2="7.9502" layer="37"/>
<rectangle x1="7.239" y1="7.8994" x2="7.8486" y2="7.9502" layer="37"/>
<rectangle x1="8.3566" y1="7.8994" x2="8.763" y2="7.9502" layer="37"/>
<rectangle x1="9.6266" y1="7.8994" x2="10.1854" y2="7.9502" layer="37"/>
<rectangle x1="10.8966" y1="7.8994" x2="11.303" y2="7.9502" layer="37"/>
<rectangle x1="6.2738" y1="7.8994" x2="6.3246" y2="7.9502" layer="37"/>
<rectangle x1="7.1882" y1="7.8994" x2="7.239" y2="7.9502" layer="37"/>
<rectangle x1="7.8486" y1="7.8994" x2="7.8994" y2="7.9502" layer="37"/>
<rectangle x1="8.763" y1="7.8994" x2="8.8138" y2="7.9502" layer="37"/>
<rectangle x1="5.6134" y1="7.8994" x2="5.6642" y2="7.9502" layer="37"/>
<rectangle x1="10.8458" y1="7.8994" x2="10.8966" y2="7.9502" layer="37"/>
<rectangle x1="2.921" y1="7.9502" x2="3.429" y2="8.001" layer="37"/>
<rectangle x1="4.191" y1="7.9502" x2="4.699" y2="8.001" layer="37"/>
<rectangle x1="5.6134" y1="7.9502" x2="6.2738" y2="8.001" layer="37"/>
<rectangle x1="7.1882" y1="7.9502" x2="7.8486" y2="8.001" layer="37"/>
<rectangle x1="8.3566" y1="7.9502" x2="8.8138" y2="8.001" layer="37"/>
<rectangle x1="9.6266" y1="7.9502" x2="10.1854" y2="8.001" layer="37"/>
<rectangle x1="10.8966" y1="7.9502" x2="11.303" y2="8.001" layer="37"/>
<rectangle x1="7.1374" y1="7.9502" x2="7.1882" y2="8.001" layer="37"/>
<rectangle x1="8.8138" y1="7.9502" x2="8.8646" y2="8.001" layer="37"/>
<rectangle x1="5.5626" y1="7.9502" x2="5.6134" y2="8.001" layer="37"/>
<rectangle x1="8.3058" y1="7.9502" x2="8.3566" y2="8.001" layer="37"/>
<rectangle x1="10.8458" y1="7.9502" x2="10.8966" y2="8.001" layer="37"/>
<rectangle x1="2.921" y1="8.001" x2="3.429" y2="8.0518" layer="37"/>
<rectangle x1="4.191" y1="8.001" x2="4.699" y2="8.0518" layer="37"/>
<rectangle x1="5.5626" y1="8.001" x2="6.223" y2="8.0518" layer="37"/>
<rectangle x1="7.1374" y1="8.001" x2="7.7978" y2="8.0518" layer="37"/>
<rectangle x1="8.3058" y1="8.001" x2="8.8646" y2="8.0518" layer="37"/>
<rectangle x1="9.6266" y1="8.001" x2="10.1854" y2="8.0518" layer="37"/>
<rectangle x1="10.8966" y1="8.001" x2="11.303" y2="8.0518" layer="37"/>
<rectangle x1="7.0866" y1="8.001" x2="7.1374" y2="8.0518" layer="37"/>
<rectangle x1="6.223" y1="8.001" x2="6.2738" y2="8.0518" layer="37"/>
<rectangle x1="8.8646" y1="8.001" x2="8.9154" y2="8.0518" layer="37"/>
<rectangle x1="10.8458" y1="8.001" x2="10.8966" y2="8.0518" layer="37"/>
<rectangle x1="2.921" y1="8.0518" x2="3.429" y2="8.1026" layer="37"/>
<rectangle x1="4.191" y1="8.0518" x2="4.699" y2="8.1026" layer="37"/>
<rectangle x1="5.5118" y1="8.0518" x2="6.1722" y2="8.1026" layer="37"/>
<rectangle x1="7.1374" y1="8.0518" x2="7.747" y2="8.1026" layer="37"/>
<rectangle x1="8.3058" y1="8.0518" x2="8.9154" y2="8.1026" layer="37"/>
<rectangle x1="9.6266" y1="8.0518" x2="10.1854" y2="8.1026" layer="37"/>
<rectangle x1="10.8966" y1="8.0518" x2="11.303" y2="8.1026" layer="37"/>
<rectangle x1="7.0866" y1="8.0518" x2="7.1374" y2="8.1026" layer="37"/>
<rectangle x1="6.1722" y1="8.0518" x2="6.223" y2="8.1026" layer="37"/>
<rectangle x1="7.747" y1="8.0518" x2="7.7978" y2="8.1026" layer="37"/>
<rectangle x1="10.8458" y1="8.0518" x2="10.8966" y2="8.1026" layer="37"/>
<rectangle x1="2.921" y1="8.1026" x2="3.429" y2="8.1534" layer="37"/>
<rectangle x1="4.191" y1="8.1026" x2="4.699" y2="8.1534" layer="37"/>
<rectangle x1="5.5118" y1="8.1026" x2="6.1214" y2="8.1534" layer="37"/>
<rectangle x1="7.0866" y1="8.1026" x2="7.6962" y2="8.1534" layer="37"/>
<rectangle x1="8.3058" y1="8.1026" x2="8.9154" y2="8.1534" layer="37"/>
<rectangle x1="9.6266" y1="8.1026" x2="10.1854" y2="8.1534" layer="37"/>
<rectangle x1="10.8966" y1="8.1026" x2="11.303" y2="8.1534" layer="37"/>
<rectangle x1="5.461" y1="8.1026" x2="5.5118" y2="8.1534" layer="37"/>
<rectangle x1="7.0358" y1="8.1026" x2="7.0866" y2="8.1534" layer="37"/>
<rectangle x1="8.9154" y1="8.1026" x2="8.9662" y2="8.1534" layer="37"/>
<rectangle x1="6.1214" y1="8.1026" x2="6.1722" y2="8.1534" layer="37"/>
<rectangle x1="7.6962" y1="8.1026" x2="7.747" y2="8.1534" layer="37"/>
<rectangle x1="10.8458" y1="8.1026" x2="10.8966" y2="8.1534" layer="37"/>
<rectangle x1="2.921" y1="8.1534" x2="3.429" y2="8.2042" layer="37"/>
<rectangle x1="4.191" y1="8.1534" x2="4.699" y2="8.2042" layer="37"/>
<rectangle x1="5.461" y1="8.1534" x2="6.0706" y2="8.2042" layer="37"/>
<rectangle x1="7.0358" y1="8.1534" x2="7.6962" y2="8.2042" layer="37"/>
<rectangle x1="8.3566" y1="8.1534" x2="8.9662" y2="8.2042" layer="37"/>
<rectangle x1="9.6266" y1="8.1534" x2="10.1854" y2="8.2042" layer="37"/>
<rectangle x1="10.8966" y1="8.1534" x2="11.303" y2="8.2042" layer="37"/>
<rectangle x1="6.0706" y1="8.1534" x2="6.1214" y2="8.2042" layer="37"/>
<rectangle x1="6.985" y1="8.1534" x2="7.0358" y2="8.2042" layer="37"/>
<rectangle x1="5.4102" y1="8.1534" x2="5.461" y2="8.2042" layer="37"/>
<rectangle x1="8.9662" y1="8.1534" x2="9.017" y2="8.2042" layer="37"/>
<rectangle x1="10.8458" y1="8.1534" x2="10.8966" y2="8.2042" layer="37"/>
<rectangle x1="2.921" y1="8.2042" x2="3.429" y2="8.255" layer="37"/>
<rectangle x1="4.191" y1="8.2042" x2="4.699" y2="8.255" layer="37"/>
<rectangle x1="5.4102" y1="8.2042" x2="6.0706" y2="8.255" layer="37"/>
<rectangle x1="6.985" y1="8.2042" x2="7.6962" y2="8.255" layer="37"/>
<rectangle x1="8.3566" y1="8.2042" x2="9.017" y2="8.255" layer="37"/>
<rectangle x1="9.6266" y1="8.2042" x2="10.1854" y2="8.255" layer="37"/>
<rectangle x1="10.8966" y1="8.2042" x2="11.303" y2="8.255" layer="37"/>
<rectangle x1="7.6962" y1="8.2042" x2="7.747" y2="8.255" layer="37"/>
<rectangle x1="6.9342" y1="8.2042" x2="6.985" y2="8.255" layer="37"/>
<rectangle x1="5.3594" y1="8.2042" x2="5.4102" y2="8.255" layer="37"/>
<rectangle x1="9.017" y1="8.2042" x2="9.0678" y2="8.255" layer="37"/>
<rectangle x1="10.8458" y1="8.2042" x2="10.8966" y2="8.255" layer="37"/>
<rectangle x1="2.921" y1="8.255" x2="3.429" y2="8.3058" layer="37"/>
<rectangle x1="4.191" y1="8.255" x2="4.699" y2="8.3058" layer="37"/>
<rectangle x1="5.3594" y1="8.255" x2="6.0198" y2="8.3058" layer="37"/>
<rectangle x1="6.9342" y1="8.255" x2="7.747" y2="8.3058" layer="37"/>
<rectangle x1="8.4074" y1="8.255" x2="9.0678" y2="8.3058" layer="37"/>
<rectangle x1="9.6266" y1="8.255" x2="10.1854" y2="8.3058" layer="37"/>
<rectangle x1="10.8966" y1="8.255" x2="11.303" y2="8.3058" layer="37"/>
<rectangle x1="6.8834" y1="8.255" x2="6.9342" y2="8.3058" layer="37"/>
<rectangle x1="5.3086" y1="8.255" x2="5.3594" y2="8.3058" layer="37"/>
<rectangle x1="7.747" y1="8.255" x2="7.7978" y2="8.3058" layer="37"/>
<rectangle x1="6.0198" y1="8.255" x2="6.0706" y2="8.3058" layer="37"/>
<rectangle x1="10.8458" y1="8.255" x2="10.8966" y2="8.3058" layer="37"/>
<rectangle x1="2.921" y1="8.3058" x2="3.429" y2="8.3566" layer="37"/>
<rectangle x1="4.191" y1="8.3058" x2="4.699" y2="8.3566" layer="37"/>
<rectangle x1="5.3086" y1="8.3058" x2="5.969" y2="8.3566" layer="37"/>
<rectangle x1="6.8834" y1="8.3058" x2="7.7978" y2="8.3566" layer="37"/>
<rectangle x1="8.4582" y1="8.3058" x2="9.1186" y2="8.3566" layer="37"/>
<rectangle x1="9.6266" y1="8.3058" x2="10.1854" y2="8.3566" layer="37"/>
<rectangle x1="10.8966" y1="8.3058" x2="11.303" y2="8.3566" layer="37"/>
<rectangle x1="5.969" y1="8.3058" x2="6.0198" y2="8.3566" layer="37"/>
<rectangle x1="6.8326" y1="8.3058" x2="6.8834" y2="8.3566" layer="37"/>
<rectangle x1="10.8458" y1="8.3058" x2="10.8966" y2="8.3566" layer="37"/>
<rectangle x1="2.921" y1="8.3566" x2="3.429" y2="8.4074" layer="37"/>
<rectangle x1="4.191" y1="8.3566" x2="4.699" y2="8.4074" layer="37"/>
<rectangle x1="5.2578" y1="8.3566" x2="5.969" y2="8.4074" layer="37"/>
<rectangle x1="6.8834" y1="8.3566" x2="7.7978" y2="8.4074" layer="37"/>
<rectangle x1="8.509" y1="8.3566" x2="9.1186" y2="8.4074" layer="37"/>
<rectangle x1="9.6266" y1="8.3566" x2="10.1854" y2="8.4074" layer="37"/>
<rectangle x1="10.8966" y1="8.3566" x2="11.303" y2="8.4074" layer="37"/>
<rectangle x1="6.8326" y1="8.3566" x2="6.8834" y2="8.4074" layer="37"/>
<rectangle x1="7.7978" y1="8.3566" x2="7.8486" y2="8.4074" layer="37"/>
<rectangle x1="9.1186" y1="8.3566" x2="9.1694" y2="8.4074" layer="37"/>
<rectangle x1="8.4582" y1="8.3566" x2="8.509" y2="8.4074" layer="37"/>
<rectangle x1="10.8458" y1="8.3566" x2="10.8966" y2="8.4074" layer="37"/>
<rectangle x1="2.921" y1="8.4074" x2="3.429" y2="8.4582" layer="37"/>
<rectangle x1="4.191" y1="8.4074" x2="4.699" y2="8.4582" layer="37"/>
<rectangle x1="5.2578" y1="8.4074" x2="5.9182" y2="8.4582" layer="37"/>
<rectangle x1="6.8326" y1="8.4074" x2="7.8486" y2="8.4582" layer="37"/>
<rectangle x1="8.509" y1="8.4074" x2="9.1694" y2="8.4582" layer="37"/>
<rectangle x1="9.6266" y1="8.4074" x2="10.1854" y2="8.4582" layer="37"/>
<rectangle x1="10.8966" y1="8.4074" x2="11.303" y2="8.4582" layer="37"/>
<rectangle x1="5.207" y1="8.4074" x2="5.2578" y2="8.4582" layer="37"/>
<rectangle x1="6.7818" y1="8.4074" x2="6.8326" y2="8.4582" layer="37"/>
<rectangle x1="9.1694" y1="8.4074" x2="9.2202" y2="8.4582" layer="37"/>
<rectangle x1="7.8486" y1="8.4074" x2="7.8994" y2="8.4582" layer="37"/>
<rectangle x1="10.8458" y1="8.4074" x2="10.8966" y2="8.4582" layer="37"/>
<rectangle x1="2.921" y1="8.4582" x2="3.429" y2="8.509" layer="37"/>
<rectangle x1="4.191" y1="8.4582" x2="4.699" y2="8.509" layer="37"/>
<rectangle x1="5.207" y1="8.4582" x2="5.8166" y2="8.509" layer="37"/>
<rectangle x1="6.7818" y1="8.4582" x2="7.8994" y2="8.509" layer="37"/>
<rectangle x1="8.6106" y1="8.4582" x2="9.1694" y2="8.509" layer="37"/>
<rectangle x1="9.6266" y1="8.4582" x2="10.1854" y2="8.509" layer="37"/>
<rectangle x1="10.8966" y1="8.4582" x2="11.303" y2="8.509" layer="37"/>
<rectangle x1="5.1562" y1="8.4582" x2="5.207" y2="8.509" layer="37"/>
<rectangle x1="5.8166" y1="8.4582" x2="5.8674" y2="8.509" layer="37"/>
<rectangle x1="6.731" y1="8.4582" x2="6.7818" y2="8.509" layer="37"/>
<rectangle x1="8.5598" y1="8.4582" x2="8.6106" y2="8.509" layer="37"/>
<rectangle x1="9.1694" y1="8.4582" x2="9.2202" y2="8.509" layer="37"/>
<rectangle x1="9.2202" y1="8.4582" x2="9.271" y2="8.509" layer="37"/>
<rectangle x1="10.8458" y1="8.4582" x2="10.8966" y2="8.509" layer="37"/>
<rectangle x1="2.921" y1="8.509" x2="3.429" y2="8.5598" layer="37"/>
<rectangle x1="4.191" y1="8.509" x2="4.699" y2="8.5598" layer="37"/>
<rectangle x1="5.1562" y1="8.509" x2="5.8166" y2="8.5598" layer="37"/>
<rectangle x1="6.731" y1="8.509" x2="7.9502" y2="8.5598" layer="37"/>
<rectangle x1="8.6106" y1="8.509" x2="9.271" y2="8.5598" layer="37"/>
<rectangle x1="9.6266" y1="8.509" x2="10.1854" y2="8.5598" layer="37"/>
<rectangle x1="10.8966" y1="8.509" x2="11.303" y2="8.5598" layer="37"/>
<rectangle x1="5.1054" y1="8.509" x2="5.1562" y2="8.5598" layer="37"/>
<rectangle x1="6.6802" y1="8.509" x2="6.731" y2="8.5598" layer="37"/>
<rectangle x1="10.8458" y1="8.509" x2="10.8966" y2="8.5598" layer="37"/>
<rectangle x1="2.921" y1="8.5598" x2="3.429" y2="8.6106" layer="37"/>
<rectangle x1="4.191" y1="8.5598" x2="4.699" y2="8.6106" layer="37"/>
<rectangle x1="5.1054" y1="8.5598" x2="5.7658" y2="8.6106" layer="37"/>
<rectangle x1="6.6802" y1="8.5598" x2="7.2898" y2="8.6106" layer="37"/>
<rectangle x1="7.4422" y1="8.5598" x2="8.001" y2="8.6106" layer="37"/>
<rectangle x1="8.6614" y1="8.5598" x2="9.271" y2="8.6106" layer="37"/>
<rectangle x1="9.6266" y1="8.5598" x2="10.1854" y2="8.6106" layer="37"/>
<rectangle x1="10.8966" y1="8.5598" x2="11.303" y2="8.6106" layer="37"/>
<rectangle x1="7.2898" y1="8.5598" x2="7.3406" y2="8.6106" layer="37"/>
<rectangle x1="9.271" y1="8.5598" x2="9.3218" y2="8.6106" layer="37"/>
<rectangle x1="7.3914" y1="8.5598" x2="7.4422" y2="8.6106" layer="37"/>
<rectangle x1="6.6294" y1="8.5598" x2="6.6802" y2="8.6106" layer="37"/>
<rectangle x1="5.0546" y1="8.5598" x2="5.1054" y2="8.6106" layer="37"/>
<rectangle x1="8.6106" y1="8.5598" x2="8.6614" y2="8.6106" layer="37"/>
<rectangle x1="5.7658" y1="8.5598" x2="5.8166" y2="8.6106" layer="37"/>
<rectangle x1="7.3406" y1="8.5598" x2="7.3914" y2="8.6106" layer="37"/>
<rectangle x1="10.8458" y1="8.5598" x2="10.8966" y2="8.6106" layer="37"/>
<rectangle x1="2.921" y1="8.6106" x2="3.429" y2="8.6614" layer="37"/>
<rectangle x1="4.191" y1="8.6106" x2="4.699" y2="8.6614" layer="37"/>
<rectangle x1="5.0546" y1="8.6106" x2="5.715" y2="8.6614" layer="37"/>
<rectangle x1="6.6294" y1="8.6106" x2="7.239" y2="8.6614" layer="37"/>
<rectangle x1="7.4422" y1="8.6106" x2="8.001" y2="8.6614" layer="37"/>
<rectangle x1="8.7122" y1="8.6106" x2="9.3218" y2="8.6614" layer="37"/>
<rectangle x1="9.6266" y1="8.6106" x2="10.1854" y2="8.6614" layer="37"/>
<rectangle x1="10.8966" y1="8.6106" x2="11.303" y2="8.6614" layer="37"/>
<rectangle x1="7.239" y1="8.6106" x2="7.2898" y2="8.6614" layer="37"/>
<rectangle x1="8.6614" y1="8.6106" x2="8.7122" y2="8.6614" layer="37"/>
<rectangle x1="9.3218" y1="8.6106" x2="9.3726" y2="8.6614" layer="37"/>
<rectangle x1="5.715" y1="8.6106" x2="5.7658" y2="8.6614" layer="37"/>
<rectangle x1="8.001" y1="8.6106" x2="8.0518" y2="8.6614" layer="37"/>
<rectangle x1="6.5786" y1="8.6106" x2="6.6294" y2="8.6614" layer="37"/>
<rectangle x1="10.8458" y1="8.6106" x2="10.8966" y2="8.6614" layer="37"/>
<rectangle x1="7.2898" y1="8.6106" x2="7.3406" y2="8.6614" layer="37"/>
<rectangle x1="2.921" y1="8.6614" x2="3.429" y2="8.7122" layer="37"/>
<rectangle x1="4.191" y1="8.6614" x2="4.699" y2="8.7122" layer="37"/>
<rectangle x1="5.0546" y1="8.6614" x2="5.6642" y2="8.7122" layer="37"/>
<rectangle x1="6.5786" y1="8.6614" x2="7.239" y2="8.7122" layer="37"/>
<rectangle x1="7.493" y1="8.6614" x2="8.0518" y2="8.7122" layer="37"/>
<rectangle x1="8.763" y1="8.6614" x2="9.3726" y2="8.7122" layer="37"/>
<rectangle x1="9.6266" y1="8.6614" x2="10.1854" y2="8.7122" layer="37"/>
<rectangle x1="10.8966" y1="8.6614" x2="11.303" y2="8.7122" layer="37"/>
<rectangle x1="5.0038" y1="8.6614" x2="5.0546" y2="8.7122" layer="37"/>
<rectangle x1="8.7122" y1="8.6614" x2="8.763" y2="8.7122" layer="37"/>
<rectangle x1="5.6642" y1="8.6614" x2="5.715" y2="8.7122" layer="37"/>
<rectangle x1="7.239" y1="8.6614" x2="7.2898" y2="8.7122" layer="37"/>
<rectangle x1="9.3726" y1="8.6614" x2="9.4234" y2="8.7122" layer="37"/>
<rectangle x1="10.8458" y1="8.6614" x2="10.8966" y2="8.7122" layer="37"/>
<rectangle x1="2.921" y1="8.7122" x2="3.429" y2="8.763" layer="37"/>
<rectangle x1="4.191" y1="8.7122" x2="4.699" y2="8.763" layer="37"/>
<rectangle x1="5.0038" y1="8.7122" x2="5.6134" y2="8.763" layer="37"/>
<rectangle x1="6.5278" y1="8.7122" x2="7.1882" y2="8.763" layer="37"/>
<rectangle x1="7.5438" y1="8.7122" x2="8.1026" y2="8.763" layer="37"/>
<rectangle x1="8.763" y1="8.7122" x2="9.4234" y2="8.763" layer="37"/>
<rectangle x1="9.6266" y1="8.7122" x2="10.1854" y2="8.763" layer="37"/>
<rectangle x1="10.8966" y1="8.7122" x2="11.303" y2="8.763" layer="37"/>
<rectangle x1="4.953" y1="8.7122" x2="5.0038" y2="8.763" layer="37"/>
<rectangle x1="5.6134" y1="8.7122" x2="5.6642" y2="8.763" layer="37"/>
<rectangle x1="7.1882" y1="8.7122" x2="7.239" y2="8.763" layer="37"/>
<rectangle x1="7.493" y1="8.7122" x2="7.5438" y2="8.763" layer="37"/>
<rectangle x1="10.8458" y1="8.7122" x2="10.8966" y2="8.763" layer="37"/>
<rectangle x1="2.921" y1="8.763" x2="3.429" y2="8.8138" layer="37"/>
<rectangle x1="4.191" y1="8.763" x2="4.699" y2="8.8138" layer="37"/>
<rectangle x1="4.953" y1="8.763" x2="5.6134" y2="8.8138" layer="37"/>
<rectangle x1="6.5278" y1="8.763" x2="7.1374" y2="8.8138" layer="37"/>
<rectangle x1="7.5946" y1="8.763" x2="8.1026" y2="8.8138" layer="37"/>
<rectangle x1="8.8138" y1="8.763" x2="9.4234" y2="8.8138" layer="37"/>
<rectangle x1="9.6266" y1="8.763" x2="10.1854" y2="8.8138" layer="37"/>
<rectangle x1="10.8966" y1="8.763" x2="11.303" y2="8.8138" layer="37"/>
<rectangle x1="6.477" y1="8.763" x2="6.5278" y2="8.8138" layer="37"/>
<rectangle x1="7.1374" y1="8.763" x2="7.1882" y2="8.8138" layer="37"/>
<rectangle x1="7.5438" y1="8.763" x2="7.5946" y2="8.8138" layer="37"/>
<rectangle x1="9.4234" y1="8.763" x2="9.4742" y2="8.8138" layer="37"/>
<rectangle x1="4.9022" y1="8.763" x2="4.953" y2="8.8138" layer="37"/>
<rectangle x1="8.1026" y1="8.763" x2="8.1534" y2="8.8138" layer="37"/>
<rectangle x1="10.8458" y1="8.763" x2="10.8966" y2="8.8138" layer="37"/>
<rectangle x1="8.763" y1="8.763" x2="8.8138" y2="8.8138" layer="37"/>
<rectangle x1="2.921" y1="8.8138" x2="3.429" y2="8.8646" layer="37"/>
<rectangle x1="4.191" y1="8.8138" x2="4.699" y2="8.8646" layer="37"/>
<rectangle x1="4.9022" y1="8.8138" x2="5.5626" y2="8.8646" layer="37"/>
<rectangle x1="6.477" y1="8.8138" x2="7.0866" y2="8.8646" layer="37"/>
<rectangle x1="7.5946" y1="8.8138" x2="8.1534" y2="8.8646" layer="37"/>
<rectangle x1="8.8646" y1="8.8138" x2="9.4742" y2="8.8646" layer="37"/>
<rectangle x1="9.6266" y1="8.8138" x2="10.1854" y2="8.8646" layer="37"/>
<rectangle x1="10.8966" y1="8.8138" x2="11.303" y2="8.8646" layer="37"/>
<rectangle x1="7.0866" y1="8.8138" x2="7.1374" y2="8.8646" layer="37"/>
<rectangle x1="6.4262" y1="8.8138" x2="6.477" y2="8.8646" layer="37"/>
<rectangle x1="9.4742" y1="8.8138" x2="9.525" y2="8.8646" layer="37"/>
<rectangle x1="4.8514" y1="8.8138" x2="4.9022" y2="8.8646" layer="37"/>
<rectangle x1="8.8138" y1="8.8138" x2="8.8646" y2="8.8646" layer="37"/>
<rectangle x1="10.8458" y1="8.8138" x2="10.8966" y2="8.8646" layer="37"/>
<rectangle x1="2.921" y1="8.8646" x2="3.429" y2="8.9154" layer="37"/>
<rectangle x1="4.191" y1="8.8646" x2="4.699" y2="8.9154" layer="37"/>
<rectangle x1="4.8514" y1="8.8646" x2="5.5118" y2="8.9154" layer="37"/>
<rectangle x1="6.4262" y1="8.8646" x2="7.0866" y2="8.9154" layer="37"/>
<rectangle x1="7.6454" y1="8.8646" x2="8.1534" y2="8.9154" layer="37"/>
<rectangle x1="8.9154" y1="8.8646" x2="9.525" y2="8.9154" layer="37"/>
<rectangle x1="9.6266" y1="8.8646" x2="10.1854" y2="8.9154" layer="37"/>
<rectangle x1="10.8966" y1="8.8646" x2="11.303" y2="8.9154" layer="37"/>
<rectangle x1="8.1534" y1="8.8646" x2="8.2042" y2="8.9154" layer="37"/>
<rectangle x1="8.8646" y1="8.8646" x2="8.9154" y2="8.9154" layer="37"/>
<rectangle x1="6.3754" y1="8.8646" x2="6.4262" y2="8.9154" layer="37"/>
<rectangle x1="9.525" y1="8.8646" x2="9.5758" y2="8.9154" layer="37"/>
<rectangle x1="7.5946" y1="8.8646" x2="7.6454" y2="8.9154" layer="37"/>
<rectangle x1="4.8006" y1="8.8646" x2="4.8514" y2="8.9154" layer="37"/>
<rectangle x1="10.8458" y1="8.8646" x2="10.8966" y2="8.9154" layer="37"/>
<rectangle x1="5.5118" y1="8.8646" x2="5.5626" y2="8.9154" layer="37"/>
<rectangle x1="2.921" y1="8.9154" x2="3.429" y2="8.9662" layer="37"/>
<rectangle x1="4.191" y1="8.9154" x2="4.699" y2="8.9662" layer="37"/>
<rectangle x1="4.8006" y1="8.9154" x2="5.461" y2="8.9662" layer="37"/>
<rectangle x1="6.3754" y1="8.9154" x2="7.0358" y2="8.9662" layer="37"/>
<rectangle x1="7.6962" y1="8.9154" x2="8.2042" y2="8.9662" layer="37"/>
<rectangle x1="8.9662" y1="8.9154" x2="10.1854" y2="8.9662" layer="37"/>
<rectangle x1="10.8966" y1="8.9154" x2="11.303" y2="8.9662" layer="37"/>
<rectangle x1="8.9154" y1="8.9154" x2="8.9662" y2="8.9662" layer="37"/>
<rectangle x1="7.6454" y1="8.9154" x2="7.6962" y2="8.9662" layer="37"/>
<rectangle x1="8.2042" y1="8.9154" x2="8.255" y2="8.9662" layer="37"/>
<rectangle x1="5.461" y1="8.9154" x2="5.5118" y2="8.9662" layer="37"/>
<rectangle x1="7.0358" y1="8.9154" x2="7.0866" y2="8.9662" layer="37"/>
<rectangle x1="10.8458" y1="8.9154" x2="10.8966" y2="8.9662" layer="37"/>
<rectangle x1="6.3246" y1="8.9154" x2="6.3754" y2="8.9662" layer="37"/>
<rectangle x1="2.921" y1="8.9662" x2="3.429" y2="9.017" layer="37"/>
<rectangle x1="4.191" y1="8.9662" x2="5.4102" y2="9.017" layer="37"/>
<rectangle x1="6.3246" y1="8.9662" x2="6.985" y2="9.017" layer="37"/>
<rectangle x1="7.6962" y1="8.9662" x2="8.255" y2="9.017" layer="37"/>
<rectangle x1="8.9662" y1="8.9662" x2="10.1854" y2="9.017" layer="37"/>
<rectangle x1="10.8966" y1="8.9662" x2="11.303" y2="9.017" layer="37"/>
<rectangle x1="5.4102" y1="8.9662" x2="5.461" y2="9.017" layer="37"/>
<rectangle x1="6.985" y1="8.9662" x2="7.0358" y2="9.017" layer="37"/>
<rectangle x1="10.8458" y1="8.9662" x2="10.8966" y2="9.017" layer="37"/>
<rectangle x1="8.255" y1="8.9662" x2="8.3058" y2="9.017" layer="37"/>
<rectangle x1="2.921" y1="9.017" x2="3.429" y2="9.0678" layer="37"/>
<rectangle x1="4.191" y1="9.017" x2="5.3594" y2="9.0678" layer="37"/>
<rectangle x1="6.2738" y1="9.017" x2="6.9342" y2="9.0678" layer="37"/>
<rectangle x1="7.747" y1="9.017" x2="8.255" y2="9.0678" layer="37"/>
<rectangle x1="9.017" y1="9.017" x2="10.1854" y2="9.0678" layer="37"/>
<rectangle x1="10.8966" y1="9.017" x2="11.303" y2="9.0678" layer="37"/>
<rectangle x1="8.255" y1="9.017" x2="8.3058" y2="9.0678" layer="37"/>
<rectangle x1="5.3594" y1="9.017" x2="5.4102" y2="9.0678" layer="37"/>
<rectangle x1="6.9342" y1="9.017" x2="6.985" y2="9.0678" layer="37"/>
<rectangle x1="8.9662" y1="9.017" x2="9.017" y2="9.0678" layer="37"/>
<rectangle x1="10.8458" y1="9.017" x2="10.8966" y2="9.0678" layer="37"/>
<rectangle x1="2.921" y1="9.0678" x2="3.429" y2="9.1186" layer="37"/>
<rectangle x1="4.191" y1="9.0678" x2="5.3594" y2="9.1186" layer="37"/>
<rectangle x1="6.2738" y1="9.0678" x2="6.9342" y2="9.1186" layer="37"/>
<rectangle x1="7.7978" y1="9.0678" x2="8.3058" y2="9.1186" layer="37"/>
<rectangle x1="9.0678" y1="9.0678" x2="10.1854" y2="9.1186" layer="37"/>
<rectangle x1="10.8966" y1="9.0678" x2="11.303" y2="9.1186" layer="37"/>
<rectangle x1="6.223" y1="9.0678" x2="6.2738" y2="9.1186" layer="37"/>
<rectangle x1="9.017" y1="9.0678" x2="9.0678" y2="9.1186" layer="37"/>
<rectangle x1="8.3058" y1="9.0678" x2="8.3566" y2="9.1186" layer="37"/>
<rectangle x1="7.747" y1="9.0678" x2="7.7978" y2="9.1186" layer="37"/>
<rectangle x1="10.8458" y1="9.0678" x2="10.8966" y2="9.1186" layer="37"/>
<rectangle x1="2.921" y1="9.1186" x2="3.429" y2="9.1694" layer="37"/>
<rectangle x1="4.191" y1="9.1186" x2="5.3086" y2="9.1694" layer="37"/>
<rectangle x1="6.223" y1="9.1186" x2="6.8834" y2="9.1694" layer="37"/>
<rectangle x1="7.8486" y1="9.1186" x2="8.3566" y2="9.1694" layer="37"/>
<rectangle x1="9.1186" y1="9.1186" x2="10.1854" y2="9.1694" layer="37"/>
<rectangle x1="10.8966" y1="9.1186" x2="11.303" y2="9.1694" layer="37"/>
<rectangle x1="7.7978" y1="9.1186" x2="7.8486" y2="9.1694" layer="37"/>
<rectangle x1="9.0678" y1="9.1186" x2="9.1186" y2="9.1694" layer="37"/>
<rectangle x1="6.1722" y1="9.1186" x2="6.223" y2="9.1694" layer="37"/>
<rectangle x1="8.3566" y1="9.1186" x2="8.4074" y2="9.1694" layer="37"/>
<rectangle x1="10.8458" y1="9.1186" x2="10.8966" y2="9.1694" layer="37"/>
<rectangle x1="2.921" y1="9.1694" x2="3.429" y2="9.2202" layer="37"/>
<rectangle x1="4.191" y1="9.1694" x2="5.2578" y2="9.2202" layer="37"/>
<rectangle x1="6.1722" y1="9.1694" x2="6.8326" y2="9.2202" layer="37"/>
<rectangle x1="7.8486" y1="9.1694" x2="8.3566" y2="9.2202" layer="37"/>
<rectangle x1="9.1186" y1="9.1694" x2="10.1854" y2="9.2202" layer="37"/>
<rectangle x1="10.8966" y1="9.1694" x2="11.303" y2="9.2202" layer="37"/>
<rectangle x1="8.3566" y1="9.1694" x2="8.4074" y2="9.2202" layer="37"/>
<rectangle x1="6.1214" y1="9.1694" x2="6.1722" y2="9.2202" layer="37"/>
<rectangle x1="5.2578" y1="9.1694" x2="5.3086" y2="9.2202" layer="37"/>
<rectangle x1="10.8458" y1="9.1694" x2="10.8966" y2="9.2202" layer="37"/>
<rectangle x1="2.921" y1="9.2202" x2="3.429" y2="9.271" layer="37"/>
<rectangle x1="4.191" y1="9.2202" x2="5.207" y2="9.271" layer="37"/>
<rectangle x1="6.1214" y1="9.2202" x2="6.7818" y2="9.271" layer="37"/>
<rectangle x1="7.8994" y1="9.2202" x2="8.4074" y2="9.271" layer="37"/>
<rectangle x1="9.1694" y1="9.2202" x2="10.1854" y2="9.271" layer="37"/>
<rectangle x1="10.8966" y1="9.2202" x2="11.303" y2="9.271" layer="37"/>
<rectangle x1="8.4074" y1="9.2202" x2="8.4582" y2="9.271" layer="37"/>
<rectangle x1="5.207" y1="9.2202" x2="5.2578" y2="9.271" layer="37"/>
<rectangle x1="7.8486" y1="9.2202" x2="7.8994" y2="9.271" layer="37"/>
<rectangle x1="9.1186" y1="9.2202" x2="9.1694" y2="9.271" layer="37"/>
<rectangle x1="6.0706" y1="9.2202" x2="6.1214" y2="9.271" layer="37"/>
<rectangle x1="6.7818" y1="9.2202" x2="6.8326" y2="9.271" layer="37"/>
<rectangle x1="10.8458" y1="9.2202" x2="10.8966" y2="9.271" layer="37"/>
<rectangle x1="2.921" y1="9.271" x2="3.429" y2="9.3218" layer="37"/>
<rectangle x1="4.191" y1="9.271" x2="5.1562" y2="9.3218" layer="37"/>
<rectangle x1="6.0706" y1="9.271" x2="6.731" y2="9.3218" layer="37"/>
<rectangle x1="7.9502" y1="9.271" x2="8.4582" y2="9.3218" layer="37"/>
<rectangle x1="9.2202" y1="9.271" x2="10.1854" y2="9.3218" layer="37"/>
<rectangle x1="10.8966" y1="9.271" x2="11.303" y2="9.3218" layer="37"/>
<rectangle x1="9.1694" y1="9.271" x2="9.2202" y2="9.3218" layer="37"/>
<rectangle x1="5.1562" y1="9.271" x2="5.207" y2="9.3218" layer="37"/>
<rectangle x1="7.8994" y1="9.271" x2="7.9502" y2="9.3218" layer="37"/>
<rectangle x1="6.731" y1="9.271" x2="6.7818" y2="9.3218" layer="37"/>
<rectangle x1="8.4582" y1="9.271" x2="8.509" y2="9.3218" layer="37"/>
<rectangle x1="10.8458" y1="9.271" x2="10.8966" y2="9.3218" layer="37"/>
<rectangle x1="2.921" y1="9.3218" x2="3.429" y2="9.3726" layer="37"/>
<rectangle x1="4.191" y1="9.3218" x2="5.1054" y2="9.3726" layer="37"/>
<rectangle x1="6.0706" y1="9.3218" x2="6.6802" y2="9.3726" layer="37"/>
<rectangle x1="7.9502" y1="9.3218" x2="8.509" y2="9.3726" layer="37"/>
<rectangle x1="9.2202" y1="9.3218" x2="10.1854" y2="9.3726" layer="37"/>
<rectangle x1="10.8966" y1="9.3218" x2="11.303" y2="9.3726" layer="37"/>
<rectangle x1="5.1054" y1="9.3218" x2="5.1562" y2="9.3726" layer="37"/>
<rectangle x1="6.0198" y1="9.3218" x2="6.0706" y2="9.3726" layer="37"/>
<rectangle x1="6.6802" y1="9.3218" x2="6.731" y2="9.3726" layer="37"/>
<rectangle x1="10.8458" y1="9.3218" x2="10.8966" y2="9.3726" layer="37"/>
<rectangle x1="2.921" y1="9.3726" x2="3.429" y2="9.4234" layer="37"/>
<rectangle x1="4.191" y1="9.3726" x2="5.1054" y2="9.4234" layer="37"/>
<rectangle x1="6.0198" y1="9.3726" x2="6.6802" y2="9.4234" layer="37"/>
<rectangle x1="8.001" y1="9.3726" x2="8.509" y2="9.4234" layer="37"/>
<rectangle x1="9.271" y1="9.3726" x2="10.1854" y2="9.4234" layer="37"/>
<rectangle x1="10.8966" y1="9.3726" x2="11.303" y2="9.4234" layer="37"/>
<rectangle x1="8.509" y1="9.3726" x2="8.5598" y2="9.4234" layer="37"/>
<rectangle x1="5.969" y1="9.3726" x2="6.0198" y2="9.4234" layer="37"/>
<rectangle x1="7.9502" y1="9.3726" x2="8.001" y2="9.4234" layer="37"/>
<rectangle x1="10.8458" y1="9.3726" x2="10.8966" y2="9.4234" layer="37"/>
<rectangle x1="2.921" y1="9.4234" x2="3.429" y2="9.4742" layer="37"/>
<rectangle x1="4.191" y1="9.4234" x2="5.0546" y2="9.4742" layer="37"/>
<rectangle x1="5.969" y1="9.4234" x2="6.6294" y2="9.4742" layer="37"/>
<rectangle x1="8.0518" y1="9.4234" x2="8.5598" y2="9.4742" layer="37"/>
<rectangle x1="9.3218" y1="9.4234" x2="10.1854" y2="9.4742" layer="37"/>
<rectangle x1="10.8966" y1="9.4234" x2="11.303" y2="9.4742" layer="37"/>
<rectangle x1="8.001" y1="9.4234" x2="8.0518" y2="9.4742" layer="37"/>
<rectangle x1="8.5598" y1="9.4234" x2="8.6106" y2="9.4742" layer="37"/>
<rectangle x1="5.9182" y1="9.4234" x2="5.969" y2="9.4742" layer="37"/>
<rectangle x1="9.271" y1="9.4234" x2="9.3218" y2="9.4742" layer="37"/>
<rectangle x1="10.8458" y1="9.4234" x2="10.8966" y2="9.4742" layer="37"/>
<rectangle x1="5.0546" y1="9.4234" x2="5.1054" y2="9.4742" layer="37"/>
<rectangle x1="2.921" y1="9.4742" x2="3.429" y2="9.525" layer="37"/>
<rectangle x1="4.191" y1="9.4742" x2="5.0038" y2="9.525" layer="37"/>
<rectangle x1="5.9182" y1="9.4742" x2="6.5786" y2="9.525" layer="37"/>
<rectangle x1="8.1026" y1="9.4742" x2="8.6106" y2="9.525" layer="37"/>
<rectangle x1="9.3726" y1="9.4742" x2="10.1854" y2="9.525" layer="37"/>
<rectangle x1="10.8966" y1="9.4742" x2="11.303" y2="9.525" layer="37"/>
<rectangle x1="8.0518" y1="9.4742" x2="8.1026" y2="9.525" layer="37"/>
<rectangle x1="9.3218" y1="9.4742" x2="9.3726" y2="9.525" layer="37"/>
<rectangle x1="5.0038" y1="9.4742" x2="5.0546" y2="9.525" layer="37"/>
<rectangle x1="5.8674" y1="9.4742" x2="5.9182" y2="9.525" layer="37"/>
<rectangle x1="8.6106" y1="9.4742" x2="8.6614" y2="9.525" layer="37"/>
<rectangle x1="10.8458" y1="9.4742" x2="10.8966" y2="9.525" layer="37"/>
<rectangle x1="2.921" y1="9.525" x2="3.429" y2="9.5758" layer="37"/>
<rectangle x1="4.191" y1="9.525" x2="4.953" y2="9.5758" layer="37"/>
<rectangle x1="5.8674" y1="9.525" x2="6.5278" y2="9.5758" layer="37"/>
<rectangle x1="8.1534" y1="9.525" x2="8.6614" y2="9.5758" layer="37"/>
<rectangle x1="9.4234" y1="9.525" x2="10.1854" y2="9.5758" layer="37"/>
<rectangle x1="10.8966" y1="9.525" x2="11.303" y2="9.5758" layer="37"/>
<rectangle x1="8.1026" y1="9.525" x2="8.1534" y2="9.5758" layer="37"/>
<rectangle x1="9.3726" y1="9.525" x2="9.4234" y2="9.5758" layer="37"/>
<rectangle x1="4.953" y1="9.525" x2="5.0038" y2="9.5758" layer="37"/>
<rectangle x1="6.5278" y1="9.525" x2="6.5786" y2="9.5758" layer="37"/>
<rectangle x1="10.8458" y1="9.525" x2="10.8966" y2="9.5758" layer="37"/>
<rectangle x1="5.8166" y1="9.525" x2="5.8674" y2="9.5758" layer="37"/>
<rectangle x1="2.921" y1="9.5758" x2="3.429" y2="9.6266" layer="37"/>
<rectangle x1="4.191" y1="9.5758" x2="4.9022" y2="9.6266" layer="37"/>
<rectangle x1="5.8166" y1="9.5758" x2="6.477" y2="9.6266" layer="37"/>
<rectangle x1="8.1534" y1="9.5758" x2="8.7122" y2="9.6266" layer="37"/>
<rectangle x1="9.4234" y1="9.5758" x2="10.1854" y2="9.6266" layer="37"/>
<rectangle x1="10.8966" y1="9.5758" x2="11.303" y2="9.6266" layer="37"/>
<rectangle x1="4.9022" y1="9.5758" x2="4.953" y2="9.6266" layer="37"/>
<rectangle x1="8.1026" y1="9.5758" x2="8.1534" y2="9.6266" layer="37"/>
<rectangle x1="6.477" y1="9.5758" x2="6.5278" y2="9.6266" layer="37"/>
<rectangle x1="10.8458" y1="9.5758" x2="10.8966" y2="9.6266" layer="37"/>
<rectangle x1="2.921" y1="9.6266" x2="3.429" y2="9.6774" layer="37"/>
<rectangle x1="4.191" y1="9.6266" x2="4.9022" y2="9.6774" layer="37"/>
<rectangle x1="5.8166" y1="9.6266" x2="6.4262" y2="9.6774" layer="37"/>
<rectangle x1="8.2042" y1="9.6266" x2="8.7122" y2="9.6774" layer="37"/>
<rectangle x1="9.4742" y1="9.6266" x2="10.1854" y2="9.6774" layer="37"/>
<rectangle x1="10.8966" y1="9.6266" x2="11.303" y2="9.6774" layer="37"/>
<rectangle x1="5.7658" y1="9.6266" x2="5.8166" y2="9.6774" layer="37"/>
<rectangle x1="8.1534" y1="9.6266" x2="8.2042" y2="9.6774" layer="37"/>
<rectangle x1="6.4262" y1="9.6266" x2="6.477" y2="9.6774" layer="37"/>
<rectangle x1="8.7122" y1="9.6266" x2="8.763" y2="9.6774" layer="37"/>
<rectangle x1="10.8458" y1="9.6266" x2="10.8966" y2="9.6774" layer="37"/>
<rectangle x1="2.921" y1="9.6774" x2="3.429" y2="9.7282" layer="37"/>
<rectangle x1="4.191" y1="9.6774" x2="4.8514" y2="9.7282" layer="37"/>
<rectangle x1="5.7658" y1="9.6774" x2="6.3754" y2="9.7282" layer="37"/>
<rectangle x1="8.2042" y1="9.6774" x2="8.763" y2="9.7282" layer="37"/>
<rectangle x1="9.525" y1="9.6774" x2="10.1854" y2="9.7282" layer="37"/>
<rectangle x1="10.8966" y1="9.6774" x2="11.303" y2="9.7282" layer="37"/>
<rectangle x1="6.3754" y1="9.6774" x2="6.4262" y2="9.7282" layer="37"/>
<rectangle x1="5.715" y1="9.6774" x2="5.7658" y2="9.7282" layer="37"/>
<rectangle x1="9.4742" y1="9.6774" x2="9.525" y2="9.7282" layer="37"/>
<rectangle x1="10.8458" y1="9.6774" x2="10.8966" y2="9.7282" layer="37"/>
<rectangle x1="2.921" y1="9.7282" x2="3.429" y2="9.779" layer="37"/>
<rectangle x1="4.191" y1="9.7282" x2="4.8006" y2="9.779" layer="37"/>
<rectangle x1="5.715" y1="9.7282" x2="6.3246" y2="9.779" layer="37"/>
<rectangle x1="8.255" y1="9.7282" x2="8.8138" y2="9.779" layer="37"/>
<rectangle x1="9.5758" y1="9.7282" x2="10.1854" y2="9.779" layer="37"/>
<rectangle x1="10.8966" y1="9.7282" x2="11.303" y2="9.779" layer="37"/>
<rectangle x1="6.3246" y1="9.7282" x2="6.3754" y2="9.779" layer="37"/>
<rectangle x1="9.525" y1="9.7282" x2="9.5758" y2="9.779" layer="37"/>
<rectangle x1="5.6642" y1="9.7282" x2="5.715" y2="9.779" layer="37"/>
<rectangle x1="8.2042" y1="9.7282" x2="8.255" y2="9.779" layer="37"/>
<rectangle x1="10.8458" y1="9.7282" x2="10.8966" y2="9.779" layer="37"/>
<rectangle x1="4.8006" y1="9.7282" x2="4.8514" y2="9.779" layer="37"/>
<rectangle x1="2.921" y1="9.779" x2="3.429" y2="9.8298" layer="37"/>
<rectangle x1="4.191" y1="9.779" x2="4.7498" y2="9.8298" layer="37"/>
<rectangle x1="5.6642" y1="9.779" x2="6.3246" y2="9.8298" layer="37"/>
<rectangle x1="8.3058" y1="9.779" x2="8.8138" y2="9.8298" layer="37"/>
<rectangle x1="9.6266" y1="9.779" x2="10.1346" y2="9.8298" layer="37"/>
<rectangle x1="10.8966" y1="9.779" x2="11.303" y2="9.8298" layer="37"/>
<rectangle x1="9.5758" y1="9.779" x2="9.6266" y2="9.8298" layer="37"/>
<rectangle x1="8.255" y1="9.779" x2="8.3058" y2="9.8298" layer="37"/>
<rectangle x1="10.1346" y1="9.779" x2="10.1854" y2="9.8298" layer="37"/>
<rectangle x1="8.8138" y1="9.779" x2="8.8646" y2="9.8298" layer="37"/>
<rectangle x1="4.7498" y1="9.779" x2="4.8006" y2="9.8298" layer="37"/>
<rectangle x1="5.6134" y1="9.779" x2="5.6642" y2="9.8298" layer="37"/>
<rectangle x1="10.8458" y1="9.779" x2="10.8966" y2="9.8298" layer="37"/>
<rectangle x1="2.921" y1="9.8298" x2="3.429" y2="9.8806" layer="37"/>
<rectangle x1="4.191" y1="9.8298" x2="4.699" y2="9.8806" layer="37"/>
<rectangle x1="5.6134" y1="9.8298" x2="6.2738" y2="9.8806" layer="37"/>
<rectangle x1="8.3058" y1="9.8298" x2="8.8646" y2="9.8806" layer="37"/>
<rectangle x1="9.6266" y1="9.8298" x2="10.1346" y2="9.8806" layer="37"/>
<rectangle x1="10.8966" y1="9.8298" x2="11.303" y2="9.8806" layer="37"/>
<rectangle x1="4.699" y1="9.8298" x2="4.7498" y2="9.8806" layer="37"/>
<rectangle x1="6.2738" y1="9.8298" x2="6.3246" y2="9.8806" layer="37"/>
<rectangle x1="10.8458" y1="9.8298" x2="10.8966" y2="9.8806" layer="37"/>
<rectangle x1="8.8646" y1="9.8298" x2="8.9154" y2="9.8806" layer="37"/>
<rectangle x1="2.921" y1="9.8806" x2="3.429" y2="9.9314" layer="37"/>
<rectangle x1="4.2418" y1="9.8806" x2="4.6482" y2="9.9314" layer="37"/>
<rectangle x1="5.5626" y1="9.8806" x2="6.223" y2="9.9314" layer="37"/>
<rectangle x1="8.3566" y1="9.8806" x2="8.9154" y2="9.9314" layer="37"/>
<rectangle x1="9.6774" y1="9.8806" x2="10.1346" y2="9.9314" layer="37"/>
<rectangle x1="10.8966" y1="9.8806" x2="11.303" y2="9.9314" layer="37"/>
<rectangle x1="4.6482" y1="9.8806" x2="4.699" y2="9.9314" layer="37"/>
<rectangle x1="4.191" y1="9.8806" x2="4.2418" y2="9.9314" layer="37"/>
<rectangle x1="6.223" y1="9.8806" x2="6.2738" y2="9.9314" layer="37"/>
<rectangle x1="9.6266" y1="9.8806" x2="9.6774" y2="9.9314" layer="37"/>
<rectangle x1="10.8458" y1="9.8806" x2="10.8966" y2="9.9314" layer="37"/>
<rectangle x1="2.921" y1="9.9314" x2="3.429" y2="9.9822" layer="37"/>
<rectangle x1="4.2418" y1="9.9314" x2="4.5974" y2="9.9822" layer="37"/>
<rectangle x1="5.5626" y1="9.9314" x2="6.1722" y2="9.9822" layer="37"/>
<rectangle x1="8.4074" y1="9.9314" x2="8.9154" y2="9.9822" layer="37"/>
<rectangle x1="9.7282" y1="9.9314" x2="10.0838" y2="9.9822" layer="37"/>
<rectangle x1="10.8966" y1="9.9314" x2="11.303" y2="9.9822" layer="37"/>
<rectangle x1="4.5974" y1="9.9314" x2="4.6482" y2="9.9822" layer="37"/>
<rectangle x1="5.5118" y1="9.9314" x2="5.5626" y2="9.9822" layer="37"/>
<rectangle x1="8.9154" y1="9.9314" x2="8.9662" y2="9.9822" layer="37"/>
<rectangle x1="6.1722" y1="9.9314" x2="6.223" y2="9.9822" layer="37"/>
<rectangle x1="8.3566" y1="9.9314" x2="8.4074" y2="9.9822" layer="37"/>
<rectangle x1="9.6774" y1="9.9314" x2="9.7282" y2="9.9822" layer="37"/>
<rectangle x1="10.8458" y1="9.9314" x2="10.8966" y2="9.9822" layer="37"/>
<rectangle x1="10.0838" y1="9.9314" x2="10.1346" y2="9.9822" layer="37"/>
<rectangle x1="2.921" y1="9.9822" x2="3.429" y2="10.033" layer="37"/>
<rectangle x1="4.3434" y1="9.9822" x2="4.5466" y2="10.033" layer="37"/>
<rectangle x1="5.5118" y1="9.9822" x2="6.1214" y2="10.033" layer="37"/>
<rectangle x1="8.4074" y1="9.9822" x2="8.9662" y2="10.033" layer="37"/>
<rectangle x1="9.779" y1="9.9822" x2="9.9822" y2="10.033" layer="37"/>
<rectangle x1="10.8966" y1="9.9822" x2="11.303" y2="10.033" layer="37"/>
<rectangle x1="6.1214" y1="9.9822" x2="6.1722" y2="10.033" layer="37"/>
<rectangle x1="9.9822" y1="9.9822" x2="10.033" y2="10.033" layer="37"/>
<rectangle x1="4.5466" y1="9.9822" x2="4.5974" y2="10.033" layer="37"/>
<rectangle x1="4.2926" y1="9.9822" x2="4.3434" y2="10.033" layer="37"/>
<rectangle x1="5.461" y1="9.9822" x2="5.5118" y2="10.033" layer="37"/>
<rectangle x1="8.9662" y1="9.9822" x2="9.017" y2="10.033" layer="37"/>
<rectangle x1="10.8458" y1="9.9822" x2="10.8966" y2="10.033" layer="37"/>
<rectangle x1="9.7282" y1="9.9822" x2="9.779" y2="10.033" layer="37"/>
<rectangle x1="2.921" y1="10.033" x2="3.429" y2="10.0838" layer="37"/>
<rectangle x1="5.461" y1="10.033" x2="6.0706" y2="10.0838" layer="37"/>
<rectangle x1="8.4582" y1="10.033" x2="9.017" y2="10.0838" layer="37"/>
<rectangle x1="10.8966" y1="10.033" x2="11.303" y2="10.0838" layer="37"/>
<rectangle x1="6.0706" y1="10.033" x2="6.1214" y2="10.0838" layer="37"/>
<rectangle x1="5.4102" y1="10.033" x2="5.461" y2="10.0838" layer="37"/>
<rectangle x1="10.8458" y1="10.033" x2="10.8966" y2="10.0838" layer="37"/>
<rectangle x1="2.921" y1="10.0838" x2="3.429" y2="10.1346" layer="37"/>
<rectangle x1="5.4102" y1="10.0838" x2="6.0706" y2="10.1346" layer="37"/>
<rectangle x1="8.509" y1="10.0838" x2="9.0678" y2="10.1346" layer="37"/>
<rectangle x1="10.8966" y1="10.0838" x2="11.303" y2="10.1346" layer="37"/>
<rectangle x1="8.4582" y1="10.0838" x2="8.509" y2="10.1346" layer="37"/>
<rectangle x1="10.8458" y1="10.0838" x2="10.8966" y2="10.1346" layer="37"/>
<rectangle x1="6.0706" y1="10.0838" x2="6.1214" y2="10.1346" layer="37"/>
<rectangle x1="2.921" y1="10.1346" x2="3.429" y2="10.1854" layer="37"/>
<rectangle x1="5.3594" y1="10.1346" x2="6.0198" y2="10.1854" layer="37"/>
<rectangle x1="8.5598" y1="10.1346" x2="9.0678" y2="10.1854" layer="37"/>
<rectangle x1="10.8966" y1="10.1346" x2="11.303" y2="10.1854" layer="37"/>
<rectangle x1="8.509" y1="10.1346" x2="8.5598" y2="10.1854" layer="37"/>
<rectangle x1="9.0678" y1="10.1346" x2="9.1186" y2="10.1854" layer="37"/>
<rectangle x1="6.0198" y1="10.1346" x2="6.0706" y2="10.1854" layer="37"/>
<rectangle x1="10.8458" y1="10.1346" x2="10.8966" y2="10.1854" layer="37"/>
<rectangle x1="2.921" y1="10.1854" x2="3.429" y2="10.2362" layer="37"/>
<rectangle x1="5.3086" y1="10.1854" x2="5.969" y2="10.2362" layer="37"/>
<rectangle x1="8.5598" y1="10.1854" x2="9.1186" y2="10.2362" layer="37"/>
<rectangle x1="10.8966" y1="10.1854" x2="11.303" y2="10.2362" layer="37"/>
<rectangle x1="5.969" y1="10.1854" x2="6.0198" y2="10.2362" layer="37"/>
<rectangle x1="10.8458" y1="10.1854" x2="10.8966" y2="10.2362" layer="37"/>
<rectangle x1="9.1186" y1="10.1854" x2="9.1694" y2="10.2362" layer="37"/>
<rectangle x1="2.921" y1="10.2362" x2="3.429" y2="10.287" layer="37"/>
<rectangle x1="5.3086" y1="10.2362" x2="5.969" y2="10.287" layer="37"/>
<rectangle x1="8.6106" y1="10.2362" x2="9.1694" y2="10.287" layer="37"/>
<rectangle x1="10.8966" y1="10.2362" x2="11.303" y2="10.287" layer="37"/>
<rectangle x1="5.2578" y1="10.2362" x2="5.3086" y2="10.287" layer="37"/>
<rectangle x1="10.8458" y1="10.2362" x2="10.8966" y2="10.287" layer="37"/>
<rectangle x1="2.921" y1="10.287" x2="3.429" y2="10.3378" layer="37"/>
<rectangle x1="5.2578" y1="10.287" x2="5.9182" y2="10.3378" layer="37"/>
<rectangle x1="8.6614" y1="10.287" x2="9.1694" y2="10.3378" layer="37"/>
<rectangle x1="10.8966" y1="10.287" x2="11.303" y2="10.3378" layer="37"/>
<rectangle x1="9.1694" y1="10.287" x2="9.2202" y2="10.3378" layer="37"/>
<rectangle x1="8.6106" y1="10.287" x2="8.6614" y2="10.3378" layer="37"/>
<rectangle x1="5.207" y1="10.287" x2="5.2578" y2="10.3378" layer="37"/>
<rectangle x1="10.8458" y1="10.287" x2="10.8966" y2="10.3378" layer="37"/>
<rectangle x1="2.921" y1="10.3378" x2="3.429" y2="10.3886" layer="37"/>
<rectangle x1="5.207" y1="10.3378" x2="5.8674" y2="10.3886" layer="37"/>
<rectangle x1="8.6614" y1="10.3378" x2="9.2202" y2="10.3886" layer="37"/>
<rectangle x1="10.8966" y1="10.3378" x2="11.303" y2="10.3886" layer="37"/>
<rectangle x1="9.2202" y1="10.3378" x2="9.271" y2="10.3886" layer="37"/>
<rectangle x1="5.1562" y1="10.3378" x2="5.207" y2="10.3886" layer="37"/>
<rectangle x1="10.8458" y1="10.3378" x2="10.8966" y2="10.3886" layer="37"/>
<rectangle x1="2.921" y1="10.3886" x2="3.429" y2="10.4394" layer="37"/>
<rectangle x1="5.1562" y1="10.3886" x2="5.8166" y2="10.4394" layer="37"/>
<rectangle x1="8.7122" y1="10.3886" x2="9.2202" y2="10.4394" layer="37"/>
<rectangle x1="10.8966" y1="10.3886" x2="11.303" y2="10.4394" layer="37"/>
<rectangle x1="9.2202" y1="10.3886" x2="9.271" y2="10.4394" layer="37"/>
<rectangle x1="5.8166" y1="10.3886" x2="5.8674" y2="10.4394" layer="37"/>
<rectangle x1="10.8458" y1="10.3886" x2="10.8966" y2="10.4394" layer="37"/>
<rectangle x1="2.921" y1="10.4394" x2="3.429" y2="10.4902" layer="37"/>
<rectangle x1="5.1054" y1="10.4394" x2="5.7658" y2="10.4902" layer="37"/>
<rectangle x1="8.763" y1="10.4394" x2="9.271" y2="10.4902" layer="37"/>
<rectangle x1="10.8966" y1="10.4394" x2="11.303" y2="10.4902" layer="37"/>
<rectangle x1="9.271" y1="10.4394" x2="9.3218" y2="10.4902" layer="37"/>
<rectangle x1="5.7658" y1="10.4394" x2="5.8166" y2="10.4902" layer="37"/>
<rectangle x1="8.7122" y1="10.4394" x2="8.763" y2="10.4902" layer="37"/>
<rectangle x1="10.8458" y1="10.4394" x2="10.8966" y2="10.4902" layer="37"/>
<rectangle x1="2.921" y1="10.4902" x2="3.429" y2="10.541" layer="37"/>
<rectangle x1="5.1054" y1="10.4902" x2="5.715" y2="10.541" layer="37"/>
<rectangle x1="8.8138" y1="10.4902" x2="9.3218" y2="10.541" layer="37"/>
<rectangle x1="10.8966" y1="10.4902" x2="11.303" y2="10.541" layer="37"/>
<rectangle x1="5.0546" y1="10.4902" x2="5.1054" y2="10.541" layer="37"/>
<rectangle x1="8.763" y1="10.4902" x2="8.8138" y2="10.541" layer="37"/>
<rectangle x1="5.715" y1="10.4902" x2="5.7658" y2="10.541" layer="37"/>
<rectangle x1="9.3218" y1="10.4902" x2="9.3726" y2="10.541" layer="37"/>
<rectangle x1="10.8458" y1="10.4902" x2="10.8966" y2="10.541" layer="37"/>
<rectangle x1="2.921" y1="10.541" x2="3.429" y2="10.5918" layer="37"/>
<rectangle x1="5.0546" y1="10.541" x2="5.6642" y2="10.5918" layer="37"/>
<rectangle x1="8.8646" y1="10.541" x2="9.3218" y2="10.5918" layer="37"/>
<rectangle x1="10.8966" y1="10.541" x2="11.303" y2="10.5918" layer="37"/>
<rectangle x1="5.0038" y1="10.541" x2="5.0546" y2="10.5918" layer="37"/>
<rectangle x1="5.6642" y1="10.541" x2="5.715" y2="10.5918" layer="37"/>
<rectangle x1="8.8138" y1="10.541" x2="8.8646" y2="10.5918" layer="37"/>
<rectangle x1="9.3218" y1="10.541" x2="9.3726" y2="10.5918" layer="37"/>
<rectangle x1="10.8458" y1="10.541" x2="10.8966" y2="10.5918" layer="37"/>
<rectangle x1="2.921" y1="10.5918" x2="3.429" y2="10.6426" layer="37"/>
<rectangle x1="4.953" y1="10.5918" x2="5.6134" y2="10.6426" layer="37"/>
<rectangle x1="8.8646" y1="10.5918" x2="9.4234" y2="10.6426" layer="37"/>
<rectangle x1="10.8966" y1="10.5918" x2="11.303" y2="10.6426" layer="37"/>
<rectangle x1="5.6134" y1="10.5918" x2="5.6642" y2="10.6426" layer="37"/>
<rectangle x1="8.8138" y1="10.5918" x2="8.8646" y2="10.6426" layer="37"/>
<rectangle x1="4.9022" y1="10.5918" x2="4.953" y2="10.6426" layer="37"/>
<rectangle x1="10.8458" y1="10.5918" x2="10.8966" y2="10.6426" layer="37"/>
<rectangle x1="2.921" y1="10.6426" x2="3.429" y2="10.6934" layer="37"/>
<rectangle x1="4.1402" y1="10.6426" x2="4.191" y2="10.6934" layer="37"/>
<rectangle x1="4.8514" y1="10.6426" x2="5.6134" y2="10.6934" layer="37"/>
<rectangle x1="8.9154" y1="10.6426" x2="9.525" y2="10.6934" layer="37"/>
<rectangle x1="10.8966" y1="10.6426" x2="11.303" y2="10.6934" layer="37"/>
<rectangle x1="4.8006" y1="10.6426" x2="4.8514" y2="10.6934" layer="37"/>
<rectangle x1="10.8458" y1="10.6426" x2="10.8966" y2="10.6934" layer="37"/>
<rectangle x1="4.191" y1="10.6426" x2="4.2418" y2="10.6934" layer="37"/>
<rectangle x1="3.429" y1="10.6426" x2="3.4798" y2="10.6934" layer="37"/>
<rectangle x1="8.8646" y1="10.6426" x2="8.9154" y2="10.6934" layer="37"/>
<rectangle x1="4.0894" y1="10.6426" x2="4.1402" y2="10.6934" layer="37"/>
<rectangle x1="9.525" y1="10.6426" x2="9.5758" y2="10.6934" layer="37"/>
<rectangle x1="4.7498" y1="10.6426" x2="4.8006" y2="10.6934" layer="37"/>
<rectangle x1="10.795" y1="10.6426" x2="10.8458" y2="10.6934" layer="37"/>
<rectangle x1="3.4798" y1="10.6426" x2="3.5306" y2="10.6934" layer="37"/>
<rectangle x1="3.5306" y1="10.6426" x2="4.0894" y2="10.6934" layer="37"/>
<rectangle x1="4.2418" y1="10.6426" x2="4.6482" y2="10.6934" layer="37"/>
<rectangle x1="9.5758" y1="10.6426" x2="10.795" y2="10.6934" layer="37"/>
<rectangle x1="2.921" y1="10.6934" x2="5.5626" y2="10.7442" layer="37"/>
<rectangle x1="8.9662" y1="10.6934" x2="11.303" y2="10.7442" layer="37"/>
<rectangle x1="8.9154" y1="10.6934" x2="8.9662" y2="10.7442" layer="37"/>
<rectangle x1="5.5626" y1="10.6934" x2="5.6134" y2="10.7442" layer="37"/>
<rectangle x1="2.921" y1="10.7442" x2="5.5118" y2="10.795" layer="37"/>
<rectangle x1="8.9662" y1="10.7442" x2="11.303" y2="10.795" layer="37"/>
<rectangle x1="5.5118" y1="10.7442" x2="5.5626" y2="10.795" layer="37"/>
<rectangle x1="2.921" y1="10.795" x2="5.461" y2="10.8458" layer="37"/>
<rectangle x1="9.017" y1="10.795" x2="11.303" y2="10.8458" layer="37"/>
<rectangle x1="5.461" y1="10.795" x2="5.5118" y2="10.8458" layer="37"/>
<rectangle x1="8.9662" y1="10.795" x2="9.017" y2="10.8458" layer="37"/>
<rectangle x1="2.921" y1="10.8458" x2="5.4102" y2="10.8966" layer="37"/>
<rectangle x1="9.0678" y1="10.8458" x2="11.303" y2="10.8966" layer="37"/>
<rectangle x1="9.017" y1="10.8458" x2="9.0678" y2="10.8966" layer="37"/>
<rectangle x1="5.4102" y1="10.8458" x2="5.461" y2="10.8966" layer="37"/>
<rectangle x1="2.921" y1="10.8966" x2="5.3594" y2="10.9474" layer="37"/>
<rectangle x1="9.1186" y1="10.8966" x2="11.303" y2="10.9474" layer="37"/>
<rectangle x1="5.3594" y1="10.8966" x2="5.4102" y2="10.9474" layer="37"/>
<rectangle x1="9.0678" y1="10.8966" x2="9.1186" y2="10.9474" layer="37"/>
<rectangle x1="2.921" y1="10.9474" x2="5.3594" y2="10.9982" layer="37"/>
<rectangle x1="9.1186" y1="10.9474" x2="11.303" y2="10.9982" layer="37"/>
<rectangle x1="9.0678" y1="10.9474" x2="9.1186" y2="10.9982" layer="37"/>
<rectangle x1="2.9718" y1="10.9982" x2="5.3086" y2="11.049" layer="37"/>
<rectangle x1="9.1694" y1="10.9982" x2="11.2522" y2="11.049" layer="37"/>
<rectangle x1="2.921" y1="10.9982" x2="2.9718" y2="11.049" layer="37"/>
<rectangle x1="11.2522" y1="10.9982" x2="11.303" y2="11.049" layer="37"/>
<rectangle x1="9.1186" y1="10.9982" x2="9.1694" y2="11.049" layer="37"/>
<rectangle x1="5.3086" y1="10.9982" x2="5.3594" y2="11.049" layer="37"/>
<rectangle x1="2.9718" y1="11.049" x2="5.2578" y2="11.0998" layer="37"/>
<rectangle x1="9.2202" y1="11.049" x2="11.2522" y2="11.0998" layer="37"/>
<rectangle x1="9.1694" y1="11.049" x2="9.2202" y2="11.0998" layer="37"/>
<rectangle x1="11.2522" y1="11.049" x2="11.303" y2="11.0998" layer="37"/>
<rectangle x1="5.2578" y1="11.049" x2="5.3086" y2="11.0998" layer="37"/>
<rectangle x1="2.921" y1="11.049" x2="2.9718" y2="11.0998" layer="37"/>
<rectangle x1="3.0226" y1="11.0998" x2="5.207" y2="11.1506" layer="37"/>
<rectangle x1="9.2202" y1="11.0998" x2="11.2522" y2="11.1506" layer="37"/>
<rectangle x1="2.9718" y1="11.0998" x2="3.0226" y2="11.1506" layer="37"/>
<rectangle x1="5.207" y1="11.0998" x2="5.2578" y2="11.1506" layer="37"/>
<rectangle x1="3.0734" y1="11.1506" x2="5.1054" y2="11.2014" layer="37"/>
<rectangle x1="9.3218" y1="11.1506" x2="11.1506" y2="11.2014" layer="37"/>
<rectangle x1="5.1054" y1="11.1506" x2="5.1562" y2="11.2014" layer="37"/>
<rectangle x1="9.271" y1="11.1506" x2="9.3218" y2="11.2014" layer="37"/>
<rectangle x1="11.1506" y1="11.1506" x2="11.2014" y2="11.2014" layer="37"/>
</package>
</packages>
<symbols>
<symbol name="MUTEX-LOGO">
<text x="0" y="0" size="2.54" layer="94" font="vector">LOGO</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MUTEX-LOGO" prefix="LOGO">
<gates>
<gate name="G$1" symbol="MUTEX-LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MUTEX-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CC1310Misc">
<packages>
<package name="VQFN48-7X7">
<smd name="EGP" x="0" y="0" dx="5.15" dy="5.15" layer="1"/>
<smd name="43" x="-0.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="44" x="-0.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="45" x="-1.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="46" x="-1.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="47" x="-2.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="48" x="-2.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="42" x="0.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="41" x="0.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="40" x="1.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="39" x="1.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="38" x="2.25" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="37" x="2.75" y="3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="17" x="-0.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="16" x="-1.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="15" x="-1.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="14" x="-2.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="13" x="-2.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="18" x="-0.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="19" x="0.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="20" x="0.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="21" x="1.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="22" x="1.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="23" x="2.25" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="24" x="2.75" y="-3.475" dx="0.85" dy="0.28" layer="1" rot="R90"/>
<smd name="7" x="-3.475" y="-0.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="8" x="-3.475" y="-0.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="9" x="-3.475" y="-1.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="10" x="-3.475" y="-1.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="11" x="-3.475" y="-2.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="12" x="-3.475" y="-2.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="6" x="-3.475" y="0.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="5" x="-3.475" y="0.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="4" x="-3.475" y="1.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="3" x="-3.475" y="1.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="2" x="-3.475" y="2.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="1" x="-3.475" y="2.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="30" x="3.475" y="-0.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="29" x="3.475" y="-0.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="28" x="3.475" y="-1.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="27" x="3.475" y="-1.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="26" x="3.475" y="-2.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="25" x="3.475" y="-2.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="31" x="3.475" y="0.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="32" x="3.475" y="0.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="33" x="3.475" y="1.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="34" x="3.475" y="1.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="35" x="3.475" y="2.25" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<smd name="36" x="3.475" y="2.75" dx="0.85" dy="0.28" layer="1" rot="R180"/>
<wire x1="-2.8575" y1="3.4925" x2="-3.4925" y2="2.8575" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="2.8575" x2="-3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-3.4925" x2="3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-3.4925" x2="3.4925" y2="3.4925" width="0.127" layer="21"/>
<wire x1="3.4925" y1="3.4925" x2="-2.8575" y2="3.4925" width="0.127" layer="21"/>
<text x="-3.81" y="6.35" size="1.27" layer="25">&gt;Name</text>
<text x="-3.81" y="5.08" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="0850BM14E0016T">
<smd name="1" x="0.325" y="-0.05" dx="0.45" dy="0.25" layer="1" rot="R270"/>
<smd name="2" x="0.825" y="-0.05" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<smd name="3" x="1.325" y="-0.05" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<smd name="4" x="1.325" y="0.85" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<smd name="5" x="0.825" y="0.85" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<smd name="6" x="0.325" y="0.85" dx="0.45" dy="0.25" layer="1" rot="R90"/>
<wire x1="0.05" y1="0" x2="1.65" y2="0" width="0.127" layer="21"/>
<wire x1="1.65" y1="0" x2="1.65" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.65" y1="0.8" x2="0.05" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.05" y1="0.8" x2="0.05" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.4" x2="0.4" y2="0.4" width="0.127" layer="21"/>
</package>
<package name="2X05-0.05">
<pad name="9" x="1.27" y="2.54" drill="0.8" diameter="1.016" shape="offset" rot="R90"/>
<pad name="7" x="2.54" y="2.54" drill="0.8" diameter="1.016" shape="offset" rot="R90"/>
<pad name="5" x="3.81" y="2.54" drill="0.8" diameter="1.016" shape="offset" rot="R90"/>
<pad name="3" x="5.08" y="2.54" drill="0.8" diameter="1.016" shape="offset" rot="R90"/>
<pad name="1" x="6.35" y="2.54" drill="0.8" diameter="1.016" shape="offset" rot="R90"/>
<pad name="2" x="6.35" y="1.27" drill="0.8" diameter="1.016" shape="offset" rot="R270"/>
<pad name="4" x="5.08" y="1.27" drill="0.8" diameter="1.016" shape="offset" rot="R270"/>
<pad name="6" x="3.81" y="1.27" drill="0.8" diameter="1.016" shape="offset" rot="R270"/>
<pad name="8" x="2.54" y="1.27" drill="0.8" diameter="1.016" shape="offset" rot="R270"/>
<pad name="10" x="1.27" y="1.27" drill="0.8" diameter="1.016" shape="offset" rot="R270"/>
</package>
<package name="2X05-0.05-SMD">
<smd name="1" x="0" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="3" x="1.27" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="5" x="2.54" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="7" x="3.81" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="9" x="5.08" y="0" dx="0.76" dy="2.4" layer="1"/>
<smd name="10" x="5.08" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<smd name="8" x="3.81" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<smd name="6" x="2.54" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<smd name="4" x="1.27" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<smd name="2" x="0" y="3.9" dx="0.76" dy="2.4" layer="1"/>
<wire x1="-1.27" y1="0" x2="6.35" y2="0" width="0.127" layer="21"/>
<wire x1="6.35" y1="0" x2="6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.81" x2="-1.27" y2="3.81" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="0" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CC1310RGZ">
<wire x1="0" y1="0" x2="55.88" y2="0" width="0.254" layer="94"/>
<wire x1="55.88" y1="0" x2="55.88" y2="81.28" width="0.254" layer="94"/>
<wire x1="55.88" y1="81.28" x2="0" y2="81.28" width="0.254" layer="94"/>
<wire x1="0" y1="81.28" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="RF_P" x="60.96" y="33.02" length="middle" rot="R180"/>
<pin name="RF_N" x="60.96" y="27.94" length="middle" rot="R180"/>
<pin name="RX_TX" x="60.96" y="22.86" length="middle" rot="R180"/>
<pin name="X32K_Q1" x="60.96" y="15.24" length="middle" rot="R180"/>
<pin name="X32K_Q2" x="60.96" y="12.7" length="middle" rot="R180"/>
<pin name="DIO1" x="-5.08" y="76.2" length="middle"/>
<pin name="DIO2" x="-5.08" y="73.66" length="middle"/>
<pin name="DIO3" x="-5.08" y="71.12" length="middle"/>
<pin name="DIO4" x="-5.08" y="68.58" length="middle"/>
<pin name="DIO5" x="-5.08" y="66.04" length="middle"/>
<pin name="DIO6" x="-5.08" y="63.5" length="middle"/>
<pin name="DIO7" x="-5.08" y="60.96" length="middle"/>
<pin name="DIO8" x="-5.08" y="58.42" length="middle"/>
<pin name="DIO9" x="-5.08" y="55.88" length="middle"/>
<pin name="DIO10" x="-5.08" y="53.34" length="middle"/>
<pin name="DIO11" x="-5.08" y="50.8" length="middle"/>
<pin name="DIO12" x="-5.08" y="48.26" length="middle"/>
<pin name="DIO13" x="-5.08" y="45.72" length="middle"/>
<pin name="DIO14" x="-5.08" y="43.18" length="middle"/>
<pin name="DIO15" x="-5.08" y="40.64" length="middle"/>
<pin name="VDDS2" x="17.78" y="86.36" length="middle" rot="R270"/>
<pin name="VDDS3" x="22.86" y="86.36" length="middle" rot="R270"/>
<pin name="DCOUPL" x="-5.08" y="10.16" length="middle"/>
<pin name="JTAG_TMSC" x="-5.08" y="33.02" length="middle"/>
<pin name="DJTAG_TCKC" x="-5.08" y="30.48" length="middle"/>
<pin name="DIO16/JTAG_TDO" x="-5.08" y="38.1" length="middle"/>
<pin name="DIO17/JTAG_TDI" x="-5.08" y="35.56" length="middle"/>
<pin name="DIO18" x="60.96" y="71.12" length="middle" rot="R180"/>
<pin name="DIO19" x="60.96" y="68.58" length="middle" rot="R180"/>
<pin name="DIO20" x="60.96" y="66.04" length="middle" rot="R180"/>
<pin name="DIO21" x="60.96" y="63.5" length="middle" rot="R180"/>
<pin name="DIO22" x="60.96" y="60.96" length="middle" rot="R180"/>
<pin name="DCDC_SW" x="38.1" y="86.36" length="middle" rot="R270"/>
<pin name="VDDS_DCDC" x="33.02" y="86.36" length="middle" rot="R270"/>
<pin name="RESET_N" x="-5.08" y="20.32" length="middle"/>
<pin name="DIO23" x="60.96" y="58.42" length="middle" rot="R180"/>
<pin name="DIO24" x="60.96" y="55.88" length="middle" rot="R180"/>
<pin name="DIO25" x="60.96" y="53.34" length="middle" rot="R180"/>
<pin name="DIO26" x="60.96" y="50.8" length="middle" rot="R180"/>
<pin name="DIO27" x="60.96" y="48.26" length="middle" rot="R180"/>
<pin name="DIO28" x="60.96" y="45.72" length="middle" rot="R180"/>
<pin name="DIO29" x="60.96" y="43.18" length="middle" rot="R180"/>
<pin name="DIO30" x="60.96" y="40.64" length="middle" rot="R180"/>
<pin name="VDDS" x="12.7" y="86.36" length="middle" rot="R270"/>
<pin name="VDDR" x="27.94" y="86.36" length="middle" rot="R270"/>
<pin name="X24M_N" x="60.96" y="7.62" length="middle" rot="R180"/>
<pin name="X24M_P" x="60.96" y="5.08" length="middle" rot="R180"/>
<pin name="VDDR_RF" x="43.18" y="86.36" length="middle" rot="R270"/>
<pin name="GND" x="25.4" y="-5.08" length="middle" rot="R90"/>
<text x="22.86" y="55.88" size="1.778" layer="95">&gt;Name</text>
<text x="22.86" y="50.8" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="0850BM14E0016T">
<wire x1="0" y1="2.54" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="17.78" x2="0" y2="17.78" width="0.254" layer="94"/>
<wire x1="0" y1="17.78" x2="0" y2="2.54" width="0.254" layer="94"/>
<pin name="BP2" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="GND@5" x="-5.08" y="10.16" length="middle"/>
<pin name="GND@6" x="-5.08" y="5.08" length="middle"/>
<pin name="UBP" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="GND" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="BP1" x="-5.08" y="15.24" length="middle"/>
<text x="0" y="22.86" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="20.32" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="PINHEAD-2X05-0.05">
<pin name="9" x="-5.08" y="2.54" length="middle"/>
<pin name="7" x="-5.08" y="5.08" length="middle"/>
<pin name="5" x="-5.08" y="7.62" length="middle"/>
<pin name="3" x="-5.08" y="10.16" length="middle"/>
<pin name="1" x="-5.08" y="12.7" length="middle"/>
<pin name="2" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="4" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="6" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="8" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="10" x="17.78" y="2.54" length="middle" rot="R180"/>
<wire x1="0" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="22.86" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="20.32" size="1.778" layer="96">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CC1310RGZ" prefix="MCU">
<description>CC1310RGZ - Encapsulado VQFN48 7x7 0.5mm pitch</description>
<gates>
<gate name="G$1" symbol="CC1310RGZ" x="-25.4" y="-33.02"/>
</gates>
<devices>
<device name="" package="VQFN48-7X7">
<connects>
<connect gate="G$1" pin="DCDC_SW" pad="33"/>
<connect gate="G$1" pin="DCOUPL" pad="23"/>
<connect gate="G$1" pin="DIO1" pad="6"/>
<connect gate="G$1" pin="DIO10" pad="16"/>
<connect gate="G$1" pin="DIO11" pad="17"/>
<connect gate="G$1" pin="DIO12" pad="18"/>
<connect gate="G$1" pin="DIO13" pad="19"/>
<connect gate="G$1" pin="DIO14" pad="20"/>
<connect gate="G$1" pin="DIO15" pad="21"/>
<connect gate="G$1" pin="DIO16/JTAG_TDO" pad="26"/>
<connect gate="G$1" pin="DIO17/JTAG_TDI" pad="27"/>
<connect gate="G$1" pin="DIO18" pad="28"/>
<connect gate="G$1" pin="DIO19" pad="29"/>
<connect gate="G$1" pin="DIO2" pad="7"/>
<connect gate="G$1" pin="DIO20" pad="30"/>
<connect gate="G$1" pin="DIO21" pad="31"/>
<connect gate="G$1" pin="DIO22" pad="32"/>
<connect gate="G$1" pin="DIO23" pad="36"/>
<connect gate="G$1" pin="DIO24" pad="37"/>
<connect gate="G$1" pin="DIO25" pad="38"/>
<connect gate="G$1" pin="DIO26" pad="39"/>
<connect gate="G$1" pin="DIO27" pad="40"/>
<connect gate="G$1" pin="DIO28" pad="41"/>
<connect gate="G$1" pin="DIO29" pad="42"/>
<connect gate="G$1" pin="DIO3" pad="8"/>
<connect gate="G$1" pin="DIO30" pad="43"/>
<connect gate="G$1" pin="DIO4" pad="9"/>
<connect gate="G$1" pin="DIO5" pad="10"/>
<connect gate="G$1" pin="DIO6" pad="11"/>
<connect gate="G$1" pin="DIO7" pad="12"/>
<connect gate="G$1" pin="DIO8" pad="14"/>
<connect gate="G$1" pin="DIO9" pad="15"/>
<connect gate="G$1" pin="DJTAG_TCKC" pad="25"/>
<connect gate="G$1" pin="GND" pad="EGP"/>
<connect gate="G$1" pin="JTAG_TMSC" pad="24"/>
<connect gate="G$1" pin="RESET_N" pad="35"/>
<connect gate="G$1" pin="RF_N" pad="2"/>
<connect gate="G$1" pin="RF_P" pad="1"/>
<connect gate="G$1" pin="RX_TX" pad="3"/>
<connect gate="G$1" pin="VDDR" pad="45"/>
<connect gate="G$1" pin="VDDR_RF" pad="48"/>
<connect gate="G$1" pin="VDDS" pad="44"/>
<connect gate="G$1" pin="VDDS2" pad="13"/>
<connect gate="G$1" pin="VDDS3" pad="22"/>
<connect gate="G$1" pin="VDDS_DCDC" pad="34"/>
<connect gate="G$1" pin="X24M_N" pad="46"/>
<connect gate="G$1" pin="X24M_P" pad="47"/>
<connect gate="G$1" pin="X32K_Q1" pad="4"/>
<connect gate="G$1" pin="X32K_Q2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0850BM14E0016T" prefix="FL">
<description>Sub-GHz Impedance Matched Balun + LPF integrated Passive Component for Texas Instruments' CC1310 Chipset</description>
<gates>
<gate name="G$1" symbol="0850BM14E0016T" x="7.62" y="0"/>
</gates>
<devices>
<device name="" package="0850BM14E0016T">
<connects>
<connect gate="G$1" pin="BP1" pad="3"/>
<connect gate="G$1" pin="BP2" pad="4"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GND@6" pad="6"/>
<connect gate="G$1" pin="UBP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD-2X05-0.05" prefix="JP">
<description>Pinera con paso 1.27mm</description>
<gates>
<gate name="G$1" symbol="PINHEAD-2X05-0.05" x="7.62" y="5.08"/>
</gates>
<devices>
<device name="TH" package="2X05-0.05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="2X05-0.05-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Clocks">
<description>&lt;h3&gt;SparkFun Clocks, Oscillators and Resonators&lt;/h3&gt;
This library contains the real-time clocks, oscillators, resonators, and crystals we use. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CRYSTAL-SMD-5X3.2-4PAD">
<description>&lt;h3&gt;5x3.2mm SMD Crystal&lt;/h3&gt;
&lt;p&gt;Example: &lt;a href="https://www.sparkfun.com/products/94"&gt;16MHz SMD Crystal&lt;/a&gt; (&lt;a href="https://www.sparkfun.com/datasheets/Components/SPK-5032-16MHZ.pdf"&gt;Datasheet&lt;/a&gt;)&lt;/p&gt;</description>
<wire x1="-0.6" y1="1.7" x2="0.6" y2="1.7" width="0.2032" layer="21"/>
<wire x1="2.6" y1="0.3" x2="2.6" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.7" x2="-0.6" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="0.3" x2="-2.6" y2="-0.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.85" y="-1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="3" x="1.85" y="1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="4" x="-1.85" y="1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="2" x="1.85" y="-1.15" dx="1.9" dy="1.1" layer="1"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-1.6" x2="2.5" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.5" y1="-1.6" x2="2.5" y2="1.6" width="0.127" layer="51"/>
<wire x1="2.5" y1="1.6" x2="-2.5" y2="1.6" width="0.127" layer="51"/>
<polygon width="0.127" layer="51">
<vertex x="-2.5" y="1.6"/>
<vertex x="-2.5" y="0.8"/>
<vertex x="-1.3" y="0.8"/>
<vertex x="-1.3" y="1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="2.5" y="-1.6"/>
<vertex x="2.5" y="-0.8"/>
<vertex x="1.3" y="-0.8"/>
<vertex x="1.3" y="-1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="1.3" y="1.6"/>
<vertex x="1.3" y="0.8"/>
<vertex x="2.5" y="0.8"/>
<vertex x="2.5" y="1.6"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="-1.3" y="-1.6"/>
<vertex x="-1.3" y="-0.8"/>
<vertex x="-2.5" y="-0.8"/>
<vertex x="-2.5" y="-1.6"/>
</polygon>
</package>
<package name="CRYSTAL-SMD-3.2X2.5MM">
<description>&lt;h3&gt;3.2 x 2.5mm SMD Crystal Package&lt;/h3&gt;
&lt;p&gt;Example: &lt;a href="http://www.digikey.com/product-search/en?keywords=SER3627TR-ND"&gt;SX-32S&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-0.4" y1="1.377" x2="0.4" y2="1.377" width="0.2032" layer="21"/>
<wire x1="-1.727" y1="-0.15" x2="-1.727" y2="0.15" width="0.2032" layer="21"/>
<wire x1="1.727" y1="0.15" x2="1.727" y2="-0.15" width="0.2032" layer="21"/>
<wire x1="0.4" y1="-1.377" x2="-0.4" y2="-1.377" width="0.2032" layer="21"/>
<rectangle x1="-1.6" y1="0.35" x2="-0.6" y2="1.15" layer="51"/>
<rectangle x1="0.6" y1="-1.15" x2="1.6" y2="-0.35" layer="51" rot="R180"/>
<rectangle x1="-1.6" y1="-1.15" x2="-0.6" y2="-0.35" layer="51"/>
<rectangle x1="0.6" y1="0.35" x2="1.6" y2="1.15" layer="51" rot="R180"/>
<smd name="1" x="-1.175" y="-0.875" dx="1.2" dy="1.1" layer="1" rot="R180"/>
<smd name="2" x="1.175" y="-0.875" dx="1.2" dy="1.1" layer="1"/>
<smd name="3" x="1.175" y="0.875" dx="1.2" dy="1.1" layer="1"/>
<smd name="4" x="-1.175" y="0.875" dx="1.2" dy="1.1" layer="1" rot="R180"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="CRYSTAL-PTH-2X6-CYL">
<description>&lt;h3&gt;2x6mm Cylindrical Can (Radial) PTH Crystal&lt;/h3&gt;

&lt;p&gt;Example product: &lt;a href="https://www.sparkfun.com/products/540"&gt;32kHz crystal&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ecsxtal.com/store/pdf/ECS-3x8.pdf"&gt;Example datasheet&lt;/a&gt; (ECS-2X6)&lt;/p&gt;</description>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.747" x2="1.016" y2="7.493" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="7.493" x2="-0.762" y2="7.747" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="7.747" x2="0.762" y2="7.747" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="4.953" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.953" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.334" x2="0" y2="5.334" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.191" x2="0" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0.508" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0.508" y2="5.334" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6764"/>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="0.762" x2="1.778" y2="8.382" layer="43"/>
<text x="-1.27" y="4.572" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.27" y="4.572" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="CRYSTAL-SMD-2X6-CYL">
<description>&lt;h3&gt;6.0x2.0mm Cylindrical Can (Radial) SMD Crystal&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://cfm.citizen.co.jp/english/product/pdf/CMR200T.pdf"&gt;Example&lt;/a&gt;&lt;/p&gt;</description>
<smd name="X1" x="-1.27" y="0" dx="1" dy="2.5" layer="1"/>
<smd name="X2" x="1.27" y="0" dx="1" dy="2.5" layer="1"/>
<smd name="SHEILD" x="0" y="5.08" dx="2.5" dy="6" layer="1"/>
<text x="0" y="8.255" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="-1.27" y1="-1" x2="-1.27" y2="1" width="0.3" layer="51"/>
<wire x1="-1.27" y1="1" x2="-0.6" y2="1" width="0.3" layer="51"/>
<wire x1="-0.6" y1="1" x2="-0.6" y2="2" width="0.3" layer="51"/>
<wire x1="-0.6" y1="2" x2="-0.5" y2="2" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1" x2="1.27" y2="1" width="0.3" layer="51"/>
<wire x1="1.27" y1="1" x2="0.6" y2="1" width="0.3" layer="51"/>
<wire x1="0.6" y1="1" x2="0.6" y2="2" width="0.3" layer="51"/>
<wire x1="0.6" y1="2" x2="0.5" y2="2" width="0.127" layer="51"/>
<polygon width="0.1" layer="51">
<vertex x="-1" y="8"/>
<vertex x="-1" y="2"/>
<vertex x="1" y="2"/>
<vertex x="1" y="8"/>
</polygon>
</package>
<package name="CRYSTAL-PTH-2X6-CYL-KIT">
<description>&lt;h3&gt;2x6mm Cylindrical Can (Radial) PTH Crystal&lt;/h3&gt;

This is the "KIT" version, which has limited top masking for improved ease of assembly.

&lt;p&gt;Example product: &lt;a href="https://www.sparkfun.com/products/540"&gt;32kHz crystal&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ecsxtal.com/store/pdf/ECS-3x8.pdf"&gt;Example datasheet&lt;/a&gt; (ECS-2X6)&lt;/p&gt;</description>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.747" x2="1.016" y2="7.493" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="7.493" x2="-0.762" y2="7.747" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="7.747" x2="0.762" y2="7.747" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="4.953" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.953" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.334" x2="0" y2="5.334" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.191" x2="0" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0.508" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0.508" y2="5.334" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764" stop="no"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6764" stop="no"/>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="0.762" x2="1.778" y2="8.382" layer="43"/>
<circle x="-1.27" y="0" radius="0.508" width="0" layer="29"/>
<circle x="1.27" y="0" radius="0.508" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="0.924571875" width="0" layer="30"/>
<circle x="1.27" y="0" radius="0.915809375" width="0" layer="30"/>
<text x="-1.27" y="4.572" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.27" y="4.572" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="CRYSTAL-SMD-3.2X1.5MM">
<description>&lt;h3&gt;3.2 x 1.5mm SMD Crystal Package&lt;/h3&gt;
&lt;p&gt;Example: &lt;a href="http://www.sii.co.jp/en/quartz/files/2013/03/file_PRODUCT_MASTER_50812_GRAPHIC03.pdf"&gt;SX-32S&lt;/a&gt;&lt;/p&gt;</description>
<smd name="P$1" x="-1.25" y="0" dx="1" dy="1.8" layer="1"/>
<smd name="P$2" x="1.25" y="0" dx="1" dy="1.8" layer="1"/>
<wire x1="-1.6" y1="0.75" x2="1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.75" x2="1.6" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.75" x2="-1.6" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.75" x2="-1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0.85" x2="0.5" y2="0.85" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.85" x2="-0.5" y2="-0.85" width="0.2032" layer="21"/>
<text x="0" y="1.043" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.043" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="CRYSTAL-SMD-MC-146">
<description>&lt;h3&gt;7x1.5mm MC-146 Flat Lead SMD Crystal&lt;/h3&gt;

&lt;p&gt;&lt;a href="https://support.epson.biz/td/api/doc_check.php?dl=brief_MC-156_en.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="0.35" y1="0" x2="7.05" y2="0" width="0.127" layer="51"/>
<wire x1="7.05" y1="0" x2="7.05" y2="1.5" width="0.127" layer="51"/>
<wire x1="7.05" y1="1.5" x2="0.35" y2="1.5" width="0.127" layer="51"/>
<wire x1="0.35" y1="1.5" x2="0.35" y2="0" width="0.127" layer="51"/>
<smd name="P$1" x="0.6" y="0.3" dx="1.2" dy="0.6" layer="1"/>
<smd name="P$2" x="0.6" y="1.2" dx="1.2" dy="0.6" layer="1"/>
<smd name="NC2" x="6.9" y="0.3" dx="1.2" dy="0.6" layer="1"/>
<smd name="NC1" x="6.9" y="1.2" dx="1.2" dy="0.6" layer="1"/>
<text x="3.81" y="1.851" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="3.81" y="-0.327" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="7.05" y1="-0.2" x2="0.35" y2="-0.2" width="0.2032" layer="21"/>
<wire x1="0.35" y1="1.7" x2="7.05" y2="1.7" width="0.2032" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL-GND">
<description>&lt;h3&gt;Crystal with Ground pin&lt;/h3&gt;</description>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<text x="1.524" y="-1.524" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0" y1="-2.8" x2="0" y2="-1.6" width="0.1524" layer="94"/>
<text x="-1.27" y="-1.524" size="1.778" layer="95" font="vector" align="top-right">&gt;NAME</text>
</symbol>
<symbol name="CRYSTAL">
<description>&lt;h3&gt;Crystal (no ground pin)&lt;/h3&gt;</description>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="0" y="2.032" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.032" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTAL-GROUNDED" prefix="Y">
<description>&lt;h3&gt;Crystals w/ Ground Pin (Generic)&lt;/h3&gt;
&lt;p&gt;These are &lt;b&gt;passive&lt;/b&gt; quartz crystals, which can be used as a clock source for a microcontroller.&lt;/p&gt;
&lt;p&gt;Crystal's are usually two-terminal devices. A third terminal may be optionally connected to ground, as recommended by the manufaturer.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CRYSTAL-GND" x="0" y="0"/>
</gates>
<devices>
<device name="SMD-5X3.2" package="CRYSTAL-SMD-5X3.2-4PAD">
<connects>
<connect gate="G$1" pin="1" pad="3"/>
<connect gate="G$1" pin="2" pad="1"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-3.2X2.5" package="CRYSTAL-SMD-3.2X2.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-32.768KHZ" prefix="Y" uservalue="yes">
<description>&lt;h3&gt;32.768kHz Crystal&lt;/h3&gt;
&lt;p&gt;&lt;ul&gt;&lt;li&gt;Frequency: 32.768kHz&lt;/li&gt;
&lt;li&gt;Frequency Stability: &amp;plusmn;20ppm&lt;/li&gt;
&lt;li&gt;Load Capacitance: 12.5pF (PTH-2X6 &amp; SMD-3.2X1.5), 6pF (SMD-2X6)&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/11734”&gt;SparkFun BigTime Watch Kit&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13990”&gt;SparkFun nRF52832 Breakout&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12708”&gt;SparkFun Real Time Clock Module&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="PTH-2X6" package="CRYSTAL-PTH-2X6-CYL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XTAL-10112"/>
<attribute name="VALUE" value="32.768kHz"/>
</technology>
</technologies>
</device>
<device name="SMD-2X6" package="CRYSTAL-SMD-2X6-CYL">
<connects>
<connect gate="G$1" pin="1" pad="X1"/>
<connect gate="G$1" pin="2" pad="X2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XTAL-07894"/>
<attribute name="VALUE" value="32.768kHz"/>
</technology>
</technologies>
</device>
<device name="PTH-2X6-KIT" package="CRYSTAL-PTH-2X6-CYL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XTAL-10112"/>
<attribute name="VALUE" value="32.768kHz"/>
</technology>
</technologies>
</device>
<device name="SMD-3.2X1.5" package="CRYSTAL-SMD-3.2X1.5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XTAL-13062"/>
<attribute name="VALUE" value="32.768kHz"/>
</technology>
</technologies>
</device>
<device name="SMD-MC146" package="CRYSTAL-SMD-MC-146">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
<connect gate="G$1" pin="2" pad="P$1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XTAL-09342"/>
<attribute name="VALUE" value="32.768kHz"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Coils">
<description>&lt;h3&gt;SparkFun Coils&lt;/h3&gt;
In this library you'll find magnetics.

&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;Inductors&lt;/li&gt;
&lt;li&gt;Ferrite Beads&lt;/li&gt;
&lt;li&gt;Transformers&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;

&lt;br&gt;
&lt;p&gt;We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.&lt;/p&gt;</description>
<packages>
<package name="CR75">
<description>&lt;h3&gt;4600 series footprint&lt;/h3&gt;
&lt;p&gt;Not messing with it since production uses it. Origin unknown but loosely based on this &lt;a href="http://www.murata-ps.com/data/magnetics/kmp_4600.pdf"&gt;datasheet&lt;/a&gt;.&lt;/p&gt;</description>
<wire x1="-4.025" y1="3.65" x2="3.975" y2="3.65" width="0.127" layer="21"/>
<wire x1="3.975" y1="3.65" x2="3.975" y2="2.55" width="0.127" layer="21"/>
<wire x1="-4.025" y1="3.65" x2="-4.025" y2="2.55" width="0.127" layer="21"/>
<wire x1="-4.025" y1="-3.65" x2="3.975" y2="-3.65" width="0.127" layer="21"/>
<wire x1="3.975" y1="-3.65" x2="3.975" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-4.025" y1="-3.65" x2="-4.025" y2="-2.55" width="0.127" layer="21"/>
<smd name="P$1" x="-3.025" y="0" dx="4.7" dy="1.75" layer="1" rot="R90"/>
<smd name="P$2" x="3.025" y="0" dx="4.7" dy="1.75" layer="1" rot="R90"/>
<text x="0" y="3.81" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.81" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="SRN6045">
<description>&lt;h3&gt;SRN6045 series  footprint&lt;/h3&gt;
&lt;p&gt;Roughly based on the recommendation in this &lt;a href="http://www.mouser.com/ds/2/54/RN6045-778135.pdf"&gt;datasheet&lt;/a&gt;.&lt;/p&gt;</description>
<smd name="1" x="-2.175" y="0" dx="6" dy="2.5" layer="1" rot="R90"/>
<smd name="2" x="2.125" y="0" dx="6" dy="2.5" layer="1" rot="R90"/>
<wire x1="-2.175" y1="-3" x2="-3.175" y2="-2" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-2" x2="-3.175" y2="2" width="0.127" layer="51"/>
<wire x1="-3.175" y1="2" x2="-2.175" y2="3" width="0.127" layer="51"/>
<wire x1="-2.175" y1="3" x2="2.125" y2="3" width="0.127" layer="51"/>
<wire x1="2.125" y1="3" x2="3.125" y2="2" width="0.127" layer="51"/>
<wire x1="3.125" y1="2" x2="3.125" y2="-2" width="0.127" layer="51"/>
<wire x1="3.125" y1="-2" x2="2.125" y2="-3" width="0.127" layer="51"/>
<wire x1="2.125" y1="-3" x2="-2.175" y2="-3" width="0.127" layer="51"/>
<text x="0" y="3.223" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.254" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-0.762" y1="2.969" x2="0.762" y2="2.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-3" x2="0.762" y2="-3" width="0.1524" layer="21"/>
</package>
<package name="INDUCTOR_4.7UH">
<description>&lt;h3&gt;CDRH2D18/HPNP footprint&lt;/h3&gt;
&lt;p&gt;Recommended footprint for CDRH2D18/HPNP series inductors from &lt;a href="http://products.sumida.com/products/pdf/CDRH2D18HP.pdf"&gt;here&lt;/a&gt;.&lt;/p&gt;</description>
<wire x1="-1.2" y1="0.9" x2="-0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="1.5" x2="1.2" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-0.9" x2="-0.6783" y2="-1.3739" width="0.2032" layer="21"/>
<wire x1="-0.6783" y1="-1.3739" x2="0.6783" y2="-1.3739" width="0.2032" layer="21" curve="85.420723"/>
<wire x1="0.6783" y1="-1.3739" x2="1.2" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.6" x2="-0.7071" y2="-1.3929" width="0.03" layer="51"/>
<wire x1="-0.7071" y1="-1.3929" x2="0.7071" y2="-1.3929" width="0.03" layer="51" curve="90"/>
<wire x1="0.7071" y1="-1.3929" x2="1.5" y2="-0.6" width="0.03" layer="51"/>
<wire x1="1.5" y1="-0.6" x2="1.5" y2="0.6" width="0.03" layer="51"/>
<wire x1="1.5" y1="0.6" x2="0.6" y2="1.5" width="0.03" layer="51"/>
<wire x1="0.6" y1="1.5" x2="-0.6" y2="1.5" width="0.03" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="-1.5" y2="0.6" width="0.03" layer="51"/>
<wire x1="-1.5" y1="0.6" x2="-1.5" y2="-0.6" width="0.03" layer="51"/>
<smd name="P$1" x="-1.5" y="0" dx="1.3" dy="1.3" layer="1" rot="R90"/>
<smd name="P$2" x="1.5" y="0" dx="1.3" dy="1.3" layer="1" rot="R90"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="INDUCTOR_SDR1307">
<description>&lt;h3&gt;SDR1307 series footprint&lt;/h3&gt;
&lt;p&gt;Footprint based on recommendation from &lt;a href="https://www.bourns.com/pdfs/SDR1307.pdf"&gt;here&lt;/a&gt;.&lt;/p&gt;</description>
<smd name="P$1" x="0" y="4.6" dx="14" dy="4.75" layer="1"/>
<smd name="P$2" x="0" y="-4.6" dx="14" dy="4.75" layer="1"/>
<wire x1="-6.5" y1="1.5" x2="-6.5" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="6.5" y1="1.5" x2="6.5" y2="-1.5" width="0.3048" layer="21"/>
<text x="0" y="7.246" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.23" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="CR54">
<description>&lt;h3&gt;CR54 footprint&lt;/h3&gt;
&lt;p&gt;These vary by manufacturer, but we used the NPIS54LS footprint from &lt;a href="http://www.niccomp.com/catalog/npis_ls.pdf"&gt;here&lt;/a&gt;.</description>
<wire x1="2.6" y1="2.6" x2="-2.6" y2="2.6" width="0.127" layer="51"/>
<wire x1="-2.6" y1="2.6" x2="-2.6" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.6" y1="-2.6" x2="2.6" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2.6" y1="-2.6" x2="2.6" y2="2.6" width="0.127" layer="51"/>
<wire x1="-2.87" y1="2.6" x2="-2.87" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-2.87" x2="2.6" y2="-2.87" width="0.2032" layer="21"/>
<wire x1="2.87" y1="-2.6" x2="2.87" y2="2.6" width="0.2032" layer="21"/>
<wire x1="2.6" y1="2.87" x2="-2.6" y2="2.87" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="1.85" dx="4.2" dy="1.4" layer="1"/>
<smd name="P$2" x="0" y="-1.85" dx="4.2" dy="1.4" layer="1"/>
<text x="0" y="3.077" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.048" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<rectangle x1="-2.1" y1="1.15" x2="2.1" y2="2.55" layer="51"/>
<rectangle x1="-2.1" y1="-2.55" x2="2.1" y2="-1.15" layer="51"/>
<wire x1="-2.87" y1="-2.6" x2="-2.6" y2="-2.87" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-2.87" x2="2.87" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-2.87" y1="2.6" x2="-2.6" y2="2.87" width="0.2032" layer="21"/>
<wire x1="2.6" y1="2.87" x2="2.87" y2="2.6" width="0.2032" layer="21"/>
</package>
<package name="0805">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="INDUCTOR">
<description>&lt;h3&gt;Inductors&lt;/h3&gt;
&lt;p&gt;Resist changes in electrical current. Basically a coil of wire.&lt;/p&gt;</description>
<text x="1.27" y="2.54" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.27" y="-2.54" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="INDUCTOR" prefix="L">
<description>&lt;h3&gt;Inductors&lt;/h3&gt;
&lt;p&gt;Resist changes in electrical current. Basically a coil of wire.&lt;/p&gt;
&lt;p&gt;SparkFun Products:
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13879"&gt;SparkFun Load Cell Amplifier - HX711&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13613"&gt;IOIO-OTG - V2.2&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=""&gt;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-CR75-68UH" package="CR75">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-09739"/>
<attribute name="VALUE" value="68µH/±20%/1.05A"/>
</technology>
</technologies>
</device>
<device name="-SRN6045-33UH" package="SRN6045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-11627"/>
<attribute name="VALUE" value="33µH/±20%/1.4A"/>
</technology>
</technologies>
</device>
<device name="-CDRH-4.7UH" package="INDUCTOR_4.7UH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-10146"/>
<attribute name="VALUE" value="4.7µH/±30%/1.2A"/>
</technology>
</technologies>
</device>
<device name="-SDR13-27UH" package="INDUCTOR_SDR1307">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-12529"/>
<attribute name="VALUE" value="27µH/±20%/3.3A"/>
</technology>
</technologies>
</device>
<device name="-CR54-3.3UH" package="CR54">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-11157"/>
<attribute name="VALUE" value="3.3µH/±30%/3.4A"/>
</technology>
</technologies>
</device>
<device name="-CR54-47UH" package="CR54">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-10789"/>
<attribute name="VALUE" value="47µH/±30%/750mA"/>
</technology>
</technologies>
</device>
<device name="-0805-3.3UH" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-13076"/>
<attribute name="VALUE" value="3.3µH/±20%/450mA"/>
</technology>
</technologies>
</device>
<device name="-0603-33NH" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-07874"/>
<attribute name="VALUE" value="33nH/±5%/500mA"/>
</technology>
</technologies>
</device>
<device name="-0402-3.9NH" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-13457" constant="no"/>
<attribute name="VALUE" value="3.9NH" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-coax" urn="urn:adsk.eagle:library:133">
<description>&lt;b&gt;Coax Connectors&lt;/b&gt;&lt;p&gt;
Radiall  and M/A COM.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="BU-SMA-G" urn="urn:adsk.eagle:footprint:6180/1" library_version="1">
<description>FEMALE &lt;b&gt;SMA CONNECTOR&lt;/b&gt;&lt;p&gt;
Radiall&lt;p&gt;
distributor RS 193-9117</description>
<wire x1="-1.1" y1="3.4" x2="1.1" y2="3.4" width="0.2032" layer="21"/>
<wire x1="3.4" y1="1.1" x2="3.4" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-3.4" x2="-1.1" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="-1.1" x2="-3.4" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="3.4" x2="3.4" y2="3.4" width="0.2032" layer="51"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="-3.4" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-3.4" x2="-3.4" y2="-3.4" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-3.4" x2="-3.4" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.4" y1="3.3" x2="6.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="6.3" y1="3.3" x2="6.3" y2="2.9" width="0.2032" layer="21"/>
<wire x1="6.3" y1="2.9" x2="7.1" y2="2.9" width="0.2032" layer="21"/>
<wire x1="7.1" y1="2.9" x2="7.1" y2="3.3" width="0.2032" layer="21"/>
<wire x1="7.1" y1="3.3" x2="8" y2="3.3" width="0.2032" layer="21"/>
<wire x1="15" y1="3.4" x2="15.3" y2="2.9" width="0.2032" layer="21"/>
<wire x1="15.3" y1="2.9" x2="15.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="15.9" y1="-2.9" x2="15.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="8.9" y1="3.4" x2="15" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.4" y1="-3.3" x2="6.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="6.3" y1="-3.3" x2="6.3" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="6.3" y1="-2.9" x2="7.1" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="7.1" y1="-2.9" x2="7.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="7.1" y1="-3.3" x2="8" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="15" y1="-3.4" x2="15.3" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="15.3" y1="-2.9" x2="15.9" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="8.9" y1="-3.4" x2="15" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="15.3" y1="-2.9" x2="15.3" y2="2.9" width="0.2032" layer="21"/>
<wire x1="5.4" y1="-3.9" x2="5.4" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="5.4" y1="-3.3" x2="5.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="5.4" y1="3.3" x2="5.4" y2="3.9" width="0.2032" layer="21"/>
<wire x1="8" y1="4.4" x2="8.9" y2="4.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="-4.4" x2="8" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="8" y1="-2.2" x2="8" y2="2.2" width="0.2032" layer="21"/>
<wire x1="8" y1="-2.2" x2="8.9" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="8" y1="-2.2" x2="8" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8" y1="-3.3" x2="8" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="8" y1="2.2" x2="8.9" y2="2.2" width="0.2032" layer="21"/>
<wire x1="8" y1="2.2" x2="8" y2="3.3" width="0.2032" layer="21"/>
<wire x1="8" y1="3.3" x2="8" y2="4.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="2.2" x2="8.9" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="3.4" x2="8.9" y2="4.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="2.2" x2="8.9" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="8.9" y1="-4.4" x2="8.9" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="-3.4" x2="8.9" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="3.4" y1="3.9" x2="5.4" y2="3.9" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-3.9" x2="5.4" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="3.4" y1="3.9" x2="3.4" y2="3.4" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-3.4" x2="3.4" y2="-3.9" width="0.2032" layer="51"/>
<wire x1="6.3" y1="2.9" x2="6.3" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="7.1" y1="2.9" x2="7.1" y2="-2.9" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.27"/>
<pad name="2" x="-2.5499" y="2.5499" drill="1.778"/>
<pad name="3" x="2.5499" y="2.5499" drill="1.778"/>
<pad name="4" x="2.5499" y="-2.5499" drill="1.778"/>
<pad name="5" x="-2.5499" y="-2.5499" drill="1.778"/>
<text x="-2.54" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="BU-SMA-G" urn="urn:adsk.eagle:package:6205/1" type="box" library_version="1">
<description>FEMALE SMA CONNECTOR
Radiall
distributor RS 193-9117</description>
</package3d>
</packages3d>
<symbols>
<symbol name="BNC-FGND" urn="urn:adsk.eagle:symbol:6174/1" library_version="1">
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.508" x2="-0.762" y2="0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.508" x2="-2.54" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BU-SMA-G" urn="urn:adsk.eagle:component:6236/1" prefix="X" library_version="1">
<description>FEMALE &lt;b&gt;SMA CONNECTOR&lt;/b&gt;&lt;p&gt;
Radiall&lt;p&gt;
distributor RS 193-9117</description>
<gates>
<gate name="G1" symbol="BNC-FGND" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BU-SMA-G">
<connects>
<connect gate="G1" pin="1" pad="1"/>
<connect gate="G1" pin="2" pad="2 3 4 5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6205/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Aesthetics&lt;/h3&gt;
This library contiains non-functional items such as logos, build/ordering notes, frame blocks, etc. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="FR-A4L">
<description>&lt;h3&gt;Schematic Frame - A4L - European Format&lt;/h3&gt;
&lt;p&gt;Standard A4 size frame in Landscape&lt;/p&gt;
&lt;p&gt;Devices using:
&lt;ul&gt;&lt;li&gt;FRAME-A4L&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
<rectangle x1="178.7652" y1="0" x2="179.3748" y2="20.32" layer="94"/>
<rectangle x1="225.7552" y1="-26.67" x2="226.3648" y2="67.31" layer="94" rot="R90"/>
<wire x1="225.29" y1="-0.1" x2="225.29" y2="5.08" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="273.05" y2="5.08" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="179.07" y2="5.08" width="0.1016" layer="94"/>
<wire x1="179.07" y1="10.16" x2="225.29" y2="10.16" width="0.1016" layer="94"/>
<wire x1="225.29" y1="10.16" x2="273.05" y2="10.16" width="0.1016" layer="94"/>
<wire x1="179.07" y1="15.24" x2="273.05" y2="15.24" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="225.29" y2="10.16" width="0.1016" layer="94"/>
<wire x1="179.07" y1="19.05" x2="179.07" y2="20.32" width="0.6096" layer="94"/>
<wire x1="179.07" y1="20.32" x2="180.34" y2="20.32" width="0.6096" layer="94"/>
<text x="181.61" y="11.43" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="181.61" y="6.35" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="195.58" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="181.61" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="181.61" y="16.51" size="2.54" layer="94" font="vector">&gt;CNAME</text>
<text x="226.16" y="1.27" size="2.54" layer="94" font="vector">Rev:</text>
<text x="226.26" y="6.35" size="2.54" layer="94" font="vector">&gt;DESIGNER</text>
<text x="234.92" y="1.17" size="2.54" layer="94" font="vector">&gt;CREVISION</text>
<frame x1="-3.81" y1="-3.81" x2="276.86" y2="182.88" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A4L" prefix="FRAME">
<description>&lt;h3&gt;Schematic Frame - A4L - European Format&lt;/h3&gt;
&lt;p&gt;Standard A4 size frame in Landscape&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="FR-A4L" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="CNAME" value="Surtidor"/>
<attribute name="CREVISION" value="0.1"/>
<attribute name="DESIGNER" value="Ing. Agustin I. Reyes"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U3" library="SparkFun-IC-Power" deviceset="V_REG_317" device="SMD" value="1117"/>
<part name="X2" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-102" device=""/>
<part name="X3" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-102" device=""/>
<part name="D1" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="R3" library="SparkFun-Resistors" deviceset="0.27OHM" device="-0603-1/10W-1%" value="330"/>
<part name="D3" library="SparkFun-DiscreteSemi" deviceset="DIODE-SCHOTTKY" device="-BAT20J" value="BAT20J"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C7" library="SparkFun-Capacitors" deviceset="22UF" device="-1210-16V-20%"/>
<part name="C8" library="SparkFun-Capacitors" deviceset="2.2UF" device="-0805-25V-(+80/-20%)" value="2.2uF"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C9" library="SparkFun-Capacitors" deviceset="12PF" device="-0603-50V-5%" value="100nF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+12V" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="U4" library="SparkFun-IC-Power" deviceset="V_REG_317" device="SMD" value="1117"/>
<part name="C10" library="SparkFun-Capacitors" deviceset="2.2UF" device="-0805-25V-(+80/-20%)" value="2.2uF"/>
<part name="C11" library="SparkFun-Capacitors" deviceset="2.2UF" device="-0805-25V-(+80/-20%)" value="2.2uF"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X4" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-102" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="LOGO1" library="aestethics" deviceset="MUTEX-LOGO" device=""/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_03" device="POLAR"/>
<part name="MCU2" library="CC1310Misc" deviceset="CC1310RGZ" device=""/>
<part name="FL2" library="CC1310Misc" deviceset="0850BM14E0016T" device=""/>
<part name="Y3" library="SparkFun-Clocks" deviceset="CRYSTAL-GROUNDED" device="SMD-3.2X2.5" value="24MHz"/>
<part name="Y4" library="SparkFun-Clocks" deviceset="CRYSTAL-32.768KHZ" device="SMD-3.2X1.5" value="32.768kHz"/>
<part name="C21" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C22" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C23" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C24" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0402-16V-10%" value="0.1uF"/>
<part name="C25" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-100V-10%" value="0.1uF"/>
<part name="C26" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-100V-10%" value="0.1uF"/>
<part name="R1" library="SparkFun-Resistors" deviceset="0.05OHM" device="-0603-1/5W-1%" value="100k"/>
<part name="L5" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="BLM18HE152SN1"/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="C27" library="SparkFun-Capacitors" deviceset="22UF" device="-0805-6.3V-20%" value="22uF"/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="C28" library="SparkFun-Capacitors" deviceset="22UF" device="-0805-6.3V-20%" value="22uF"/>
<part name="L6" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0603-33NH" value="6.8uHy"/>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="GND42" library="supply1" deviceset="GND" device=""/>
<part name="C29" library="SparkFun-Capacitors" deviceset="0.8PF" device="-0402-50V-0.1PF" value="100pF"/>
<part name="GND43" library="supply1" deviceset="GND" device=""/>
<part name="C30" library="SparkFun-Capacitors" deviceset="0.8PF" device="-0402-50V-0.1PF" value="12pF"/>
<part name="C31" library="SparkFun-Capacitors" deviceset="0.8PF" device="-0402-50V-0.1PF" value="12pF"/>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="GND45" library="supply1" deviceset="GND" device=""/>
<part name="GND46" library="supply1" deviceset="GND" device=""/>
<part name="C32" library="SparkFun-Capacitors" deviceset="0.18UF" device="-0603-10V-10%" value="0.1uF"/>
<part name="C33" library="SparkFun-Capacitors" deviceset="0.18UF" device="-0603-10V-10%" value="1uF"/>
<part name="GND47" library="supply1" deviceset="GND" device=""/>
<part name="GND48" library="supply1" deviceset="GND" device=""/>
<part name="L7" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0402-3.9NH" value="3.9NH"/>
<part name="L8" library="SparkFun-Coils" deviceset="INDUCTOR" device="-0402-3.9NH" value="3.9NH"/>
<part name="GND49" library="supply1" deviceset="GND" device=""/>
<part name="GND50" library="supply1" deviceset="GND" device=""/>
<part name="JP1" library="CC1310Misc" deviceset="PINHEAD-2X05-0.05" device="SMD"/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="GND51" library="supply1" deviceset="GND" device=""/>
<part name="X5" library="con-coax" library_urn="urn:adsk.eagle:library:133" deviceset="BU-SMA-G" device="" package3d_urn="urn:adsk.eagle:package:6205/1"/>
<part name="GND52" library="supply1" deviceset="GND" device=""/>
<part name="D2" library="SparkFun-LED" deviceset="LED" device="0603"/>
<part name="R2" library="SparkFun-Resistors" deviceset="0.27OHM" device="-0603-1/10W-1%" value="330"/>
<part name="Q1" library="SparkFun-DiscreteSemi" deviceset="MOSFET-NCH" device="-BSS138" value="BSS138"/>
<part name="R4" library="SparkFun-Resistors" deviceset="0.27OHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R5" library="SparkFun-Resistors" deviceset="0.27OHM" device="-0603-1/10W-1%" value="10k"/>
<part name="Q2" library="SparkFun-DiscreteSemi" deviceset="MOSFET-NCH" device="-BSS138" value="BSS138"/>
<part name="R6" library="SparkFun-Resistors" deviceset="0.27OHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R7" library="SparkFun-Resistors" deviceset="0.27OHM" device="-0603-1/10W-1%" value="10k"/>
<part name="FRAME1" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device=""/>
<part name="FRAME2" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="LOGO1" gate="G$1" x="259.08" y="22.86"/>
<instance part="MCU2" gate="G$1" x="48.26" y="40.64"/>
<instance part="FL2" gate="G$1" x="157.48" y="58.42" rot="R90"/>
<instance part="Y3" gate="G$1" x="114.3" y="38.1"/>
<instance part="Y4" gate="G$1" x="181.61" y="40.132" rot="R180"/>
<instance part="R1" gate="G$1" x="20.32" y="71.12" rot="R90"/>
<instance part="GND41" gate="1" x="147.32" y="48.26"/>
<instance part="GND42" gate="1" x="152.4" y="48.26"/>
<instance part="C29" gate="G$1" x="172.72" y="83.82" rot="R90"/>
<instance part="GND43" gate="1" x="114.3" y="27.94"/>
<instance part="C30" gate="G$1" x="175.26" y="35.56" rot="R180"/>
<instance part="C31" gate="G$1" x="187.96" y="35.56" rot="MR180"/>
<instance part="GND44" gate="1" x="175.26" y="25.4"/>
<instance part="GND45" gate="1" x="187.96" y="25.4"/>
<instance part="GND46" gate="1" x="73.66" y="30.48"/>
<instance part="C32" gate="G$1" x="20.32" y="50.8"/>
<instance part="C33" gate="G$1" x="35.56" y="40.64"/>
<instance part="GND47" gate="1" x="35.56" y="30.48"/>
<instance part="GND48" gate="1" x="20.32" y="30.48"/>
<instance part="L7" gate="G$1" x="162.56" y="76.2"/>
<instance part="L8" gate="G$1" x="180.34" y="76.2"/>
<instance part="GND49" gate="1" x="162.56" y="66.04"/>
<instance part="GND50" gate="1" x="180.34" y="66.04"/>
<instance part="C21" gate="G$1" x="157.48" y="147.32"/>
<instance part="C22" gate="G$1" x="170.18" y="147.32"/>
<instance part="C23" gate="G$1" x="182.88" y="147.32"/>
<instance part="C24" gate="G$1" x="208.28" y="147.32"/>
<instance part="C25" gate="G$1" x="195.58" y="114.3"/>
<instance part="C26" gate="G$1" x="208.28" y="114.3"/>
<instance part="L5" gate="G$1" x="144.78" y="154.94" rot="R90"/>
<instance part="GND33" gate="1" x="157.48" y="139.7"/>
<instance part="GND34" gate="1" x="170.18" y="139.7"/>
<instance part="GND35" gate="1" x="182.88" y="139.7"/>
<instance part="GND36" gate="1" x="195.58" y="139.7"/>
<instance part="GND37" gate="1" x="208.28" y="139.7"/>
<instance part="C27" gate="G$1" x="195.58" y="147.32"/>
<instance part="+3V3" gate="G$1" x="129.54" y="160.02"/>
<instance part="C28" gate="G$1" x="180.34" y="114.3"/>
<instance part="L6" gate="G$1" x="170.18" y="121.92" rot="R90"/>
<instance part="GND38" gate="1" x="180.34" y="106.68"/>
<instance part="GND39" gate="1" x="195.58" y="106.68"/>
<instance part="GND40" gate="1" x="208.28" y="106.68"/>
<instance part="JP1" gate="G$1" x="231.14" y="50.8"/>
<instance part="+3V5" gate="G$1" x="220.98" y="71.12"/>
<instance part="GND51" gate="1" x="220.98" y="45.72"/>
<instance part="X5" gate="G1" x="195.58" y="83.82" rot="MR0"/>
<instance part="GND52" gate="1" x="190.5" y="71.12"/>
<instance part="D2" gate="G$1" x="129.54" y="96.52" rot="R90"/>
<instance part="R2" gate="G$1" x="119.38" y="96.52" rot="R180"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="FL2" gate="G$1" pin="GND@5"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="147.32" y1="53.34" x2="147.32" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="FL2" gate="G$1" pin="GND@6"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND43" gate="1" pin="GND"/>
<pinref part="Y3" gate="G$1" pin="3"/>
<wire x1="114.3" y1="30.48" x2="114.3" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="175.26" y1="30.48" x2="175.26" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="1"/>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="187.96" y1="30.48" x2="187.96" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MCU2" gate="G$1" pin="GND"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="73.66" y1="35.56" x2="73.66" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="35.56" y1="38.1" x2="35.56" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="20.32" y1="48.26" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L8" gate="G$1" pin="2"/>
<pinref part="GND50" gate="1" pin="GND"/>
<wire x1="180.34" y1="71.12" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L7" gate="G$1" pin="2"/>
<pinref part="GND49" gate="1" pin="GND"/>
<wire x1="162.56" y1="71.12" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="157.48" y1="144.78" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="170.18" y1="144.78" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="182.88" y1="144.78" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="195.58" y1="144.78" x2="195.58" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="208.28" y1="144.78" x2="208.28" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="180.34" y1="111.76" x2="180.34" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="195.58" y1="111.76" x2="195.58" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="208.28" y1="111.76" x2="208.28" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="3"/>
<pinref part="GND51" gate="1" pin="GND"/>
<wire x1="226.06" y1="60.96" x2="220.98" y2="60.96" width="0.1524" layer="91"/>
<wire x1="220.98" y1="60.96" x2="220.98" y2="58.42" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="5"/>
<wire x1="220.98" y1="58.42" x2="220.98" y2="53.34" width="0.1524" layer="91"/>
<wire x1="220.98" y1="53.34" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
<wire x1="226.06" y1="58.42" x2="220.98" y2="58.42" width="0.1524" layer="91"/>
<junction x="220.98" y="58.42"/>
<pinref part="JP1" gate="G$1" pin="9"/>
<wire x1="226.06" y1="53.34" x2="220.98" y2="53.34" width="0.1524" layer="91"/>
<junction x="220.98" y="53.34"/>
</segment>
<segment>
<pinref part="X5" gate="G1" pin="2"/>
<pinref part="GND52" gate="1" pin="GND"/>
<wire x1="193.04" y1="81.28" x2="190.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="190.5" y1="81.28" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="134.62" y1="96.52" x2="139.7" y2="96.52" width="0.1524" layer="91"/>
<label x="139.7" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DIO9"/>
<wire x1="43.18" y1="96.52" x2="38.1" y2="96.52" width="0.1524" layer="91"/>
<label x="38.1" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DIO10"/>
<wire x1="43.18" y1="93.98" x2="38.1" y2="93.98" width="0.1524" layer="91"/>
<label x="38.1" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VDDS" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="VDDS"/>
<wire x1="60.96" y1="127" x2="60.96" y2="132.08" width="0.1524" layer="91"/>
<pinref part="MCU2" gate="G$1" pin="VDDS3"/>
<wire x1="71.12" y1="127" x2="71.12" y2="132.08" width="0.1524" layer="91"/>
<wire x1="71.12" y1="132.08" x2="66.04" y2="132.08" width="0.1524" layer="91"/>
<junction x="60.96" y="132.08"/>
<pinref part="MCU2" gate="G$1" pin="VDDS2"/>
<wire x1="66.04" y1="132.08" x2="60.96" y2="132.08" width="0.1524" layer="91"/>
<wire x1="66.04" y1="127" x2="66.04" y2="132.08" width="0.1524" layer="91"/>
<junction x="66.04" y="132.08"/>
<pinref part="MCU2" gate="G$1" pin="VDDS_DCDC"/>
<wire x1="81.28" y1="127" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<wire x1="81.28" y1="132.08" x2="71.12" y2="132.08" width="0.1524" layer="91"/>
<junction x="71.12" y="132.08"/>
<wire x1="60.96" y1="132.08" x2="60.96" y2="139.7" width="0.1524" layer="91"/>
<label x="60.96" y="139.7" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="83.82" width="0.1524" layer="91"/>
<label x="20.32" y="83.82" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="L5" gate="G$1" pin="2"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="149.86" y1="154.94" x2="157.48" y2="154.94" width="0.1524" layer="91"/>
<wire x1="157.48" y1="154.94" x2="170.18" y2="154.94" width="0.1524" layer="91"/>
<wire x1="170.18" y1="154.94" x2="182.88" y2="154.94" width="0.1524" layer="91"/>
<wire x1="182.88" y1="154.94" x2="195.58" y2="154.94" width="0.1524" layer="91"/>
<wire x1="195.58" y1="154.94" x2="208.28" y2="154.94" width="0.1524" layer="91"/>
<wire x1="208.28" y1="154.94" x2="208.28" y2="152.4" width="0.1524" layer="91"/>
<wire x1="195.58" y1="152.4" x2="195.58" y2="154.94" width="0.1524" layer="91"/>
<junction x="195.58" y="154.94"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="182.88" y1="152.4" x2="182.88" y2="154.94" width="0.1524" layer="91"/>
<junction x="182.88" y="154.94"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="170.18" y1="152.4" x2="170.18" y2="154.94" width="0.1524" layer="91"/>
<junction x="170.18" y="154.94"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="157.48" y1="152.4" x2="157.48" y2="154.94" width="0.1524" layer="91"/>
<junction x="157.48" y="154.94"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="157.48" y1="154.94" x2="157.48" y2="160.02" width="0.1524" layer="91"/>
<label x="157.48" y="160.02" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VDDR" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="VDDR"/>
<wire x1="76.2" y1="127" x2="76.2" y2="134.62" width="0.1524" layer="91"/>
<pinref part="MCU2" gate="G$1" pin="VDDR_RF"/>
<wire x1="91.44" y1="127" x2="91.44" y2="134.62" width="0.1524" layer="91"/>
<wire x1="91.44" y1="134.62" x2="76.2" y2="134.62" width="0.1524" layer="91"/>
<wire x1="76.2" y1="134.62" x2="76.2" y2="139.7" width="0.1524" layer="91"/>
<junction x="76.2" y="134.62"/>
<label x="76.2" y="139.7" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="L6" gate="G$1" pin="2"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="175.26" y1="121.92" x2="180.34" y2="121.92" width="0.1524" layer="91"/>
<wire x1="180.34" y1="121.92" x2="187.96" y2="121.92" width="0.1524" layer="91"/>
<wire x1="187.96" y1="121.92" x2="195.58" y2="121.92" width="0.1524" layer="91"/>
<wire x1="195.58" y1="121.92" x2="208.28" y2="121.92" width="0.1524" layer="91"/>
<wire x1="208.28" y1="121.92" x2="208.28" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="195.58" y1="119.38" x2="195.58" y2="121.92" width="0.1524" layer="91"/>
<junction x="195.58" y="121.92"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="180.34" y1="119.38" x2="180.34" y2="121.92" width="0.1524" layer="91"/>
<junction x="180.34" y="121.92"/>
<wire x1="187.96" y1="121.92" x2="187.96" y2="127" width="0.1524" layer="91"/>
<junction x="187.96" y="121.92"/>
<label x="187.96" y="127" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="DCDC_SW" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DCDC_SW"/>
<wire x1="86.36" y1="127" x2="86.36" y2="139.7" width="0.1524" layer="91"/>
<label x="86.36" y="139.7" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="165.1" y1="121.92" x2="157.48" y2="121.92" width="0.1524" layer="91"/>
<label x="157.48" y="121.92" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="RF_P"/>
<wire x1="109.22" y1="73.66" x2="127" y2="73.66" width="0.1524" layer="91"/>
<wire x1="127" y1="73.66" x2="127" y2="86.36" width="0.1524" layer="91"/>
<pinref part="FL2" gate="G$1" pin="BP2"/>
<wire x1="127" y1="86.36" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="142.24" y1="86.36" x2="142.24" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="RF_N"/>
<wire x1="109.22" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<wire x1="127" y1="68.58" x2="127" y2="50.8" width="0.1524" layer="91"/>
<pinref part="FL2" gate="G$1" pin="BP1"/>
<wire x1="127" y1="50.8" x2="142.24" y2="50.8" width="0.1524" layer="91"/>
<wire x1="142.24" y1="50.8" x2="142.24" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="RX_TX"/>
<wire x1="109.22" y1="63.5" x2="124.46" y2="63.5" width="0.1524" layer="91"/>
<wire x1="124.46" y1="63.5" x2="124.46" y2="88.9" width="0.1524" layer="91"/>
<pinref part="FL2" gate="G$1" pin="GND"/>
<wire x1="124.46" y1="88.9" x2="147.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="147.32" y1="88.9" x2="147.32" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="FL2" gate="G$1" pin="UBP"/>
<wire x1="152.4" y1="81.28" x2="152.4" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="152.4" y1="83.82" x2="162.56" y2="83.82" width="0.1524" layer="91"/>
<pinref part="L7" gate="G$1" pin="1"/>
<wire x1="162.56" y1="83.82" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<wire x1="162.56" y1="81.28" x2="162.56" y2="83.82" width="0.1524" layer="91"/>
<junction x="162.56" y="83.82"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="X24M_P"/>
<wire x1="109.22" y1="45.72" x2="109.22" y2="38.1" width="0.1524" layer="91"/>
<pinref part="Y3" gate="G$1" pin="1"/>
<wire x1="109.22" y1="38.1" x2="111.76" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="X24M_N"/>
<wire x1="109.22" y1="48.26" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<wire x1="119.38" y1="48.26" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<pinref part="Y3" gate="G$1" pin="2"/>
<wire x1="119.38" y1="38.1" x2="116.84" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XTAL1" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="175.26" y1="38.1" x2="175.26" y2="40.132" width="0.1524" layer="91"/>
<pinref part="Y4" gate="G$1" pin="2"/>
<wire x1="175.26" y1="40.132" x2="175.26" y2="45.72" width="0.1524" layer="91"/>
<wire x1="179.07" y1="40.132" x2="175.26" y2="40.132" width="0.1524" layer="91"/>
<junction x="175.26" y="40.132"/>
<label x="175.26" y="45.72" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="MCU2" gate="G$1" pin="X32K_Q1"/>
<wire x1="109.22" y1="55.88" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<label x="114.3" y="55.88" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="XTAL2" class="0">
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="187.96" y1="38.1" x2="187.96" y2="40.132" width="0.1524" layer="91"/>
<pinref part="Y4" gate="G$1" pin="1"/>
<wire x1="187.96" y1="40.132" x2="187.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="184.15" y1="40.132" x2="187.96" y2="40.132" width="0.1524" layer="91"/>
<junction x="187.96" y="40.132"/>
<label x="187.96" y="45.72" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="MCU2" gate="G$1" pin="X32K_Q2"/>
<wire x1="109.22" y1="53.34" x2="114.3" y2="53.34" width="0.1524" layer="91"/>
<label x="114.3" y="53.34" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="JTAG_TDO" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DIO16/JTAG_TDO"/>
<wire x1="43.18" y1="78.74" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<label x="40.64" y="78.74" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="6"/>
<wire x1="248.92" y1="58.42" x2="254" y2="58.42" width="0.1524" layer="91"/>
<label x="254" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="JTAG_TDI" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DIO17/JTAG_TDI"/>
<wire x1="43.18" y1="76.2" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<label x="40.64" y="76.2" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="8"/>
<wire x1="248.92" y1="55.88" x2="254" y2="55.88" width="0.1524" layer="91"/>
<label x="254" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="JTAG_TMSC" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="JTAG_TMSC"/>
<wire x1="43.18" y1="73.66" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<label x="40.64" y="73.66" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="248.92" y1="63.5" x2="254" y2="63.5" width="0.1524" layer="91"/>
<label x="254" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="JTAG_TCKC" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DJTAG_TCKC"/>
<wire x1="43.18" y1="71.12" x2="40.64" y2="71.12" width="0.1524" layer="91"/>
<label x="40.64" y="71.12" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="4"/>
<wire x1="248.92" y1="60.96" x2="254" y2="60.96" width="0.1524" layer="91"/>
<label x="254" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DCOUPL"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="43.18" y1="50.8" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="50.8" x2="35.56" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="175.26" y1="83.82" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="180.34" y1="83.82" x2="193.04" y2="83.82" width="0.1524" layer="91"/>
<wire x1="180.34" y1="81.28" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<junction x="180.34" y="83.82"/>
<pinref part="X5" gate="G1" pin="1"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DIO24"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="109.22" y1="96.52" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N_RESET" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="20.32" y1="66.04" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<pinref part="MCU2" gate="G$1" pin="RESET_N"/>
<wire x1="20.32" y1="60.96" x2="20.32" y2="55.88" width="0.1524" layer="91"/>
<wire x1="43.18" y1="60.96" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<junction x="20.32" y="60.96"/>
<wire x1="20.32" y1="60.96" x2="12.7" y2="60.96" width="0.1524" layer="91"/>
<label x="12.7" y="60.96" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="10"/>
<wire x1="248.92" y1="53.34" x2="254" y2="53.34" width="0.1524" layer="91"/>
<label x="254" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="139.7" y1="154.94" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="129.54" y1="154.94" x2="129.54" y2="157.48" width="0.1524" layer="91"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="226.06" y1="63.5" x2="220.98" y2="63.5" width="0.1524" layer="91"/>
<wire x1="220.98" y1="63.5" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="124.46" y1="96.52" x2="127" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SURT_TX" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DIO29"/>
<wire x1="109.22" y1="83.82" x2="114.3" y2="83.82" width="0.1524" layer="91"/>
<label x="114.3" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SURT_RX" class="0">
<segment>
<pinref part="MCU2" gate="G$1" pin="DIO30"/>
<wire x1="109.22" y1="81.28" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<label x="114.3" y="81.28" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="X3" gate="-1" x="198.12" y="35.56" rot="R180"/>
<instance part="X3" gate="-2" x="198.12" y="40.64" rot="R180"/>
<instance part="J2" gate="J$1" x="165.1" y="38.1" rot="R180"/>
<instance part="Q1" gate="NMOS" x="78.74" y="114.3" rot="MR270"/>
<instance part="R4" gate="G$1" x="68.58" y="121.92" rot="MR270"/>
<instance part="R5" gate="G$1" x="91.44" y="121.92" rot="MR270"/>
<instance part="Q2" gate="NMOS" x="78.74" y="76.2" rot="MR270"/>
<instance part="R6" gate="G$1" x="68.58" y="83.82" rot="MR270"/>
<instance part="R7" gate="G$1" x="91.44" y="83.82" rot="MR270"/>
<instance part="U3" gate="G$1" x="180.34" y="144.78"/>
<instance part="X2" gate="-1" x="139.7" y="144.78"/>
<instance part="X2" gate="-2" x="139.7" y="139.7"/>
<instance part="D1" gate="G$1" x="193.04" y="127"/>
<instance part="R3" gate="G$1" x="193.04" y="137.16" rot="R90"/>
<instance part="D3" gate="G$1" x="149.86" y="144.78"/>
<instance part="GND5" gate="1" x="147.32" y="132.08"/>
<instance part="GND6" gate="1" x="180.34" y="132.08"/>
<instance part="GND8" gate="1" x="193.04" y="114.3"/>
<instance part="C7" gate="G$1" x="200.66" y="137.16"/>
<instance part="C8" gate="G$1" x="162.56" y="137.16"/>
<instance part="GND9" gate="1" x="162.56" y="127"/>
<instance part="GND10" gate="1" x="200.66" y="127"/>
<instance part="C9" gate="G$1" x="205.74" y="73.66"/>
<instance part="GND14" gate="1" x="205.74" y="66.04"/>
<instance part="P+1" gate="1" x="162.56" y="152.4"/>
<instance part="P+4" gate="1" x="205.74" y="83.82"/>
<instance part="U4" gate="G$1" x="165.1" y="83.82"/>
<instance part="C10" gate="G$1" x="149.86" y="73.66"/>
<instance part="C11" gate="G$1" x="180.34" y="73.66"/>
<instance part="GND15" gate="1" x="180.34" y="63.5"/>
<instance part="GND16" gate="1" x="149.86" y="63.5"/>
<instance part="GND17" gate="1" x="165.1" y="63.5"/>
<instance part="X4" gate="-1" x="139.7" y="111.76"/>
<instance part="X4" gate="-2" x="139.7" y="106.68"/>
<instance part="GND20" gate="1" x="149.86" y="101.6"/>
<instance part="P+7" gate="1" x="149.86" y="119.38"/>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J2" gate="J$1" pin="3"/>
<wire x1="157.48" y1="35.56" x2="154.94" y2="35.56" width="0.1524" layer="91"/>
<label x="154.94" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X2" gate="-2" pin="KL"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="144.78" y1="139.7" x2="147.32" y2="139.7" width="0.1524" layer="91"/>
<wire x1="147.32" y1="139.7" x2="147.32" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="ADJ"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="180.34" y1="137.16" x2="180.34" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="193.04" y1="121.92" x2="193.04" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="162.56" y1="134.62" x2="162.56" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="200.66" y1="134.62" x2="200.66" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="205.74" y1="71.12" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="149.86" y1="71.12" x2="149.86" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="180.34" y1="66.04" x2="180.34" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="ADJ"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="165.1" y1="76.2" x2="165.1" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="-2" pin="KL"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="144.78" y1="106.68" x2="149.86" y2="106.68" width="0.1524" layer="91"/>
<wire x1="149.86" y1="106.68" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="J2" gate="J$1" pin="2"/>
<wire x1="157.48" y1="38.1" x2="154.94" y2="38.1" width="0.1524" layer="91"/>
<label x="154.94" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="J2" gate="J$1" pin="1"/>
<wire x1="157.48" y1="40.64" x2="154.94" y2="40.64" width="0.1524" layer="91"/>
<label x="154.94" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="Q1" gate="NMOS" pin="G"/>
<wire x1="81.28" y1="119.38" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="81.28" y1="132.08" x2="91.44" y2="132.08" width="0.1524" layer="91"/>
<wire x1="91.44" y1="132.08" x2="91.44" y2="127" width="0.1524" layer="91"/>
<wire x1="91.44" y1="132.08" x2="91.44" y2="137.16" width="0.1524" layer="91"/>
<junction x="91.44" y="132.08"/>
<label x="91.44" y="137.16" size="1.27" layer="95" rot="MR90" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="91.44" y1="88.9" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<pinref part="Q2" gate="NMOS" pin="G"/>
<wire x1="81.28" y1="81.28" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
<wire x1="81.28" y1="93.98" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<junction x="91.44" y="93.98"/>
<wire x1="91.44" y1="93.98" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<label x="91.44" y="99.06" size="1.27" layer="95" rot="MR90" xref="yes"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="180.34" y1="78.74" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="OUT"/>
<wire x1="180.34" y1="83.82" x2="172.72" y2="83.82" width="0.1524" layer="91"/>
<wire x1="187.96" y1="83.82" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<junction x="180.34" y="83.82"/>
<label x="187.96" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SURT_TX" class="0">
<segment>
<pinref part="Q1" gate="NMOS" pin="S"/>
<wire x1="83.82" y1="111.76" x2="91.44" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="91.44" y1="111.76" x2="96.52" y2="111.76" width="0.1524" layer="91"/>
<wire x1="91.44" y1="116.84" x2="91.44" y2="111.76" width="0.1524" layer="91"/>
<junction x="91.44" y="111.76"/>
<label x="96.52" y="111.76" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="SURT_RX" class="0">
<segment>
<pinref part="Q2" gate="NMOS" pin="S"/>
<wire x1="83.82" y1="73.66" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<wire x1="91.44" y1="73.66" x2="99.06" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="91.44" y1="78.74" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<junction x="91.44" y="73.66"/>
<label x="99.06" y="73.66" size="1.27" layer="95" rot="MR180" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="68.58" y1="127" x2="68.58" y2="137.16" width="0.1524" layer="91"/>
<label x="68.58" y="137.16" size="1.27" layer="95" rot="MR90" xref="yes"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="68.58" y1="88.9" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<label x="68.58" y="99.06" size="1.27" layer="95" rot="MR90" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="OUT"/>
<wire x1="187.96" y1="144.78" x2="193.04" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="193.04" y1="144.78" x2="200.66" y2="144.78" width="0.1524" layer="91"/>
<wire x1="200.66" y1="144.78" x2="210.82" y2="144.78" width="0.1524" layer="91"/>
<wire x1="200.66" y1="142.24" x2="200.66" y2="144.78" width="0.1524" layer="91"/>
<junction x="200.66" y="144.78"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="193.04" y1="142.24" x2="193.04" y2="144.78" width="0.1524" layer="91"/>
<junction x="193.04" y="144.78"/>
<label x="210.82" y="144.78" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="205.74" y1="78.74" x2="205.74" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN"/>
<wire x1="157.48" y1="83.82" x2="149.86" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="149.86" y1="83.82" x2="137.16" y2="83.82" width="0.1524" layer="91"/>
<wire x1="149.86" y1="78.74" x2="149.86" y2="83.82" width="0.1524" layer="91"/>
<junction x="149.86" y="83.82"/>
<label x="137.16" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X4" gate="-1" pin="KL"/>
<wire x1="144.78" y1="111.76" x2="149.86" y2="111.76" width="0.1524" layer="91"/>
<pinref part="P+7" gate="1" pin="+5V"/>
<wire x1="149.86" y1="116.84" x2="149.86" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SURT_TX_5V" class="0">
<segment>
<pinref part="X3" gate="-2" pin="KL"/>
<wire x1="193.04" y1="40.64" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
<label x="187.96" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="Q1" gate="NMOS" pin="D"/>
<wire x1="73.66" y1="111.76" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="68.58" y1="111.76" x2="60.96" y2="111.76" width="0.1524" layer="91"/>
<wire x1="68.58" y1="116.84" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<junction x="68.58" y="111.76"/>
<label x="60.96" y="111.76" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="SURT_RX_5V" class="0">
<segment>
<pinref part="X3" gate="-1" pin="KL"/>
<wire x1="193.04" y1="35.56" x2="187.96" y2="35.56" width="0.1524" layer="91"/>
<label x="187.96" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="Q2" gate="NMOS" pin="D"/>
<wire x1="73.66" y1="73.66" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="68.58" y1="73.66" x2="60.96" y2="73.66" width="0.1524" layer="91"/>
<wire x1="68.58" y1="78.74" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<junction x="68.58" y="73.66"/>
<label x="60.96" y="73.66" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="193.04" y1="132.08" x2="193.04" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="X2" gate="-1" pin="KL"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="144.78" y1="144.78" x2="147.32" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="U3" gate="G$1" pin="IN"/>
<wire x1="152.4" y1="144.78" x2="162.56" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="162.56" y1="144.78" x2="172.72" y2="144.78" width="0.1524" layer="91"/>
<wire x1="162.56" y1="142.24" x2="162.56" y2="144.78" width="0.1524" layer="91"/>
<junction x="162.56" y="144.78"/>
<pinref part="P+1" gate="1" pin="+12V"/>
<wire x1="162.56" y1="149.86" x2="162.56" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
