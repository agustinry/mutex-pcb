<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.0.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="rcl" urn="urn:adsk.eagle:library:334">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="R0402" urn="urn:adsk.eagle:footprint:23043/3" library_version="5">
<description>&lt;b&gt;Chip RESISTOR 0402 EIA (1005 Metric)&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1" y1="0.483" x2="1" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="0.483" x2="1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="-0.483" x2="-1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="-0.483" x2="-1" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.35" x2="0.1999" y2="0.35" layer="35"/>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:23044/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:23045/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W" urn="urn:adsk.eagle:footprint:23046/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:23047/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W" urn="urn:adsk.eagle:footprint:23048/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210" urn="urn:adsk.eagle:footprint:23049/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W" urn="urn:adsk.eagle:footprint:23050/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010" urn="urn:adsk.eagle:footprint:23051/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W" urn="urn:adsk.eagle:footprint:23052/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012" urn="urn:adsk.eagle:footprint:23053/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W" urn="urn:adsk.eagle:footprint:23054/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512" urn="urn:adsk.eagle:footprint:23055/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W" urn="urn:adsk.eagle:footprint:23056/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216" urn="urn:adsk.eagle:footprint:23057/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W" urn="urn:adsk.eagle:footprint:23058/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225" urn="urn:adsk.eagle:footprint:23059/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W" urn="urn:adsk.eagle:footprint:23060/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025" urn="urn:adsk.eagle:footprint:23061/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W" urn="urn:adsk.eagle:footprint:23062/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332" urn="urn:adsk.eagle:footprint:23063/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W" urn="urn:adsk.eagle:footprint:25646/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805" urn="urn:adsk.eagle:footprint:23065/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206" urn="urn:adsk.eagle:footprint:23066/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406" urn="urn:adsk.eagle:footprint:23067/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012" urn="urn:adsk.eagle:footprint:23068/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309" urn="urn:adsk.eagle:footprint:23069/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216" urn="urn:adsk.eagle:footprint:23070/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516" urn="urn:adsk.eagle:footprint:23071/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923" urn="urn:adsk.eagle:footprint:23072/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5" urn="urn:adsk.eagle:footprint:22991/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7" urn="urn:adsk.eagle:footprint:22998/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V" urn="urn:adsk.eagle:footprint:22999/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10" urn="urn:adsk.eagle:footprint:22992/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12" urn="urn:adsk.eagle:footprint:22993/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15" urn="urn:adsk.eagle:footprint:22997/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V" urn="urn:adsk.eagle:footprint:22994/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V" urn="urn:adsk.eagle:footprint:22995/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7" urn="urn:adsk.eagle:footprint:22996/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10" urn="urn:adsk.eagle:footprint:23073/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12" urn="urn:adsk.eagle:footprint:23074/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V" urn="urn:adsk.eagle:footprint:23075/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12" urn="urn:adsk.eagle:footprint:23076/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15" urn="urn:adsk.eagle:footprint:23077/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V" urn="urn:adsk.eagle:footprint:23078/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15" urn="urn:adsk.eagle:footprint:23079/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V" urn="urn:adsk.eagle:footprint:23080/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17" urn="urn:adsk.eagle:footprint:23081/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22" urn="urn:adsk.eagle:footprint:23082/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V" urn="urn:adsk.eagle:footprint:23083/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22" urn="urn:adsk.eagle:footprint:23084/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V" urn="urn:adsk.eagle:footprint:23085/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15" urn="urn:adsk.eagle:footprint:23086/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22" urn="urn:adsk.eagle:footprint:23087/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V" urn="urn:adsk.eagle:footprint:23088/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12" urn="urn:adsk.eagle:footprint:23089/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17" urn="urn:adsk.eagle:footprint:23090/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0" urn="urn:adsk.eagle:footprint:23091/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R" urn="urn:adsk.eagle:footprint:23092/1" library_version="5">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W" urn="urn:adsk.eagle:footprint:23093/1" library_version="5">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R" urn="urn:adsk.eagle:footprint:25676/1" library_version="5">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W" urn="urn:adsk.eagle:footprint:25677/1" library_version="5">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R" urn="urn:adsk.eagle:footprint:25678/1" library_version="5">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W" urn="urn:adsk.eagle:footprint:25679/1" library_version="5">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V" urn="urn:adsk.eagle:footprint:23098/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15" urn="urn:adsk.eagle:footprint:23099/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX" urn="urn:adsk.eagle:footprint:23100/1" library_version="5">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201" urn="urn:adsk.eagle:footprint:25683/1" library_version="5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52" urn="urn:adsk.eagle:footprint:25684/1" library_version="5">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53" urn="urn:adsk.eagle:footprint:25685/1" library_version="5">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54" urn="urn:adsk.eagle:footprint:25686/1" library_version="5">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55" urn="urn:adsk.eagle:footprint:25687/1" library_version="5">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56" urn="urn:adsk.eagle:footprint:25688/1" library_version="5">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55" urn="urn:adsk.eagle:footprint:25689/1" library_version="5">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60" urn="urn:adsk.eagle:footprint:25690/1" library_version="5">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527" urn="urn:adsk.eagle:footprint:13246/1" library_version="5">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001" urn="urn:adsk.eagle:footprint:25692/1" library_version="5">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002" urn="urn:adsk.eagle:footprint:25693/1" library_version="5">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2" urn="urn:adsk.eagle:footprint:25694/1" library_version="5">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515" urn="urn:adsk.eagle:footprint:25695/1" library_version="5">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527" urn="urn:adsk.eagle:footprint:25696/1" library_version="5">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927" urn="urn:adsk.eagle:footprint:25697/1" library_version="5">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218" urn="urn:adsk.eagle:footprint:25698/1" library_version="5">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R" urn="urn:adsk.eagle:footprint:25699/1" library_version="5">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="PRL1632" urn="urn:adsk.eagle:footprint:25700/1" library_version="5">
<description>&lt;b&gt;PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)&lt;/b&gt;&lt;p&gt;
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<wire x1="0.7275" y1="-1.5228" x2="-0.7277" y2="-1.5228" width="0.1524" layer="51"/>
<wire x1="0.7275" y1="1.5228" x2="-0.7152" y2="1.5228" width="0.1524" layer="51"/>
<smd name="2" x="0.822" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="1" x="-0.822" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-1.4" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.4" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.4" y2="1.6" layer="51"/>
<rectangle x1="0.4" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="R01005" urn="urn:adsk.eagle:footprint:25701/1" library_version="5">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="R0402" urn="urn:adsk.eagle:package:23547/2" type="box" library_version="5">
<description>Chip RESISTOR 0402 EIA (1005 Metric)</description>
<packageinstances>
<packageinstance name="R0402"/>
</packageinstances>
</package3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:23555/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:23553/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
<package3d name="R0805W" urn="urn:adsk.eagle:package:23537/1" type="box" library_version="5">
<description>RESISTOR wave soldering</description>
<packageinstances>
<packageinstance name="R0805W"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:23540/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R1206W" urn="urn:adsk.eagle:package:23539/1" type="box" library_version="5">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1206W"/>
</packageinstances>
</package3d>
<package3d name="R1210" urn="urn:adsk.eagle:package:23554/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1210"/>
</packageinstances>
</package3d>
<package3d name="R1210W" urn="urn:adsk.eagle:package:23541/1" type="box" library_version="5">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1210W"/>
</packageinstances>
</package3d>
<package3d name="R2010" urn="urn:adsk.eagle:package:23551/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2010"/>
</packageinstances>
</package3d>
<package3d name="R2010W" urn="urn:adsk.eagle:package:23542/1" type="box" library_version="5">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2010W"/>
</packageinstances>
</package3d>
<package3d name="R2012" urn="urn:adsk.eagle:package:23543/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2012"/>
</packageinstances>
</package3d>
<package3d name="R2012W" urn="urn:adsk.eagle:package:23544/1" type="box" library_version="5">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2012W"/>
</packageinstances>
</package3d>
<package3d name="R2512" urn="urn:adsk.eagle:package:23545/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2512"/>
</packageinstances>
</package3d>
<package3d name="R2512W" urn="urn:adsk.eagle:package:23565/1" type="box" library_version="5">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2512W"/>
</packageinstances>
</package3d>
<package3d name="R3216" urn="urn:adsk.eagle:package:23557/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3216"/>
</packageinstances>
</package3d>
<package3d name="R3216W" urn="urn:adsk.eagle:package:23548/1" type="box" library_version="5">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3216W"/>
</packageinstances>
</package3d>
<package3d name="R3225" urn="urn:adsk.eagle:package:23549/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3225"/>
</packageinstances>
</package3d>
<package3d name="R3225W" urn="urn:adsk.eagle:package:23550/1" type="box" library_version="5">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3225W"/>
</packageinstances>
</package3d>
<package3d name="R5025" urn="urn:adsk.eagle:package:23552/2" type="model" library_version="5">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R5025"/>
</packageinstances>
</package3d>
<package3d name="R5025W" urn="urn:adsk.eagle:package:23558/1" type="box" library_version="5">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R5025W"/>
</packageinstances>
</package3d>
<package3d name="R6332" urn="urn:adsk.eagle:package:23559/2" type="model" library_version="5">
<description>RESISTOR
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332"/>
</packageinstances>
</package3d>
<package3d name="R6332W" urn="urn:adsk.eagle:package:26078/1" type="box" library_version="5">
<description>RESISTOR wave soldering
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332W"/>
</packageinstances>
</package3d>
<package3d name="M0805" urn="urn:adsk.eagle:package:23556/1" type="box" library_version="5">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M0805"/>
</packageinstances>
</package3d>
<package3d name="M1206" urn="urn:adsk.eagle:package:23566/1" type="box" library_version="5">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M1206"/>
</packageinstances>
</package3d>
<package3d name="M1406" urn="urn:adsk.eagle:package:23569/1" type="box" library_version="5">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M1406"/>
</packageinstances>
</package3d>
<package3d name="M2012" urn="urn:adsk.eagle:package:23561/1" type="box" library_version="5">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M2012"/>
</packageinstances>
</package3d>
<package3d name="M2309" urn="urn:adsk.eagle:package:23562/1" type="box" library_version="5">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M2309"/>
</packageinstances>
</package3d>
<package3d name="M3216" urn="urn:adsk.eagle:package:23563/1" type="box" library_version="5">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M3216"/>
</packageinstances>
</package3d>
<package3d name="M3516" urn="urn:adsk.eagle:package:23573/1" type="box" library_version="5">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M3516"/>
</packageinstances>
</package3d>
<package3d name="M5923" urn="urn:adsk.eagle:package:23564/1" type="box" library_version="5">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M5923"/>
</packageinstances>
</package3d>
<package3d name="0204/5" urn="urn:adsk.eagle:package:23488/1" type="box" library_version="5">
<description>RESISTOR
type 0204, grid 5 mm</description>
<packageinstances>
<packageinstance name="0204/5"/>
</packageinstances>
</package3d>
<package3d name="0204/7" urn="urn:adsk.eagle:package:23498/1" type="box" library_version="5">
<description>RESISTOR
type 0204, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0204/7"/>
</packageinstances>
</package3d>
<package3d name="0204V" urn="urn:adsk.eagle:package:23495/1" type="box" library_version="5">
<description>RESISTOR
type 0204, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0204V"/>
</packageinstances>
</package3d>
<package3d name="0207/10" urn="urn:adsk.eagle:package:23491/1" type="box" library_version="5">
<description>RESISTOR
type 0207, grid 10 mm</description>
<packageinstances>
<packageinstance name="0207/10"/>
</packageinstances>
</package3d>
<package3d name="0207/12" urn="urn:adsk.eagle:package:23489/1" type="box" library_version="5">
<description>RESISTOR
type 0207, grid 12 mm</description>
<packageinstances>
<packageinstance name="0207/12"/>
</packageinstances>
</package3d>
<package3d name="0207/15" urn="urn:adsk.eagle:package:23492/1" type="box" library_version="5">
<description>RESISTOR
type 0207, grid 15mm</description>
<packageinstances>
<packageinstance name="0207/15"/>
</packageinstances>
</package3d>
<package3d name="0207/2V" urn="urn:adsk.eagle:package:23490/1" type="box" library_version="5">
<description>RESISTOR
type 0207, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0207/2V"/>
</packageinstances>
</package3d>
<package3d name="0207/5V" urn="urn:adsk.eagle:package:23502/1" type="box" library_version="5">
<description>RESISTOR
type 0207, grid 5 mm</description>
<packageinstances>
<packageinstance name="0207/5V"/>
</packageinstances>
</package3d>
<package3d name="0207/7" urn="urn:adsk.eagle:package:23493/1" type="box" library_version="5">
<description>RESISTOR
type 0207, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0207/7"/>
</packageinstances>
</package3d>
<package3d name="0309/10" urn="urn:adsk.eagle:package:23567/1" type="box" library_version="5">
<description>RESISTOR
type 0309, grid 10mm</description>
<packageinstances>
<packageinstance name="0309/10"/>
</packageinstances>
</package3d>
<package3d name="0309/12" urn="urn:adsk.eagle:package:23571/1" type="box" library_version="5">
<description>RESISTOR
type 0309, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0309/12"/>
</packageinstances>
</package3d>
<package3d name="0309V" urn="urn:adsk.eagle:package:23572/1" type="box" library_version="5">
<description>RESISTOR
type 0309, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0309V"/>
</packageinstances>
</package3d>
<package3d name="0411/12" urn="urn:adsk.eagle:package:23578/1" type="box" library_version="5">
<description>RESISTOR
type 0411, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0411/12"/>
</packageinstances>
</package3d>
<package3d name="0411/15" urn="urn:adsk.eagle:package:23568/1" type="box" library_version="5">
<description>RESISTOR
type 0411, grid 15 mm</description>
<packageinstances>
<packageinstance name="0411/15"/>
</packageinstances>
</package3d>
<package3d name="0411V" urn="urn:adsk.eagle:package:23570/1" type="box" library_version="5">
<description>RESISTOR
type 0411, grid 3.81 mm</description>
<packageinstances>
<packageinstance name="0411V"/>
</packageinstances>
</package3d>
<package3d name="0414/15" urn="urn:adsk.eagle:package:23579/1" type="box" library_version="5">
<description>RESISTOR
type 0414, grid 15 mm</description>
<packageinstances>
<packageinstance name="0414/15"/>
</packageinstances>
</package3d>
<package3d name="0414V" urn="urn:adsk.eagle:package:23574/1" type="box" library_version="5">
<description>RESISTOR
type 0414, grid 5 mm</description>
<packageinstances>
<packageinstance name="0414V"/>
</packageinstances>
</package3d>
<package3d name="0617/17" urn="urn:adsk.eagle:package:23575/1" type="box" library_version="5">
<description>RESISTOR
type 0617, grid 17.5 mm</description>
<packageinstances>
<packageinstance name="0617/17"/>
</packageinstances>
</package3d>
<package3d name="0617/22" urn="urn:adsk.eagle:package:23577/1" type="box" library_version="5">
<description>RESISTOR
type 0617, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0617/22"/>
</packageinstances>
</package3d>
<package3d name="0617V" urn="urn:adsk.eagle:package:23576/1" type="box" library_version="5">
<description>RESISTOR
type 0617, grid 5 mm</description>
<packageinstances>
<packageinstance name="0617V"/>
</packageinstances>
</package3d>
<package3d name="0922/22" urn="urn:adsk.eagle:package:23580/1" type="box" library_version="5">
<description>RESISTOR
type 0922, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0922/22"/>
</packageinstances>
</package3d>
<package3d name="P0613V" urn="urn:adsk.eagle:package:23582/1" type="box" library_version="5">
<description>RESISTOR
type 0613, grid 5 mm</description>
<packageinstances>
<packageinstance name="P0613V"/>
</packageinstances>
</package3d>
<package3d name="P0613/15" urn="urn:adsk.eagle:package:23581/1" type="box" library_version="5">
<description>RESISTOR
type 0613, grid 15 mm</description>
<packageinstances>
<packageinstance name="P0613/15"/>
</packageinstances>
</package3d>
<package3d name="P0817/22" urn="urn:adsk.eagle:package:23583/1" type="box" library_version="5">
<description>RESISTOR
type 0817, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="P0817/22"/>
</packageinstances>
</package3d>
<package3d name="P0817V" urn="urn:adsk.eagle:package:23608/1" type="box" library_version="5">
<description>RESISTOR
type 0817, grid 6.35 mm</description>
<packageinstances>
<packageinstance name="P0817V"/>
</packageinstances>
</package3d>
<package3d name="V234/12" urn="urn:adsk.eagle:package:23592/1" type="box" library_version="5">
<description>RESISTOR
type V234, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="V234/12"/>
</packageinstances>
</package3d>
<package3d name="V235/17" urn="urn:adsk.eagle:package:23586/1" type="box" library_version="5">
<description>RESISTOR
type V235, grid 17.78 mm</description>
<packageinstances>
<packageinstance name="V235/17"/>
</packageinstances>
</package3d>
<package3d name="V526-0" urn="urn:adsk.eagle:package:23590/1" type="box" library_version="5">
<description>RESISTOR
type V526-0, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="V526-0"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102R" urn="urn:adsk.eagle:package:23591/1" type="box" library_version="5">
<description>CECC Size RC2211 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102W" urn="urn:adsk.eagle:package:23588/1" type="box" library_version="5">
<description>CECC Size RC2211 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204R" urn="urn:adsk.eagle:package:26109/1" type="box" library_version="5">
<description>CECC Size RC3715 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204W" urn="urn:adsk.eagle:package:26111/1" type="box" library_version="5">
<description>CECC Size RC3715 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207R" urn="urn:adsk.eagle:package:26113/1" type="box" library_version="5">
<description>CECC Size RC6123 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207W" urn="urn:adsk.eagle:package:26112/1" type="box" library_version="5">
<description>CECC Size RC6123 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207W"/>
</packageinstances>
</package3d>
<package3d name="0922V" urn="urn:adsk.eagle:package:23589/1" type="box" library_version="5">
<description>RESISTOR
type 0922, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0922V"/>
</packageinstances>
</package3d>
<package3d name="RDH/15" urn="urn:adsk.eagle:package:23595/1" type="box" library_version="5">
<description>RESISTOR
type RDH, grid 15 mm</description>
<packageinstances>
<packageinstance name="RDH/15"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102AX" urn="urn:adsk.eagle:package:23594/1" type="box" library_version="5">
<description>Mini MELF 0102 Axial</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102AX"/>
</packageinstances>
</package3d>
<package3d name="R0201" urn="urn:adsk.eagle:package:26117/1" type="box" library_version="5">
<description>RESISTOR chip
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R0201"/>
</packageinstances>
</package3d>
<package3d name="VTA52" urn="urn:adsk.eagle:package:26116/1" type="box" library_version="5">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR52
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA52"/>
</packageinstances>
</package3d>
<package3d name="VTA53" urn="urn:adsk.eagle:package:26118/1" type="box" library_version="5">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR53
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA53"/>
</packageinstances>
</package3d>
<package3d name="VTA54" urn="urn:adsk.eagle:package:26119/1" type="box" library_version="5">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR54
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA54"/>
</packageinstances>
</package3d>
<package3d name="VTA55" urn="urn:adsk.eagle:package:26120/1" type="box" library_version="5">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA55"/>
</packageinstances>
</package3d>
<package3d name="VTA56" urn="urn:adsk.eagle:package:26129/1" type="box" library_version="5">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR56
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA56"/>
</packageinstances>
</package3d>
<package3d name="VMTA55" urn="urn:adsk.eagle:package:26121/1" type="box" library_version="5">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTA55"/>
</packageinstances>
</package3d>
<package3d name="VMTB60" urn="urn:adsk.eagle:package:26122/1" type="box" library_version="5">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC60
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTB60"/>
</packageinstances>
</package3d>
<package3d name="R4527" urn="urn:adsk.eagle:package:13310/1" type="box" library_version="5">
<description>Package 4527
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<packageinstances>
<packageinstance name="R4527"/>
</packageinstances>
</package3d>
<package3d name="WSC0001" urn="urn:adsk.eagle:package:26123/1" type="box" library_version="5">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0001"/>
</packageinstances>
</package3d>
<package3d name="WSC0002" urn="urn:adsk.eagle:package:26125/1" type="box" library_version="5">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0002"/>
</packageinstances>
</package3d>
<package3d name="WSC01/2" urn="urn:adsk.eagle:package:26127/1" type="box" library_version="5">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC01/2"/>
</packageinstances>
</package3d>
<package3d name="WSC2515" urn="urn:adsk.eagle:package:26134/1" type="box" library_version="5">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC2515"/>
</packageinstances>
</package3d>
<package3d name="WSC4527" urn="urn:adsk.eagle:package:26126/1" type="box" library_version="5">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC4527"/>
</packageinstances>
</package3d>
<package3d name="WSC6927" urn="urn:adsk.eagle:package:26128/1" type="box" library_version="5">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC6927"/>
</packageinstances>
</package3d>
<package3d name="R1218" urn="urn:adsk.eagle:package:26131/1" type="box" library_version="5">
<description>CRCW1218 Thick Film, Rectangular Chip Resistors
Source: http://www.vishay.com .. dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R1218"/>
</packageinstances>
</package3d>
<package3d name="1812X7R" urn="urn:adsk.eagle:package:26130/1" type="box" library_version="5">
<description>Chip Monolithic Ceramic Capacitors Medium Voltage High Capacitance for General Use
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<packageinstances>
<packageinstance name="1812X7R"/>
</packageinstances>
</package3d>
<package3d name="PRL1632" urn="urn:adsk.eagle:package:26132/1" type="box" library_version="5">
<description>PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<packageinstances>
<packageinstance name="PRL1632"/>
</packageinstances>
</package3d>
<package3d name="R01005" urn="urn:adsk.eagle:package:26133/1" type="box" library_version="5">
<packageinstances>
<packageinstance name="R01005"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R-EU" urn="urn:adsk.eagle:symbol:23042/1" library_version="5">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" urn="urn:adsk.eagle:component:23791/16" prefix="R" uservalue="yes" library_version="5">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23547/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23553/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23537/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23540/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23539/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23554/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23541/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23551/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23542/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23543/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23544/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23545/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23565/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23557/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23548/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23549/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23550/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23552/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23558/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23559/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26078/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23556/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23566/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23569/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23561/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23562/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23563/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23573/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23564/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23488/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23498/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23495/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23491/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23489/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23492/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23490/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23502/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23493/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23567/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23571/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23572/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23578/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23568/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23570/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23579/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23574/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23575/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23577/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23576/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23580/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23582/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23581/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23583/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23608/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23592/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23586/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23590/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23591/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23588/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26109/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26111/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26113/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26112/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23589/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23595/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23594/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26117/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26116/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26118/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26119/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26120/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26129/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26121/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26122/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13310/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26123/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26125/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26127/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26134/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26126/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26128/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26131/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26130/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="PRL1632" package="PRL1632">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26132/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26133/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="R">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
</devicesets>
</library>
<library name="switch-coto" urn="urn:adsk.eagle:library:374">
<description>&lt;b&gt;COTO TECHNOLOGY&lt;/b&gt;&lt;p&gt;
Reed switch&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="CT10-XXXX-G1" urn="urn:adsk.eagle:footprint:27087/1" library_version="1">
<description>&lt;b&gt;CT10 Series Molded Switch&lt;/b&gt;&lt;p&gt;
Source: www.cotorelay.com .. Coto_Technology__CT10-1530-G1.pdf</description>
<wire x1="-6.275" y1="1.1" x2="6.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6.3" y1="1.1" x2="6.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="6.3" y1="-1.1" x2="-6.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-6.3" y1="-1.1" x2="-6.3" y2="1.1" width="0.2032" layer="21"/>
<smd name="1" x="-7.625" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="7.625" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-8.32" y="1.3302" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.32" y="-3.1082" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-8.2" y1="-0.575" x2="-6.4" y2="0.575" layer="51"/>
<rectangle x1="6.4" y1="-0.575" x2="8.2" y2="0.575" layer="51"/>
</package>
<package name="CT10-XXXX-A2" urn="urn:adsk.eagle:footprint:27086/1" library_version="1">
<description>&lt;b&gt;CT10 Series Molded Switch&lt;/b&gt;&lt;p&gt;
Source: www.cotorelay.com .. Coto_Technology__CT10-1530-G1.pdf</description>
<wire x1="-6.275" y1="1.1" x2="6.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6.3" y1="1.1" x2="6.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="6.3" y1="-1.1" x2="-6.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-6.3" y1="-1.1" x2="-6.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6.5" y1="-1.3" x2="-6.5" y2="1.3" width="0" layer="20"/>
<wire x1="-6.5" y1="1.3" x2="6.5" y2="1.3" width="0" layer="20"/>
<wire x1="6.5" y1="1.3" x2="6.5" y2="-1.3" width="0" layer="20"/>
<wire x1="6.5" y1="-1.3" x2="-6.5" y2="-1.3" width="0" layer="20"/>
<smd name="1" x="-7.5" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="7.5" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-8.32" y="1.3302" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.32" y="-3.1082" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-7.85" y1="-0.575" x2="-6.25" y2="0.575" layer="51"/>
<rectangle x1="6.3" y1="-0.575" x2="7.85" y2="0.575" layer="51"/>
</package>
<package name="CT10-XXXX-G4" urn="urn:adsk.eagle:footprint:27088/1" library_version="1">
<description>&lt;b&gt;CT10 Series Molded Switch&lt;/b&gt;&lt;p&gt;
Source: www.cotorelay.com .. Coto_Technology__CT10-1530-G1.pdf</description>
<wire x1="-6.275" y1="1.1" x2="6.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6.3" y1="1.1" x2="6.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="6.3" y1="-1.1" x2="-6.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-6.3" y1="-1.1" x2="-6.3" y2="1.1" width="0.2032" layer="21"/>
<smd name="1" x="-9.2" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="9.225" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-8.32" y="1.3302" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.32" y="-3.1082" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-9.8" y1="-0.575" x2="-6.4" y2="0.575" layer="51"/>
<rectangle x1="6.4" y1="-0.575" x2="9.8" y2="0.575" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="CT10-XXXX-G1" urn="urn:adsk.eagle:package:27089/1" type="box" library_version="1">
<description>CT10 Series Molded Switch
Source: www.cotorelay.com .. Coto_Technology__CT10-1530-G1.pdf</description>
<packageinstances>
<packageinstance name="CT10-XXXX-G1"/>
</packageinstances>
</package3d>
<package3d name="CT10-XXXX-A2" urn="urn:adsk.eagle:package:27090/1" type="box" library_version="1">
<description>CT10 Series Molded Switch
Source: www.cotorelay.com .. Coto_Technology__CT10-1530-G1.pdf</description>
<packageinstances>
<packageinstance name="CT10-XXXX-A2"/>
</packageinstances>
</package3d>
<package3d name="CT10-XXXX-G4" urn="urn:adsk.eagle:package:27091/1" type="box" library_version="1">
<description>CT10 Series Molded Switch
Source: www.cotorelay.com .. Coto_Technology__CT10-1530-G1.pdf</description>
<packageinstances>
<packageinstance name="CT10-XXXX-G4"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SWITCH-NO" urn="urn:adsk.eagle:symbol:27085/1" library_version="1">
<wire x1="-2.54" y1="0" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.2839" width="0" layer="94"/>
<circle x="2.54" y="0" radius="0.2839" width="0" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CT10-XXXX-" urn="urn:adsk.eagle:component:27092/1" prefix="SW" library_version="1">
<description>&lt;b&gt;CT10 Series Molded Switch&lt;/b&gt;&lt;p&gt;
Source: www.cotorelay.com .. Coto_Technology__CT10-1530-G1.pdf</description>
<gates>
<gate name="G$1" symbol="SWITCH-NO" x="0" y="0"/>
</gates>
<devices>
<device name="A2" package="CT10-XXXX-A2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27090/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="G1" package="CT10-XXXX-G1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27089/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="G4" package="CT10-XXXX-G4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27091/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X04">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X4">
<description>&lt;h3&gt;Molex 4-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<text x="2.286" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.286" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -4 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="12.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="3.4" x2="12.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="12.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.25" y1="3.15" x2="12.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="3.15" x2="12.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="2.15" x2="12.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="0" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch: 1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.016" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Locking Footprint&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Long Pads w/ Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X4_LOCK">
<description>&lt;h3&gt;Molex 4-Pin Plated Through-Hole Locking&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="2.667" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-3.556" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_RA_MALE">
<description>&lt;h3&gt;SMD - 4 Pin Right Angle Male Header&lt;/h3&gt;
tDocu layer shows pin locations.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
<text x="-4.318" y="6.731" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-4-PTH">
<description>&lt;h3&gt;JST Right Angle 4 Pin Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-3" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="0" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="0" drill="0.7" diameter="1.6"/>
<text x="-3.4" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<text x="0.7" y="0.9" size="0.8" layer="51">S</text>
<text x="2.7" y="0.9" size="0.8" layer="51">S</text>
<wire x1="-4.95" y1="-1.6" x2="-4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="6" x2="4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="6" x2="4.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-1.6" x2="-4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-1.6" x2="4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.6" x2="-4.3" y2="0" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.6" x2="4.3" y2="0" width="0.2032" layer="21"/>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -4 Pin PTH Locking&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="3.81" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.81" y="1.524" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_1MM_RA">
<description>&lt;h3&gt;SMD- 4 Pin Right Angle &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_VERTICAL_COMBO">
<description>&lt;h3&gt;SMD - 4 Pin Vertical Connector&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;SMD Pad count:8&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-0.508" y="2.921" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_LONG">
<description>&lt;h3&gt;SMD - 4 Pin w/ Long Solder Pads&lt;/h3&gt;
No silk, but tDocu layer shows pin position. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<description>&lt;h3&gt;JST Vertical 4 Pin Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.jst-mfg.com/product/pdf/eng/ePH.pdf"&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
<text x="-1.143" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_RA_FEMALE">
<description>&lt;h3&gt;SMD - 4 Pin Right-Angle Female Header&lt;/h3&gt;
Silk outline shows header location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.205" y1="4.25" x2="-5.205" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="4.25" x2="-5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="-4.25" x2="5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-5.205" y1="-4.25" x2="5.205" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-1.59" y1="6.8" x2="-0.95" y2="7.65" layer="51"/>
<rectangle x1="0.95" y1="6.8" x2="1.59" y2="7.65" layer="51"/>
<rectangle x1="-4.13" y1="6.8" x2="-3.49" y2="7.65" layer="51"/>
<smd name="3" x="1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="-1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<rectangle x1="3.49" y1="6.8" x2="4.13" y2="7.65" layer="51"/>
<smd name="4" x="3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_04">
<description>&lt;h3&gt;4 Pin Connection&lt;/h3&gt;</description>
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-5.08" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_04" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="CONN_04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08186" constant="no"/>
<attribute name="SF_ID" value="PRT-08231" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="2xCONN-08399" constant="no"/>
<attribute name="SF_ID" value="2xPRT-08084" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.27MM" package="1X04_1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08186" constant="no"/>
<attribute name="SF_ID" value="PRT-08231" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X04_SMD_RA_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09140" constant="no"/>
<attribute name="SF_ID" value="PRT-12638" constant="no"/>
</technology>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="WIRE-13531" constant="no"/>
<attribute name="SF_ID" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04_1MM_RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10310" constant="no"/>
<attribute name="SF_ID" value="PRT-10208" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_VERTICAL_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08511"/>
<attribute name="VALUE" value="1X04_SMD_STRAIGHT_COMBO"/>
</technology>
</technologies>
</device>
<device name="SMD_LONG" package="1X04_SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09140" constant="no"/>
<attribute name="SF_ID" value="PRT-12638" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13251"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X04_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Sensors">
<description>&lt;h3&gt;SparkFun Sensors&lt;/h3&gt;
This library contains sensors- accelerometers, gyros, compasses, magnetometers, light sensors, imagers, temp sensors, etc.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TO-92-AMMO">
<description>&lt;h3&gt;TO-92 3-Pin PTH AMMO package&lt;/h3&gt;
&lt;p&gt;&lt;a href=""&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;
&lt;li&gt;Pin Count: 3&lt;/li&gt;
&lt;li&gt;Dimensions:  3.68 x 4.83 x 4.83 mm&lt;/li&gt;
&lt;li&gt;Pitch: 2.54 mm&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Devices Using:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;DS18B20&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-2.0946" y1="-1.651" x2="-0.7863" y2="2.5485" width="0.2032" layer="21" curve="-111.098957" cap="flat"/>
<wire x1="0.7863" y1="2.5484" x2="2.0945" y2="-1.651" width="0.2032" layer="21" curve="-111.09954" cap="flat"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.2032" layer="21" curve="-25.057615"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="1.905" width="0.2032" layer="21" curve="-28.072487"/>
<wire x1="-1.905" y1="1.905" x2="0.635" y2="2.54" width="0.2032" layer="21" curve="-53.130102"/>
<pad name="3" x="2.54" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="2" x="0" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="-1.778" size="0.6096" layer="25" font="vector" ratio="20" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0" y="2.794" size="0.6096" layer="27" font="vector" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="DS18B20">
<description>&lt;h3&gt;Dallas/Maxim DS18B20 1-Wire Temperature Sensor&lt;/h3&gt;
&lt;p&gt;The DS18B20 reports degrees C with 9 to 12-bit precision, -55C to 125C (+/-0.5C). Each sensor has a unique 64-Bit Serial number etched into it - allows for a huge number of sensors to be used on one data bus.&lt;/p&gt;</description>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="DQ" x="-10.16" y="0" length="short"/>
<pin name="VDD" x="-10.16" y="5.08" length="short" direction="pwr"/>
<pin name="GND" x="-10.16" y="-5.08" length="short" direction="pwr"/>
<text x="-7.62" y="7.874" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-7.62" y="-7.874" size="1.778" layer="96" font="vector" align="top-left">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DS18B20" prefix="U">
<description>&lt;h3&gt;Dallas/Maxim DS18B20 1-Wire Temperature Sensor&lt;/h3&gt;
&lt;p&gt;The DS18B20 reports degrees C with 9 to 12-bit precision, -55C to 125C (+/-0.5C). Each sensor has a unique 64-Bit Serial number etched into it - allows for a huge number of sensors to be used on one data bus.&lt;/p&gt;
&lt;p&gt;&lt;a href="http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/245"&gt;One Wire Digital Temperature Sensor - DS18B20&lt;/a&gt; (SEN-00245)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="DS18B20" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO-92-AMMO">
<connects>
<connect gate="G$1" pin="DQ" pad="2"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="STORE_ID" value="SEN-00245" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="aestethics">
<packages>
<package name="MUTEXV2-LOGO-BOTTOM">
<rectangle x1="6.985" y1="-0.005" x2="7.125" y2="0.005" layer="201" rot="R180"/>
<rectangle x1="6.925" y1="0.005" x2="7.175" y2="0.015" layer="201" rot="R180"/>
<rectangle x1="6.895" y1="0.015" x2="7.205" y2="0.025" layer="201" rot="R180"/>
<rectangle x1="6.865" y1="0.025" x2="7.235" y2="0.035" layer="201" rot="R180"/>
<rectangle x1="6.835" y1="0.035" x2="7.265" y2="0.045" layer="201" rot="R180"/>
<rectangle x1="6.825" y1="0.045" x2="7.285" y2="0.055" layer="201" rot="R180"/>
<rectangle x1="7.135" y1="0.055" x2="7.305" y2="0.065" layer="201" rot="R180"/>
<rectangle x1="6.805" y1="0.055" x2="6.965" y2="0.065" layer="201" rot="R180"/>
<rectangle x1="7.175" y1="0.065" x2="7.315" y2="0.075" layer="201" rot="R180"/>
<rectangle x1="6.785" y1="0.065" x2="6.925" y2="0.075" layer="201" rot="R180"/>
<rectangle x1="7.205" y1="0.075" x2="7.335" y2="0.085" layer="201" rot="R180"/>
<rectangle x1="6.775" y1="0.075" x2="6.895" y2="0.085" layer="201" rot="R180"/>
<rectangle x1="7.235" y1="0.085" x2="7.345" y2="0.095" layer="201" rot="R180"/>
<rectangle x1="6.765" y1="0.085" x2="6.875" y2="0.095" layer="201" rot="R180"/>
<rectangle x1="7.255" y1="0.095" x2="7.365" y2="0.105" layer="201" rot="R180"/>
<rectangle x1="6.745" y1="0.095" x2="6.855" y2="0.105" layer="201" rot="R180"/>
<rectangle x1="7.275" y1="0.105" x2="7.375" y2="0.115" layer="201" rot="R180"/>
<rectangle x1="6.735" y1="0.105" x2="6.835" y2="0.115" layer="201" rot="R180"/>
<rectangle x1="7.295" y1="0.115" x2="7.385" y2="0.125" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="0.115" x2="6.825" y2="0.125" layer="201" rot="R180"/>
<rectangle x1="7.305" y1="0.125" x2="7.395" y2="0.135" layer="201" rot="R180"/>
<rectangle x1="6.715" y1="0.125" x2="6.815" y2="0.135" layer="201" rot="R180"/>
<rectangle x1="7.325" y1="0.135" x2="7.405" y2="0.145" layer="201" rot="R180"/>
<rectangle x1="6.705" y1="0.135" x2="6.795" y2="0.145" layer="201" rot="R180"/>
<rectangle x1="7.335" y1="0.145" x2="7.415" y2="0.155" layer="201" rot="R180"/>
<rectangle x1="6.705" y1="0.145" x2="6.785" y2="0.155" layer="201" rot="R180"/>
<rectangle x1="7.345" y1="0.155" x2="7.425" y2="0.165" layer="201" rot="R180"/>
<rectangle x1="6.695" y1="0.155" x2="6.775" y2="0.165" layer="201" rot="R180"/>
<rectangle x1="7.355" y1="0.165" x2="7.435" y2="0.175" layer="201" rot="R180"/>
<rectangle x1="6.685" y1="0.165" x2="6.765" y2="0.175" layer="201" rot="R180"/>
<rectangle x1="7.375" y1="0.175" x2="7.445" y2="0.185" layer="201" rot="R180"/>
<rectangle x1="6.675" y1="0.175" x2="6.755" y2="0.185" layer="201" rot="R180"/>
<rectangle x1="7.385" y1="0.185" x2="7.445" y2="0.195" layer="201" rot="R180"/>
<rectangle x1="6.675" y1="0.185" x2="6.745" y2="0.195" layer="201" rot="R180"/>
<rectangle x1="7.395" y1="0.195" x2="7.445" y2="0.205" layer="201" rot="R180"/>
<rectangle x1="6.665" y1="0.195" x2="6.745" y2="0.205" layer="201" rot="R180"/>
<rectangle x1="7.395" y1="0.205" x2="7.445" y2="0.215" layer="201" rot="R180"/>
<rectangle x1="6.655" y1="0.205" x2="6.735" y2="0.215" layer="201" rot="R180"/>
<rectangle x1="7.405" y1="0.215" x2="7.445" y2="0.225" layer="201" rot="R180"/>
<rectangle x1="6.655" y1="0.215" x2="6.725" y2="0.225" layer="201" rot="R180"/>
<rectangle x1="7.415" y1="0.225" x2="7.445" y2="0.235" layer="201" rot="R180"/>
<rectangle x1="6.645" y1="0.225" x2="6.725" y2="0.235" layer="201" rot="R180"/>
<rectangle x1="7.425" y1="0.235" x2="7.445" y2="0.245" layer="201" rot="R180"/>
<rectangle x1="6.645" y1="0.235" x2="6.715" y2="0.245" layer="201" rot="R180"/>
<rectangle x1="7.425" y1="0.245" x2="7.445" y2="0.255" layer="201" rot="R180"/>
<rectangle x1="6.635" y1="0.245" x2="6.705" y2="0.255" layer="201" rot="R180"/>
<rectangle x1="7.435" y1="0.255" x2="7.445" y2="0.265" layer="201" rot="R180"/>
<rectangle x1="6.635" y1="0.255" x2="6.705" y2="0.265" layer="201" rot="R180"/>
<rectangle x1="6.625" y1="0.265" x2="6.695" y2="0.275" layer="201" rot="R180"/>
<rectangle x1="6.625" y1="0.275" x2="6.695" y2="0.285" layer="201" rot="R180"/>
<rectangle x1="6.625" y1="0.285" x2="6.695" y2="0.295" layer="201" rot="R180"/>
<rectangle x1="6.615" y1="0.295" x2="6.685" y2="0.305" layer="201" rot="R180"/>
<rectangle x1="6.615" y1="0.305" x2="6.685" y2="0.315" layer="201" rot="R180"/>
<rectangle x1="6.615" y1="0.315" x2="6.675" y2="0.325" layer="201" rot="R180"/>
<rectangle x1="6.605" y1="0.325" x2="6.675" y2="0.335" layer="201" rot="R180"/>
<rectangle x1="6.605" y1="0.335" x2="6.675" y2="0.345" layer="201" rot="R180"/>
<rectangle x1="6.605" y1="0.345" x2="6.675" y2="0.355" layer="201" rot="R180"/>
<rectangle x1="6.605" y1="0.355" x2="6.665" y2="0.365" layer="201" rot="R180"/>
<rectangle x1="6.605" y1="0.365" x2="6.665" y2="0.375" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.375" x2="6.665" y2="0.385" layer="201" rot="R180"/>
<rectangle x1="7.015" y1="0.385" x2="7.115" y2="0.395" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.385" x2="6.665" y2="0.395" layer="201" rot="R180"/>
<rectangle x1="5.845" y1="0.385" x2="5.965" y2="0.395" layer="201" rot="R180"/>
<rectangle x1="3.455" y1="0.385" x2="3.575" y2="0.395" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="0.385" x2="1.495" y2="0.395" layer="201" rot="R180"/>
<rectangle x1="6.955" y1="0.395" x2="7.175" y2="0.405" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.395" x2="6.665" y2="0.405" layer="201" rot="R180"/>
<rectangle x1="5.795" y1="0.395" x2="6.025" y2="0.405" layer="201" rot="R180"/>
<rectangle x1="3.395" y1="0.395" x2="3.625" y2="0.405" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="0.395" x2="1.585" y2="0.405" layer="201" rot="R180"/>
<rectangle x1="6.925" y1="0.405" x2="7.215" y2="0.415" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.405" x2="6.655" y2="0.415" layer="201" rot="R180"/>
<rectangle x1="5.755" y1="0.405" x2="6.055" y2="0.415" layer="201" rot="R180"/>
<rectangle x1="3.355" y1="0.405" x2="3.665" y2="0.415" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="0.405" x2="1.625" y2="0.415" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.415" x2="8.865" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.415" x2="8.535" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.415" x2="7.795" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="6.895" y1="0.415" x2="7.245" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.415" x2="6.655" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="5.725" y1="0.415" x2="6.085" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.415" x2="5.355" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.415" x2="4.615" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.415" x2="4.285" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="3.335" y1="0.415" x2="3.695" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.415" x2="2.965" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.415" x2="2.245" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="1.325" y1="0.415" x2="1.655" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.415" x2="1.075" y2="0.425" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.425" x2="8.865" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.425" x2="8.535" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.425" x2="7.795" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="6.875" y1="0.425" x2="7.265" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.425" x2="6.655" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="5.705" y1="0.425" x2="6.105" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.425" x2="5.355" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.425" x2="4.615" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.425" x2="4.285" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="3.315" y1="0.425" x2="3.715" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.425" x2="2.965" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.425" x2="2.245" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="1.295" y1="0.425" x2="1.685" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.425" x2="1.075" y2="0.435" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.435" x2="8.865" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.435" x2="8.535" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.435" x2="7.795" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="6.845" y1="0.435" x2="7.285" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.435" x2="6.655" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="5.685" y1="0.435" x2="6.125" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.435" x2="5.355" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.435" x2="4.615" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.435" x2="4.285" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="3.295" y1="0.435" x2="3.735" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.435" x2="2.965" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.435" x2="2.245" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="1.275" y1="0.435" x2="1.705" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.435" x2="1.075" y2="0.445" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.445" x2="8.865" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.445" x2="8.535" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.445" x2="7.795" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="7.135" y1="0.445" x2="7.305" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="6.835" y1="0.445" x2="6.985" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.445" x2="6.655" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="5.985" y1="0.445" x2="6.145" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="5.665" y1="0.445" x2="5.845" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.445" x2="5.355" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.445" x2="4.615" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.445" x2="4.285" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="3.595" y1="0.445" x2="3.755" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="3.275" y1="0.445" x2="3.455" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.445" x2="2.965" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.445" x2="2.245" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="1.545" y1="0.445" x2="1.725" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="1.255" y1="0.445" x2="1.425" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.445" x2="1.075" y2="0.455" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.455" x2="8.865" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.455" x2="8.535" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.455" x2="7.795" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="7.175" y1="0.455" x2="7.315" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="6.815" y1="0.455" x2="6.945" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.455" x2="6.655" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="6.025" y1="0.455" x2="6.165" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="5.655" y1="0.455" x2="5.805" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.455" x2="5.355" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.455" x2="4.615" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.455" x2="4.285" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="3.625" y1="0.455" x2="3.765" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="3.255" y1="0.455" x2="3.405" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.455" x2="2.965" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.455" x2="2.245" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="1.595" y1="0.455" x2="1.735" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="1.235" y1="0.455" x2="1.375" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.455" x2="1.075" y2="0.465" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.465" x2="8.865" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.465" x2="8.535" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.465" x2="7.795" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="7.205" y1="0.465" x2="7.335" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="6.795" y1="0.465" x2="6.915" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.465" x2="6.655" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="6.045" y1="0.465" x2="6.175" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="5.635" y1="0.465" x2="5.775" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.465" x2="5.355" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.465" x2="4.615" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.465" x2="4.285" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="3.655" y1="0.465" x2="3.785" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="3.245" y1="0.465" x2="3.375" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.465" x2="2.965" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.465" x2="2.245" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="1.625" y1="0.465" x2="1.755" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="1.225" y1="0.465" x2="1.345" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.465" x2="1.075" y2="0.475" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.475" x2="8.865" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.475" x2="8.535" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.475" x2="7.795" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="7.235" y1="0.475" x2="7.345" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="6.785" y1="0.475" x2="6.895" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="0.475" x2="6.655" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="6.075" y1="0.475" x2="6.195" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="5.625" y1="0.475" x2="5.745" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.475" x2="5.355" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.475" x2="4.615" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.475" x2="4.285" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="3.675" y1="0.475" x2="3.795" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="3.225" y1="0.475" x2="3.355" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.475" x2="2.965" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.475" x2="2.245" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="1.645" y1="0.475" x2="1.765" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="1.205" y1="0.475" x2="1.315" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.475" x2="1.075" y2="0.485" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.485" x2="8.865" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.485" x2="8.535" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.485" x2="7.795" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="7.255" y1="0.485" x2="7.365" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="6.765" y1="0.485" x2="6.875" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.485" x2="6.655" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="6.095" y1="0.485" x2="6.205" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="5.615" y1="0.485" x2="5.725" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.485" x2="5.355" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.485" x2="4.615" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.485" x2="4.285" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="3.695" y1="0.485" x2="3.815" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="3.215" y1="0.485" x2="3.335" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.485" x2="2.965" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.485" x2="2.245" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="1.675" y1="0.485" x2="1.775" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="1.195" y1="0.485" x2="1.295" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.485" x2="1.075" y2="0.495" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.495" x2="8.865" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.495" x2="8.535" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.495" x2="7.795" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="7.265" y1="0.495" x2="7.375" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="6.755" y1="0.495" x2="6.855" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.495" x2="6.655" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="6.105" y1="0.495" x2="6.215" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="5.595" y1="0.495" x2="5.705" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.495" x2="5.355" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.495" x2="4.615" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.495" x2="4.285" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="3.715" y1="0.495" x2="3.825" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="3.205" y1="0.495" x2="3.315" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.495" x2="2.965" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.495" x2="2.245" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="1.685" y1="0.495" x2="1.795" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="0.495" x2="1.285" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.495" x2="1.075" y2="0.505" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.505" x2="8.865" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.505" x2="8.535" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.505" x2="7.795" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="7.285" y1="0.505" x2="7.385" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="6.745" y1="0.505" x2="6.835" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.505" x2="6.655" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="6.125" y1="0.505" x2="6.225" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="5.585" y1="0.505" x2="5.695" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.505" x2="5.355" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.505" x2="4.615" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.505" x2="4.285" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="3.735" y1="0.505" x2="3.835" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="3.195" y1="0.505" x2="3.295" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.505" x2="2.965" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.505" x2="2.245" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="1.705" y1="0.505" x2="1.805" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="0.505" x2="1.265" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.505" x2="1.075" y2="0.515" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.515" x2="8.865" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.515" x2="8.535" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.515" x2="7.795" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="7.305" y1="0.515" x2="7.395" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="6.735" y1="0.515" x2="6.825" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.515" x2="6.655" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="6.135" y1="0.515" x2="6.235" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="0.515" x2="5.675" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.515" x2="5.355" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.515" x2="4.615" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.515" x2="4.285" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="3.745" y1="0.515" x2="3.845" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="3.185" y1="0.515" x2="3.285" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.515" x2="2.965" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.515" x2="2.245" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="1.715" y1="0.515" x2="1.815" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="0.515" x2="1.245" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.515" x2="1.075" y2="0.525" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.525" x2="8.865" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.525" x2="8.535" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.525" x2="7.795" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="7.315" y1="0.525" x2="7.405" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="0.525" x2="6.815" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.525" x2="6.655" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="6.155" y1="0.525" x2="6.245" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="5.565" y1="0.525" x2="5.665" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.525" x2="5.355" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.525" x2="4.615" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.525" x2="4.285" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="3.755" y1="0.525" x2="3.855" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="3.175" y1="0.525" x2="3.265" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.525" x2="2.965" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.525" x2="2.245" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="1.735" y1="0.525" x2="1.825" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="0.525" x2="1.235" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.525" x2="1.075" y2="0.535" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.535" x2="8.865" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.535" x2="8.535" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.535" x2="7.795" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="7.325" y1="0.535" x2="7.415" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="6.715" y1="0.535" x2="6.795" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.535" x2="6.655" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="6.165" y1="0.535" x2="6.255" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.535" x2="5.655" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.535" x2="5.355" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.535" x2="4.615" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.535" x2="4.285" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="3.775" y1="0.535" x2="3.865" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="0.535" x2="3.255" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.535" x2="2.965" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.535" x2="2.245" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="1.745" y1="0.535" x2="1.835" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="0.535" x2="1.225" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.535" x2="1.075" y2="0.545" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.545" x2="8.865" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.545" x2="8.535" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.545" x2="7.795" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="7.335" y1="0.545" x2="7.425" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="6.705" y1="0.545" x2="6.785" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.545" x2="6.655" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="6.175" y1="0.545" x2="6.265" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.545" x2="5.635" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.545" x2="5.355" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.545" x2="4.615" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.545" x2="4.285" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="3.775" y1="0.545" x2="3.875" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.545" x2="3.245" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.545" x2="2.965" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.545" x2="2.245" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="1.755" y1="0.545" x2="1.845" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="1.135" y1="0.545" x2="1.215" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.545" x2="1.075" y2="0.555" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.555" x2="8.865" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.555" x2="8.535" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.555" x2="7.795" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="7.345" y1="0.555" x2="7.435" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="6.695" y1="0.555" x2="6.775" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.555" x2="6.655" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="6.165" y1="0.555" x2="6.275" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.555" x2="5.625" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.555" x2="5.355" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.555" x2="4.615" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.555" x2="4.285" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="3.775" y1="0.555" x2="3.885" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.555" x2="3.235" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.555" x2="2.965" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.555" x2="2.245" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="1.765" y1="0.555" x2="1.855" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="0.555" x2="1.205" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.555" x2="1.075" y2="0.565" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.565" x2="8.865" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.565" x2="8.535" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.565" x2="7.795" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="7.355" y1="0.565" x2="7.435" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="6.695" y1="0.565" x2="6.765" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.565" x2="6.655" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="6.155" y1="0.565" x2="6.285" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.565" x2="5.615" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.565" x2="5.355" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.565" x2="4.615" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.565" x2="4.285" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="3.755" y1="0.565" x2="3.895" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.565" x2="3.225" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.565" x2="2.965" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.565" x2="2.245" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="1.775" y1="0.565" x2="1.865" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="0.565" x2="1.195" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.565" x2="1.075" y2="0.575" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.575" x2="8.865" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.575" x2="8.535" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.575" x2="7.795" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="7.365" y1="0.575" x2="7.445" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="6.685" y1="0.575" x2="6.755" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.575" x2="6.655" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="6.145" y1="0.575" x2="6.295" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.575" x2="5.605" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.575" x2="5.355" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.575" x2="4.615" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.575" x2="4.285" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="3.745" y1="0.575" x2="3.895" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.575" x2="3.215" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.575" x2="2.965" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.575" x2="2.245" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="1.785" y1="0.575" x2="1.865" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="0.575" x2="1.185" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.575" x2="1.075" y2="0.585" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.585" x2="8.865" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.585" x2="8.535" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.585" x2="7.795" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="7.375" y1="0.585" x2="7.455" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="6.675" y1="0.585" x2="6.755" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.585" x2="6.655" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="6.135" y1="0.585" x2="6.295" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.585" x2="5.595" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.585" x2="5.355" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.585" x2="4.615" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.585" x2="4.285" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="3.735" y1="0.585" x2="3.905" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.585" x2="3.205" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.585" x2="2.965" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.585" x2="2.245" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="1.795" y1="0.585" x2="1.875" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="1.105" y1="0.585" x2="1.175" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.585" x2="1.075" y2="0.595" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.595" x2="8.865" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.595" x2="8.535" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.595" x2="7.795" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="7.385" y1="0.595" x2="7.465" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="6.675" y1="0.595" x2="6.745" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.595" x2="6.655" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="6.225" y1="0.595" x2="6.305" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="6.125" y1="0.595" x2="6.205" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.595" x2="5.585" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.595" x2="5.355" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.595" x2="4.615" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.595" x2="4.285" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="3.835" y1="0.595" x2="3.915" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="3.725" y1="0.595" x2="3.815" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.595" x2="3.195" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.595" x2="2.965" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.595" x2="2.245" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="1.805" y1="0.595" x2="1.885" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="0.595" x2="1.165" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.595" x2="1.075" y2="0.605" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.605" x2="8.865" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.605" x2="8.535" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.605" x2="7.795" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="7.395" y1="0.605" x2="7.465" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="6.665" y1="0.605" x2="6.735" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.605" x2="6.655" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="6.235" y1="0.605" x2="6.315" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="6.115" y1="0.605" x2="6.195" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.605" x2="5.585" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.605" x2="5.355" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.605" x2="4.615" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.605" x2="4.285" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="3.845" y1="0.605" x2="3.915" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="3.715" y1="0.605" x2="3.805" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.605" x2="3.185" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.605" x2="2.965" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.605" x2="2.245" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="1.815" y1="0.605" x2="1.885" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="0.605" x2="1.155" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.605" x2="1.075" y2="0.615" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.615" x2="8.865" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.615" x2="8.535" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.615" x2="7.795" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="7.405" y1="0.615" x2="7.475" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.615" x2="6.725" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="6.245" y1="0.615" x2="6.315" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="6.105" y1="0.615" x2="6.185" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.615" x2="5.575" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.615" x2="5.355" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.615" x2="4.615" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.615" x2="4.285" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="3.845" y1="0.615" x2="3.925" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="3.705" y1="0.615" x2="3.795" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.615" x2="3.175" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.615" x2="2.965" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.615" x2="2.245" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="1.825" y1="0.615" x2="1.895" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="0.615" x2="1.155" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.615" x2="1.075" y2="0.625" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.625" x2="8.865" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.625" x2="8.535" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.625" x2="7.795" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="7.405" y1="0.625" x2="7.485" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.625" x2="6.715" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="6.245" y1="0.625" x2="6.325" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="6.095" y1="0.625" x2="6.175" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="0.625" x2="5.565" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.625" x2="5.355" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.625" x2="4.615" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.625" x2="4.285" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="3.855" y1="0.625" x2="3.935" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="3.695" y1="0.625" x2="3.785" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="0.625" x2="3.175" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.625" x2="2.965" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.625" x2="2.245" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="1.825" y1="0.625" x2="1.905" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.625" x2="1.145" y2="0.635" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.635" x2="8.865" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.635" x2="8.535" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.635" x2="7.795" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="7.415" y1="0.635" x2="7.485" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.635" x2="6.715" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="0.635" x2="6.335" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="6.085" y1="0.635" x2="6.165" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.635" x2="5.355" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.635" x2="4.615" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.635" x2="4.285" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="3.865" y1="0.635" x2="3.935" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="3.685" y1="0.635" x2="3.775" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.635" x2="2.965" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.635" x2="2.245" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="1.835" y1="0.635" x2="1.905" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.635" x2="1.135" y2="0.645" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.645" x2="8.865" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.645" x2="8.535" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.645" x2="7.795" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="7.415" y1="0.645" x2="7.495" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.645" x2="6.705" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="6.265" y1="0.645" x2="6.335" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="6.075" y1="0.645" x2="6.155" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.645" x2="5.355" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.645" x2="4.615" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.645" x2="4.285" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="3.865" y1="0.645" x2="3.945" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="3.675" y1="0.645" x2="3.765" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.645" x2="2.965" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.645" x2="2.245" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="1.845" y1="0.645" x2="1.915" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.645" x2="1.135" y2="0.655" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.655" x2="8.865" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.655" x2="8.535" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.655" x2="7.795" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="7.425" y1="0.655" x2="7.495" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.655" x2="6.705" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="6.265" y1="0.655" x2="6.345" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="6.065" y1="0.655" x2="6.145" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.655" x2="5.355" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.655" x2="4.615" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.655" x2="4.285" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="3.875" y1="0.655" x2="3.945" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="3.665" y1="0.655" x2="3.755" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.655" x2="2.965" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.655" x2="2.245" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="1.845" y1="0.655" x2="1.915" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.655" x2="1.125" y2="0.665" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.665" x2="8.865" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.665" x2="8.535" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.665" x2="7.795" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="7.435" y1="0.665" x2="7.505" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.665" x2="6.695" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="6.275" y1="0.665" x2="6.345" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="6.055" y1="0.665" x2="6.135" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.665" x2="5.355" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.665" x2="4.615" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.665" x2="4.285" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="3.885" y1="0.665" x2="3.955" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="3.655" y1="0.665" x2="3.745" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.665" x2="2.965" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.665" x2="2.245" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="1.855" y1="0.665" x2="1.925" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.665" x2="1.115" y2="0.675" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.675" x2="8.865" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.675" x2="8.535" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.675" x2="7.795" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="7.435" y1="0.675" x2="7.505" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.675" x2="6.695" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="6.285" y1="0.675" x2="6.355" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="6.045" y1="0.675" x2="6.125" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.675" x2="5.355" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.675" x2="4.615" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.675" x2="4.285" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="3.885" y1="0.675" x2="3.955" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="3.645" y1="0.675" x2="3.735" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.675" x2="2.965" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.675" x2="2.245" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="1.855" y1="0.675" x2="1.925" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.675" x2="1.115" y2="0.685" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.685" x2="8.865" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.685" x2="8.535" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.685" x2="7.795" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="7.445" y1="0.685" x2="7.505" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.685" x2="6.685" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="6.285" y1="0.685" x2="6.355" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="6.035" y1="0.685" x2="6.115" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.685" x2="5.355" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.685" x2="4.615" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.685" x2="4.285" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="3.895" y1="0.685" x2="3.965" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="3.635" y1="0.685" x2="3.725" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.685" x2="2.965" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.685" x2="2.245" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="1.865" y1="0.685" x2="1.935" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.685" x2="1.115" y2="0.695" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.695" x2="8.865" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.695" x2="8.535" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.695" x2="7.795" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="7.445" y1="0.695" x2="7.515" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.695" x2="6.685" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="6.285" y1="0.695" x2="6.355" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="6.025" y1="0.695" x2="6.105" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.695" x2="5.355" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.695" x2="4.615" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.695" x2="4.285" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="3.895" y1="0.695" x2="3.965" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="3.625" y1="0.695" x2="3.715" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.695" x2="2.965" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.695" x2="2.245" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="1.865" y1="0.695" x2="1.935" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.695" x2="1.105" y2="0.705" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.705" x2="8.865" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.705" x2="8.535" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.705" x2="7.795" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="7.445" y1="0.705" x2="7.515" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.705" x2="6.675" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="6.295" y1="0.705" x2="6.365" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="6.015" y1="0.705" x2="6.095" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.705" x2="5.355" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.705" x2="4.615" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.705" x2="4.285" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="3.895" y1="0.705" x2="3.965" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="3.615" y1="0.705" x2="3.705" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.705" x2="2.965" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.705" x2="2.245" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="1.875" y1="0.705" x2="1.935" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.705" x2="1.105" y2="0.715" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.715" x2="8.865" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.715" x2="8.535" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.715" x2="7.795" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="7.455" y1="0.715" x2="7.525" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.715" x2="6.675" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="6.295" y1="0.715" x2="6.365" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="6.005" y1="0.715" x2="6.085" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.715" x2="5.355" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.715" x2="4.615" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.715" x2="4.285" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="3.905" y1="0.715" x2="3.975" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="3.605" y1="0.715" x2="3.695" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.715" x2="2.965" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.715" x2="2.245" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="1.875" y1="0.715" x2="1.945" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.715" x2="1.095" y2="0.725" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.725" x2="8.865" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.725" x2="8.535" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.725" x2="7.795" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="7.455" y1="0.725" x2="7.525" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.725" x2="6.675" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="6.305" y1="0.725" x2="6.365" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="5.995" y1="0.725" x2="6.075" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.725" x2="5.355" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.725" x2="4.615" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.725" x2="4.285" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="3.905" y1="0.725" x2="3.975" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="3.595" y1="0.725" x2="3.685" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.725" x2="2.965" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.725" x2="2.245" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="1.875" y1="0.725" x2="1.945" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.725" x2="1.095" y2="0.735" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.735" x2="8.865" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.735" x2="8.535" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.735" x2="7.795" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="7.455" y1="0.735" x2="7.525" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.735" x2="6.665" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="6.305" y1="0.735" x2="6.375" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="5.985" y1="0.735" x2="6.065" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.735" x2="5.355" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.735" x2="4.615" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.735" x2="4.285" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="3.915" y1="0.735" x2="3.975" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="3.585" y1="0.735" x2="3.675" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.735" x2="2.965" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.735" x2="2.245" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="1.885" y1="0.735" x2="1.945" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.735" x2="1.095" y2="0.745" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.745" x2="8.865" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.745" x2="8.535" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.745" x2="7.795" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="7.465" y1="0.745" x2="7.525" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.745" x2="6.665" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="6.305" y1="0.745" x2="6.375" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="5.975" y1="0.745" x2="6.055" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.745" x2="5.355" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.745" x2="4.615" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.745" x2="4.285" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="3.915" y1="0.745" x2="3.985" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="3.575" y1="0.745" x2="3.665" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.745" x2="2.965" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.745" x2="2.245" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="1.885" y1="0.745" x2="1.945" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.745" x2="1.085" y2="0.755" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.755" x2="8.865" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.755" x2="8.535" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.755" x2="7.795" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="7.465" y1="0.755" x2="7.535" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.755" x2="6.665" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.755" x2="6.375" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="5.965" y1="0.755" x2="6.045" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.755" x2="5.355" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.755" x2="4.615" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.755" x2="4.285" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="3.915" y1="0.755" x2="3.985" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="3.565" y1="0.755" x2="3.655" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.755" x2="2.965" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.755" x2="2.245" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="1.885" y1="0.755" x2="1.955" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.755" x2="1.085" y2="0.765" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.765" x2="8.865" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.765" x2="8.535" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.765" x2="7.795" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="7.465" y1="0.765" x2="7.535" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.765" x2="6.665" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.765" x2="6.375" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="5.955" y1="0.765" x2="6.035" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.765" x2="5.355" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.765" x2="4.615" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.765" x2="4.285" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="3.915" y1="0.765" x2="3.985" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="3.555" y1="0.765" x2="3.645" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.765" x2="2.965" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.765" x2="2.245" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="1.885" y1="0.765" x2="1.955" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.765" x2="1.085" y2="0.775" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.775" x2="8.865" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.775" x2="8.535" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.775" x2="7.795" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="7.465" y1="0.775" x2="7.535" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.775" x2="6.655" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.775" x2="6.385" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="5.945" y1="0.775" x2="6.025" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.775" x2="5.355" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.775" x2="4.615" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.775" x2="4.285" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.775" x2="3.985" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="3.545" y1="0.775" x2="3.635" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.775" x2="2.965" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.775" x2="2.245" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.775" x2="1.955" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.775" x2="1.085" y2="0.785" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.785" x2="8.865" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.785" x2="8.535" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.785" x2="7.795" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.785" x2="7.535" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.785" x2="6.655" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.785" x2="6.385" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="5.935" y1="0.785" x2="6.015" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.785" x2="5.355" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.785" x2="4.615" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.785" x2="4.285" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.785" x2="3.985" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="3.535" y1="0.785" x2="3.625" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.785" x2="2.965" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.785" x2="2.245" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.785" x2="1.955" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.785" x2="1.075" y2="0.795" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.795" x2="8.865" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.795" x2="8.535" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.795" x2="7.795" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.795" x2="7.535" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.795" x2="6.655" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.795" x2="6.385" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="5.925" y1="0.795" x2="6.005" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.795" x2="5.355" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.795" x2="4.615" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.795" x2="4.285" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.795" x2="3.995" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="3.525" y1="0.795" x2="3.615" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.795" x2="2.965" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.795" x2="2.245" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.795" x2="1.955" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.795" x2="1.075" y2="0.805" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.805" x2="8.865" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.805" x2="8.535" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.805" x2="7.795" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.805" x2="7.535" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.805" x2="6.655" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.805" x2="6.385" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="5.915" y1="0.805" x2="5.995" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.805" x2="5.355" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.805" x2="4.615" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.805" x2="4.285" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.805" x2="3.995" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="3.515" y1="0.805" x2="3.605" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.805" x2="2.965" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.805" x2="2.245" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.805" x2="1.965" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.805" x2="1.075" y2="0.815" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.815" x2="8.865" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.815" x2="8.535" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.815" x2="7.795" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.815" x2="7.535" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.815" x2="6.655" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.815" x2="6.385" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="5.905" y1="0.815" x2="5.985" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.815" x2="5.355" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.815" x2="4.615" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.815" x2="4.285" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.815" x2="3.995" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="3.505" y1="0.815" x2="3.595" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.815" x2="2.965" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.815" x2="2.245" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.815" x2="1.965" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.815" x2="1.075" y2="0.825" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.825" x2="8.865" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.825" x2="8.535" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.825" x2="7.795" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.825" x2="7.535" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.825" x2="6.655" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.825" x2="6.385" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="5.895" y1="0.825" x2="5.975" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.825" x2="5.355" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.825" x2="4.615" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.825" x2="4.285" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.825" x2="3.995" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="3.495" y1="0.825" x2="3.585" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.825" x2="2.965" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.825" x2="2.245" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.825" x2="1.965" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.825" x2="1.075" y2="0.835" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.835" x2="8.865" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.835" x2="8.535" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.835" x2="7.795" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.835" x2="7.535" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.835" x2="6.655" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.835" x2="6.385" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="5.885" y1="0.835" x2="5.965" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.835" x2="5.355" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.835" x2="4.615" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.835" x2="4.285" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.835" x2="3.995" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="3.485" y1="0.835" x2="3.575" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.835" x2="2.965" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.835" x2="2.245" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.835" x2="1.965" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.835" x2="1.075" y2="0.845" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.845" x2="8.865" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.845" x2="8.535" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.845" x2="7.795" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.845" x2="7.545" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.845" x2="6.655" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.845" x2="6.385" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="5.875" y1="0.845" x2="5.955" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.845" x2="5.355" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.845" x2="4.615" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.845" x2="4.285" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.845" x2="3.995" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="3.475" y1="0.845" x2="3.565" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.845" x2="2.965" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.845" x2="2.245" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.845" x2="1.965" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.845" x2="1.075" y2="0.855" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.855" x2="8.865" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.855" x2="8.535" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.855" x2="7.795" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.855" x2="7.545" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.855" x2="6.655" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.855" x2="6.385" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="5.865" y1="0.855" x2="5.945" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.855" x2="5.355" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.855" x2="4.615" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.855" x2="4.285" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="3.935" y1="0.855" x2="3.995" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="3.465" y1="0.855" x2="3.555" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.855" x2="2.965" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.855" x2="2.245" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.855" x2="1.965" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.855" x2="1.075" y2="0.865" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.865" x2="8.865" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.865" x2="8.535" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.865" x2="7.795" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.865" x2="7.545" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.865" x2="6.655" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.865" x2="6.385" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="5.845" y1="0.865" x2="5.935" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.865" x2="5.355" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.865" x2="4.615" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.865" x2="4.285" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="3.935" y1="0.865" x2="3.995" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="3.455" y1="0.865" x2="3.545" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.865" x2="2.965" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.865" x2="2.245" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.865" x2="1.965" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.865" x2="1.075" y2="0.875" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.875" x2="8.865" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.875" x2="8.535" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.875" x2="7.795" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.875" x2="7.535" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.875" x2="6.655" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.875" x2="6.385" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="5.835" y1="0.875" x2="5.925" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.875" x2="5.355" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.875" x2="4.615" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.875" x2="4.285" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="3.935" y1="0.875" x2="3.995" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="3.445" y1="0.875" x2="3.535" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.875" x2="2.965" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.875" x2="2.245" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.875" x2="1.965" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.875" x2="1.075" y2="0.885" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.885" x2="8.865" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.885" x2="8.535" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.885" x2="7.795" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.885" x2="7.535" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.885" x2="6.655" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.885" x2="6.385" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="5.825" y1="0.885" x2="5.915" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.885" x2="5.355" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.885" x2="4.615" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.885" x2="4.285" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.885" x2="3.995" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="3.435" y1="0.885" x2="3.525" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.885" x2="2.965" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.885" x2="2.245" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.885" x2="1.965" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.885" x2="1.075" y2="0.895" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.895" x2="8.865" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.895" x2="8.535" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.895" x2="7.795" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.895" x2="7.535" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.895" x2="6.655" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.895" x2="6.385" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="5.815" y1="0.895" x2="5.905" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.895" x2="5.355" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.895" x2="4.615" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.895" x2="4.285" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.895" x2="3.995" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="3.425" y1="0.895" x2="3.515" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.895" x2="2.965" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.895" x2="2.245" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.895" x2="1.965" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.895" x2="1.075" y2="0.905" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.905" x2="8.865" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.905" x2="8.535" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.905" x2="7.795" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.905" x2="7.535" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.905" x2="6.655" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.905" x2="6.385" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="5.805" y1="0.905" x2="5.895" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.905" x2="5.355" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.905" x2="4.615" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.905" x2="4.285" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.905" x2="3.995" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="3.415" y1="0.905" x2="3.505" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.905" x2="2.965" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.905" x2="2.245" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.905" x2="1.965" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.905" x2="1.075" y2="0.915" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.915" x2="8.865" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.915" x2="8.535" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.915" x2="7.795" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.915" x2="7.535" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.915" x2="6.655" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="0.915" x2="6.385" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="5.795" y1="0.915" x2="5.885" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.915" x2="5.355" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.915" x2="4.615" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.915" x2="4.285" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.915" x2="3.995" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="3.405" y1="0.915" x2="3.495" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.915" x2="2.965" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.915" x2="2.245" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.915" x2="1.955" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.915" x2="1.075" y2="0.925" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.925" x2="8.865" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.925" x2="8.535" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="7.725" y1="0.925" x2="7.795" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="7.475" y1="0.925" x2="7.535" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.925" x2="6.655" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.925" x2="6.385" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="5.785" y1="0.925" x2="5.875" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.925" x2="5.355" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="0.925" x2="4.615" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.925" x2="4.285" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.925" x2="3.985" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="3.395" y1="0.925" x2="3.485" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.925" x2="2.965" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.925" x2="2.245" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.925" x2="1.955" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.925" x2="1.075" y2="0.935" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.935" x2="8.865" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.935" x2="8.535" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="0.935" x2="7.795" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="7.465" y1="0.935" x2="7.535" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.935" x2="6.655" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.935" x2="6.385" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="5.775" y1="0.935" x2="5.865" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.935" x2="5.355" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="0.935" x2="4.615" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.935" x2="4.285" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.935" x2="3.985" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="3.385" y1="0.935" x2="3.475" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.935" x2="2.965" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.935" x2="2.245" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="1.895" y1="0.935" x2="1.955" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.935" x2="1.085" y2="0.945" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.945" x2="8.865" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.945" x2="8.535" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="0.945" x2="7.795" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="7.465" y1="0.945" x2="7.535" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.945" x2="6.665" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.945" x2="6.385" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="5.765" y1="0.945" x2="5.855" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.945" x2="5.355" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="0.945" x2="4.615" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.945" x2="4.285" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.945" x2="3.985" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="3.375" y1="0.945" x2="3.465" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.945" x2="2.965" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.945" x2="2.245" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="1.885" y1="0.945" x2="1.955" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.945" x2="1.085" y2="0.955" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.955" x2="8.865" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.955" x2="8.535" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="0.955" x2="7.795" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="7.465" y1="0.955" x2="7.535" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.955" x2="6.665" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.955" x2="6.375" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="5.755" y1="0.955" x2="5.845" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.955" x2="5.355" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="0.955" x2="4.615" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.955" x2="4.285" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="3.925" y1="0.955" x2="3.985" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="3.365" y1="0.955" x2="3.455" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.955" x2="2.965" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.955" x2="2.245" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="1.885" y1="0.955" x2="1.955" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.955" x2="1.085" y2="0.965" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.965" x2="8.865" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.965" x2="8.535" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="0.965" x2="7.795" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="7.465" y1="0.965" x2="7.525" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.965" x2="6.665" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="0.965" x2="6.375" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="5.745" y1="0.965" x2="5.835" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.965" x2="5.355" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="0.965" x2="4.615" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.965" x2="4.285" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="3.915" y1="0.965" x2="3.985" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="3.355" y1="0.965" x2="3.445" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.965" x2="2.965" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.965" x2="2.245" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="1.885" y1="0.965" x2="1.955" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.965" x2="1.085" y2="0.975" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.975" x2="8.865" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.975" x2="8.535" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="0.975" x2="7.795" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="7.455" y1="0.975" x2="7.525" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.975" x2="6.665" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="6.305" y1="0.975" x2="6.375" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="5.735" y1="0.975" x2="5.825" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.975" x2="5.355" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="0.975" x2="4.615" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.975" x2="4.285" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="3.915" y1="0.975" x2="3.985" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="3.345" y1="0.975" x2="3.435" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.975" x2="2.965" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.975" x2="2.245" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="1.885" y1="0.975" x2="1.945" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.975" x2="1.095" y2="0.985" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.985" x2="8.865" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="0.985" x2="8.535" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="0.985" x2="7.795" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="7.455" y1="0.985" x2="7.525" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.985" x2="6.675" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="6.305" y1="0.985" x2="6.375" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="5.725" y1="0.985" x2="5.815" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="0.985" x2="5.355" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="0.985" x2="4.615" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.985" x2="4.285" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="3.915" y1="0.985" x2="3.975" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="3.335" y1="0.985" x2="3.425" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.985" x2="2.965" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.985" x2="2.245" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="1.875" y1="0.985" x2="1.945" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.985" x2="1.095" y2="0.995" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="0.995" x2="8.865" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="0.995" x2="8.535" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="0.995" x2="7.795" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="7.455" y1="0.995" x2="7.525" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="0.995" x2="6.675" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="6.305" y1="0.995" x2="6.365" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="5.715" y1="0.995" x2="5.805" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="5.285" y1="0.995" x2="5.355" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="0.995" x2="4.615" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="0.995" x2="4.285" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="3.905" y1="0.995" x2="3.975" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="3.325" y1="0.995" x2="3.415" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="0.995" x2="2.965" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="0.995" x2="2.245" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="1.875" y1="0.995" x2="1.945" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="0.995" x2="1.095" y2="1.005" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.005" x2="8.865" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="1.005" x2="8.535" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="1.005" x2="7.805" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="7.445" y1="1.005" x2="7.515" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.005" x2="6.675" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="6.295" y1="1.005" x2="6.365" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="5.705" y1="1.005" x2="5.795" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="5.285" y1="1.005" x2="5.355" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="1.005" x2="4.625" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.005" x2="4.285" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="3.905" y1="1.005" x2="3.975" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="3.315" y1="1.005" x2="3.405" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.005" x2="2.965" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.005" x2="2.245" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="1.875" y1="1.005" x2="1.935" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.005" x2="1.105" y2="1.015" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.015" x2="8.865" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="1.015" x2="8.535" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="7.735" y1="1.015" x2="7.805" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="7.445" y1="1.015" x2="7.515" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.015" x2="6.685" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="6.295" y1="1.015" x2="6.365" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="5.695" y1="1.015" x2="5.785" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="5.285" y1="1.015" x2="5.355" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="4.555" y1="1.015" x2="4.625" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.015" x2="4.285" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="3.905" y1="1.015" x2="3.965" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="3.305" y1="1.015" x2="3.395" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.015" x2="2.965" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.015" x2="2.245" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="1.865" y1="1.015" x2="1.935" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.015" x2="1.105" y2="1.025" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.025" x2="8.865" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="1.025" x2="8.535" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="7.745" y1="1.025" x2="7.805" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="7.445" y1="1.025" x2="7.505" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.025" x2="6.685" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="6.285" y1="1.025" x2="6.355" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="5.685" y1="1.025" x2="5.775" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="5.285" y1="1.025" x2="5.355" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="4.565" y1="1.025" x2="4.625" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.025" x2="4.285" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="3.895" y1="1.025" x2="3.965" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="3.295" y1="1.025" x2="3.385" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.025" x2="2.965" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.025" x2="2.245" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="1.865" y1="1.025" x2="1.935" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.025" x2="1.105" y2="1.035" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.035" x2="8.865" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="8.455" y1="1.035" x2="8.535" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="7.745" y1="1.035" x2="7.805" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="7.435" y1="1.035" x2="7.505" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.035" x2="6.695" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="6.285" y1="1.035" x2="6.355" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="5.675" y1="1.035" x2="5.765" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="5.275" y1="1.035" x2="5.355" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="4.565" y1="1.035" x2="4.625" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.035" x2="4.285" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="3.895" y1="1.035" x2="3.965" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="3.285" y1="1.035" x2="3.375" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="1.035" x2="2.965" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.035" x2="2.245" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="1.855" y1="1.035" x2="1.925" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.035" x2="1.115" y2="1.045" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.045" x2="8.865" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="8.455" y1="1.045" x2="8.535" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="7.745" y1="1.045" x2="7.815" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="7.435" y1="1.045" x2="7.505" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.045" x2="6.695" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="6.275" y1="1.045" x2="6.345" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="5.665" y1="1.045" x2="5.755" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="5.275" y1="1.045" x2="5.355" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="4.565" y1="1.045" x2="4.635" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.045" x2="4.285" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="3.885" y1="1.045" x2="3.955" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="3.275" y1="1.045" x2="3.365" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="1.045" x2="2.965" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.045" x2="2.245" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="1.855" y1="1.045" x2="1.925" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.045" x2="1.115" y2="1.055" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.055" x2="8.865" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="8.455" y1="1.055" x2="8.535" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="7.745" y1="1.055" x2="7.815" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="7.425" y1="1.055" x2="7.495" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.055" x2="6.705" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="6.275" y1="1.055" x2="6.345" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="5.655" y1="1.055" x2="5.745" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="5.275" y1="1.055" x2="5.355" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="4.565" y1="1.055" x2="4.635" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.055" x2="4.285" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="3.885" y1="1.055" x2="3.955" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="3.265" y1="1.055" x2="3.355" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="1.055" x2="2.965" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.055" x2="2.245" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="1.845" y1="1.055" x2="1.915" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.055" x2="1.125" y2="1.065" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.065" x2="8.865" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="8.445" y1="1.065" x2="8.535" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="7.755" y1="1.065" x2="7.825" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="7.415" y1="1.065" x2="7.495" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.065" x2="6.705" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="6.265" y1="1.065" x2="6.335" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="5.645" y1="1.065" x2="5.735" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="5.265" y1="1.065" x2="5.355" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="4.575" y1="1.065" x2="4.645" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.065" x2="4.285" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="3.875" y1="1.065" x2="3.945" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="3.255" y1="1.065" x2="3.345" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="1.065" x2="2.965" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.065" x2="2.245" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="1.845" y1="1.065" x2="1.915" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.065" x2="1.135" y2="1.075" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.075" x2="8.865" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="8.445" y1="1.075" x2="8.535" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="7.755" y1="1.075" x2="7.825" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="7.415" y1="1.075" x2="7.485" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.075" x2="6.715" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="6.265" y1="1.075" x2="6.335" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="5.635" y1="1.075" x2="5.725" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="5.265" y1="1.075" x2="5.355" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="4.575" y1="1.075" x2="4.645" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.075" x2="4.285" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="3.865" y1="1.075" x2="3.945" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="3.245" y1="1.075" x2="3.335" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="1.075" x2="2.965" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.075" x2="2.245" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="1.835" y1="1.075" x2="1.905" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.075" x2="1.135" y2="1.085" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.085" x2="8.865" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="8.435" y1="1.085" x2="8.535" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="7.755" y1="1.085" x2="7.835" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="7.405" y1="1.085" x2="7.485" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.085" x2="6.725" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="1.085" x2="6.325" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="5.625" y1="1.085" x2="5.715" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="5.255" y1="1.085" x2="5.355" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="4.575" y1="1.085" x2="4.655" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.085" x2="4.285" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="3.865" y1="1.085" x2="3.935" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="3.235" y1="1.085" x2="3.325" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="2.885" y1="1.085" x2="2.965" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.085" x2="2.245" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="1.825" y1="1.085" x2="1.905" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.085" x2="1.145" y2="1.095" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.095" x2="8.865" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="8.435" y1="1.095" x2="8.535" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="7.765" y1="1.095" x2="7.835" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="7.395" y1="1.095" x2="7.475" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.095" x2="6.725" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="6.245" y1="1.095" x2="6.325" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="5.615" y1="1.095" x2="5.705" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="5.255" y1="1.095" x2="5.355" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="4.585" y1="1.095" x2="4.655" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.095" x2="4.285" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="3.855" y1="1.095" x2="3.925" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="3.225" y1="1.095" x2="3.315" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="2.885" y1="1.095" x2="2.965" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.095" x2="2.245" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="1.825" y1="1.095" x2="1.895" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="1.095" x2="1.155" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.095" x2="1.075" y2="1.105" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.105" x2="8.865" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="8.425" y1="1.105" x2="8.535" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="7.765" y1="1.105" x2="7.845" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="7.395" y1="1.105" x2="7.465" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="6.665" y1="1.105" x2="6.735" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.105" x2="6.655" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="6.235" y1="1.105" x2="6.315" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="5.605" y1="1.105" x2="5.695" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="5.245" y1="1.105" x2="5.355" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="4.585" y1="1.105" x2="4.665" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.105" x2="4.285" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="3.845" y1="1.105" x2="3.925" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="3.215" y1="1.105" x2="3.305" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="2.885" y1="1.105" x2="2.965" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.105" x2="2.245" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="1.815" y1="1.105" x2="1.895" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="1.105" x2="1.155" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.105" x2="1.075" y2="1.115" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.115" x2="8.865" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="8.415" y1="1.115" x2="8.535" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="7.775" y1="1.115" x2="7.845" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="7.385" y1="1.115" x2="7.465" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="6.675" y1="1.115" x2="6.745" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.115" x2="6.655" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="6.235" y1="1.115" x2="6.305" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="5.595" y1="1.115" x2="5.685" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="5.235" y1="1.115" x2="5.355" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="4.595" y1="1.115" x2="4.665" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.115" x2="4.285" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="3.835" y1="1.115" x2="3.915" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="3.205" y1="1.115" x2="3.295" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="2.875" y1="1.115" x2="2.965" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.115" x2="2.245" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="1.805" y1="1.115" x2="1.885" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="1.115" x2="1.165" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.115" x2="1.075" y2="1.125" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.125" x2="8.865" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="8.415" y1="1.125" x2="8.535" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="7.775" y1="1.125" x2="7.855" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="7.375" y1="1.125" x2="7.455" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="6.675" y1="1.125" x2="6.755" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.125" x2="6.655" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="6.225" y1="1.125" x2="6.305" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="5.585" y1="1.125" x2="5.675" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="5.235" y1="1.125" x2="5.355" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="4.595" y1="1.125" x2="4.675" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.125" x2="4.285" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="3.825" y1="1.125" x2="3.905" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="3.195" y1="1.125" x2="3.285" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="2.875" y1="1.125" x2="2.965" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.125" x2="2.245" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="1.795" y1="1.125" x2="1.875" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="1.125" x2="1.175" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.125" x2="1.075" y2="1.135" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.135" x2="8.865" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.135" x2="8.535" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="8.405" y1="1.135" x2="8.465" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="7.785" y1="1.135" x2="7.865" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="7.365" y1="1.135" x2="7.445" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="6.685" y1="1.135" x2="6.765" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.135" x2="6.655" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="6.215" y1="1.135" x2="6.295" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="1.135" x2="5.665" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.135" x2="5.355" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="5.225" y1="1.135" x2="5.285" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="4.605" y1="1.135" x2="4.685" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.135" x2="4.285" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="3.825" y1="1.135" x2="3.905" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="3.185" y1="1.135" x2="3.275" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="2.865" y1="1.135" x2="2.965" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.135" x2="2.245" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="1.785" y1="1.135" x2="1.875" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="1.105" y1="1.135" x2="1.185" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.135" x2="1.075" y2="1.145" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.145" x2="8.865" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.145" x2="8.535" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="8.395" y1="1.145" x2="8.465" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="7.785" y1="1.145" x2="7.865" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="7.355" y1="1.145" x2="7.435" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="6.695" y1="1.145" x2="6.765" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.145" x2="6.655" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="6.205" y1="1.145" x2="6.285" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="5.565" y1="1.145" x2="5.655" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.145" x2="5.355" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="5.215" y1="1.145" x2="5.285" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="4.605" y1="1.145" x2="4.685" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.145" x2="4.285" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="3.815" y1="1.145" x2="3.895" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="3.175" y1="1.145" x2="3.265" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="2.865" y1="1.145" x2="2.965" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.145" x2="2.245" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="1.775" y1="1.145" x2="1.865" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="1.145" x2="1.195" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.145" x2="1.075" y2="1.155" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.155" x2="8.865" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.155" x2="8.535" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="8.385" y1="1.155" x2="8.455" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="7.795" y1="1.155" x2="7.875" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="7.345" y1="1.155" x2="7.435" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="6.695" y1="1.155" x2="6.775" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.155" x2="6.655" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="6.195" y1="1.155" x2="6.275" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="1.155" x2="5.645" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.155" x2="5.355" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="5.205" y1="1.155" x2="5.275" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="4.615" y1="1.155" x2="4.695" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.155" x2="4.285" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="3.805" y1="1.155" x2="3.885" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="1.155" x2="3.255" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="2.855" y1="1.155" x2="2.965" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.155" x2="2.245" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="1.765" y1="1.155" x2="1.855" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="1.155" x2="1.205" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.155" x2="1.075" y2="1.165" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.165" x2="8.865" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.165" x2="8.535" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="8.375" y1="1.165" x2="8.455" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="7.805" y1="1.165" x2="7.885" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="7.335" y1="1.165" x2="7.425" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="6.705" y1="1.165" x2="6.795" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.165" x2="6.655" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="6.185" y1="1.165" x2="6.265" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="1.165" x2="5.635" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.165" x2="5.355" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="5.195" y1="1.165" x2="5.275" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="4.625" y1="1.165" x2="4.705" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.165" x2="4.285" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="3.785" y1="1.165" x2="3.875" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="1.165" x2="3.245" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="2.845" y1="1.165" x2="2.965" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.165" x2="2.245" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="1.755" y1="1.165" x2="1.845" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="1.165" x2="1.215" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.165" x2="1.075" y2="1.175" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.175" x2="8.865" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.175" x2="8.535" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="8.365" y1="1.175" x2="8.445" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="7.815" y1="1.175" x2="7.895" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="7.325" y1="1.175" x2="7.415" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="6.715" y1="1.175" x2="6.805" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.175" x2="6.655" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="6.175" y1="1.175" x2="6.265" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="5.555" y1="1.175" x2="5.655" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.175" x2="5.355" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="5.185" y1="1.175" x2="5.265" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="4.635" y1="1.175" x2="4.715" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.175" x2="4.285" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="3.775" y1="1.175" x2="3.865" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="1.175" x2="3.255" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="2.835" y1="1.175" x2="2.965" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.175" x2="2.245" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="1.745" y1="1.175" x2="1.835" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="1.135" y1="1.175" x2="1.225" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.175" x2="1.075" y2="1.185" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.185" x2="8.865" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.185" x2="8.535" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="8.355" y1="1.185" x2="8.435" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="7.815" y1="1.185" x2="7.905" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="7.315" y1="1.185" x2="7.405" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="1.185" x2="6.815" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.185" x2="6.655" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="6.155" y1="1.185" x2="6.255" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="5.565" y1="1.185" x2="5.665" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.185" x2="5.355" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="5.175" y1="1.185" x2="5.255" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="4.635" y1="1.185" x2="4.725" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.185" x2="4.285" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="3.765" y1="1.185" x2="3.855" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="3.175" y1="1.185" x2="3.265" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.185" x2="2.965" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="2.825" y1="1.185" x2="2.895" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.185" x2="2.245" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="1.735" y1="1.185" x2="1.825" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="1.185" x2="1.235" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.185" x2="1.075" y2="1.195" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.195" x2="8.865" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.195" x2="8.535" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="8.345" y1="1.195" x2="8.435" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="7.825" y1="1.195" x2="7.915" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="7.295" y1="1.195" x2="7.395" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="6.735" y1="1.195" x2="6.825" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.195" x2="6.655" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="6.145" y1="1.195" x2="6.245" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="1.195" x2="5.675" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.195" x2="5.355" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="5.165" y1="1.195" x2="5.255" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="4.645" y1="1.195" x2="4.735" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.195" x2="4.285" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="3.755" y1="1.195" x2="3.845" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="3.185" y1="1.195" x2="3.285" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.195" x2="2.965" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="2.815" y1="1.195" x2="2.885" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="2.435" y1="1.195" x2="2.445" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.195" x2="2.245" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="1.725" y1="1.195" x2="1.815" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="1.195" x2="1.245" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.195" x2="1.075" y2="1.205" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.205" x2="8.865" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.205" x2="8.535" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="8.335" y1="1.205" x2="8.425" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="7.835" y1="1.205" x2="7.925" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="7.285" y1="1.205" x2="7.385" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="6.745" y1="1.205" x2="6.845" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.205" x2="6.655" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="6.135" y1="1.205" x2="6.225" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="5.585" y1="1.205" x2="5.695" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.205" x2="5.355" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="5.155" y1="1.205" x2="5.245" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="4.655" y1="1.205" x2="4.745" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.205" x2="4.285" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="3.735" y1="1.205" x2="3.835" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="3.195" y1="1.205" x2="3.295" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.205" x2="2.965" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="2.805" y1="1.205" x2="2.885" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="2.435" y1="1.205" x2="2.455" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.205" x2="2.245" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="1.705" y1="1.205" x2="1.805" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="1.205" x2="1.265" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.205" x2="1.075" y2="1.215" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.215" x2="8.865" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.215" x2="8.535" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="8.315" y1="1.215" x2="8.415" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="7.845" y1="1.215" x2="7.945" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="7.265" y1="1.215" x2="7.375" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="6.755" y1="1.215" x2="6.855" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.215" x2="6.655" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="6.115" y1="1.215" x2="6.215" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="5.605" y1="1.215" x2="5.705" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.215" x2="5.355" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="5.135" y1="1.215" x2="5.235" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="4.665" y1="1.215" x2="4.765" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.215" x2="4.285" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="3.725" y1="1.215" x2="3.825" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="3.205" y1="1.215" x2="3.315" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.215" x2="2.965" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="2.795" y1="1.215" x2="2.875" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="2.435" y1="1.215" x2="2.475" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.215" x2="2.245" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="1.695" y1="1.215" x2="1.795" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="1.215" x2="1.275" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.215" x2="1.075" y2="1.225" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.225" x2="8.865" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.225" x2="8.535" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="8.305" y1="1.225" x2="8.405" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="7.855" y1="1.225" x2="7.955" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="7.255" y1="1.225" x2="7.355" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="6.765" y1="1.225" x2="6.875" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.225" x2="6.655" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="6.105" y1="1.225" x2="6.205" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="5.615" y1="1.225" x2="5.725" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.225" x2="5.355" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="5.125" y1="1.225" x2="5.225" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="4.675" y1="1.225" x2="4.775" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.225" x2="4.285" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="3.705" y1="1.225" x2="3.815" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="3.215" y1="1.225" x2="3.335" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.225" x2="2.965" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="2.785" y1="1.225" x2="2.865" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="2.435" y1="1.225" x2="2.485" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.225" x2="2.245" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="1.675" y1="1.225" x2="1.785" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="1.225" x2="1.295" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.225" x2="1.075" y2="1.235" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.235" x2="8.865" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.235" x2="8.535" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="8.285" y1="1.235" x2="8.395" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="7.865" y1="1.235" x2="7.975" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="7.235" y1="1.235" x2="7.345" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="6.785" y1="1.235" x2="6.895" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.235" x2="6.655" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="6.085" y1="1.235" x2="6.195" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="5.625" y1="1.235" x2="5.745" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.235" x2="5.355" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="5.105" y1="1.235" x2="5.215" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="4.685" y1="1.235" x2="4.795" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.235" x2="4.285" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="3.685" y1="1.235" x2="3.795" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="3.235" y1="1.235" x2="3.355" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.235" x2="2.965" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="1.235" x2="2.855" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="2.435" y1="1.235" x2="2.505" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.235" x2="2.245" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="1.655" y1="1.235" x2="1.775" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="1.205" y1="1.235" x2="1.315" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.235" x2="1.075" y2="1.245" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.245" x2="8.865" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.245" x2="8.535" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="8.265" y1="1.245" x2="8.385" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="7.875" y1="1.245" x2="7.995" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="7.205" y1="1.245" x2="7.335" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="6.795" y1="1.245" x2="6.915" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.245" x2="6.655" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="6.055" y1="1.245" x2="6.175" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="5.635" y1="1.245" x2="5.775" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.245" x2="5.355" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="5.085" y1="1.245" x2="5.205" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="4.695" y1="1.245" x2="4.815" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.245" x2="4.285" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="3.665" y1="1.245" x2="3.785" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="3.245" y1="1.245" x2="3.375" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.245" x2="2.965" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="2.755" y1="1.245" x2="2.845" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="2.435" y1="1.245" x2="2.515" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.245" x2="2.245" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="1.635" y1="1.245" x2="1.755" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="1.215" y1="1.245" x2="1.335" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.245" x2="1.075" y2="1.255" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.255" x2="8.865" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.255" x2="8.535" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="8.235" y1="1.255" x2="8.375" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="7.895" y1="1.255" x2="8.015" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="7.175" y1="1.255" x2="7.315" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="6.815" y1="1.255" x2="6.945" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.255" x2="6.655" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="6.035" y1="1.255" x2="6.165" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="5.655" y1="1.255" x2="5.805" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.255" x2="5.355" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="5.055" y1="1.255" x2="5.195" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="4.715" y1="1.255" x2="4.835" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.255" x2="4.285" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="3.635" y1="1.255" x2="3.775" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="3.265" y1="1.255" x2="3.405" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.255" x2="2.965" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="2.725" y1="1.255" x2="2.835" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="2.435" y1="1.255" x2="2.545" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.255" x2="2.245" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="1.605" y1="1.255" x2="1.745" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="1.235" y1="1.255" x2="1.365" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.255" x2="1.075" y2="1.265" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.265" x2="8.865" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.265" x2="8.535" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="8.205" y1="1.265" x2="8.355" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="7.905" y1="1.265" x2="8.055" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="7.135" y1="1.265" x2="7.305" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="6.825" y1="1.265" x2="6.985" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.265" x2="6.655" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="5.985" y1="1.265" x2="6.145" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="5.665" y1="1.265" x2="5.845" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.265" x2="5.355" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="5.025" y1="1.265" x2="5.175" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="4.725" y1="1.265" x2="4.875" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.265" x2="4.285" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="3.595" y1="1.265" x2="3.755" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="3.275" y1="1.265" x2="3.455" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.265" x2="2.965" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="2.695" y1="1.265" x2="2.825" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="2.435" y1="1.265" x2="2.565" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.265" x2="2.245" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="1.565" y1="1.265" x2="1.725" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="1.245" y1="1.265" x2="1.405" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.265" x2="1.075" y2="1.275" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.275" x2="8.865" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.275" x2="8.535" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="7.925" y1="1.275" x2="8.345" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="6.845" y1="1.275" x2="7.285" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.275" x2="6.655" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="5.685" y1="1.275" x2="6.125" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.275" x2="5.355" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="4.745" y1="1.275" x2="5.165" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.275" x2="4.285" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="3.295" y1="1.275" x2="3.735" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.275" x2="2.965" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="2.455" y1="1.275" x2="2.805" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.275" x2="2.245" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="1.265" y1="1.275" x2="1.705" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.275" x2="1.075" y2="1.285" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.285" x2="8.865" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.285" x2="8.535" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="7.935" y1="1.285" x2="8.325" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="6.865" y1="1.285" x2="7.265" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.285" x2="6.655" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="5.705" y1="1.285" x2="6.105" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.285" x2="5.355" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="4.755" y1="1.285" x2="5.145" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.285" x2="4.285" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="3.315" y1="1.285" x2="3.715" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.285" x2="2.965" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="2.465" y1="1.285" x2="2.785" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.285" x2="2.245" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="1.285" y1="1.285" x2="1.685" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.285" x2="1.075" y2="1.295" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.295" x2="8.865" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.295" x2="8.535" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.295" x2="8.305" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="6.895" y1="1.295" x2="7.235" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="6.585" y1="1.295" x2="6.655" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="5.735" y1="1.295" x2="6.085" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.295" x2="5.355" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="4.775" y1="1.295" x2="5.125" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.295" x2="4.285" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="3.335" y1="1.295" x2="3.695" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.295" x2="2.965" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="2.485" y1="1.295" x2="2.775" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.295" x2="2.245" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="1.315" y1="1.295" x2="1.665" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.295" x2="1.075" y2="1.305" layer="201" rot="R180"/>
<rectangle x1="8.805" y1="1.305" x2="8.865" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="1.305" x2="8.535" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="7.985" y1="1.305" x2="8.275" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="6.915" y1="1.305" x2="7.215" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="6.595" y1="1.305" x2="6.655" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="5.755" y1="1.305" x2="6.055" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="5.295" y1="1.305" x2="5.355" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="4.805" y1="1.305" x2="5.095" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="4.225" y1="1.305" x2="4.285" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="3.365" y1="1.305" x2="3.665" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="1.305" x2="2.965" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="2.505" y1="1.305" x2="2.745" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="2.185" y1="1.305" x2="2.235" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="1.335" y1="1.305" x2="1.635" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="1.305" x2="1.075" y2="1.315" layer="201" rot="R180"/>
<rectangle x1="8.015" y1="1.315" x2="8.245" y2="1.325" layer="201" rot="R180"/>
<rectangle x1="6.955" y1="1.315" x2="7.175" y2="1.325" layer="201" rot="R180"/>
<rectangle x1="5.795" y1="1.315" x2="6.015" y2="1.325" layer="201" rot="R180"/>
<rectangle x1="4.835" y1="1.315" x2="5.065" y2="1.325" layer="201" rot="R180"/>
<rectangle x1="3.395" y1="1.315" x2="3.625" y2="1.325" layer="201" rot="R180"/>
<rectangle x1="2.535" y1="1.315" x2="2.715" y2="1.325" layer="201" rot="R180"/>
<rectangle x1="1.375" y1="1.315" x2="1.605" y2="1.325" layer="201" rot="R180"/>
<rectangle x1="8.065" y1="1.325" x2="8.195" y2="1.335" layer="201" rot="R180"/>
<rectangle x1="7.015" y1="1.325" x2="7.115" y2="1.335" layer="201" rot="R180"/>
<rectangle x1="5.855" y1="1.325" x2="5.955" y2="1.335" layer="201" rot="R180"/>
<rectangle x1="4.885" y1="1.325" x2="5.015" y2="1.335" layer="201" rot="R180"/>
<rectangle x1="3.455" y1="1.325" x2="3.565" y2="1.335" layer="201" rot="R180"/>
<rectangle x1="2.565" y1="1.325" x2="2.675" y2="1.335" layer="201" rot="R180"/>
<rectangle x1="1.425" y1="1.325" x2="1.555" y2="1.335" layer="201" rot="R180"/>
<rectangle x1="8.795" y1="1.535" x2="8.875" y2="1.545" layer="201" rot="R180"/>
<rectangle x1="4.215" y1="1.535" x2="4.285" y2="1.545" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.535" x2="2.245" y2="1.545" layer="201" rot="R180"/>
<rectangle x1="8.785" y1="1.545" x2="8.885" y2="1.555" layer="201" rot="R180"/>
<rectangle x1="4.205" y1="1.545" x2="4.305" y2="1.555" layer="201" rot="R180"/>
<rectangle x1="2.165" y1="1.545" x2="2.255" y2="1.555" layer="201" rot="R180"/>
<rectangle x1="8.785" y1="1.555" x2="8.895" y2="1.565" layer="201" rot="R180"/>
<rectangle x1="4.195" y1="1.555" x2="4.305" y2="1.565" layer="201" rot="R180"/>
<rectangle x1="2.155" y1="1.555" x2="2.265" y2="1.565" layer="201" rot="R180"/>
<rectangle x1="8.785" y1="1.565" x2="8.895" y2="1.575" layer="201" rot="R180"/>
<rectangle x1="4.195" y1="1.565" x2="4.305" y2="1.575" layer="201" rot="R180"/>
<rectangle x1="2.155" y1="1.565" x2="2.265" y2="1.575" layer="201" rot="R180"/>
<rectangle x1="8.775" y1="1.575" x2="8.895" y2="1.585" layer="201" rot="R180"/>
<rectangle x1="4.195" y1="1.575" x2="4.315" y2="1.585" layer="201" rot="R180"/>
<rectangle x1="2.155" y1="1.575" x2="2.265" y2="1.585" layer="201" rot="R180"/>
<rectangle x1="8.775" y1="1.585" x2="8.895" y2="1.595" layer="201" rot="R180"/>
<rectangle x1="4.195" y1="1.585" x2="4.315" y2="1.595" layer="201" rot="R180"/>
<rectangle x1="2.155" y1="1.585" x2="2.265" y2="1.595" layer="201" rot="R180"/>
<rectangle x1="8.785" y1="1.595" x2="8.895" y2="1.605" layer="201" rot="R180"/>
<rectangle x1="4.195" y1="1.595" x2="4.305" y2="1.605" layer="201" rot="R180"/>
<rectangle x1="2.155" y1="1.595" x2="2.265" y2="1.605" layer="201" rot="R180"/>
<rectangle x1="8.785" y1="1.605" x2="8.895" y2="1.615" layer="201" rot="R180"/>
<rectangle x1="4.195" y1="1.605" x2="4.305" y2="1.615" layer="201" rot="R180"/>
<rectangle x1="2.155" y1="1.605" x2="2.265" y2="1.615" layer="201" rot="R180"/>
<rectangle x1="8.785" y1="1.615" x2="8.885" y2="1.625" layer="201" rot="R180"/>
<rectangle x1="4.205" y1="1.615" x2="4.305" y2="1.625" layer="201" rot="R180"/>
<rectangle x1="2.165" y1="1.615" x2="2.255" y2="1.625" layer="201" rot="R180"/>
<rectangle x1="8.795" y1="1.625" x2="8.875" y2="1.635" layer="201" rot="R180"/>
<rectangle x1="4.215" y1="1.625" x2="4.285" y2="1.635" layer="201" rot="R180"/>
<rectangle x1="2.175" y1="1.625" x2="2.245" y2="1.635" layer="201" rot="R180"/>
<rectangle x1="6.465" y1="1.825" x2="6.735" y2="1.835" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.835" x2="8.895" y2="1.845" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.835" x2="8.165" y2="1.845" layer="201" rot="R180"/>
<rectangle x1="6.425" y1="1.835" x2="6.775" y2="1.845" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.835" x2="4.965" y2="1.845" layer="201" rot="R180"/>
<rectangle x1="2.855" y1="1.835" x2="3.385" y2="1.845" layer="201" rot="R180"/>
<rectangle x1="1.535" y1="1.835" x2="1.785" y2="1.845" layer="201" rot="R180"/>
<rectangle x1="0.895" y1="1.835" x2="1.135" y2="1.845" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.845" x2="8.895" y2="1.855" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.845" x2="8.165" y2="1.855" layer="201" rot="R180"/>
<rectangle x1="6.405" y1="1.845" x2="6.795" y2="1.855" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.845" x2="4.985" y2="1.855" layer="201" rot="R180"/>
<rectangle x1="2.835" y1="1.845" x2="3.385" y2="1.855" layer="201" rot="R180"/>
<rectangle x1="1.535" y1="1.845" x2="1.785" y2="1.855" layer="201" rot="R180"/>
<rectangle x1="0.895" y1="1.845" x2="1.135" y2="1.855" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.855" x2="8.895" y2="1.865" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.855" x2="8.165" y2="1.865" layer="201" rot="R180"/>
<rectangle x1="6.385" y1="1.855" x2="6.815" y2="1.865" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.855" x2="4.985" y2="1.865" layer="201" rot="R180"/>
<rectangle x1="2.815" y1="1.855" x2="3.385" y2="1.865" layer="201" rot="R180"/>
<rectangle x1="1.535" y1="1.855" x2="1.785" y2="1.865" layer="201" rot="R180"/>
<rectangle x1="0.905" y1="1.855" x2="1.135" y2="1.865" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.865" x2="8.895" y2="1.875" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.865" x2="8.165" y2="1.875" layer="201" rot="R180"/>
<rectangle x1="6.365" y1="1.865" x2="6.825" y2="1.875" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.865" x2="4.995" y2="1.875" layer="201" rot="R180"/>
<rectangle x1="2.805" y1="1.865" x2="3.385" y2="1.875" layer="201" rot="R180"/>
<rectangle x1="1.535" y1="1.865" x2="1.775" y2="1.875" layer="201" rot="R180"/>
<rectangle x1="0.905" y1="1.865" x2="1.135" y2="1.875" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.875" x2="8.895" y2="1.885" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.875" x2="8.165" y2="1.885" layer="201" rot="R180"/>
<rectangle x1="6.355" y1="1.875" x2="6.845" y2="1.885" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.875" x2="4.995" y2="1.885" layer="201" rot="R180"/>
<rectangle x1="2.795" y1="1.875" x2="3.385" y2="1.885" layer="201" rot="R180"/>
<rectangle x1="1.525" y1="1.875" x2="1.775" y2="1.885" layer="201" rot="R180"/>
<rectangle x1="0.905" y1="1.875" x2="1.145" y2="1.885" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.885" x2="8.895" y2="1.895" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.885" x2="8.165" y2="1.895" layer="201" rot="R180"/>
<rectangle x1="6.335" y1="1.885" x2="6.855" y2="1.895" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.885" x2="4.995" y2="1.895" layer="201" rot="R180"/>
<rectangle x1="2.785" y1="1.885" x2="3.385" y2="1.895" layer="201" rot="R180"/>
<rectangle x1="1.525" y1="1.885" x2="1.775" y2="1.895" layer="201" rot="R180"/>
<rectangle x1="0.915" y1="1.885" x2="1.145" y2="1.895" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.895" x2="8.895" y2="1.905" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.895" x2="8.165" y2="1.905" layer="201" rot="R180"/>
<rectangle x1="6.325" y1="1.895" x2="6.865" y2="1.905" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.895" x2="4.995" y2="1.905" layer="201" rot="R180"/>
<rectangle x1="2.785" y1="1.895" x2="3.385" y2="1.905" layer="201" rot="R180"/>
<rectangle x1="1.525" y1="1.895" x2="1.765" y2="1.905" layer="201" rot="R180"/>
<rectangle x1="0.915" y1="1.895" x2="1.145" y2="1.905" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.905" x2="8.895" y2="1.915" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.905" x2="8.165" y2="1.915" layer="201" rot="R180"/>
<rectangle x1="6.315" y1="1.905" x2="6.875" y2="1.915" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.905" x2="4.995" y2="1.915" layer="201" rot="R180"/>
<rectangle x1="2.775" y1="1.905" x2="3.385" y2="1.915" layer="201" rot="R180"/>
<rectangle x1="1.515" y1="1.905" x2="1.765" y2="1.915" layer="201" rot="R180"/>
<rectangle x1="0.915" y1="1.905" x2="1.145" y2="1.915" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.915" x2="8.895" y2="1.925" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.915" x2="8.165" y2="1.925" layer="201" rot="R180"/>
<rectangle x1="6.305" y1="1.915" x2="6.885" y2="1.925" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.915" x2="4.995" y2="1.925" layer="201" rot="R180"/>
<rectangle x1="2.775" y1="1.915" x2="3.385" y2="1.925" layer="201" rot="R180"/>
<rectangle x1="1.515" y1="1.915" x2="1.765" y2="1.925" layer="201" rot="R180"/>
<rectangle x1="0.915" y1="1.915" x2="1.155" y2="1.925" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.925" x2="8.895" y2="1.935" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.925" x2="8.165" y2="1.935" layer="201" rot="R180"/>
<rectangle x1="6.305" y1="1.925" x2="6.895" y2="1.935" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.925" x2="4.995" y2="1.935" layer="201" rot="R180"/>
<rectangle x1="2.775" y1="1.925" x2="3.385" y2="1.935" layer="201" rot="R180"/>
<rectangle x1="1.515" y1="1.925" x2="1.765" y2="1.935" layer="201" rot="R180"/>
<rectangle x1="0.925" y1="1.925" x2="1.155" y2="1.935" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.935" x2="8.895" y2="1.945" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.935" x2="8.165" y2="1.945" layer="201" rot="R180"/>
<rectangle x1="6.295" y1="1.935" x2="6.895" y2="1.945" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.935" x2="4.995" y2="1.945" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="1.935" x2="3.385" y2="1.945" layer="201" rot="R180"/>
<rectangle x1="1.515" y1="1.935" x2="1.755" y2="1.945" layer="201" rot="R180"/>
<rectangle x1="0.925" y1="1.935" x2="1.155" y2="1.945" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.945" x2="8.895" y2="1.955" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.945" x2="8.165" y2="1.955" layer="201" rot="R180"/>
<rectangle x1="6.285" y1="1.945" x2="6.905" y2="1.955" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.945" x2="4.995" y2="1.955" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="1.945" x2="3.385" y2="1.955" layer="201" rot="R180"/>
<rectangle x1="1.505" y1="1.945" x2="1.755" y2="1.955" layer="201" rot="R180"/>
<rectangle x1="0.925" y1="1.945" x2="1.155" y2="1.955" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.955" x2="8.895" y2="1.965" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.955" x2="8.165" y2="1.965" layer="201" rot="R180"/>
<rectangle x1="6.285" y1="1.955" x2="6.915" y2="1.965" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.955" x2="4.995" y2="1.965" layer="201" rot="R180"/>
<rectangle x1="2.755" y1="1.955" x2="3.385" y2="1.965" layer="201" rot="R180"/>
<rectangle x1="1.505" y1="1.955" x2="1.755" y2="1.965" layer="201" rot="R180"/>
<rectangle x1="0.935" y1="1.955" x2="1.165" y2="1.965" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.965" x2="8.895" y2="1.975" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.965" x2="8.165" y2="1.975" layer="201" rot="R180"/>
<rectangle x1="6.275" y1="1.965" x2="6.915" y2="1.975" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.965" x2="4.995" y2="1.975" layer="201" rot="R180"/>
<rectangle x1="2.755" y1="1.965" x2="3.385" y2="1.975" layer="201" rot="R180"/>
<rectangle x1="1.505" y1="1.965" x2="1.745" y2="1.975" layer="201" rot="R180"/>
<rectangle x1="0.935" y1="1.965" x2="1.165" y2="1.975" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.975" x2="8.895" y2="1.985" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.975" x2="8.165" y2="1.985" layer="201" rot="R180"/>
<rectangle x1="6.275" y1="1.975" x2="6.925" y2="1.985" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.975" x2="4.995" y2="1.985" layer="201" rot="R180"/>
<rectangle x1="2.745" y1="1.975" x2="3.385" y2="1.985" layer="201" rot="R180"/>
<rectangle x1="1.495" y1="1.975" x2="1.745" y2="1.985" layer="201" rot="R180"/>
<rectangle x1="0.935" y1="1.975" x2="1.165" y2="1.985" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.985" x2="8.895" y2="1.995" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.985" x2="8.165" y2="1.995" layer="201" rot="R180"/>
<rectangle x1="6.265" y1="1.985" x2="6.925" y2="1.995" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.985" x2="4.995" y2="1.995" layer="201" rot="R180"/>
<rectangle x1="2.745" y1="1.985" x2="3.385" y2="1.995" layer="201" rot="R180"/>
<rectangle x1="1.495" y1="1.985" x2="1.745" y2="1.995" layer="201" rot="R180"/>
<rectangle x1="0.935" y1="1.985" x2="1.165" y2="1.995" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="1.995" x2="8.895" y2="2.005" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="1.995" x2="8.165" y2="2.005" layer="201" rot="R180"/>
<rectangle x1="6.265" y1="1.995" x2="6.925" y2="2.005" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="1.995" x2="4.995" y2="2.005" layer="201" rot="R180"/>
<rectangle x1="2.735" y1="1.995" x2="3.385" y2="2.005" layer="201" rot="R180"/>
<rectangle x1="1.495" y1="1.995" x2="1.735" y2="2.005" layer="201" rot="R180"/>
<rectangle x1="0.945" y1="1.995" x2="1.175" y2="2.005" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.005" x2="8.895" y2="2.015" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.005" x2="8.165" y2="2.015" layer="201" rot="R180"/>
<rectangle x1="6.645" y1="2.005" x2="6.935" y2="2.015" layer="201" rot="R180"/>
<rectangle x1="6.265" y1="2.005" x2="6.555" y2="2.015" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.005" x2="4.995" y2="2.015" layer="201" rot="R180"/>
<rectangle x1="2.735" y1="2.005" x2="3.385" y2="2.015" layer="201" rot="R180"/>
<rectangle x1="1.495" y1="2.005" x2="1.735" y2="2.015" layer="201" rot="R180"/>
<rectangle x1="0.945" y1="2.005" x2="1.175" y2="2.015" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.015" x2="8.895" y2="2.025" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.015" x2="8.165" y2="2.025" layer="201" rot="R180"/>
<rectangle x1="6.665" y1="2.015" x2="6.935" y2="2.025" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.015" x2="6.525" y2="2.025" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.015" x2="4.995" y2="2.025" layer="201" rot="R180"/>
<rectangle x1="2.735" y1="2.015" x2="3.385" y2="2.025" layer="201" rot="R180"/>
<rectangle x1="1.485" y1="2.015" x2="1.735" y2="2.025" layer="201" rot="R180"/>
<rectangle x1="0.945" y1="2.015" x2="1.175" y2="2.025" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.025" x2="8.895" y2="2.035" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.025" x2="8.165" y2="2.035" layer="201" rot="R180"/>
<rectangle x1="6.685" y1="2.025" x2="6.935" y2="2.035" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.025" x2="6.515" y2="2.035" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.025" x2="4.995" y2="2.035" layer="201" rot="R180"/>
<rectangle x1="2.725" y1="2.025" x2="3.385" y2="2.035" layer="201" rot="R180"/>
<rectangle x1="1.485" y1="2.025" x2="1.735" y2="2.035" layer="201" rot="R180"/>
<rectangle x1="0.945" y1="2.025" x2="1.175" y2="2.035" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.035" x2="8.895" y2="2.045" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.035" x2="8.165" y2="2.045" layer="201" rot="R180"/>
<rectangle x1="6.695" y1="2.035" x2="6.935" y2="2.045" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.035" x2="6.495" y2="2.045" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.035" x2="4.995" y2="2.045" layer="201" rot="R180"/>
<rectangle x1="2.725" y1="2.035" x2="3.385" y2="2.045" layer="201" rot="R180"/>
<rectangle x1="1.485" y1="2.035" x2="1.725" y2="2.045" layer="201" rot="R180"/>
<rectangle x1="0.955" y1="2.035" x2="1.185" y2="2.045" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.045" x2="8.895" y2="2.055" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.045" x2="8.165" y2="2.055" layer="201" rot="R180"/>
<rectangle x1="6.705" y1="2.045" x2="6.935" y2="2.055" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.045" x2="6.495" y2="2.055" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.045" x2="4.995" y2="2.055" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.045" x2="3.385" y2="2.055" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="2.045" x2="1.725" y2="2.055" layer="201" rot="R180"/>
<rectangle x1="0.955" y1="2.045" x2="1.185" y2="2.055" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.055" x2="8.895" y2="2.065" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.055" x2="8.165" y2="2.065" layer="201" rot="R180"/>
<rectangle x1="6.705" y1="2.055" x2="6.935" y2="2.065" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.055" x2="6.485" y2="2.065" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.055" x2="4.995" y2="2.065" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.055" x2="3.385" y2="2.065" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="2.055" x2="1.725" y2="2.065" layer="201" rot="R180"/>
<rectangle x1="0.955" y1="2.055" x2="1.185" y2="2.065" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.065" x2="8.895" y2="2.075" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.065" x2="8.165" y2="2.075" layer="201" rot="R180"/>
<rectangle x1="6.715" y1="2.065" x2="6.935" y2="2.075" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.065" x2="6.475" y2="2.075" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.065" x2="4.995" y2="2.075" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.065" x2="3.385" y2="2.075" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="2.065" x2="1.715" y2="2.075" layer="201" rot="R180"/>
<rectangle x1="0.965" y1="2.065" x2="1.185" y2="2.075" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.075" x2="8.895" y2="2.085" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.075" x2="8.165" y2="2.085" layer="201" rot="R180"/>
<rectangle x1="6.715" y1="2.075" x2="6.945" y2="2.085" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.075" x2="6.475" y2="2.085" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.075" x2="4.995" y2="2.085" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.075" x2="3.385" y2="2.085" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="2.075" x2="1.715" y2="2.085" layer="201" rot="R180"/>
<rectangle x1="0.965" y1="2.075" x2="1.195" y2="2.085" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.085" x2="8.895" y2="2.095" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.085" x2="8.165" y2="2.095" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.085" x2="6.945" y2="2.095" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.085" x2="6.475" y2="2.095" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.085" x2="4.995" y2="2.095" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.085" x2="3.385" y2="2.095" layer="201" rot="R180"/>
<rectangle x1="1.465" y1="2.085" x2="1.715" y2="2.095" layer="201" rot="R180"/>
<rectangle x1="0.965" y1="2.085" x2="1.195" y2="2.095" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.095" x2="8.895" y2="2.105" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.095" x2="8.165" y2="2.105" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.095" x2="6.945" y2="2.105" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.095" x2="6.465" y2="2.105" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.095" x2="4.995" y2="2.105" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.095" x2="3.385" y2="2.105" layer="201" rot="R180"/>
<rectangle x1="1.465" y1="2.095" x2="1.715" y2="2.105" layer="201" rot="R180"/>
<rectangle x1="0.965" y1="2.095" x2="1.195" y2="2.105" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.105" x2="8.895" y2="2.115" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.105" x2="8.165" y2="2.115" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.105" x2="6.945" y2="2.115" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.105" x2="6.465" y2="2.115" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.105" x2="4.995" y2="2.115" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.105" x2="3.385" y2="2.115" layer="201" rot="R180"/>
<rectangle x1="1.465" y1="2.105" x2="1.705" y2="2.115" layer="201" rot="R180"/>
<rectangle x1="0.975" y1="2.105" x2="1.195" y2="2.115" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.115" x2="8.895" y2="2.125" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.115" x2="8.165" y2="2.125" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.115" x2="6.945" y2="2.125" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.115" x2="6.465" y2="2.125" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.115" x2="4.995" y2="2.125" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.115" x2="3.385" y2="2.125" layer="201" rot="R180"/>
<rectangle x1="1.455" y1="2.115" x2="1.705" y2="2.125" layer="201" rot="R180"/>
<rectangle x1="0.975" y1="2.115" x2="1.205" y2="2.125" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.125" x2="8.895" y2="2.135" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.125" x2="8.165" y2="2.135" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.125" x2="6.945" y2="2.135" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.125" x2="6.465" y2="2.135" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.125" x2="4.995" y2="2.135" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.125" x2="3.385" y2="2.135" layer="201" rot="R180"/>
<rectangle x1="1.455" y1="2.125" x2="1.705" y2="2.135" layer="201" rot="R180"/>
<rectangle x1="0.975" y1="2.125" x2="1.205" y2="2.135" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.135" x2="8.895" y2="2.145" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.135" x2="8.165" y2="2.145" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.135" x2="6.945" y2="2.145" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.135" x2="6.465" y2="2.145" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.135" x2="4.995" y2="2.145" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.135" x2="3.385" y2="2.145" layer="201" rot="R180"/>
<rectangle x1="1.455" y1="2.135" x2="1.695" y2="2.145" layer="201" rot="R180"/>
<rectangle x1="0.985" y1="2.135" x2="1.205" y2="2.145" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.145" x2="8.895" y2="2.155" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.145" x2="8.165" y2="2.155" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.145" x2="6.945" y2="2.155" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.145" x2="6.465" y2="2.155" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.145" x2="4.995" y2="2.155" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.145" x2="3.385" y2="2.155" layer="201" rot="R180"/>
<rectangle x1="1.455" y1="2.145" x2="1.695" y2="2.155" layer="201" rot="R180"/>
<rectangle x1="0.985" y1="2.145" x2="1.205" y2="2.155" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.155" x2="8.895" y2="2.165" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.155" x2="8.165" y2="2.165" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.155" x2="6.945" y2="2.165" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.155" x2="6.465" y2="2.165" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.155" x2="4.995" y2="2.165" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.155" x2="3.385" y2="2.165" layer="201" rot="R180"/>
<rectangle x1="1.445" y1="2.155" x2="1.695" y2="2.165" layer="201" rot="R180"/>
<rectangle x1="0.985" y1="2.155" x2="1.215" y2="2.165" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.165" x2="8.895" y2="2.175" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.165" x2="8.165" y2="2.175" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.165" x2="6.945" y2="2.175" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.165" x2="6.465" y2="2.175" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.165" x2="4.995" y2="2.175" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.165" x2="3.385" y2="2.175" layer="201" rot="R180"/>
<rectangle x1="1.445" y1="2.165" x2="1.685" y2="2.175" layer="201" rot="R180"/>
<rectangle x1="0.985" y1="2.165" x2="1.215" y2="2.175" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.175" x2="8.895" y2="2.185" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.175" x2="8.165" y2="2.185" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.175" x2="6.945" y2="2.185" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.175" x2="6.465" y2="2.185" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.175" x2="4.995" y2="2.185" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.175" x2="3.385" y2="2.185" layer="201" rot="R180"/>
<rectangle x1="1.445" y1="2.175" x2="1.685" y2="2.185" layer="201" rot="R180"/>
<rectangle x1="0.995" y1="2.175" x2="1.215" y2="2.185" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.185" x2="8.895" y2="2.195" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.185" x2="8.165" y2="2.195" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.185" x2="6.945" y2="2.195" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.185" x2="6.465" y2="2.195" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.185" x2="4.995" y2="2.195" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.185" x2="3.385" y2="2.195" layer="201" rot="R180"/>
<rectangle x1="1.435" y1="2.185" x2="1.685" y2="2.195" layer="201" rot="R180"/>
<rectangle x1="0.995" y1="2.185" x2="1.225" y2="2.195" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.195" x2="8.895" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="8.325" y1="2.195" x2="8.425" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.195" x2="8.165" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.195" x2="6.945" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.195" x2="6.465" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.195" x2="4.995" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.195" x2="3.385" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="1.435" y1="2.195" x2="1.685" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="0.995" y1="2.195" x2="1.225" y2="2.205" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.205" x2="8.895" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="8.325" y1="2.205" x2="8.455" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.205" x2="8.165" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.205" x2="6.945" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.205" x2="6.465" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.205" x2="4.995" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.205" x2="3.385" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="1.435" y1="2.205" x2="1.675" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="1.005" y1="2.205" x2="1.225" y2="2.215" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.215" x2="8.895" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="8.325" y1="2.215" x2="8.475" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.215" x2="8.165" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.215" x2="6.945" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.215" x2="6.465" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.215" x2="4.995" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.215" x2="3.385" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="1.435" y1="2.215" x2="1.675" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="1.005" y1="2.215" x2="1.225" y2="2.225" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.225" x2="8.895" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="8.325" y1="2.225" x2="8.485" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.225" x2="8.165" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.225" x2="6.945" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.225" x2="6.465" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.225" x2="4.995" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.225" x2="3.385" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="1.425" y1="2.225" x2="1.675" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="1.005" y1="2.225" x2="1.235" y2="2.235" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.235" x2="8.895" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="8.315" y1="2.235" x2="8.495" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.235" x2="8.165" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.235" x2="6.945" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.235" x2="6.465" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.235" x2="4.995" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.235" x2="3.385" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="1.425" y1="2.235" x2="1.665" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="1.005" y1="2.235" x2="1.235" y2="2.245" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.245" x2="8.895" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="8.315" y1="2.245" x2="8.505" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.245" x2="8.165" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.245" x2="6.945" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.245" x2="6.465" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.245" x2="4.995" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.245" x2="3.385" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="1.425" y1="2.245" x2="1.665" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="2.245" x2="1.235" y2="2.255" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.255" x2="8.895" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="8.315" y1="2.255" x2="8.515" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.255" x2="8.165" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.255" x2="6.945" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.255" x2="6.465" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.255" x2="4.995" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.255" x2="3.385" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="1.415" y1="2.255" x2="1.665" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="2.255" x2="1.235" y2="2.265" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.265" x2="8.895" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="8.315" y1="2.265" x2="8.515" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.265" x2="8.165" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.265" x2="6.945" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.265" x2="6.465" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.265" x2="4.995" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.265" x2="3.385" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="1.415" y1="2.265" x2="1.655" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="2.265" x2="1.245" y2="2.275" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.275" x2="8.895" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="8.305" y1="2.275" x2="8.525" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.275" x2="8.165" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.275" x2="6.945" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.275" x2="6.465" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.275" x2="4.995" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.275" x2="3.385" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="1.415" y1="2.275" x2="1.655" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="1.025" y1="2.275" x2="1.245" y2="2.285" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.285" x2="8.895" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="8.305" y1="2.285" x2="8.535" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.285" x2="8.165" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.285" x2="6.945" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.285" x2="6.465" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.285" x2="4.995" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.285" x2="3.385" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="1.415" y1="2.285" x2="1.655" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="1.025" y1="2.285" x2="1.245" y2="2.295" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.295" x2="8.895" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="8.305" y1="2.295" x2="8.535" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.295" x2="8.165" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.295" x2="6.945" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.295" x2="6.465" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.295" x2="4.995" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.295" x2="3.385" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="1.405" y1="2.295" x2="1.655" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="1.025" y1="2.295" x2="1.245" y2="2.305" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.305" x2="8.895" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="8.305" y1="2.305" x2="8.535" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.305" x2="8.165" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.305" x2="6.945" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.305" x2="6.465" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.305" x2="4.995" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.305" x2="3.385" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="1.405" y1="2.305" x2="1.645" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="1.025" y1="2.305" x2="1.255" y2="2.315" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.315" x2="8.895" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="8.305" y1="2.315" x2="8.545" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.315" x2="8.165" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.315" x2="6.945" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.315" x2="6.465" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.315" x2="4.995" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.315" x2="3.385" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="1.405" y1="2.315" x2="1.645" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="1.035" y1="2.315" x2="1.255" y2="2.325" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.325" x2="8.895" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="8.295" y1="2.325" x2="8.545" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.325" x2="8.165" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.325" x2="6.945" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.325" x2="6.465" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.325" x2="4.995" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.325" x2="3.385" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="1.395" y1="2.325" x2="1.645" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="1.035" y1="2.325" x2="1.255" y2="2.335" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.335" x2="8.895" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="8.295" y1="2.335" x2="8.545" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.335" x2="8.165" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.335" x2="6.945" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.335" x2="6.465" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.335" x2="4.995" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.335" x2="3.385" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="1.395" y1="2.335" x2="1.635" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="1.035" y1="2.335" x2="1.255" y2="2.345" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.345" x2="8.895" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="8.295" y1="2.345" x2="8.555" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.345" x2="8.165" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.345" x2="6.945" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.345" x2="6.465" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.345" x2="4.995" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.345" x2="3.385" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="1.395" y1="2.345" x2="1.635" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="1.035" y1="2.345" x2="1.265" y2="2.355" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.355" x2="8.895" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="8.295" y1="2.355" x2="8.555" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.355" x2="8.165" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.355" x2="6.945" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.355" x2="6.465" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.355" x2="4.995" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.355" x2="3.385" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="1.395" y1="2.355" x2="1.635" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="1.045" y1="2.355" x2="1.265" y2="2.365" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.365" x2="8.895" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="8.285" y1="2.365" x2="8.555" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.365" x2="8.165" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.365" x2="6.945" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.365" x2="6.465" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.365" x2="4.995" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.365" x2="3.385" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="2.365" x2="1.625" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="1.045" y1="2.365" x2="1.265" y2="2.375" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.375" x2="8.895" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="8.285" y1="2.375" x2="8.555" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.375" x2="8.165" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.375" x2="6.945" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.375" x2="6.465" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.375" x2="4.995" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.375" x2="3.385" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="2.375" x2="1.625" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="1.045" y1="2.375" x2="1.265" y2="2.385" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.385" x2="8.895" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="8.285" y1="2.385" x2="8.565" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.385" x2="8.165" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.385" x2="6.945" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.385" x2="6.465" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.385" x2="4.995" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.385" x2="3.385" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="2.385" x2="1.625" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="1.055" y1="2.385" x2="1.275" y2="2.395" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.395" x2="8.895" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="8.285" y1="2.395" x2="8.565" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.395" x2="8.165" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.395" x2="6.945" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.395" x2="6.465" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.395" x2="4.995" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.395" x2="3.385" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="2.395" x2="1.625" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="1.055" y1="2.395" x2="1.275" y2="2.405" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.405" x2="8.895" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="8.275" y1="2.405" x2="8.565" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.405" x2="8.165" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.405" x2="6.945" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.405" x2="6.465" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.405" x2="4.995" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.405" x2="3.385" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="1.375" y1="2.405" x2="1.615" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="1.055" y1="2.405" x2="1.275" y2="2.415" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.415" x2="8.895" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="8.275" y1="2.415" x2="8.565" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.415" x2="8.165" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.415" x2="6.945" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.415" x2="6.465" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.415" x2="4.995" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.415" x2="3.385" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="1.375" y1="2.415" x2="1.615" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="1.055" y1="2.415" x2="1.275" y2="2.425" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.425" x2="8.895" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="8.275" y1="2.425" x2="8.565" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.425" x2="8.165" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.425" x2="6.945" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.425" x2="6.465" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.425" x2="4.995" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.425" x2="3.385" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="1.375" y1="2.425" x2="1.615" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="1.065" y1="2.425" x2="1.285" y2="2.435" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.435" x2="8.895" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="8.275" y1="2.435" x2="8.575" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.435" x2="8.165" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.435" x2="6.945" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.435" x2="6.465" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.435" x2="4.995" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.435" x2="3.385" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="1.365" y1="2.435" x2="1.605" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="1.065" y1="2.435" x2="1.285" y2="2.445" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.445" x2="8.895" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="8.275" y1="2.445" x2="8.575" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.445" x2="8.165" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.445" x2="6.945" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.445" x2="6.465" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.445" x2="4.995" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.445" x2="3.385" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="1.365" y1="2.445" x2="1.605" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="1.065" y1="2.445" x2="1.285" y2="2.455" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.455" x2="8.895" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="8.265" y1="2.455" x2="8.575" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.455" x2="8.165" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.455" x2="6.945" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.455" x2="6.465" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.455" x2="4.995" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.455" x2="3.385" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="1.365" y1="2.455" x2="1.605" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="1.075" y1="2.455" x2="1.285" y2="2.465" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.465" x2="8.895" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="8.265" y1="2.465" x2="8.575" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.465" x2="8.165" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.465" x2="6.945" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.465" x2="6.465" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.465" x2="4.995" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.465" x2="3.385" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="1.365" y1="2.465" x2="1.595" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="1.075" y1="2.465" x2="1.295" y2="2.475" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.475" x2="8.895" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="8.265" y1="2.475" x2="8.585" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.475" x2="8.165" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.475" x2="6.945" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.475" x2="6.465" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.475" x2="4.995" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.475" x2="3.385" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="1.355" y1="2.475" x2="1.595" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="1.075" y1="2.475" x2="1.295" y2="2.485" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.485" x2="8.895" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="8.265" y1="2.485" x2="8.585" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.485" x2="8.165" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.485" x2="6.945" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.485" x2="6.465" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.485" x2="4.995" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.485" x2="3.385" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="1.355" y1="2.485" x2="1.595" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="1.075" y1="2.485" x2="1.295" y2="2.495" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.495" x2="8.895" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="8.255" y1="2.495" x2="8.585" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.495" x2="8.165" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.495" x2="6.945" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.495" x2="6.465" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.495" x2="4.995" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.495" x2="3.385" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="1.355" y1="2.495" x2="1.595" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="2.495" x2="1.295" y2="2.505" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.505" x2="8.895" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="8.255" y1="2.505" x2="8.585" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.505" x2="8.165" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.505" x2="6.945" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.505" x2="6.465" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.505" x2="4.995" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.505" x2="3.385" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="2.505" x2="1.585" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="2.505" x2="1.305" y2="2.515" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.515" x2="8.895" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="8.255" y1="2.515" x2="8.595" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.515" x2="8.165" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.515" x2="6.945" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.515" x2="6.465" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.515" x2="4.995" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.515" x2="3.385" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="2.515" x2="1.585" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="2.515" x2="1.305" y2="2.525" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.525" x2="8.895" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="8.255" y1="2.525" x2="8.595" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.525" x2="8.165" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.525" x2="6.945" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.525" x2="6.465" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.525" x2="4.995" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.525" x2="3.385" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="2.525" x2="1.585" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="2.525" x2="1.305" y2="2.535" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.535" x2="8.895" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="8.255" y1="2.535" x2="8.595" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.535" x2="8.165" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.535" x2="6.945" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.535" x2="6.465" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.535" x2="4.995" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.535" x2="3.385" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="2.535" x2="1.575" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="2.535" x2="1.305" y2="2.545" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.545" x2="8.895" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="8.245" y1="2.545" x2="8.595" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.545" x2="8.165" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.545" x2="6.945" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.545" x2="6.465" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.545" x2="4.995" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.545" x2="3.385" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="1.335" y1="2.545" x2="1.575" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="2.545" x2="1.315" y2="2.555" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.555" x2="8.895" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="8.245" y1="2.555" x2="8.595" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.555" x2="8.165" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.555" x2="6.945" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.555" x2="6.465" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.555" x2="4.995" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.555" x2="3.385" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="1.335" y1="2.555" x2="1.575" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="2.555" x2="1.315" y2="2.565" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.565" x2="8.895" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="8.245" y1="2.565" x2="8.605" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.565" x2="8.165" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.565" x2="6.945" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.565" x2="6.465" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.565" x2="4.995" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.565" x2="3.385" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="1.335" y1="2.565" x2="1.565" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="1.105" y1="2.565" x2="1.315" y2="2.575" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.575" x2="8.895" y2="2.585" layer="201" rot="R180"/>
<rectangle x1="8.245" y1="2.575" x2="8.605" y2="2.585" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.575" x2="8.165" y2="2.585" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.575" x2="6.945" y2="2.585" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.575" x2="6.465" y2="2.585" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.575" x2="4.995" y2="2.585" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.575" x2="3.385" y2="2.585" layer="201" rot="R180"/>
<rectangle x1="1.105" y1="2.575" x2="1.565" y2="2.585" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.585" x2="8.895" y2="2.595" layer="201" rot="R180"/>
<rectangle x1="8.235" y1="2.585" x2="8.605" y2="2.595" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.585" x2="8.165" y2="2.595" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.585" x2="6.945" y2="2.595" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.585" x2="6.465" y2="2.595" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.585" x2="4.995" y2="2.595" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.585" x2="3.385" y2="2.595" layer="201" rot="R180"/>
<rectangle x1="1.105" y1="2.585" x2="1.565" y2="2.595" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.595" x2="8.895" y2="2.605" layer="201" rot="R180"/>
<rectangle x1="8.235" y1="2.595" x2="8.605" y2="2.605" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.595" x2="8.165" y2="2.605" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.595" x2="6.945" y2="2.605" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.595" x2="6.465" y2="2.605" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.595" x2="4.995" y2="2.605" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.595" x2="3.385" y2="2.605" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="2.595" x2="1.565" y2="2.605" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.605" x2="8.895" y2="2.615" layer="201" rot="R180"/>
<rectangle x1="8.235" y1="2.605" x2="8.615" y2="2.615" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.605" x2="8.165" y2="2.615" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.605" x2="6.945" y2="2.615" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.605" x2="6.465" y2="2.615" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.605" x2="4.995" y2="2.615" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.605" x2="3.385" y2="2.615" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="2.605" x2="1.555" y2="2.615" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.615" x2="8.895" y2="2.625" layer="201" rot="R180"/>
<rectangle x1="8.235" y1="2.615" x2="8.615" y2="2.625" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.615" x2="8.165" y2="2.625" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.615" x2="6.945" y2="2.625" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.615" x2="6.465" y2="2.625" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.615" x2="4.995" y2="2.625" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.615" x2="3.385" y2="2.625" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="2.615" x2="1.555" y2="2.625" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.625" x2="8.895" y2="2.635" layer="201" rot="R180"/>
<rectangle x1="8.235" y1="2.625" x2="8.615" y2="2.635" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.625" x2="8.165" y2="2.635" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.625" x2="6.945" y2="2.635" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.625" x2="6.465" y2="2.635" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.625" x2="4.995" y2="2.635" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.625" x2="3.385" y2="2.635" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="2.625" x2="1.555" y2="2.635" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.635" x2="8.895" y2="2.645" layer="201" rot="R180"/>
<rectangle x1="8.225" y1="2.635" x2="8.615" y2="2.645" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.635" x2="8.165" y2="2.645" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.635" x2="6.945" y2="2.645" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.635" x2="6.465" y2="2.645" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.635" x2="4.995" y2="2.645" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.635" x2="3.385" y2="2.645" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="2.635" x2="1.545" y2="2.645" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.645" x2="8.895" y2="2.655" layer="201" rot="R180"/>
<rectangle x1="8.225" y1="2.645" x2="8.625" y2="2.655" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.645" x2="8.165" y2="2.655" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.645" x2="6.945" y2="2.655" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.645" x2="6.465" y2="2.655" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.645" x2="4.995" y2="2.655" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.645" x2="3.385" y2="2.655" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="2.645" x2="1.545" y2="2.655" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.655" x2="8.895" y2="2.665" layer="201" rot="R180"/>
<rectangle x1="8.225" y1="2.655" x2="8.625" y2="2.665" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.655" x2="8.165" y2="2.665" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.655" x2="6.945" y2="2.665" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.655" x2="6.465" y2="2.665" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.655" x2="4.995" y2="2.665" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.655" x2="3.385" y2="2.665" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="2.655" x2="1.545" y2="2.665" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.665" x2="8.895" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="8.425" y1="2.665" x2="8.625" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="8.225" y1="2.665" x2="8.415" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.665" x2="8.165" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.665" x2="6.945" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.665" x2="6.465" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.665" x2="4.995" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.665" x2="3.385" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="2.665" x2="1.535" y2="2.675" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.675" x2="8.895" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="8.425" y1="2.675" x2="8.625" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="8.215" y1="2.675" x2="8.415" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.675" x2="8.165" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.675" x2="6.945" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.675" x2="6.465" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.675" x2="4.995" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.675" x2="3.385" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="1.135" y1="2.675" x2="1.535" y2="2.685" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.685" x2="8.895" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="8.425" y1="2.685" x2="8.625" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="8.215" y1="2.685" x2="8.415" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.685" x2="8.165" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.685" x2="6.945" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.685" x2="6.465" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.685" x2="4.995" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.685" x2="3.385" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="1.135" y1="2.685" x2="1.535" y2="2.695" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.695" x2="8.895" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="8.425" y1="2.695" x2="8.635" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="8.215" y1="2.695" x2="8.405" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.695" x2="8.165" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.695" x2="6.945" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.695" x2="6.465" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.695" x2="4.995" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.695" x2="3.385" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="1.135" y1="2.695" x2="1.535" y2="2.705" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.705" x2="8.895" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="8.425" y1="2.705" x2="8.635" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="8.215" y1="2.705" x2="8.405" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.705" x2="8.165" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.705" x2="6.945" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.705" x2="6.465" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.705" x2="4.995" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.705" x2="3.385" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="2.705" x2="1.525" y2="2.715" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.715" x2="8.895" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="8.435" y1="2.715" x2="8.635" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="8.215" y1="2.715" x2="8.405" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.715" x2="8.165" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.715" x2="6.945" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.715" x2="6.465" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.715" x2="4.995" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.715" x2="3.385" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="2.715" x2="1.525" y2="2.725" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.725" x2="8.895" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="8.435" y1="2.725" x2="8.635" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="8.205" y1="2.725" x2="8.405" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.725" x2="8.165" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.725" x2="6.945" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.725" x2="6.465" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.725" x2="4.995" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.725" x2="3.385" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="2.725" x2="1.525" y2="2.735" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.735" x2="8.895" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="8.435" y1="2.735" x2="8.645" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="8.205" y1="2.735" x2="8.405" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.735" x2="8.165" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.735" x2="6.945" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.735" x2="6.465" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.735" x2="4.995" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.735" x2="3.385" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="2.735" x2="1.515" y2="2.745" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.745" x2="8.895" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="8.435" y1="2.745" x2="8.645" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="8.205" y1="2.745" x2="8.395" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.745" x2="8.165" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.745" x2="6.945" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.745" x2="6.465" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.745" x2="4.995" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.745" x2="3.385" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="2.745" x2="1.515" y2="2.755" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.755" x2="8.895" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="8.445" y1="2.755" x2="8.645" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="8.205" y1="2.755" x2="8.395" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.755" x2="8.165" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.755" x2="6.945" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.755" x2="6.465" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.755" x2="4.995" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.755" x2="3.385" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="2.755" x2="1.515" y2="2.765" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.765" x2="8.895" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="8.445" y1="2.765" x2="8.645" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="8.195" y1="2.765" x2="8.395" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.765" x2="8.165" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.765" x2="6.945" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.765" x2="6.465" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.765" x2="4.995" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.765" x2="3.385" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="2.765" x2="1.505" y2="2.775" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.775" x2="8.895" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="8.445" y1="2.775" x2="8.655" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="8.195" y1="2.775" x2="8.395" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.775" x2="8.165" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.775" x2="6.945" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.775" x2="6.465" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.775" x2="4.995" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.775" x2="3.385" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="2.775" x2="1.505" y2="2.785" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.785" x2="8.895" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="8.445" y1="2.785" x2="8.655" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="8.195" y1="2.785" x2="8.395" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.785" x2="8.165" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.785" x2="6.945" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.785" x2="6.465" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.785" x2="4.995" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.785" x2="3.385" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="2.785" x2="1.505" y2="2.795" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.795" x2="8.895" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="8.445" y1="2.795" x2="8.655" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="8.195" y1="2.795" x2="8.385" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.795" x2="8.165" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.795" x2="6.945" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.795" x2="6.465" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.795" x2="4.995" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="2.795" x2="3.385" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="2.795" x2="1.505" y2="2.805" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.805" x2="8.895" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="8.455" y1="2.805" x2="8.655" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="8.185" y1="2.805" x2="8.385" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.805" x2="8.165" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.805" x2="6.945" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.805" x2="6.465" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.805" x2="4.995" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="2.805" x2="3.385" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="2.805" x2="1.495" y2="2.815" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.815" x2="8.895" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="8.455" y1="2.815" x2="8.655" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="8.185" y1="2.815" x2="8.385" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.815" x2="8.165" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.815" x2="6.945" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.815" x2="6.465" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.815" x2="4.995" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="3.145" y1="2.815" x2="3.385" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="2.815" x2="1.495" y2="2.825" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.825" x2="8.895" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="8.455" y1="2.825" x2="8.665" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="8.185" y1="2.825" x2="8.385" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.825" x2="8.165" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.825" x2="6.945" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.825" x2="6.465" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.825" x2="4.995" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="2.995" y1="2.825" x2="3.385" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="2.825" x2="1.495" y2="2.835" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.835" x2="8.895" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="8.455" y1="2.835" x2="8.665" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="8.185" y1="2.835" x2="8.385" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.835" x2="8.165" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.835" x2="6.945" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.835" x2="6.465" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.835" x2="4.995" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="2.965" y1="2.835" x2="3.385" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="2.835" x2="1.485" y2="2.845" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.845" x2="8.895" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="8.455" y1="2.845" x2="8.665" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="8.185" y1="2.845" x2="8.375" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.845" x2="8.165" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.845" x2="6.945" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.845" x2="6.465" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.845" x2="4.995" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="2.945" y1="2.845" x2="3.385" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="2.845" x2="1.485" y2="2.855" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.855" x2="8.895" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="2.855" x2="8.665" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="8.175" y1="2.855" x2="8.375" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.855" x2="8.165" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.855" x2="6.945" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.855" x2="6.465" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.855" x2="4.995" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="2.935" y1="2.855" x2="3.385" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="2.855" x2="1.485" y2="2.865" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.865" x2="8.895" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="2.865" x2="8.675" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="8.175" y1="2.865" x2="8.375" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.865" x2="8.165" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.865" x2="6.945" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.865" x2="6.465" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.865" x2="4.995" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="2.925" y1="2.865" x2="3.385" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="2.865" x2="1.475" y2="2.875" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.875" x2="8.895" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="2.875" x2="8.675" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="8.175" y1="2.875" x2="8.375" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.875" x2="8.165" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.875" x2="6.945" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.875" x2="6.465" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.875" x2="4.995" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="2.915" y1="2.875" x2="3.385" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="2.875" x2="1.475" y2="2.885" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="2.885" x2="8.895" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="2.885" x2="8.675" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="8.175" y1="2.885" x2="8.375" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.885" x2="8.165" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.885" x2="6.945" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.885" x2="6.465" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.885" x2="4.995" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="2.915" y1="2.885" x2="3.385" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="1.195" y1="2.885" x2="1.475" y2="2.895" layer="201" rot="R180"/>
<rectangle x1="8.465" y1="2.895" x2="8.895" y2="2.905" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.895" x2="8.365" y2="2.905" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.895" x2="6.945" y2="2.905" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.895" x2="6.465" y2="2.905" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.895" x2="4.995" y2="2.905" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="2.895" x2="3.385" y2="2.905" layer="201" rot="R180"/>
<rectangle x1="1.195" y1="2.895" x2="1.475" y2="2.905" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="2.905" x2="8.895" y2="2.915" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.905" x2="8.365" y2="2.915" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.905" x2="6.945" y2="2.915" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.905" x2="6.465" y2="2.915" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.905" x2="4.995" y2="2.915" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="2.905" x2="3.385" y2="2.915" layer="201" rot="R180"/>
<rectangle x1="1.195" y1="2.905" x2="1.465" y2="2.915" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="2.915" x2="8.895" y2="2.925" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.915" x2="8.365" y2="2.925" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.915" x2="6.945" y2="2.925" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.915" x2="6.465" y2="2.925" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.915" x2="4.995" y2="2.925" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="2.915" x2="3.385" y2="2.925" layer="201" rot="R180"/>
<rectangle x1="1.205" y1="2.915" x2="1.465" y2="2.925" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="2.925" x2="8.895" y2="2.935" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.925" x2="8.365" y2="2.935" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.925" x2="6.945" y2="2.935" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.925" x2="6.465" y2="2.935" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.925" x2="4.995" y2="2.935" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="2.925" x2="3.385" y2="2.935" layer="201" rot="R180"/>
<rectangle x1="1.205" y1="2.925" x2="1.465" y2="2.935" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="2.935" x2="8.895" y2="2.945" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.935" x2="8.365" y2="2.945" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.935" x2="6.945" y2="2.945" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.935" x2="6.465" y2="2.945" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.935" x2="4.995" y2="2.945" layer="201" rot="R180"/>
<rectangle x1="2.885" y1="2.935" x2="3.385" y2="2.945" layer="201" rot="R180"/>
<rectangle x1="1.205" y1="2.935" x2="1.455" y2="2.945" layer="201" rot="R180"/>
<rectangle x1="8.475" y1="2.945" x2="8.895" y2="2.955" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.945" x2="8.355" y2="2.955" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.945" x2="6.945" y2="2.955" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.945" x2="6.465" y2="2.955" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.945" x2="4.995" y2="2.955" layer="201" rot="R180"/>
<rectangle x1="2.885" y1="2.945" x2="3.385" y2="2.955" layer="201" rot="R180"/>
<rectangle x1="1.205" y1="2.945" x2="1.455" y2="2.955" layer="201" rot="R180"/>
<rectangle x1="8.485" y1="2.955" x2="8.895" y2="2.965" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.955" x2="8.355" y2="2.965" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.955" x2="6.945" y2="2.965" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.955" x2="6.465" y2="2.965" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.955" x2="4.995" y2="2.965" layer="201" rot="R180"/>
<rectangle x1="2.885" y1="2.955" x2="3.385" y2="2.965" layer="201" rot="R180"/>
<rectangle x1="1.205" y1="2.955" x2="1.455" y2="2.965" layer="201" rot="R180"/>
<rectangle x1="8.485" y1="2.965" x2="8.895" y2="2.975" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.965" x2="8.355" y2="2.975" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.965" x2="6.945" y2="2.975" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.965" x2="6.465" y2="2.975" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.965" x2="4.995" y2="2.975" layer="201" rot="R180"/>
<rectangle x1="2.875" y1="2.965" x2="3.385" y2="2.975" layer="201" rot="R180"/>
<rectangle x1="1.205" y1="2.965" x2="1.465" y2="2.975" layer="201" rot="R180"/>
<rectangle x1="8.485" y1="2.975" x2="8.895" y2="2.985" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.975" x2="8.355" y2="2.985" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.975" x2="6.945" y2="2.985" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.975" x2="6.465" y2="2.985" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.975" x2="4.995" y2="2.985" layer="201" rot="R180"/>
<rectangle x1="2.875" y1="2.975" x2="3.385" y2="2.985" layer="201" rot="R180"/>
<rectangle x1="1.195" y1="2.975" x2="1.465" y2="2.985" layer="201" rot="R180"/>
<rectangle x1="8.485" y1="2.985" x2="8.895" y2="2.995" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.985" x2="8.355" y2="2.995" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.985" x2="6.945" y2="2.995" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.985" x2="6.465" y2="2.995" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.985" x2="4.995" y2="2.995" layer="201" rot="R180"/>
<rectangle x1="2.865" y1="2.985" x2="3.385" y2="2.995" layer="201" rot="R180"/>
<rectangle x1="1.195" y1="2.985" x2="1.465" y2="2.995" layer="201" rot="R180"/>
<rectangle x1="8.485" y1="2.995" x2="8.895" y2="3.005" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="2.995" x2="8.345" y2="3.005" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="2.995" x2="6.945" y2="3.005" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="2.995" x2="6.465" y2="3.005" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="2.995" x2="4.995" y2="3.005" layer="201" rot="R180"/>
<rectangle x1="2.865" y1="2.995" x2="3.385" y2="3.005" layer="201" rot="R180"/>
<rectangle x1="1.195" y1="2.995" x2="1.475" y2="3.005" layer="201" rot="R180"/>
<rectangle x1="8.495" y1="3.005" x2="8.895" y2="3.015" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.005" x2="8.345" y2="3.015" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.005" x2="6.945" y2="3.015" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.005" x2="6.465" y2="3.015" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.005" x2="4.995" y2="3.015" layer="201" rot="R180"/>
<rectangle x1="2.855" y1="3.005" x2="3.385" y2="3.015" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="3.005" x2="1.475" y2="3.015" layer="201" rot="R180"/>
<rectangle x1="8.495" y1="3.015" x2="8.895" y2="3.025" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.015" x2="8.345" y2="3.025" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.015" x2="6.945" y2="3.025" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.015" x2="6.465" y2="3.025" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.015" x2="4.995" y2="3.025" layer="201" rot="R180"/>
<rectangle x1="2.855" y1="3.015" x2="3.385" y2="3.025" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="3.015" x2="1.475" y2="3.025" layer="201" rot="R180"/>
<rectangle x1="8.495" y1="3.025" x2="8.895" y2="3.035" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.025" x2="8.345" y2="3.035" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.025" x2="6.945" y2="3.035" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.025" x2="6.465" y2="3.035" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.025" x2="4.995" y2="3.035" layer="201" rot="R180"/>
<rectangle x1="2.855" y1="3.025" x2="3.385" y2="3.035" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="3.025" x2="1.475" y2="3.035" layer="201" rot="R180"/>
<rectangle x1="8.495" y1="3.035" x2="8.895" y2="3.045" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.035" x2="8.345" y2="3.045" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.035" x2="6.945" y2="3.045" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.035" x2="6.465" y2="3.045" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.035" x2="4.995" y2="3.045" layer="201" rot="R180"/>
<rectangle x1="2.845" y1="3.035" x2="3.385" y2="3.045" layer="201" rot="R180"/>
<rectangle x1="1.185" y1="3.035" x2="1.485" y2="3.045" layer="201" rot="R180"/>
<rectangle x1="8.505" y1="3.045" x2="8.895" y2="3.055" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.045" x2="8.335" y2="3.055" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.045" x2="6.945" y2="3.055" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.045" x2="6.465" y2="3.055" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.045" x2="4.995" y2="3.055" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.045" x2="3.385" y2="3.055" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="3.045" x2="1.485" y2="3.055" layer="201" rot="R180"/>
<rectangle x1="8.505" y1="3.055" x2="8.895" y2="3.065" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.055" x2="8.335" y2="3.065" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.055" x2="6.945" y2="3.065" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.055" x2="6.465" y2="3.065" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.055" x2="4.995" y2="3.065" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.055" x2="3.385" y2="3.065" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="3.055" x2="1.485" y2="3.065" layer="201" rot="R180"/>
<rectangle x1="8.505" y1="3.065" x2="8.895" y2="3.075" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.065" x2="8.335" y2="3.075" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.065" x2="6.945" y2="3.075" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.065" x2="6.465" y2="3.075" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.065" x2="4.995" y2="3.075" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.065" x2="3.385" y2="3.075" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="3.065" x2="1.495" y2="3.075" layer="201" rot="R180"/>
<rectangle x1="8.505" y1="3.075" x2="8.895" y2="3.085" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.075" x2="8.335" y2="3.085" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.075" x2="6.945" y2="3.085" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.075" x2="6.465" y2="3.085" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.075" x2="4.995" y2="3.085" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.075" x2="3.385" y2="3.085" layer="201" rot="R180"/>
<rectangle x1="1.175" y1="3.075" x2="1.495" y2="3.085" layer="201" rot="R180"/>
<rectangle x1="8.505" y1="3.085" x2="8.895" y2="3.095" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.085" x2="8.335" y2="3.095" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.085" x2="6.945" y2="3.095" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.085" x2="6.465" y2="3.095" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.085" x2="4.995" y2="3.095" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.085" x2="3.385" y2="3.095" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="3.085" x2="1.495" y2="3.095" layer="201" rot="R180"/>
<rectangle x1="8.515" y1="3.095" x2="8.895" y2="3.105" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.095" x2="8.325" y2="3.105" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.095" x2="6.945" y2="3.105" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.095" x2="6.465" y2="3.105" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.095" x2="4.995" y2="3.105" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.095" x2="3.385" y2="3.105" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="3.095" x2="1.495" y2="3.105" layer="201" rot="R180"/>
<rectangle x1="8.515" y1="3.105" x2="8.895" y2="3.115" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.105" x2="8.325" y2="3.115" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.105" x2="6.945" y2="3.115" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.105" x2="6.465" y2="3.115" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.105" x2="4.995" y2="3.115" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.105" x2="3.385" y2="3.115" layer="201" rot="R180"/>
<rectangle x1="1.165" y1="3.105" x2="1.505" y2="3.115" layer="201" rot="R180"/>
<rectangle x1="8.515" y1="3.115" x2="8.895" y2="3.125" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.115" x2="8.325" y2="3.125" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.115" x2="6.945" y2="3.125" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.115" x2="6.465" y2="3.125" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.115" x2="4.995" y2="3.125" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.115" x2="3.385" y2="3.125" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="3.115" x2="1.505" y2="3.125" layer="201" rot="R180"/>
<rectangle x1="8.515" y1="3.125" x2="8.895" y2="3.135" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.125" x2="8.325" y2="3.135" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.125" x2="6.945" y2="3.135" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.125" x2="6.465" y2="3.135" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.125" x2="4.995" y2="3.135" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.125" x2="3.385" y2="3.135" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="3.125" x2="1.505" y2="3.135" layer="201" rot="R180"/>
<rectangle x1="8.515" y1="3.135" x2="8.895" y2="3.145" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.135" x2="8.325" y2="3.145" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.135" x2="6.945" y2="3.145" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.135" x2="6.465" y2="3.145" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.135" x2="4.995" y2="3.145" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.135" x2="3.385" y2="3.145" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="3.135" x2="1.515" y2="3.145" layer="201" rot="R180"/>
<rectangle x1="8.525" y1="3.145" x2="8.895" y2="3.155" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.145" x2="8.315" y2="3.155" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.145" x2="6.945" y2="3.155" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.145" x2="6.465" y2="3.155" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.145" x2="4.995" y2="3.155" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.145" x2="3.385" y2="3.155" layer="201" rot="R180"/>
<rectangle x1="1.155" y1="3.145" x2="1.515" y2="3.155" layer="201" rot="R180"/>
<rectangle x1="8.525" y1="3.155" x2="8.895" y2="3.165" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.155" x2="8.315" y2="3.165" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.155" x2="6.945" y2="3.165" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.155" x2="6.465" y2="3.165" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.155" x2="4.995" y2="3.165" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.155" x2="3.385" y2="3.165" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="3.155" x2="1.515" y2="3.165" layer="201" rot="R180"/>
<rectangle x1="8.525" y1="3.165" x2="8.895" y2="3.175" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.165" x2="8.315" y2="3.175" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.165" x2="6.945" y2="3.175" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.165" x2="6.465" y2="3.175" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.165" x2="4.995" y2="3.175" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.165" x2="3.385" y2="3.175" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="3.165" x2="1.515" y2="3.175" layer="201" rot="R180"/>
<rectangle x1="8.525" y1="3.175" x2="8.895" y2="3.185" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.175" x2="8.315" y2="3.185" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.175" x2="6.945" y2="3.185" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.175" x2="6.465" y2="3.185" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.175" x2="4.995" y2="3.185" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.175" x2="3.385" y2="3.185" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="3.175" x2="1.525" y2="3.185" layer="201" rot="R180"/>
<rectangle x1="8.525" y1="3.185" x2="8.895" y2="3.195" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.185" x2="8.315" y2="3.195" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.185" x2="6.945" y2="3.195" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.185" x2="6.465" y2="3.195" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.185" x2="4.995" y2="3.195" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.185" x2="3.385" y2="3.195" layer="201" rot="R180"/>
<rectangle x1="1.145" y1="3.185" x2="1.525" y2="3.195" layer="201" rot="R180"/>
<rectangle x1="8.535" y1="3.195" x2="8.895" y2="3.205" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.195" x2="8.315" y2="3.205" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.195" x2="6.945" y2="3.205" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.195" x2="6.465" y2="3.205" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.195" x2="4.995" y2="3.205" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.195" x2="3.385" y2="3.205" layer="201" rot="R180"/>
<rectangle x1="1.135" y1="3.195" x2="1.525" y2="3.205" layer="201" rot="R180"/>
<rectangle x1="8.535" y1="3.205" x2="8.895" y2="3.215" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.205" x2="8.305" y2="3.215" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.205" x2="6.945" y2="3.215" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.205" x2="6.465" y2="3.215" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.205" x2="4.995" y2="3.215" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.205" x2="3.385" y2="3.215" layer="201" rot="R180"/>
<rectangle x1="1.135" y1="3.205" x2="1.535" y2="3.215" layer="201" rot="R180"/>
<rectangle x1="8.535" y1="3.215" x2="8.895" y2="3.225" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.215" x2="8.305" y2="3.225" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.215" x2="6.945" y2="3.225" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.215" x2="6.465" y2="3.225" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.215" x2="4.995" y2="3.225" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.215" x2="3.385" y2="3.225" layer="201" rot="R180"/>
<rectangle x1="1.135" y1="3.215" x2="1.535" y2="3.225" layer="201" rot="R180"/>
<rectangle x1="8.535" y1="3.225" x2="8.895" y2="3.235" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.225" x2="8.305" y2="3.235" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.225" x2="6.945" y2="3.235" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.225" x2="6.465" y2="3.235" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.225" x2="4.995" y2="3.235" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.225" x2="3.385" y2="3.235" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="3.225" x2="1.535" y2="3.235" layer="201" rot="R180"/>
<rectangle x1="8.535" y1="3.235" x2="8.895" y2="3.245" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.235" x2="8.305" y2="3.245" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.235" x2="6.945" y2="3.245" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.235" x2="6.465" y2="3.245" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.235" x2="4.995" y2="3.245" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.235" x2="3.385" y2="3.245" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="3.235" x2="1.535" y2="3.245" layer="201" rot="R180"/>
<rectangle x1="8.545" y1="3.245" x2="8.895" y2="3.255" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.245" x2="8.305" y2="3.255" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.245" x2="6.945" y2="3.255" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.245" x2="6.465" y2="3.255" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.245" x2="4.995" y2="3.255" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.245" x2="3.385" y2="3.255" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="3.245" x2="1.545" y2="3.255" layer="201" rot="R180"/>
<rectangle x1="8.545" y1="3.255" x2="8.895" y2="3.265" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.255" x2="8.295" y2="3.265" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.255" x2="6.945" y2="3.265" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.255" x2="6.465" y2="3.265" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.255" x2="4.995" y2="3.265" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.255" x2="3.385" y2="3.265" layer="201" rot="R180"/>
<rectangle x1="1.125" y1="3.255" x2="1.545" y2="3.265" layer="201" rot="R180"/>
<rectangle x1="8.545" y1="3.265" x2="8.895" y2="3.275" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.265" x2="8.295" y2="3.275" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.265" x2="6.945" y2="3.275" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.265" x2="6.465" y2="3.275" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.265" x2="4.995" y2="3.275" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.265" x2="3.385" y2="3.275" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="3.265" x2="1.545" y2="3.275" layer="201" rot="R180"/>
<rectangle x1="8.545" y1="3.275" x2="8.895" y2="3.285" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.275" x2="8.295" y2="3.285" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.275" x2="6.945" y2="3.285" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.275" x2="6.465" y2="3.285" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.275" x2="4.995" y2="3.285" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.275" x2="3.385" y2="3.285" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="3.275" x2="1.555" y2="3.285" layer="201" rot="R180"/>
<rectangle x1="8.545" y1="3.285" x2="8.895" y2="3.295" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.285" x2="8.295" y2="3.295" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.285" x2="6.945" y2="3.295" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.285" x2="6.465" y2="3.295" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.285" x2="4.995" y2="3.295" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.285" x2="3.385" y2="3.295" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="3.285" x2="1.555" y2="3.295" layer="201" rot="R180"/>
<rectangle x1="8.555" y1="3.295" x2="8.895" y2="3.305" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.295" x2="8.295" y2="3.305" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.295" x2="6.945" y2="3.305" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.295" x2="6.465" y2="3.305" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.295" x2="4.995" y2="3.305" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.295" x2="3.385" y2="3.305" layer="201" rot="R180"/>
<rectangle x1="1.115" y1="3.295" x2="1.555" y2="3.305" layer="201" rot="R180"/>
<rectangle x1="8.555" y1="3.305" x2="8.895" y2="3.315" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.305" x2="8.285" y2="3.315" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.305" x2="6.945" y2="3.315" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.305" x2="6.465" y2="3.315" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.305" x2="4.995" y2="3.315" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.305" x2="3.385" y2="3.315" layer="201" rot="R180"/>
<rectangle x1="1.105" y1="3.305" x2="1.555" y2="3.315" layer="201" rot="R180"/>
<rectangle x1="8.555" y1="3.315" x2="8.895" y2="3.325" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.315" x2="8.285" y2="3.325" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.315" x2="6.945" y2="3.325" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.315" x2="6.465" y2="3.325" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.315" x2="4.995" y2="3.325" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.315" x2="3.385" y2="3.325" layer="201" rot="R180"/>
<rectangle x1="1.335" y1="3.315" x2="1.565" y2="3.325" layer="201" rot="R180"/>
<rectangle x1="1.105" y1="3.315" x2="1.325" y2="3.325" layer="201" rot="R180"/>
<rectangle x1="8.555" y1="3.325" x2="8.895" y2="3.335" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.325" x2="8.285" y2="3.335" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.325" x2="6.945" y2="3.335" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.325" x2="6.465" y2="3.335" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.325" x2="4.995" y2="3.335" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.325" x2="3.385" y2="3.335" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="3.325" x2="1.565" y2="3.335" layer="201" rot="R180"/>
<rectangle x1="1.105" y1="3.325" x2="1.325" y2="3.335" layer="201" rot="R180"/>
<rectangle x1="8.565" y1="3.335" x2="8.895" y2="3.345" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.335" x2="8.285" y2="3.345" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.335" x2="6.945" y2="3.345" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.335" x2="6.465" y2="3.345" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.335" x2="4.995" y2="3.345" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.335" x2="3.385" y2="3.345" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="3.335" x2="1.565" y2="3.345" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="3.335" x2="1.325" y2="3.345" layer="201" rot="R180"/>
<rectangle x1="8.565" y1="3.345" x2="8.895" y2="3.355" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.345" x2="8.285" y2="3.355" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.345" x2="6.945" y2="3.355" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.345" x2="6.465" y2="3.355" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.345" x2="4.995" y2="3.355" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.345" x2="3.385" y2="3.355" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="3.345" x2="1.575" y2="3.355" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="3.345" x2="1.325" y2="3.355" layer="201" rot="R180"/>
<rectangle x1="8.565" y1="3.355" x2="8.895" y2="3.365" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.355" x2="8.275" y2="3.365" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.355" x2="6.945" y2="3.365" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.355" x2="6.465" y2="3.365" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.355" x2="4.995" y2="3.365" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.355" x2="3.385" y2="3.365" layer="201" rot="R180"/>
<rectangle x1="1.345" y1="3.355" x2="1.575" y2="3.365" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="3.355" x2="1.315" y2="3.365" layer="201" rot="R180"/>
<rectangle x1="8.565" y1="3.365" x2="8.895" y2="3.375" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.365" x2="8.275" y2="3.375" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.365" x2="6.945" y2="3.375" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.365" x2="6.465" y2="3.375" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.365" x2="4.995" y2="3.375" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.365" x2="3.385" y2="3.375" layer="201" rot="R180"/>
<rectangle x1="1.355" y1="3.365" x2="1.575" y2="3.375" layer="201" rot="R180"/>
<rectangle x1="1.095" y1="3.365" x2="1.315" y2="3.375" layer="201" rot="R180"/>
<rectangle x1="8.565" y1="3.375" x2="8.895" y2="3.385" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.375" x2="8.275" y2="3.385" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.375" x2="6.945" y2="3.385" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.375" x2="6.465" y2="3.385" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.375" x2="4.995" y2="3.385" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.375" x2="3.385" y2="3.385" layer="201" rot="R180"/>
<rectangle x1="1.355" y1="3.375" x2="1.575" y2="3.385" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="3.375" x2="1.315" y2="3.385" layer="201" rot="R180"/>
<rectangle x1="8.575" y1="3.385" x2="8.895" y2="3.395" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.385" x2="8.275" y2="3.395" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.385" x2="6.945" y2="3.395" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.385" x2="6.465" y2="3.395" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.385" x2="4.995" y2="3.395" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.385" x2="3.385" y2="3.395" layer="201" rot="R180"/>
<rectangle x1="1.355" y1="3.385" x2="1.585" y2="3.395" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="3.385" x2="1.315" y2="3.395" layer="201" rot="R180"/>
<rectangle x1="8.575" y1="3.395" x2="8.895" y2="3.405" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.395" x2="8.275" y2="3.405" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.395" x2="6.945" y2="3.405" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.395" x2="6.465" y2="3.405" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.395" x2="4.995" y2="3.405" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.395" x2="3.385" y2="3.405" layer="201" rot="R180"/>
<rectangle x1="1.355" y1="3.395" x2="1.585" y2="3.405" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="3.395" x2="1.305" y2="3.405" layer="201" rot="R180"/>
<rectangle x1="8.575" y1="3.405" x2="8.895" y2="3.415" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.405" x2="8.265" y2="3.415" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.405" x2="6.945" y2="3.415" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.405" x2="6.465" y2="3.415" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.405" x2="4.995" y2="3.415" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.405" x2="3.385" y2="3.415" layer="201" rot="R180"/>
<rectangle x1="1.365" y1="3.405" x2="1.585" y2="3.415" layer="201" rot="R180"/>
<rectangle x1="1.085" y1="3.405" x2="1.305" y2="3.415" layer="201" rot="R180"/>
<rectangle x1="8.575" y1="3.415" x2="8.895" y2="3.425" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.415" x2="8.265" y2="3.425" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.415" x2="6.945" y2="3.425" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.415" x2="6.465" y2="3.425" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.415" x2="4.995" y2="3.425" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.415" x2="3.385" y2="3.425" layer="201" rot="R180"/>
<rectangle x1="1.365" y1="3.415" x2="1.595" y2="3.425" layer="201" rot="R180"/>
<rectangle x1="1.075" y1="3.415" x2="1.305" y2="3.425" layer="201" rot="R180"/>
<rectangle x1="8.575" y1="3.425" x2="8.895" y2="3.435" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.425" x2="8.265" y2="3.435" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.425" x2="6.945" y2="3.435" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.425" x2="6.465" y2="3.435" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.425" x2="4.995" y2="3.435" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.425" x2="3.385" y2="3.435" layer="201" rot="R180"/>
<rectangle x1="1.365" y1="3.425" x2="1.595" y2="3.435" layer="201" rot="R180"/>
<rectangle x1="1.075" y1="3.425" x2="1.305" y2="3.435" layer="201" rot="R180"/>
<rectangle x1="8.585" y1="3.435" x2="8.895" y2="3.445" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.435" x2="8.265" y2="3.445" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.435" x2="6.945" y2="3.445" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.435" x2="6.465" y2="3.445" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.435" x2="4.995" y2="3.445" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.435" x2="3.385" y2="3.445" layer="201" rot="R180"/>
<rectangle x1="1.375" y1="3.435" x2="1.595" y2="3.445" layer="201" rot="R180"/>
<rectangle x1="1.075" y1="3.435" x2="1.295" y2="3.445" layer="201" rot="R180"/>
<rectangle x1="8.585" y1="3.445" x2="8.895" y2="3.455" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.445" x2="8.265" y2="3.455" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.445" x2="6.945" y2="3.455" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.445" x2="6.465" y2="3.455" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.445" x2="4.995" y2="3.455" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.445" x2="3.385" y2="3.455" layer="201" rot="R180"/>
<rectangle x1="1.375" y1="3.445" x2="1.595" y2="3.455" layer="201" rot="R180"/>
<rectangle x1="1.065" y1="3.445" x2="1.295" y2="3.455" layer="201" rot="R180"/>
<rectangle x1="8.585" y1="3.455" x2="8.895" y2="3.465" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.455" x2="8.255" y2="3.465" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.455" x2="6.945" y2="3.465" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.455" x2="6.465" y2="3.465" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.455" x2="4.995" y2="3.465" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.455" x2="3.385" y2="3.465" layer="201" rot="R180"/>
<rectangle x1="1.375" y1="3.455" x2="1.605" y2="3.465" layer="201" rot="R180"/>
<rectangle x1="1.065" y1="3.455" x2="1.295" y2="3.465" layer="201" rot="R180"/>
<rectangle x1="8.585" y1="3.465" x2="8.895" y2="3.475" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.465" x2="8.255" y2="3.475" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.465" x2="6.945" y2="3.475" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.465" x2="6.465" y2="3.475" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.465" x2="4.995" y2="3.475" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.465" x2="3.385" y2="3.475" layer="201" rot="R180"/>
<rectangle x1="1.375" y1="3.465" x2="1.605" y2="3.475" layer="201" rot="R180"/>
<rectangle x1="1.065" y1="3.465" x2="1.295" y2="3.475" layer="201" rot="R180"/>
<rectangle x1="8.585" y1="3.475" x2="8.895" y2="3.485" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.475" x2="8.255" y2="3.485" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.475" x2="6.945" y2="3.485" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.475" x2="6.465" y2="3.485" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.475" x2="4.995" y2="3.485" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.475" x2="3.385" y2="3.485" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="3.475" x2="1.605" y2="3.485" layer="201" rot="R180"/>
<rectangle x1="1.065" y1="3.475" x2="1.285" y2="3.485" layer="201" rot="R180"/>
<rectangle x1="8.595" y1="3.485" x2="8.895" y2="3.495" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.485" x2="8.255" y2="3.495" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.485" x2="6.945" y2="3.495" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.485" x2="6.465" y2="3.495" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.485" x2="4.995" y2="3.495" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.485" x2="3.385" y2="3.495" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="3.485" x2="1.615" y2="3.495" layer="201" rot="R180"/>
<rectangle x1="1.055" y1="3.485" x2="1.285" y2="3.495" layer="201" rot="R180"/>
<rectangle x1="8.595" y1="3.495" x2="8.895" y2="3.505" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.495" x2="8.255" y2="3.505" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.495" x2="6.945" y2="3.505" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.495" x2="6.465" y2="3.505" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.495" x2="4.995" y2="3.505" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.495" x2="3.385" y2="3.505" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="3.495" x2="1.615" y2="3.505" layer="201" rot="R180"/>
<rectangle x1="1.055" y1="3.495" x2="1.285" y2="3.505" layer="201" rot="R180"/>
<rectangle x1="8.595" y1="3.505" x2="8.895" y2="3.515" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.505" x2="8.245" y2="3.515" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.505" x2="6.945" y2="3.515" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.505" x2="6.465" y2="3.515" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.505" x2="4.995" y2="3.515" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.505" x2="3.385" y2="3.515" layer="201" rot="R180"/>
<rectangle x1="1.385" y1="3.505" x2="1.615" y2="3.515" layer="201" rot="R180"/>
<rectangle x1="1.055" y1="3.505" x2="1.285" y2="3.515" layer="201" rot="R180"/>
<rectangle x1="8.595" y1="3.515" x2="8.895" y2="3.525" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.515" x2="8.245" y2="3.525" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.515" x2="6.945" y2="3.525" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.515" x2="6.465" y2="3.525" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.515" x2="4.995" y2="3.525" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.515" x2="3.385" y2="3.525" layer="201" rot="R180"/>
<rectangle x1="1.395" y1="3.515" x2="1.615" y2="3.525" layer="201" rot="R180"/>
<rectangle x1="1.055" y1="3.515" x2="1.275" y2="3.525" layer="201" rot="R180"/>
<rectangle x1="8.595" y1="3.525" x2="8.895" y2="3.535" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.525" x2="8.245" y2="3.535" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.525" x2="6.945" y2="3.535" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.525" x2="6.465" y2="3.535" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.525" x2="4.995" y2="3.535" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.525" x2="3.385" y2="3.535" layer="201" rot="R180"/>
<rectangle x1="1.395" y1="3.525" x2="1.625" y2="3.535" layer="201" rot="R180"/>
<rectangle x1="1.045" y1="3.525" x2="1.275" y2="3.535" layer="201" rot="R180"/>
<rectangle x1="8.605" y1="3.535" x2="8.895" y2="3.545" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.535" x2="8.245" y2="3.545" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.535" x2="6.945" y2="3.545" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.535" x2="6.465" y2="3.545" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.535" x2="4.995" y2="3.545" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.535" x2="3.385" y2="3.545" layer="201" rot="R180"/>
<rectangle x1="1.395" y1="3.535" x2="1.625" y2="3.545" layer="201" rot="R180"/>
<rectangle x1="1.045" y1="3.535" x2="1.275" y2="3.545" layer="201" rot="R180"/>
<rectangle x1="8.605" y1="3.545" x2="8.895" y2="3.555" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.545" x2="8.245" y2="3.555" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.545" x2="6.945" y2="3.555" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.545" x2="6.465" y2="3.555" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.545" x2="4.995" y2="3.555" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.545" x2="3.385" y2="3.555" layer="201" rot="R180"/>
<rectangle x1="1.405" y1="3.545" x2="1.625" y2="3.555" layer="201" rot="R180"/>
<rectangle x1="1.045" y1="3.545" x2="1.275" y2="3.555" layer="201" rot="R180"/>
<rectangle x1="8.605" y1="3.555" x2="8.895" y2="3.565" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.555" x2="8.235" y2="3.565" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.555" x2="6.945" y2="3.565" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.555" x2="6.465" y2="3.565" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.555" x2="4.995" y2="3.565" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.555" x2="3.385" y2="3.565" layer="201" rot="R180"/>
<rectangle x1="1.405" y1="3.555" x2="1.635" y2="3.565" layer="201" rot="R180"/>
<rectangle x1="1.035" y1="3.555" x2="1.265" y2="3.565" layer="201" rot="R180"/>
<rectangle x1="8.605" y1="3.565" x2="8.895" y2="3.575" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.565" x2="8.235" y2="3.575" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.565" x2="6.945" y2="3.575" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.565" x2="6.465" y2="3.575" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.565" x2="4.995" y2="3.575" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.565" x2="3.385" y2="3.575" layer="201" rot="R180"/>
<rectangle x1="1.405" y1="3.565" x2="1.635" y2="3.575" layer="201" rot="R180"/>
<rectangle x1="1.035" y1="3.565" x2="1.265" y2="3.575" layer="201" rot="R180"/>
<rectangle x1="8.605" y1="3.575" x2="8.895" y2="3.585" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.575" x2="8.235" y2="3.585" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.575" x2="6.945" y2="3.585" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.575" x2="6.465" y2="3.585" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.575" x2="4.995" y2="3.585" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.575" x2="3.385" y2="3.585" layer="201" rot="R180"/>
<rectangle x1="1.405" y1="3.575" x2="1.635" y2="3.585" layer="201" rot="R180"/>
<rectangle x1="1.035" y1="3.575" x2="1.265" y2="3.585" layer="201" rot="R180"/>
<rectangle x1="8.615" y1="3.585" x2="8.895" y2="3.595" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.585" x2="8.235" y2="3.595" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.585" x2="6.945" y2="3.595" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.585" x2="6.465" y2="3.595" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.585" x2="4.995" y2="3.595" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.585" x2="3.385" y2="3.595" layer="201" rot="R180"/>
<rectangle x1="1.415" y1="3.585" x2="1.635" y2="3.595" layer="201" rot="R180"/>
<rectangle x1="1.035" y1="3.585" x2="1.255" y2="3.595" layer="201" rot="R180"/>
<rectangle x1="8.615" y1="3.595" x2="8.895" y2="3.605" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.595" x2="8.235" y2="3.605" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.595" x2="6.945" y2="3.605" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.595" x2="6.465" y2="3.605" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.595" x2="4.995" y2="3.605" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.595" x2="3.385" y2="3.605" layer="201" rot="R180"/>
<rectangle x1="1.415" y1="3.595" x2="1.645" y2="3.605" layer="201" rot="R180"/>
<rectangle x1="1.025" y1="3.595" x2="1.255" y2="3.605" layer="201" rot="R180"/>
<rectangle x1="8.615" y1="3.605" x2="8.895" y2="3.615" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.605" x2="8.225" y2="3.615" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.605" x2="6.945" y2="3.615" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.605" x2="6.465" y2="3.615" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.605" x2="4.995" y2="3.615" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.605" x2="3.385" y2="3.615" layer="201" rot="R180"/>
<rectangle x1="1.415" y1="3.605" x2="1.645" y2="3.615" layer="201" rot="R180"/>
<rectangle x1="1.025" y1="3.605" x2="1.255" y2="3.615" layer="201" rot="R180"/>
<rectangle x1="8.615" y1="3.615" x2="8.895" y2="3.625" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.615" x2="8.225" y2="3.625" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.615" x2="6.945" y2="3.625" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.615" x2="6.465" y2="3.625" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.615" x2="4.995" y2="3.625" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.615" x2="3.385" y2="3.625" layer="201" rot="R180"/>
<rectangle x1="1.415" y1="3.615" x2="1.645" y2="3.625" layer="201" rot="R180"/>
<rectangle x1="1.025" y1="3.615" x2="1.255" y2="3.625" layer="201" rot="R180"/>
<rectangle x1="8.615" y1="3.625" x2="8.895" y2="3.635" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.625" x2="8.225" y2="3.635" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.625" x2="6.945" y2="3.635" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.625" x2="6.465" y2="3.635" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.625" x2="4.995" y2="3.635" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.625" x2="3.385" y2="3.635" layer="201" rot="R180"/>
<rectangle x1="1.425" y1="3.625" x2="1.655" y2="3.635" layer="201" rot="R180"/>
<rectangle x1="1.025" y1="3.625" x2="1.245" y2="3.635" layer="201" rot="R180"/>
<rectangle x1="8.625" y1="3.635" x2="8.895" y2="3.645" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.635" x2="8.225" y2="3.645" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.635" x2="6.945" y2="3.645" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.635" x2="6.465" y2="3.645" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.635" x2="4.995" y2="3.645" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.635" x2="3.385" y2="3.645" layer="201" rot="R180"/>
<rectangle x1="1.425" y1="3.635" x2="1.655" y2="3.645" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="3.635" x2="1.245" y2="3.645" layer="201" rot="R180"/>
<rectangle x1="8.625" y1="3.645" x2="8.895" y2="3.655" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.645" x2="8.225" y2="3.655" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.645" x2="6.945" y2="3.655" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.645" x2="6.465" y2="3.655" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.645" x2="4.995" y2="3.655" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.645" x2="3.385" y2="3.655" layer="201" rot="R180"/>
<rectangle x1="1.425" y1="3.645" x2="1.655" y2="3.655" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="3.645" x2="1.245" y2="3.655" layer="201" rot="R180"/>
<rectangle x1="8.625" y1="3.655" x2="8.895" y2="3.665" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.655" x2="8.215" y2="3.665" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.655" x2="6.945" y2="3.665" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.655" x2="6.465" y2="3.665" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.655" x2="4.995" y2="3.665" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.655" x2="3.385" y2="3.665" layer="201" rot="R180"/>
<rectangle x1="1.435" y1="3.655" x2="1.655" y2="3.665" layer="201" rot="R180"/>
<rectangle x1="1.015" y1="3.655" x2="1.245" y2="3.665" layer="201" rot="R180"/>
<rectangle x1="8.625" y1="3.665" x2="8.895" y2="3.675" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.665" x2="8.215" y2="3.675" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.665" x2="6.945" y2="3.675" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.665" x2="6.465" y2="3.675" layer="201" rot="R180"/>
<rectangle x1="4.785" y1="3.665" x2="4.995" y2="3.675" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.665" x2="3.385" y2="3.675" layer="201" rot="R180"/>
<rectangle x1="1.435" y1="3.665" x2="1.665" y2="3.675" layer="201" rot="R180"/>
<rectangle x1="1.005" y1="3.665" x2="1.235" y2="3.675" layer="201" rot="R180"/>
<rectangle x1="8.635" y1="3.675" x2="8.895" y2="3.685" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.675" x2="8.215" y2="3.685" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.675" x2="6.945" y2="3.685" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.675" x2="6.465" y2="3.685" layer="201" rot="R180"/>
<rectangle x1="4.775" y1="3.675" x2="4.995" y2="3.685" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.675" x2="3.385" y2="3.685" layer="201" rot="R180"/>
<rectangle x1="1.435" y1="3.675" x2="1.665" y2="3.685" layer="201" rot="R180"/>
<rectangle x1="1.005" y1="3.675" x2="1.235" y2="3.685" layer="201" rot="R180"/>
<rectangle x1="8.635" y1="3.685" x2="8.895" y2="3.695" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.685" x2="8.215" y2="3.695" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.685" x2="6.945" y2="3.695" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.685" x2="6.465" y2="3.695" layer="201" rot="R180"/>
<rectangle x1="4.775" y1="3.685" x2="4.995" y2="3.695" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="3.685" x2="3.385" y2="3.695" layer="201" rot="R180"/>
<rectangle x1="1.435" y1="3.685" x2="1.665" y2="3.695" layer="201" rot="R180"/>
<rectangle x1="1.005" y1="3.685" x2="1.235" y2="3.695" layer="201" rot="R180"/>
<rectangle x1="8.635" y1="3.695" x2="8.895" y2="3.705" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.695" x2="8.215" y2="3.705" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.695" x2="6.945" y2="3.705" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.695" x2="6.465" y2="3.705" layer="201" rot="R180"/>
<rectangle x1="4.765" y1="3.695" x2="4.995" y2="3.705" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="3.695" x2="3.385" y2="3.705" layer="201" rot="R180"/>
<rectangle x1="1.445" y1="3.695" x2="1.675" y2="3.705" layer="201" rot="R180"/>
<rectangle x1="1.005" y1="3.695" x2="1.235" y2="3.705" layer="201" rot="R180"/>
<rectangle x1="8.635" y1="3.705" x2="8.895" y2="3.715" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.705" x2="8.215" y2="3.715" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.705" x2="6.945" y2="3.715" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.705" x2="6.465" y2="3.715" layer="201" rot="R180"/>
<rectangle x1="4.755" y1="3.705" x2="4.995" y2="3.715" layer="201" rot="R180"/>
<rectangle x1="3.145" y1="3.705" x2="3.385" y2="3.715" layer="201" rot="R180"/>
<rectangle x1="1.445" y1="3.705" x2="1.675" y2="3.715" layer="201" rot="R180"/>
<rectangle x1="0.995" y1="3.705" x2="1.225" y2="3.715" layer="201" rot="R180"/>
<rectangle x1="8.635" y1="3.715" x2="8.895" y2="3.725" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.715" x2="8.205" y2="3.725" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.715" x2="6.945" y2="3.725" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.715" x2="6.465" y2="3.725" layer="201" rot="R180"/>
<rectangle x1="4.745" y1="3.715" x2="4.995" y2="3.725" layer="201" rot="R180"/>
<rectangle x1="2.935" y1="3.715" x2="3.385" y2="3.725" layer="201" rot="R180"/>
<rectangle x1="1.445" y1="3.715" x2="1.675" y2="3.725" layer="201" rot="R180"/>
<rectangle x1="0.995" y1="3.715" x2="1.225" y2="3.725" layer="201" rot="R180"/>
<rectangle x1="8.645" y1="3.725" x2="8.895" y2="3.735" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.725" x2="8.205" y2="3.735" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.725" x2="6.945" y2="3.735" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.725" x2="6.465" y2="3.735" layer="201" rot="R180"/>
<rectangle x1="4.505" y1="3.725" x2="5.295" y2="3.735" layer="201" rot="R180"/>
<rectangle x1="2.885" y1="3.725" x2="3.385" y2="3.735" layer="201" rot="R180"/>
<rectangle x1="1.445" y1="3.725" x2="1.675" y2="3.735" layer="201" rot="R180"/>
<rectangle x1="0.995" y1="3.725" x2="1.225" y2="3.735" layer="201" rot="R180"/>
<rectangle x1="8.645" y1="3.735" x2="8.895" y2="3.745" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.735" x2="8.205" y2="3.745" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.735" x2="6.945" y2="3.745" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.735" x2="6.465" y2="3.745" layer="201" rot="R180"/>
<rectangle x1="4.485" y1="3.735" x2="5.295" y2="3.745" layer="201" rot="R180"/>
<rectangle x1="2.865" y1="3.735" x2="3.385" y2="3.745" layer="201" rot="R180"/>
<rectangle x1="1.455" y1="3.735" x2="1.685" y2="3.745" layer="201" rot="R180"/>
<rectangle x1="0.995" y1="3.735" x2="1.225" y2="3.745" layer="201" rot="R180"/>
<rectangle x1="8.645" y1="3.745" x2="8.895" y2="3.755" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.745" x2="8.205" y2="3.755" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.745" x2="6.945" y2="3.755" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.745" x2="6.465" y2="3.755" layer="201" rot="R180"/>
<rectangle x1="4.465" y1="3.745" x2="5.295" y2="3.755" layer="201" rot="R180"/>
<rectangle x1="2.855" y1="3.745" x2="3.385" y2="3.755" layer="201" rot="R180"/>
<rectangle x1="1.455" y1="3.745" x2="1.685" y2="3.755" layer="201" rot="R180"/>
<rectangle x1="0.985" y1="3.745" x2="1.215" y2="3.755" layer="201" rot="R180"/>
<rectangle x1="8.645" y1="3.755" x2="8.895" y2="3.765" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.755" x2="8.205" y2="3.765" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.755" x2="6.945" y2="3.765" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.755" x2="6.465" y2="3.765" layer="201" rot="R180"/>
<rectangle x1="4.455" y1="3.755" x2="5.295" y2="3.765" layer="201" rot="R180"/>
<rectangle x1="2.845" y1="3.755" x2="3.385" y2="3.765" layer="201" rot="R180"/>
<rectangle x1="1.455" y1="3.755" x2="1.685" y2="3.765" layer="201" rot="R180"/>
<rectangle x1="0.985" y1="3.755" x2="1.215" y2="3.765" layer="201" rot="R180"/>
<rectangle x1="8.645" y1="3.765" x2="8.895" y2="3.775" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.765" x2="8.195" y2="3.775" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.765" x2="6.945" y2="3.775" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.765" x2="6.465" y2="3.775" layer="201" rot="R180"/>
<rectangle x1="4.445" y1="3.765" x2="5.295" y2="3.775" layer="201" rot="R180"/>
<rectangle x1="2.835" y1="3.765" x2="3.385" y2="3.775" layer="201" rot="R180"/>
<rectangle x1="1.465" y1="3.765" x2="1.695" y2="3.775" layer="201" rot="R180"/>
<rectangle x1="0.985" y1="3.765" x2="1.215" y2="3.775" layer="201" rot="R180"/>
<rectangle x1="8.655" y1="3.775" x2="8.895" y2="3.785" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.775" x2="8.195" y2="3.785" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.775" x2="6.945" y2="3.785" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.775" x2="6.465" y2="3.785" layer="201" rot="R180"/>
<rectangle x1="4.445" y1="3.775" x2="5.295" y2="3.785" layer="201" rot="R180"/>
<rectangle x1="2.825" y1="3.775" x2="3.385" y2="3.785" layer="201" rot="R180"/>
<rectangle x1="1.465" y1="3.775" x2="1.695" y2="3.785" layer="201" rot="R180"/>
<rectangle x1="0.975" y1="3.775" x2="1.215" y2="3.785" layer="201" rot="R180"/>
<rectangle x1="8.655" y1="3.785" x2="8.895" y2="3.795" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.785" x2="8.195" y2="3.795" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.785" x2="6.945" y2="3.795" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.785" x2="6.465" y2="3.795" layer="201" rot="R180"/>
<rectangle x1="4.435" y1="3.785" x2="5.295" y2="3.795" layer="201" rot="R180"/>
<rectangle x1="2.825" y1="3.785" x2="3.385" y2="3.795" layer="201" rot="R180"/>
<rectangle x1="1.465" y1="3.785" x2="1.695" y2="3.795" layer="201" rot="R180"/>
<rectangle x1="0.975" y1="3.785" x2="1.205" y2="3.795" layer="201" rot="R180"/>
<rectangle x1="8.655" y1="3.795" x2="8.895" y2="3.805" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.795" x2="8.195" y2="3.805" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.795" x2="6.945" y2="3.805" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.795" x2="6.465" y2="3.805" layer="201" rot="R180"/>
<rectangle x1="4.435" y1="3.795" x2="5.295" y2="3.805" layer="201" rot="R180"/>
<rectangle x1="2.825" y1="3.795" x2="3.385" y2="3.805" layer="201" rot="R180"/>
<rectangle x1="1.465" y1="3.795" x2="1.695" y2="3.805" layer="201" rot="R180"/>
<rectangle x1="0.975" y1="3.795" x2="1.205" y2="3.805" layer="201" rot="R180"/>
<rectangle x1="8.655" y1="3.805" x2="8.895" y2="3.815" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.805" x2="8.195" y2="3.815" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.805" x2="6.945" y2="3.815" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.805" x2="6.465" y2="3.815" layer="201" rot="R180"/>
<rectangle x1="4.435" y1="3.805" x2="5.295" y2="3.815" layer="201" rot="R180"/>
<rectangle x1="2.815" y1="3.805" x2="3.385" y2="3.815" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="3.805" x2="1.705" y2="3.815" layer="201" rot="R180"/>
<rectangle x1="0.975" y1="3.805" x2="1.205" y2="3.815" layer="201" rot="R180"/>
<rectangle x1="8.655" y1="3.815" x2="8.895" y2="3.825" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.815" x2="8.185" y2="3.825" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.815" x2="6.945" y2="3.825" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.815" x2="6.465" y2="3.825" layer="201" rot="R180"/>
<rectangle x1="4.425" y1="3.815" x2="5.295" y2="3.825" layer="201" rot="R180"/>
<rectangle x1="2.815" y1="3.815" x2="3.385" y2="3.825" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="3.815" x2="1.705" y2="3.825" layer="201" rot="R180"/>
<rectangle x1="0.965" y1="3.815" x2="1.205" y2="3.825" layer="201" rot="R180"/>
<rectangle x1="8.665" y1="3.825" x2="8.895" y2="3.835" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.825" x2="8.185" y2="3.835" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.825" x2="6.945" y2="3.835" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.825" x2="6.465" y2="3.835" layer="201" rot="R180"/>
<rectangle x1="4.425" y1="3.825" x2="5.295" y2="3.835" layer="201" rot="R180"/>
<rectangle x1="2.805" y1="3.825" x2="3.385" y2="3.835" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="3.825" x2="1.705" y2="3.835" layer="201" rot="R180"/>
<rectangle x1="0.965" y1="3.825" x2="1.195" y2="3.835" layer="201" rot="R180"/>
<rectangle x1="8.665" y1="3.835" x2="8.895" y2="3.845" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.835" x2="8.185" y2="3.845" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.835" x2="6.945" y2="3.845" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.835" x2="6.465" y2="3.845" layer="201" rot="R180"/>
<rectangle x1="4.415" y1="3.835" x2="5.295" y2="3.845" layer="201" rot="R180"/>
<rectangle x1="2.805" y1="3.835" x2="3.385" y2="3.845" layer="201" rot="R180"/>
<rectangle x1="1.475" y1="3.835" x2="1.715" y2="3.845" layer="201" rot="R180"/>
<rectangle x1="0.965" y1="3.835" x2="1.195" y2="3.845" layer="201" rot="R180"/>
<rectangle x1="8.665" y1="3.845" x2="8.895" y2="3.855" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.845" x2="8.185" y2="3.855" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.845" x2="6.945" y2="3.855" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.845" x2="6.465" y2="3.855" layer="201" rot="R180"/>
<rectangle x1="4.415" y1="3.845" x2="5.295" y2="3.855" layer="201" rot="R180"/>
<rectangle x1="2.795" y1="3.845" x2="3.385" y2="3.855" layer="201" rot="R180"/>
<rectangle x1="1.485" y1="3.845" x2="1.715" y2="3.855" layer="201" rot="R180"/>
<rectangle x1="0.965" y1="3.845" x2="1.195" y2="3.855" layer="201" rot="R180"/>
<rectangle x1="8.665" y1="3.855" x2="8.895" y2="3.865" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.855" x2="8.185" y2="3.865" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.855" x2="6.945" y2="3.865" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.855" x2="6.465" y2="3.865" layer="201" rot="R180"/>
<rectangle x1="4.405" y1="3.855" x2="5.295" y2="3.865" layer="201" rot="R180"/>
<rectangle x1="2.795" y1="3.855" x2="3.385" y2="3.865" layer="201" rot="R180"/>
<rectangle x1="1.485" y1="3.855" x2="1.715" y2="3.865" layer="201" rot="R180"/>
<rectangle x1="0.955" y1="3.855" x2="1.195" y2="3.865" layer="201" rot="R180"/>
<rectangle x1="8.675" y1="3.865" x2="8.895" y2="3.875" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.865" x2="8.175" y2="3.875" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.865" x2="6.945" y2="3.875" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.865" x2="6.465" y2="3.875" layer="201" rot="R180"/>
<rectangle x1="4.405" y1="3.865" x2="5.295" y2="3.875" layer="201" rot="R180"/>
<rectangle x1="2.785" y1="3.865" x2="3.385" y2="3.875" layer="201" rot="R180"/>
<rectangle x1="1.485" y1="3.865" x2="1.715" y2="3.875" layer="201" rot="R180"/>
<rectangle x1="0.955" y1="3.865" x2="1.185" y2="3.875" layer="201" rot="R180"/>
<rectangle x1="8.675" y1="3.875" x2="8.895" y2="3.885" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.875" x2="8.175" y2="3.885" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.875" x2="6.935" y2="3.885" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.875" x2="6.465" y2="3.885" layer="201" rot="R180"/>
<rectangle x1="4.395" y1="3.875" x2="5.295" y2="3.885" layer="201" rot="R180"/>
<rectangle x1="2.785" y1="3.875" x2="3.385" y2="3.885" layer="201" rot="R180"/>
<rectangle x1="1.495" y1="3.875" x2="1.725" y2="3.885" layer="201" rot="R180"/>
<rectangle x1="0.955" y1="3.875" x2="1.185" y2="3.885" layer="201" rot="R180"/>
<rectangle x1="8.675" y1="3.885" x2="8.895" y2="3.895" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.885" x2="8.165" y2="3.895" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.885" x2="6.935" y2="3.895" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.885" x2="6.465" y2="3.895" layer="201" rot="R180"/>
<rectangle x1="4.395" y1="3.885" x2="5.295" y2="3.895" layer="201" rot="R180"/>
<rectangle x1="2.775" y1="3.885" x2="3.385" y2="3.895" layer="201" rot="R180"/>
<rectangle x1="1.495" y1="3.885" x2="1.725" y2="3.895" layer="201" rot="R180"/>
<rectangle x1="0.945" y1="3.885" x2="1.175" y2="3.895" layer="201" rot="R180"/>
<rectangle x1="8.685" y1="3.895" x2="8.895" y2="3.905" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.895" x2="8.165" y2="3.905" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.895" x2="6.935" y2="3.905" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.895" x2="6.465" y2="3.905" layer="201" rot="R180"/>
<rectangle x1="4.385" y1="3.895" x2="5.285" y2="3.905" layer="201" rot="R180"/>
<rectangle x1="2.775" y1="3.895" x2="3.375" y2="3.905" layer="201" rot="R180"/>
<rectangle x1="1.505" y1="3.895" x2="1.725" y2="3.905" layer="201" rot="R180"/>
<rectangle x1="0.945" y1="3.895" x2="1.175" y2="3.905" layer="201" rot="R180"/>
<rectangle x1="8.695" y1="3.905" x2="8.895" y2="3.915" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.905" x2="8.155" y2="3.915" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.905" x2="6.925" y2="3.915" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.905" x2="6.465" y2="3.915" layer="201" rot="R180"/>
<rectangle x1="4.385" y1="3.905" x2="5.285" y2="3.915" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="3.905" x2="3.375" y2="3.915" layer="201" rot="R180"/>
<rectangle x1="1.515" y1="3.905" x2="1.735" y2="3.915" layer="201" rot="R180"/>
<rectangle x1="0.945" y1="3.905" x2="1.165" y2="3.915" layer="201" rot="R180"/>
<rectangle x1="8.705" y1="3.915" x2="8.895" y2="3.925" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.915" x2="8.135" y2="3.925" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.915" x2="6.915" y2="3.925" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.915" x2="6.465" y2="3.925" layer="201" rot="R180"/>
<rectangle x1="4.375" y1="3.915" x2="5.275" y2="3.925" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="3.915" x2="3.365" y2="3.925" layer="201" rot="R180"/>
<rectangle x1="1.525" y1="3.915" x2="1.735" y2="3.925" layer="201" rot="R180"/>
<rectangle x1="0.945" y1="3.915" x2="1.155" y2="3.925" layer="201" rot="R180"/>
<rectangle x1="8.725" y1="3.925" x2="8.895" y2="3.935" layer="201" rot="R180"/>
<rectangle x1="7.955" y1="3.925" x2="8.115" y2="3.935" layer="201" rot="R180"/>
<rectangle x1="6.725" y1="3.925" x2="6.905" y2="3.935" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="3.925" x2="6.465" y2="3.935" layer="201" rot="R180"/>
<rectangle x1="4.375" y1="3.925" x2="5.255" y2="3.935" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="3.925" x2="3.345" y2="3.935" layer="201" rot="R180"/>
<rectangle x1="1.545" y1="3.925" x2="1.735" y2="3.935" layer="201" rot="R180"/>
<rectangle x1="0.935" y1="3.925" x2="1.135" y2="3.935" layer="201" rot="R180"/>
<rectangle x1="6.285" y1="4.535" x2="6.315" y2="4.545" layer="201" rot="R180"/>
<rectangle x1="3.475" y1="4.535" x2="3.505" y2="4.545" layer="201" rot="R180"/>
<rectangle x1="6.265" y1="4.545" x2="6.335" y2="4.555" layer="201" rot="R180"/>
<rectangle x1="3.455" y1="4.545" x2="3.525" y2="4.555" layer="201" rot="R180"/>
<rectangle x1="6.245" y1="4.555" x2="6.355" y2="4.565" layer="201" rot="R180"/>
<rectangle x1="3.435" y1="4.555" x2="3.545" y2="4.565" layer="201" rot="R180"/>
<rectangle x1="6.235" y1="4.565" x2="6.375" y2="4.575" layer="201" rot="R180"/>
<rectangle x1="3.415" y1="4.565" x2="3.555" y2="4.575" layer="201" rot="R180"/>
<rectangle x1="6.215" y1="4.575" x2="6.385" y2="4.585" layer="201" rot="R180"/>
<rectangle x1="3.405" y1="4.575" x2="3.575" y2="4.585" layer="201" rot="R180"/>
<rectangle x1="6.195" y1="4.585" x2="6.405" y2="4.595" layer="201" rot="R180"/>
<rectangle x1="3.385" y1="4.585" x2="3.595" y2="4.595" layer="201" rot="R180"/>
<rectangle x1="6.175" y1="4.595" x2="6.425" y2="4.605" layer="201" rot="R180"/>
<rectangle x1="3.365" y1="4.595" x2="3.615" y2="4.605" layer="201" rot="R180"/>
<rectangle x1="6.165" y1="4.605" x2="6.435" y2="4.615" layer="201" rot="R180"/>
<rectangle x1="3.345" y1="4.605" x2="3.625" y2="4.615" layer="201" rot="R180"/>
<rectangle x1="6.145" y1="4.615" x2="6.455" y2="4.625" layer="201" rot="R180"/>
<rectangle x1="3.335" y1="4.615" x2="3.645" y2="4.625" layer="201" rot="R180"/>
<rectangle x1="6.125" y1="4.625" x2="6.475" y2="4.635" layer="201" rot="R180"/>
<rectangle x1="3.315" y1="4.625" x2="3.665" y2="4.635" layer="201" rot="R180"/>
<rectangle x1="6.105" y1="4.635" x2="6.495" y2="4.645" layer="201" rot="R180"/>
<rectangle x1="3.295" y1="4.635" x2="3.675" y2="4.645" layer="201" rot="R180"/>
<rectangle x1="6.095" y1="4.645" x2="6.505" y2="4.655" layer="201" rot="R180"/>
<rectangle x1="3.275" y1="4.645" x2="3.695" y2="4.655" layer="201" rot="R180"/>
<rectangle x1="6.075" y1="4.655" x2="6.525" y2="4.665" layer="201" rot="R180"/>
<rectangle x1="3.265" y1="4.655" x2="3.715" y2="4.665" layer="201" rot="R180"/>
<rectangle x1="6.055" y1="4.665" x2="6.545" y2="4.675" layer="201" rot="R180"/>
<rectangle x1="3.245" y1="4.665" x2="3.735" y2="4.675" layer="201" rot="R180"/>
<rectangle x1="6.035" y1="4.675" x2="6.565" y2="4.685" layer="201" rot="R180"/>
<rectangle x1="3.225" y1="4.675" x2="3.745" y2="4.685" layer="201" rot="R180"/>
<rectangle x1="6.025" y1="4.685" x2="6.575" y2="4.695" layer="201" rot="R180"/>
<rectangle x1="3.215" y1="4.685" x2="3.765" y2="4.695" layer="201" rot="R180"/>
<rectangle x1="6.005" y1="4.695" x2="6.595" y2="4.705" layer="201" rot="R180"/>
<rectangle x1="3.195" y1="4.695" x2="3.785" y2="4.705" layer="201" rot="R180"/>
<rectangle x1="5.985" y1="4.705" x2="6.615" y2="4.715" layer="201" rot="R180"/>
<rectangle x1="3.175" y1="4.705" x2="3.805" y2="4.715" layer="201" rot="R180"/>
<rectangle x1="5.975" y1="4.715" x2="6.635" y2="4.725" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="4.715" x2="3.815" y2="4.725" layer="201" rot="R180"/>
<rectangle x1="5.955" y1="4.725" x2="6.645" y2="4.735" layer="201" rot="R180"/>
<rectangle x1="3.145" y1="4.725" x2="3.835" y2="4.735" layer="201" rot="R180"/>
<rectangle x1="5.935" y1="4.735" x2="6.665" y2="4.745" layer="201" rot="R180"/>
<rectangle x1="3.125" y1="4.735" x2="3.855" y2="4.745" layer="201" rot="R180"/>
<rectangle x1="5.915" y1="4.745" x2="6.685" y2="4.755" layer="201" rot="R180"/>
<rectangle x1="3.105" y1="4.745" x2="3.875" y2="4.755" layer="201" rot="R180"/>
<rectangle x1="5.905" y1="4.755" x2="6.695" y2="4.765" layer="201" rot="R180"/>
<rectangle x1="3.085" y1="4.755" x2="3.885" y2="4.765" layer="201" rot="R180"/>
<rectangle x1="5.885" y1="4.765" x2="6.715" y2="4.775" layer="201" rot="R180"/>
<rectangle x1="3.075" y1="4.765" x2="3.905" y2="4.775" layer="201" rot="R180"/>
<rectangle x1="5.865" y1="4.775" x2="6.735" y2="4.785" layer="201" rot="R180"/>
<rectangle x1="3.055" y1="4.775" x2="3.925" y2="4.785" layer="201" rot="R180"/>
<rectangle x1="5.845" y1="4.785" x2="6.755" y2="4.795" layer="201" rot="R180"/>
<rectangle x1="3.035" y1="4.785" x2="3.935" y2="4.795" layer="201" rot="R180"/>
<rectangle x1="5.835" y1="4.795" x2="6.765" y2="4.805" layer="201" rot="R180"/>
<rectangle x1="3.015" y1="4.795" x2="3.955" y2="4.805" layer="201" rot="R180"/>
<rectangle x1="5.815" y1="4.805" x2="6.785" y2="4.815" layer="201" rot="R180"/>
<rectangle x1="3.005" y1="4.805" x2="3.975" y2="4.815" layer="201" rot="R180"/>
<rectangle x1="5.795" y1="4.815" x2="6.805" y2="4.825" layer="201" rot="R180"/>
<rectangle x1="2.985" y1="4.815" x2="3.995" y2="4.825" layer="201" rot="R180"/>
<rectangle x1="5.775" y1="4.825" x2="6.825" y2="4.835" layer="201" rot="R180"/>
<rectangle x1="2.965" y1="4.825" x2="4.005" y2="4.835" layer="201" rot="R180"/>
<rectangle x1="5.765" y1="4.835" x2="6.835" y2="4.845" layer="201" rot="R180"/>
<rectangle x1="2.955" y1="4.835" x2="4.025" y2="4.845" layer="201" rot="R180"/>
<rectangle x1="5.745" y1="4.845" x2="6.855" y2="4.855" layer="201" rot="R180"/>
<rectangle x1="2.935" y1="4.845" x2="4.045" y2="4.855" layer="201" rot="R180"/>
<rectangle x1="5.725" y1="4.855" x2="6.875" y2="4.865" layer="201" rot="R180"/>
<rectangle x1="2.915" y1="4.855" x2="4.065" y2="4.865" layer="201" rot="R180"/>
<rectangle x1="5.715" y1="4.865" x2="6.895" y2="4.875" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="4.865" x2="4.075" y2="4.875" layer="201" rot="R180"/>
<rectangle x1="5.695" y1="4.875" x2="6.905" y2="4.885" layer="201" rot="R180"/>
<rectangle x1="2.885" y1="4.875" x2="4.095" y2="4.885" layer="201" rot="R180"/>
<rectangle x1="5.675" y1="4.885" x2="6.925" y2="4.895" layer="201" rot="R180"/>
<rectangle x1="2.865" y1="4.885" x2="4.115" y2="4.895" layer="201" rot="R180"/>
<rectangle x1="5.655" y1="4.895" x2="6.945" y2="4.905" layer="201" rot="R180"/>
<rectangle x1="2.845" y1="4.895" x2="4.135" y2="4.905" layer="201" rot="R180"/>
<rectangle x1="5.645" y1="4.905" x2="6.955" y2="4.915" layer="201" rot="R180"/>
<rectangle x1="2.825" y1="4.905" x2="4.145" y2="4.915" layer="201" rot="R180"/>
<rectangle x1="5.625" y1="4.915" x2="6.975" y2="4.925" layer="201" rot="R180"/>
<rectangle x1="2.815" y1="4.915" x2="4.165" y2="4.925" layer="201" rot="R180"/>
<rectangle x1="5.605" y1="4.925" x2="6.995" y2="4.935" layer="201" rot="R180"/>
<rectangle x1="2.795" y1="4.925" x2="4.185" y2="4.935" layer="201" rot="R180"/>
<rectangle x1="5.585" y1="4.935" x2="7.015" y2="4.945" layer="201" rot="R180"/>
<rectangle x1="2.775" y1="4.935" x2="4.195" y2="4.945" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="4.945" x2="7.025" y2="4.955" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="4.945" x2="4.215" y2="4.955" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="4.955" x2="7.025" y2="4.965" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="4.955" x2="4.215" y2="4.965" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="4.965" x2="7.025" y2="4.975" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="4.965" x2="4.215" y2="4.975" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="4.975" x2="7.025" y2="4.985" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="4.975" x2="4.215" y2="4.985" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="4.985" x2="7.025" y2="4.995" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="4.985" x2="4.215" y2="4.995" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="4.995" x2="7.025" y2="5.005" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="4.995" x2="4.215" y2="5.005" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.005" x2="7.025" y2="5.015" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.005" x2="4.215" y2="5.015" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.015" x2="7.025" y2="5.025" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.015" x2="4.215" y2="5.025" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.025" x2="7.025" y2="5.035" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.025" x2="4.215" y2="5.035" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.035" x2="7.025" y2="5.045" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.035" x2="4.215" y2="5.045" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.045" x2="7.025" y2="5.055" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.045" x2="4.215" y2="5.055" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.055" x2="7.025" y2="5.065" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.055" x2="4.215" y2="5.065" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.065" x2="7.025" y2="5.075" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.065" x2="4.215" y2="5.075" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.075" x2="7.025" y2="5.085" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.075" x2="4.215" y2="5.085" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.085" x2="7.025" y2="5.095" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.085" x2="4.215" y2="5.095" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.095" x2="7.025" y2="5.105" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.095" x2="4.215" y2="5.105" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.105" x2="7.025" y2="5.115" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.105" x2="4.215" y2="5.115" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.115" x2="7.025" y2="5.125" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.115" x2="4.215" y2="5.125" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.125" x2="7.025" y2="5.135" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.125" x2="4.215" y2="5.135" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.135" x2="7.025" y2="5.145" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.135" x2="4.215" y2="5.145" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.145" x2="7.025" y2="5.155" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.145" x2="4.215" y2="5.155" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.155" x2="7.025" y2="5.165" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.155" x2="4.215" y2="5.165" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.165" x2="7.025" y2="5.175" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.165" x2="4.215" y2="5.175" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.175" x2="7.025" y2="5.185" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.175" x2="4.215" y2="5.185" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.185" x2="7.025" y2="5.195" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.185" x2="4.215" y2="5.195" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.195" x2="7.025" y2="5.205" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.195" x2="4.215" y2="5.205" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.205" x2="7.025" y2="5.215" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.205" x2="4.215" y2="5.215" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.215" x2="7.025" y2="5.225" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.215" x2="4.215" y2="5.225" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.225" x2="7.025" y2="5.235" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.225" x2="4.215" y2="5.235" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.235" x2="7.025" y2="5.245" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.235" x2="4.215" y2="5.245" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.245" x2="7.025" y2="5.255" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.245" x2="4.215" y2="5.255" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.255" x2="7.025" y2="5.265" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.255" x2="4.215" y2="5.265" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.265" x2="7.025" y2="5.275" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.265" x2="4.215" y2="5.275" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.275" x2="7.025" y2="5.285" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.275" x2="4.215" y2="5.285" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.285" x2="7.025" y2="5.295" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.285" x2="4.215" y2="5.295" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.295" x2="7.025" y2="5.305" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.295" x2="4.215" y2="5.305" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.305" x2="7.025" y2="5.315" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.305" x2="4.215" y2="5.315" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.315" x2="7.025" y2="5.325" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.315" x2="4.215" y2="5.325" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.325" x2="7.025" y2="5.335" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.325" x2="4.215" y2="5.335" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.335" x2="7.025" y2="5.345" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.335" x2="4.215" y2="5.345" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.345" x2="7.025" y2="5.355" layer="201" rot="R180"/>
<rectangle x1="4.875" y1="5.345" x2="4.905" y2="5.355" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.345" x2="4.215" y2="5.355" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.355" x2="7.025" y2="5.365" layer="201" rot="R180"/>
<rectangle x1="4.865" y1="5.355" x2="4.925" y2="5.365" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.355" x2="4.215" y2="5.365" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.365" x2="7.025" y2="5.375" layer="201" rot="R180"/>
<rectangle x1="4.845" y1="5.365" x2="4.945" y2="5.375" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.365" x2="4.215" y2="5.375" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.375" x2="7.025" y2="5.385" layer="201" rot="R180"/>
<rectangle x1="4.825" y1="5.375" x2="4.965" y2="5.385" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.375" x2="4.215" y2="5.385" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.385" x2="7.025" y2="5.395" layer="201" rot="R180"/>
<rectangle x1="4.805" y1="5.385" x2="4.975" y2="5.395" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.385" x2="4.215" y2="5.395" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.395" x2="7.025" y2="5.405" layer="201" rot="R180"/>
<rectangle x1="4.795" y1="5.395" x2="4.995" y2="5.405" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.395" x2="4.215" y2="5.405" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.405" x2="7.025" y2="5.415" layer="201" rot="R180"/>
<rectangle x1="4.775" y1="5.405" x2="5.015" y2="5.415" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.405" x2="4.215" y2="5.415" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.415" x2="7.025" y2="5.425" layer="201" rot="R180"/>
<rectangle x1="4.755" y1="5.415" x2="5.035" y2="5.425" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.415" x2="4.215" y2="5.425" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.425" x2="7.025" y2="5.435" layer="201" rot="R180"/>
<rectangle x1="4.745" y1="5.425" x2="5.045" y2="5.435" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.425" x2="4.215" y2="5.435" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.435" x2="7.025" y2="5.445" layer="201" rot="R180"/>
<rectangle x1="4.725" y1="5.435" x2="5.065" y2="5.445" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.435" x2="4.215" y2="5.445" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.445" x2="7.025" y2="5.455" layer="201" rot="R180"/>
<rectangle x1="4.705" y1="5.445" x2="5.085" y2="5.455" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.445" x2="4.215" y2="5.455" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.455" x2="7.025" y2="5.465" layer="201" rot="R180"/>
<rectangle x1="4.685" y1="5.455" x2="5.105" y2="5.465" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.455" x2="4.215" y2="5.465" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.465" x2="7.025" y2="5.475" layer="201" rot="R180"/>
<rectangle x1="4.675" y1="5.465" x2="5.115" y2="5.475" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.465" x2="4.215" y2="5.475" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.475" x2="7.025" y2="5.485" layer="201" rot="R180"/>
<rectangle x1="4.655" y1="5.475" x2="5.135" y2="5.485" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.475" x2="4.215" y2="5.485" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.485" x2="7.025" y2="5.495" layer="201" rot="R180"/>
<rectangle x1="4.635" y1="5.485" x2="5.155" y2="5.495" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.485" x2="4.215" y2="5.495" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.495" x2="7.025" y2="5.505" layer="201" rot="R180"/>
<rectangle x1="4.615" y1="5.495" x2="5.165" y2="5.505" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.495" x2="4.215" y2="5.505" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.505" x2="7.025" y2="5.515" layer="201" rot="R180"/>
<rectangle x1="4.605" y1="5.505" x2="5.185" y2="5.515" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.505" x2="4.215" y2="5.515" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.515" x2="7.025" y2="5.525" layer="201" rot="R180"/>
<rectangle x1="4.585" y1="5.515" x2="5.205" y2="5.525" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.515" x2="4.215" y2="5.525" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.525" x2="7.025" y2="5.535" layer="201" rot="R180"/>
<rectangle x1="4.565" y1="5.525" x2="5.225" y2="5.535" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.525" x2="4.215" y2="5.535" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.535" x2="7.025" y2="5.545" layer="201" rot="R180"/>
<rectangle x1="4.545" y1="5.535" x2="5.235" y2="5.545" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.535" x2="4.215" y2="5.545" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.545" x2="7.025" y2="5.555" layer="201" rot="R180"/>
<rectangle x1="4.535" y1="5.545" x2="5.255" y2="5.555" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.545" x2="4.215" y2="5.555" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.555" x2="7.025" y2="5.565" layer="201" rot="R180"/>
<rectangle x1="4.515" y1="5.555" x2="5.275" y2="5.565" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.555" x2="4.215" y2="5.565" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.565" x2="7.025" y2="5.575" layer="201" rot="R180"/>
<rectangle x1="4.495" y1="5.565" x2="5.295" y2="5.575" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.565" x2="4.215" y2="5.575" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.575" x2="7.025" y2="5.585" layer="201" rot="R180"/>
<rectangle x1="4.485" y1="5.575" x2="5.305" y2="5.585" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.575" x2="4.215" y2="5.585" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.585" x2="7.025" y2="5.595" layer="201" rot="R180"/>
<rectangle x1="4.465" y1="5.585" x2="5.325" y2="5.595" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.585" x2="4.215" y2="5.595" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.595" x2="7.025" y2="5.605" layer="201" rot="R180"/>
<rectangle x1="4.445" y1="5.595" x2="5.345" y2="5.605" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.595" x2="4.215" y2="5.605" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.605" x2="7.025" y2="5.615" layer="201" rot="R180"/>
<rectangle x1="4.425" y1="5.605" x2="5.365" y2="5.615" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.605" x2="4.215" y2="5.615" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.615" x2="7.025" y2="5.625" layer="201" rot="R180"/>
<rectangle x1="4.415" y1="5.615" x2="5.375" y2="5.625" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.615" x2="4.215" y2="5.625" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.625" x2="7.025" y2="5.635" layer="201" rot="R180"/>
<rectangle x1="4.395" y1="5.625" x2="5.395" y2="5.635" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.625" x2="4.215" y2="5.635" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.635" x2="7.025" y2="5.645" layer="201" rot="R180"/>
<rectangle x1="4.375" y1="5.635" x2="5.415" y2="5.645" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.635" x2="4.215" y2="5.645" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.645" x2="7.025" y2="5.655" layer="201" rot="R180"/>
<rectangle x1="4.355" y1="5.645" x2="5.425" y2="5.655" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.645" x2="4.215" y2="5.655" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.655" x2="7.025" y2="5.665" layer="201" rot="R180"/>
<rectangle x1="4.345" y1="5.655" x2="5.445" y2="5.665" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.655" x2="4.215" y2="5.665" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.665" x2="7.025" y2="5.675" layer="201" rot="R180"/>
<rectangle x1="4.325" y1="5.665" x2="5.465" y2="5.675" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.665" x2="4.215" y2="5.675" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.675" x2="7.025" y2="5.685" layer="201" rot="R180"/>
<rectangle x1="4.305" y1="5.675" x2="5.485" y2="5.685" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.675" x2="4.215" y2="5.685" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.685" x2="7.025" y2="5.695" layer="201" rot="R180"/>
<rectangle x1="4.295" y1="5.685" x2="5.495" y2="5.695" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.685" x2="4.215" y2="5.695" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.695" x2="7.025" y2="5.705" layer="201" rot="R180"/>
<rectangle x1="4.275" y1="5.695" x2="5.515" y2="5.705" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.695" x2="4.215" y2="5.705" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.705" x2="7.025" y2="5.715" layer="201" rot="R180"/>
<rectangle x1="4.255" y1="5.705" x2="5.535" y2="5.715" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.705" x2="4.215" y2="5.715" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.715" x2="7.025" y2="5.725" layer="201" rot="R180"/>
<rectangle x1="4.235" y1="5.715" x2="5.555" y2="5.725" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.715" x2="4.215" y2="5.725" layer="201" rot="R180"/>
<rectangle x1="5.575" y1="5.725" x2="7.025" y2="5.735" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.725" x2="5.565" y2="5.735" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.735" x2="7.025" y2="5.745" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.745" x2="7.025" y2="5.755" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.755" x2="7.025" y2="5.765" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.765" x2="7.025" y2="5.775" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.775" x2="7.025" y2="5.785" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.785" x2="7.025" y2="5.795" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.795" x2="7.025" y2="5.805" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.805" x2="7.025" y2="5.815" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.815" x2="7.025" y2="5.825" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.825" x2="7.025" y2="5.835" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.835" x2="7.025" y2="5.845" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.845" x2="7.025" y2="5.855" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.855" x2="7.025" y2="5.865" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.865" x2="7.025" y2="5.875" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.875" x2="7.025" y2="5.885" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.885" x2="7.025" y2="5.895" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.895" x2="7.025" y2="5.905" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.905" x2="7.025" y2="5.915" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.915" x2="7.025" y2="5.925" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.925" x2="7.025" y2="5.935" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.935" x2="7.025" y2="5.945" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.945" x2="7.025" y2="5.955" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.955" x2="7.025" y2="5.965" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.965" x2="7.025" y2="5.975" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.975" x2="7.025" y2="5.985" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.985" x2="7.025" y2="5.995" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="5.995" x2="7.025" y2="6.005" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.005" x2="7.025" y2="6.015" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.015" x2="7.025" y2="6.025" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.025" x2="7.025" y2="6.035" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.035" x2="7.025" y2="6.045" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.045" x2="7.025" y2="6.055" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.055" x2="7.025" y2="6.065" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.065" x2="7.025" y2="6.075" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.075" x2="7.025" y2="6.085" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.085" x2="7.025" y2="6.095" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.095" x2="7.025" y2="6.105" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.105" x2="7.025" y2="6.115" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.115" x2="7.025" y2="6.125" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.125" x2="7.025" y2="6.135" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.135" x2="7.025" y2="6.145" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.145" x2="7.025" y2="6.155" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.155" x2="7.025" y2="6.165" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.165" x2="7.025" y2="6.175" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.175" x2="7.025" y2="6.185" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.185" x2="7.025" y2="6.195" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.195" x2="7.025" y2="6.205" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.205" x2="7.025" y2="6.215" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.215" x2="7.025" y2="6.225" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.225" x2="7.025" y2="6.235" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.235" x2="7.025" y2="6.245" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.245" x2="7.025" y2="6.255" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.255" x2="7.025" y2="6.265" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.265" x2="7.025" y2="6.275" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.275" x2="7.025" y2="6.285" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.285" x2="7.025" y2="6.295" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.295" x2="7.025" y2="6.305" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.305" x2="7.025" y2="6.315" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.315" x2="7.025" y2="6.325" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.325" x2="7.025" y2="6.335" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.335" x2="7.025" y2="6.345" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.345" x2="7.025" y2="6.355" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.355" x2="7.025" y2="6.365" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.365" x2="7.025" y2="6.375" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.375" x2="7.025" y2="6.385" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.385" x2="7.025" y2="6.395" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.395" x2="7.025" y2="6.405" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.405" x2="7.025" y2="6.415" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.415" x2="7.025" y2="6.425" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.425" x2="7.025" y2="6.435" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.435" x2="7.025" y2="6.445" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.445" x2="7.025" y2="6.455" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.455" x2="7.025" y2="6.465" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.465" x2="7.025" y2="6.475" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.475" x2="7.025" y2="6.485" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.485" x2="7.025" y2="6.495" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.495" x2="7.025" y2="6.505" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.505" x2="7.025" y2="6.515" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.515" x2="7.025" y2="6.525" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.525" x2="7.025" y2="6.535" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.535" x2="7.025" y2="6.545" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.545" x2="7.025" y2="6.555" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.555" x2="7.025" y2="6.565" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.565" x2="7.025" y2="6.575" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.575" x2="7.025" y2="6.585" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.585" x2="7.025" y2="6.595" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.595" x2="7.025" y2="6.605" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.605" x2="7.025" y2="6.615" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.615" x2="7.025" y2="6.625" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.625" x2="7.025" y2="6.635" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.635" x2="7.025" y2="6.645" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.645" x2="7.025" y2="6.655" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.655" x2="7.025" y2="6.665" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.665" x2="7.025" y2="6.675" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.675" x2="7.025" y2="6.685" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.685" x2="7.025" y2="6.695" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.695" x2="7.025" y2="6.705" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.705" x2="7.025" y2="6.715" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.715" x2="7.025" y2="6.725" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.725" x2="7.025" y2="6.735" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.735" x2="7.025" y2="6.745" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.745" x2="7.025" y2="6.755" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.755" x2="7.025" y2="6.765" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.765" x2="7.025" y2="6.775" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.775" x2="7.025" y2="6.785" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.785" x2="7.025" y2="6.795" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.795" x2="7.025" y2="6.805" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.805" x2="7.025" y2="6.815" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.815" x2="7.025" y2="6.825" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.825" x2="7.025" y2="6.835" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.835" x2="7.025" y2="6.845" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.845" x2="7.025" y2="6.855" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.855" x2="7.025" y2="6.865" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.865" x2="7.025" y2="6.875" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.875" x2="7.025" y2="6.885" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.885" x2="7.025" y2="6.895" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.895" x2="7.025" y2="6.905" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.905" x2="7.025" y2="6.915" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.915" x2="7.025" y2="6.925" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.925" x2="7.025" y2="6.935" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.935" x2="7.025" y2="6.945" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.945" x2="7.025" y2="6.955" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.955" x2="7.025" y2="6.965" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.965" x2="7.025" y2="6.975" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.975" x2="7.025" y2="6.985" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.985" x2="7.025" y2="6.995" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="6.995" x2="7.025" y2="7.005" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.005" x2="7.025" y2="7.015" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.015" x2="7.025" y2="7.025" layer="201" rot="R180"/>
<rectangle x1="4.905" y1="7.025" x2="7.025" y2="7.035" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.025" x2="4.885" y2="7.035" layer="201" rot="R180"/>
<rectangle x1="4.925" y1="7.035" x2="7.025" y2="7.045" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.035" x2="4.865" y2="7.045" layer="201" rot="R180"/>
<rectangle x1="4.945" y1="7.045" x2="7.025" y2="7.055" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.045" x2="4.845" y2="7.055" layer="201" rot="R180"/>
<rectangle x1="4.955" y1="7.055" x2="7.025" y2="7.065" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.055" x2="4.835" y2="7.065" layer="201" rot="R180"/>
<rectangle x1="4.975" y1="7.065" x2="7.025" y2="7.075" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.065" x2="4.815" y2="7.075" layer="201" rot="R180"/>
<rectangle x1="4.995" y1="7.075" x2="7.025" y2="7.085" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.075" x2="4.795" y2="7.085" layer="201" rot="R180"/>
<rectangle x1="5.005" y1="7.085" x2="7.025" y2="7.095" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.085" x2="4.775" y2="7.095" layer="201" rot="R180"/>
<rectangle x1="5.025" y1="7.095" x2="7.025" y2="7.105" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.095" x2="4.765" y2="7.105" layer="201" rot="R180"/>
<rectangle x1="5.045" y1="7.105" x2="7.025" y2="7.115" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.105" x2="4.745" y2="7.115" layer="201" rot="R180"/>
<rectangle x1="5.065" y1="7.115" x2="7.025" y2="7.125" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.115" x2="4.725" y2="7.125" layer="201" rot="R180"/>
<rectangle x1="5.075" y1="7.125" x2="7.025" y2="7.135" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.125" x2="4.715" y2="7.135" layer="201" rot="R180"/>
<rectangle x1="5.095" y1="7.135" x2="7.025" y2="7.145" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.135" x2="4.695" y2="7.145" layer="201" rot="R180"/>
<rectangle x1="5.115" y1="7.145" x2="7.025" y2="7.155" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.145" x2="4.675" y2="7.155" layer="201" rot="R180"/>
<rectangle x1="5.135" y1="7.155" x2="7.025" y2="7.165" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.155" x2="4.655" y2="7.165" layer="201" rot="R180"/>
<rectangle x1="5.145" y1="7.165" x2="7.025" y2="7.175" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.165" x2="4.645" y2="7.175" layer="201" rot="R180"/>
<rectangle x1="5.165" y1="7.175" x2="7.025" y2="7.185" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.175" x2="4.625" y2="7.185" layer="201" rot="R180"/>
<rectangle x1="5.185" y1="7.185" x2="7.025" y2="7.195" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.185" x2="4.605" y2="7.195" layer="201" rot="R180"/>
<rectangle x1="5.205" y1="7.195" x2="7.025" y2="7.205" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.195" x2="4.585" y2="7.205" layer="201" rot="R180"/>
<rectangle x1="5.215" y1="7.205" x2="7.025" y2="7.215" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.205" x2="4.575" y2="7.215" layer="201" rot="R180"/>
<rectangle x1="5.235" y1="7.215" x2="7.025" y2="7.225" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.215" x2="4.555" y2="7.225" layer="201" rot="R180"/>
<rectangle x1="5.255" y1="7.225" x2="7.025" y2="7.235" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.225" x2="4.535" y2="7.235" layer="201" rot="R180"/>
<rectangle x1="5.265" y1="7.235" x2="7.025" y2="7.245" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.235" x2="4.515" y2="7.245" layer="201" rot="R180"/>
<rectangle x1="5.285" y1="7.245" x2="7.025" y2="7.255" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.245" x2="4.505" y2="7.255" layer="201" rot="R180"/>
<rectangle x1="5.305" y1="7.255" x2="7.025" y2="7.265" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.255" x2="4.485" y2="7.265" layer="201" rot="R180"/>
<rectangle x1="5.325" y1="7.265" x2="7.025" y2="7.275" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.265" x2="4.465" y2="7.275" layer="201" rot="R180"/>
<rectangle x1="5.335" y1="7.275" x2="7.025" y2="7.285" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.275" x2="4.455" y2="7.285" layer="201" rot="R180"/>
<rectangle x1="5.355" y1="7.285" x2="7.025" y2="7.295" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.285" x2="4.435" y2="7.295" layer="201" rot="R180"/>
<rectangle x1="5.375" y1="7.295" x2="7.025" y2="7.305" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.295" x2="4.415" y2="7.305" layer="201" rot="R180"/>
<rectangle x1="5.395" y1="7.305" x2="7.025" y2="7.315" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.305" x2="4.395" y2="7.315" layer="201" rot="R180"/>
<rectangle x1="5.405" y1="7.315" x2="7.025" y2="7.325" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.315" x2="4.385" y2="7.325" layer="201" rot="R180"/>
<rectangle x1="5.425" y1="7.325" x2="7.025" y2="7.335" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.325" x2="4.365" y2="7.335" layer="201" rot="R180"/>
<rectangle x1="5.445" y1="7.335" x2="7.025" y2="7.345" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.335" x2="4.345" y2="7.345" layer="201" rot="R180"/>
<rectangle x1="5.455" y1="7.345" x2="7.025" y2="7.355" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.345" x2="4.325" y2="7.355" layer="201" rot="R180"/>
<rectangle x1="5.475" y1="7.355" x2="7.025" y2="7.365" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.355" x2="4.315" y2="7.365" layer="201" rot="R180"/>
<rectangle x1="5.495" y1="7.365" x2="7.025" y2="7.375" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.365" x2="4.295" y2="7.375" layer="201" rot="R180"/>
<rectangle x1="5.515" y1="7.375" x2="7.025" y2="7.385" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.375" x2="4.275" y2="7.385" layer="201" rot="R180"/>
<rectangle x1="5.525" y1="7.385" x2="7.025" y2="7.395" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.385" x2="4.255" y2="7.395" layer="201" rot="R180"/>
<rectangle x1="5.545" y1="7.395" x2="7.025" y2="7.405" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.395" x2="4.245" y2="7.405" layer="201" rot="R180"/>
<rectangle x1="5.565" y1="7.405" x2="7.025" y2="7.415" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.405" x2="4.225" y2="7.415" layer="201" rot="R180"/>
<rectangle x1="5.585" y1="7.415" x2="7.015" y2="7.425" layer="201" rot="R180"/>
<rectangle x1="2.765" y1="7.415" x2="4.205" y2="7.425" layer="201" rot="R180"/>
<rectangle x1="5.595" y1="7.425" x2="7.005" y2="7.435" layer="201" rot="R180"/>
<rectangle x1="2.785" y1="7.425" x2="4.195" y2="7.435" layer="201" rot="R180"/>
<rectangle x1="5.615" y1="7.435" x2="6.985" y2="7.445" layer="201" rot="R180"/>
<rectangle x1="2.805" y1="7.435" x2="4.175" y2="7.445" layer="201" rot="R180"/>
<rectangle x1="5.635" y1="7.445" x2="6.965" y2="7.455" layer="201" rot="R180"/>
<rectangle x1="2.825" y1="7.445" x2="4.155" y2="7.455" layer="201" rot="R180"/>
<rectangle x1="5.655" y1="7.455" x2="6.955" y2="7.465" layer="201" rot="R180"/>
<rectangle x1="2.835" y1="7.455" x2="4.135" y2="7.465" layer="201" rot="R180"/>
<rectangle x1="5.665" y1="7.465" x2="6.935" y2="7.475" layer="201" rot="R180"/>
<rectangle x1="2.855" y1="7.465" x2="4.125" y2="7.475" layer="201" rot="R180"/>
<rectangle x1="5.685" y1="7.475" x2="6.915" y2="7.485" layer="201" rot="R180"/>
<rectangle x1="2.875" y1="7.475" x2="4.105" y2="7.485" layer="201" rot="R180"/>
<rectangle x1="5.705" y1="7.485" x2="6.895" y2="7.495" layer="201" rot="R180"/>
<rectangle x1="2.895" y1="7.485" x2="4.085" y2="7.495" layer="201" rot="R180"/>
<rectangle x1="5.715" y1="7.495" x2="6.885" y2="7.505" layer="201" rot="R180"/>
<rectangle x1="2.905" y1="7.495" x2="4.065" y2="7.505" layer="201" rot="R180"/>
<rectangle x1="5.735" y1="7.505" x2="6.865" y2="7.515" layer="201" rot="R180"/>
<rectangle x1="2.925" y1="7.505" x2="4.055" y2="7.515" layer="201" rot="R180"/>
<rectangle x1="5.755" y1="7.515" x2="6.845" y2="7.525" layer="201" rot="R180"/>
<rectangle x1="2.945" y1="7.515" x2="4.035" y2="7.525" layer="201" rot="R180"/>
<rectangle x1="5.775" y1="7.525" x2="6.825" y2="7.535" layer="201" rot="R180"/>
<rectangle x1="2.955" y1="7.525" x2="4.015" y2="7.535" layer="201" rot="R180"/>
<rectangle x1="5.785" y1="7.535" x2="6.815" y2="7.545" layer="201" rot="R180"/>
<rectangle x1="2.975" y1="7.535" x2="3.995" y2="7.545" layer="201" rot="R180"/>
<rectangle x1="5.805" y1="7.545" x2="6.795" y2="7.555" layer="201" rot="R180"/>
<rectangle x1="2.995" y1="7.545" x2="3.985" y2="7.555" layer="201" rot="R180"/>
<rectangle x1="5.825" y1="7.555" x2="6.775" y2="7.565" layer="201" rot="R180"/>
<rectangle x1="3.015" y1="7.555" x2="3.965" y2="7.565" layer="201" rot="R180"/>
<rectangle x1="5.845" y1="7.565" x2="6.755" y2="7.575" layer="201" rot="R180"/>
<rectangle x1="3.025" y1="7.565" x2="3.945" y2="7.575" layer="201" rot="R180"/>
<rectangle x1="5.855" y1="7.575" x2="6.745" y2="7.585" layer="201" rot="R180"/>
<rectangle x1="3.045" y1="7.575" x2="3.935" y2="7.585" layer="201" rot="R180"/>
<rectangle x1="5.875" y1="7.585" x2="6.725" y2="7.595" layer="201" rot="R180"/>
<rectangle x1="3.065" y1="7.585" x2="3.915" y2="7.595" layer="201" rot="R180"/>
<rectangle x1="5.895" y1="7.595" x2="6.705" y2="7.605" layer="201" rot="R180"/>
<rectangle x1="3.085" y1="7.595" x2="3.895" y2="7.605" layer="201" rot="R180"/>
<rectangle x1="5.915" y1="7.605" x2="6.695" y2="7.615" layer="201" rot="R180"/>
<rectangle x1="3.095" y1="7.605" x2="3.875" y2="7.615" layer="201" rot="R180"/>
<rectangle x1="5.925" y1="7.615" x2="6.675" y2="7.625" layer="201" rot="R180"/>
<rectangle x1="3.115" y1="7.615" x2="3.865" y2="7.625" layer="201" rot="R180"/>
<rectangle x1="5.945" y1="7.625" x2="6.655" y2="7.635" layer="201" rot="R180"/>
<rectangle x1="3.135" y1="7.625" x2="3.845" y2="7.635" layer="201" rot="R180"/>
<rectangle x1="5.965" y1="7.635" x2="6.635" y2="7.645" layer="201" rot="R180"/>
<rectangle x1="3.155" y1="7.635" x2="3.825" y2="7.645" layer="201" rot="R180"/>
<rectangle x1="5.975" y1="7.645" x2="6.625" y2="7.655" layer="201" rot="R180"/>
<rectangle x1="3.165" y1="7.645" x2="3.805" y2="7.655" layer="201" rot="R180"/>
<rectangle x1="5.995" y1="7.655" x2="6.605" y2="7.665" layer="201" rot="R180"/>
<rectangle x1="3.185" y1="7.655" x2="3.795" y2="7.665" layer="201" rot="R180"/>
<rectangle x1="6.015" y1="7.665" x2="6.585" y2="7.675" layer="201" rot="R180"/>
<rectangle x1="3.205" y1="7.665" x2="3.775" y2="7.675" layer="201" rot="R180"/>
<rectangle x1="6.035" y1="7.675" x2="6.565" y2="7.685" layer="201" rot="R180"/>
<rectangle x1="3.215" y1="7.675" x2="3.755" y2="7.685" layer="201" rot="R180"/>
<rectangle x1="6.045" y1="7.685" x2="6.555" y2="7.695" layer="201" rot="R180"/>
<rectangle x1="3.235" y1="7.685" x2="3.745" y2="7.695" layer="201" rot="R180"/>
<rectangle x1="6.065" y1="7.695" x2="6.535" y2="7.705" layer="201" rot="R180"/>
<rectangle x1="3.255" y1="7.695" x2="3.725" y2="7.705" layer="201" rot="R180"/>
<rectangle x1="6.085" y1="7.705" x2="6.515" y2="7.715" layer="201" rot="R180"/>
<rectangle x1="3.275" y1="7.705" x2="3.705" y2="7.715" layer="201" rot="R180"/>
<rectangle x1="6.105" y1="7.715" x2="6.505" y2="7.725" layer="201" rot="R180"/>
<rectangle x1="3.285" y1="7.715" x2="3.685" y2="7.725" layer="201" rot="R180"/>
<rectangle x1="6.115" y1="7.725" x2="6.485" y2="7.735" layer="201" rot="R180"/>
<rectangle x1="3.305" y1="7.725" x2="3.675" y2="7.735" layer="201" rot="R180"/>
<rectangle x1="6.135" y1="7.735" x2="6.465" y2="7.745" layer="201" rot="R180"/>
<rectangle x1="3.325" y1="7.735" x2="3.655" y2="7.745" layer="201" rot="R180"/>
<rectangle x1="6.155" y1="7.745" x2="6.445" y2="7.755" layer="201" rot="R180"/>
<rectangle x1="3.345" y1="7.745" x2="3.635" y2="7.755" layer="201" rot="R180"/>
<rectangle x1="6.175" y1="7.755" x2="6.435" y2="7.765" layer="201" rot="R180"/>
<rectangle x1="3.355" y1="7.755" x2="3.615" y2="7.765" layer="201" rot="R180"/>
<rectangle x1="6.185" y1="7.765" x2="6.415" y2="7.775" layer="201" rot="R180"/>
<rectangle x1="3.375" y1="7.765" x2="3.605" y2="7.775" layer="201" rot="R180"/>
<rectangle x1="6.205" y1="7.775" x2="6.395" y2="7.785" layer="201" rot="R180"/>
<rectangle x1="3.395" y1="7.775" x2="3.585" y2="7.785" layer="201" rot="R180"/>
<rectangle x1="6.225" y1="7.785" x2="6.375" y2="7.795" layer="201" rot="R180"/>
<rectangle x1="3.415" y1="7.785" x2="3.565" y2="7.795" layer="201" rot="R180"/>
<rectangle x1="6.235" y1="7.795" x2="6.365" y2="7.805" layer="201" rot="R180"/>
<rectangle x1="3.425" y1="7.795" x2="3.545" y2="7.805" layer="201" rot="R180"/>
<rectangle x1="6.255" y1="7.805" x2="6.345" y2="7.815" layer="201" rot="R180"/>
<rectangle x1="3.445" y1="7.805" x2="3.535" y2="7.815" layer="201" rot="R180"/>
<rectangle x1="6.275" y1="7.815" x2="6.325" y2="7.825" layer="201" rot="R180"/>
<rectangle x1="3.465" y1="7.815" x2="3.515" y2="7.825" layer="201" rot="R180"/>
<rectangle x1="6.295" y1="7.825" x2="6.305" y2="7.835" layer="201" rot="R180"/>
<rectangle x1="3.475" y1="7.825" x2="3.495" y2="7.835" layer="201" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="MUTEXV2-LOGO">
<text x="0" y="0" size="1.778" layer="94" font="vector" ratio="12">MUTEX Ingenieria</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MUTEXV2-LOGO-BOTTOM">
<gates>
<gate name="G$1" symbol="MUTEXV2-LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MUTEXV2-LOGO-BOTTOM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.1UF" prefix="C">
<description>&lt;h3&gt;0.1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12416"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-(+80/-20%)" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08604"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-KIT-EZ-50V-20%" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08370"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-100V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08390"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="S1" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R1" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S2" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R2" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S3" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R3" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S4" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R4" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S5" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R5" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S6" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R6" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S7" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R7" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S8" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R8" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S9" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R9" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S10" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R10" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S11" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R11" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S12" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R12" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S13" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R13" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S14" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R14" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S15" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R15" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S16" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R16" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S17" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R17" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S18" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R18" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S19" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R19" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S20" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R20" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S21" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R21" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S22" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R22" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S23" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R23" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S24" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R24" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S25" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R25" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S26" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R26" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S27" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R27" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S28" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R28" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S29" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R29" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S30" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R30" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S31" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R31" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S32" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R32" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S33" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R33" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S34" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R34" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S35" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R35" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S36" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R36" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S37" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R37" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S38" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R38" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S39" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R39" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S40" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R40" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S41" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R41" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S42" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R42" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S43" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R43" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S44" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R44" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S45" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R45" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S46" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R46" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S47" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R47" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S48" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R48" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S49" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R49" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S50" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R50" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="R51" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S51" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R52" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S52" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R53" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S53" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R54" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S54" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R55" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="S55" library="switch-coto" library_urn="urn:adsk.eagle:library:374" deviceset="CT10-XXXX-" device="G1" package3d_urn="urn:adsk.eagle:package:27089/1" value="REED"/>
<part name="R56" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="J1" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
<part name="U1" library="SparkFun-Sensors" deviceset="DS18B20" device=""/>
<part name="R62" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/2"/>
<part name="U$1" library="aestethics" deviceset="MUTEXV2-LOGO-BOTTOM" device=""/>
<part name="C1" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="S1" gate="G$1" x="12.7" y="55.88" rot="R90"/>
<instance part="R1" gate="G$1" x="5.08" y="45.72"/>
<instance part="S2" gate="G$1" x="30.48" y="55.88" rot="R90"/>
<instance part="R2" gate="G$1" x="22.86" y="45.72"/>
<instance part="S3" gate="G$1" x="48.26" y="55.88" rot="R90"/>
<instance part="R3" gate="G$1" x="40.64" y="45.72"/>
<instance part="S4" gate="G$1" x="66.04" y="55.88" rot="R90"/>
<instance part="R4" gate="G$1" x="58.42" y="45.72"/>
<instance part="S5" gate="G$1" x="83.82" y="55.88" rot="R90"/>
<instance part="R5" gate="G$1" x="76.2" y="45.72"/>
<instance part="S6" gate="G$1" x="101.6" y="55.88" rot="R90"/>
<instance part="R6" gate="G$1" x="93.98" y="45.72"/>
<instance part="S7" gate="G$1" x="119.38" y="55.88" rot="R90"/>
<instance part="R7" gate="G$1" x="111.76" y="45.72"/>
<instance part="S8" gate="G$1" x="137.16" y="55.88" rot="R90"/>
<instance part="R8" gate="G$1" x="129.54" y="45.72"/>
<instance part="S9" gate="G$1" x="154.94" y="55.88" rot="R90"/>
<instance part="R9" gate="G$1" x="147.32" y="45.72"/>
<instance part="S10" gate="G$1" x="172.72" y="55.88" rot="R90"/>
<instance part="R10" gate="G$1" x="165.1" y="45.72"/>
<instance part="S11" gate="G$1" x="190.5" y="55.88" rot="R90"/>
<instance part="R11" gate="G$1" x="182.88" y="45.72"/>
<instance part="S12" gate="G$1" x="208.28" y="55.88" rot="R90"/>
<instance part="R12" gate="G$1" x="200.66" y="45.72"/>
<instance part="S13" gate="G$1" x="226.06" y="55.88" rot="R90"/>
<instance part="R13" gate="G$1" x="218.44" y="45.72"/>
<instance part="S14" gate="G$1" x="243.84" y="55.88" rot="R90"/>
<instance part="R14" gate="G$1" x="236.22" y="45.72"/>
<instance part="S15" gate="G$1" x="261.62" y="55.88" rot="R90"/>
<instance part="R15" gate="G$1" x="254" y="45.72"/>
<instance part="S16" gate="G$1" x="279.4" y="55.88" rot="R90"/>
<instance part="R16" gate="G$1" x="271.78" y="45.72"/>
<instance part="S17" gate="G$1" x="297.18" y="55.88" rot="R90"/>
<instance part="R17" gate="G$1" x="289.56" y="45.72"/>
<instance part="S18" gate="G$1" x="314.96" y="55.88" rot="R90"/>
<instance part="R18" gate="G$1" x="307.34" y="45.72"/>
<instance part="S19" gate="G$1" x="332.74" y="55.88" rot="R90"/>
<instance part="R19" gate="G$1" x="325.12" y="45.72"/>
<instance part="S20" gate="G$1" x="350.52" y="55.88" rot="R90"/>
<instance part="R20" gate="G$1" x="342.9" y="45.72"/>
<instance part="S21" gate="G$1" x="368.3" y="55.88" rot="R90"/>
<instance part="R21" gate="G$1" x="360.68" y="45.72"/>
<instance part="S22" gate="G$1" x="386.08" y="55.88" rot="R90"/>
<instance part="R22" gate="G$1" x="378.46" y="45.72"/>
<instance part="S23" gate="G$1" x="403.86" y="55.88" rot="R90"/>
<instance part="R23" gate="G$1" x="396.24" y="45.72"/>
<instance part="S24" gate="G$1" x="421.64" y="55.88" rot="R90"/>
<instance part="R24" gate="G$1" x="414.02" y="45.72"/>
<instance part="S25" gate="G$1" x="439.42" y="55.88" rot="R90"/>
<instance part="R25" gate="G$1" x="431.8" y="45.72"/>
<instance part="S26" gate="G$1" x="457.2" y="55.88" rot="R90"/>
<instance part="R26" gate="G$1" x="449.58" y="45.72"/>
<instance part="S27" gate="G$1" x="474.98" y="55.88" rot="R90"/>
<instance part="R27" gate="G$1" x="467.36" y="45.72"/>
<instance part="S28" gate="G$1" x="492.76" y="55.88" rot="R90"/>
<instance part="R28" gate="G$1" x="485.14" y="45.72"/>
<instance part="S29" gate="G$1" x="510.54" y="55.88" rot="R90"/>
<instance part="R29" gate="G$1" x="502.92" y="45.72"/>
<instance part="S30" gate="G$1" x="528.32" y="55.88" rot="R90"/>
<instance part="R30" gate="G$1" x="520.7" y="45.72"/>
<instance part="S31" gate="G$1" x="546.1" y="55.88" rot="R90"/>
<instance part="R31" gate="G$1" x="538.48" y="45.72"/>
<instance part="S32" gate="G$1" x="563.88" y="55.88" rot="R90"/>
<instance part="R32" gate="G$1" x="556.26" y="45.72"/>
<instance part="S33" gate="G$1" x="581.66" y="55.88" rot="R90"/>
<instance part="R33" gate="G$1" x="574.04" y="45.72"/>
<instance part="S34" gate="G$1" x="599.44" y="55.88" rot="R90"/>
<instance part="R34" gate="G$1" x="591.82" y="45.72"/>
<instance part="S35" gate="G$1" x="617.22" y="55.88" rot="R90"/>
<instance part="R35" gate="G$1" x="609.6" y="45.72"/>
<instance part="S36" gate="G$1" x="635" y="55.88" rot="R90"/>
<instance part="R36" gate="G$1" x="627.38" y="45.72"/>
<instance part="S37" gate="G$1" x="652.78" y="55.88" rot="R90"/>
<instance part="R37" gate="G$1" x="645.16" y="45.72"/>
<instance part="S38" gate="G$1" x="670.56" y="55.88" rot="R90"/>
<instance part="R38" gate="G$1" x="662.94" y="45.72"/>
<instance part="S39" gate="G$1" x="688.34" y="55.88" rot="R90"/>
<instance part="R39" gate="G$1" x="680.72" y="45.72"/>
<instance part="S40" gate="G$1" x="706.12" y="55.88" rot="R90"/>
<instance part="R40" gate="G$1" x="698.5" y="45.72"/>
<instance part="S41" gate="G$1" x="723.9" y="55.88" rot="R90"/>
<instance part="R41" gate="G$1" x="716.28" y="45.72"/>
<instance part="S42" gate="G$1" x="741.68" y="55.88" rot="R90"/>
<instance part="R42" gate="G$1" x="734.06" y="45.72"/>
<instance part="S43" gate="G$1" x="759.46" y="55.88" rot="R90"/>
<instance part="R43" gate="G$1" x="751.84" y="45.72"/>
<instance part="S44" gate="G$1" x="777.24" y="55.88" rot="R90"/>
<instance part="R44" gate="G$1" x="769.62" y="45.72"/>
<instance part="S45" gate="G$1" x="795.02" y="55.88" rot="R90"/>
<instance part="R45" gate="G$1" x="787.4" y="45.72"/>
<instance part="S46" gate="G$1" x="812.8" y="55.88" rot="R90"/>
<instance part="R46" gate="G$1" x="805.18" y="45.72"/>
<instance part="S47" gate="G$1" x="830.58" y="55.88" rot="R90"/>
<instance part="R47" gate="G$1" x="822.96" y="45.72"/>
<instance part="S48" gate="G$1" x="848.36" y="55.88" rot="R90"/>
<instance part="R48" gate="G$1" x="840.74" y="45.72"/>
<instance part="S49" gate="G$1" x="866.14" y="55.88" rot="R90"/>
<instance part="R49" gate="G$1" x="858.52" y="45.72"/>
<instance part="S50" gate="G$1" x="883.92" y="55.88" rot="R90"/>
<instance part="R50" gate="G$1" x="876.3" y="45.72"/>
<instance part="R51" gate="G$1" x="-5.08" y="73.66" rot="R90"/>
<instance part="S51" gate="G$1" x="904.24" y="55.88" rot="R90"/>
<instance part="R52" gate="G$1" x="896.62" y="45.72"/>
<instance part="S52" gate="G$1" x="922.02" y="55.88" rot="R90"/>
<instance part="R53" gate="G$1" x="914.4" y="45.72"/>
<instance part="S53" gate="G$1" x="939.8" y="55.88" rot="R90"/>
<instance part="R54" gate="G$1" x="932.18" y="45.72"/>
<instance part="S54" gate="G$1" x="957.58" y="55.88" rot="R90"/>
<instance part="R55" gate="G$1" x="949.96" y="45.72"/>
<instance part="S55" gate="G$1" x="975.36" y="55.88" rot="R90"/>
<instance part="R56" gate="G$1" x="967.74" y="45.72"/>
<instance part="J1" gate="G$1" x="-58.42" y="55.88"/>
<instance part="J2" gate="G$1" x="-58.42" y="81.28"/>
<instance part="U1" gate="G$1" x="10.16" y="111.76"/>
<instance part="R62" gate="G$1" x="-7.62" y="124.46" rot="R90"/>
<instance part="U$1" gate="G$1" x="5.08" y="91.44"/>
<instance part="C1" gate="G$1" x="-30.48" y="109.22"/>
<instance part="GND1" gate="1" x="-30.48" y="101.6"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="12.7" y1="50.8" x2="12.7" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="12.7" y1="45.72" x2="10.16" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="17.78" y1="45.72" x2="12.7" y2="45.72" width="0.1524" layer="91"/>
<junction x="12.7" y="45.72"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<label x="-15.24" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="0" y1="45.72" x2="-15.24" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="53.34" x2="-40.64" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="53.34" x2="-40.64" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="78.74" x2="-40.64" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="78.74" x2="-40.64" y2="78.74" width="0.1524" layer="91"/>
<junction x="-40.64" y="78.74"/>
<label x="-40.64" y="96.52" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="0" y1="106.68" x2="-12.7" y2="106.68" width="0.1524" layer="91"/>
<label x="-12.7" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-30.48" y1="106.68" x2="-30.48" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="30.48" y1="50.8" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="30.48" y1="45.72" x2="27.94" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="35.56" y1="45.72" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<junction x="30.48" y="45.72"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="1"/>
<wire x1="48.26" y1="50.8" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="48.26" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="53.34" y1="45.72" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<junction x="48.26" y="45.72"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="S4" gate="G$1" pin="1"/>
<wire x1="66.04" y1="50.8" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="66.04" y1="45.72" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="71.12" y1="45.72" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<junction x="66.04" y="45.72"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="S5" gate="G$1" pin="1"/>
<wire x1="83.82" y1="50.8" x2="83.82" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="83.82" y1="45.72" x2="81.28" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="88.9" y1="45.72" x2="83.82" y2="45.72" width="0.1524" layer="91"/>
<junction x="83.82" y="45.72"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="S6" gate="G$1" pin="1"/>
<wire x1="101.6" y1="50.8" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="101.6" y1="45.72" x2="99.06" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="106.68" y1="45.72" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<junction x="101.6" y="45.72"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="S7" gate="G$1" pin="1"/>
<wire x1="119.38" y1="50.8" x2="119.38" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="119.38" y1="45.72" x2="116.84" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="124.46" y1="45.72" x2="119.38" y2="45.72" width="0.1524" layer="91"/>
<junction x="119.38" y="45.72"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="S8" gate="G$1" pin="1"/>
<wire x1="137.16" y1="50.8" x2="137.16" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="137.16" y1="45.72" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="142.24" y1="45.72" x2="137.16" y2="45.72" width="0.1524" layer="91"/>
<junction x="137.16" y="45.72"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="S9" gate="G$1" pin="1"/>
<wire x1="154.94" y1="50.8" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="154.94" y1="45.72" x2="152.4" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="160.02" y1="45.72" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<junction x="154.94" y="45.72"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="S10" gate="G$1" pin="1"/>
<wire x1="172.72" y1="50.8" x2="172.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="172.72" y1="45.72" x2="170.18" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="177.8" y1="45.72" x2="172.72" y2="45.72" width="0.1524" layer="91"/>
<junction x="172.72" y="45.72"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="S11" gate="G$1" pin="1"/>
<wire x1="190.5" y1="50.8" x2="190.5" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="190.5" y1="45.72" x2="187.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="195.58" y1="45.72" x2="190.5" y2="45.72" width="0.1524" layer="91"/>
<junction x="190.5" y="45.72"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="S12" gate="G$1" pin="1"/>
<wire x1="208.28" y1="50.8" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="208.28" y1="45.72" x2="205.74" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="213.36" y1="45.72" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
<junction x="208.28" y="45.72"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="S13" gate="G$1" pin="1"/>
<wire x1="226.06" y1="50.8" x2="226.06" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="226.06" y1="45.72" x2="223.52" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="231.14" y1="45.72" x2="226.06" y2="45.72" width="0.1524" layer="91"/>
<junction x="226.06" y="45.72"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="S14" gate="G$1" pin="1"/>
<wire x1="243.84" y1="50.8" x2="243.84" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="243.84" y1="45.72" x2="241.3" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="248.92" y1="45.72" x2="243.84" y2="45.72" width="0.1524" layer="91"/>
<junction x="243.84" y="45.72"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="S15" gate="G$1" pin="1"/>
<wire x1="261.62" y1="50.8" x2="261.62" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="261.62" y1="45.72" x2="259.08" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="266.7" y1="45.72" x2="261.62" y2="45.72" width="0.1524" layer="91"/>
<junction x="261.62" y="45.72"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="S16" gate="G$1" pin="1"/>
<wire x1="279.4" y1="50.8" x2="279.4" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="279.4" y1="45.72" x2="276.86" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="284.48" y1="45.72" x2="279.4" y2="45.72" width="0.1524" layer="91"/>
<junction x="279.4" y="45.72"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="S17" gate="G$1" pin="1"/>
<wire x1="297.18" y1="50.8" x2="297.18" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="297.18" y1="45.72" x2="294.64" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="302.26" y1="45.72" x2="297.18" y2="45.72" width="0.1524" layer="91"/>
<junction x="297.18" y="45.72"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="S18" gate="G$1" pin="1"/>
<wire x1="314.96" y1="50.8" x2="314.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="314.96" y1="45.72" x2="312.42" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="320.04" y1="45.72" x2="314.96" y2="45.72" width="0.1524" layer="91"/>
<junction x="314.96" y="45.72"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="S21" gate="G$1" pin="1"/>
<wire x1="368.3" y1="50.8" x2="368.3" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="368.3" y1="45.72" x2="365.76" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="373.38" y1="45.72" x2="368.3" y2="45.72" width="0.1524" layer="91"/>
<junction x="368.3" y="45.72"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="S22" gate="G$1" pin="1"/>
<wire x1="386.08" y1="50.8" x2="386.08" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="386.08" y1="45.72" x2="383.54" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="391.16" y1="45.72" x2="386.08" y2="45.72" width="0.1524" layer="91"/>
<junction x="386.08" y="45.72"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="S23" gate="G$1" pin="1"/>
<wire x1="403.86" y1="50.8" x2="403.86" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="403.86" y1="45.72" x2="401.32" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="408.94" y1="45.72" x2="403.86" y2="45.72" width="0.1524" layer="91"/>
<junction x="403.86" y="45.72"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="S24" gate="G$1" pin="1"/>
<wire x1="421.64" y1="50.8" x2="421.64" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="421.64" y1="45.72" x2="419.1" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="426.72" y1="45.72" x2="421.64" y2="45.72" width="0.1524" layer="91"/>
<junction x="421.64" y="45.72"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="S25" gate="G$1" pin="1"/>
<wire x1="439.42" y1="50.8" x2="439.42" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="439.42" y1="45.72" x2="436.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="444.5" y1="45.72" x2="439.42" y2="45.72" width="0.1524" layer="91"/>
<junction x="439.42" y="45.72"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="S26" gate="G$1" pin="1"/>
<wire x1="457.2" y1="50.8" x2="457.2" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="457.2" y1="45.72" x2="454.66" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="462.28" y1="45.72" x2="457.2" y2="45.72" width="0.1524" layer="91"/>
<junction x="457.2" y="45.72"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="S27" gate="G$1" pin="1"/>
<wire x1="474.98" y1="50.8" x2="474.98" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="474.98" y1="45.72" x2="472.44" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="480.06" y1="45.72" x2="474.98" y2="45.72" width="0.1524" layer="91"/>
<junction x="474.98" y="45.72"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="S28" gate="G$1" pin="1"/>
<wire x1="492.76" y1="50.8" x2="492.76" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="492.76" y1="45.72" x2="490.22" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="497.84" y1="45.72" x2="492.76" y2="45.72" width="0.1524" layer="91"/>
<junction x="492.76" y="45.72"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="S29" gate="G$1" pin="1"/>
<wire x1="510.54" y1="50.8" x2="510.54" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="510.54" y1="45.72" x2="508" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="515.62" y1="45.72" x2="510.54" y2="45.72" width="0.1524" layer="91"/>
<junction x="510.54" y="45.72"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="S30" gate="G$1" pin="1"/>
<wire x1="528.32" y1="50.8" x2="528.32" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="528.32" y1="45.72" x2="525.78" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="533.4" y1="45.72" x2="528.32" y2="45.72" width="0.1524" layer="91"/>
<junction x="528.32" y="45.72"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="S31" gate="G$1" pin="1"/>
<wire x1="546.1" y1="50.8" x2="546.1" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="546.1" y1="45.72" x2="543.56" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="551.18" y1="45.72" x2="546.1" y2="45.72" width="0.1524" layer="91"/>
<junction x="546.1" y="45.72"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="S32" gate="G$1" pin="1"/>
<wire x1="563.88" y1="50.8" x2="563.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="563.88" y1="45.72" x2="561.34" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="568.96" y1="45.72" x2="563.88" y2="45.72" width="0.1524" layer="91"/>
<junction x="563.88" y="45.72"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="S33" gate="G$1" pin="1"/>
<wire x1="581.66" y1="50.8" x2="581.66" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="581.66" y1="45.72" x2="579.12" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="586.74" y1="45.72" x2="581.66" y2="45.72" width="0.1524" layer="91"/>
<junction x="581.66" y="45.72"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="S34" gate="G$1" pin="1"/>
<wire x1="599.44" y1="50.8" x2="599.44" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="599.44" y1="45.72" x2="596.9" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="604.52" y1="45.72" x2="599.44" y2="45.72" width="0.1524" layer="91"/>
<junction x="599.44" y="45.72"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="S35" gate="G$1" pin="1"/>
<wire x1="617.22" y1="50.8" x2="617.22" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="617.22" y1="45.72" x2="614.68" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="622.3" y1="45.72" x2="617.22" y2="45.72" width="0.1524" layer="91"/>
<junction x="617.22" y="45.72"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="S36" gate="G$1" pin="1"/>
<wire x1="635" y1="50.8" x2="635" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="635" y1="45.72" x2="632.46" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="640.08" y1="45.72" x2="635" y2="45.72" width="0.1524" layer="91"/>
<junction x="635" y="45.72"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="S37" gate="G$1" pin="1"/>
<wire x1="652.78" y1="50.8" x2="652.78" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="652.78" y1="45.72" x2="650.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="657.86" y1="45.72" x2="652.78" y2="45.72" width="0.1524" layer="91"/>
<junction x="652.78" y="45.72"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="S39" gate="G$1" pin="1"/>
<wire x1="688.34" y1="50.8" x2="688.34" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="688.34" y1="45.72" x2="685.8" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="693.42" y1="45.72" x2="688.34" y2="45.72" width="0.1524" layer="91"/>
<junction x="688.34" y="45.72"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="S40" gate="G$1" pin="1"/>
<wire x1="706.12" y1="50.8" x2="706.12" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="706.12" y1="45.72" x2="703.58" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="711.2" y1="45.72" x2="706.12" y2="45.72" width="0.1524" layer="91"/>
<junction x="706.12" y="45.72"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="S41" gate="G$1" pin="1"/>
<wire x1="723.9" y1="50.8" x2="723.9" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="723.9" y1="45.72" x2="721.36" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="728.98" y1="45.72" x2="723.9" y2="45.72" width="0.1524" layer="91"/>
<junction x="723.9" y="45.72"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="S42" gate="G$1" pin="1"/>
<wire x1="741.68" y1="50.8" x2="741.68" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="741.68" y1="45.72" x2="739.14" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="746.76" y1="45.72" x2="741.68" y2="45.72" width="0.1524" layer="91"/>
<junction x="741.68" y="45.72"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="S43" gate="G$1" pin="1"/>
<wire x1="759.46" y1="50.8" x2="759.46" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="759.46" y1="45.72" x2="756.92" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="764.54" y1="45.72" x2="759.46" y2="45.72" width="0.1524" layer="91"/>
<junction x="759.46" y="45.72"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="S44" gate="G$1" pin="1"/>
<wire x1="777.24" y1="50.8" x2="777.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="777.24" y1="45.72" x2="774.7" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="782.32" y1="45.72" x2="777.24" y2="45.72" width="0.1524" layer="91"/>
<junction x="777.24" y="45.72"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="S45" gate="G$1" pin="1"/>
<wire x1="795.02" y1="50.8" x2="795.02" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="795.02" y1="45.72" x2="792.48" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="800.1" y1="45.72" x2="795.02" y2="45.72" width="0.1524" layer="91"/>
<junction x="795.02" y="45.72"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="S46" gate="G$1" pin="1"/>
<wire x1="812.8" y1="50.8" x2="812.8" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="812.8" y1="45.72" x2="810.26" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="817.88" y1="45.72" x2="812.8" y2="45.72" width="0.1524" layer="91"/>
<junction x="812.8" y="45.72"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="S47" gate="G$1" pin="1"/>
<wire x1="830.58" y1="50.8" x2="830.58" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="830.58" y1="45.72" x2="828.04" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="835.66" y1="45.72" x2="830.58" y2="45.72" width="0.1524" layer="91"/>
<junction x="830.58" y="45.72"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="S48" gate="G$1" pin="1"/>
<wire x1="848.36" y1="50.8" x2="848.36" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="848.36" y1="45.72" x2="845.82" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="853.44" y1="45.72" x2="848.36" y2="45.72" width="0.1524" layer="91"/>
<junction x="848.36" y="45.72"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="S49" gate="G$1" pin="1"/>
<wire x1="866.14" y1="50.8" x2="866.14" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="866.14" y1="45.72" x2="863.6" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="871.22" y1="45.72" x2="866.14" y2="45.72" width="0.1524" layer="91"/>
<junction x="866.14" y="45.72"/>
</segment>
</net>
<net name="R_OUT" class="0">
<segment>
<pinref part="S50" gate="G$1" pin="1"/>
<wire x1="883.92" y1="50.8" x2="883.92" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="883.92" y1="45.72" x2="881.38" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="891.54" y1="45.72" x2="883.92" y2="45.72" width="0.1524" layer="91"/>
<junction x="883.92" y="45.72"/>
</segment>
</net>
<net name="+VCC" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="78.74" x2="-5.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="81.28" x2="-15.24" y2="81.28" width="0.1524" layer="91"/>
<label x="-15.24" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="-53.34" y1="60.96" x2="-48.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="60.96" x2="-48.26" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="-48.26" y1="86.36" x2="-48.26" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="86.36" x2="-48.26" y2="86.36" width="0.1524" layer="91"/>
<junction x="-48.26" y="86.36"/>
<label x="-48.26" y="96.52" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="0" y1="116.84" x2="-2.54" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="116.84" x2="-2.54" y2="134.62" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="134.62" x2="-2.54" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="129.54" x2="-7.62" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="134.62" x2="-2.54" y2="134.62" width="0.1524" layer="91"/>
<junction x="-2.54" y="134.62"/>
<label x="-2.54" y="137.16" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-30.48" y1="114.3" x2="-30.48" y2="119.38" width="0.1524" layer="91"/>
<label x="-30.48" y="119.38" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="S20" gate="G$1" pin="1"/>
<wire x1="350.52" y1="50.8" x2="350.52" y2="45.72" width="0.1524" layer="91"/>
<wire x1="350.52" y1="45.72" x2="347.98" y2="45.72" width="0.1524" layer="91"/>
<wire x1="355.6" y1="45.72" x2="350.52" y2="45.72" width="0.1524" layer="91"/>
<junction x="350.52" y="45.72"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="S19" gate="G$1" pin="1"/>
<wire x1="332.74" y1="50.8" x2="332.74" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="332.74" y1="45.72" x2="330.2" y2="45.72" width="0.1524" layer="91"/>
<wire x1="337.82" y1="45.72" x2="332.74" y2="45.72" width="0.1524" layer="91"/>
<junction x="332.74" y="45.72"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<pinref part="S38" gate="G$1" pin="1"/>
<wire x1="670.56" y1="50.8" x2="670.56" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="670.56" y1="45.72" x2="668.02" y2="45.72" width="0.1524" layer="91"/>
<wire x1="675.64" y1="45.72" x2="670.56" y2="45.72" width="0.1524" layer="91"/>
<junction x="670.56" y="45.72"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="S51" gate="G$1" pin="1"/>
<wire x1="904.24" y1="50.8" x2="904.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="904.24" y1="45.72" x2="901.7" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="909.32" y1="45.72" x2="904.24" y2="45.72" width="0.1524" layer="91"/>
<junction x="904.24" y="45.72"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="S52" gate="G$1" pin="1"/>
<wire x1="922.02" y1="50.8" x2="922.02" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
<wire x1="922.02" y1="45.72" x2="919.48" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="927.1" y1="45.72" x2="922.02" y2="45.72" width="0.1524" layer="91"/>
<junction x="922.02" y="45.72"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="S53" gate="G$1" pin="1"/>
<wire x1="939.8" y1="50.8" x2="939.8" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="939.8" y1="45.72" x2="937.26" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="944.88" y1="45.72" x2="939.8" y2="45.72" width="0.1524" layer="91"/>
<junction x="939.8" y="45.72"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="S54" gate="G$1" pin="1"/>
<wire x1="957.58" y1="50.8" x2="957.58" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="957.58" y1="45.72" x2="955.04" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="962.66" y1="45.72" x2="957.58" y2="45.72" width="0.1524" layer="91"/>
<junction x="957.58" y="45.72"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="S55" gate="G$1" pin="1"/>
<wire x1="975.36" y1="50.8" x2="975.36" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="975.36" y1="45.72" x2="972.82" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SIGNAL" class="0">
<segment>
<pinref part="S54" gate="G$1" pin="2"/>
<wire x1="957.58" y1="60.96" x2="957.58" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S55" gate="G$1" pin="2"/>
<wire x1="975.36" y1="60.96" x2="975.36" y2="63.5" width="0.1524" layer="91"/>
<wire x1="975.36" y1="63.5" x2="957.58" y2="63.5" width="0.1524" layer="91"/>
<junction x="957.58" y="63.5"/>
<wire x1="939.8" y1="63.5" x2="957.58" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S53" gate="G$1" pin="2"/>
<wire x1="939.8" y1="60.96" x2="939.8" y2="63.5" width="0.1524" layer="91"/>
<junction x="939.8" y="63.5"/>
<wire x1="922.02" y1="63.5" x2="939.8" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S52" gate="G$1" pin="2"/>
<wire x1="922.02" y1="60.96" x2="922.02" y2="63.5" width="0.1524" layer="91"/>
<wire x1="883.92" y1="63.5" x2="904.24" y2="63.5" width="0.1524" layer="91"/>
<junction x="922.02" y="63.5"/>
<pinref part="S51" gate="G$1" pin="2"/>
<wire x1="904.24" y1="63.5" x2="922.02" y2="63.5" width="0.1524" layer="91"/>
<wire x1="904.24" y1="60.96" x2="904.24" y2="63.5" width="0.1524" layer="91"/>
<junction x="904.24" y="63.5"/>
<pinref part="S39" gate="G$1" pin="2"/>
<wire x1="688.34" y1="60.96" x2="688.34" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S43" gate="G$1" pin="2"/>
<wire x1="759.46" y1="60.96" x2="759.46" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S46" gate="G$1" pin="2"/>
<wire x1="812.8" y1="60.96" x2="812.8" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S50" gate="G$1" pin="2"/>
<wire x1="883.92" y1="60.96" x2="883.92" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S49" gate="G$1" pin="2"/>
<wire x1="866.14" y1="60.96" x2="866.14" y2="63.5" width="0.1524" layer="91"/>
<wire x1="866.14" y1="63.5" x2="883.92" y2="63.5" width="0.1524" layer="91"/>
<junction x="866.14" y="63.5"/>
<wire x1="848.36" y1="63.5" x2="866.14" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S48" gate="G$1" pin="2"/>
<wire x1="848.36" y1="60.96" x2="848.36" y2="63.5" width="0.1524" layer="91"/>
<junction x="848.36" y="63.5"/>
<pinref part="S47" gate="G$1" pin="2"/>
<wire x1="830.58" y1="60.96" x2="830.58" y2="63.5" width="0.1524" layer="91"/>
<wire x1="830.58" y1="63.5" x2="848.36" y2="63.5" width="0.1524" layer="91"/>
<junction x="830.58" y="63.5"/>
<wire x1="830.58" y1="63.5" x2="812.8" y2="63.5" width="0.1524" layer="91"/>
<junction x="812.8" y="63.5"/>
<wire x1="795.02" y1="63.5" x2="812.8" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S45" gate="G$1" pin="2"/>
<wire x1="795.02" y1="60.96" x2="795.02" y2="63.5" width="0.1524" layer="91"/>
<junction x="795.02" y="63.5"/>
<pinref part="S44" gate="G$1" pin="2"/>
<wire x1="777.24" y1="60.96" x2="777.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="777.24" y1="63.5" x2="795.02" y2="63.5" width="0.1524" layer="91"/>
<junction x="777.24" y="63.5"/>
<wire x1="777.24" y1="63.5" x2="759.46" y2="63.5" width="0.1524" layer="91"/>
<junction x="759.46" y="63.5"/>
<wire x1="741.68" y1="63.5" x2="759.46" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S42" gate="G$1" pin="2"/>
<wire x1="741.68" y1="60.96" x2="741.68" y2="63.5" width="0.1524" layer="91"/>
<junction x="741.68" y="63.5"/>
<wire x1="723.9" y1="63.5" x2="741.68" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S41" gate="G$1" pin="2"/>
<wire x1="723.9" y1="60.96" x2="723.9" y2="63.5" width="0.1524" layer="91"/>
<wire x1="688.34" y1="63.5" x2="706.12" y2="63.5" width="0.1524" layer="91"/>
<junction x="688.34" y="63.5"/>
<junction x="723.9" y="63.5"/>
<pinref part="S40" gate="G$1" pin="2"/>
<wire x1="706.12" y1="63.5" x2="723.9" y2="63.5" width="0.1524" layer="91"/>
<wire x1="706.12" y1="60.96" x2="706.12" y2="63.5" width="0.1524" layer="91"/>
<junction x="706.12" y="63.5"/>
<pinref part="S21" gate="G$1" pin="2"/>
<wire x1="368.3" y1="60.96" x2="368.3" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S22" gate="G$1" pin="2"/>
<wire x1="386.08" y1="60.96" x2="386.08" y2="63.5" width="0.1524" layer="91"/>
<wire x1="386.08" y1="63.5" x2="368.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="386.08" y1="63.5" x2="403.86" y2="63.5" width="0.1524" layer="91"/>
<junction x="386.08" y="63.5"/>
<pinref part="S24" gate="G$1" pin="2"/>
<wire x1="403.86" y1="63.5" x2="421.64" y2="63.5" width="0.1524" layer="91"/>
<wire x1="421.64" y1="60.96" x2="421.64" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S23" gate="G$1" pin="2"/>
<wire x1="403.86" y1="60.96" x2="403.86" y2="63.5" width="0.1524" layer="91"/>
<junction x="403.86" y="63.5"/>
<pinref part="S25" gate="G$1" pin="2"/>
<wire x1="421.64" y1="63.5" x2="439.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="439.42" y1="60.96" x2="439.42" y2="63.5" width="0.1524" layer="91"/>
<junction x="421.64" y="63.5"/>
<pinref part="S26" gate="G$1" pin="2"/>
<wire x1="457.2" y1="60.96" x2="457.2" y2="63.5" width="0.1524" layer="91"/>
<wire x1="457.2" y1="63.5" x2="439.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="457.2" y1="63.5" x2="474.98" y2="63.5" width="0.1524" layer="91"/>
<junction x="457.2" y="63.5"/>
<pinref part="S28" gate="G$1" pin="2"/>
<wire x1="474.98" y1="63.5" x2="492.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="492.76" y1="60.96" x2="492.76" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S27" gate="G$1" pin="2"/>
<wire x1="474.98" y1="60.96" x2="474.98" y2="63.5" width="0.1524" layer="91"/>
<junction x="474.98" y="63.5"/>
<pinref part="S29" gate="G$1" pin="2"/>
<wire x1="510.54" y1="60.96" x2="510.54" y2="63.5" width="0.1524" layer="91"/>
<wire x1="510.54" y1="63.5" x2="492.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="510.54" y1="63.5" x2="528.32" y2="63.5" width="0.1524" layer="91"/>
<junction x="510.54" y="63.5"/>
<pinref part="S31" gate="G$1" pin="2"/>
<wire x1="528.32" y1="63.5" x2="546.1" y2="63.5" width="0.1524" layer="91"/>
<wire x1="546.1" y1="60.96" x2="546.1" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S30" gate="G$1" pin="2"/>
<wire x1="528.32" y1="60.96" x2="528.32" y2="63.5" width="0.1524" layer="91"/>
<junction x="528.32" y="63.5"/>
<junction x="492.76" y="63.5"/>
<pinref part="S32" gate="G$1" pin="2"/>
<wire x1="563.88" y1="60.96" x2="563.88" y2="63.5" width="0.1524" layer="91"/>
<wire x1="563.88" y1="63.5" x2="546.1" y2="63.5" width="0.1524" layer="91"/>
<wire x1="563.88" y1="63.5" x2="581.66" y2="63.5" width="0.1524" layer="91"/>
<junction x="563.88" y="63.5"/>
<pinref part="S34" gate="G$1" pin="2"/>
<wire x1="581.66" y1="63.5" x2="599.44" y2="63.5" width="0.1524" layer="91"/>
<wire x1="599.44" y1="60.96" x2="599.44" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S33" gate="G$1" pin="2"/>
<wire x1="581.66" y1="60.96" x2="581.66" y2="63.5" width="0.1524" layer="91"/>
<junction x="581.66" y="63.5"/>
<pinref part="S35" gate="G$1" pin="2"/>
<wire x1="617.22" y1="60.96" x2="617.22" y2="63.5" width="0.1524" layer="91"/>
<wire x1="617.22" y1="63.5" x2="599.44" y2="63.5" width="0.1524" layer="91"/>
<wire x1="617.22" y1="63.5" x2="635" y2="63.5" width="0.1524" layer="91"/>
<junction x="617.22" y="63.5"/>
<pinref part="S37" gate="G$1" pin="2"/>
<wire x1="635" y1="63.5" x2="652.78" y2="63.5" width="0.1524" layer="91"/>
<wire x1="652.78" y1="60.96" x2="652.78" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S36" gate="G$1" pin="2"/>
<wire x1="635" y1="60.96" x2="635" y2="63.5" width="0.1524" layer="91"/>
<junction x="635" y="63.5"/>
<junction x="599.44" y="63.5"/>
<junction x="546.1" y="63.5"/>
<pinref part="S38" gate="G$1" pin="2"/>
<wire x1="670.56" y1="60.96" x2="670.56" y2="63.5" width="0.1524" layer="91"/>
<wire x1="670.56" y1="63.5" x2="652.78" y2="63.5" width="0.1524" layer="91"/>
<junction x="652.78" y="63.5"/>
<junction x="439.42" y="63.5"/>
<wire x1="350.52" y1="63.5" x2="368.3" y2="63.5" width="0.1524" layer="91"/>
<junction x="368.3" y="63.5"/>
<pinref part="S20" gate="G$1" pin="2"/>
<wire x1="350.52" y1="60.96" x2="350.52" y2="63.5" width="0.1524" layer="91"/>
<junction x="350.52" y="63.5"/>
<wire x1="688.34" y1="63.5" x2="670.56" y2="63.5" width="0.1524" layer="91"/>
<junction x="670.56" y="63.5"/>
<pinref part="S12" gate="G$1" pin="2"/>
<wire x1="208.28" y1="60.96" x2="208.28" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S15" gate="G$1" pin="2"/>
<wire x1="261.62" y1="60.96" x2="261.62" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S18" gate="G$1" pin="2"/>
<wire x1="314.96" y1="60.96" x2="314.96" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S19" gate="G$1" pin="2"/>
<wire x1="332.74" y1="60.96" x2="332.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="332.74" y1="63.5" x2="314.96" y2="63.5" width="0.1524" layer="91"/>
<junction x="314.96" y="63.5"/>
<wire x1="297.18" y1="63.5" x2="314.96" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S17" gate="G$1" pin="2"/>
<wire x1="297.18" y1="60.96" x2="297.18" y2="63.5" width="0.1524" layer="91"/>
<junction x="297.18" y="63.5"/>
<pinref part="S16" gate="G$1" pin="2"/>
<wire x1="279.4" y1="60.96" x2="279.4" y2="63.5" width="0.1524" layer="91"/>
<wire x1="279.4" y1="63.5" x2="297.18" y2="63.5" width="0.1524" layer="91"/>
<junction x="279.4" y="63.5"/>
<wire x1="279.4" y1="63.5" x2="261.62" y2="63.5" width="0.1524" layer="91"/>
<junction x="261.62" y="63.5"/>
<wire x1="243.84" y1="63.5" x2="261.62" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S14" gate="G$1" pin="2"/>
<wire x1="243.84" y1="60.96" x2="243.84" y2="63.5" width="0.1524" layer="91"/>
<junction x="243.84" y="63.5"/>
<pinref part="S13" gate="G$1" pin="2"/>
<wire x1="226.06" y1="60.96" x2="226.06" y2="63.5" width="0.1524" layer="91"/>
<wire x1="226.06" y1="63.5" x2="243.84" y2="63.5" width="0.1524" layer="91"/>
<junction x="226.06" y="63.5"/>
<wire x1="226.06" y1="63.5" x2="208.28" y2="63.5" width="0.1524" layer="91"/>
<junction x="208.28" y="63.5"/>
<wire x1="-5.08" y1="63.5" x2="-15.24" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="68.58" x2="-5.08" y2="63.5" width="0.1524" layer="91"/>
<junction x="-5.08" y="63.5"/>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="12.7" y1="60.96" x2="12.7" y2="63.5" width="0.1524" layer="91"/>
<wire x1="12.7" y1="63.5" x2="-5.08" y2="63.5" width="0.1524" layer="91"/>
<junction x="12.7" y="63.5"/>
<wire x1="12.7" y1="63.5" x2="30.48" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="2"/>
<wire x1="30.48" y1="60.96" x2="30.48" y2="63.5" width="0.1524" layer="91"/>
<junction x="30.48" y="63.5"/>
<wire x1="30.48" y1="63.5" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S3" gate="G$1" pin="2"/>
<wire x1="48.26" y1="60.96" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
<junction x="48.26" y="63.5"/>
<pinref part="S4" gate="G$1" pin="2"/>
<wire x1="66.04" y1="60.96" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="66.04" y1="63.5" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
<junction x="66.04" y="63.5"/>
<wire x1="66.04" y1="63.5" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S5" gate="G$1" pin="2"/>
<wire x1="83.82" y1="60.96" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<junction x="83.82" y="63.5"/>
<wire x1="83.82" y1="63.5" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S6" gate="G$1" pin="2"/>
<wire x1="101.6" y1="60.96" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<junction x="101.6" y="63.5"/>
<pinref part="S7" gate="G$1" pin="2"/>
<wire x1="119.38" y1="60.96" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<wire x1="119.38" y1="63.5" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<junction x="119.38" y="63.5"/>
<wire x1="119.38" y1="63.5" x2="137.16" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S8" gate="G$1" pin="2"/>
<wire x1="137.16" y1="60.96" x2="137.16" y2="63.5" width="0.1524" layer="91"/>
<junction x="137.16" y="63.5"/>
<wire x1="137.16" y1="63.5" x2="154.94" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S9" gate="G$1" pin="2"/>
<wire x1="154.94" y1="60.96" x2="154.94" y2="63.5" width="0.1524" layer="91"/>
<junction x="154.94" y="63.5"/>
<pinref part="S10" gate="G$1" pin="2"/>
<wire x1="172.72" y1="60.96" x2="172.72" y2="63.5" width="0.1524" layer="91"/>
<wire x1="172.72" y1="63.5" x2="154.94" y2="63.5" width="0.1524" layer="91"/>
<junction x="172.72" y="63.5"/>
<wire x1="172.72" y1="63.5" x2="190.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="190.5" y1="63.5" x2="208.28" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S11" gate="G$1" pin="2"/>
<wire x1="190.5" y1="60.96" x2="190.5" y2="63.5" width="0.1524" layer="91"/>
<junction x="190.5" y="63.5"/>
<label x="-15.24" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="332.74" y="63.5"/>
<wire x1="350.52" y1="63.5" x2="332.74" y2="63.5" width="0.1524" layer="91"/>
<junction x="883.92" y="63.5"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="55.88" x2="-43.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="55.88" x2="-43.18" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="-43.18" y1="81.28" x2="-53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="81.28" x2="-43.18" y2="96.52" width="0.1524" layer="91"/>
<junction x="-43.18" y="81.28"/>
<label x="-43.18" y="96.52" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="TEMP" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="-53.34" y1="58.42" x2="-45.72" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="58.42" x2="-45.72" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="-45.72" y1="83.82" x2="-45.72" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="83.82" x2="-45.72" y2="83.82" width="0.1524" layer="91"/>
<junction x="-45.72" y="83.82"/>
<label x="-45.72" y="96.52" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="DQ"/>
<wire x1="0" y1="111.76" x2="-7.62" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="111.76" x2="-12.7" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="119.38" x2="-7.62" y2="111.76" width="0.1524" layer="91"/>
<junction x="-7.62" y="111.76"/>
<label x="-12.7" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
